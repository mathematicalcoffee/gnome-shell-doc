// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/*
 * Copyright 2011 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
/**
 * Module for peforming batch tasks. See {@link namespace:LoginDialog} for
 * examples of how to use it.
 * @fileOverview
 */
const Lang = imports.lang;
const Signals = imports.signals;

/** Creates a new Task, being basically a function call.
 * @param {Object} scope - scope for the `handler` (what the `handler` sees as
 * the `this` object).
 * @param {function} handler - function to run (that is the task).
 * No input arguments.
 * It is run via `handler.call(scope)`, so `handler` treats `this` as being
 * `scope`.
 * @classdesc
 * A task. Consists of a function and a scope to run it in (i.e. what is
 * `this` when the function is run).
 * @class
 */
function Task() {
    this._init.apply(this, arguments);
}

Task.prototype = {
    _init: function(scope, handler) {
        if (scope)
            /** The scope in which to run the task (i.e. what is `this`
             * for the function).
             * @type {Object} */
            this.scope = scope;
        else
            this.scope = this;

        /** The function representing the task. No input arguments.
         * @type {function} */
        this.handler = handler;
    },

    /** Run the task. If {@link #scope} was set, it will be run with
     * that as `this`. Otherwise, the instance will be used as `this`.
     * @returns {any} the result of `this.handler.call(this.scope)`, or `null`
     * if there is no handler. */
    run: function() {
        if (this.handler)
            return this.handler.call(this.scope);

        return null;
    },
};
Signals.addSignalMethods(Task.prototype);

// @@?????????
/** Creates a new Hold.
 *
 * This is a {@link Task} with scope being the instance and the task function
 * just returning the hold itself.
 * @classdesc
 * Dummy task that represents a hold on something, where you can acquire
 * a hold and wait for its release.
 *
 * The idea is that instead of just creating a {@link Task}, you create a
 * function that:
 *
 * 1. creates a {@link Hold};
 * 2. do whatever you wanted the {@link Task} to do, **calling
 * {@link #release}** when complete (which may be some seconds after the
 * function actually *returns*, if it is an asynchronous task);
 * 3. returns the {@link Hold}.
 *
 * Make a {@link Task} with the above function and add it to a {@link Batch};
 * in this way the {@link Batch} can (for example) wait until this task has
 * finished before it performs a new one.
 * @class
 * @extends Task
 */
function Hold() {
    this._init.apply(this, arguments);
}

Hold.prototype = {
    __proto__: Task.prototype,

    _init: function() {
        Task.prototype._init.call(this,
                                  this,
                                  function () {
                                      return this;
                                  });

        this._acquisitions = 1;
    },

    /** Acquire a hold. */
    acquire: function() {
        if (this._acquisitions <= 0)
            throw new Error("Cannot acquire hold after it's been released");
        this._acquisitions++;
    },

    /** Wait until a *different* hold has been released before releasing
     * this one.
     * @param {Batch.Hold} hold - the hold we want to wait to finish.
     */
    acquireUntilAfter: function(hold) {
        if (!hold.isAcquired())
            return;

        this.acquire();
        let signalId = hold.connect('release', Lang.bind(this, function() {
                                        hold.disconnect(signalId);
                                        this.release();
                                    }));
    },

    /** Releases the current hold.
     * When all outstanding holds have been released, this hold will emit
     * the {@link .event:release} signal which will in turn cause any holds that
     * have had {@link #acquireUntilAfter} called on *this* hold to be
     * released.
     * @fires .event:release */
    release: function() {
        this._acquisitions--;

        if (this._acquisitions == 0)
            this.emit('release');
    },

    /** Whether a hold has been acquired on this task.
     * @returns {boolean} whether a hold has been acquired on this task. */
    isAcquired: function() {
        return this._acquisitions > 0;
    }
}
Signals.addSignalMethods(Hold.prototype);
/** Fired when all holds have been released on this object.
 * @memberof Hold
 * @name release
 * @event
 */

/** Creates a new Batch (of tasks to run).
 * @param {Object} scope - scope in which to run each task (i.e. what the
 * task thinks `this` is).
 * @param {Array.<Batch.Task or function>} - array of {@link Task}s, functions
 * (tasks will be created for them) or other {@link Batch}es to be run.
 *
 * In order to get most use out of a {@link Batch}, your task function should:
 *
 * 1. creates a {@link Hold};
 * 2. do whatever you wanted the {@link Task} to do, **calling
 * {@link Hold#release}** when complete;
 * 3. returns the {@link Hold}.
 *
 * Then the {@link Batch} can use this to (for example) wait until that task is
 * done before starting the next. This is mainly useful for asynchronous
 * functions (like tweening) where you might call {@link Hold#release} only
 * once the tween is finished in order to queue a sequence of animations.
 *
 * @classdesc A `Batch` is a list of {@link Task}s. The {@link #process}
 * method must be implemented by a subclass which determines how tasks will
 * be run (for example {@link ConcurrentBatch} runs tasks concurrently while
 * {@link ConsecutiveBatch} runs them consecutively).
 *
 * **NOTE**: if you want to make use of the {@link ConcurrentBatch} and
 * {@link ConsecutiveBatch} classes, your tasks should return
 * {@link Hold}s and call {@link Hold#release} when they are finished;
 * see for example {@link LoginDialog.LoginDialog#_showItem}.
 * @class
 * @extends Task
 */
function Batch() {
    this._init.apply(this, arguments);
}

Batch.prototype = {
    __proto__: Task.prototype,

    _init: function(scope, tasks) {
        Task.prototype._init.call(this);

        /** List of tasks.
         * @type {Batch.Task[]} */
        this.tasks = [];

        for (let i = 0; i < tasks.length; i++) {
            let task;

            if (tasks[i] instanceof Task) {
                task = tasks[i];
            } else if (typeof tasks[i] == 'function') {
                task = new Task(scope, tasks[i]);
            } else {
                throw new Error('Batch tasks must be functions or Task, Hold or Batch objects');
            }

            this.tasks.push(task);
        }
    },

    /** Must be implemented.
     * This is called on {@link #_start} and {@link #nextTask}.
     * It must actually run the task in {@link #tasks} somehow, usually by
     * using {@link #runTask} and {@link #nextTask}.
     *
     * It could also interact with {@link #hold} - just make sure that 
     * the 'release' signal fires only when all the tasks are complete.
     * @see Concurrent#process
     * @see Consecutive#process */
    process: function() {
        throw new Error('Not implemented');
    },

    /** Runs the current task.
     * @returns {any} null if we're finished the tasks, or the result of
     * calling {@link Task#run} for the current task otherwise.
     *
     * **NOTE**: if you want to make use of the {@link ConcurrentBatch} and
     * {@link ConsecutiveBatch} classes, your tasks should return
     * {@link Hold}s and call {@link Hold#release} when they are finished;
     * see for example {@link LoginDialog.LoginDialog#_showItem}.
     */
    runTask: function() {
        if (!(this._currentTaskIndex in this.tasks)) {
            return null;
        }

        return this.tasks[this._currentTaskIndex].run();
    },

    /** Called when all the tasks have finished. Calls {@link Hold#release} on
     * {@link #hold}, so one can connect to the 'release' signal to
     * determine when *all* acquired holds have been released (i.e. all
     * tasks are complete). */
    _finish: function() {
        this.hold.release();
    },

    /** Moves on to the next task and calls {@link #process}.
     * If we've finished all the task, calls {@link #_finish}. */
    nextTask: function() {
        this._currentTaskIndex++;

        // if the entire batch of tasks is finished, release
        // the hold and notify anyone waiting on the batch
        if (this._currentTaskIndex >= this.tasks.length) {
            this._finish();
            return;
        }

        this.process();
    },

    /** Called when the user calls {@link #run}. Acquires a hold to
     * get released when the entire batch of tasks is finished, then calls
     * {@link #process}. */
    _start: function() {
        /** A hold to get released when the entire batch of tasks is finished.
         * @type {Batch.Hold} */
        this.hold = new Hold();
        this._currentTaskIndex = 0;
        this.process();
    },

    /** Runs the batch of tasks.
     * @returns {Batch.Hold} a hold on the batch of tasks(?) */
    run: function() {
        this._start();

        // hold may be destroyed at this point
        // if we're already done running
        return this.hold;
    },

    /** Cancels the batch by clearing {@link #tasks} (??) */
    cancel: function() {
        this.tasks = this.tasks.splice(0, this._currentTaskIndex + 1);
    }

};
Signals.addSignalMethods(Batch.prototype);

/** Creates a new ConcurrentBatch (of tasks).
 * @classdesc
 * This is a batch of tasks that are run concurrently, i.e. the next task in
 * the list is started straight after the current has been started without
 * necessarily waiting for the current to finish first.
 *
 * This is implemented as follows (in {@link #process}):
 *
 * 1. the current task is started using {@link #runTask}.
 * 2. if it returns a {@link Hold}, acquire a hold on it using
 * {@link Hold#acquireUntilAfter} and {@link #hold}.
 * 3. start the next task ({@link #nextTask}).
 *
 * By virtue of the `aquireUntilAfter` call, {@link #hold} will *not* emit
 * the ['release']{@link Hold.event:release} signal until *all* the tasks that returned
 * {@link Hold}s have completed.
 *
 * Hence the user can connect to {@link #hold}'s 'release' signal
 * and know that upon this signal, all the tasks in the batch have completed.
 *
 * @class
 * @extends Batch */
function ConcurrentBatch() {
    this._init.apply(this, arguments);
}

ConcurrentBatch.prototype = {
    __proto__: Batch.prototype,

    _init: function(scope, tasks) {
        Batch.prototype._init.call(this, scope, tasks);
    },

    /** Implements the parent function - all this does is start each task
     * as quick as possible after the previous, making sure that
     * {@link #hold} doesn't emit its 'release' signal until
     * all the tasks in the list have.
     *
     * 1. the current task is started using {@link #runTask}.
     * 2. if it returns a {@link Hold}, acquire a hold on it using
     * {@link Hold#acquireUntilAfter} and {@link #hold}.
     * 3. start the next task ({@link #nextTask}).
     *
     * By virtue of the `aquireUntilAfter` call, {@link #hold} will *not* emit
     * the ['release']{@link Hold.event:release} signal until *all* the tasks that returned
     * {@link Hold}s have completed.
     *
     * Hence the user can connect to {@link #hold}'s 'release' signal
     * and know that upon this signal, all the tasks in the batch have completed.
     * @override
     */
    process: function() {
       let hold = this.runTask();

       if (hold) {
           this.hold.acquireUntilAfter(hold);
       }

       // Regardless of the state of the just run task,
       // fire off the next one, so all the tasks can run
       // concurrently.
       this.nextTask();
    }
};
Signals.addSignalMethods(ConcurrentBatch.prototype);

/** Creates a new ConsecutiveBatch (of tasks).
 * @classdesc
 * This is a batch of tasks that are run consecutively, i.e. the next task in
 * the list is not started until the current has finished.
 *
 * This is implemented as follows (in {@link #process}):
 *
 * 1. the current task is started using {@link #runTask}.
 * 2. if it returns a {@link Hold}, wait until its 'release' signal (signalling
 * that the task is complete) before we can start the next task with
 * {@link #nextTask}.
 * 3. if it didn't return the hold or the hold is finished already, just go
 * straight on to the next task.
 *
 * @class
 * @extends Batch */
function ConsecutiveBatch() {
    this._init.apply(this, arguments);
}

ConsecutiveBatch.prototype = {
    __proto__: Batch.prototype,

    _init: function(scope, tasks) {
        Batch.prototype._init.call(this, scope, tasks);
    },

    /** implements the parent function, waiting for the current task to
     * finish before starting the next:
     *
     * 1. the current task is started using {@link #runTask}.
     * 2. if it returns a {@link Hold}, wait until its 'release' signal (signalling
     * that the task is complete) before we can start the next task with
     * {@link #nextTask}.
     * 3. if it didn't return the hold or the hold is finished already, just go
     * straight on to the next task.
     * @override
     */
    process: function() {
       let hold = this.runTask();

       if (hold && hold.isAcquired()) {
           // This task is inhibiting the batch. Wait on it
           // before processing the next one.
           let signalId = hold.connect('release',
                                       Lang.bind(this, function() {
                                           hold.disconnect(signalId);
                                           this.nextTask();
                                       }));
           return;
       } else {
           // This task finished, process the next one
           this.nextTask();
       }
    }
};
Signals.addSignalMethods(ConsecutiveBatch.prototype);
