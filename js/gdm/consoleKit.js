// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines DBus proxy classes allowing gnome-shell to ineract with Console Kit
 * (/org/freedesktop/ConsoleKit/Manager on org.freedesktop.ConsoleKit,
 * interface org.freedesktop.ConsoleKit.Manager).
 *
 * See the [ConsoleKit DBus documentation](http://www.freedesktop.org/software/ConsoleKit/doc/ConsoleKit.html).
 */
const DBus = imports.dbus;

/** Definition for DBus interface 'org.freedesktop.ConsoleKit.Manager'.
 *
 * See the [ConsoleKit DBus documentation](http://www.freedesktop.org/software/ConsoleKit/doc/ConsoleKit.html#Manager).
 * @todo document methods here too...(requires me marking this as a class??)
 * @const
 * @see ConsoleKitManager
 * @type {Object}
 */
const ConsoleKitManagerIface = {
    /** name of the interface
     * @type {string} */
    name: 'org.freedesktop.ConsoleKit.Manager',
    methods: [
            /** Whether the 'restart' option should be available to the user.
             * @name ConsoleKitManagerIface.CanRestart
             * @function
             * @returns {boolean} whether the restart option is available to
             * the user.
             * @todo also document as <methodname>Remote in {@link ConsoleKitManager}.
             */
              { name: 'CanRestart',
                inSignature: '',
                outSignature: 'b' },
            /** Whether the stop (i.e. shutdown) option should be available to
             * the user.
             * @name ConsoleKitManagerIface.CanStop
             * @function
             * @returns {boolean} whether the stop/shutdown option is available
             * to the user.
             * @todo also document as <methodname>Remote in {@link ConsoleKitManager}.
             */
              { name: 'CanStop',
                inSignature: '',
                outSignature: 'b' },
            /** Requests to restart the system.
             * @name ConsoleKitManagerIface.Restart
             * @function
             * @todo also document as <methodname>Remote in {@link ConsoleKitManager}.
             */
              { name: 'Restart',
                inSignature: '',
                outSignature: '' },
            /** Requests to shut down the system.
             * @name ConsoleKitManagerIface.Stop
             * @function
             * @todo also document as <methodname>Remote in {@link ConsoleKitManager}.
             */
              { name: 'Stop',
                inSignature: '',
                outSignature: '' }]
};

/** Creates a new ConsoleKitManager.
 * This exports this instance as a proxy for the object at
 * '/org/freedesktop/ConsoleKit/Manager' on bus 'org.freedesktop.ConsoleKit'
 * (implementing interface 'org.freedesktop.ConsoleKit.Manager').
 *
 * It has methods CanRestart, CanStop, Restart, and Stop (TODO: document
 * them in the interface definition).
 *
 * @classdesc
 * This class allows gnome-shell to interact with the DBus service
 * 'org.freedesktop.ConsoleKit'.
 * See the
 * [ConsoleKit homepage](http://www.freedesktop.org/wiki/Software/ConsoleKit).
 *
 * It is used by the {@link PowerMenu.PowerMenuButton}.
 *
 * It is different to {@link AutomountManager.ConsoleKitManager}
 * which is used by the {@link AutomountManager.AutomountManager} in that that
 * one just exposes the `GetSession` method while this one exposes methods
 * to do with shutting down and restarting.
 * @todo document the <methodname>Remote DBus methods.
 * @class
 * @see ConsoleKitManagerIface
 * @see AutomountManager.ConsoleKitManager
 */
function ConsoleKitManager() {
    this._init();
};

ConsoleKitManager.prototype = {
    _init: function() {
        DBus.system.proxifyObject(this,
                                  'org.freedesktop.ConsoleKit',
                                  '/org/freedesktop/ConsoleKit/Manager');
    }
};
DBus.proxifyPrototype(ConsoleKitManager.prototype, ConsoleKitManagerIface);
