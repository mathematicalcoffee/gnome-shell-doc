// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/*
 * Copyright 2011 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

const AccountsService = imports.gi.AccountsService;
const Clutter = imports.gi.Clutter;
const CtrlAltTab = imports.ui.ctrlAltTab;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Mainloop = imports.mainloop;
const Lang = imports.lang;
const Pango = imports.gi.Pango;
const Signals = imports.signals;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const GdmGreeter = imports.gi.GdmGreeter;

const Batch = imports.gdm.batch;
const DBus = imports.dbus;
const Fprint = imports.gdm.fingerprint;
const Lightbox = imports.ui.lightbox;
const Main = imports.ui.main;
const ModalDialog = imports.ui.modalDialog;
const Tweener = imports.ui.tweener;
/** @+
 * @const
 * @default */
/** Name of the password service (to tell the GdmGreeter.Client). */
const _PASSWORD_SERVICE_NAME = 'gdm-password';
/** Name of the fingerprint service (to tell the GdmGreeter.Client). */
const _FINGERPRINT_SERVICE_NAME = 'gdm-fingerprint';
/** @+
 * @type {number} */
/** When things are faded in or out (e.g. the login dialog), this is the
 * number of seconds over which they should be faded.
 * @see _fadeInActor
 * @see _fadeOutActor
 */
const _FADE_ANIMATION_TIME = 0.16;
/** When UI elements are resized in an animation, this is the number of seconds
 * the animation should take.
 * @see _smoothlyResizeActor */
const _RESIZE_ANIMATION_TIME = 0.25;
/** When UI elements are scrolled (e.g. scrolling around the {@link UserList},
 * this is the number of seconds over which the animation occurs. */
const _SCROLL_ANIMATION_TIME = 2.0;
/** We wait this many seconds before starting the "timed login" animation
 * (if the user has selected timed login) */
const _TIMED_LOGIN_IDLE_THRESHOLD = 5.0;
/** The size of the logo image in the login dialog. */
const _LOGO_ICON_NAME_SIZE = 48;
/** @- */

/** @+
 * @type {string} */
/** The gsettings schema for login information settings (number of allowed
 * authorisation failures, what to show in the login dialog, ...). */
const _LOGIN_SCREEN_SCHEMA = 'org.gnome.login-screen';
/** Gsettings key under {@link _LOGIN_SCREEN_SCHEMA} - whether fingerprint
 * authentication is enabled. */
const _FINGERPRINT_AUTHENTICATION_KEY = 'enable-fingerprint-authentication';

/** Gsettings key under {@link _LOGIN_SCREEN_SCHEMA} - the value of the key
 * is a URI to a logo to show on the login dialog. */
const _LOGO_KEY = 'logo';
/** @- */
/** @- */

let _loginDialog = null;

/** Fades in an actor (if it is not already faded in) by both fading in the
 * opacity from transparent to opaque, and growing the actor from height 0 to
 * its natural height.
 *
 * The animation occurs over time {@Link _FADE_ANIMATION_TIME}.
 * @param {Clutter.Actor} - actor to animate
 * @returns {?Batch.Hold} a hold on the operation that is released upon completion,
 * so that animations can be queued one after the other, or `null` if the
 * effect is already in place.
 * @family gdm.animate
 */
function _fadeInActor(actor) {
    let hold = new Batch.Hold();

    if (actor.opacity == 255 && actor.visible)
        return null;

    actor.show();
    let [minHeight, naturalHeight] = actor.get_preferred_height(-1);

    actor.opacity = 0;
    actor.set_height(0);
    Tweener.addTween(actor,
                     { opacity: 255,
                       height: naturalHeight,
                       time: _FADE_ANIMATION_TIME,
                       transition: 'easeOutQuad',
                       onComplete: function() {
                           actor.set_height(-1);
                           hold.release();
                       },
                       onCompleteScope: this
                     });
    return hold;
}

/** Fades out an actor (if it is not already faded out) by reducing the
 * opacity to transparent and shrinking the actor to height 0 over time
 * {@link _FADE_ANIMATION_TIME}.
 * @family gdm.animate
 * @inheritparams _fadeInActor
 */
function _fadeOutActor(actor) {
    let hold = new Batch.Hold();

    if (!actor.visible) {
        actor.opacity = 0;
        return null;
    }

    if (actor.opacity == 0) {
        actor.hide();
        return null;
    }

    Tweener.addTween(actor,
                     { opacity: 0,
                       height: 0,
                       time: _FADE_ANIMATION_TIME,
                       transition: 'easeOutQuad',
                       onComplete: function() {
                           actor.hide();
                           actor.set_height(-1);
                           hold.release();
                       },
                       onCompleteScope: this
                     });
    return hold;
}

/** Animates resizing an actor to width `width` and height `height` over
 * time {@link _RESIZE_ANIMATION_TIME}.
 * @family gdm.animate
 * @inheritparams _fadeInActor
 * @param {number} width - target width to animate to
 * (or -1 to use `actor`'s natural width).
 * @param {number} width - target height to animate to
 * (or -1 to use `actor`'s natural height).
 */
function _smoothlyResizeActor(actor, width, height) {
    let finalWidth;
    let finalHeight;

    if (width < 0)
        finalWidth = actor.width;
    else
        finalWidth = width;

    if (height < 0)
        finalHeight = actor.height;
    else
        finalHeight = height;

    actor.set_size(actor.width, actor.height);

    if (actor.width == finalWidth && actor.height == finalHeight)
        return null;

    let hold = new Batch.Hold();

    Tweener.addTween(actor,
                     { width: finalWidth,
                       height: finalHeight,
                       time: _RESIZE_ANIMATION_TIME,
                       transition: 'easeOutQuad',
                       onComplete: Lang.bind(this, function() {
                                       hold.release();
                                   })
                     });
    return hold;
}

/** Creates a new UserListItem.
 *
 * ![A {@link UserList} showing two {@link UserListItem}s](pics/LoginDialog.UserList.png)
 *
 * First we store `user` in {@link #user} and connect to its 'changed' signal
 * to listen when any of its details change (like username or avatar).
 *
 * Then we create the top-level actor {@link #actor}, a St.Button with
 * style class 'login-dialog-user-list-item'.
 *
 * In this we place a vertical St.BoxLayout {@link #_verticalBox}, and in
 * this we place:
 *
 * * a horizontal St.BoxLayout that shows the user's avatar in an icon
 * ({@link #_iconBin} and a label with their name ({@link #_nameLabel});
 * * a St.Bin that forms a horizontal grey line under the username, {@link #_focusBin}.
 * It appears when the item is selected. In the image above, user
 * 'mathematical.coffee' has the {@link #_focusBin} visible.
 *
 * @param {AccountsService.User} user - a user
 * @param {?} reason - UNUSED
 * @classdesc
 * ![A {@link UserList} showing two {@link UserListItem}s](pics/LoginDialog.UserList.png)
 *
 * The login dialog shows a list of users that can be logged on to the system.
 *
 * Each item in that list is a {@link UserListItem}.
 *
 * It shows the user's avatar and real name. Navigating keyboard focus to
 * an item causes a grey line to appear under that item.
 *
 * @class
 */
function UserListItem(user, reason) {
    this._init(user, reason);
}

UserListItem.prototype = {
    _init: function(user) {
        /** The user the list item is for.
         * @type {AccountsService.User}
         */
        this.user = user;
        this._userChangedId = this.user.connect('changed',
                                                 Lang.bind(this, this._onUserChanged));

        /** St.BoxLayout in {@link #actor} that we pack our UI elements into.
         * Style class 'login-dialog-user-list-item-vertical-layout'.
         * @type {St.BoxLayout} */
        this._verticalBox = new St.BoxLayout({ style_class: 'login-dialog-user-list-item-vertical-layout',
                                               vertical: true });

        /** Top-level actor, style 'login-dialog-user-list-item'. A button.
         * @type {St.Button} */
        this.actor = new St.Button({ style_class: 'login-dialog-user-list-item',
                                     can_focus: true,
                                     child: this._verticalBox,
                                     reactive: true,
                                     x_align: St.Align.START,
                                     x_fill: true });
        let layout = new St.BoxLayout({ vertical: false });

        this._verticalBox.add(layout,
                              { y_fill: true,
                                x_fill: true,
                                expand: true });

        /** This is a grey horizontal line 2 pixels thick (defined by the
         * style class) that is only visible when the user list item
         * has keyboard focus.
         * Style class 'login-dialog-user-list-item-focus-bin'.
         * @see #showFocusAnimation
         * @type {St.Bin} */
        this._focusBin = new St.Bin({ style_class: 'login-dialog-user-list-item-focus-bin' });
        this._verticalBox.add(this._focusBin,
                              { x_fill: false,
                                x_align: St.Align.MIDDLE,
                                y_fill: false,
                                expand: true });

        /** The user's avatar is placed here.
         * @type {St.Bin} */
        this._iconBin = new St.Bin();
        layout.add(this._iconBin);
        let textLayout = new St.BoxLayout({ style_class: 'login-dialog-user-list-item-text-box',
                                            vertical:    true });
        layout.add(textLayout,
                   { y_fill: false,
                     y_align: St.Align.MIDDLE,
                     expand: true });

        /** Label showing the user's name.
         * Style class 'login-dialog-user-list-item-name'.
         * @type {St.Label} */
        this._nameLabel = new St.Label({ text:        this.user.get_real_name(),
                                         style_class: 'login-dialog-user-list-item-name' });
        textLayout.add(this._nameLabel);

        this._updateIcon();

        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
    },

    /** Callback for the 'changed' signal of {@link #user}.
     *
     * This updates the name and avatar shown on {@link #_nameLabel} and
     * {@link #_iconBin} to be in sync with the user's details.
     * @see #_updateIcon */
    _onUserChanged: function() {
        this._nameLabel.set_text(this.user.get_real_name());
        this._updateIcon();
    },

    /** @inheritdoc EndSessionDialog.EndSessionDialog#_setIconFromFile */
    _setIconFromFile: function(iconFile, styleClass) {
        if (styleClass)
            this._iconBin.set_style_class_name(styleClass);
        this._iconBin.set_style(null);

        this._iconBin.child = null;
        if (iconFile) {
            this._iconBin.show();
            // We use background-image instead of, say, St.TextureCache
            // so the theme writers can add a rounded frame around the image
            // and so theme writers can pick the icon size.
            this._iconBin.set_style('background-image: url("' + iconFile + '");');
        } else {
            this._iconBin.hide();
        }
    },

    /** @inheritdoc EndSessionDialog.EndSessionDialog#_setIconFromName */
    _setIconFromName: function(iconName, styleClass) {
        if (styleClass)
            this._iconBin.set_style_class_name(styleClass);
        this._iconBin.set_style(null);

        if (iconName != null) {
            let icon = new St.Icon();
            icon.set_icon_name(iconName)

            this._iconBin.child = icon;
            this._iconBin.show();
        } else {
            this._iconBin.child = null;
            this._iconBin.hide();
        }
    },

    /** Updates the user's avatar.
     * If the user's avatar is a picture and that picture exists we set it
     * ({@link #_setIconFromFile}). Otherwise, we use the 'avatar-default'
     * icon ({@link #_setIconFromName}).
     */
    _updateIcon: function() {
        let iconFileName = this.user.get_icon_file();
        let gicon = null;

        if (GLib.file_test(iconFileName, GLib.FileTest.EXISTS))
            this._setIconFromFile(iconFileName, 'login-dialog-user-list-item-icon');
        else
            this._setIconFromName('avatar-default', 'login-dialog-user-list-item-icon');
    },

    /** Callback when this item ({@link #actor}) is clicked.
     * Emits 'activate'.
     * @fires .event:activate */
    _onClicked: function() {
        this.emit('activate');
    },

    /** Fades out the user's name.
     *
     * Uses {@link _fadeOutActor}.
     * @see _fadeOutActor */
    fadeOutName: function() {
        return _fadeOutActor(this._nameLabel);
    },

    /** Fades in the user's name.
     *
     * Uses {@link _fadeInActor}.
     * @see _fadeInActor */
    fadeInName: function() {
        return _fadeInActor(this._nameLabel);
    },

    /** Animation that occurs when key focus is given to an item.
     *
     * This animates {@link #_focusBin} (the grey line underneath the
     * username/avatar) growing wider out to its allocation.
     *
     * ![A {@link UserListItem} with its {@link #_focusBin} showing](pics/LoginDialog.UserListItem.png)
     * @param {number} time - seconds over which to do the animation
     * @returns {?Batch.Hold} a hold that is released upon completion of the
     * animation, so that animations can be queued one after the other.
     */
    showFocusAnimation: function(time) {
        let hold = new Batch.Hold();

        let node = this.actor.get_theme_node();
        let padding = node.get_horizontal_padding();

        let box = this._verticalBox.get_allocation_box();

        Tweener.removeTweens(this._focusBin);
        this._focusBin.width = 0;
        Tweener.addTween(this._focusBin,
                         { width: (box.x2 - box.x1 - padding),
                           time: time,
                           transition: 'linear',
                           onComplete: function() {
                               hold.release();
                           },
                           onCompleteScope: this
                         });
        return hold;
    }

};
Signals.addSignalMethods(UserListItem.prototype);
/** Emitted when the {@link UserListItem} is clicked.
 * @event
 * @name activate
 * @memberof UserListItem
 */

/** Creates a new UserList, a vertical scrollable list of {@link UserListItem}s
 * in the login dialog.
 *
 * ![A {@link UserList} showing two {@link UserListItem}s](pics/LoginDialog.UserListItem.png)
 *
 * We create the top-level actor {@link #actor}, a {@link St.ScrollView} and set
 * it to allow vertical scrolling.
 *
 * Into this we place a vertical St.BoxLayout {@link #_box} into which each
 * {@link UserListItem} will be placed.
 *
 * @classdesc
 * ![A {@link UserList} showing two {@link UserListItem}s](pics/LoginDialog.UserListItem.png)
 *
 * The UserList is a vertical scrollable list/menu of {@link UserListItem}s,
 * displayed in the login dialog.
 *
 * Add an item to it with {@link #addItem} and remove it with {@link #removeItem}.
 *
 * Most the public functions are to do with animations - fading and expanding
 * in the list of users, then fading in each user's item ({@link #showItems}),
 * the animation when you click on a particular user that hides all the other
 * user items ({@link #hideAllExcept}), and many others.
 *
 * The animations make user of {@link Batch.ConcurrentBatch}es and
 * {@link Batch.ConsecutiveBatch}es, which provide a framework for chaining
 * together animations simultaneously or sequentually.
 * @class */
function UserList() {
    this._init.apply(this, arguments);
}

UserList.prototype = {
    _init: function() {
        /** Top-level actor - the scrollbox holding all the users. It
         * never scrolls horizontally and the vertical scrollbar will appear
         * automatically if needed.
         * Style class 'login-dialog-user-list-view'.
         * @type {St.ScrollView} */
        this.actor = new St.ScrollView({ style_class: 'login-dialog-user-list-view'});
        this.actor.set_policy(Gtk.PolicyType.NEVER,
                              Gtk.PolicyType.AUTOMATIC);

        /** The vertical box layout that the {@link UserListItem}s themselves
         * are added to. Style class 'login-dialog-user-list'.
         * @type {St.BoxLayout} */
        this._box = new St.BoxLayout({ vertical: true,
                                       style_class: 'login-dialog-user-list' });

        this.actor.add_actor(this._box,
                             { x_fill: true,
                               y_fill: true,
                               x_align: St.Align.START,
                               y_align: St.Align.MIDDLE });

        /** Hash map mapping username to its {@link UserListItem}.
         * @type {Object.<string, UserListItem>} */
        this._items = {};

        this.actor.connect('key-focus-in', Lang.bind(this, this._moveFocusToItems));
    },

    /** Gives keyboard focus to the {@link UserListItem}s (if there are any). */
    _moveFocusToItems: function() {
        let hasItems = Object.keys(this._items).length > 0;

        if (!hasItems)
            return;

        if (global.stage.get_key_focus() != this.actor)
            return;

        this.actor.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
    },

    /** Shows the item for a user by first fading in `item.actor`, and then
     * fading in the name in `item.actor`.
     *
     * These are run as a {@link Batch.ConsecutiveBatch} so that the second
     * animation does not start until the first has finished.
     * @param {UserListItem} item - the item to show.
     * @returns {Batch.Hold} a hold to know when the animations are done/queue
     * up further tasks upon completion of these ones.
     * @see _fadeInActor
     * @see UserItem#fadeInName
     */
    _showItem: function(item) {
        let tasks = [function() {
                         return _fadeInActor(item.actor);
                     },

                     function() {
                         return item.fadeInName();
                     }];

        let batch = new Batch.ConsecutiveBatch(this, tasks);
        return batch.run();
    },

    /** Callback when any of our {@link UserListItem}s is clicked on
     * (i.e. emits its {@link UserListItem.activate} signal).
     *
     * We re-emit this as an 'activate' signal with the item that was
     * activated.
     * @param {UserListItem} activatedItem - {@link UserListItem} that was
     * activated.
     * @fires .activate
     */
    _onItemActivated: function(activatedItem) {
        this.emit('activate', activatedItem);
    },

    /** Causes the user list to give up extra whitespace.
     *
     * Note this may enable the vertical scrollbar.
     *
     * It sets {@link #actor}'s 'expand' property to `false` within its parent.
     * @see #takeOverWhitespace
     */
    giveUpWhitespace: function() {
        let container = this.actor.get_parent();

        container.child_set(this.actor, { expand: false });
    },

    /** Causes the user list to take up as much space as it can.
     *
     * It sets {@link #actor}'s 'expand' property to `true` within its parent.
     * @see #giveUpWhitespace
     */
    takeOverWhitespace: function() {
        let container = this.actor.get_parent();

        container.child_set(this.actor, { expand: true });
    },

    /** This "pins" {@link #_box}'s size to its current size. I think this
     * means that further additions or removals of items from the list
     * will *not* affect its size. Unsure entirely what the effect of this is.
     *
     * Used in the {@link LoginDialog#_onUserListActivated} animation, when
     * a username is clicked on and the user list animates to show just
     * that item.
     */
    pinInPlace: function() {
        this._box.set_size(this._box.width, this._box.height);
    },

    /** Resizes the list to its natural height (i.e. as if it weren't in a
     * scrollbox).
     *
     * It does this by calculating {@link #_box}'s preferred height (without
     * being in a scroll box) and animating the resize.
     *
     * @returns {?Batch.Hold} a hold that is released upon completion of the
     * animation, so that animations can be queued one after the other.
     * @see _smoothlyResizeActor
     */
    shrinkToNaturalHeight: function() {
        let oldWidth = this._box.width;
        let oldHeight = this._box.height;
        this._box.set_size(-1, -1);
        let [minHeight, naturalHeight] = this._box.get_preferred_height(-1);
        this._box.set_size(oldWidth, oldHeight);

        let batch = new Batch.ConsecutiveBatch(this,
                                               [function() {
                                                    return _smoothlyResizeActor(this._box, -1, naturalHeight);
                                                },

                                                function() {
                                                    this._box.set_size(-1, -1);
                                                }
                                               ]);

        return batch.run();
    },

    /** Hides all the user items except for `exception` - for example when you
     * click on a user in the list the other items are all faded out showing
     * just the clicked item.
     *
     * The animation is to fade out the scroll box and *then* all the user list
     * items (except `exception`) concurrently (see {@link Batch.ConsecutiveBatch},
     * {@link Batch.ConcurrentBatch}).
     * @param {?UserListItem} exception - item *not* to hide, or `null` to
     * hide them all.
     * @see _fadeOutActor
     * @inheritparams #shrinkToNaturalHeight */
    hideItemsExcept: function(exception) {
        let tasks = [];

        for (let userName in this._items) {
            let item = this._items[userName];

            item.actor.can_focus = false;
            item._focusBin.width = 0;
            if (item != exception)
                tasks.push(function() {
                    return _fadeOutActor(item.actor);
                });
        }

        let batch = new Batch.ConsecutiveBatch(this,
                                               [function() {
                                                    return _fadeOutActor(this.actor.vscroll);
                                                },

                                                new Batch.ConcurrentBatch(this, tasks)
                                               ]);

        return batch.run();
    },

    /** Hides all the user list items in the list.
     * @inheritparams #hideItemsExcept
     * @see #hideItemsExcept */
    hideItems: function() {
        return this.hideItemsExcept(null);
    },

    /** Calculates the total height the user list would need to display
     * *all* of its {@link UserListItem}s.
     * @returns {number} the total height the user list needs to display
     * all its items. */
    _getExpandedHeight: function() {
        let hiddenActors = [];
        for (let userName in this._items) {
            let item = this._items[userName];
            if (!item.actor.visible) {
                item.actor.show();
                hiddenActors.push(item.actor);
            }
        }

        if (!this._box.visible) {
            this._box.show();
            hiddenActors.push(this._box);
        }

        this._box.set_size(-1, -1);
        let [minHeight, naturalHeight] = this._box.get_preferred_height(-1);

        for (let i = 0; i < hiddenActors.length; i++) {
            let actor = hiddenActors[i];
            actor.hide();
        }

        return naturalHeight;
    },

    /** Shows every {@link UserListItem} by performing the following animation
     * sequence in order ({@link Batch.ConsecutiveBatch}):
     *
     * * set the user list to take up as much space as it needs rather than
     * being constrained by the scrollbox ({@link #takeOverWhitespace});
     * * work out the height required to display every single user item
     * ({@link #_getExpandedHeight}) and expand the list out to that height
     * ({@link _smoothlyResizeActor});
     * * show each {@link UserListItem} concurrently with {@link #_showItem}
     * and a {@link Batch.ConcurrentBatch};
     * * set {@link #actor}'s size to (-1, -1) (TODO: does this put the
     * scroll bar in effect?);
     * * fade in the scrollbar with {@link _fadeInActor}.
     * @inheritparams #hideItems
     */
    showItems: function() {
        let tasks = [];

        for (let userName in this._items) {
            let item = this._items[userName];
            item.actor.can_focus = true;
            tasks.push(function() {
                return this._showItem(item);
            });
        }

        let batch = new Batch.ConsecutiveBatch(this,
                                               [function() {
                                                    this.takeOverWhitespace();
                                                },

                                                function() {
                                                    let fullHeight = this._getExpandedHeight();
                                                    return _smoothlyResizeActor(this._box, -1, fullHeight);
                                                },

                                                new Batch.ConcurrentBatch(this, tasks),

                                                function() {
                                                    this.actor.set_size(-1, -1);
                                                },

                                                function() {
                                                    return _fadeInActor(this.actor.vscroll);
                                                }]);
        return batch.run();
    },

    /** Scrolls so that the user list is centred on `item`
     * by tweening the scroll box over {@link _SCROLL_ANIMATION_TIME}.
     * @see {@link #jumpToItem} which does the same but without animation.
     * @param {UserListItem} item - user item in the user list.
     */
    scrollToItem: function(item) {
        let box = item.actor.get_allocation_box();

        let adjustment = this.actor.get_vscroll_bar().get_adjustment();

        let value = (box.y1 + adjustment.step_increment / 2.0) - (adjustment.page_size / 2.0);
        Tweener.removeTweens(adjustment);
        Tweener.addTween (adjustment,
                          { value: value,
                            time: _SCROLL_ANIMATION_TIME,
                            transition: 'linear' });
    },

    /** Centres the user list on `item`.
     * @see {@link #jumpToItem} which does the same but with animation.
     * @inheritparams #scrollToItem */
    jumpToItem: function(item) {
        let box = item.actor.get_allocation_box();

        let adjustment = this.actor.get_vscroll_bar().get_adjustment();

        let value = (box.y1 + adjustment.step_increment / 2.0) - (adjustment.page_size / 2.0);

        adjustment.set_value(value);
    },

    /** Gets the {@link UserListItem} for user `userName`.
     * @param {string} userName - the user name to look up.
     * @returns {?UserListItem} the item for user `userName`, or `null` if none
     * found.
     * @see #_items */
    getItemFromUserName: function(userName) {
        let item = this._items[userName];

        if (!item)
            return null;

        return item;
    },

    /** Creates a {@link UserListItem} for `user` and adds it to the user list.
     *
     * If the user isn't loaded, or is a system account, or doesn't have a
     * username, we do not add it.
     *
     * Otherwise we create a {@link UserListItem} for it and add it to
     * our hash map {@link #_items}.
     *
     * We connect to its ['activate' signal]{@link UserListItem.activate}
     * to call {@link #_onItemActivated}.
     *
     * When key focus is given to the item, we scroll to it
     * ({@link #scrollToItem}) and animate the item with
     * [item.showFocusAnimation]{@link UserListItem#showFocusAnimation}.
     *
     * Finally we emit 'item-added'.
     * @fires .item-added
     * @param {@AccountsService.User} user - user to add.
     */
    addUser: function(user) {
        if (!user.is_loaded)
            return;

        if (user.is_system_account())
            return;

        let userName = user.get_user_name();

        if (!userName)
            return;

        this.removeUser(user);

        let item = new UserListItem(user);
        this._box.add(item.actor, { x_fill: true });

        this._items[userName] = item;

        item.connect('activate',
                     Lang.bind(this, this._onItemActivated));

        // Try to keep the focused item front-and-center
        item.actor.connect('key-focus-in',
                           Lang.bind(this,
                                     function() {
                                         this.scrollToItem(item);
                                         item.showFocusAnimation(0);
                                     }));

        this._moveFocusToItems();

        this.emit('item-added', item);
    },

    /** Removes the item for `user` from the list, destroying it too. */
    removeUser: function(user) {
        if (!user.is_loaded)
            return;

        let userName = user.get_user_name();

        if (!userName)
            return;

        let item = this._items[userName];

        if (!item)
            return;

        item.actor.destroy();
        delete this._items[userName];
    }
};
Signals.addSignalMethods(UserList.prototype);
/** Emitted when one of the items in a {@link UserList} is clicked on.
 * @param {UserList} o - the the user list that emitted the signal
 * @param {UserListItem} activatedItem - the {@link UserListItem} that
 * was clicked o/activated.
 * @name activate
 * @event
 * @memberof UserList
 */
/** Emitted when a {@link UserListItem} is added to the {@link UserList}.
 * @param {UserList} o - the the user list that emitted the signal.
 * @param {UserListItem} item - the {@link UserListItem} that was added.
 * @name item-added
 * @event
 * @memberof UserList
 */

/** Creates a new SessionListItem.
 *
 * ![Many {@link SessionListItem}s, GNOME one with {@link #setShowDot} `true`](pics/LoginDialog.SessionList.Expanded.png)
 *
 * We store the session ID `id` in {@link #id}.
 *
 * Then we create the top-level actor {@link #actor}, a clickable St.Button.
 *
 * The child of this button is a horizontal St.BoxLayout {@link #_box}.
 * Into this we place a St.Label showing the session name `name`
 * (e.g. "Classic GNOME with Compiz"), as well as some space for a filled circle
 * to the left of the label ({@link #_dot}).
 *
 * @param {string} id - the ID of the session ('icewm', 'gnome', 'compiz-gnome', ...)
 * @param {string} name - the name of the session ('Icewm', 'GNOME',
 * 'Classic GNOME with Compiz')
 *
 * @classdesc
 * ![{@link SessionList} expanded, showing its {@link SessionListItem}s](pics/LoginDialog.SessionList.Expanded.png)
 *
 * When a user is clicked on from the user list in the login dialog,
 * another dialog shows allowing you to enter your password and select which
 * session (gnome-shell, unity, ...) to log in to.
 *
 * This is one item of that session list. A dot is shown next to the item
 * that is currently selected. This dot is just about the same as the one
 * on a {@link PopupMenu.PopupBaseMenuItem} (see
 * {@link PopupMenu.PopupBaseMenuItem#setShowDot}). In the picture above,
 * the "GNOME" session is selected.
 * @class
 */
function SessionListItem(id, name) {
    this._init(id, name);
}

SessionListItem.prototype = {
    _init: function(id, name) {
        /** The ID of the session - e.g. 'gnome', 'compiz-gnome', 'icewm'.
         * @type {string} */
        this.id = id;

        /** The top-level actor for the session list item, a clickable button.
         * Style class 'login-dialog-session-list-item'.
         * @type {St.Button} */
        this.actor = new St.Button({ style_class: 'login-dialog-session-list-item',
                                     can_focus: true,
                                     reactive: true,
                                     x_fill: true,
                                     x_align: St.Align.START });

        /** The St.BoxLayout we pack the various components for this item
         * into (the dot and the session label), placed into {@link #actor}.
         *
         * Style class 'login-dialog-session-list-item-box'.
         * @type {St.BoxLayout} */
        this._box = new St.BoxLayout({ style_class: 'login-dialog-session-list-item-box' });

        this.actor.add_actor(this._box,
                             { expand: true,
                               x_fill: true,
                               y_fill: true });
        this.actor.connect('clicked', Lang.bind(this, this._onClicked));

        /** The dot shown to the left of the session label when this session
         * is selected. Drawn on in much the way that {@link PopupMenu.PopupBaseMenuItem#_dot}
         * is drawn.
         * @type {St.DrawingArea} */
        this._dot = new St.DrawingArea({ style_class: 'login-dialog-session-list-item-dot' });
        this._dot.connect('repaint', Lang.bind(this, this._onRepaintDot));
        this._box.add_actor(this._dot);
        this.setShowDot(false);

        let label = new St.Label({ style_class: 'login-dialog-session-list-item-label',
                                   text: name });

        this._box.add_actor(label,
                            { expand: true,
                              x_fill: true,
                              y_fill: true });
    },

    /** Shows or hides the dot preceding the text on this item.
     *
     * ![Example of a {@link PopupMenuItem} with `setShowDot(true)` (second from the top)](pics/PopupBaseMenuItem.styles.png)
     * @param {boolean} show - whether to show the dot or not.
     */
    setShowDot: function(show) {
        if (show)
            this._dot.opacity = 255;
        else
            this._dot.opacity = 0;
    },

    /** Connected to the 'repaint' signal of {@link #_dot} - draws the dot.
     *
     * It's just a filled sphere drawn in the colour specified by {@link #_dot}'s
     * style class (probably white).
     * @param {St.DrawingArea} area - the drawing area to be repainted
     * ({@link #_dot}) */
    _onRepaintDot: function(area) {
        let cr = area.get_context();
        let [width, height] = area.get_surface_size();
        let color = area.get_theme_node().get_foreground_color();

        cr.setSourceRGBA (color.red / 255,
                          color.green / 255,
                          color.blue / 255,
                          color.alpha / 255);
        cr.arc(width / 2, height / 2, width / 3, 0, 2 * Math.PI);
        cr.fill();
    },

    /** Called when the item is clicked on.
     * All it does is emit the 'activate' signal.
     * @fires .activate
     */
    _onClicked: function() {
        this.emit('activate');
    }
};
Signals.addSignalMethods(SessionListItem.prototype);
/** Emitted when a session list item is clicked on.
 * @name activate
 * @memberof SessionListItem
 * @event
 */

/** Creates a new SessionList.
 *
 * This is the expandable menu of {@link SessionListItem}s in the login
 * dialog.
 *
 * ![{@link LoginDialog} showing an expanded {@link SessionList}](pics/LoginDialog.SessionList.png)
 * 
 * ![{@link SessionList}, unexpanded](pics/LoginDialog.SessionList.Collapsed.png)
 *
 * We create the top-level actor {@link #actor}, a St.Bin in which to display
 * everything.
 *
 * In this we place a vertical St.BoxLayout {@link #_box} which shows:
 *
 * * a St.Button {@link #_button} showing the "Session..." heading and the
 *   expand/collapse triangle ▸ to the left {@link #_triangle}.
 * * below that, a {@link St.ScrollView} containing a vertical {@link St.BoxLayout}
 *   which will show all the {@link UserListItem}s ({@link #_scrollView},
 *   {@link #_itemList}). This is the outlined rounded rectangle in the picture
 *   above.
 *
 * We set {@link #isOpen} to false to indicate the session list is not expanded,
 * and hide the session list.
 *
 * Then we call {@link #_populate} to add all the {@link SessionListItem}s to
 * the list.
 *
 * @classdesc
 * ![{@link LoginDialog} showing an expanded {@link SessionList}](pics/LoginDialog.SessionList.png)
 * 
 * ![{@link SessionList}, unexpanded](pics/LoginDialog.SessionList.Collapsed.png)
 *
 * ![{@link SessionList} expanded, showing its {@link SessionListItem}s](pics/LoginDialog.SessionList.Expanded.png)
 *
 * This shows the collapsible list of available sessions in the login
 * dialog (if the user has multiple sessions).
 *
 * The list is seen on the password prompt of the login dialog when the user has
 * multiple available sessions (like GNOME, GNOME-classic, IceWM, ...).
 *
 * It has a title item "Session..." that, when clicked, expands to show
 * the list of {@link SessionListItem}s.
 *
 * Use {@link #open} to expand the list and {@link #close} to collapse it.
 * @class
 */
function SessionList() {
    this._init();
}

SessionList.prototype = {
    _init: function() {
        /** Top-level actor.
         * @type {St.Bin} */
        this.actor = new St.Bin();

        /** Child of {@link #actor}, into which we place the "Session..."
         * heading item and the list of session items.
         *
         * Vertical box layout, style class 'login-dialog-session-list'.
         * @type {St.BoxLayout} */
        this._box = new St.BoxLayout({ style_class: 'login-dialog-session-list',
                                       vertical: true});
        this.actor.child = this._box;

        /** The "▸ Session..." button - contains the triangle {@link #_triangle}
         * and the "Session..." label.
         *
         * Style class 'login-dialog-session-list-button'.
         * @type {St.Button}
         */
        this._button = new St.Button({ style_class: 'login-dialog-session-list-button',
                                       can_focus: true,
                                       x_fill: true,
                                       y_fill: true });
        let box = new St.BoxLayout();
        this._button.add_actor(box,
                               { x_fill: true,
                                 y_fill: true,
                                 expand: true });

        /** The triangle left of the "Session..." heading. When the session
         * list is collapse it displays '▸' (left pointing triangle) and when
         * it's expanded it displays '▾' (downwards pointing triangle).
         *
         * Style class 'login-dialog-session-list-triangle'.
         * @type {St.Label} */
        this._triangle = new St.Label({ style_class: 'login-dialog-session-list-triangle',
                                        text: '\u25B8' });
        box.add_actor(this._triangle);

        let label = new St.Label({ style_class: 'login-dialog-session-list-label',
                                   text: _("Session...") });
        box.add_actor(label,
                      { x_fill: true,
                        y_fill: true,
                        expand: true });

        this._button.connect('clicked',
                             Lang.bind(this, this._onClicked));
        this._box.add_actor(this._button,
                            { x_fill: true,
                              y_fill: true,
                              expand: true });
        /** Vertical scroll box into which the list of aviailable sessions are
         * placed. It never scrolls horizontally and scrolls vertically on-demand.
         *
         * Style class 'login-dialog-session-list-scroll-view'.
         * @type {St.ScrollView}
         */
        this._scrollView = new St.ScrollView({ style_class: 'login-dialog-session-list-scroll-view'});
        this._scrollView.set_policy(Gtk.PolicyType.NEVER,
                                    Gtk.PolicyType.AUTOMATIC);
        this._box.add_actor(this._scrollView,
                            { x_fill: true,
                              y_fill: true,
                              expand: true });
        /** Vertical St.BoxLayout into which the {@link SessionListItem}s are
         * placed.
         *
         * Style class 'login-dialog-session-item-list'.
         * @type {St.BoxLayout} */
        this._itemList = new St.BoxLayout({ style_class: 'login-dialog-session-item-list',
                                            vertical: true });
        this._scrollView.add_actor(this._itemList,
                                   { x_fill: true,
                                     y_fill: true,
                                     expand: true });
        this._scrollView.hide();
        /** Whether the session list is expanded or not.
         * @type {boolean} */
        this.isOpen = false;
        this._populate();
    },

    /** Expands the session list to show the list of available sessions underneath
     * the "Sessions..." heading.
     *
     * ![{@link SessionList} opened/expanded](pics/LoginDialog.SessionList.Expanded.png)
     *
     * This changes the triangle label next to "Session..." to be a downwards-pointing
     * triangle, shows the list of sessions {@link #_scrollView} and adds
     * pseudo style class 'open' to the "Session..." title {@link #_button}.
     *
     * @see #close
     */
    open: function() {
        if (this.isOpen)
            return;

        this._button.add_style_pseudo_class('open');
        this._scrollView.show();
        this._triangle.set_text('\u25BE');

        this.isOpen = true;
    },

    /** Collapses the session list, hiding the list of available sessions and
     * just showing the heading "Sessions...".
     *
     * ![{@link SessionList} collapsed/closed](pics/LoginDialog.SessionList.Collapsed.png)
     *
     * This changes the triangle label to be a right-pointing triangle
     * instead of a downwards-pointing one, hides the session list
     * ({@link #_scrollView}) and adjusts the pseudo style class of {@link #_button}.
     *
     * @see #open
     */
    close: function() {
        if (!this.isOpen)
            return;

        this._button.remove_style_pseudo_class('open');
        this._scrollView.hide();
        this._triangle.set_text('\u25B8');

        this.isOpen = false;
    },

    /** Callback when the "Sessions..." heading {@link #_button} is clicked.
     *
     * This toggles the list to be expanded or unexpanded.
     *
     * @see #open
     * @see #close
     */
    _onClicked: function() {
        if (!this.isOpen)
            this.open();
        else
            this.close();
    },

    /** Sets which of the sessions is the selected one.
     *
     * This shows a dot to the left of the selected session and emits a signal
     * to alert others.
     *
     * @see SessionListItem#setShowDot
     * @fires .session-activated
     * @inheritparams LoginDialog#_onDefaultSessionChanged
     */
    setActiveSession: function(sessionId) {
         if (sessionId == this._activeSessionId)
             return;

         if (this._activeSessionId)
             this._items[this._activeSessionId].setShowDot(false);

         this._items[sessionId].setShowDot(true);
         this._activeSessionId = sessionId;

         this.emit('session-activated', this._activeSessionId);
    },

    /** Populates the scrollview with a {@link SessionListItem} for each available
     * session.
     *
     * We use `GdmGreeter.get_session_ids()` to list all the available sessions
     * (like 'gnome', 'icewm', 'fluxbox').
     *
     * For each of these, we retrieve the session name ("GNOME", "Icewm", "Fluxbox")
     * and create a {@link SessionListItem} with the id and name.
     *
     * We add this item to our BoxLayout {@link #_itemList} (which is in our
     * scrollbox).
     *
     * We connect to the item's ['activate']{@link SessionListItem.activate}
     * signal and call {@link #setActiveSession} upon receiving it.
     * @see #setActiveSession
     * @listens SessionListItem.activate
     */
    _populate: function() {
        this._itemList.destroy_children();
        this._activeSessionId = null;
        this._items = {};

        let ids = GdmGreeter.get_session_ids();
        ids.sort();

        if (ids.length <= 1) {
            this._box.hide();
            this._button.hide();
        } else {
            this._button.show();
            this._box.show();
        }

        for (let i = 0; i < ids.length; i++) {
            let [sessionName, sessionDescription] = GdmGreeter.get_session_name_and_description(ids[i]);

            let item = new SessionListItem(ids[i], sessionName);
            this._itemList.add_actor(item.actor,
                              { x_align: St.Align.START,
                                y_align: St.Align.START,
                                x_fill: true,
                                y_fill: true });
            this._items[ids[i]] = item;

            if (!this._activeSessionId)
                this.setActiveSession(ids[i]);

            item.connect('activate',
                         Lang.bind(this, function() {
                             this.setActiveSession(item.id);
                         }));
        }
    }
};
Signals.addSignalMethods(SessionList.prototype);
/** Emitted when the active session is set (e.g. the user selects one of
 * the available sessions from the session list, or through calling
 * {@link #setActiveSession}.
 * @param {SessionList} sessionList - the instance that emitted the signal.
 * @param {string} sessionId - the ID of the session (e.g. 'gnome')
 * @event
 * @name session-activated
 * @memberof SessionList
 */

/** Makes a new LoginDialog.
 *
 * First we call the [parent constructor]{@link ModalDialog.ModalDialog}
 * with parameters `shellReactive` to `true` (this allows interaction with any
 * other UI elements like the top panel while the dialog is showing), and
 * style class 'login-dialog'.
 *
 * Then we store the default {@link AccountsService.UserManager} in
 * {@link #_userManager} (used for retrieving information about the users
 * on the system).
 *
 * We create a new GdmGreeter.Client, stored in {@link #_greeterClient}.
 * This is what handles all the login/authorisation logic - we listen to
 * its signals which tell us what we need to display on the login dialog
 * (e.g. ask the user for their password, or username).
 *
 * We create a [fingerprint manager]{@link Fprint.FprintManager}
 * {@link #_fprintManager} and indicate to {@link #_greeterClient} that we
 * are interested in collecting fingerprints, if it is not disabled in
 * gsettings AND if we actually have a fingerprint device.
 *
 * We then construct the graphical parts of the dialog.
 *
 * Note that the dialog has a number of "modes":
 *
 * * the initial dialog where the user list is shown
 * * the dialog that appears upon clicking "Not listed?" or a user from the
 *   list - possibly shows an icon and a label asking the user for some
 *   information ("Password:", "Username:") plus a text box for typing in
 *   the response, with a "Log in" and "Cancel" button.
 *
 * We listen to various signals from {@link #_greeterClient} to tell us which
 * elements to display. For example, the 'reset' signal tells us to go back
 * to the initial dialog showing the user list. The 'info-query' or
 * 'secret-info-query' signals tell us to switch to the mode where we show the
 * text box asking the user for some input.
 *
 * ![The {@link LoginDialog} in its "user list" mode](pics/LoginDialog.LoginDialog.png)
 *
 * The initial dialog that shows the user list has the following components:
 *
 * * {@link _#logoBox}: displays the logo of your distribution (the logo image
 * can be set in gsettings - see {@link _LOGIN_SCREEN_SCHEMA} and {@link _LOGO_KEY}).
 * In the image above, this is the fedora logo.
 * * {@link #_titleLabel}: this is the "Sign In" label.
 * * {@link #_userList}: the list of users that can be logged in, a {@link UserList}.
 * * {@link #_notListedButton}: the "Not listed?" label (clicking on this
 * allows the user to enter in another user name to log in with).
 *
 * ![The {@link LoginDialog} in its "prompt" mode](pics/LoginDialog.NotListed.png)
 * ![The {@link LoginDialog} in its "prompt" mode](pics/LoginDialog.Password.png)
 *
 * The "prompt" dialog (asking the user for information) has the following
 * components:
 *
 * * a label to display text to the user (like "Password:" or "Username:"),
 *   {@link #_promptLabel};
 * * a text box for the user to type the information (password/username)
 *   into, {@link #_promptEntry};
 * * a text box showing a message below the password box telling the user they
 *   can do fingerprint authentication if they wish ("(or swipe finger)"),
 *   only shown if it's supported {@link #_promptFingerprintMessage};
 * * the list of sessions available to log into, shown below the
 *   entry box ({@link #_sessionList}, a {@link SessionList});
 * * a vertical St.BoxLayout {@link #_promptBox} to hold it all together
 *   ({@link #_promptBox}).
 *
 * Finally we connect to {@link #_userManager}'s 'notify::is-loaded' signal to
 * populate the user list ({@link #_loadUserList}).
 *
 * @classdesc
 *
 * This is the dialog that appears on the GDM greeter screen (login screen),
 * if the user has GDM as their login manager.
 *
 * The dialog interacts with GDM's DBus API (?) `imports.gi.GdmGreeter.Client`
 * to be told what information is needed from the user and to pass the user's
 * inputs to be authenticated.
 *
 * The dialog has two main modes, the "user list" dialog and the "prompt" dialog.
 *
 * ![The {@link LoginDialog} in its "user list" mode](pics/LoginDialog.LoginDialog.png)
 *
 * The "user list" dialog displays the distributor's logo on the top (if
 * specified by the distribution), a list of users ({@link UserList}) to log
 * in as, and a "Not listed?" button allowing another username to be entered.
 *
 * ![The {@link LoginDialog} in its "prompt" mode](pics/LoginDialog.Password.png)
 *
 * The "prompt" dialog is shown when the user is prompted for either their
 * password or username, and has a text box for user entry along with
 * "Sign in" and "Cancel" buttons. It also shows a list of available sessions
 * that can be logged into (e.g. GNOME, Classic GNOME, Fluxbox, ... - see
 * {@link SessionList}), if there are more than one.
 *
 * @class
 * @extends ModalDialog.ModalDialog
 */
function LoginDialog() {
    if (_loginDialog == null) {
        this._init();
        _loginDialog = this;
    }

    return _loginDialog;
}

LoginDialog.prototype = {
    __proto__: ModalDialog.ModalDialog.prototype,

    _init: function() {
        ModalDialog.ModalDialog.prototype._init.call(this, { shellReactive: true,
                                                             styleClass: 'login-dialog' });
        this.connect('destroy',
                     Lang.bind(this, this._onDestroy));
        this.connect('opened',
                     Lang.bind(this, this._onOpened));

        /** The default user manager - we use it to get a list of users on
         * the system to display in our {@link UserList}.
         * @type {AccountsService.UserManager} */
        this._userManager = AccountsService.UserManager.get_default();
        /** Provided by the GDM API for us to initiate the login, as well as
         * it telling us when authentication fails, or when information is
         * needed from the user.
         * @type {GdmGreeter.Client} */ 
        this._greeterClient = new GdmGreeter.Client();

        this._greeterClient.open_connection();

        this._greeterClient.call_start_conversation(_PASSWORD_SERVICE_NAME);

        this._greeterClient.connect('reset',
                                    Lang.bind(this, this._onReset));
        this._greeterClient.connect('default-session-changed',
                                    Lang.bind(this, this._onDefaultSessionChanged));
        this._greeterClient.connect('info',
                                    Lang.bind(this, this._onInfo));
        this._greeterClient.connect('problem',
                                    Lang.bind(this, this._onProblem));
        this._greeterClient.connect('info-query',
                                    Lang.bind(this, this._onInfoQuery));
        this._greeterClient.connect('secret-info-query',
                                    Lang.bind(this, this._onSecretInfoQuery));
        this._greeterClient.connect('session-opened',
                                    Lang.bind(this, this._onSessionOpened));
        this._greeterClient.connect('timed-login-requested',
                                    Lang.bind(this, this._onTimedLoginRequested));
        this._greeterClient.connect('authentication-failed',
                                    Lang.bind(this, this._onAuthenticationFailed));
        this._greeterClient.connect('conversation-stopped',
                                    Lang.bind(this, this._onConversationStopped));

        /** Gsettings schema for the login screen (what logo image to use,
         * whether to allow fingerprint authorisation, ...)
         * @see _LOGIN_SCREEN_SCHEMA
         * @type {Gio.Settings} */
        this._settings = new Gio.Settings({ schema: _LOGIN_SCREEN_SCHEMA });

        /** Fingerprint manager, used for logging in with a fingerprin.
         * @type {Fprint.FprintManager} */
        this._fprintManager = new Fprint.FprintManager();
        this._startFingerprintConversationIfNeeded();
        this._settings.connect('changed::' + _LOGO_KEY,
                               Lang.bind(this, this._updateLogo));

        /** Displays the distributor logo (if set; the URI to this image
         * can be specified in gsettings, 'org.gnome.login-screen logo').
         *
         * Style class 'login-dialog-logo-box'.
         * @type {St.Bin} */
        this._logoBox = new St.Bin({ style_class: 'login-dialog-logo-box' });
        this.contentLayout.add(this._logoBox);
        this._updateLogo();

        /** The "Sign In" title label on the login dialog.
         *
         * Style class 'login-dialog-title'.
         * @type {St.Label} */
        this._titleLabel = new St.Label({ style_class: 'login-dialog-title',
                                          text: C_("title", "Sign In") });

        this.contentLayout.add(this._titleLabel,
                              { y_fill: false,
                                y_align: St.Align.START });

        let mainContentBox = new St.BoxLayout({ vertical: false });
        this.contentLayout.add(mainContentBox,
                               { expand: true,
                                 x_fill: true,
                                 y_fill: false });

        /** The list of users that can be logged in, shown in the login dialog.
         * @type {UserList} */
        this._userList = new UserList();
        mainContentBox.add(this._userList.actor,
                           { expand: true,
                             x_fill: true,
                             y_fill: true });

        this.setInitialKeyFocus(this._userList.actor);

        /** Holds the widgets for the "prompt" part of the dialog
         * (the label e.g. "Password:" and the text box)>
         *
         * Style class 'login-dialog-prompt-layout'.
         * @type {St.BoxLayout} */
        this._promptBox = new St.BoxLayout({ style_class: 'login-dialog-prompt-layout',
                                             vertical: true });
        mainContentBox.add(this._promptBox,
                           { expand: true,
                             x_fill: true,
                             y_fill: true,
                             x_align: St.Align.START });
        /** Displays a prompt to the user, like "Username:" or "Password:".
         *
         * Style class 'login-dialog-prompt-label'.
         * @type {St.Label} */
        this._promptLabel = new St.Label({ style_class: 'login-dialog-prompt-label' });

        this._mainContentBox = mainContentBox;

        this._promptBox.add(this._promptLabel,
                            { expand: true,
                              x_fill: true,
                              y_fill: true,
                              x_align: St.Align.START });
        /** A text box for the user to type requested information (like
         * username and password) into.
         *
         * Style class 'login-dialog-prompt-entry'.
         * @type {St.Entry} */
        this._promptEntry = new St.Entry({ style_class: 'login-dialog-prompt-entry',
                                           can_focus: true });
        this._promptBox.add(this._promptEntry,
                            { expand: true,
                              x_fill: true,
                              y_fill: false,
                              x_align: St.Align.START });
        // translators: this message is shown below the password entry field
        // to indicate the user can swipe their finger instead
        /** A label shown below the password box to indicate that the user
         * can swipe their finger instead of providing a password, text
         * "(or swipe finger)". Only shown if fingerprint reading is enabled.
         *
         * Style class 'login-dialog-prompt-fingerprint-message'.
         * @type {St.BoxLayout} */
        this._promptFingerprintMessage = new St.Label({ text: _("(or swipe finger)"),
                                                        style_class: 'login-dialog-prompt-fingerprint-message' });
        this._promptFingerprintMessage.hide();
        this._promptBox.add(this._promptFingerprintMessage);

        /** A collapsible menu showing a list of sessions (GNOME, IceWM, ...)
         * that the user can log in to.
         * @type {SessionList} */
        this._sessionList = new SessionList();
        this._sessionList.connect('session-activated',
                                  Lang.bind(this, function(list, sessionId) {
                                                this._greeterClient.call_select_session (sessionId);
                                            }));

        this._promptBox.add(this._sessionList.actor,
                            { expand: true,
                              x_fill: false,
                              y_fill: true,
                              x_align: St.Align.START });
        this._promptBox.hide();

        let notListedLabel = new St.Label({ text: _("Not listed?"),
                                            style_class: 'login-dialog-not-listed-label' });
        /** The "Not listed?" label in the login dialog.
         *
         * Its label has style class 'login-dialog-not-listed-label', and this
         * button has style class 'login-dialog-not-listed-button'.
         * @see #-onNotListedClicked
         * @type {St.Button} */
        this._notListedButton = new St.Button({ style_class: 'login-dialog-not-listed-button',
                                                can_focus: true,
                                                child: notListedLabel,
                                                reactive: true,
                                                x_align: St.Align.START,
                                                x_fill: true });

        this._notListedButton.connect('clicked', Lang.bind(this, this._onNotListedClicked));

        this.contentLayout.add(this._notListedButton,
                               { expand: false,
                                 x_align: St.Align.START,
                                 x_fill: true });

        if (!this._userManager.is_loaded)
            this._userManagerLoadedId = this._userManager.connect('notify::is-loaded',
                                                                  Lang.bind(this, function() {
                                                                      if (this._userManager.is_loaded) {
                                                                          this._loadUserList();
                                                                          this._userManager.disconnect(this._userManagerLoadedId);
                                                                          this._userManagerLoadedId = 0;
                                                                      }
                                                                  }));
        else
            this._loadUserList();

        this._userList.connect('activate',
                               Lang.bind(this, function(userList, item) {
                                   this._onUserListActivated(item);
                               }));

   },

    /** Tells {@link #_greeterClient} we are interested in collecting
     * fingerprints (for login).
     *
     * This will only be done if:
     *
     * * fingerprint authentication is enabled (gsettings key
     *   {@link _FINGERPRINT_AUTHENTICATION_KEY}); AND
     * * there is a fingerprint reader device.
     */
   _startFingerprintConversationIfNeeded: function() {
        this._haveFingerprintReader = false;

        if (!this._settings.get_boolean(_FINGERPRINT_AUTHENTICATION_KEY))
            return;

        this._fprintManager.GetDefaultDeviceRemote(DBus.CALL_FLAG_START, Lang.bind(this,
            function(device, error) {
                if (!error && device)
                    this._haveFingerprintReader = true;

                if (this._haveFingerprintReader)
                    this._greeterClient.call_start_conversation(_FINGERPRINT_SERVICE_NAME);
            }));
    },

   /** Updates the logo shown on the login dialog (if any).
    *
    * We load the URI to the logo from gsettings key {@link _LOGO_KEY},
    * and if present we load the image and put it into the login dialog
    * in {@link #_logoBox}.
    */
    _updateLogo: function() {
        this._logoBox.child = null;
        let path = this._settings.get_string(_LOGO_KEY);

        if (path) {
            let file = Gio.file_new_for_path(path);
            let uri = file.get_uri();

            let textureCache = St.TextureCache.get_default();
            this._logoBox.child = textureCache.load_uri_async(uri, -1, _LOGO_ICON_NAME_SIZE);
        }

    },

    /** Callback for {@link #_greeterClient}'s 'reset' signal.
     *
     * This restarts the 'gdm-password' process ("conversation") with
     * {@link #_greeterClient}.
     *
     * It also calls {@link #_startFingerprintConversationIfNeeded} to initiate
     * fingerprint collection (if applicable).
     *
     * Then a number of consecutive tasks are performed:
     *
     * 1. Hide the prompt (asking for username or password) so that we can
     *    show the user list ({@link #_hidePrompt});
     * 2. Concurrently fade in the title label ("Sign in"), "Not Listed?"
     *    button, and logo ({@link #_fadeInTitleLabel},
     *    {@link #_fadeInNotListedButton}, {@link #_fadeInLogo});
     * 3. Close the list of available sessions {@link #_sessionList},
     *    show the user list, animate showing all the items in the user
     *    list ({@link UserList#showItems});
     * 4. grab key focus to the user list.
     *
     * @inheritparams #_onInfoQuery
     * @see Batch.ConsecutiveBatch
     */
    _onReset: function(client, serviceName) {
        this._greeterClient.call_start_conversation(_PASSWORD_SERVICE_NAME);
        this._startFingerprintConversationIfNeeded();

        let tasks = [this._hidePrompt,

                     new Batch.ConcurrentBatch(this, [this._fadeInTitleLabel,
                                                      this._fadeInNotListedButton,
                                                      this._fadeInLogo]),

                     function() {
                         this._sessionList.close();
                         this._promptFingerprintMessage.hide();
                         this._userList.actor.show();
                         this._userList.actor.opacity = 255;
                         return this._userList.showItems();
                     },

                     function() {
                         this._userList.actor.reactive = true;
                         this._userList.actor.grab_key_focus();
                     }];

        this._user = null;

        let batch = new Batch.ConsecutiveBatch(this, tasks);
        batch.run();
    },

    /** Callback for {@link #_greeterClient}'s 'default-session-changed'
     * signal, when the default session (e.g. GNOME-shell or Unity) changes.
     *
     * This updates our session list {@link #_sessionList} to select `sessionId`
     * as the active session.
     * @inheritparams #_onInfoQuery
     * @param {string} sessionId - ID of the session, e.g. 'gnome'.
     * @see SessionList#setActiveSession
     */
    _onDefaultSessionChanged: function(client, sessionId) {
        this._sessionList.setActiveSession(sessionId);
    },

    /** Callback for {@link #_greeterClient}'s 'info' signal.
     *
     * This is emitted when information needs to be communicated
     * to the user.
     *
     * We use {@link Main.notifyError} to pop up a notification to the user.
     * @inheritparams #_onInfoQuery
     * @param {string} info - the message to show the user.
     * @see Main.notifyError
     * @see #_onInfo
     */
    _onInfo: function(client, serviceName, info) {
        // We don't display fingerprint messages, because they
        // have words like UPEK in them. Instead we use the messages
        // as a cue to display our own message.
        if (serviceName == _FINGERPRINT_SERVICE_NAME &&
            this._haveFingerprintReader &&
            (!this._promptFingerprintMessage.visible ||
             this._promptFingerprintMessage.opacity != 255)) {

            _fadeInActor(this._promptFingerprintMessage);
            return;
        }

        if (serviceName != _PASSWORD_SERVICE_NAME)
            return;
        Main.notifyError(info);
    },

    /** Callback for {@link #_greeterClient}'s 'problem' signal.
     *
     * This is emitted when a problem occurs that needs to be communicated
     * to the user.
     *
     * We use {@link Main.notifyError} to pop up a notification to the user.
     * @inheritparams #_onInfoQuery
     * @param {string} problem - the string describing the problem, e.g.
     * ("Authorisation failure.").
     * @see Main.notifyError
     * @see #_onInfo
     */
    _onProblem: function(client, serviceName, problem) {
        // we don't want to show auth failed messages to
        // users who haven't enrolled their fingerprint.
        if (serviceName != _PASSWORD_SERVICE_NAME)
            return;
        Main.notifyError(problem);
    },

    /** Called when the user clicks the "Cancel" button in the prompt
     * dialog (where they've been asked for username or password).
     *
     * This calls `this._greeterClient.call_cancel` (which will emit the
     * appropriate signals, e.g. 'reset', so that we show the user list
     * dialog again).
     */
    _onCancel: function(client) {
        this._greeterClient.call_cancel();
    },

    /** Fades in the information prompt (the "Password:"/"Username:" label
     * plus the text box).
     *
     * This is done by running the following tasks concurrently:
     *
     * * {@link _fadeInActor} the prompt label {@link #_promptLabel};
     * * {@link _fadeInActor} the prompt text box {@link #_promptEntry};
     * * allocate space for the prompt fingerprint message
     *   {@link #_promptFingerprintMessage} (but do not make it visible yet);
     * * {@link _fadeInActor} the prompt box holding the label/text box/etc
     *   {@link #_promptBox};
     * * {@link #_fadeInActor} the list of available sessions (like
     *   'Unity' or 'gnome-shell'), if relevant ({@link #_sessionList});
     * * grab key focus to the prompt text box.
     *    
     * @inheritparams #_onUserListActivated
     * @see Batch.ConcurrentBatch
     * @family LoginDialog.getInfo
     */
    _fadeInPrompt: function() {
        let tasks = [function() {
                         return _fadeInActor(this._promptLabel);
                     },

                     function() {
                         return _fadeInActor(this._promptEntry);
                     },

                     function() {
                         // Show it with 0 opacity so we preallocate space for it
                         // in the event we need to fade in the message
                         this._promptFingerprintMessage.opacity = 0;
                         this._promptFingerprintMessage.show();
                     },

                     function() {
                         return _fadeInActor(this._promptBox);
                     },

                     function() {
                         if (this._user && this._user.is_logged_in())
                             return null;

                         return _fadeInActor(this._sessionList.actor);
                     },

                     function() {
                         this._promptEntry.grab_key_focus();
                     }];

        this._sessionList.actor.hide();
        let batch = new Batch.ConcurrentBatch(this, tasks);
        return batch.run();
    },

    /** Shows the information prompt (the "Password:"/"Username:" label plus
     * the text box).
     *
     * The following actions are all performed concurrently:
     *
     * * fade in the prompt label and text box with {@link #_fadeInPrompt};
     * * create the dialog buttons, "Cancel" and "Sign In" ({@link #setButtons});
     * * add a {@link Batch.Hold} that is *only* released when the user
     *    activates the text box (presses "Enter") or clicks on the "Sign In"
     *    button.
     *
     * The resulting hold for these tasks (from `batch.run()`) is only released
     * when all three tasks are complete; it is used in {@link #_askQuestion}
     * to communicate the user's response to {@link #_greeterClient} only once
     * the user has submitted their response.
     * @family LoginDialog.getInfo
     * @inheritparams #_onUserListActivated
     * @see Batch.ConcurrentBatch
     * @see Batch.Hold
     */
    _showPrompt: function() {
        let hold = new Batch.Hold();

        let buttons = [{ action: Lang.bind(this, this._onCancel),
                         label: _("Cancel"),
                         key: Clutter.Escape },
                       { action: Lang.bind(this, function() {
                                     hold.release();
                                 }),
                         label: C_("button", "Sign In") }];

        this._promptEntryActivateCallbackId = this._promptEntry.clutter_text.connect('activate',
                                                                                     Lang.bind(this, function() {
                                                                                         hold.release();
                                                                                     }));
        hold.connect('release', Lang.bind(this, function() {
                         this._promptEntry.clutter_text.disconnect(this._promptEntryActivateCallbackId);
                         this._promptEntryActivateCallbackId = null;
                     }));

        let tasks = [function() {
                         return this._fadeInPrompt();
                     },

                     function() {
                         this.setButtons(buttons);
                     },

                     hold];

        let batch = new Batch.ConcurrentBatch(this, tasks);

        return batch.run();
    },

    /** Hides the information prompt (The "Password:" label plus the entry
     * box).
     *
     * We clear the buttons from the dialog ({@link #setButtons}) and then
     * start a set of consecutive tasks:
     *
     * 1. fade out the prompt label/entry box {@link #_promptBox} ({@link _fadeOutActor});
     * 2. hide the fingerprint message (if present), set the entry box to
     *    be non-reactive and clear its text.
     *
     * @inheritparams #_onUserListActivated
     * @family LoginDialog.getInfo
     */
    _hidePrompt: function() {
        if (this._promptEntryActivateCallbackId) {
            this._promptEntry.clutter_text.disconnect(this._promptEntryActivateCallbackId);
            this._promptEntryActivateCallbackId = null;
        }

        this.setButtons([]);

        let tasks = [function() {
                         return _fadeOutActor(this._promptBox);
                     },

                     function() {
                         this._promptFingerprintMessage.hide();
                         this._promptEntry.reactive = true;
                         this._promptEntry.remove_style_pseudo_class('insensitive');
                         this._promptEntry.set_text('');
                     }];

        let batch = new Batch.ConsecutiveBatch(this, tasks);

        return batch.run();
    },

    /** Updates our dialog to ask the user a question, like "User:" or
     * "Password:".
     *
     * We set {@link #_promptLabel}'s text to `question`.
     *
     * Then we perform a number of consecutive tasks:
     * 
     * 1. show the prompt label and text box ({@link #_showPrompt}), where
     *    that hold is not released until the user has pressed "Enter" in the
     *    text box or pressed the "Sign in" button);
     * 2. get the text from the text box and communicate the response to the
     *    {@link #_greeterClient}.
     *
     * @inheritparams #_onUserListActivated
     * @inheritparams #_onInfoQuery
     * @family LoginDialog.getInfo
     */
    _askQuestion: function(serviceName, question) {
        this._promptLabel.set_text(question);

        let tasks = [this._showPrompt,

                     function() {
                         let _text = this._promptEntry.get_text();
                         this._promptEntry.reactive = false;
                         this._promptEntry.add_style_pseudo_class('insensitive');
                         this._greeterClient.call_answer_query(serviceName, _text);
                     }];

        let batch = new Batch.ConsecutiveBatch(this, tasks);
        return batch.run();
    },

    /** Callback on {@link #_greeterClient}'s 'info-query' signal,
     * when we need some sort of information from the user (like a username
     * when you click on the "Not listed?" ).
     *
     * We clear [our entry box's]{@link #_promptEntry} text and set it to
     * show typed characters (as opposed to the password black dot ●).
     *
     * Then we call {@link #_askQuestion} with `question` to display
     * the appropriate question on our dialog (like "Username:").
     * @inheritparams #_onSecretInfoQuery
     * @param {string} question - question to ask the user (like "Username:").
     * @family LoginDialog.getInfo
     */
    _onInfoQuery: function(client, serviceName, question) {
        // We only expect questions to come from the main auth service
        if (serviceName != _PASSWORD_SERVICE_NAME)
            return;

        this._promptEntry.set_text('');
        this._promptEntry.clutter_text.set_password_char('');
        this._askQuestion(serviceName, question);
    },

    /** Callback for {@link #_greeterClient}'s 'secret-info-query' signal,
     * when we need some secret information from the user (like a password).
     *
     * We clear [our entry box's]{@link #_promptEntry} text and set it
     * to display a black dot ● instead of characters.
     *
     * Then we call {@link #_askQuestion} with `secretQuestion` to display
     * the appropriate question on our dialog (like "Password:").
     * @inheritparams #_onConversationStopped
     * @param {string} secretQuestion - the question to be asked to the user
     * (e.g. "Password:").
     * @family LoginDialog.getInfo
     */
    _onSecretInfoQuery: function(client, serviceName, secretQuestion) {
        // We only expect secret requests to come from the main auth service
        if (serviceName != _PASSWORD_SERVICE_NAME)
            return;

        this._promptEntry.set_text('');
        this._promptEntry.clutter_text.set_password_char('\u25cf');
        this._askQuestion(serviceName, secretQuestion);
    },

    /** Callback for {@link #_greeterClient}'s 'session-opened' signal.
     *
     * We call `this._greeterClient.call_start_session_when_ready`.
     *
     * I assume {@link #_greeterClient} will then emit the appropriate signals
     * telling us what to show on the login dialog.
     * @inheritparams #_onConversationStopped
     */
    _onSessionOpened: function(client, serviceName) {
        this._greeterClient.call_start_session_when_ready(serviceName, true);
    },

    /** Returns a {@link Batch.Hold} that is released when the item for
     * `userName` appears in the user list.
     * @inheritparams #_startTimedLogin
     * @see {@link UserList.item-added}
     */
    _waitForItemForUser: function(userName) {
        let item = this._userList.getItemFromUserName(userName);

        if (item)
          return null;

        let hold = new Batch.Hold();
        let signalId = this._userList.connect('item-added',
                                              Lang.bind(this, function() {
                                                  let item = this._userList.getItemFromUserName(userName);

                                                  if (item)
                                                      hold.release();
                                              }));

        hold.connect('release', Lang.bind(this, function() {
                         this._userList.disconnect(signalId);
                     }));

        return hold;
    },

    /** Starts the timed login animation - simply focuses the timed-login-user's
     * {@link UserListItem} in the user list.
     * @see UserListItem#showFocusAnimation
     * @inheritparams #_startTimedLogin
     * @family timedLogin */
    _showTimedLoginAnimation: function() {
        this._timedLoginItem.actor.grab_key_focus();
        return this._timedLoginItem.showFocusAnimation(this._timedLoginAnimationTime);
    },

    /** Returns a {@link Batch.Hold} that is released upon the passing of
     * {@link _TIMED_LOGIN_IDLE_THRESHOLD} seconds.
     *
     * This is used purely to defer the timed login animation (scrolling
     * to the user to be logged in and focusing their item in the user list)
     * for a bit.
     *
     * We skip this step if the timed login delay is shorter than
     * {@link _TIMED_LOGIN_IDLE_THRESHOLD}.
     *
     * @inheritparams #_startTimedLogin
     * @family timedLogin */
    _blockTimedLoginUntilIdle: function() {
        // This blocks timed login from starting until a few
        // seconds after the user stops interacting with the
        // login screen.
        //
        // We skip this step if the timed login delay is very
        // short.
        if ((this._timedLoginDelay - _TIMED_LOGIN_IDLE_THRESHOLD) <= 0)
          return null;

        let hold = new Batch.Hold();

        this._timedLoginIdleTimeOutId = Mainloop.timeout_add_seconds(_TIMED_LOGIN_IDLE_THRESHOLD,
                                                                     function() {
                                                                         this._timedLoginAnimationTime -= _TIMED_LOGIN_IDLE_THRESHOLD;
                                                                         hold.release();
                                                                     });
        return hold;
    },

    /** Starts timed login.
     *
     * This is executed as a {@link Batch.ConsecutiveBatch}, i.e. a list of
     * tasks such that one task should only occur when the previous task has
     * finished.
     *
     * The sequence of tasks is like so:
     *
     * 1. we wait until the {@link UserListItem} for `userName` has appeared
     *    in the user list ({@link #_waitForItemForUser}).
     * 2. we jump to `userName`'s item in the user list ({@link #_timedLoginItem})
     *    if the user list hasn't finished loading yet;
     * 3. we wait until the user is idle ({@link #_blockTimedLoginUntilIdle})
     *    (before starting all the animations);
     * 4. we scroll such that `userName`'s item is centred
     *    ({@link UserList#scrollToItem}_;
     * 5. we show the timed login animation, which is focusing on `userName`'s
     *    {@link UserListItem}, making the animation run as long as `delay`
     *    minus the time we waited in step 3;
     *    ({@link UserListItem#showFocusAnimation}, {@link #_showTimedLoginAnimation}),
     * 6. We perform the auto login with {@link #_greeterClient}.
     *
     * @inheritparams #_onTimedLoginRequested
     * @inheritparams #_onUserListActivated
     * @param {number} second - seconds before `userName` is automatically logged in
     * @family timedLogin */
    _startTimedLogin: function(userName, delay) {
        /** If timed login has been specified, this is the {@link UserListItem}
         * of the user that is to be logged in upon timeout.
         * @type {UserListItem} */
        this._timedLoginItem = null;
        /** If timed login has been specified, this is the number of seconds
         * after which the login is to occur.
         * @type {number} */
        this._timedLoginDelay = delay;
        this._timedLoginAnimationTime = delay;

        let tasks = [function() {
                         return this._waitForItemForUser(userName);
                     },

                     function() {
                         this._timedLoginItem = this._userList.getItemFromUserName(userName);
                     },

                     function() {
                         // If we're just starting out, start on the right
                         // item.
                         if (!this.is_loaded) {
                             this._userList.jumpToItem(this._timedLoginItem);
                             this._timedLoginItem.showFocusAnimation(0);
                         }
                     },

                     this._blockTimedLoginUntilIdle,

                     function() {
                         this._userList.scrollToItem(this._timedLoginItem);
                     },

                     this._showTimedLoginAnimation,

                     function() {
                         this._timedLoginBatch = null;
                         this._greeterClient.call_begin_auto_login(userName);
                     }];

        /** If timed login is currently in progress this is the set of tasks that will
         * cause it to happen (wait for the timed-login-user's name to appear
         * in the user list, jump to it, wait until the user is idle,
         * scroll to the timed-login-user's name in the list, and animate
         * it over the specified delay before logging in).
         * @type {?Batch.ConsecutiveBatch} */
        this._timedLoginBatch = new Batch.ConsecutiveBatch(this, tasks);

        return this._timedLoginBatch.run();
    },

    /** Resets timed login (i.e. starts the timeout again).
     *
     * Cancels our timed login task and restarts it with {@link #_startTimedLogin}.
     * @family timedLogin */
    _resetTimedLogin: function() {
        if (this._timedLoginBatch) {
            this._timedLoginBatch.cancel();
            this._timedLoginBatch = null;
        }

        let userName = this._timedLoginItem.user.get_user_name();

        if (userName)
            this._startTimedLogin(userName, this._timedLoginDelay);
    },

    /** Callback when a time login is requested on {@link #_greeterClient}
     * (i.e. you have `seconds` seconds to select another user, or else
     * the default one will be logged in).
     *
     * We call {@link #_startTimedLogin}, which jumps to the auto-login user
     * from the user list, waits until the user is idle for a bit and then starts
     * the login.
     *
     * We also listen to any key or button press or release events, which will
     * defer the timed login.
     *
     * @inheritparams #_onConversationStopped
     * @param {string} userName - a user's username.
     * @param {number} second - seconds before `userName` is automatically logged in
     * @family timedLogin
     */
    _onTimedLoginRequested: function(client, userName, seconds) {
        this._startTimedLogin(userName, seconds);

        global.stage.connect('captured-event',
                             Lang.bind(this, function(actor, event) {
                                if (this._timedLoginDelay == undefined)
                                    return false;

                                if (event.type() == Clutter.EventType.KEY_PRESS ||
                                    event.type() == Clutter.EventType.BUTTON_PRESS) {
                                    if (this._timedLoginBatch) {
                                        this._timedLoginBatch.cancel();
                                        this._timedLoginBatch = null;
                                    }
                                } else if (event.type() == Clutter.EventType.KEY_RELEASE ||
                                           event.type() == Clutter.EventType.BUTTON_RELEASE) {
                                    this._resetTimedLogin();
                                }

                                return false;
                             }));
    },

    /** Callback when authentication fails.
     * We call `call_cancel` on {@link #_greeterClient}.
     *
     * {@link #_greeterClent} will then emit the appropriate signals
     * (like 'reset') for us to re-show the user list to have another go at
     * logging in.
     */
    _onAuthenticationFailed: function(client) {
        this._greeterClient.call_cancel();
    },

    /** Callback for {@link #_greeterClient}'s 'conversation-stopped' signal.
     *
     * If the stopped conversation was the gdm-password one, we call
     * `this._greeterClient.call_cancel()` ({@link #_greeterClient} will
     * then emit the appropriate signals, like 'reset', to indicate to us
     * which dialog it wants us to show).
     *
     * If it was a fingerprint request that was stopped, we fade out the
     * finger print message {@link #_promptFingerprintMessage}.
     *
     * @param {GdmGreeter.Client} client - the client that emiited the signal
     * ({@link #_greeterClient})
     * @param {string} serviceName - the name of the service (e.g.
     * 'gdm-password' or 'gdm-fingerprint')
     */
    _onConversationStopped: function(client, serviceName) {
        // if the password service fails, then cancel everything.
        // But if, e.g., fingerprint fails, still give
        // password authentication a chance to succeed
        if (serviceName == _PASSWORD_SERVICE_NAME) {
            this._greeterClient.call_cancel();
        } else if (serviceName == _FINGERPRINT_SERVICE_NAME) {
            _fadeOutActor(this._promptFingerprintMessage);
        }
    },

    /** Callback when the user clicks on the "Not Listed?" button.
     *
     * This animates out the user list and animates in the dialog that
     * lets the user enter in their username, and signals to the
     * {@link #_greeterClient} that we wish to log in (it will then
     * send signals indicating that it wants a username and password,
     * see {@link #_onInfoQuery} and {@link #_onSecretInfoQuery}).
     *
     * A {@link Batch.ConsecutiveBatch} is used to perform the tasks
     * sequentially:
     *
     * 1. hide all the users in the user list ({@link UserList#hideItems}),
     * 2. shrink the user list ({@link #UserList#giveUpWhitespace}),
     * 3. hide the user list entirely,
     * 4. Fade out the title label ("Sign in"), "Not Listed?" button, and
     *    logo ({@link #_fadeOutTitleLabel}, {@link #_fadeOutNotListedButton},
     *    {@link #_fadeOutLogo}) concurrently.
     * 5. Signal to the {@link #_greeterClient} that we wish to start a login.
     *    It will then emit various signals to ask for the username and
     *    password, see {@link #_onInfoQuery} and {@link #_onSecretInfoQuery}.
     * @inheritparams #_onUserListActivated
     */
    _onNotListedClicked: function(user) {
        let tasks = [function() {
                         return this._userList.hideItems();
                     },

                     function() {
                         return this._userList.giveUpWhitespace();
                     },

                     function() {
                         this._userList.actor.hide();
                     },

                     new Batch.ConcurrentBatch(this, [this._fadeOutTitleLabel,
                                                      this._fadeOutNotListedButton,
                                                      this._fadeOutLogo]),

                     function() {
                         this._greeterClient.call_begin_verification(_PASSWORD_SERVICE_NAME);
                     }];

        let batch = new Batch.ConsecutiveBatch(this, tasks);
        batch.run();
    },

    /** Fades in the logo.
     * @inheritdoc _fadeInActor */
    _fadeInLogo: function() {
        return _fadeInActor(this._logoBox);
    },

    /** Fades out the logo.
     * @inheritdoc _fadeOutActor */
    _fadeOutLogo: function() {
        return _fadeOutActor(this._logoBox);
    },

    /** Fades in the title label ("Sign in", {@link #_titleLabel}).
     * @inheritdoc _fadeInActor */
    _fadeInTitleLabel: function() {
        return _fadeInActor(this._titleLabel);
    },

    /** Fades out the title label ("Sign in", {@link #_titleLabel}).
     * @inheritdoc _fadeOutActor */
    _fadeOutTitleLabel: function() {
        return _fadeOutActor(this._titleLabel);
    },

    /** Fades in the "Not Listed?" button ({@link #_notListedButton}).
     * @inheritdoc _fadeInActor */
    _fadeInNotListedButton: function() {
        return _fadeInActor(this._notListedButton);
    },

    /** Fades out the "Not Listed?" button ({@link #_notListedButton}).
     * @inheritdoc _fadeOutActor */
    _fadeOutNotListedButton: function() {
        return _fadeOutActor(this._notListedButton);
    },

    /** Callback for {@link #_userList}'s ['activated']{@link UserList.event:activated}
     * signal, emitted whenever you click on a user from the user list.
     *
     * This does the animation that reduces the login dialog from showing the
     * user list to showing the "prompt" dialog (where just the user's avatar
     * and a password/username prompt textbox are shown).
     *
     * The animations are performed with a {@link Batch.ConsecutiveBatch} (i.e.
     * one after the other):
     *
     * 1. the user list is pinned in place (i.e. the size is temporarily held
     *    fixed, {@link UserList#pinInPlace}),
     * 2. all items are hidden except the clicked one ({@link UserList#hideItemsExcept}),
     * 3. the user list is shrunk ({@link UserList#giveUpWhitespace}),
     * 4. the user's name is hidden from `activatedItem` showing just the
     *    avatar ({@link UserListItem#fadOutName}),
     * 5. the title label, "Not listed?" button and logo are all faded out
     *    concurrently ({@link #_fadeOutTitleLabel}, {@link #_fadeOutNotListedButton},
     *    {@link #_fadeOutLogo}) with a {@link Batch.ConcurrentBatch},
     * 6. the user list is shrinked to its natural height, disabling the
     *    scroll bar ({@link UserList#shrinkToNaturalHeight}),
     * 7. we start the authentication with {@link #_greeterClient}.
     *
     * @inheritparams UserList#_onItemActivated
     * @returns {Batch.Hold} a hold to know when the animations are done/queue
     * up further tasks upon completion of these ones.
     */
    _onUserListActivated: function(activatedItem) {
        let tasks = [function() {
                         this._userList.actor.reactive = false;
                         return this._userList.pinInPlace();
                     },

                     function() {
                         return this._userList.hideItemsExcept(activatedItem);
                     },

                     function() {
                         return this._userList.giveUpWhitespace();
                     },

                     function() {
                         return activatedItem.fadeOutName();
                     },

                     new Batch.ConcurrentBatch(this, [this._fadeOutTitleLabel,
                                                      this._fadeOutNotListedButton,
                                                      this._fadeOutLogo]),

                     function() {
                         return this._userList.shrinkToNaturalHeight();
                     },

                     function() {
                         let userName = activatedItem.user.get_user_name();
                         this._greeterClient.call_begin_verification_for_user(_PASSWORD_SERVICE_NAME,
                                                                              userName);

                         if (this._haveFingerprintReader)
                             this._greeterClient.call_begin_verification_for_user(_FINGERPRINT_SERVICE_NAME, userName);
                     }];

        this._user = activatedItem.user;

        let batch = new Batch.ConsecutiveBatch(this, tasks);
        batch.run();
    },

    /** Callback for our ['destroy' signal]{@link .event:destroy}.
     *
     * We disconnect the signals we were listening to (e.g. listening to
     * {@link #_userManager} for it to finish loading).
     */
    _onDestroy: function() {
        if (this._userManagerLoadedId) {
            this._userManager.disconnect(this._userManagerLoadedId);
            this._userManagerLoadedId = 0;
        }
    },

    /** Loads the user list by adding all the existing users to the list
     * of users in the login dialog.
     *
     * ![A {@link UserList} showing two users](pics/LoginDialog.UserList.png)
     *
     * 1. Get all the users from {@link #_userManager}.
     * 2. Create a {@link UserListItem} for each user with {@link UserList#addUser}.
     * 3. Connect to {@link #_userManager}'s 'user-added' and 'user-removed'
     * signals to add or remove the user from the user list appropriately.
     * 4. Emit the {@link .event:loaded} signal to let everyone know we're
     * finished (and set {@link #is_loaded} to true).
     *
     * @fires .event:loaded
     * @see UserList
     * @see UserListItem
     * @see UserList#addUser
     * @see UserList#removeUser
     */
    _loadUserList: function() {
        let users = this._userManager.list_users();

        for (let i = 0; i < users.length; i++) {
            this._userList.addUser(users[i]);
        }

        this._userManager.connect('user-added',
                                  Lang.bind(this, function(userManager, user) {
                                      this._userList.addUser(user);
                                  }));

        this._userManager.connect('user-removed',
                                  Lang.bind(this, function(userManager, user) {
                                      this._userList.removeUser(user);
                                  }));

        // emitted in idle so caller doesn't have to explicitly check if
        // it's loaded immediately after construction
        // (since there's no way the caller could be listening for
        // 'loaded' yet)
        Mainloop.idle_add(Lang.bind(this, function() {
            this.emit('loaded');
            /** Whether the {@link UserList} has been loaded in the login dialog
             * yet.
             * @type {boolean} */
            this.is_loaded = true;
        }));
    },

    /** Callback to our ['opened']{@link .event:opened} signal, emitted when
     * the dialog is opened.
     *
     * This adds the dialog to the {@link Main.ctrlAltTabManager} (with text
     * "Login Window" and icon 'dialog-password').
     * @see CtrlAltTab.CtrlAltTabManager#addGroup
     * @see #open
     */
    _onOpened: function() {
        Main.ctrlAltTabManager.addGroup(this._mainContentBox,
                                        _("Login Window"),
                                        'dialog-password',
                                        { sortGroup: CtrlAltTab.SortGroup.MIDDLE });

    },

    /** Extends the parent method to closes the login dialog.
     *
     * Calls the [parent method]{@link ModalDialog.ModalDialog#close} and
     * removes us from the ctrl alt tab manager.
     * @see CtrlAltTab.CtrlAltTabManager#removeGroup
     * @override */
    close: function() {
        ModalDialog.ModalDialog.prototype.close.call(this);

        Main.ctrlAltTabManager.removeGroup(this._group);
    }
};
/** Emitted when the {@link UserList} for the login dialog has been loaded.
 * @memberof LoginDialog
 * @event
 * @name loaded
 */
