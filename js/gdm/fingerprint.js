// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Makes a DBus proxy for fprintd (fingerprint reading devices):
 * interface 'net.reactivated.Fprint.Manager', bus 'net.reactivated.Fprint',
 * object '/net/reactivated/Fprint/Manager'.
 */

const DBus = imports.dbus;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Signals = imports.signals;

/** Definition for DBus interface 'net.reactivated.Fprint.Manager'.
 * Methods are documented in the {@link FprintManager} class.
 * @todo document methods here too...(requires me marking this as a class??)
 * @const
 * @see FprintManager
 */
const FprintManagerIface = {
    /** name of the interface
     * @type {string} */
    name: 'net.reactivated.Fprint.Manager',
    methods: [
        /** DBus method to get the default fingerprint device.
         * 
         * Note - the DBus method itself requires no input parameters but
         * as a DBus method you use it like so:
         *
         *     fprintManager.GetDefaultDeviceRemote(DBus.CALL_FLAG_START, cb);
         *
         * Where `cb` is a function with two parameters, the first being
         * the returned device and the second being a dbus error (if any).
         *
         * See {@link LoginDialog.LoginDialog#_startFingerprintConversationIfNeeded}
         * for example of use.
         *
         * @todo how to document the DBus method? Document according to its
         * signature in the Iface documentation and then according to
         * how you would call it (with a flag and callback) in the
         * proxy class documentation??
         * @name FprintManager#GetDefaultDeviceRemote
         * @alias FprintManager#GetDefaultDevice
         * @function
         * @returns {?} the default DBus device (signature 'o')
         */
        { name: 'GetDefaultDevice',
                inSignature: '',
                outSignature: 'o' }
    ]
};

/** Creates a new FprintManager.
 * This exports this instance as a proxy for the object at
 * '/net/reactivated/Fprint/Manager' on bus 'net.reactivated.Fprint'
 * (implementing interface 'net.reactivated.Fprint.Manager').
 *
 * In particular, you can call ({@link FprintManager#GetDefaultDeviceRemote})
 * on the class.
 *
 * @classdesc
 * This class allows gnome-shell to interact with the DBus service
 * 'net.reactivated.Fprint' (from fprintd) to determine if the user has a
 * fingerprint reader.
 *
 * `fprintd` provides fingerprint scanning functionality over DBus: see the
 * [fprintd homepage](http://www.freedesktop.org/wiki/Software/fprint/fprintd)
 * for further details.
 * 
 * See [this page](http://hadess.fedorapeople.org/fprintd/docs/Manager.html) for
 * a description of the DBus interface ('net.reactivated.Fprint.Manager').
 *
 * It is used by the {@link LoginDialog.LoginDialog}.
 * @class
 * @see LoginDialog.LoginDialog
 */
function FprintManager() {
    this._init();
};

FprintManager.prototype = {
    _init: function() {
        DBus.system.proxifyObject(this,
                                  'net.reactivated.Fprint',
                                  '/net/reactivated/Fprint/Manager');
    }
};
DBus.proxifyPrototype(FprintManager.prototype, FprintManagerIface);
