// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the 'power' button on the login screen.
 * @registerlink UPowerGLib.Client http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/UPowerGlib.Client.html
 */
/*
 * Copyright 2011 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

const Lang = imports.lang;
const UPowerGlib = imports.gi.UPowerGlib;

const ConsoleKit = imports.gdm.consoleKit;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

/** Creates a new PowerMenuButton.
 *
 * We create a {@link ConsoleKit.ConsoleKitManager} instance to determine
 * which options (shutdown, suspend, restart) to display on the menu.
 *
 * Then we create the menu.
 *
 * Also creates a {@link UPowerGLib.Client} instance for the suspend item.
 * @classdesc
 * ![{@link PowerMenuButton}](pics/GDM.PowerMenu.png)
 *
 * This is the button on the login screen with the 'power' symbol and dropdown
 * menu allowing the user to shutdown/restart/suspend the computer.
 * @class
 * @extends PanelMenu.SystemStatusButton
 * @todo screenshot
 */
function PowerMenuButton() {
    this._init();
}

PowerMenuButton.prototype = {
    __proto__: PanelMenu.SystemStatusButton.prototype,

    _init: function() {
        PanelMenu.SystemStatusButton.prototype._init.call(this, 'system-shutdown', null);
        this._consoleKitManager = new ConsoleKit.ConsoleKitManager();
        this._upClient = new UPowerGlib.Client();

        this._createSubMenu();

        this._upClient.connect('notify::can-suspend',
                               Lang.bind(this, this._updateHaveSuspend));
        this._updateHaveSuspend();

        // ConsoleKit doesn't send notifications when shutdown/reboot
        // are disabled, so we update the menu item each time the menu opens
        this.menu.connect('open-state-changed', Lang.bind(this,
            function(menu, open) {
                if (open) {
                    this._updateHaveShutdown();
                    this._updateHaveRestart();
                }
            }));
        this._updateHaveShutdown();
        this._updateHaveRestart();
    },

    /** Hides the button if there is no suspend, shutdown, or restart option
     * available to the user. */
    _updateVisibility: function() {
        if (!this._haveSuspend && !this._haveShutdown && !this._haveRestart)
            this.actor.hide();
        else
            this.actor.show();
    },

    /** Hides or shows the 'power off' item depending on whether the user has
     * that option available to them.
     * @see ConsoleKit.ConsoleKitManagerIface#CanStop */
    _updateHaveShutdown: function() {
        this._consoleKitManager.CanStopRemote(Lang.bind(this,
            function(result, error) {
                if (!error)
                    this._haveShutdown = result;
                else
                    this._haveShutdown = false;

                if (this._haveShutdown) {
                    this._powerOffItem.actor.show();
                } else {
                    this._powerOffItem.actor.hide();
                }

                this._updateVisibility();
            }));
    },

    /** Hides or shows the 'restart' item depending on whether the user
     * has that option available to them.
     * @see ConsoleKit.ConsoleKitManagerIface#CanRestart */
    _updateHaveRestart: function() {
        this._consoleKitManager.CanRestartRemote(Lang.bind(this,
            function(result, error) {
                if (!error)
                    this._haveRestart = result;
                else
                    this._haveRestart = false;

                if (this._haveRestart) {
                    this._restartItem.actor.show();
                } else {
                    this._restartItem.actor.hide();
                }

                this._updateVisibility();
            }));
    },

    /** Hides or shows the 'suspend' item depending on whether the user
     * has that option available to them.
     * Uses a {@link UPowerGLib.Client}.
     * to determine this. */
    _updateHaveSuspend: function() {
        this._haveSuspend = this._upClient.get_can_suspend();

        if (this._haveSuspend)
            this._suspendItem.actor.show();
        else
            this._suspendItem.actor.hide();

        this._updateVisibility();
    },

    /** Creates the dropdown menu of the power button.
     * It adds a Suspend, Restart, and Power Off item to the menu (the
     * `_update*` functions then determine whether these items should be
     * visible).
     */
    _createSubMenu: function() {
        let item;

        item = new PopupMenu.PopupMenuItem(_("Suspend"));
        item.connect('activate', Lang.bind(this, this._onActivateSuspend));
        this.menu.addMenuItem(item);
        this._suspendItem = item;

        item = new PopupMenu.PopupMenuItem(_("Restart"));
        item.connect('activate', Lang.bind(this, this._onActivateRestart));
        this.menu.addMenuItem(item);
        this._restartItem = item;

        item = new PopupMenu.PopupMenuItem(_("Power Off"));
        item.connect('activate', Lang.bind(this, this._onActivatePowerOff));
        this.menu.addMenuItem(item);
        this._powerOffItem = item;
    },

    /** callback when the user selects 'suspend'. Suspends the computer
     * (using the `UPowerGLib.Client`) */
    _onActivateSuspend: function() {
        if (this._haveSuspend)
            this._upClient.suspend_sync(null);
    },

    /** callback when the user selects 'restart'. Restarts the computer using
     * {@link ConsoleKit.ConsoleKitManager#Restart}. */
    _onActivateRestart: function() {
        if (this._haveRestart)
            this._consoleKitManager.RestartRemote();
    },

    /** callback when the user selects 'power off'. shuts down the computer using
     * {@link ConsoleKit.ConsoleKitManager#PowerOff}. */
    _onActivatePowerOff: function() {
        if (this._haveShutdown)
            this._consoleKitManager.StopRemote();
    }
};
