// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file handles the automagic detection of external media (USB sticks,
 *  ...).
 */

const Lang = imports.lang;
const DBus = imports.dbus;
const Mainloop = imports.mainloop;
const Gio = imports.gi.Gio;
const Params = imports.misc.params;

const Main = imports.ui.main;
const ShellMountOperation = imports.ui.shellMountOperation;
const ScreenSaver = imports.misc.screenSaver;

// GSettings keys
/** @+
 * @const
 * @default */
/** Gsettings schema for media handling.
 * @type {string} */
const SETTINGS_SCHEMA = 'org.gnome.desktop.media-handling';
/** Gsettings key under {@link SETTINGS_SCHEMA} as to whether automount is supported.
 * @type {string} */
const SETTING_ENABLE_AUTOMOUNT = 'automount';

/** Time in seconds before autorun is disabled on a device.
 * @type {number} */
const AUTORUN_EXPIRE_TIMEOUT_SECS = 10;
/** @- */

/** Partial definition of interface 'org.freedesktop.ConsoleKit.Session'.
 * See [the online documentation for the Session interface](http://www.freedesktop.org/software/ConsoleKit/doc/ConsoleKit.html#Session).
 * @const
 * @see ConsoleKitSessionProxy
 */
const ConsoleKitSessionIface = {
    name: 'org.freedesktop.ConsoleKit.Session',
    /** Whether the session is active.
     * @function
     * @returns {boolean} whether the session is active.
     * @memberof ConsoleKitSessionProxy
     * @name IsActive
     */
    methods: [{ name: 'IsActive',
                inSignature: '',
                outSignature: 'b' }],
    /** signal fired whenever the session is activated or deactivated.
     * @param {boolean} active - whether the session is active or not.
     * @event
     * @memberof ConsoleKitSessionProxy
     * @name ActiveChanged */
    signals: [{ name: 'ActiveChanged',
                inSignature: 'b' }]
};
/** Creates a new ConsoleKitSessionProxy.
 * @classdesc
 * Proxy for {@link ConsoleKitSessionIface} (interface
 * 'org.freedesktop.ConsoleKit.Session')
 * @class
 * @see ConsoleKitSessionIface
 * @fires ConsoleKitSessionProxy.ActiveChanged
 */
const ConsoleKitSessionProxy = DBus.makeProxyClass(ConsoleKitSessionIface);

/** Partial definition of interface 'org.freedesktop.ConsoleKit.Manager'.
 *
 * This is also defined in {@link namespace:ConsoleKit} as
 * {@link ConsoleKit.ConsoleKitManagerIface}, although this one exposes different
 * methods to that one. That one is primarily concerned with shutdown/restart
 * options whereas this one just exposes `GetCurrentSession`.
 *
 * See the [ConsoleKit DBus documentation](http://www.freedesktop.org/software/ConsoleKit/doc/ConsoleKit.html#Manager).
 * @const
 * @see ConsoleKitManager
 * @type {Object}
 */
const ConsoleKitManagerIface = {
    name: 'org.freedesktop.ConsoleKit.Manager',
    /** Attempts to determine the session ID that the caller belongs to.
     * @name GetCurrentSession
     * @returns {string} The object identifier for the current session (signature 'o').
     * i.e. DBus object path to the current session as far as I can tell.
     * @function
     * @memberof ConsoleKitManagerIface
     */
    methods: [{ name: 'GetCurrentSession',
                inSignature: '',
                outSignature: 'o' }]
};

/** creates a new ConsoleKitManager
 * This exports this instance as a proxy for the object at
 * '/org/freedesktop/ConsoleKit/Manager' on bus 'org.freedesktop.ConsoleKit'
 * (implementing interface 'org.freedesktop.ConsoleKit.Manager').
 *
 * It also watches bus name 'org.freedesktop.ConsoleKit' to make sure it keeps
 * local property {@link #sessionActive} in sync with the true
 * value.
 *
 * It has method `GetCurrentSession` (it is different to {@link ConsoleKit.ConsoleKitManager}
 * which is used in GDM (login screen), which exposes methods `CanRestart`,
 * `CanStop`, `Restart` and `Stop` on the interface - same object and path).
 *
 * @classdesc
 * This class allows gnome-shell to interact with the DBus service
 * 'org.freedesktop.ConsoleKit'.
 * See the
 * [ConsoleKit homepage](http://www.freedesktop.org/wiki/Software/ConsoleKit).
 *
 * Its main function is to keep its member {@link #sessionActive}
 * up-to-date.
 *
 * It is different to {@link ConsoleKit.ConsoleKitManager}
 * which is used in GDM (login screen) in that that one primarily exposes methods
 * to do with shutting down and restarting, whereas this one just exposes
 * the `GetSession` method.
 *
 * @class
 * @see ConsoleKitManagerIface
 * @see ConsoleKit.ConsoleKitManager
 */
function ConsoleKitManager() {
    this._init();
};

ConsoleKitManager.prototype = {
    _init: function() {
        /** whether the session is active. Updated by monitoring DBus.
         * @type {boolean} */
        this.sessionActive = true;

        DBus.system.proxifyObject(this,
                                  'org.freedesktop.ConsoleKit',
                                  '/org/freedesktop/ConsoleKit/Manager');

        DBus.system.watch_name('org.freedesktop.ConsoleKit',
                               false, // do not launch a name-owner if none exists
                               Lang.bind(this, this._onManagerAppeared),
                               Lang.bind(this, this._onManagerVanished));
    },

    /** callback when the owner for bus name 'org.freedesktop.ConsoleKit'
     * appears. Updates {@link #sessionActive}. */
    _onManagerAppeared: function(owner) {
        this.GetCurrentSessionRemote(Lang.bind(this, this._onCurrentSession));
    },

    /** callback when the owner for bus name 'org.freedesktop.ConsoleKit'
     * vanishes. Updates {@link #sessionActive}. */
    _onManagerVanished: function(oldOwner) {
        this.sessionActive = true;
    },

    /** callback for {@link ConsoleKitManagerIface#GetCurrentSession}, called
     * when the name owner of bus 'org.freedesktop.ConsoleKit' changes.
     *
     * It creates a {@link ConsoleKitSessionProxy} for the session at object
     * path `session` and connects to its {@link ConsoleKitSessionProxy#ActiveChanged}
     * signal/calls its {@link ConsoleKitSessionProxy#IsActive} method in
     * order to update {@link #sessionActive}.
     * @param {string} session - DBus object path for the the current session
     * (returned by DBus, signature `o`).
     */
    _onCurrentSession: function(session) {
        this._ckSession = new ConsoleKitSessionProxy(DBus.system, 'org.freedesktop.ConsoleKit', session);

        this._ckSession.connect
            ('ActiveChanged', Lang.bind(this, function(object, isActive) {
                this.sessionActive = isActive;            
            }));
        this._ckSession.IsActiveRemote(Lang.bind(this, function(isActive) {
            this.sessionActive = isActive;            
        }));
    }
};
DBus.proxifyPrototype(ConsoleKitManager.prototype, ConsoleKitManagerIface);

/** creates a new AutomountManager
 *
 * This also makes a local {@link ConsoleKitManager} instance in order to
 * know whether the session is active or not, and a local
 * {@link ScreenSaver.ScreenSaverProxy} to monitor when the screensaver switches
 * on and off.
 *
 * It also stores a `Gio.VolumeMonitor` (volumes as in drives, not audio) and
 * connects to signals to know when the user plugs in, ejects etc a drive.
 *
 * @classdesc
 *
 * The automount manager monitors for the connection of
 * a mountable volume/media and when it detects this, checks whether the volume
 * should be automounted. If so, it mounts the volume.
 * @class
 */
function AutomountManager() {
    this._init();
}

AutomountManager.prototype = {
    _init: function() {
        this._settings = new Gio.Settings({ schema: SETTINGS_SCHEMA });
        this._volumeQueue = [];

        /** ConsoleKit manager to monitor the current session status.
         * @type {AutomountManager.ConsoleKitManager} */
        this.ckListener = new ConsoleKitManager();

        /** Used to monitor when the screensaver is switched on and off
         * @type {ScreenSaver.ScreenSaverProxy} */
        this._ssProxy = new ScreenSaver.ScreenSaverProxy();
        this._ssProxy.connect('ActiveChanged',
                              Lang.bind(this,
                                        this._screenSaverActiveChanged));

        /** Used to know when the user connects/disconnects etc a drive or
         * volume.
         * @type {Gio.VolumeMonitor} */
        this._volumeMonitor = Gio.VolumeMonitor.get();

        this._volumeMonitor.connect('volume-added',
                                    Lang.bind(this,
                                              this._onVolumeAdded));
        this._volumeMonitor.connect('volume-removed',
                                    Lang.bind(this,
                                              this._onVolumeRemoved));
        this._volumeMonitor.connect('drive-connected',
                                    Lang.bind(this,
                                              this._onDriveConnected));
        this._volumeMonitor.connect('drive-disconnected',
                                    Lang.bind(this,
                                              this._onDriveDisconnected));
        this._volumeMonitor.connect('drive-eject-button',
                                    Lang.bind(this,
                                              this._onDriveEjectButton));

        Mainloop.idle_add(Lang.bind(this, this._startupMountAll));
    },

    /** callback when the screensaver is switched on and off.
     * If the screensaver has just been switched off, this rechecks all the
     * attached volumes and automounts them if they want to be.
     * @param {?} object - object firing the signal (the {@link ScreenSaver.ScreenSaverProxy}?)
     * @param {boolean} isActive - whether the screensaver is on (`isActive = true`) or not.
     */
    _screenSaverActiveChanged: function(object, isActive) {
        if (!isActive) {
            this._volumeQueue.forEach(Lang.bind(this, function(volume) {
                this._checkAndMountVolume(volume);
            }));
        }

        // clear the queue anyway
        this._volumeQueue = [];
    },

    /** Gets all the volumes from {@link #_volumeMonitor} and
     * mounts the ones that want to be automounted. */
    _startupMountAll: function() {
        let volumes = this._volumeMonitor.get_volumes();
        volumes.forEach(Lang.bind(this, function(volume) {
            this._checkAndMountVolume(volume, { checkSession: false,
                                                useMountOp: false });
        }));

        return false;
    },

    /** callback when a drive is connected to the system. If the session is active and
     * the screensaver is not, this just plays the 'device-added-media' sound.
     */
    _onDriveConnected: function() {
        // if we're not in the current ConsoleKit session,
        // or screensaver is active, don't play sounds
        if (!this.ckListener.sessionActive)
            return;        

        if (this._ssProxy.screenSaverActive)
            return;

        global.play_theme_sound(0, 'device-added-media');
    },

    /** callback when a drive is disconnected from the system. If the session is active and
     * the screensaver is not, this just plays the 'device-added-media' sound.
     */
    _onDriveDisconnected: function() {
        // if we're not in the current ConsoleKit session,
        // or screensaver is active, don't play sounds
        if (!this.ckListener.sessionActive)
            return;        

        if (this._ssProxy.screenSaverActive)
            return;

        global.play_theme_sound(0, 'device-removed-media');        
    },

    /** callback when the user presses the eject button for a drive.
     * This stops/ejects the drive `drive`.
     * @param {Gio.VolumeMonitor} monitor - the monitor that emitted the signal
     * @param {Gio.Drive} drive - the drive the eject button was pressed on.
     */
    _onDriveEjectButton: function(monitor, drive) {
        // TODO: this code path is not tested, as the GVfs volume monitor
        // doesn't emit this signal just yet.
        if (!this.ckListener.sessionActive)
            return;

        // we force stop/eject in this case, so we don't have to pass a
        // mount operation object
        if (drive.can_stop()) {
            drive.stop
                (Gio.MountUnmountFlags.FORCE, null, null,
                 Lang.bind(this, function(drive, res) {
                     try {
                         drive.stop_finish(res);
                     } catch (e) {
                         log("Unable to stop the drive after drive-eject-button " + e.toString());
                     }
                 }));
        } else if (drive.can_eject()) {
            drive.eject_with_operation 
                (Gio.MountUnmountFlags.FORCE, null, null,
                 Lang.bind(this, function(drive, res) {
                     try {
                         drive.eject_with_operation_finish(res);
                     } catch (e) {
                         log("Unable to eject the drive after drive-eject-button " + e.toString());
                     }
                 }));
        }
    },

    /** callback when a mountable volume is added to the system. This checks
     * whether the drive should be automounted and does so if so (see
     * {@link #_checkAndMountVolume}).
     * @inheritparams onDriveEjectButton
     * @param {Gio.Volume} volume - the relevant volume.
     */
    _onVolumeAdded: function(monitor, volume) {
        this._checkAndMountVolume(volume);
    },

    /** This checks a volume as to whether it can be mounted and should be
     * automounted and if so, mounts it.
     *
     * If the screensaver is active the volume will instead be checked upon
     * the screensaver deactivating.
     * @param {Gio.Volume} volume - the volume to check/mount.
     * @param {Object} params - parameters for the mounting:
     * @param {boolean} [params.checkSession=true] - whether to check whether the
     * current ConsoleKit session is active. If the check is enabled and the
     * console kit session is not active, this will not mount the volume.
     * @param {boolean} [params.useMountOp=true] - whether to use a
     * `ShellMountOperation.ShellMountOperation` to mount the volume (this means
     * the user gets to interact with the operation -> confirm/deny/scan for files (?))
     */
    _checkAndMountVolume: function(volume, params) {
        params = Params.parse(params, { checkSession: true,
                                        useMountOp: true });

        if (params.checkSession) {
            // if we're not in the current ConsoleKit session,
            // don't attempt automount
            if (!this.ckListener.sessionActive)
                return;

            if (this._ssProxy.screenSaverActive) {
                if (this._volumeQueue.indexOf(volume) == -1)
                    this._volumeQueue.push(volume);

                return;
            }
        }

        // Volume is already mounted, don't bother.
        if (volume.get_mount())
            return;

        if (!this._settings.get_boolean(SETTING_ENABLE_AUTOMOUNT) ||
            !volume.should_automount() ||
            !volume.can_mount()) {
            // allow the autorun to run anyway; this can happen if the
            // mount gets added programmatically later, even if 
            // should_automount() or can_mount() are false, like for
            // blank optical media.
            this._allowAutorun(volume);
            this._allowAutorunExpire(volume);

            return;
        }

        if (params.useMountOp) {
            let operation = new ShellMountOperation.ShellMountOperation(volume);
            this._mountVolume(volume, operation.mountOp);
        } else {
            this._mountVolume(volume, null);
        }
    },

    /** Mounts a volume.
     * @param {Gio.Volume} volume - volume to be mounted.
     * @param {?ShellMountOperation.ShellMountOperation} operation - mount operation
     * associated with the mount. If `null`, the user will not be consulted
     * about the mounting and it will just be done.
     */
    _mountVolume: function(volume, operation) {
        this._allowAutorun(volume);
        volume.mount(0, operation, null,
                     Lang.bind(this, this._onVolumeMounted));
    },

    /** callback when a volume is done mounting
     * (from {@link #_onVolumeMounted}).
     * This attempts to finalise mounting the volume.
     *
     * If the operation did not succeed, either an error is logged to the console
     * OR if the reason for failure was due to a wrong or missing password, the
     * user is asked for their password ({@link #_reaskPassword}).
     * @param {Gio.Volume} volume - volume that was attempted to be mounted.
     * @param {Gio.AsyncResult} res - result of the operation.
     */
    _onVolumeMounted: function(volume, res) {
        this._allowAutorunExpire(volume);

        try {
            volume.mount_finish(res);
        } catch (e) {
            let string = e.toString();

            // FIXME: needs proper error code handling instead of this
            // See https://bugzilla.gnome.org/show_bug.cgi?id=591480
            if (string.indexOf('No key available with this passphrase') != -1)
                this._reaskPassword(volume);
            else
                log('Unable to mount volume ' + volume.get_name() + ': ' + string);
        }
    },

    /** callback when a mounted volume is removed from the system.
     * @inheritparams _onVolumeAdded
     */
    _onVolumeRemoved: function(monitor, volume) {
        this._volumeQueue = 
            this._volumeQueue.filter(function(element) {
                return (element != volume);
            });
    },

    /** Reasks the user for their password by creating a new `ShellMountOperation`
     * with `reaskPassword: true` and calling {@link #_reaskPassword}
     * with this new operation.
     * @inheritparams _onVolumeAdded
     */
    _reaskPassword: function(volume) {
        let operation = new ShellMountOperation.ShellMountOperation(volume, { reaskPassword: true });
        this._mountVolume(volume, operation.mountOp);        
    },

    /** Enables autorun on a volume.
     * @inheritparams _onVolumeAdded
     */
    _allowAutorun: function(volume) {
        volume.allowAutorun = true;
    },

    /** Causes autorun to expire after {@link AUTORUN_EXPIRE_TIMEOUT_SECS}.
     * @inheritparams _onVolumeAdded
     */
    _allowAutorunExpire: function(volume) {
        Mainloop.timeout_add_seconds(AUTORUN_EXPIRE_TIMEOUT_SECS, function() {
            volume.allowAutorun = false;
            return false;
        });
    }
}
