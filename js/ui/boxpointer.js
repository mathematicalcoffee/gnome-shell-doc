// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Whenever you open a popup menu there's a triangle from the menu that connects
 * it to the item the menu is coming from. This file defines that class.
 *
 * ![BoxPointer is the triangle connecting the popup menu to the source](pics/BoxPointer.png)
 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const St = imports.gi.St;
const Shell = imports.gi.Shell;

const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

/** Time over which to show/hide the box pointer if we are animating it.
 * @const
 * @default
 * @type {number} */
const POPUP_ANIMATION_TIME = 0.15;

/**
 * Creates a new BoxPointer
 *
 * An actor which displays a triangle "arrow" pointing from a given
 * side.  The {@link #bin} property is a container in which content can be
 * placed. For example a {@link PopupMenu.PopupMenu} puts the actual popup menu
 * bit in here. A border is drawn around {@link #bin} and the pointer.
 *
 * The arrow position may be controlled via {@link #setArrowOrigin}.
 * @param {St.Side} side - side of {@link #bin} to draw the arrow on - TOP, BOTTOM, LEFT or RIGHT.
 * @param {Object} binProperties - Properties to set on contained St.Bin, see
 * [St.Bin properties](http://developer.gnome.org/st/stable/StBin.html#StBin.properties).
 * Examples - `(x|y)_(align|fill)`, `child`, `style_class`, `reactive`,
 * `track_hover` ...
 *
 * ![BoxPointer; red arrow as % of *menu* width is {@link #_arrowAlignment}; yellow arrow as % of *source* width is {@link #_sourceAlignment}](pics/BoxPointerDiagram.png)
 *
 * ![BoxPointer diagram. a: {@link #_arrowAlignment}. b: {@link BoPointer#_sourceAlignment}. c: CSS '-arrow-base'. d: CSS '-arrow-rise'. e: CSS '-boxpointer-gap' (default 0). f: CSS '-arrow-border-radius'.](pics/boxpointer.svg);
 *
 *
 * Explanation of parts:
 *
 * * {@link #_arrowAlignment} is how far along the *menu* the pointer
 * is positioned as a percentage of the menu width; this is the **red** arrow in the
 * picture above (for the date menu it's 25%).
 * * {@link #_sourceAlignment} is how far along the *source actor* (the
 * button for the date menu in this case) the pointer is positioned as a percentage
 * of the source actor's width; this is the **yellow** arrow in the picture above.
 * * {@link #_arrowOrigin} is the length of {@link #_arrowAlignment}
 * in pixels, as far as I can tell (red arrow).
 * * {@link #_arrowSide} is the side of {@link #bin} (e.g. the menu part of the popup menu) on which the box
 * pointer is drawn; in the picture above this is `St.Side.TOP` (since the
 * pointer is on the top side of the menu).
 *
 * The dimensions of the pointer are determined by the following properties
 * of {@link #actor}'s CSS style class:
 *
 * * '-arrow-rise': height of the arrow (base to tip) in pixels.
 * * '-arrow-base': width of the base of the arrow in pixels.
 * * '-arrow-border-radius': corner radius of the border drawn around {@link #bin}
 * (so in the case of a {@link PopupMenu.PopupMenu}, this is the corner border
 * radius for the popup menu).
 * * '-arrow-border-width': width of the border drawn around both the pointer and
 * {@link #bin}
 * * '-arrow-background-color', '-arrow-border-color': colour of the background
 * and border of the arrow and {@link #bin}.
 *
 * Explanation of actors:
 *
 * * {@link #actor} - the top-level actor for the box pointer. Just
 * a container. Properties of the pointer (e.g. border, base, and height) come
 * from this actor's CSS style class - for example the popup menu uses
 * style class 'popup-menu-boxpointer' for this.
 * * {@link #_container} - nested in {@link #actor}, this is
 * the actor that does all the work - it contains {@link #_border} which
 * is the pointer+border part, and {@link #bin} which is the content part
 * (example the popup menu itself), and allocates them each the appropriate sizes.
 * * {@link #bin} - this is where the users of this class will place
 * content such as a popup menu to go with the pointer.
 * * {@link #_border} - this is a St.DrawingArea
 * drawn with cairo commands in {@link #_drawBorder}. It draws the pointer
 * itself as well as a border around {@link #bin} and the pointer.
 * @classdesc
 * ![BoxPointer is the triangle connecting the popup menu to the source](pics/BoxPointer.png)
 *
 * Whenever you open a popup menu there's a triangle from the menu that connects
 * it to the item the menu is coming from. This file defines that class.
 *
 * An actor which displays a triangle "arrow" pointing from a given
 * side.  The {@link #bin} property is a container in which content can be
 * placed. For example, {@link PopupMenu.PopupMenu} consists of a pointer with
 * the actual popup menu contents placed into {@link #bin}.
 *
 * Use {@link #setPosition} to set where the *base* of the arrow is
 * with respect to the side of {@link #bin} it's on (default 0.5, i.e. base
 * of the arrow occurs midway down that side of {@link #bin}). This is the red
 * arrow above.
 *
 * Use {@link #setSourceAlignment} to set where the *tip* of the arrow
 * lines up to the source actor (default 0.5). For example, a value of 0 causes
 * the tip of the arrow to line up to the start of the side of the source actor.
 * This is the yellow arrow above.
 *
 * ![BoxPointer; red arrow as % of *menu* width is {@link #_arrowAlignment}; yellow arrow as % of *source* width is {@link #_sourceAlignment}](pics/BoxPointerDiagram.png)
 *
 * ![BoxPointer diagram. a: {@link #_arrowAlignment}. b: {@link BoPointer#_sourceAlignment}. c: CSS '-arrow-base'. d: CSS '-arrow-rise'. e: CSS '-boxpointer-gap' (default 0). f: CSS '-arrow-border-radius'.](pics/boxpointer.svg);
 *
 * Explanation of parts:
 *
 * * {@link #_arrowAlignment} is how far along the *menu* the pointer
 * is positioned as a percentage of the menu width; this is the **red** arrow in the
 * picture above (for the date menu it's 25%).
 * * {@link #_sourceAlignment} is how far along the *source actor* (the
 * button for the date menu in this case) the pointer is positioned as a percentage
 * of the source actor's width; this is the **yellow** arrow in the picture above.
 * * {@link #_arrowOrigin} is the length of {@link #_arrowAlignment}
 * in pixels, as far as I can tell (red arrow).
 * * {@link #_arrowSide} is the side of {@link #bin} (e.g. the menu part of the popup menu) on which the box
 * pointer is drawn; in the picture above this is `St.Side.TOP` (since the
 * pointer is on the top side of the menu).
 *
 * The dimensions of the pointer are determined by the following properties
 * of {@link #actor}'s CSS style class:
 *
 * * '-arrow-rise': height of the arrow (base to tip) in pixels.
 * * '-arrow-base': width of the base of the arrow in pixels.
 * * '-arrow-border-radius': TODO
 * * '-arrow-border-width': width of the border drawn around both the pointer and
 * {@link #bin}
 * * '-arrow-background-color', '-arrow-border-color': colour of the background
 * and border of the arrow and {@link #bin}.
 *
 * Explanation of actors:
 *
 * * {@link #actor} - the top-level actor for the box pointer. Just
 * a container. Properties of the pointer (e.g. border, base, and height) come
 * from this actor's CSS style class - for example the popup menu uses
 * style class 'popup-menu-boxpointer' for this.
 * * {@link #_container} - nested in {@link #actor}, this is
 * the actor that does all the work - it contains {@link #_border} which
 * is the pointer+border part, and {@link #bin} which is the content part
 * (example the popup menu itself), and allocates them each the appropriate sizes.
 * * {@link #bin} - this is where the users of this class will place
 * content such as a popup menu to go with the pointer.
 * * {@link #_border} - this is a St.DrawingArea
 * drawn with cairo commands in {@link #_drawBorder}. It draws the pointer
 * itself as well as a border around {@link #bin} and the pointer.
 *
 * @class
 * @see PopupMenu.PopupMenu
 */
function BoxPointer(side, binProperties) {
    this._init(side, binProperties);
}

BoxPointer.prototype = {
    _init: function(arrowSide, binProperties) {
        /** The side of {@link #bin} that the arrow appears on. So using (say)
         * `St.Side.TOP` in a PopupMenu's box pointer is like the popup menus for the status buttons in the
         * top panel - the box pointer comes out the top of the menu, pointing
         * to the bottom of the source button.
         * @type {St.Side} */
        this._arrowSide = arrowSide;
        /** This is {@link #_arrowAlignment} in pixels as far as I can
         * tell. The pixels between the start of the menu side that the arrow is
         * on to the middle of the box pointer (red arrow in the diagram in the
         * class documentation).
         * @type {number} */
        this._arrowOrigin = 0;
        /** Top-level actor for the box pointer is a St.Bin, but the user
         * is more insterested in {@link #bin} whch is nested inside.
         * @type {St.Bin} */
        this.actor = new St.Bin({ x_fill: true,
                                  y_fill: true });
        /** This controls allocating the size of the pointer. It has
         * {@link #bin} and {@link #_border} in it */
        this._container = new Shell.GenericContainer();
        this.actor.set_child(this._container);
        this._container.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this._container.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this._container.connect('allocate', Lang.bind(this, this._allocate));
        /** A container into which content can be placed, properties defined
         * by the `binProperties` parameter in the constructor.
         * For example, {@link PopupMenu.PopupMenu} places the popup menu into
         * the bin (the pointer is in {@link #_border}).
         * @type {St.Bin} */
        this.bin = new St.Bin(binProperties);
        this._container.add_actor(this.bin);
        /** This is the canvas where the triangle and border around both the
         * triangle and {@link #bin} will be painted.
         * @type {St.DrawingArea}
         * @see #_drawBorder */
        this._border = new St.DrawingArea();
        this._border.connect('repaint', Lang.bind(this, this._drawBorder));
        this._container.add_actor(this._border);
        this.bin.raise(this._border);
        this._xOffset = 0;
        this._yOffset = 0;
        this._xPosition = 0;
        this._yPosition = 0;
        /** The position of the *tip* of the arrow with respect to the source
         * actor that it is pointing to. Default 0.5, i.e. the middle.
         * (yellow arrow in the diagram in the class documentation, unit is
         * fraction of the source actor's width).
         * @type {number} */
        this._sourceAlignment = 0.5;
    },

    /** Shows the box pointer by tweening its opacity from 0 to 255 over time
     * {@link POPUP_ANIMATION_TIME}.
     *
     * If `animate` is true, we also animate by "sliding" the pointer/menu out
     * from the source actor (it achieves this by setting {@link #xOffset}
     * and {@link #yOffset}).
     *
     * @param {boolean} [animate=false] - whether to animate the pointer by
     * sliding it out from the source actor (if you use {@link PopupMenu.PopupMenu#show}
     * with the `animate` variable set to `true` it is passed on to this
     * function).
     * @param {function()} [onComplete] - a callback to be executed when
     * the animation is complete (the message tray box pointer uses this but
     * nothing else as far as I can tell).
     */
    show: function(animate, onComplete) {
        let themeNode = this.actor.get_theme_node();
        let rise = themeNode.get_length('-arrow-rise');

        this.opacity = 0;
        this.actor.show();

        if (animate) {
            switch (this._arrowSide) {
                case St.Side.TOP:
                    this.yOffset = -rise;
                    break;
                case St.Side.BOTTOM:
                    this.yOffset = rise;
                    break;
                case St.Side.LEFT:
                    this.xOffset = -rise;
                    break;
                case St.Side.RIGHT:
                    this.xOffset = rise;
                    break;
            }
        }

        Tweener.addTween(this, { opacity: 255,
                                 xOffset: 0,
                                 yOffset: 0,
                                 transition: 'linear',
                                 onComplete: onComplete,
                                 time: POPUP_ANIMATION_TIME });
    },

    /** Hides the box pointer by fading it out (opacity 255 to 0) over time
     * {@link POPUP_ANIMATION_TIME}..
     *
     * If `animate` is true, the pointer/attached menu will additionally appear
     * to "slide" back into the source actor.
     * @param {boolean} [animate=false] - whether to animate the pointer by
     * sliding it back into the source actor (if you use {@link PopupMenu.PopupMenu#hide}
     * with the `animate` variable set to `true` it is passed on to this
     * function).
     * @param {function()} [onComplete] - a callback to be executed when
     * the animation is complete (the message tray box pointer uses this but
     * nothing else as far as I can tell).
     */
    hide: function(animate, onComplete) {
        let xOffset = 0;
        let yOffset = 0;
        let themeNode = this.actor.get_theme_node();
        let rise = themeNode.get_length('-arrow-rise');

        if (animate) {
            switch (this._arrowSide) {
                case St.Side.TOP:
                    yOffset = rise;
                    break;
                case St.Side.BOTTOM:
                    yOffset = -rise;
                    break;
                case St.Side.LEFT:
                    xOffset = rise;
                    break;
                case St.Side.RIGHT:
                    xOffset = -rise;
                    break;
            }
        }

        Tweener.addTween(this, { opacity: 0,
                                 xOffset: xOffset,
                                 yOffset: yOffset,
                                 transition: 'linear',
                                 time: POPUP_ANIMATION_TIME,
                                 onComplete: Lang.bind(this, function () {
                                     this.actor.hide();
                                     this.xOffset = 0;
                                     this.yOffset = 0;
                                     if (onComplete)
                                         onComplete();
                                 })
                               });
    },

    /** Adjusts an input allocation, adding in space for the box pointer.
     *
     * Basically, if the input allocation is for the width property and the
     * arrow is on the left or right side of {@link #bin}, the arrow size is
     * added (`'-arrow-rise' + 2 * '-arrow-border-width'`), and likewise
     * if thte allocation is for the height property and the pointer is on the
     * top or bottom side of {@link BoxPoiner#bin}
     *
     * @see #_getPreferredWidth
     * @see #_getPreferredHeight
     *
     * @param {boolean} isWidth - whether the allocation is for the width
     * or height property.
     * @param {Object} alloc - the allocation for width or height.
     * @param {number} alloc.min_size - minimum width/height.
     * @param {number} alloc.natural_size - natural width/height.
     */
    _adjustAllocationForArrow: function(isWidth, alloc) {
        let themeNode = this.actor.get_theme_node();
        let borderWidth = themeNode.get_length('-arrow-border-width');
        alloc.min_size += borderWidth * 2;
        alloc.natural_size += borderWidth * 2;
        if ((!isWidth && (this._arrowSide == St.Side.TOP || this._arrowSide == St.Side.BOTTOM))
            || (isWidth && (this._arrowSide == St.Side.LEFT || this._arrowSide == St.Side.RIGHT))) {
            let rise = themeNode.get_length('-arrow-rise');
            alloc.min_size += rise;
            alloc.natural_size += rise;
        }
    },

    /** Callback for the 'get-preferred-width' signal of {@link #_container}.
     *
     * This allocates the width requested by {@link #bin} and adds on
     * the pointer size (if the pointer is on the left or right side).
     * @see {@link #_adjustAllocationForArrow}
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        let [minInternalSize, natInternalSize] = this.bin.get_preferred_width(forHeight);
        alloc.min_size = minInternalSize;
        alloc.natural_size = natInternalSize;
        this._adjustAllocationForArrow(true, alloc);
    },

    /** Callback for the 'get-preferred-height' signal of {@link #_container}.
     *
     * This allocates the height requested by {@link #bin} and adds on
     * the pointer size (if the pointer is on the top or bottom side).
     * @see {@link #_adjustAllocationForArrow}
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        let [minSize, naturalSize] = this.bin.get_preferred_height(forWidth);
        alloc.min_size = minSize;
        alloc.natural_size = naturalSize;
        this._adjustAllocationForArrow(false, alloc);
    },

    /** Callback for the 'allocate' signal of {@link #_container}.
     *
     * This allocates {@link #_border} the entire width/height available
     * to it and then positions {@link #bin} such it also leaves space
     * for a border and pointer to be drawn around it, but otherwise takes up
     * as much space as it can.
     *
     * If the actor is visible it is redrawn.
     * @see #_reposition
     */
    _allocate: function(actor, box, flags) {
        let themeNode = this.actor.get_theme_node();
        let borderWidth = themeNode.get_length('-arrow-border-width');
        let rise = themeNode.get_length('-arrow-rise');
        let childBox = new Clutter.ActorBox();
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;

        childBox.x1 = 0;
        childBox.y1 = 0;
        childBox.x2 = availWidth;
        childBox.y2 = availHeight;
        this._border.allocate(childBox, flags);

        childBox.x1 = borderWidth;
        childBox.y1 = borderWidth;
        childBox.x2 = availWidth - borderWidth;
        childBox.y2 = availHeight - borderWidth;
        switch (this._arrowSide) {
            case St.Side.TOP:
                childBox.y1 += rise;
                break;
            case St.Side.BOTTOM:
                childBox.y2 -= rise;
                break;
            case St.Side.LEFT:
                childBox.x1 += rise;
                break;
            case St.Side.RIGHT:
                childBox.x2 -= rise;
                break;
        }
        this.bin.allocate(childBox, flags);

        if (this._sourceActor && this._sourceActor.mapped)
            this._reposition(this._sourceActor, this._arrowAlignment);
    },

    /** Responsible for drawing both the box pointer, and the border around
     * it and {@link #bin}. Drawn on {@link #_border}.
     *
     * The dimensions of the pointer are determined by the following properties
     * of {@link #actor}'s CSS style class:
     *
     * * '-arrow-rise': height of the arrow (base to tip) in pixels.
     * * '-arrow-base': width of the base of the arrow in pixels.
     * * '-arrow-border-radius': corner radius of the border drawn around {@link #bin}
     * (so in the case of a {@link PopupMenu.PopupMenu}, this is the corner border
     * radius for the popup menu).
     * * '-arrow-border-width': width of the border drawn around both the pointer and
     * {@link #bin}
     * * '-arrow-background-color', '-arrow-border-color': colour of the background
     * and border of the arrow and {@link #bin} (of course if {@link #bin}
     * has its own background colour specified then its background will not take
     * on '-arrow-background-color' but the pointer background will, so this
     * could look a bit weird).
     *
     * @param {St.DrawingArea} area - area to draw on ({@link #_border}.
     */
    _drawBorder: function(area) {
        let themeNode = this.actor.get_theme_node();

        let borderWidth = themeNode.get_length('-arrow-border-width');
        let base = themeNode.get_length('-arrow-base');
        let rise = themeNode.get_length('-arrow-rise');
        let borderRadius = themeNode.get_length('-arrow-border-radius');

        let halfBorder = borderWidth / 2;
        let halfBase = Math.floor(base/2);

        let borderColor = themeNode.get_color('-arrow-border-color');
        let backgroundColor = themeNode.get_color('-arrow-background-color');

        let [width, height] = area.get_surface_size();
        let [boxWidth, boxHeight] = [width, height];
        if (this._arrowSide == St.Side.TOP || this._arrowSide == St.Side.BOTTOM) {
            boxHeight -= rise;
        } else {
            boxWidth -= rise;
        }
        let cr = area.get_context();
        Clutter.cairo_set_source_color(cr, borderColor);

        // Translate so that box goes from 0,0 to boxWidth,boxHeight,
        // with the arrow poking out of that
        if (this._arrowSide == St.Side.TOP) {
            cr.translate(0, rise);
        } else if (this._arrowSide == St.Side.LEFT) {
            cr.translate(rise, 0);
        }

        let [x1, y1] = [halfBorder, halfBorder];
        let [x2, y2] = [boxWidth - halfBorder, boxHeight - halfBorder];

        cr.moveTo(x1 + borderRadius, y1);
        if (this._arrowSide == St.Side.TOP) {
            if (this._arrowOrigin < (x1 + (borderRadius + halfBase))) {
                cr.lineTo(this._arrowOrigin, y1 - rise);
                cr.lineTo(Math.max(x1 + borderRadius, this._arrowOrigin) + halfBase, y1);
            } else if (this._arrowOrigin > (x2 - (borderRadius + halfBase))) {
                cr.lineTo(Math.min(x2 - borderRadius, this._arrowOrigin) - halfBase, y1);
                cr.lineTo(this._arrowOrigin, y1 - rise);
            } else {
                cr.lineTo(this._arrowOrigin - halfBase, y1);
                cr.lineTo(this._arrowOrigin, y1 - rise);
                cr.lineTo(this._arrowOrigin + halfBase, y1);
            }
        }

        cr.lineTo(x2 - borderRadius, y1);

        // top-right corner
        cr.arc(x2 - borderRadius, y1 + borderRadius, borderRadius,
               3*Math.PI/2, Math.PI*2);

        if (this._arrowSide == St.Side.RIGHT) {
            if (this._arrowOrigin < (y1 + (borderRadius + halfBase))) {
                cr.lineTo(x2 + rise, this._arrowOrigin);
                cr.lineTo(x2, Math.max(y1 + borderRadius, this._arrowOrigin) + halfBase);
            } else if (this._arrowOrigin > (y2 - (borderRadius + halfBase))) {
                cr.lineTo(x2, Math.min(y2 - borderRadius, this._arrowOrigin) - halfBase);
                cr.lineTo(x2 + rise, this._arrowOrigin);
            } else {
                cr.lineTo(x2, this._arrowOrigin - halfBase);
                cr.lineTo(x2 + rise, this._arrowOrigin);
                cr.lineTo(x2, this._arrowOrigin + halfBase);
            }
        }

        cr.lineTo(x2, y2 - borderRadius);

        // bottom-right corner
        cr.arc(x2 - borderRadius, y2 - borderRadius, borderRadius,
               0, Math.PI/2);

        if (this._arrowSide == St.Side.BOTTOM) {
            if (this._arrowOrigin < (x1 + (borderRadius + halfBase))) {
                cr.lineTo(Math.max(x1 + borderRadius, this._arrowOrigin) + halfBase, y2);
                cr.lineTo(this._arrowOrigin, y2 + rise);
            } else if (this._arrowOrigin > (x2 - (borderRadius + halfBase))) {
                cr.lineTo(this._arrowOrigin, y2 + rise);
                cr.lineTo(Math.min(x2 - borderRadius, this._arrowOrigin) - halfBase, y2);
            } else {
                cr.lineTo(this._arrowOrigin + halfBase, y2);
                cr.lineTo(this._arrowOrigin, y2 + rise);
                cr.lineTo(this._arrowOrigin - halfBase, y2);
            }
        }

        cr.lineTo(x1 + borderRadius, y2);

        // bottom-left corner
        cr.arc(x1 + borderRadius, y2 - borderRadius, borderRadius,
               Math.PI/2, Math.PI);

        if (this._arrowSide == St.Side.LEFT) {
            if (this._arrowOrigin < (y1 + (borderRadius + halfBase))) {
                cr.lineTo(x1, Math.max(y1 + borderRadius, this._arrowOrigin) + halfBase);
                cr.lineTo(x1 - rise, this._arrowOrigin);
            } else if (this._arrowOrigin > (y2 - (borderRadius + halfBase))) {
                cr.lineTo(x1 - rise, this._arrowOrigin);
                cr.lineTo(x1, Math.min(y2 - borderRadius, this._arrowOrigin) - halfBase);
            } else {
                cr.lineTo(x1, this._arrowOrigin + halfBase);
                cr.lineTo(x1 - rise, this._arrowOrigin);
                cr.lineTo(x1, this._arrowOrigin - halfBase);
            }
        }

        cr.lineTo(x1, y1 + borderRadius);

        // top-left corner
        cr.arc(x1 + borderRadius, y1 + borderRadius, borderRadius,
               Math.PI, 3*Math.PI/2);

        Clutter.cairo_set_source_color(cr, backgroundColor);
        cr.fillPreserve();
        Clutter.cairo_set_source_color(cr, borderColor);
        cr.setLineWidth(borderWidth);
        cr.stroke();
    },

    /** Sets the alignment of the *base* of the pointer relative to {@link #bin},
     * and also sets the pointer's source actor (i.e. which actor it is pointing
     * to).
     *
     * This is a fraction of {@link #bin}'s width/height from the start of the
     * corresponding side (red arrow below), that is {@link #_arrowAlignment}.
     *
     * ![BoxPointer; red arrow as fraction of {@link #bin}'s width is {@link #_arrowAlignment}](pics/BoxPointerDiagram.png)
     * ![BoxPointer diagram. a: {@link #_arrowAlignment} (`alignment` parameter)](pics/boxpointer.svg);
     *
     *
     * @param {Clutter.Actor} sourceActor - actor the arrow is pointing to.
     * @param {number} alignment - How far along {@link #bin}'s
     * {@link #_arrowSide} side we should position the box pointer.
     * A fraction of {@link #bin}'s
     * width/height (depends on which side the arrow is positioned). Between 0
     * and 1. {@link #_arrowAlignment}.
     */
    setPosition: function(sourceActor, alignment) {
        // We need to show it now to force an allocation,
        // so that we can query the correct size.
        this.actor.show();

        this._sourceActor = sourceActor;
        this._arrowAlignment = alignment;

        this._reposition(sourceActor, alignment);
    },

    /** Sets the alignment of the tip of the pointer relative to its source
     * actor ({@link #_sourceAlignment}).
     *
     * This is a fraction of the source actor's width/height from the start of the
     * source actor (yellow arrow below), that is {@link #_sourceAlignment}.
     *
     * ![BoxPointer; yellow arrow as fraction of *source* width is {@link #_sourceAlignment}](pics/BoxPointerDiagram.png)
     * ![BoxPointer diagram. b: {@link #_sourceAlignment} (`alignment` parameter)](pics/boxpointer.svg);
     *
     * @param {number} alignment - How far along the *source* actor we should
     * position the tip of the box pointer as a fraction of the source actor's
     * width/height (depends on which side the arrow is positioned). Between 0
     * and 1. {@link #_sourceAlignment}.
     */
    setSourceAlignment: function(alignment) {
        this._sourceAlignment = alignment;

        if (!this._sourceActor)
            return;

        // We need to show it now to force an allocation,
        // so that we can query the correct size.
        this.actor.show();

        this._reposition(this._sourceActor, this._arrowAlignment);
    },

    /** Internal function, refreshes various internal variables to make sure
     * the box pointer is positioned correctly with respect to its source actor,
     * {@link #_arrowAlignment}, and {@link #_sourceAlignment}
     * and {@link #_arrowSide}.
     * @inheritparams #setPosition
     */
    _reposition: function(sourceActor, alignment) {
        // Position correctly relative to the sourceActor
        let sourceNode = sourceActor.get_theme_node();
        let sourceContentBox = sourceNode.get_content_box(sourceActor.get_allocation_box());
        let sourceAllocation = Shell.util_get_transformed_allocation(sourceActor);
        let sourceCenterX = sourceAllocation.x1 + sourceContentBox.x1 + (sourceContentBox.x2 - sourceContentBox.x1) * this._sourceAlignment;
        let sourceCenterY = sourceAllocation.y1 + sourceContentBox.y1 + (sourceContentBox.y2 - sourceContentBox.y1) * this._sourceAlignment;
        let [minWidth, minHeight, natWidth, natHeight] = this.actor.get_preferred_size();

        // We also want to keep it onscreen, and separated from the
        // edge by the same distance as the main part of the box is
        // separated from its sourceActor
        let monitor = Main.layoutManager.findMonitorForActor(sourceActor);
        let themeNode = this.actor.get_theme_node();
        let borderWidth = themeNode.get_length('-arrow-border-width');
        let arrowBase = themeNode.get_length('-arrow-base');
        let borderRadius = themeNode.get_length('-arrow-border-radius');
        let margin = (4 * borderRadius + borderWidth + arrowBase);
        let halfMargin = margin / 2;

        //let themeNode = this.actor.get_theme_node();
        let gap = themeNode.get_length('-boxpointer-gap');

        let resX, resY;

        switch (this._arrowSide) {
        case St.Side.TOP:
            resY = sourceAllocation.y2 + gap;
            break;
        case St.Side.BOTTOM:
            resY = sourceAllocation.y1 - natHeight - gap;
            break;
        case St.Side.LEFT:
            resX = sourceAllocation.x2 + gap;
            break;
        case St.Side.RIGHT:
            resX = sourceAllocation.x1 - natWidth - gap;
            break;
        }

        // Now align and position the pointing axis, making sure
        // it fits on screen
        switch (this._arrowSide) {
        case St.Side.TOP:
        case St.Side.BOTTOM:
            resX = sourceCenterX - (halfMargin + (natWidth - margin) * alignment);

            resX = Math.max(resX, monitor.x + 10);
            resX = Math.min(resX, monitor.x + monitor.width - (10 + natWidth));
            this.setArrowOrigin(sourceCenterX - resX);
            break;

        case St.Side.LEFT:
        case St.Side.RIGHT:
            resY = sourceCenterY - (halfMargin + (natHeight - margin) * alignment);

            resY = Math.max(resY, monitor.y + 10);
            resY = Math.min(resY, monitor.y + monitor.height - (10 + natHeight));

            this.setArrowOrigin(sourceCenterY - resY);
            break;
        }

        let parent = this.actor.get_parent();
        let success, x, y;
        while (!success) {
            [success, x, y] = parent.transform_stage_point(resX, resY);
            parent = parent.get_parent();
        }

        this._xPosition = Math.floor(x);
        this._yPosition = Math.floor(y);
        this._shiftActor();
    },

    /** Sets {@link #_arrowOrigin}, being {@link #_arrowAlignment}
     * as pixels (distance along {@link #bin}'s side the centre
     * of the base of the pointer is).
     * @see #_arrowOrigin
     * @param {number} origin - number of pixels along {@link #bin}'s
     * side the centre of the base of the pointer is.
     */
    setArrowOrigin: function(origin) {
        if (this._arrowOrigin != origin) {
            this._arrowOrigin = origin;
            this._border.queue_repaint();
        }
    },

    /** Internal function, shifts the Box Pointer so it's positioned properly
     * relative to its contents {@link #bin}. */
    _shiftActor : function() {
        // Since the position of the BoxPointer depends on the allocated size
        // of the BoxPointer and the position of the source actor, trying
        // to position the BoxPoiner via the x/y properties will result in
        // allocation loops and warnings. Instead we do the positioning via
        // the anchor point, which is independent of allocation, and leave
        // x == y == 0.
        this.actor.set_anchor_point(-(this._xPosition + this._xOffset),
                                    -(this._yPosition + this._yOffset));
    },

    /** Set the x offset for the pointer/contents, triggering a redraw.
     * This is really just used by {@link #show} and
     * {@link #hide} to animate sliding the pointer in and out.
     * @param {number} offset - the new x offset */
    set xOffset(offset) {
        this._xOffset = offset;
        this._shiftActor();
    },

    /** Get the x offset for the pointer.
     * @returns {number} the arrow's x offset */
    get xOffset() {
        return this._xOffset;
    },

    /** Set the y offset for the pointer/contents, triggering a redraw.
     * This is really just used by {@link #show} and
     * {@link #hide} to animate sliding the pointer in and out.
     * @param {number} offset - the new y offset */
    set yOffset(offset) {
        this._yOffset = offset;
        this._shiftActor();
    },

    /** Get the y offset for the pointer.
     * @returns {number} the arrow's y offset */
    get yOffset() {
        return this._yOffset;
    },

    /** Set the opacity for the pointer.
     * @param {number} opacity - the new opacity. */
    set opacity(opacity) {
        this.actor.opacity = opacity;
    },

    /** Get the opacity for the pointer.
     * @returns {number} the arrow's opacity */
    get opacity() {
        return this.actor.opacity;
    }
};
