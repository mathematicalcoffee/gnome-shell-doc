// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file handles installing, enabling, and disabling extensions, both
 * locally and from the web (extensions.gnome.org).
 *
 * This object will itself emit signals - for example:
 *
 *     const ExtensionSystem = imports.misc.extensionSystem;
 *     ExtensionSystem.connect('extension-loaded', ....);
 *
 * Signals - 'extension-state-changed' and 'extension-loaded'.
 *
 * @fires extension-state-changed
 * @fires extension-loaded
 */

// Note - there are some events and typedefs documented at the *bottom* of the
// file (to be out of the way).
const Lang = imports.lang;
const Signals = imports.signals;

const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const St = imports.gi.St;
const Shell = imports.gi.Shell;
const Soup = imports.gi.Soup;

const Config = imports.misc.config;
const FileUtils = imports.misc.fileUtils;
const ModalDialog = imports.ui.modalDialog;

const API_VERSION = 1;

/** @+
 * @const */
/** Represents the current state of an extension.
 * @enum
 */
const ExtensionState = {
    /** extension is enabled */
    ENABLED: 1,
    /** extension is disabled */
    DISABLED: 2,
    /** extension has experienced an error upon loading */
    ERROR: 3,
    /** extension is out of date (i.e. your GNOME-shell version is too new
     * for any of the ones it supports) */
    OUT_OF_DATE: 4,
    /** extension is downloading... */
    DOWNLOADING: 5,

    /** Used as an error state for operations on unknown extensions,
     * should never be in a real extensionMeta object. */
    UNINSTALLED: 99
};

/** Whether an extension is a global or local one.
 * @enum
 */
const ExtensionType = {
    /** extension is installed in the system directory
     * (`/usr/share`, `/usr/local/share`) under `gnome-shell/extensions` */
    SYSTEM: 1,
    /** extension is installed in the user directory
     * `$HOME/.local/share/gnome-shell/extensions` */
    PER_USER: 2
};
/** @+
 * @default */
/** base URL for online extensions ([e.g.o](https://extensions.gnome.org)) */
const REPOSITORY_URL_BASE = 'https://extensions.gnome.org';
/** URL from which to download an extension (by UUID) */
const REPOSITORY_URL_DOWNLOAD = REPOSITORY_URL_BASE + '/download-extension/%s.shell-extension.zip';
/** URL to get an extension's info  */
const REPOSITORY_URL_INFO =     REPOSITORY_URL_BASE + '/extension-info/';
/** @- */
/** @- */

const _httpSession = new Soup.SessionAsync();

// The unfortunate state of gjs, gobject-introspection and libsoup
// means that I have to do a hack to add a feature.
// See: https://bugzilla.gnome.org/show_bug.cgi?id=655189 for context.

if (Soup.Session.prototype.add_feature != null)
    Soup.Session.prototype.add_feature.call(_httpSession, new Soup.ProxyResolverDefault());

/** Retrieves the certificate for extensions.gnome.org (crt file). ("TLS CA list"?)
 * Looks first for `HOME/.local/share/gnome-shell/extensions.gnome.org.crt`,
 * and if it doesn't find that, returns {@link Config.SHELL_SYSTEM_CA_FILE}.
 *
 * Used when installing extensions from [e.g.o](https://extensions.gnome.org).
 */
function _getCertFile() {
    let localCert = GLib.build_filenamev([global.userdatadir, 'extensions.gnome.org.crt']);
    if (GLib.file_test(localCert, GLib.FileTest.EXISTS))
        return localCert;
    else
        return Config.SHELL_SYSTEM_CA_FILE;
}

_httpSession.ssl_ca_file = _getCertFile();

/** Maps uuid -> metadata object 
 * @type {Object.<string, ExtensionSystem.ExtensionMeta>} */
const extensionMeta = {};
/** Maps uuid -> importer object (extension directory tree)
 *
 * Basically `extensions[uuid]` is the `imports` object for that extension's
 * directory, so to load a file `extension_uuid/myFile.js` one would use
 * `extensions[extension_uuid].myFile`.
 * @type {Object.<string, Object>} */
const extensions = {};
/** Maps uuid -> extension state object (returned from init()).
 *
 * i.e. `extensionStateObjs[uuid]` has all the global methods/classes/variables
 * defined in that extension (like `enable()` and `disable()`).
 * @type {Object.<string, Object>} */
const extensionStateObjs = {};
/** Arrays of uuids of enalbed extensions.
 * @type {string[]} */
var enabledExtensions;
/** GFile for user extensions (i.e. `$HOME/.local/share/gnome-shell/extensions`).
 * @type {Gio.File} */
var userExtensionsDir = null;

// We don't really have a class to add signals on. So, create
// a simple dummy object, add the signal methods, and export those
// publically.
var _signals = {};
Signals.addSignalMethods(_signals);

/** Used to connect to signals emitted by the 'ExtensionSystem' object (when
 * imported using `imports.misc.extensionSystem`).
 * @function
 * @param {string} signal - signal name to connect to.
 * @param {function} cb - callback for when the signal is fired.
 * @returns {number} ID of the signal connection to be used when disconnecting.
 * @see extension-state-changed
 * @see extension-loaded
 */
const connect = Lang.bind(_signals, _signals.connect);
/** Used to disconnect from signals emitted by the 'ExtensionSystem' object
 * (when imported using `imports.misc.extensionSystem`).
 * @param {number} signal ID to disconnect.
 * @see extension-state-changed
 * @see extension-loaded
 */
const disconnect = Lang.bind(_signals, _signals.disconnect);

/** Object mapping UUID to Array of error messages
 * @type {Object.<string, string[]>}
 */
var errors = {};

/** Gsettings key under 'org.gnome.shell' that holds the array of
 * currently-enabled extensions.
 * @type {string}
 * @default
 * @const
 */
const ENABLED_EXTENSIONS_KEY = 'enabled-extensions';

/**
 * Check if a component is compatible for an extension.
 * `required` is an array, and at least one version must match.
 * `current` must be in the format `[major].[minor].[point].[micro]`.
 *
 * * `[micro]` is always ignored
 * * `[point]` is ignored if `[minor]` is even (so you can target the
 * whole stable release)
 * * `[minor]` and `[major]` must match
 *
 * Each target version must be at least `[major]` and `[minor]`.
 *
 * @param {string[]} required: - an array of versions we're compatible with
 * @param {string} current: - the version we have
 * @returns {boolean} true if our version is compatible with the required one,
 * false otherwise.
 * @todo examples
 */
function versionCheck(required, current) {
    let currentArray = current.split('.');
    let major = currentArray[0];
    let minor = currentArray[1];
    let point = currentArray[2];
    for (let i = 0; i < required.length; i++) {
        let requiredArray = required[i].split('.');
        if (requiredArray[0] == major &&
            requiredArray[1] == minor &&
            (requiredArray[2] == point ||
             (requiredArray[2] == undefined && parseInt(minor) % 2 == 0)))
            return true;
    }
    return false;
}

/** Installs an extension from extensions.gnome.org compatible with the current
 * gnome-shell.
 *
 * This will create a {@link InstallExtensionDialog} to enable the user to
 * confirm/cancel the install.
 *
 * This is used by the [Shell DBus service]{@link shellDBus.GnomeShell} to
 * remotely install extensions; I have a feeling that [e.g.o](https://extensions.gnome.org) uses it
 * too to let a user install an extension from the website..
 * @param {string} uuid - UUID of extension to install.
 * @param {string} version_tag - version of extension to install (? the integer
 * *extension* versions as opposed to the gnome-shell version).
 */
function installExtensionFromUUID(uuid, version_tag) {
    let params = { uuid: uuid,
                   version_tag: version_tag,
                   shell_version: Config.PACKAGE_VERSION,
                   api_version: API_VERSION.toString() };

    let message = Soup.form_request_new_from_hash('GET', REPOSITORY_URL_INFO, params);

    _httpSession.queue_message(message,
                               function(session, message) {
                                   let info = JSON.parse(message.response_body.data);
                                   let dialog = new InstallExtensionDialog(uuid, version_tag, info.name);
                                   dialog.open(global.get_current_time());
                               });
}

/** Uninstalls an extension by its UUID. If the extension is a system one,
 * it will not be uninstalled.  
 * Otherwise:
 * 
 * 1. the extension is disabled {@link disableExtension};
 * 2. the 'extension-state-changed' signal is emitted with the state set to
 * `ExtensionState.UNINSTALLED`;
 * 3. extension meta is deleted from {@link extensionMeta};
 * 4. extension imports are removed from {@link extensions};
 * 5. extension objects are removed from {@link extensionStateObjs};
 * 6. extension errors are removed from {@link errors};
 * 7. extension directory is deleted.
 *
 * This is used by the [Shell DBus service]{@link shellDBus.GnomeShell} to
 * remotely uninstall extensions.
 * @returns {boolean} true if the uninstall succeeded, false otherwise.
 * @fires extension-state-changed
 */
function uninstallExtensionFromUUID(uuid) {
    let meta = extensionMeta[uuid];
    if (!meta)
        return false;

    // Try to disable it -- if it's ERROR'd, we can't guarantee that,
    // but it will be removed on next reboot, and hopefully nothing
    // broke too much.
    disableExtension(uuid);

    // Don't try to uninstall system extensions
    if (meta.type != ExtensionType.PER_USER)
        return false;

    meta.state = ExtensionState.UNINSTALLED;
    _signals.emit('extension-state-changed', meta);

    delete extensionMeta[uuid];

    // Importers are marked as PERMANENT, so we can't do this.
    // delete extensions[uuid];
    extensions[uuid] = undefined;

    delete extensionStateObjs[uuid];
    delete errors[uuid];

    FileUtils.recursivelyDeleteDir(Gio.file_new_for_path(meta.path));

    return true;
}

/** callback for when the zip file for an extension has been downloaded from
 * extensions.gnome.org (during an install).
 *
 * If the zip file did not download properly, an error is logged.
 * Otherwise, the file is unzipped (asynchronously) into path
 * {@link userExtensionsDir}/uuid. If the file unzips successfully,
 * it is enabled and loaded.
 *
 * @param {Soup.Session} session - the soup session
 * @param {Soup.Message} message - the soup message for the download request
 * @param {string} uuid - UUID of extension being installed
 * @see loadExtension
 */
function gotExtensionZipFile(session, message, uuid) {
    if (message.status_code != Soup.KnownStatusCode.OK) {
        logExtensionError(uuid, 'downloading extension: ' + message.status_code);
        return;
    }

    // FIXME: use a GFile mkstemp-type method once one exists
    let fd, tmpzip;
    try {
        [fd, tmpzip] = GLib.file_open_tmp('XXXXXX.shell-extension.zip');
    } catch (e) {
        logExtensionError(uuid, 'tempfile: ' + e.toString());
        return;
    }

    let stream = new Gio.UnixOutputStream({ fd: fd });
    let dir = userExtensionsDir.get_child(uuid);
    Shell.write_soup_message_to_stream(stream, message);
    stream.close(null);
    let [success, pid] = GLib.spawn_async(null,
                                          ['unzip', '-uod', dir.get_path(), '--', tmpzip],
                                          null,
                                          GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                                          null);

    if (!success) {
        logExtensionError(uuid, 'extract: could not extract');
        return;
    }

    GLib.child_watch_add(GLib.PRIORITY_DEFAULT, pid, function(pid, status) {
        GLib.spawn_close_pid(pid);

        // Add extension to 'enabled-extensions' for the user, always...
        let enabledExtensions = global.settings.get_strv(ENABLED_EXTENSIONS_KEY);
        if (enabledExtensions.indexOf(uuid) == -1) {
            enabledExtensions.push(uuid);
            global.settings.set_strv(ENABLED_EXTENSIONS_KEY, enabledExtensions);
        }

        loadExtension(dir, true, ExtensionType.PER_USER);
    });
}

/** Disables an extension by UUID.
 *
 * This calls the `disable()` method of the extension and if it succeeds, emits
 * the 'extension-state-changed' signal with the extension metadata.
 * @param {string} uuid - UUID of extension to disable.
 * @fires extension-state-changed
 */
function disableExtension(uuid) {
    let meta = extensionMeta[uuid];
    if (!meta)
        return;

    if (meta.state != ExtensionState.ENABLED)
        return;

    let extensionState = extensionStateObjs[uuid];

    try {
        extensionState.disable();
    } catch(e) {
        logExtensionError(uuid, e.toString());
        return;
    }

    meta.state = ExtensionState.DISABLED;
    _signals.emit('extension-state-changed', meta);
}

/** Enables an extension by UUID.
 *
 * This calls the `enable()` method of the extension and if it succeeds, emits
 * the 'extension-state-changed' signal with the extension metadata.
 * @param {string} uuid - UUID of extension to disable.
 * @fires extension-state-changed
 */
function enableExtension(uuid) {
    let meta = extensionMeta[uuid];
    if (!meta)
        return;

    if (meta.state != ExtensionState.DISABLED)
        return;

    let extensionState = extensionStateObjs[uuid];

    try {
        extensionState.enable();
    } catch(e) {
        logExtensionError(uuid, e.toString());
        return;
    }

    meta.state = ExtensionState.ENABLED;
    _signals.emit('extension-state-changed', meta);
}

/** Logs an error with an extension, sets the extension state to
 * `ExtensionState.ERROR`, and emits the 'extension-state-changed' signal
 * setting the 'error' property of the metadata to the error message.
 * @param {string} uuid - UUID of extension with the error
 * @param {string} message - error message
 * @param {ExtensionSystem.ExtensionState} [state=ExtensionSystem.ExtensionState.ERROR] - new state of the extension
 * @fires extension-state-changed
 */
function logExtensionError(uuid, message, state) {
    if (!errors[uuid]) errors[uuid] = [];
    errors[uuid].push(message);
    global.logError('Extension "%s" had error: %s'.format(uuid, message));
    state = state || ExtensionState.ERROR;
    _signals.emit('extension-state-changed', { uuid: uuid,
                                               error: message,
                                               state: state });
}

/** loads an extension from a directory, optionally enabling it. Fires both
 * 'extension-loaded' and 'extension-state-changed' with the newly-created
 * `metadata.json` information stored as a {@link ExtensionMeta}.
 *
 * 1. Looks for file `metadata.json`. If not found, logs an error and quits.
 * 2. Parses `metadata.json` to create an {@link ExtensionMeta}.
 * If this fails, logs an error and quits.
 * 3. Checks that the required properties 'uuid', 'name', 'description' and
 * 'shell-version' are in the metadata. If not found, logs an error and quits.
 * 4. Checks that the extension isn't already loaded. If it is already loaded,
 * logs an error and quits.
 * 5. Checks for (optional) `url` property in the metadata. If not found,
 * logs a *warning* and continues.
 * 6. Checks that the UUID from the metadata matches the directory name (if
 * not, logs an error and quits).
 * 7. Checks that the extension is compatible with the current shell and GJS
 * (based on the `shell-version` and `js-version` properties). If not, logs an
 * error and quits.
 * 8. Populates the `type`, `state`, `path` and `error` properties of the
 * extension's {@link ExtensionMeta}.
 * 9. Checks that `extension.js` exists. If not, error and quit.
 * 10. Checks if `stylesheet.css` exists and if so, loads it (and if there's
 * an error in the CSS file, logs an error and quits).
 * 11. Adds the imports from the extension to {@link extensions}. If this
 * fails, logs an error, unloads any extension stylesheets, and quits.
 * 12. Checks for the existence of the extension's `init` function and calls it.
 * If this fails, it logs an error, unloads any extension
 * stylesheet, and quits.
 * 13. Checks for the existence of the `enable` and `disable` functions in
 * the extension.
 * If any of these do not exist, it logs an error, unloads any extension
 * stylesheet, and quits.
 * 14. sets the extension state to `ExtensionState.DISABLED`.
 * 15. If the parameter `enabled` is true, it enables the extension (see
 * {@link enableExtension}).
 * 16. Finally, emit the 'extension-loaded' and 'extension-state-changed'
 * signals.
 *
 * @param {Gio.File} dir - the Gio.File for the extension directory (create
 * with `Gio.file_new_for_path(...)`).
 * @param {boolean} enabled - whether to enable the extension after loading.
 * @param {ExtensionSystem.ExtensionType} type - the type of the extension.
 * @fires extension-state-changed
 * @fires extension-loaded
 */
function loadExtension(dir, enabled, type) {
    let info;
    let uuid = dir.get_basename();

    let metadataFile = dir.get_child('metadata.json');
    if (!metadataFile.query_exists(null)) {
        logExtensionError(uuid, 'Missing metadata.json');
        return;
    }

    let metadataContents;
    try {
        metadataContents = Shell.get_file_contents_utf8_sync(metadataFile.get_path());
    } catch (e) {
        logExtensionError(uuid, 'Failed to load metadata.json: ' + e);
        return;
    }
    let meta;
    try {
        meta = JSON.parse(metadataContents);
    } catch (e) {
        logExtensionError(uuid, 'Failed to parse metadata.json: ' + e);
        return;
    }

    let requiredProperties = ['uuid', 'name', 'description', 'shell-version'];
    for (let i = 0; i < requiredProperties.length; i++) {
        let prop = requiredProperties[i];
        if (!meta[prop]) {
            logExtensionError(uuid, 'missing "' + prop + '" property in metadata.json');
            return;
        }
    }

    if (extensions[uuid] != undefined) {
        logExtensionError(uuid, 'extension already loaded');
        return;
    }

    // Encourage people to add this
    if (!meta['url']) {
        global.log('Warning: Missing "url" property in metadata.json');
    }

    if (uuid != meta.uuid) {
        logExtensionError(uuid, 'uuid "' + meta.uuid + '" from metadata.json does not match directory name "' + uuid + '"');
        return;
    }

    if (!versionCheck(meta['shell-version'], Config.PACKAGE_VERSION) ||
        (meta['js-version'] && !versionCheck(meta['js-version'], Config.GJS_VERSION))) {
        logExtensionError(uuid, 'extension is not compatible with current GNOME Shell and/or GJS version');
        return;
    }

    extensionMeta[uuid] = meta;
    meta.type = type;
    meta.path = dir.get_path();
    meta.error = '';

    // Default to error, we set success as the last step
    meta.state = ExtensionState.ERROR;

    if (!versionCheck(meta['shell-version'], Config.PACKAGE_VERSION) ||
        (meta['js-version'] && !versionCheck(meta['js-version'], Config.GJS_VERSION))) {
        logExtensionError(uuid, 'extension is not compatible with current GNOME Shell and/or GJS version', ExtensionState.OUT_OF_DATE);
        meta.state = ExtensionState.OUT_OF_DATE;
        return;
    }

    let extensionJs = dir.get_child('extension.js');
    if (!extensionJs.query_exists(null)) {
        logExtensionError(uuid, 'Missing extension.js');
        return;
    }
    let stylesheetPath = null;
    let themeContext = St.ThemeContext.get_for_stage(global.stage);
    let theme = themeContext.get_theme();
    let stylesheetFile = dir.get_child('stylesheet.css');
    if (stylesheetFile.query_exists(null)) {
        try {
            theme.load_stylesheet(stylesheetFile.get_path());
        } catch (e) {
            logExtensionError(uuid, 'Stylesheet parse error: ' + e);
            return;
        }
    }

    let extensionModule;
    let extensionState = null;
    try {
        global.add_extension_importer('imports.ui.extensionSystem.extensions', meta.uuid, dir.get_path());
        extensionModule = extensions[meta.uuid].extension;
    } catch (e) {
        if (stylesheetPath != null)
            theme.unload_stylesheet(stylesheetPath);
        logExtensionError(uuid, e);
        return;
    }

    if (!extensionModule.init) {
        logExtensionError(uuid, 'missing \'init\' function');
        return;
    }

    try {
        extensionState = extensionModule.init(meta);
    } catch (e) {
        if (stylesheetPath != null)
            theme.unload_stylesheet(stylesheetPath);
        logExtensionError(uuid, 'Failed to evaluate init function:' + e);
        return;
    }

    if (!extensionState)
        extensionState = extensionModule;
    extensionStateObjs[uuid] = extensionState;

    if (!extensionState.enable) {
        logExtensionError(uuid, 'missing \'enable\' function');
        return;
    }
    if (!extensionState.disable) {
        logExtensionError(uuid, 'missing \'disable\' function');
        return;
    }

    meta.state = ExtensionState.DISABLED;

    if (enabled)
        enableExtension(uuid);

    _signals.emit('extension-loaded', meta.uuid);
    _signals.emit('extension-state-changed', meta);
    global.log('Loaded extension ' + meta.uuid);
}

/** Called when the enabled extensions change (callback for monitoring
 * {@link ENABLED_EXTENSIONS_KEY}).
 *
 * This enables any newly-added extensions, and disables any newly-removed
 * extensions, and also keeps {@link enabledExtensions} up to date.
 *
 * The callback is connected in {@link init}.
 * @see init
 * @see enableExtension
 * @see disableExtension
 */
function onEnabledExtensionsChanged() {
    let newEnabledExtensions = global.settings.get_strv(ENABLED_EXTENSIONS_KEY);

    // Find and enable all the newly enabled extensions: UUIDs found in the
    // new setting, but not in the old one.
    newEnabledExtensions.filter(function(uuid) {
        return enabledExtensions.indexOf(uuid) == -1;
    }).forEach(function(uuid) {
        enableExtension(uuid);
    });

    // Find and disable all the newly disabled extensions: UUIDs found in the
    // old setting, but not in the new one.
    enabledExtensions.filter(function(item) {
        return newEnabledExtensions.indexOf(item) == -1;
    }).forEach(function(uuid) {
        disableExtension(uuid);
    });

    enabledExtensions = newEnabledExtensions;
}

/**
 * Called to initialise the extensions system.
 * This populates {@link userExtensionsDir} (usually
 * `$HOME/.local/share/gnome-shell/extensions`), and monitors changes in gsettings
 * key {@link ENABLED_EXTENSIONS_KEY} to keep the enabled/disabled extensions
 * in sync with that (see {@link onEnabledExtensionsChanged}).
 *
 * Called from {@link namespace:main}.
 */
function init() {
    let userExtensionsPath = GLib.build_filenamev([global.userdatadir, 'extensions']);
    userExtensionsDir = Gio.file_new_for_path(userExtensionsPath);
    try {
        if (!userExtensionsDir.query_exists(null))
            userExtensionsDir.make_directory_with_parents(null);
    } catch (e) {
        global.logError('' + e);
    }

    global.settings.connect('changed::' + ENABLED_EXTENSIONS_KEY, onEnabledExtensionsChanged);
    enabledExtensions = global.settings.get_strv(ENABLED_EXTENSIONS_KEY);
}

/** Loads all extensions in a given directory.
 *
 * It searches through all children of the input directory and calls
 * {@link loadExtension} on each child directory with the given type, given that
 * the directory name (i.e. extension UUID) is in {@link enabledExtensions}.
 * @param {Gio.File} dir - directory to find extensions in.
 * @param {ExtensionSystem.ExtensionType} type - type of extension to load.
 */
function _loadExtensionsIn(dir, type) {
    let fileEnum;
    let file, info;
    try {
        fileEnum = dir.enumerate_children('standard::*', Gio.FileQueryInfoFlags.NONE, null);
    } catch (e) {
        global.logError('' + e);
       return;
    }

    while ((info = fileEnum.next_file(null)) != null) {
        let fileType = info.get_file_type();
        if (fileType != Gio.FileType.DIRECTORY)
            continue;
        let name = info.get_name();
        let child = dir.get_child(name);
        let enabled = enabledExtensions.indexOf(name) != -1;
        loadExtension(child, enabled, type);
    }
    fileEnum.close(null);
}

/** Loads all extensions.
 * This first looks for global extensions (`/usr/share/gnome-shell/extensions`,
 * `/usr/local/share/gnome-shell/extensions`) and calls {@link _loadExtensionsIn}
 * on them, and then loads all local extensions in {@link _loadExtensionsIn}.
 *
 * Called in {@link namespace:Main} to initially load all extensions.
 */
function loadExtensions() {
    let systemDataDirs = GLib.get_system_data_dirs();
    for (let i = 0; i < systemDataDirs.length; i++) {
        let dirPath = systemDataDirs[i] + '/gnome-shell/extensions';
        let dir = Gio.file_new_for_path(dirPath);
        if (dir.query_exists(null))
            _loadExtensionsIn(dir, ExtensionType.SYSTEM);
    }
    _loadExtensionsIn(userExtensionsDir, ExtensionType.PER_USER);
}

/** creates a new InstallExtensionDialog
 * @param {string} uuid - UUID of extension to install.
 * @param {string} version_tag - extension version to install.
 * @param {string} name - name of extension to install.
 * @classdesc
 * This is the 'install/cancel' dialog that pops up when you try to install
 * an extension (for example from [e.g.o](https://extensions.gnome.org)).
 *
 * Upon clicking 'Install', the dialog starts a http request (using Soup) with
 * [e.g.o](https://extensions.gnome.org) to download the extension.
 *
 * ![ExtensionSystem's `InstallExtensionDialog`](pics/InstallExtensionDialog.png)
 *
 * @extends ModalDialog.ModalDialog
 * @see ModalDialog.ModalDialog
 * @class
 */
function InstallExtensionDialog(uuid, version_tag, name) {
    this._init(uuid, version_tag, name);
}

InstallExtensionDialog.prototype = {
    __proto__: ModalDialog.ModalDialog.prototype,

    _init: function(uuid, version_tag, name) {
        ModalDialog.ModalDialog.prototype._init.call(this, { styleClass: 'extension-dialog' });

        this._uuid = uuid;
        this._version_tag = version_tag;
        this._name = name;

        this.setButtons([{ label: _("Cancel"),
                           action: Lang.bind(this, this._onCancelButtonPressed),
                           key:    Clutter.Escape
                         },
                         { label:  _("Install"),
                           action: Lang.bind(this, this._onInstallButtonPressed)
                         }]);

        let message = _("Download and install '%s' from extensions.gnome.org?").format(name);

        this._descriptionLabel = new St.Label({ text: message });

        this.contentLayout.add(this._descriptionLabel,
                               { y_fill:  true,
                                 y_align: St.Align.START });
    },

    /** callback when the user clicks 'Cancel' - closes the dialog and emits
     * an 'extension-state-changed' signal with the extension state being
     * `ExtensionState.UNINSTALLED`.
     *
     * Even though the extension is already "uninstalled", send through
     * a state-changed signal for any users who want to know if the install
     * went through correctly -- using proper async DBus would block more
     * traditional clients like the plugin.
     *
     * @fires extension-state-changed
     */
    _onCancelButtonPressed: function(button, event) {
        this.close(global.get_current_time());

        // Even though the extension is already "uninstalled", send through
        // a state-changed signal for any users who want to know if the install
        // went through correctly -- using proper async DBus would block more
        // traditional clients like the plugin
        let meta = { uuid: this._uuid,
                     state: ExtensionState.UNINSTALLED,
                     error: '' };

        _signals.emit('extension-state-changed', meta);
    },

    /** callback when the user clicks 'Install' - emits 'extension-state-changed'
     * with state `ExtensionState.DOWNLOADING`, requests the extension from
     * [e.g.o](https://extensions.gnome.org) using Soup and calls {@link gotExtensionZipFile} when done.
     * Then the dilaog is closed.
     * @fires extension-state-changed
     */
    _onInstallButtonPressed: function(button, event) {
        let meta = { uuid: this._uuid,
                     state: ExtensionState.DOWNLOADING,
                     error: '' };

        extensionMeta[this._uuid] = meta;

        _signals.emit('extension-state-changed', meta);

        let params = { version_tag: this._version_tag,
                       shell_version: Config.PACKAGE_VERSION,
                       api_version: API_VERSION.toString() };

        let url = REPOSITORY_URL_DOWNLOAD.format(this._uuid);
        let message = Soup.form_request_new_from_hash('GET', url, params);

        _httpSession.queue_message(message,
                                   Lang.bind(this, function(session, message) {
                                       gotExtensionZipFile(session, message, this._uuid);
                                   }));

        this.close(global.get_current_time());
    }
};

// Typedef.
/** Summarises information about an extension.
 *
 * This is essentially a `JSON.parse` of an extension's `metadata.json`.
 *
 * The properties 'uuid', 'name', 'description' and 'shell-version' are
 * populated from `metadata.json` and are required.
 *
 * The property 'url' is taken from `metadata.json` and is optional.
 *
 * The properties 'state', 'type', 'path' and 'error' are populated upon
 * loading the extension.
 *
 * There may be additional properties in this object that were in the
 * extension's `metadata.json` (for example, some extensions add an 'authors'
 * property in there or additional metadata as will be used by the extension).
 *
 * @typedef ExtensionMeta
 * @type {Object}
 * @property {string} uuid - UUID of the extension (e.g. 'hello-world@my.domain.com')
 * @property {string} name - name of the extension (e.g. 'Hello World')
 * @property {string} description - description of the extension (e.g.
 * 'says "Hello, World!" when the user clicks a button')
 * @property {string[]} shell-version - array of shell versions with which the
 * extension is compatible (e.g. `['3.2', '3.4']`).
 * @property {string} [url] - the URL of the extension's home page
 * @property {ExtensionSystem.ExtensionType} type - the type of extension
 * (system or per-user).
 * @property {string} path - the path to the extension directory (e.g.
 * '/home/foobar/.local/share/gnome-shell/extensions/hello-world@my.domain.com')
 * @property {string} error - any error the extension currently has
 * @property {string[]} [js-version] - GJS versions the extension is compatible
 * with. Omit to assume compatibility with them all.
 * @property {ExtensionSystem.ExtensionState} state - the current state of the
 * extension (enabled, disabled, error, ...)
 */
// Events
/** Event fired when an extension's state changes, for example from
 * `ExtensionState.ENABLED` to `ExtensionState.DISABLED`.
 *
 * The signal is fired from this import module, like so:
 *
 * @example
 * const ES = imports.ui.extensionSystem;
 * ES.connect('extension-state-changed', callback)
 *
 * @event
 * @name extension-state-changed
 * @param {Object} o - object that fired the signal
 * @param {ExtensionSystem.ExtensionMeta} meta - the metadata for the extension
 * whose state changed (`meta.state`).
 * @see ExtensionState
 * @see ExtensionMeta
 */
/** Event fired when an extension is loaded.
 *
 * The signal is fired from this import module, like so:
 *
 * @example
 * const ES = imports.ui.extensionSystem;
 * ES.connect('extension-loaded', callback)
 *
 * @event
 * @name extension-loaded
 * @param {Object} o - object that fired the signal
 * @param {ExtensionSystem.ExtensionMeta} meta - the metadata for the extension
 * that was just loaded.
 * @see ExtensionMeta
 */
