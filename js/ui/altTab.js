// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This files defines the alt-tab popup class.
 *
 * The alt tab popup is actually *used* in the {@link WindowManager} class.
 *
 * ![Alt Tab Popup](pics/altTabPopup.png)
 */

const Clutter = imports.gi.Clutter;
const Gdk = imports.gi.Gdk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

// unused
const POPUP_APPICON_SIZE = 96;
/** @+
 * @const
 * @default
 * @type {number} */
/** The time taken in seconds for the alt-tab popup contents to scroll if the
 * popup has more thumbnails than will fit on the screen. */
const POPUP_SCROLL_TIME = 0.10; // seconds
/** Time in milliseconds before the alt-tab popup will appear (after the popup is
 * launched, e.g. by a keybinding). */
const POPUP_DELAY_TIMEOUT = 150; // milliseconds

/** Time in seconds the alt-tab popup takes to do its fade out animation when it
 * is closed. */
const POPUP_FADE_OUT_TIME = 0.1; // seconds

/** Time in milliseconds you must hover over an app in the alt-tab popup before
 * its window list popup opens. */
const APP_ICON_HOVER_TIMEOUT = 200; // milliseconds

/** Time in milliseconds for mouse enter-events on the app switcher popup to be
 * disabled when {@link AltTabPopup#_disableHover} is called. */
const DISABLE_HOVER_TIMEOUT = 500; // milliseconds

/** Default height for window thumbnails in the window switcher of the alt-tab
 * popup. */
const THUMBNAIL_DEFAULT_SIZE = 256;
/** Time in milliseconds for the window switcher for a particular app to show
 * once the app is selected. */
const THUMBNAIL_POPUP_TIME = 500; // milliseconds
/** Time in seconds for the window thumbnail fade in/out animation in the alt-tab
 * popup. */
const THUMBNAIL_FADE_TIME = 0.1; // seconds

/** Preferred sizes for the app icons in the alt-tab switcher; the maximum size
 * that lets the popup still fit on the screen will be used. */
const iconSizes = [96, 64, 48, 32, 22];
/** @- */

/** Performs the operation `(a + b) mod b`.
 * @param {number} a
 * @param {number} b
 * @returns {number} `(a + b) % b`.
 */
function mod(a, b) {
    return (a + b) % b;
}

/** Returns the primary modifier (largest power of 2) in the given input mask.
 * @param {int} mask - mask we want to find the primary modifier of.
 * @returns {int} the primary modifier.
 */
function primaryModifier(mask) {
    if (mask == 0)
        return 0;

    let primary = 1;
    while (mask > 1) {
        mask >>= 1;
        primary <<= 1;
    }
    return primary;
}

/**
 * constructs an alt-tab popup.
 * @classdesc This class defines the alt tab popup. The UI element
 * ({@link #actor}) is a `Shell.GenericContainer`.
 * ![Alt Tab Popup](pics/altTabPopup.png)
 * @class
 */
function AltTabPopup() {
    this._init();
}

AltTabPopup.prototype = {
    _init : function() {
        /** The top-level actor for this object is a Shell.GenericContainer. Its
         * `allocate`, `get-preferred-width` and `get-preferred-height` signals
         * are handled by {@link #_allocate},
         * {@link #_getPreferredWidth} and
         * {@link #_getPreferredHeight}.
         * @type {Shell.GenericContainer}
         */
        this.actor = new Shell.GenericContainer({ name: 'altTabPopup',
                                                  reactive: true,
                                                  visible: false });

        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        this._haveModal = false;
        this._modifierMask = 0;

        this._currentApp = 0;
        this._currentWindow = -1;
        this._thumbnailTimeoutId = 0;
        this._motionTimeoutId = 0;
        this._initialDelayTimeoutId = 0;

        this.thumbnailsVisible = false;

        // Initially disable hover so we ignore the enter-event if
        // the switcher appears underneath the current pointer location
        this._disableHover();

        Main.uiGroup.add_actor(this.actor);
    },

    /** Used in the `get-preferred-width` signal for {@link #actor}
     * to get the actor's preferred width */
    _getPreferredWidth: function (actor, forHeight, alloc) {
        alloc.min_size = global.screen_width;
        alloc.natural_size = global.screen_width;
    },

    /** Used in the `get-preferred-height` signal for {@link #actor}
     * to get the actor's preferred height */
    _getPreferredHeight: function (actor, forWidth, alloc) {
        alloc.min_size = global.screen_height;
        alloc.natural_size = global.screen_height;
    },

    /** Used in the `allocate` signal for {@link #actor} to allocate
     * the alt tab popup - the app switcher and window thumbnail switcher.
     */
    _allocate: function (actor, box, flags) {
        let childBox = new Clutter.ActorBox();
        let primary = Main.layoutManager.primaryMonitor;

        let leftPadding = this.actor.get_theme_node().get_padding(St.Side.LEFT);
        let rightPadding = this.actor.get_theme_node().get_padding(St.Side.RIGHT);
        let bottomPadding = this.actor.get_theme_node().get_padding(St.Side.BOTTOM);
        let vPadding = this.actor.get_theme_node().get_vertical_padding();
        let hPadding = leftPadding + rightPadding;

        // Allocate the appSwitcher
        // We select a size based on an icon size that does not overflow the screen
        let [childMinHeight, childNaturalHeight] = this._appSwitcher.actor.get_preferred_height(primary.width - hPadding);
        let [childMinWidth, childNaturalWidth] = this._appSwitcher.actor.get_preferred_width(childNaturalHeight);
        childBox.x1 = Math.max(primary.x + leftPadding, primary.x + Math.floor((primary.width - childNaturalWidth) / 2));
        childBox.x2 = Math.min(primary.x + primary.width - rightPadding, childBox.x1 + childNaturalWidth);
        childBox.y1 = primary.y + Math.floor((primary.height - childNaturalHeight) / 2);
        childBox.y2 = childBox.y1 + childNaturalHeight;
        this._appSwitcher.actor.allocate(childBox, flags);

        // Allocate the thumbnails
        // We try to avoid overflowing the screen so we base the resulting size on
        // those calculations
        if (this._thumbnails) {
            let icon = this._appIcons[this._currentApp].actor;
            let [posX, posY] = icon.get_transformed_position();
            let thumbnailCenter = posX + icon.width / 2;
            let [childMinWidth, childNaturalWidth] = this._thumbnails.actor.get_preferred_width(-1);
            childBox.x1 = Math.max(primary.x + leftPadding, Math.floor(thumbnailCenter - childNaturalWidth / 2));
            if (childBox.x1 + childNaturalWidth > primary.x + primary.width - hPadding) {
                let offset = childBox.x1 + childNaturalWidth - primary.width + hPadding;
                childBox.x1 = Math.max(primary.x + leftPadding, childBox.x1 - offset - hPadding);
            }

            let spacing = this.actor.get_theme_node().get_length('spacing');

            childBox.x2 = childBox.x1 +  childNaturalWidth;
            if (childBox.x2 > primary.x + primary.width - rightPadding)
                childBox.x2 = primary.x + primary.width - rightPadding;
            childBox.y1 = this._appSwitcher.actor.allocation.y2 + spacing;
            this._thumbnails.addClones(primary.height - bottomPadding - childBox.y1);
            let [childMinHeight, childNaturalHeight] = this._thumbnails.actor.get_preferred_height(-1);
            childBox.y2 = childBox.y1 + childNaturalHeight;
            this._thumbnails.actor.allocate(childBox, flags);
        }
    },

    /** Returns a list of apps on the current and other workspaces.
     * @returns {Shell.App[]} apps - an array of apps on the current workspace
     * in standard Alt+Tab order (MRU (most recently used) except for minimized
     * windows).
     * @returns {Shell.App[]} allApps - an array of apps on other workspaces
     * sorted by `user_time`.
     */
    _getAppLists: function() {
        let tracker = Shell.WindowTracker.get_default();
        let appSys = Shell.AppSystem.get_default();
        let allApps = appSys.get_running ();

        let screen = global.screen;
        let display = screen.get_display();
        let windows = display.get_tab_list(Meta.TabList.NORMAL, screen,
                                           screen.get_active_workspace());

        // windows is only the windows on the current workspace. For
        // each one, if it corresponds to an app we know, move that
        // app from allApps to apps.
        let apps = [];
        for (let i = 0; i < windows.length && allApps.length != 0; i++) {
            let app = tracker.get_window_app(windows[i]);
            let index = allApps.indexOf(app);
            if (index != -1) {
                apps.push(app);
                allApps.splice(index, 1);
            }
        }

        // Now @apps is a list of apps on the current workspace, in
        // standard Alt+Tab order (MRU except for minimized windows),
        // and allApps is a list of apps that only appear on other
        // workspaces, sorted by user_time, which is good enough.
        return [apps, allApps];
    },

    /** Shows the alt-tab popup after a delay of {@link POPUP_DELAY_TIMEOUT}
     * (the delay is just so that if you use the popup very quickly, you won't
     * get a flicker as the popup displays and destroys instantly).
     *
     * This also listens to key presses (for the user cycling to the next/
     * previous app/window) and handles all the logic for switching between
     * the app list and window list for those apps.
     *
     * The actor is added to gnome-shell's modal stack with
     * {@link Main.pushModal}.
     *
     * @param {boolean} backward - whether we are cycling through the apps
     * backwards or forwards.
     * @param {string} binding - the name of the binding that launched the
     * dialog, one of 'switch_group', 'switch_group_backward', or
     * 'switch_windows_backward'.
     * @param {int} mask - the modifiers for the keybinding(?)
     */
    show : function(backward, binding, mask) {
        let [localApps, otherApps] = this._getAppLists();

        if (localApps.length == 0 && otherApps.length == 0)
            return false;

        if (!Main.pushModal(this.actor))
            return false;
        this._haveModal = true;
        this._modifierMask = primaryModifier(mask);

        this.actor.connect('key-press-event', Lang.bind(this, this._keyPressEvent));
        this.actor.connect('key-release-event', Lang.bind(this, this._keyReleaseEvent));

        this.actor.connect('button-press-event', Lang.bind(this, this._clickedOutside));
        this.actor.connect('scroll-event', Lang.bind(this, this._onScroll));

        this._appSwitcher = new AppSwitcher(localApps, otherApps, this);
        this.actor.add_actor(this._appSwitcher.actor);
        this._appSwitcher.connect('item-activated', Lang.bind(this, this._appActivated));
        this._appSwitcher.connect('item-entered', Lang.bind(this, this._appEntered));

        this._appIcons = this._appSwitcher.icons;

        // Need to force an allocation so we can figure out whether we
        // need to scroll when selecting
        this.actor.opacity = 0;
        this.actor.show();
        this.actor.get_allocation_box();

        // Make the initial selection
        if (binding == 'switch_group') {
            if (backward) {
                this._select(0, this._appIcons[0].cachedWindows.length - 1);
            } else {
                if (this._appIcons[0].cachedWindows.length > 1)
                    this._select(0, 1);
                else
                    this._select(0, 0);
            }
        } else if (binding == 'switch_group_backward') {
            this._select(0, this._appIcons[0].cachedWindows.length - 1);
        } else if (binding == 'switch_windows_backward') {
            this._select(this._appIcons.length - 1);
        } else if (this._appIcons.length == 1) {
            this._select(0);
        } else if (backward) {
            this._select(this._appIcons.length - 1);
        } else {
            this._select(1);
        }

        // There's a race condition; if the user released Alt before
        // we got the grab, then we won't be notified. (See
        // https://bugzilla.gnome.org/show_bug.cgi?id=596695 for
        // details.) So we check now. (Have to do this after updating
        // selection.)
        let [x, y, mods] = global.get_pointer();
        if (!(mods & this._modifierMask)) {
            this._finish();
            return false;
        }

        // We delay showing the popup so that fast Alt+Tab users aren't
        // disturbed by the popup briefly flashing.
        this._initialDelayTimeoutId = Mainloop.timeout_add(POPUP_DELAY_TIMEOUT,
                                                           Lang.bind(this, function () {
                                                               this.actor.opacity = 255;
                                                               this._initialDelayTimeoutId = 0;
                                                           }));

        return true;
    },

    /** Returns the index of the next app (wraps around).
     * @returns {number} index of the next app */
    _nextApp : function() {
        return mod(this._currentApp + 1, this._appIcons.length);
    },

    /** Returns the index of the previous app (wraps around).
     * @returns {number} index of the previous app */
    _previousApp : function() {
        return mod(this._currentApp - 1, this._appIcons.length);
    },

    /** Returns the index of the next window for the current app
     * (wraps around).
     * @returns {number} index of the next window */
    _nextWindow : function() {
        // We actually want the second window if we're in the unset state
        if (this._currentWindow == -1)
            this._currentWindow = 0;
        return mod(this._currentWindow + 1,
                   this._appIcons[this._currentApp].cachedWindows.length);
    },

    /** Returns the index of the previous window for the current app
     * (wraps around).
     * @returns {number} index of the previous window */
    _previousWindow : function() {
        // Also assume second window here
        if (this._currentWindow == -1)
            this._currentWindow = 1;
        return mod(this._currentWindow - 1,
                   this._appIcons[this._currentApp].cachedWindows.length);
    },

    /** Handler for key-press event while the alt tab popup is showing.
     * If the key pressed was:
     * 
     * - Escape: the popup stops showing (destroys itself).
     * - the keybinding for `SWITCH_GROUP`: goes to the next window for the
     *   current group (app) (or previous if Shift was held down).
     * - the keybinding for `SWITCH_GROUP_BACKWARD`: goes to the previous window
     *   for the current group (app)
     * - the keybinding for `SWITCH_WINDOWS`: goes to the next application on
     *   the list (or previous if Shift was held down).
     * - the keybinding for `SWITCH_WINDOWS_BACKWARD`: goes to the previous
     *   application on the list.
     * - direction keys (left, right, up, down): navigates between windows/apps
     *   or shows the windows for the current app/goes back to the app from the
     *   window switcher.
     */
    _keyPressEvent : function(actor, event) {
        let keysym = event.get_key_symbol();
        let event_state = Shell.get_event_state(event);
        let backwards = event_state & Clutter.ModifierType.SHIFT_MASK;
        let action = global.display.get_keybinding_action(event.get_key_code(), event_state);

        this._disableHover();

        if (keysym == Clutter.Escape) {
            this.destroy();
        } else if (action == Meta.KeyBindingAction.SWITCH_GROUP) {
            this._select(this._currentApp, backwards ? this._previousWindow() : this._nextWindow());
        } else if (action == Meta.KeyBindingAction.SWITCH_GROUP_BACKWARD) {
            this._select(this._currentApp, this._previousWindow());
        } else if (action == Meta.KeyBindingAction.SWITCH_WINDOWS) {
            this._select(backwards ? this._previousApp() : this._nextApp());
        } else if (action == Meta.KeyBindingAction.SWITCH_WINDOWS_BACKWARD) {
            this._select(this._previousApp());
        } else if (this._thumbnailsFocused) {
            if (keysym == Clutter.Left)
                this._select(this._currentApp, this._previousWindow());
            else if (keysym == Clutter.Right)
                this._select(this._currentApp, this._nextWindow());
            else if (keysym == Clutter.Up)
                this._select(this._currentApp, null, true);
        } else {
            if (keysym == Clutter.Left)
                this._select(this._previousApp());
            else if (keysym == Clutter.Right)
                this._select(this._nextApp());
            else if (keysym == Clutter.Down)
                this._select(this._currentApp, 0);
        }

        return true;
    },

    /** Handler for key-release-event while the popup is open.
     * If the user stopped holding down the modifier for the popup (i.e.
     * Alt for the alt-tab popup), it hides/destroys the popup.
     */
    _keyReleaseEvent : function(actor, event) {
        let [x, y, mods] = global.get_pointer();
        let state = mods & this._modifierMask;

        if (state == 0)
            this._finish();

        return true;
    },

    /** Handler for the user scolling over the popup.
     * This will scroll through all the windows of an app and on to the next
     * app and so on, allowing the user to quickly select a window to switch to.
     * @see #_select
     */
    _onScroll : function(actor, event) {
        let direction = event.get_scroll_direction();
        if (direction == Clutter.ScrollDirection.UP) {
            if (this._thumbnailsFocused) {
                if (this._currentWindow == 0 || this._currentWindow == -1)
                    this._select(this._previousApp());
                else
                    this._select(this._currentApp, this._previousWindow());
            } else {
                let nwindows = this._appIcons[this._currentApp].cachedWindows.length;
                if (nwindows > 1)
                    this._select(this._currentApp, nwindows - 1);
                else
                    this._select(this._previousApp());
            }
        } else if (direction == Clutter.ScrollDirection.DOWN) {
            if (this._thumbnailsFocused) {
                if (this._currentWindow == this._appIcons[this._currentApp].cachedWindows.length - 1)
                    this._select(this._nextApp());
                else
                    this._select(this._currentApp, this._nextWindow());
            } else {
                let nwindows = this._appIcons[this._currentApp].cachedWindows.length;
                if (nwindows > 1)
                    this._select(this._currentApp, 0);
                else
                    this._select(this._nextApp());
            }
        }
    },

    /** Callback for when the user clicks the mouse (and not on an app/window).
     * Removes/destroys the alt tab popup.
     */
    _clickedOutside : function(actor, event) {
        this.destroy();
    },

    /** called when the user activates an app item (by clicking on it or
     * releasing the keybinding (alt) while over that app. It launches the
     * first window for that app and destroys the alt tab popup.
     */
    _appActivated : function(appSwitcher, n) {
        // If the user clicks on the selected app, activate the
        // selected window; otherwise (eg, they click on an app while
        // !mouseActive) activate the the clicked-on app.
        if (n == this._currentApp) {
            let window;
            if (this._currentWindow >= 0)
                window = this._appIcons[this._currentApp].cachedWindows[this._currentWindow];
            else
                window = null;
            this._appIcons[this._currentApp].app.activate_window(window, global.get_current_time());
        } else {
            this._appIcons[n].app.activate_window(null, global.get_current_time());
        }
        this.destroy();
    },

    /** called when the user "enters" an app item, opening the list of windows
     * for that app.
     * @see #_select
     */
    _appEntered : function(appSwitcher, n) {
        if (!this._mouseActive)
            return;

        this._select(n);
    },

    /** called when the user activates a window by clicking on it or using
     * the keybindings - brings that window to the front and destroys the
     * alt tab popup.
     */
    _windowActivated : function(thumbnailList, n) {
        let appIcon = this._appIcons[this._currentApp];
        Main.activateWindow(appIcon.cachedWindows[n]);
        this.destroy();
    },

    /** called when the user hovers over/navigates to a window (but doesn't
     * activate it).
     * @see #_select
     */
    _windowEntered : function(thumbnailList, n) {
        if (!this._mouseActive)
            return;

        this._select(this._currentApp, n);
    },

    /** Temporarily ignores the mouse pointer for {@link DISABLE_HOVER_TIMEOUT}.
     * For example when you first display the alt-tab popup and if your mouse
     * happens to be over an app icon already, this will be ignored (until you
     * actually move your mouse over an app icon after the popup has already
     * been made).
     */
    _disableHover : function() {
        this._mouseActive = false;

        if (this._motionTimeoutId != 0)
            Mainloop.source_remove(this._motionTimeoutId);

        this._motionTimeoutId = Mainloop.timeout_add(DISABLE_HOVER_TIMEOUT, Lang.bind(this, this._mouseTimedOut));
    },

    /** callback in {@link #_disableHover}. Re-enables listening
     * to mouse events after they've been temporarily disabled by
     * `_disableHover`. */
    _mouseTimedOut : function() {
        this._motionTimeoutId = 0;
        this._mouseActive = true;
    },

    /** Called when the app switcher should remove itself but first activate
     * whatever window the user had selected. (for example when the user holds
     * down Alt+Tab, tabs to the app/window they want to select and then
     * releases Alt, this is called to activate the selected app/window and
     * then destroy the popup). */
    _finish : function() {
        let app = this._appIcons[this._currentApp];
        if (this._currentWindow >= 0) {
            Main.activateWindow(app.cachedWindows[this._currentWindow]);
        } else {
            app.app.activate_window(null, global.get_current_time());
        }
        this.destroy();
    },

    /** calls {@link Main.popModal} on the actor to remove it from the modal
     * stack */
    _popModal: function() {
        if (this._haveModal) {
            Main.popModal(this.actor);
            this._haveModal = false;
        }
    },

    /** destroys the alt-tab popup. First pops the actor from the modal stack
     * ({@link #_popModal}), then fades it out
     * ({@link POPUP_FADE_OUT_TIME}), and finally destroys the actor.
     */
    destroy : function() {
        this._popModal();
        if (this.actor.visible) {
            Tweener.addTween(this.actor,
                             { opacity: 0,
                               time: POPUP_FADE_OUT_TIME,
                               transition: 'easeOutQuad',
                               onComplete: Lang.bind(this,
                                   function() {
                                       this.actor.destroy();
                                   })
                             });
        } else
            this.actor.destroy();
    },

    /** callback for when {@link #actor} is destroyed. Destroys the popup's
     * list of apps/thumbnails, disconnects any signals/timeouts remaining. */
    _onDestroy : function() {
        this._popModal();

        if (this._thumbnails)
            this._destroyThumbnails();

        if (this._motionTimeoutId != 0)
            Mainloop.source_remove(this._motionTimeoutId);
        if (this._thumbnailTimeoutId != 0)
            Mainloop.source_remove(this._thumbnailTimeoutId);
        if (this._initialDelayTimeoutId != 0)
            Mainloop.source_remove(this._initialDelayTimeoutId);
    },

    /**
     * The main function of the alt-tab popup.
     *
     * Selects the indicated `app`, and optional `window`, and sets
     * `this._thumbnailsFocused` appropriately to indicate whether the
     * arrow keys should act on the app list or the thumbnail list.
     *
     * If `app` is specified and `window` is unspecified or `null`, then
     * the app is highlighted (ie, given a light background), and the
     * current thumbnail list, if any, is destroyed. If `app` has
     * multiple windows, and `forceAppFocus` is not `true`, then a
     * timeout is started to open a thumbnail list.
     *
     * If `app` and `window` are specified (and `forceAppFocus` is not),
     * then `app` will be outlined, a thumbnail list will be created
     * and focused (if it hasn't been already), and the `window`'th
     * window in it will be highlighted.
     *
     * If `app` and `window` are specified and `forceAppFocus` is `true`,
     * then `app` will be highlighted, and `window` outlined, and the
     * app list will have the keyboard focus.
     *
     * @param {int} app - index of the app to select
     * @param {int} [window] - index of which of `app`'s windows to select
     * @param {boolean} [forceAppFocus] - optional flag, see the description.
     */
    _select : function(app, window, forceAppFocus) {
        if (app != this._currentApp || window == null) {
            if (this._thumbnails)
                this._destroyThumbnails();
        }

        if (this._thumbnailTimeoutId != 0) {
            Mainloop.source_remove(this._thumbnailTimeoutId);
            this._thumbnailTimeoutId = 0;
        }

        this._thumbnailsFocused = (window != null) && !forceAppFocus;

        this._currentApp = app;
        this._currentWindow = window ? window : -1;
        this._appSwitcher.highlight(app, this._thumbnailsFocused);

        if (window != null) {
            if (!this._thumbnails)
                this._createThumbnails();
            this._currentWindow = window;
            this._thumbnails.highlight(window, forceAppFocus);
        } else if (this._appIcons[this._currentApp].cachedWindows.length > 1 &&
                   !forceAppFocus) {
            this._thumbnailTimeoutId = Mainloop.timeout_add (
                THUMBNAIL_POPUP_TIME,
                Lang.bind(this, this._timeoutPopupThumbnails));
        }
    },

    /** callback when window thumbnails are created after a delay of 
     * {@link THUMBNAIL_POPUP_TIME}. Creates the thumbnails but retains focus
     * to the app switcher. */
    _timeoutPopupThumbnails: function() {
        if (!this._thumbnails)
            this._createThumbnails();
        this._thumbnailTimeoutId = 0;
        this._thumbnailsFocused = false;
        return false;
    },

    /** destroys the window thumbnails (fades them out with
     * {@link THUMBNAIL_FADE_TIME} before destroying the window/thumbnail
     * switcher). */
    _destroyThumbnails : function() {
        let thumbnailsActor = this._thumbnails.actor;
        Tweener.addTween(thumbnailsActor,
                         { opacity: 0,
                           time: THUMBNAIL_FADE_TIME,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this, function() {
                                                            thumbnailsActor.destroy();
                                                            this.thumbnailsVisible = false;
                                                        })
                         });
        this._thumbnails = null;
    },

    /** creates the window switcher (containing window thumbnails) for the
     * current-selected app in the alt-tab popup. Fades them in using
     * {@link THUMBNAIL_FADE_TIME} */
    _createThumbnails : function() {
        this._thumbnails = new ThumbnailList (this._appIcons[this._currentApp].cachedWindows);
        this._thumbnails.connect('item-activated', Lang.bind(this, this._windowActivated));
        this._thumbnails.connect('item-entered', Lang.bind(this, this._windowEntered));

        this.actor.add_actor(this._thumbnails.actor);

        // Need to force an allocation so we can figure out whether we
        // need to scroll when selecting
        this._thumbnails.actor.get_allocation_box();

        this._thumbnails.actor.opacity = 0;
        Tweener.addTween(this._thumbnails.actor,
                         { opacity: 255,
                           time: THUMBNAIL_FADE_TIME,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this, function () { this.thumbnailsVisible = true; })
                         });
    }
};

/**
 * constructs a switcher list.
 * @param {boolean} squareItems - whether the items in the list should be square
 * in dimensions.
 * @classdesc The SwitcherList is a class defining a list of items to switch
 * between. It handles the allocation of sizes to its children. Its top-level
 * actor is a `Shell.GenericContainer` (`this.actor`), but the container that
 * actually manages allocation of sizes to the items in the list is a
 * `Shell.GenericContainer` stored as `this._list`. The difference is that
 * `this._list` contains *every* item in the switcher list, but `this.actor`
 * *clips* `this._list` to the size of the popup.
 * Both the app switcher and per-app window switcher in the {@link AltTabPopup}
 * are examples of these.
 * @see AppSwitcher
 * @see ThumbnailList
 * @class
 */
function SwitcherList(squareItems) {
    this._init(squareItems);
}

SwitcherList.prototype = {
    _init : function(squareItems) {
        /** this.actor for the SwitcherList is a `Shell.GenericContainer`. Think
         * of this as the parent container object */
        this.actor = new Shell.GenericContainer({ style_class: 'switcher-list' });
        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocateTop));

        // Here we use a GenericContainer so that we can force all the
        // children except the separator to have the same width.
        /** `this.actor` *contains* another `Shell.GenericContainer`
         * `this._list` which is what actually holds all the child items
         * (it is the job of `this.actor` to clip `this._list` to whatever
         * width is available */
        this._list = new Shell.GenericContainer({ style_class: 'switcher-list-item-container' });
        this._list.spacing = 0;
        this._list.connect('style-changed', Lang.bind(this, function() {
                                                        this._list.spacing = this._list.get_theme_node().get_length('spacing');
                                                     }));

        this._list.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this._list.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this._list.connect('allocate', Lang.bind(this, this._allocate));

        this._clipBin = new St.Bin({style_class: 'cbin'});
        this._clipBin.child = this._list;
        this.actor.add_actor(this._clipBin);

        this._leftGradient = new St.BoxLayout({style_class: 'thumbnail-scroll-gradient-left', vertical: true});
        this._rightGradient = new St.BoxLayout({style_class: 'thumbnail-scroll-gradient-right', vertical: true});
        this.actor.add_actor(this._leftGradient);
        this.actor.add_actor(this._rightGradient);

        // Those arrows indicate whether scrolling in one direction is possible
        this._leftArrow = new St.DrawingArea({ style_class: 'switcher-arrow',
                                               pseudo_class: 'highlighted' });
        this._leftArrow.connect('repaint', Lang.bind(this,
            function() { _drawArrow(this._leftArrow, St.Side.LEFT); }));
        this._rightArrow = new St.DrawingArea({ style_class: 'switcher-arrow',
                                                pseudo_class: 'highlighted' });
        this._rightArrow.connect('repaint', Lang.bind(this,
            function() { _drawArrow(this._rightArrow, St.Side.RIGHT); }));

        this.actor.add_actor(this._leftArrow);
        this.actor.add_actor(this._rightArrow);

        this._items = [];
        this._highlighted = -1;
        this._separator = null;
        this._squareItems = squareItems;
        this._minSize = 0;
        this._scrollableRight = true;
        this._scrollableLeft = false;
    },

    /** callback for the `allocate` signal of `this.actor`. Makes sure each
     * child element gets the same size. */
    _allocateTop: function(actor, box, flags) {
        let leftPadding = this.actor.get_theme_node().get_padding(St.Side.LEFT);
        let rightPadding = this.actor.get_theme_node().get_padding(St.Side.RIGHT);

        let childBox = new Clutter.ActorBox();
        let scrollable = this._minSize > box.x2 - box.x1;

        this._clipBin.allocate(box, flags);

        childBox.x1 = 0;
        childBox.y1 = 0;
        childBox.x2 = this._leftGradient.width;
        childBox.y2 = this.actor.height;
        this._leftGradient.allocate(childBox, flags);
        this._leftGradient.opacity = (this._scrollableLeft && scrollable) ? 255 : 0;

        childBox.x1 = (this.actor.allocation.x2 - this.actor.allocation.x1) - this._rightGradient.width;
        childBox.y1 = 0;
        childBox.x2 = childBox.x1 + this._rightGradient.width;
        childBox.y2 = this.actor.height;
        this._rightGradient.allocate(childBox, flags);
        this._rightGradient.opacity = (this._scrollableRight && scrollable) ? 255 : 0;

        let arrowWidth = Math.floor(leftPadding / 3);
        let arrowHeight = arrowWidth * 2;
        childBox.x1 = leftPadding / 2;
        childBox.y1 = this.actor.height / 2 - arrowWidth;
        childBox.x2 = childBox.x1 + arrowWidth;
        childBox.y2 = childBox.y1 + arrowHeight;
        this._leftArrow.allocate(childBox, flags);
        this._leftArrow.opacity = this._leftGradient.opacity;

        arrowWidth = Math.floor(rightPadding / 3);
        arrowHeight = arrowWidth * 2;
        childBox.x1 = this.actor.width - arrowWidth - rightPadding / 2;
        childBox.y1 = this.actor.height / 2 - arrowWidth;
        childBox.x2 = childBox.x1 + arrowWidth;
        childBox.y2 = childBox.y1 + arrowHeight;
        this._rightArrow.allocate(childBox, flags);
        this._rightArrow.opacity = this._rightGradient.opacity;
    },

    /** Adds an item to the switcher.
     * @param {Clutter.Actor|St.Widget} item - item to add to the switcher. It
     * will be added to a `St.Button` via `.set_child`, so basically it should
        * be `Clutter.Actor` at heart (this includes `St` classes).
     * @param {St.Label|Clutter.Text} label - the label that defines this widget
     * (but it is not displayed.... just set as a `label_actor`? I think it
     * is for accessibility purposes.)
     */
    addItem : function(item, label) {
        let bbox = new St.Button({ style_class: 'item-box',
                                   reactive: true });

        bbox.set_child(item);
        this._list.add_actor(bbox);

        let n = this._items.length;
        bbox.connect('clicked', Lang.bind(this, function() { this._onItemClicked(n); }));
        bbox.connect('enter-event', Lang.bind(this, function() { this._onItemEnter(n); }));

        bbox.label_actor = label;

        this._items.push(bbox);
    },

    /** callback when an item is clicked.
     * @param {int} index - index of the item that has been clicked.
     * @see #_itemActivated.
     */
    _onItemClicked: function (index) {
        this._itemActivated(index);
    },

    /** callback when an item is entered (hovering over it, i.e. an
     * enter-event).
     * @param {int} index - index of the item that has been entered.
     * @see #_itemEntered.
     */
    _onItemEnter: function (index) {
        this._itemEntered(index);
    },

    /** adds a separator to the switcher. For example, the one separating
     * apps on the current vs. other workspaces in the {@link AltTabPopup}.
     * Example in the screenshot above (the vertical line).
     */
    addSeparator: function () {
        let box = new St.Bin({ style_class: 'separator' });
        this._separator = box;
        this._list.add_actor(box);
    },

    /** highlights an item by outlining it or changing its background colour to
     * give the "selected"/"hovered over" appearance.
     *
     * This basically just adds the style class 'outlined' (if `justOutline` is
     * `true`) or `selected` (if `justOutline` is `false`).
     *
     * @param {int} index - index of the item to highlight
     * @param {boolean} justOutline - whether to just outline the item, or
     * highlight the whole thing.
     */
    highlight: function(index, justOutline) {
        if (this._highlighted != -1) {
            this._items[this._highlighted].remove_style_pseudo_class('outlined');
            this._items[this._highlighted].remove_style_pseudo_class('selected');
        }

        this._highlighted = index;

        if (this._highlighted != -1) {
            if (justOutline)
                this._items[this._highlighted].add_style_pseudo_class('outlined');
            else
                this._items[this._highlighted].add_style_pseudo_class('selected');
        }

        let [absItemX, absItemY] = this._items[index].get_transformed_position();
        let [result, posX, posY] = this.actor.transform_stage_point(absItemX, 0);
        let [containerWidth, containerHeight] = this.actor.get_transformed_size();
        if (posX + this._items[index].get_width() > containerWidth)
            this._scrollToRight();
        else if (posX < 0)
            this._scrollToLeft();

    },

    /** scrolls the switcher list to the left (if you are trying to select/
     * highlight an item that is not currently being displayed due to the list
     * being too wide for the screen).
     * @see POPUP_SCROLL_TIME
     */
    _scrollToLeft : function() {
        let x = this._items[this._highlighted].allocation.x1;
        this._scrollableRight = true;
        Tweener.addTween(this._list, { anchor_x: x,
                                        time: POPUP_SCROLL_TIME,
                                        transition: 'easeOutQuad',
                                        onComplete: Lang.bind(this, function () {
                                                                        if (this._highlighted == 0) {
                                                                            this._scrollableLeft = false;
                                                                            this.actor.queue_relayout();
                                                                        }
                                                             })
                        });
    },

    /** scrolls the switcher list to the right (if you are trying to select/
     * highlight an item that is not currently being displayed due to the list
     * being too wide for the screen).
     * @see POPUP_SCROLL_TIME
     */
    _scrollToRight : function() {
        this._scrollableLeft = true;
        let monitor = Main.layoutManager.primaryMonitor;
        let padding = this.actor.get_theme_node().get_horizontal_padding();
        let parentPadding = this.actor.get_parent().get_theme_node().get_horizontal_padding();
        let x = this._items[this._highlighted].allocation.x2 - monitor.width + padding + parentPadding;
        Tweener.addTween(this._list, { anchor_x: x,
                                        time: POPUP_SCROLL_TIME,
                                        transition: 'easeOutQuad',
                                        onComplete: Lang.bind(this, function () {
                                                                        if (this._highlighted == this._items.length - 1) {
                                                                            this._scrollableRight = false;
                                                                            this.actor.queue_relayout();
                                                                        }
                                                             })
                        });
    },

    /** called when an item in the list is activated (when the item is clicked).
     * @param {int} n - index of the item activated.
     * This just emits the signal 'item-activated' with the index of the
     * activated item; it is up to the class that uses this on to connect
     * to that and perform the appropriate action (such as activating a window
     * in the case of the {@link AltTabPopup}).
     */
    _itemActivated: function(n) {
        this.emit('item-activated', n);
    },

    /** called when an item in the list is entered (it records an enter-event).
     * @param {int} n - index of the item entered.
     * This just emits the signal 'item-entered' with the index of the
     * entered item; it is up to the class that uses this on to connect
     * to that and perform the appropriate action (like calling
     * {@link #highlight} on the item).
     */
    _itemEntered: function(n) {
        this.emit('item-entered', n);
    },

    /** calculates the maximum child (item in the list) width for a given
     * height. Used in allocation calculations to make sure each child gets
     * allocated the same width.
     * @param {number} forHeight - height to calculate the maximum width for
     * @returns {number, number} `[maxChildMin, maxChildNat]` being the maximum
     * minimum height of all the items in the list, and the maximum *natural*
     * height of all the items in the list.
     */
    _maxChildWidth: function (forHeight) {
        let maxChildMin = 0;
        let maxChildNat = 0;

        for (let i = 0; i < this._items.length; i++) {
            let [childMin, childNat] = this._items[i].get_preferred_width(forHeight);
            maxChildMin = Math.max(childMin, maxChildMin);
            maxChildNat = Math.max(childNat, maxChildNat);

            if (this._squareItems) {
                let [childMin, childNat] = this._items[i].get_preferred_height(-1);
                maxChildMin = Math.max(childMin, maxChildMin);
                maxChildNat = Math.max(childNat, maxChildNat);
            }
        }

        return [maxChildMin, maxChildNat];
    },

    /** callback for the `get-preferred-width` signal of `this._list` *and*
     * `this.actor`.
     * Returns a width request being
     *
     *     itemSpacing * (n - 1) + childWidth * n + separatorWidth * m,
     *
     * where:
     *
     * - `itemSpacing` is the spacing between each item in the list,
     * - `n` is the number of items in the list,
     * - `childWidth` is the maximum minimum height of al the list items (see
     *   {@link #_maxChildWidth}),
     * - `separatorWidth` is the width of any separators in the list (see
     *   {@link #addSeparator}), and
     * - `m` is the number of separators in the list.
     *
     * This might be larger than the width of the screen/popup; it is the duty
     * of `this.actor` to clip the items displayed if the returned width is too
     * large (see {@link #_allocateTop}).
     */
    _getPreferredWidth: function (actor, forHeight, alloc) {
        let [maxChildMin, maxChildNat] = this._maxChildWidth(forHeight);

        let separatorWidth = 0;
        if (this._separator) {
            let [sepMin, sepNat] = this._separator.get_preferred_width(forHeight);
            separatorWidth = sepNat + this._list.spacing;
        }

        let totalSpacing = this._list.spacing * (this._items.length - 1);
        alloc.min_size = this._items.length * maxChildMin + separatorWidth + totalSpacing;
        alloc.natural_size = alloc.min_size;
        this._minSize = alloc.min_size;
    },

    /** callback for the `get-preferred-height` signal of `this._list` *and*
     * `this.actor`. */
    _getPreferredHeight: function (actor, forWidth, alloc) {
        let maxChildMin = 0;
        let maxChildNat = 0;

        for (let i = 0; i < this._items.length; i++) {
            let [childMin, childNat] = this._items[i].get_preferred_height(-1);
            maxChildMin = Math.max(childMin, maxChildMin);
            maxChildNat = Math.max(childNat, maxChildNat);
        }

        if (this._squareItems) {
            let [childMin, childNat] = this._maxChildWidth(-1);
            maxChildMin = Math.max(childMin, maxChildMin);
            maxChildNat = maxChildMin;
        }

        alloc.min_size = maxChildMin;
        alloc.natural_size = maxChildNat;
    },

    /** callback for the `allocate` signal of `this._list`. Allocates square
     * dimensions to its items if the user set `squareItems=true` in the
     * constructor {@link #_init}.
     */
    _allocate: function (actor, box, flags) {
        let childHeight = box.y2 - box.y1;

        let [maxChildMin, maxChildNat] = this._maxChildWidth(childHeight);
        let totalSpacing = this._list.spacing * (this._items.length - 1);

        let separatorWidth = 0;
        if (this._separator) {
            let [sepMin, sepNat] = this._separator.get_preferred_width(childHeight);
            separatorWidth = sepNat;
            totalSpacing += this._list.spacing;
        }

        let childWidth = Math.floor(Math.max(0, box.x2 - box.x1 - totalSpacing - separatorWidth) / this._items.length);

        let x = 0;
        let children = this._list.get_children();
        let childBox = new Clutter.ActorBox();

        let primary = Main.layoutManager.primaryMonitor;
        let parentRightPadding = this.actor.get_parent().get_theme_node().get_padding(St.Side.RIGHT);
        if (this.actor.allocation.x2 == primary.x + primary.width - parentRightPadding) {
            if (this._squareItems)
                childWidth = childHeight;
            else {
                let [childMin, childNat] = children[0].get_preferred_width(childHeight);
                childWidth = childMin;
            }
        }

        for (let i = 0; i < children.length; i++) {
            if (this._items.indexOf(children[i]) != -1) {
                let [childMin, childNat] = children[i].get_preferred_height(childWidth);
                let vSpacing = (childHeight - childNat) / 2;
                childBox.x1 = x;
                childBox.y1 = vSpacing;
                childBox.x2 = x + childWidth;
                childBox.y2 = childBox.y1 + childNat;
                children[i].allocate(childBox, flags);

                x += this._list.spacing + childWidth;
            } else if (children[i] == this._separator) {
                // We want the separator to be more compact than the rest.
                childBox.x1 = x;
                childBox.y1 = 0;
                childBox.x2 = x + separatorWidth;
                childBox.y2 = childHeight;
                children[i].allocate(childBox, flags);
                x += this._list.spacing + separatorWidth;
            } else {
                // Something else, eg, AppSwitcher's arrows;
                // we don't allocate it.
            }
        }

        let leftPadding = this.actor.get_theme_node().get_padding(St.Side.LEFT);
        let rightPadding = this.actor.get_theme_node().get_padding(St.Side.RIGHT);
        let topPadding = this.actor.get_theme_node().get_padding(St.Side.TOP);
        let bottomPadding = this.actor.get_theme_node().get_padding(St.Side.BOTTOM);

        // Clip the area for scrolling
        this._clipBin.set_clip(0, -topPadding, (this.actor.allocation.x2 - this.actor.allocation.x1) - leftPadding - rightPadding, this.actor.height + bottomPadding);
    }
};

Signals.addSignalMethods(SwitcherList.prototype);

/** creates a new AppIcon
 * @param {Shell.App} app - the app for which the icon is for.
 * @classdesc This class defines an icon for a particular app with a label being
 * the app's name.
 *
 * ![AppIcons in the Alt Tab Popup are in green](pics/altTabPopup.png)
 *
 * The app icons are outlined in green in the screenshot above (showing the
 * {@link AltTabPopup}).
 * @class
 */
function AppIcon(app) {
    this._init(app);
}

AppIcon.prototype = {
    _init: function(app) {
        this.app = app;
        this.actor = new St.BoxLayout({ style_class: 'alt-tab-app',
                                         vertical: true });
        this.icon = null;
        this._iconBin = new St.Bin({ x_fill: true, y_fill: true });

        this.actor.add(this._iconBin, { x_fill: false, y_fill: false } );
        this.label = new St.Label({ text: this.app.get_name() });
        this.actor.add(this.label, { x_fill: false });
    },

    /** Set the size of the app icon.
     * @param {number} size - new size of the app icon.
     */
    set_size: function(size) {
        this.icon = this.app.create_icon_texture(size);
        this._iconBin.set_size(size, size);
        this._iconBin.child = this.icon;
    }
};

/** creates a new AppSwitcher.
 * @classdesc This is a {@link SwitcherList} specific to the app switcher in the
 * alt-tab popup. It basically has an input set of apps and creates
 * {@link AppIcon}s for each of them and adds these to itself.
 *
 * ![AppSwitcher in the Alt Tab Popup](pics/appSwitcher.png)
 *
 * The AppSwitcher is the top one of the two in the screenshot above (looks
 * ugly because the {@link AppIcon}s are outlined in green).
 * @extends SwitcherList
 * @param {Shell.App[]} localApps - list of apps in this workspace.
 * @param {Shell.App[]} otherApps - list of apps on other workspaces.
 * @param {AltTab.AltTabPopup} altTabPopup - an instance of an {@link AltTabPopup}.
 * @class
 */
function AppSwitcher() {
    this._init.apply(this, arguments);
}

AppSwitcher.prototype = {
    __proto__ : SwitcherList.prototype,

    _init : function(localApps, otherApps, altTabPopup) {
        SwitcherList.prototype._init.call(this, true);

        // Construct the AppIcons, add to the popup
        let activeWorkspace = global.screen.get_active_workspace();
        let workspaceIcons = [];
        let otherIcons = [];
        for (let i = 0; i < localApps.length; i++) {
            let appIcon = new AppIcon(localApps[i]);
            // Cache the window list now; we don't handle dynamic changes here,
            // and we don't want to be continually retrieving it
            appIcon.cachedWindows = appIcon.app.get_windows();
            workspaceIcons.push(appIcon);
        }
        for (let i = 0; i < otherApps.length; i++) {
            let appIcon = new AppIcon(otherApps[i]);
            appIcon.cachedWindows = appIcon.app.get_windows();
            otherIcons.push(appIcon);
        }

        this.icons = [];
        this._arrows = [];
        for (let i = 0; i < workspaceIcons.length; i++)
            this._addIcon(workspaceIcons[i]);
        if (workspaceIcons.length > 0 && otherIcons.length > 0)
            this.addSeparator();
        for (let i = 0; i < otherIcons.length; i++)
            this._addIcon(otherIcons[i]);

        this._curApp = -1;
        this._iconSize = 0;
        this._altTabPopup = altTabPopup;
        this._mouseTimeOutId = 0;
    },

    /** Overrides the parent `_getPreferredHeight` to set the height of the
     * app switcher (`this._list` and `this.actor`). What is different is that
     * the icon heights are chosen from {@link iconSizes} - the maximum such
     * that the icons will still fit widthwise into the popup.
     * @override
     */
    _getPreferredHeight: function (actor, forWidth, alloc) {
        let j = 0;
        while(this._items.length > 1 && this._items[j].style_class != 'item-box') {
                j++;
        }
        let themeNode = this._items[j].get_theme_node();
        let iconPadding = themeNode.get_horizontal_padding();
        let iconBorder = themeNode.get_border_width(St.Side.LEFT) + themeNode.get_border_width(St.Side.RIGHT);
        let [iconMinHeight, iconNaturalHeight] = this.icons[j].label.get_preferred_height(-1);
        let iconSpacing = iconNaturalHeight + iconPadding + iconBorder;
        let totalSpacing = this._list.spacing * (this._items.length - 1);
        if (this._separator)
           totalSpacing += this._separator.width + this._list.spacing;

        // We just assume the whole screen here due to weirdness happing with the passed width
        let primary = Main.layoutManager.primaryMonitor;
        let parentPadding = this.actor.get_parent().get_theme_node().get_horizontal_padding();
        let availWidth = primary.width - parentPadding - this.actor.get_theme_node().get_horizontal_padding();
        let height = 0;

        for(let i =  0; i < iconSizes.length; i++) {
                this._iconSize = iconSizes[i];
                height = iconSizes[i] + iconSpacing;
                let w = height * this._items.length + totalSpacing;
                if (w <= availWidth)
                        break;
        }

        if (this._items.length == 1) {
            this._iconSize = iconSizes[0];
            height = iconSizes[0] + iconSpacing;
        }

        for(let i = 0; i < this.icons.length; i++) {
            if (this.icons[i].icon != null)
                break;
            this.icons[i].set_size(this._iconSize);
        }

        alloc.min_size = height;
        alloc.natural_size = height;
    },

    /** Overrides the parent `_allocate` (allocation of `this._list`).
     * Additionally allocates the arrow underneath an app that indicates an
     * app has multiple windows open on the workspace in the alt-tab popup.
     * (example in screenshot above).
     * @override
     */
    _allocate: function (actor, box, flags) {
        // Allocate the main list items
        SwitcherList.prototype._allocate.call(this, actor, box, flags);

        let arrowHeight = Math.floor(this.actor.get_theme_node().get_padding(St.Side.BOTTOM) / 3);
        let arrowWidth = arrowHeight * 2;

        // Now allocate each arrow underneath its item
        let childBox = new Clutter.ActorBox();
        for (let i = 0; i < this._items.length; i++) {
            let itemBox = this._items[i].allocation;
            childBox.x1 = Math.floor(itemBox.x1 + (itemBox.x2 - itemBox.x1 - arrowWidth) / 2);
            childBox.x2 = childBox.x1 + arrowWidth;
            childBox.y1 = itemBox.y2 + arrowHeight;
            childBox.y2 = childBox.y1 + arrowHeight;
            this._arrows[i].allocate(childBox, flags);
        }
    },

    /** Overrides the parent method to delay entering an item while the
     * thumbnail list is open (see {@link APP_ICON_HOVER_TIMEOUT}).
     * @override
     */
    _onItemEnter: function (index) {
        if (this._mouseTimeOutId != 0)
            Mainloop.source_remove(this._mouseTimeOutId);
        if (this._altTabPopup.thumbnailsVisible) {
            this._mouseTimeOutId = Mainloop.timeout_add(APP_ICON_HOVER_TIMEOUT,
                                                        Lang.bind(this, function () {
                                                                            this._enterItem(index);
                                                                            this._mouseTimeOutId = 0;
                                                                            return false;
                                                        }));
        } else
           this._itemEntered(index);
    },

    /** callback to the delayed entering of an item while the
     * thumbnail list is open (see {@link #_onItemEnter}); merely
     * checks that after te timeout has occurred, the pointer is still over the
     * item that was going to be entered (and calls `itemEntered` on it).
     */
    _enterItem: function(index) {
        let [x, y, mask] = global.get_pointer();
        let pickedActor = global.stage.get_actor_at_pos(Clutter.PickMode.ALL, x, y);
        if (this._items[index].contains(pickedActor))
            this._itemEntered(index);
    },

    /** Overrides the parent method to also deal with the
     * AppSwitcher->ThumbnailList arrows. Apps with only 1 window
     * will hide their arrows by default, but show them when their
     * thumbnails are visible (ie, when the app icon is supposed to be
     * in justOutline mode). Apps with multiple windows will normally
     * show a dim arrow, but show a bright arrow when they are
     * highlighted.
     * @override
     */
    highlight : function(n, justOutline) {
        if (this._curApp != -1) {
            if (this.icons[this._curApp].cachedWindows.length == 1)
                this._arrows[this._curApp].hide();
            else
                this._arrows[this._curApp].remove_style_pseudo_class('highlighted');
        }

        SwitcherList.prototype.highlight.call(this, n, justOutline);
        this._curApp = n;

        if (this._curApp != -1) {
            if (justOutline && this.icons[this._curApp].cachedWindows.length == 1)
                this._arrows[this._curApp].show();
            else
                this._arrows[this._curApp].add_style_pseudo_class('highlighted');
        }
    },

    /** adds an icon to the app switcher along with its down arrow */
    _addIcon : function(appIcon) {
        this.icons.push(appIcon);
        this.addItem(appIcon.actor, appIcon.label);

        let n = this._arrows.length;
        let arrow = new St.DrawingArea({ style_class: 'switcher-arrow' });
        arrow.connect('repaint', function() { _drawArrow(arrow, St.Side.BOTTOM); });
        this._list.add_actor(arrow);
        this._arrows.push(arrow);

        if (appIcon.cachedWindows.length == 1)
            arrow.hide();
    }
};

/** creates a new ThumbnailList
 * @classdesc The ThumbnailList is a {@link SwitcherList} that shows the list
 * of windows (their thumbnails) in the alt-tab popup when an app with multiple
 * windows is selected.
 *
 * ![Alt Tab Popup's `ThumbnailList` displays window thumbnails](pics/AltTabPopupThumbnailList.png)
 *
 * The ThumbnailList is the bottom one of the two in the screenshot above (looks
 * ugly because the {@link AppIcon}s are outlined in green).
 * @extends SwitcherList
 * @param {Meta.Window[]} windows - array of windows to be in the thumbnail list
 * @class
 */
function ThumbnailList(windows) {
    this._init(windows);
}

ThumbnailList.prototype = {
    __proto__ : SwitcherList.prototype,

    _init : function(windows) {
        SwitcherList.prototype._init.call(this);

        let activeWorkspace = global.screen.get_active_workspace();

        // We fake the value of 'separatorAdded' when the app has no window
        // on the current workspace, to avoid displaying a useless separator in
        // that case.
        let separatorAdded = windows.length == 0 || windows[0].get_workspace() != activeWorkspace;

        this._labels = new Array();
        this._thumbnailBins = new Array();
        this._clones = new Array();
        this._windows = windows;

        for (let i = 0; i < windows.length; i++) {
            if (!separatorAdded && windows[i].get_workspace() != activeWorkspace) {
              this.addSeparator();
              separatorAdded = true;
            }

            let box = new St.BoxLayout({ style_class: 'thumbnail-box',
                                         vertical: true });

            let bin = new St.Bin({ style_class: 'thumbnail' });

            box.add_actor(bin);
            this._thumbnailBins.push(bin);

            let title = windows[i].get_title();
            if (title) {
                let name = new St.Label({ text: title });
                // St.Label doesn't support text-align so use a Bin
                let bin = new St.Bin({ x_align: St.Align.MIDDLE });
                this._labels.push(bin);
                bin.add_actor(name);
                box.add_actor(bin);

                this.addItem(box, name);
            } else {
                this.addItem(box, null);
            }

        }
    },

    /** adds the thumbnails for the current windows to the list.
     * The thumbnails are `Clutter.Clone`s of each window's texture.
     * @param {number} availHeight - available height in the switcher (this is
     * used to determine thumbnail size, see also
     * {@link THUMBNAIL_DEFAULT_SIZE}).
     */
    addClones : function (availHeight) {
        if (!this._thumbnailBins.length)
            return;
        let totalPadding = this._items[0].get_theme_node().get_horizontal_padding() + this._items[0].get_theme_node().get_vertical_padding();
        totalPadding += this.actor.get_theme_node().get_horizontal_padding() + this.actor.get_theme_node().get_vertical_padding();
        let [labelMinHeight, labelNaturalHeight] = this._labels[0].get_preferred_height(-1);
        let spacing = this._items[0].child.get_theme_node().get_length('spacing');

        availHeight = Math.min(availHeight - labelNaturalHeight - totalPadding - spacing, THUMBNAIL_DEFAULT_SIZE);
        let binHeight = availHeight + this._items[0].get_theme_node().get_vertical_padding() + this.actor.get_theme_node().get_vertical_padding() - spacing;
        binHeight = Math.min(THUMBNAIL_DEFAULT_SIZE, binHeight);

        for (let i = 0; i < this._thumbnailBins.length; i++) {
            let mutterWindow = this._windows[i].get_compositor_private();
            if (!mutterWindow)
                continue;

            let windowTexture = mutterWindow.get_texture ();
            let [width, height] = windowTexture.get_size();
            let scale = Math.min(1.0, THUMBNAIL_DEFAULT_SIZE / width, availHeight / height);
            let clone = new Clutter.Clone ({ source: windowTexture,
                                                reactive: true,
                                                width: width * scale,
                                                height: height * scale });

            this._thumbnailBins[i].set_height(binHeight);
            this._thumbnailBins[i].add_actor(clone);
            this._clones.push(clone);
        }

        // Make sure we only do this once
        this._thumbnailBins = new Array();
    }
};

/** Function to draw the arrow on an app icon in the alt-tab popup (indicating
 * that the app has multiple windows that can be browsed through), and also the
 * arrows that get shown when a SwitcherList is doesn't fit on the screen and
 * needs to be scrolled through to show all the contents.
 * @param {St.DrawingArea} area - the St.DrawingArea that needs to be painted.
 * @param {St.Side} side - the direction that the arrow points (`TOP`, `BOTTOM`,
 * `LEFT` or `RIGHT`).
 */
function _drawArrow(area, side) {
    let themeNode = area.get_theme_node();
    let borderColor = themeNode.get_border_color(side);
    let bodyColor = themeNode.get_foreground_color();

    let [width, height] = area.get_surface_size ();
    let cr = area.get_context();

    cr.setLineWidth(1.0);
    Clutter.cairo_set_source_color(cr, borderColor);

    switch (side) {
    case St.Side.TOP:
        cr.moveTo(0, height);
        cr.lineTo(Math.floor(width * 0.5), 0);
        cr.lineTo(width, height);
        break;

    case St.Side.BOTTOM:
        cr.moveTo(width, 0);
        cr.lineTo(Math.floor(width * 0.5), height);
        cr.lineTo(0, 0);
        break;

    case St.Side.LEFT:
        cr.moveTo(width, height);
        cr.lineTo(0, Math.floor(height * 0.5));
        cr.lineTo(width, 0);
        break;

    case St.Side.RIGHT:
        cr.moveTo(0, 0);
        cr.lineTo(width, Math.floor(height * 0.5));
        cr.lineTo(0, height);
        break;
    }

    cr.strokePreserve();

    Clutter.cairo_set_source_color(cr, bodyColor);
    cr.fill();
}
