// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file handles the display and searching of contacts in the Overview.
 */

const Folks = imports.gi.Folks
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Util = imports.misc.util;
const IconGrid = imports.ui.iconGrid;
const Search = imports.ui.search;
const SearchDisplay = imports.ui.searchDisplay;

/** @+
 * @default
 * @const
 * @type {number} */
/** Maximum number of rows of results returned in the Overview when searching
 * for contacts.
 * @see ContactSearchProvider */
const MAX_SEARCH_RESULTS_ROWS = 1;
/** Default icon size for a contact's avatar.
 * @see Contact */
const ICON_SIZE = 81;
/** @- */

/** Launches a contact by ID in `gnome-contacts`.
 * @param {string} id - contact ID. This is an ID returned by the underlying
 * `Shell.ContactSystem`/{@link Folks.IndividualAggregator}
 * and is a unique string (like 'b191eef6967fd9eb8dce4f260d4ff4233a030d92'),
 * not anything human-readable.
 */
function launchContact(id) {
    Util.spawn(['gnome-contacts', '-i', id]);
}


/** Creates a new Contact.
 *
 * This involves first creating {@link #individual}, the
 * {@link Folks.Individual}
 * representing the contact.
 *
 * The main actor of the contact ({@link #actor}) is a `St.Bin` with
 * various BoxLayouts nested inside it.
 *
 * Displayed in the actor are;
 *
 * * the individual's avatar (`this.individual.avatar`) or the
 * 'avatar-default' icon if they don't have an avatar
 * (![avatar-default status icon](pics/icons/avatar-default.png)).
 * * the individual's "alias", being the first of {@link #individual}'s
 * `alias`, `full_name` and `nickname` properties that is not `null`. If all
 * of these are null, their email is used, and finally if that is null too,
 * `_("Unknown")` is used.
 * * the individual's current presence (available, busy, ...) - see
 *   {@link #_createPresence}.
 *
 * Relevant styles: 'contact', 'contact-content', 'contact-details',
 * 'contact-details-alias, 'contact-status', 'contact-status-icon'.
 * @param {string} id - contact ID. This is an ID returned by the underlying
 * `Shell.ContactSystem`/{@link Folks.IndividualAggregator}
 * and is a unique string (like 'b191eef6967fd9eb8dce4f260d4ff4233a030d92'),
 * not anything human-readable. To get one from a contact name/email, use the
 * {@link ContactSearchProvider}.
 * @classdesc 
 * ![Contact](pics/Contact.png)
 *
 * This class represents a shown contact search result in the
 * overview. It also contains information about the contact, like their
 * icon, avatar, alias, etc.
 *
 * It wraps around a {@link Folks.Individual}.
 * @class
 */
function Contact(id) {
    this._init(id);
}

Contact.prototype = {
    _init: function(id) {
        /** Underlying contact system (which wraps around a
         * {@link Folks.IndividualAggregator}
         * from which to look up the contact's information.
         * @type {Shell.ContactSystem} */
        this._contactSys = Shell.ContactSystem.get_default();
        /** The underlying {@link Folks.Individual}
         * representing the contact.
         * @type {Folks.Individual}
         * @property {?Gio.LoadableIcon} avatar - the contact's avatar, or null
         * if none
         * @property {string} alias - the contact's alias
         * @property {string} full_name - the contact's full name
         * @property {string} nickname - the contact's nickname
         * @property {Folks.PresenceType} presence_type - the contact's
         * presence (away, offline, ...) as a {@link Folks.PresenceType}.
         */
        this.individual = this._contactSys.get_individual(id);

        /** The actor for the contact when displayed as a search result.
         * Style class 'contact'. Inside this a `St.BoxLayout` with
         * style class 'contact-content' is nested (not saved in `this`), and
         * the UI elements (avatar, ...) get added to the BoxLayout.
         * ![Contact](pics/Contact.png)
         * @type {St.Bin} */
        this.actor = new St.Bin({ style_class: 'contact',
                                  reactive: true,
                                  track_hover: true });

        let content = new St.BoxLayout( { style_class: 'contact-content',
                                          vertical: false });
        this.actor.set_child(content);

        let icon = new St.Icon({ icon_type: St.IconType.FULLCOLOR,
                                 icon_size: ICON_SIZE,
                                 style_class: 'contact-icon' });
        if (this.individual.avatar != null)
            icon.gicon = this.individual.avatar;
        else
            icon.icon_name = 'avatar-default';

        content.add(icon, { x_fill: true,
                            y_fill: false,
                            x_align: St.Align.START,
                            y_align: St.Align.MIDDLE });

        let details = new St.BoxLayout({ style_class: 'contact-details',
                                         vertical: true });
        content.add(details, { x_fill: true,
                               y_fill: false,
                               x_align: St.Align.START,
                               y_align: St.Align.MIDDLE });

        let email = this._contactSys.get_email_for_display(this.individual);
        let aliasText = this.individual.alias     ||
                        this.individual.full_name ||
                        this.individual.nickname  ||
                        email                     ||
                        _("Unknown");
        let aliasLabel = new St.Label({ text: aliasText,
                                        style_class: 'contact-details-alias' });
        details.add(aliasLabel, { x_fill: true,
                                  y_fill: false,
                                  x_align: St.Align.START,
                                  y_align: St.Align.START });

        let presence = this._createPresence(this.individual.presence_type);
        details.add(presence, { x_fill: false,
                                y_fill: true,
                                x_align: St.Align.START,
                                y_align: St.Align.END });
    },

    /** Creates an icon and text representing the contact's presence.
     * Icon names for various `Folks.PresenceType`s:
     *
     * * `AVAILABLE`: 'user-available'
     * (example ![user-available status icon](pics/icons/user-available.png)).
     * * `AWAY`, `EXTENDED_AWAY`: 'user-away'
     * (example ![user-available status icon](pics/icons/user-away.png)).
     * * `BUSY`: 'user-busy'
     * (example ![user-available status icon](pics/icons/user-busy.png)).
     * * everything else (`UNSET`, `OFFLINE`, `HIDDEN`, `UNKNOWN`, `ERROR`):
     * 'user-offline'
     * (example ![user-available status icon](pics/icons/user-offline.png)).
     *
     * The icon is to the left of the text, which is a translated version of
     * the user's status.
     * @param {Folks.PresenceType} presence - the presence we wish to retrieve
     * @returns {St.BoxLayout} a box layout with style 'contact-details-status'
     * and the presence icon to the left of the presence text.
     */
    _createPresence: function(presence) {
        let text;
        let iconName;

        switch(presence) {
          case Folks.PresenceType.AVAILABLE:
            text = _("Available");
            iconName = 'user-available';
            break;
          case Folks.PresenceType.AWAY:
          case Folks.PresenceType.EXTENDED_AWAY:
            text = _("Away");
            iconName = 'user-away';
            break;
          case Folks.PresenceType.BUSY:
            text = _("Busy");
            iconName = 'user-busy';
            break;
          default:
            text = _("Offline");
            iconName = 'user-offline';
          }

        let icon = new St.Icon({ icon_name: iconName,
                                 icon_type: St.IconType.FULLCOLOR,
                                 icon_size: 16,
                                 style_class: 'contact-details-status-icon' });
        let label = new St.Label({ text: text });

        let box = new St.BoxLayout({ vertical: false,
                                     style_class: 'contact-details-status' });
        box.add(icon, { x_fill: true,
                        y_fill: false,
                        x_align: St.Align.START,
                        y_align: St.Align.START });

        box.add(label, { x_fill: true,
                         y_fill: false,
                         x_align: St.Align.END,
                         y_align: St.Align.START });

        return box;
    },

    /** Creates an icon for the Contact of the requested size, being their
     * avatar or the avatar-default icon if they don't have one.
     * @param {number} size - size of the icon to make.
     */
    createIcon: function(size) {
        let tc = St.TextureCache.get_default();
        let icon = this.individual.avatar;

        if (icon != null) {
            return tc.load_gicon(null, icon, size);
        } else {
            return tc.load_icon_name(null, 'avatar-default', St.IconType.FULLCOLOR, size);
        }
    },
};


/** creates a new ContactSearchProvider
 *
 * Initialises the `Shell.ContactSystem` the class wraps around and also
 * initialises `Search.SearchProvider` with the title "CONTACTS"
 * (translated).
 * @classdesc
 * Searches for and returns contacts.
 * Wraps around a `Shell.ContactSystem`.
 * @class
 * @extends Search.SearchProvider
 */
function ContactSearchProvider() {
    this._init();
}

ContactSearchProvider.prototype = {
    __proto__: Search.SearchProvider.prototype,

    _init: function() {
        Search.SearchProvider.prototype._init.call(this, _("CONTACTS"));
        this._contactSys = Shell.ContactSystem.get_default();
    },

    /** Implements the parent function to return an object describing
     * a contact.
     * 
     * This creates a {@link Contact} for the given ID to get information
     * about it.
     * @param {string} id - contact ID to get the result metadata of.
     * This is an ID returned by the underlying `Shell.ContactSystem`
     * and is a unique string (like 'b191eef6967fd9eb8dce4f260d4ff4233a030d92'),
     * not anything human-readable.
     * @returns {Search.ResultMeta} object describing the contact. Properties
     * `id` being the input `id`, `name` being `contact.alias` (**NOTE**:
     * this is undefined as {@link Contact} never defines it!!), and
     * `createIcon` being {@link Contact#createIcon}.
     * @override
     */
    getResultMeta: function(id) {
        let contact = new Contact(id);
        return { 'id': id,
                 'name': contact.alias,
                 'createIcon': function(size) {
                         return contact.createIcon(size);
                 }
        };
    },

    /** Implements the parent function to return contacts matching the
     * search terms.
     *
     * Just calls `initial_search` on the underlying `Shell.ContactSystem`.
     * @returns {string[]} array of contact IDs matching the search terms.
     * @override
     */
    getInitialResultSet: function(terms) {
        return this._contactSys.initial_search(terms);
    },

    /** Implements the parent function to return the subset of the current
     * results (contacts) matching the search terms.
     * @returns {string[]} array of matching contact IDs.
     * @override
     */
    getSubsearchResultSet: function(previousResults, terms) {
        return this._contactSys.subsearch(previousResults, terms);
    },

    /** Overrides the parent function to provide a custom UI element for each
     * search result (contact).
     *
     * It creates a new {@link Contact} for the `resultMeta` and returns
     * {@link Contact#actor} being the user's avatar, alias, and presence.
     * @returns {St.Bin} actor for the {@link Contact} - user's avatar, alias
     * and presence.
     * @override
     */
    createResultActor: function(resultMeta, terms) {
        let contact = new Contact(resultMeta.id);
        return contact.actor;
    },

    /** Overrides the parent function to provide results in a grid.
     * It returns a {@link SearchDisplay.GridSearchResults} with style
     * class 'contact-grid'.
     * @returns {SearchDisplay.GridSearchResults} - actor to display the results
     * in.
     * @see SearchDisplay.GridSearchResults
     * @override
     */
    createResultContainerActor: function() {
        let grid = new IconGrid.IconGrid({ rowLimit: MAX_SEARCH_RESULTS_ROWS,
                                             xAlign: St.Align.START });
        grid.actor.style_class = 'contact-grid';

        let actor = new SearchDisplay.GridSearchResults(this, grid);
        return actor;
    },

    /** Implements the parent function - what happens when the user selects
     * a result (a contact).
     *
     * This calls {@link launchContact} on the contact (opens
     * `gnome-contacts` to that contact).
     * @param {string} id - contact ID to open.
     */
    activateResult: function(id, params) {
        launchContact(id);
    }
};
