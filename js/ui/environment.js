// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file sets up the GJS environment for the rest of the code by patching
 * some classes and also adding objects into the global environment.
 *
 * See {@link init} for further information.
 *
 * Changes:
 *
 * * defines an object `global` being a {@link Shell.Global};
 *   amongst other things it has properties `stage` (the default Clutter.Stage),
 *   `screen` (the default Meta.Screen), `settings` (Gio.Settings for
 *   org.gnome.shell), and others;
 * * adds `_` as `gettext`, `C_` being `pgettext` and `ngettext` being
 *   `ngettext`;
 * * Sets the default direction for St Widgets to be the same as that of
 *   Gtk widgets (right-to-left, left-to-right);
 * * Adds custom methods `add` and `child_set` to `St.BoxLayout` and `St.Table`
 *  (see {@link _patchContainerClass});
 * * Overrides `Clutter.Actor.prototype.toString` with `St.describe_actor`
 * * Overrides `Object.toString` such that if the object has a member
 *   `this.actor` being a Clutter.Actor, the object's description has
 *   `delegate for <pointer> <this.actor.toString()>` added on to it;
 * * Initialises the Tweener module (see `tweener.js`);
 * * Adds the function `String.prototype.format` which is like `sprintf`
 *   (see {@link Format.format}).
 *
 * @todo put this documentation in the file overview and just mention that
 * this is the function that does it? Because if this function just gets
 * documented as 'init()' that's not very descriptive.
 * @todo I think there's a proper way to document when we override things like
 * `toString` on Clutter.Actor - perhaps overrides tag?
 */

imports.gi.versions.Clutter = '1.0';
imports.gi.versions.Gio = '2.0';
imports.gi.versions.Gdk = '3.0';
imports.gi.versions.GdkPixbuf = '2.0';
imports.gi.versions.Gtk = '3.0';

const Clutter = imports.gi.Clutter;;
const Gettext = imports.gettext;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

// We can't import shell JS modules yet, because they may have
// variable initializations, etc, that depend on init() already having
// been run.

/**
 * Monkey patches in some varargs `ClutterContainer` methods; we need
 * to do this per-container class since there is no representation
 * of interfaces in Javascript.
 *
 * In particular, it adds the methods:
 * 
 * * `child_set`: function taking in a Clutter.Actor and an object of
 *   properties (`x_fill`, `y_fill`, `x_align` etc) and sets these properties
 *   on the actor (which is a child of the container). Basically wraps the
 *   function `clutter_container_get_child_meta`.
 * * `add`: function taking a Clutter.Actor and an object of properties (as
 *   would be given to `child_set`), and both adds the actor to the container
 *   and calls `child_set` with the given properties.
 *
 * @param {Clutter.Container} containerClass - the class to patch.
 */
function _patchContainerClass(containerClass) {
    // This one is a straightforward mapping of the C method
    containerClass.prototype.child_set = function(actor, props) {
        let meta = this.get_child_meta(actor);
        for (let prop in props)
            meta[prop] = props[prop];
    };

    // clutter_container_add() actually is a an add-many-actors
    // method. We conveniently, but somewhat dubiously, take the
    // this opportunity to make it do something more useful.
    containerClass.prototype.add = function(actor, props) {
        this.add_actor(actor);
        if (props)
            this.child_set(actor, props);
    };
}

/**
 * Add some bindings to the global JS namespace; (gjs keeps the web
 * browser convention of having that namespace be called 'window'.)
 *
 * Changes:
 *
 * * defines an object `global` being a {@link Shell.Global};
 *   amongst other things it has properties `stage` (the default Clutter.Stage),
 *   `screen` (the default Meta.Screen), `settings` (Gio.Settings for
 *   org.gnome.shell), and others;
 * * adds `_` as `gettext`, `C_` being `pgettext` and `ngettext` being
 *   `ngettext`;
 * * Sets the default direction for St Widgets to be the same as that of
 *   Gtk widgets (right-to-left, left-to-right);
 * * Calls {@link _patchContainerClass} on `St.BoxLayout` and `St.Table`;
 * * Overrides `Clutter.Actor.prototype.toString` with `St.describe_actor`
 * * Overrides `Object.toString` such that if the object has a member
 *   `this.actor` being a Clutter.Actor, the object's description has
 *   `delegate for <pointer> <this.actor.toString()>` added on to it;
 * * Initialises the Tweener module (see `tweener.js`);
 * * Adds the function `String.prototype.format` which is like `sprintf`
 *   (see {@link Format.format}).
 *
 * @todo put this documentation in the file overview and just mention that
 * this is the function that does it? Because if this function just gets
 * documented as 'init()' that's not very descriptive.
 * @todo I think there's a proper way to document when we override things like
 * `toString` on Clutter.Actor - perhaps overrides tag?
 */
function init() {
    // Add some bindings to the global JS namespace; (gjs keeps the web
    // browser convention of having that namespace be called 'window'.)
    window.global = Shell.Global.get();

    window._ = Gettext.gettext;
    window.C_ = Gettext.pgettext;
    window.ngettext = Gettext.ngettext;

    // Set the default direction for St widgets (this needs to be done before any use of St)
    if (Gtk.Widget.get_default_direction() == Gtk.TextDirection.RTL) {
        St.Widget.set_default_direction(St.TextDirection.RTL);
    }

    // Miscellaneous monkeypatching
    _patchContainerClass(St.BoxLayout);
    _patchContainerClass(St.Table);

    Clutter.Actor.prototype.toString = function() {
        return St.describe_actor(this);
    };

    let origToString = Object.prototype.toString;
    Object.prototype.toString = function() {
        let base = origToString.call(this);
        if ('actor' in this && this.actor instanceof Clutter.Actor)
            return base.replace(/\]$/, ' delegate for ' + this.actor.toString().substring(1));
        else
            return base;
    };

    // Work around https://bugzilla.mozilla.org/show_bug.cgi?id=508783
    Date.prototype.toLocaleFormat = function(format) {
        return Shell.util_format_date(format, this.getTime());
    };

    let slowdownEnv = GLib.getenv('GNOME_SHELL_SLOWDOWN_FACTOR');
    if (slowdownEnv) {
        let factor = parseFloat(slowdownEnv);
        if (!isNaN(factor) && factor > 0.0)
            St.set_slow_down_factor(factor);
    }

    // OK, now things are initialized enough that we can import shell JS
    const Format = imports.misc.format;
    const Tweener = imports.ui.tweener;

    Tweener.init();
    String.prototype.format = Format.format;
}
