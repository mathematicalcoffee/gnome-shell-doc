// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file defines links in the looking glass.
 */

const Lang = imports.lang;
const Signals = imports.signals;
const St = imports.gi.St;

/**
 * Creates a new Link.
 *
 * We create our top-level actor {@link #actor}.
 *
 * Note that this link contains no text .... !
 * @param {Object} [props] - an optional object containing parameters for the
 * link's actor (a St.Button). By default, it gets the properties
 * `reactive: true`, `track_hover: true`, and `style_class: 'shell-link'`.
 * @param {boolean} [props.reactive=true] - whether the link is reactive (clickable).
 * @param {boolean} [props.track_hover=true] - whether the link tracks hovers
 * (so that a hovered-over link gets the ':hover' pseudo style).
 * @param {string} [props.style_class='shell-link'] - the style class for
 * the link.
 * @classdesc This is the base class for a Link, used in the
 * {@link LookingGlass.LookingGlass}. This base class *does nothing* besides
 * creating a reactive `St.Button` with style class 'shell-link' (underlined).
 * 
 * As created, a {@link Link} will *not* do anything when clicked; it is
 * up to subclasses to do that. It doesn't even has text, so that's up
 * to the subclass (see {@link LookingGlass.ObjLink}).
 * @class
 * @see LookingGlass.ObjLink
 */
function Link(props) {
    this._init(props);
}

Link.prototype = {
    _init : function(props) {
        let realProps = { reactive: true,
                          track_hover: true,
                          style_class: 'shell-link' };
        // The user can pass in reactive: false to override the above and get
        // a non-reactive link (a link to the current page, perhaps)
        Lang.copyProperties(props, realProps);

        /** the actor for the Link is a `St.Button`.
         * @type {St.Button} */
        this.actor = new St.Button(realProps);
    }
};

Signals.addSignalMethods(Link.prototype);
