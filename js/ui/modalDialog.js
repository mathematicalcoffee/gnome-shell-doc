// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines a base class for all gnome-shell modal popup dialogs.
 * Examples of these are {@link EndSessionDialog.EndSessionDialog},
 * {@link RunDialog.RunDialog}, {@link ExtensionSystem.InstallExtensionDialog}.
 *
 * ![An example ModalDialog](pics/ModalDialog.png)
 * 
 */

const Clutter = imports.gi.Clutter;
const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const Pango = imports.gi.Pango;
const St = imports.gi.St;
const Shell = imports.gi.Shell;
const Signals = imports.signals;

const Params = imports.misc.params;

const Lightbox = imports.ui.lightbox;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

/** @+
 * @const
 * @default
 * @type {number} */
/** Time taken in seconds for a modal dialog to fade in or out view when it
 * opens or closes. */
const OPEN_AND_CLOSE_TIME = 0.1;
/** Time taken in seconds for a modal dialog's *buttons* to fade into view. */
const FADE_IN_BUTTONS_TIME = 0.33;
/** Time taken in seconds for a modal dialog to fade out of view you wish
 * it to fade out very slowly (see {@link ModalDialog#_fadeOutDialog}). */
const FADE_OUT_DIALOG_TIME = 1.0;
/** @- */

/** The current state of a ModalDialog. (see {@link ModalDialog#state}).
 * @enum
 * @const */
const State = {
    /** Dialog is open. */
    OPENED: 0,
    /** Dialog is closed. */
    CLOSED: 1,
    /** Dialog is in the process of opening (fading in) */
    OPENING: 2,
    /** Dialog is in the process of opening (fading out) */
    CLOSING: 3,
    /** Dialog has faded out so it is not visible, but the lightbox is still
     * visible (see {@link ModalDialog#_fadeOutDialog}). */
    FADED_OUT: 4
};

/** creates a new ModalDialog
 * @param {Object} [params] - parameters for the modal dialog:
 * @param {boolean} [params.shellReactive=false] - whether the rest of the system
 * should remain interactive while the dialog is open, or if the dialog should
 * prevent interaction with the system until it is dismissed. Default is the
 * latter behaviour (`shellReactive=false`).
 * @param {?string} [params.styleClass=null] - CSS style class to add to the dialog
 * (in addition to the default 'modal-dialog').
 * @classdesc
 * ![An example ModalDialog](pics/ModalDialog.png)
 *
 * A modal dialog pops up in the centre of the primary monitor with some sort
 * of content and optionally buttons for the user to press. It appears on
 * top of all other windows, and usually prevents interaction with anything
 * else until the dialog has been dismissed (see the `params.shellReactive`
 * parameter in the constructor).
 *
 * When a modal dialog is opened, the rest of the screen is dimmed using a
 * {@link Lightbox.Lightbox}.
 *
 * Examples of modal dialogs are {@link EndSessionDialog.EndSessionDialog},
 * {@link RunDialog.RunDialog}, and {@link ExtensionSystem.InstallExtensionDialog}.
 *
 * To add content to the dialog (like text) add it to
 * {@link #contentLayout}; to add buttons to the dialog use
 * {@link #setButtons}.
 *
 * The modal dialog above was created with the code:
 *
 *     const ModalDialog = imports.ui.modalDialog;
 *     dlg = new ModalDialog.ModalDialog();
 *     dlg.contentLayout.add(new St.Label({text: 'A ModalDialog.'}),
 *                           {expand: true, fill: true});
 *     dlg.setButtons([
 *         {label: 'Close',
 *          action: function () { dlg.close(global.get_current_time()); }
 *         }]);
 *     dlg.open();
 * @example
 *     // creates the ModalDialog in the screenshot up the top of the class documentation.
 *     const ModalDialog = imports.ui.modalDialog;
 *     dlg = new ModalDialog.ModalDialog();
 *     dlg.contentLayout.add(new St.Label({text: 'A ModalDialog.'}),
 *                           {expand: true, fill: true});
 *     dlg.setButtons([
 *         {label: 'Close',
 *          action: function () { dlg.close(global.get_current_time()); }
 *         }]);
 *     dlg.open();
 * @class
 */
function ModalDialog() {
    this._init();
}

ModalDialog.prototype = {
    _init: function(params) {
        params = Params.parse(params, { shellReactive: false,
                                        styleClass: null });

        /** Current state of the modal dialog.
         * {@type ModalDialog.State} */
        this.state = State.CLOSED;
        this._hasModal = false;
        this._shellReactive = params.shellReactive;

        this._group = new St.Group({ visible: false,
                                     x: 0,
                                     y: 0 });
        Main.uiGroup.add_actor(this._group);

        let constraint = new Clutter.BindConstraint({ source: global.stage,
                                                      coordinate: Clutter.BindCoordinate.POSITION | Clutter.BindCoordinate.SIZE });
        this._group.add_constraint(constraint);

        this._group.connect('destroy', Lang.bind(this, this._onGroupDestroy));

        this._actionKeys = {};
        this._group.connect('key-press-event', Lang.bind(this, this._onKeyPressEvent));

        this._backgroundBin = new St.Bin();
        this._group.add_actor(this._backgroundBin);

        this._dialogLayout = new St.BoxLayout({ style_class: 'modal-dialog',
                                                vertical:    true });
        if (params.styleClass != null) {
            this._dialogLayout.add_style_class_name(params.styleClass);
        }

        if (!this._shellReactive) {
            this._lightbox = new Lightbox.Lightbox(this._group,
                                                   { inhibitEvents: true });
            this._lightbox.highlight(this._backgroundBin);

            let stack = new Shell.Stack();
            this._backgroundBin.child = stack;

            this._eventBlocker = new Clutter.Group({ reactive: true });
            stack.add_actor(this._eventBlocker);
            stack.add_actor(this._dialogLayout);
        } else {
            this._backgroundBin.child = this._dialogLayout;
        }

        /** Holds all the non-button content of the dialog. It is a vertical
         * `St.BoxLayout`. To add content to the dialog (like text), add it
         * to this.
         * @type {St.BoxLayout} */
        this.contentLayout = new St.BoxLayout({ vertical: true });
        this._dialogLayout.add(this.contentLayout,
                               { x_fill:  true,
                                 y_fill:  true,
                                 x_align: St.Align.MIDDLE,
                                 y_align: St.Align.START });

        this._buttonLayout = new St.BoxLayout({ style_class: 'modal-dialog-button-box',
                                                vertical:    false });
        this._dialogLayout.add(this._buttonLayout,
                               { expand:  true,
                                 x_align: St.Align.MIDDLE,
                                 y_align: St.Align.END });

        global.focus_manager.add_group(this._dialogLayout);
        this._initialKeyFocus = this._dialogLayout;
        this._savedKeyFocus = null;
    },

    /** Destroys the dialog. */
    destroy: function() {
        this._group.destroy();
    },

    /** Sets the buttons for the dialog (if called more than once, subsequent
     * calls replace the buttons of earlier calls).
     *
     * If no buttons were visible before and they are now, they are faded
     * in over time {@link FADE_IN_BUTTONS_TIME}.
     *
     * When all the buttons are added/faded in, the `buttons-set` signal is fired.
     * @param {{@link ModalDialog.DialogButtonInfo}[]} buttons -
     * array of objects where each object describes a button to be added.
     * The 'label' property is the text to appear on the button, the 'action'
     * property is the callback to execute when the button is clicked, and the
     * 'key' property, if provided, means that pressing this key will activated
     * the button (`Clutter.KEY_[keyname]`, for example `Clutter.KEY_Escape`).
     * @fires .buttons-set
     */
    setButtons: function(buttons) {
        let hadChildren = this._buttonLayout.get_children() > 0;

        this._buttonLayout.destroy_children();
        this._actionKeys = {};

        for (let i = 0; i < buttons.length; i ++) {
            let buttonInfo = buttons[i];
            let label = buttonInfo['label'];
            let action = buttonInfo['action'];
            let key = buttonInfo['key'];

            buttonInfo.button = new St.Button({ style_class: 'modal-dialog-button',
                                                reactive:    true,
                                                can_focus:   true,
                                                label:       label });

            let x_alignment;
            if (buttons.length == 1)
                x_alignment = St.Align.END;
            else if (i == 0)
                x_alignment = St.Align.START;
            else if (i == buttons.length - 1)
                x_alignment = St.Align.END;
            else
                x_alignment = St.Align.MIDDLE;

            if (this._initialKeyFocus == this._dialogLayout ||
                this._buttonLayout.contains(this._initialKeyFocus))
                this._initialKeyFocus = buttonInfo.button;
            this._buttonLayout.add(buttonInfo.button,
                                   { expand: true,
                                     x_fill: false,
                                     y_fill: false,
                                     x_align: x_alignment,
                                     y_align: St.Align.MIDDLE });

            buttonInfo.button.connect('clicked', action);

            if (key)
                this._actionKeys[key] = action;
        }

        // Fade in buttons if there weren't any before
        if (!hadChildren && buttons.length > 0) {
            this._buttonLayout.opacity = 0;
            Tweener.addTween(this._buttonLayout,
                             { opacity: 255,
                               time: FADE_IN_BUTTONS_TIME,
                               transition: 'easeOutQuad',
                               onComplete: Lang.bind(this, function() {
                                   this.emit('buttons-set');
                               })
                             });
        } else {
            this.emit('buttons-set');
        }

    },

    /** callback when the user presses a key while the modal dialog is open.
     * If the key pressed was one of the keys specified by `buttonInfo.key` in
     * {@link #setButtons}, the action for that button is performed.
     */
    _onKeyPressEvent: function(object, keyPressEvent) {
        let symbol = keyPressEvent.get_key_symbol();
        let action = this._actionKeys[symbol];

        if (action)
            action();
    },

    /** callback when the modal dialog is destroyed. Emits the 'destroy' signal.
     * @fires .destroy */
    _onGroupDestroy: function() {
        this.emit('destroy');
    },

    /** Fades the dialog into view over {@link OPEN_AND_CLOSE_TIME}, emitting
     * the 'opened' signal when done and updating {@link #state}
     * to {@link ModalDialog.State.OPENED}.
     * 
     * Also dims the rest of the screen using a {@link Lightbox.Lightbox}.
     * @fires .opened
     */
    _fadeOpen: function() {
        let monitor = Main.layoutManager.focusMonitor;

        this._backgroundBin.set_position(monitor.x, monitor.y);
        this._backgroundBin.set_size(monitor.width, monitor.height);

        this.state = State.OPENING;

        this._dialogLayout.opacity = 255;
        if (this._lightbox)
            this._lightbox.show();
        this._group.opacity = 0;
        this._group.show();
        Tweener.addTween(this._group,
                         { opacity: 255,
                           time: OPEN_AND_CLOSE_TIME,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this,
                               function() {
                                   this.state = State.OPENED;
                                   this.emit('opened');
                               })
                         });
    },

    /** Sets the initial key focus for the dialog (for example to a particular
     * button). Initial key focus means upon opening the dialog. By default
     * the last button added will have the initial key focus, or the dialog
     * itself it there are no buttons.
     * @param {Clutter.Actor} actor - actor to give initial key focus to.
     */
    setInitialKeyFocus: function(actor) {
        this._initialKeyFocus = actor;
    },

    /** Opens the dialog, fading it in over time {@link OPEN_AND_CLOSE_TIME}.
     * @inheritparams #close
     * @returns {boolean} whether the open succeeded or not.
     */
    open: function(timestamp) {
        if (this.state == State.OPENED || this.state == State.OPENING)
            return true;

        if (!this.pushModal(timestamp))
            return false;

        this._fadeOpen();
        return true;
    },

    /** Closes the dialog, fading it in over time {@link OPEN_AND_CLOSE_TIME}.
     * @param {?number} timestamp - the timestamp. Used for {@link Main.pushModal} or
     * {@link Main.popModal}.
     */
    close: function(timestamp) {
        if (this.state == State.CLOSED || this.state == State.CLOSING)
            return;

        this.state = State.CLOSING;
        this.popModal(timestamp);
        this._savedKeyFocus = null;

        Tweener.addTween(this._group,
                         { opacity: 0,
                           time: OPEN_AND_CLOSE_TIME,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this,
                               function() {
                                   this.state = State.CLOSED;
                                   this._group.hide();
                               })
                         });
    },

    /** Drop modal status without closing the dialog; this makes the
     * dialog insensitive as well, so it needs to be followed shortly
     * by either a {@link #close} or a {@link #pushModal}.
     * @inheritparams #close
     */
    popModal: function(timestamp) {
        if (!this._hasModal)
            return;

        let focus = global.stage.key_focus;
        if (focus && this._group.contains(focus))
            this._savedKeyFocus = focus;
        else
            this._savedKeyFocus = null;
        Main.popModal(this._group, timestamp);
        global.gdk_screen.get_display().sync();
        this._hasModal = false;

        if (!this._shellReactive)
            this._eventBlocker.raise_top();
    },

    /** Adds the dialog to the modal stack without opening it; should be
     * followed by a {@link _fadeOpen}.
     * @inheritparams #close
     * @returns {boolean} whether the push succeeded or not.
     */
    pushModal: function (timestamp) {
        if (this._hasModal)
            return true;
        if (!Main.pushModal(this._group, timestamp))
            return false;

        this._hasModal = true;
        if (this._savedKeyFocus) {
            this._savedKeyFocus.grab_key_focus();
            this._savedKeyFocus = null;
        } else
            this._initialKeyFocus.grab_key_focus();

        if (!this._shellReactive)
            this._eventBlocker.lower_bottom();
        return true;
    },

    /** This method is like close, but fades the dialog out much slower,
     * and leaves the lightbox in place. Once in the faded out state,
     * the dialog can be brought back by an open call, or the lightbox
     * can be dismissed by a close call.
     *
     * The main point of this method is to give some indication to the user
     * that the dialog reponse has been acknowledged but will take a few
     * moments before being processed.
     * e.g., if a user clicked "Log Out" then the dialog should go away
     * imediately, but the lightbox should remain until the logout is
     * complete.
     * @inheritparams #close
     */
    _fadeOutDialog: function(timestamp) {
        if (this.state == State.CLOSED || this.state == State.CLOSING)
            return;

        if (this.state == State.FADED_OUT)
            return;

        this.popModal(timestamp);
        Tweener.addTween(this._dialogLayout,
                         { opacity: 0,
                           time:    FADE_OUT_DIALOG_TIME,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this,
                               function() {
                                   this.state = State.FADED_OUT;
                               })
                         });
    }
};
Signals.addSignalMethods(ModalDialog.prototype);
/** Object describing a button to be added to a dialog.
 * @type {Object}
 * @property {string} label - the text to display on the button.
 * @property {function(St.Button, number)} action - callback when the button
 * is clicked. The first argument is the button instance that was clicked, and
 * the second is the mouse button that was clicked (1 is left click, 2 is middle
 * click, 3 is right click).
 * @property {number} key - optional. If provided, pressing this key will trigger
 * this button's action. The key is a keycode (?) - for example, `Clutter.Escape`
 * or `Clutter.KEY_keyname`.
 * @typedef DialogButtonInfo
 */
/** Fired when buttons are added to a ModalDialog.
 * @event
 * @name buttons-set
 * @memberof ModalDialog
 */
/** Fired when the modal dialog is destroyed.
 * @event
 * @name destroy
 * @memberof ModalDialog
 */
/** Fired when the modal dialog is opened.
 * @event
 * @name opened
 * @memberof ModalDialog
 */
