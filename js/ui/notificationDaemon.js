// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This provides the notification daemon service on 'org.freedesktop.Notifications'.
 *
 * Other applications use this service to send notifications and our
 * {@link NotificationDaemon} is responsible for implementing that on the
 * gnome-shell side, creating notifications as requested and communicating
 * responses back over DBus.
 *
 * We create a {@link Source} in the message tray when a process uses the service
 * to create notifications and pop up {@link MessageTray.Notification}s for its
 * notifications.
 *
 * We also have some extra care for tray icons so that the user can interact
 * with them both as a notification source and as a tray icon.
 *
 * See http://developer.gnome.org/notification-spec/.
 */

const Clutter = imports.gi.Clutter;
const DBus = imports.dbus;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Mainloop = imports.mainloop;
const St = imports.gi.St;

const Config = imports.misc.config;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const Params = imports.misc.params;
const Util = imports.misc.util;

let nextNotificationId = 1;

// Should really be defined in dbus.js
/** Tiny stub of implementation for interface org.freedesktop.DBus, just
 * method GetConnectionUnixProcessID.
 * @see Bus
 * @const */
const BusIface = {
    name: 'org.freedesktop.DBus',
    methods: [{ name: 'GetConnectionUnixProcessID',
                inSignature: 's',
                outSignature: 'i' }]
};

/** creates a new Bus
 *
 * This just provides method 'GetConnectionUnixProcessID' on bus
 * 'org.freedesktop.DBus', object '/org/freedesktop/DBus'. (defined in
 * {@link BusIface}).
 *
 * Exports this instance as a proxy for '/org/freedesktop/Dbus'.
 * @classdesc
 * This just provides method 'GetConnectionUnixProcessID' on bus
 * 'org.freedesktop.DBus', object '/org/freedesktop/DBus'. (defined in
 * {@link BusIface}).
 *
 * Note in the code says it should really be provided with gjs but since it
 * isn't we make a tiny little class to provide just this method.
 *
 * Used by the {@link NotificationDaemon}.
 * @class
 */
const Bus = function () {
    this._init();
};

Bus.prototype = {
     _init: function() {
         DBus.session.proxifyObject(this, 'org.freedesktop.DBus', '/org/freedesktop/DBus');
     }
};

DBus.proxifyPrototype(Bus.prototype, BusIface);


/** Dummy "class" to document DBus methods defined in the NotificationDaemonIface
 * constant, which the {@link NotificationDaemon} implements.
 * (TODO: just document as methods belonging to NotificatioNDaemon?)
 * @classdesc
 * Definition of DBus interface 'org.freedesktop.Notifications'.
 *
 * The {@link NotificationDaemon} uses this to provide a DBus service that
 * other applications can use to send notifications to gnome-shell.
 *
 * See [GNOME developer notes](http://developer.gnome.org/notification-spec/)
 * on this. All methods are documented there.
 *
 * @see NotificationDaemon
 * @class
 */
const NotificationDaemonIface = {
    name: 'org.freedesktop.Notifications',
            /** Sends a notification to the notification server (service
             * org.gnome.freedesktop.Notifications).
             *
             * In signature 'susssasa{sv}i', out signature 'u'.
             * @name Notify
             * @param {string} appName - optional name of the application sending the
             * notification, may be blank.
             * @param {number} replacesId - optional notification ID that this notification
             * replaces. Value of 0 means we won't replace any existing notifications.
             * @param {string} icon - program icon of the calling application,
             * see [further information on website](http://developer.gnome.org/notification-spec/#icons-and-images).
             * Blank for none.
             * @param {string} summary - summary text briefly describing the
             * notification.
             * @param {string} body - optional detailed body text. Blank for none.
             * @param {string[]} actions - actions are sent over as a list of
             * pairs. Each even element in the list (starting at index 0) represents
             * the identifier for the action. Each odd element is the localized
             * string that will be displayed to the user.
             * @param {Object.<string, any>} - (well, `a{sv}`) Optional hints that can be passed to 
             * the server from the client program.
             * Although clients and servers should never assume each other
             * supports any specific hints, they can be used to pass along
             * information, such as the process PID or window ID, that the
             * server may be able to make use of. See
             * [Hints](http://developer.gnome.org/notification-spec/#hints).
             * Can be empty.
             * @param {number} timeout - milliseconds since the display
             * of the notification at which the notification should automatically
             * close. 0 means never expire, -1 means the server can decide.
             * @returns {number} an ID for the notification.
             * @function
             * @memberof NotificationDaemonIface
             * @see http://developer.gnome.org/notification-spec/#command-notify
             */
    methods: [{ name: 'Notify',
                inSignature: 'susssasa{sv}i',
                outSignature: 'u'
              },
            /** Closes a notification by ID.
             * @param {number} id - ID of the notification.
             * @name CloseNotification
             * @function
             * @memberof NotificationDaemonIface
             * @see http://developer.gnome.org/notification-spec/#command-close-notification
             */
              { name: 'CloseNotification',
                inSignature: 'u',
                outSignature: ''
              },
            /** Retrieves the capabilities of this notification service.
             * @returns {string[]} a string of capabilities supported by this
             * server: 'action-icons', 'action', 'body', 'body-hyperlinks',
             * 'body-images', 'body-markup', 'icon-multi', 'icon-static',
             * 'persistence', 'sound' (see the link provided). For {@link NotificationDaemon}
             * this is 'action', 'action-icons', 'body', 'body-markup', 'icon-static' and
             * 'persistence'.
             * @name GetCapabilities
             * @function
             * @memberof NotificationDaemonIface
             * @see http://developer.gnome.org/notification-spec/#command-get-capabilities
             */
              { name: 'GetCapabilities',
                inSignature: '',
                outSignature: 'as'
              },
            /** Returns information about the server.
             * @name GetServerInformation
             * @returns {string} 'ssss': product name, vendor name, version number,
             * notifications specification version we comply with. (For us,
             * {@link Config.PACKAGE_NAME} ('gnome-shell'), 'GNOME",
             * {@link Config.PACKAGE_VERSION} (e.g. '3.2.2.1'), '1.2').
             * @function
             * @memberof NotificationDaemonIface
             * @see http://developer.gnome.org/notification-spec/#command-get-server-information
             */
              { name: 'GetServerInformation',
                inSignature: '',
                outSignature: 'ssss'
              }],
            /** Emitted when a notification has timed out or been dismissed by
             * the user.
             * @param {number} id - ID of the notification
             * @param {NotificationDaemon.NotificationClosedReason} reason - the
             * reason the notification was closed (expired, dismissed,
             * a call to {@link NotificationDaemonIface#CloseNotification},
             * unknown).
             * @name NotificationClosed
             * @event
             * @memberof NotificationDaemonIface
             * @see http://developer.gnome.org/notification-spec/#signal-notification-closed
             */
    signals: [{ name: 'NotificationClosed',
                inSignature: 'uu' },
            /** Emitted when the user invokes an action on a notification, for
             * example by invoking a specific action as specified in the original
             * [Notify request]{@link NotificationDaemonIface#Notify},
             * or by just clicking on the notification itself.
             * @name ActionInvoked
             * @param {number} id - ID of the notification.
             * @param {string} action_key - the key of the action invoked
             * as given to {@link NotificationDaemonIface#Notify}.
             * @event
             * @memberof NotificationDaemonIface
             * @see http://developer.gnome.org/notification-spec/#signal-action-invoked
             */
              { name: 'ActionInvoked',
                inSignature: 'us' }]
};

/** The reason a notification was closed, same as what would be returned by
 * {@link NotificationDaemonIface.NotificationClosed}.
 * @see http://developer.gnome.org/notification-spec/#signal-notification-closed
 * @enum
 * @const */
const NotificationClosedReason = {
    /** The notification expired */
    EXPIRED: 1,
    /** The notification was dismissed by the user */
    DISMISSED: 2,
    /** The notification was closed by a call to
     * {@link NotificationDaemonIface#CloseNotification} */
    APP_CLOSED: 3,
    /** Undefined/reserved reasons */
    UNDEFINED: 4
};

/** The urgency of a notification, complies with the DBus specification.
 * @enum
 * @const
 * @see http://developer.gnome.org/notification-spec/#urgency-levels
 */
const Urgency = {
    /** Low uurgency. */
    LOW: 0,
    /** Normal uurgency. */
    NORMAL: 1,
    /** Critical uurgency. These should not automatically expire and should
     * only close when the user dismisses them. */
    CRITICAL: 2
};

/** Some rules to reformat XChat notifications.
 *
 * * `/^XChat: Private message from: (\S*) \(.*\)$/` --> `$1`
 * * `/^XChat: New public message from: (\S*) \((.*)\)$/` --> `$2 <$1>`
 * * `/^XChat: Highlighted message from: (\S*) \((.*)\)$/` --> `$2 <$1>`
 *
 * @const */
const rewriteRules = {
    'XChat': [
        { pattern:     /^XChat: Private message from: (\S*) \(.*\)$/,
          replacement: '<$1>' },
        { pattern:     /^XChat: New public message from: (\S*) \((.*)\)$/,
          replacement: '$2 <$1>' },
        { pattern:     /^XChat: Highlighted message from: (\S*) \((.*)\)$/,
          replacement: '$2 <$1>' }
    ]
};

/** creates a new NotificationDaemon
 *
 * This exports ourselves as implementing interface {@link NotificationDaemonIface}
 * and as object '/org/freedesktop/Notifications'.
 * @classdesc
 * The Notification Daemon provides service 'org.freedesktop.Notifications'
 * via object '/org/freedesktop/Notifications' and interface 'org.freedesktop.Notifications'.
 *
 * Only one instance needs to be made to provide this service; this is done
 * in {@link Main.start} and stored in {@link Main.notificationDaemon}.
 *
 * It allows other programs to send notifications to be displayed by gnome-shell,
 * see the interface definition in {@link NotificationDaemonIface} as well as
 * a more detailed description by the GNOME devs
 * [online](http://developer.gnome.org/notification-spec/#urgency-levels).
 *
 * This class basically handles making appropriate Notifications and Sources
 * for notifications received and communicating back over DBus when they are
 * interacted with.
 *
 * Note - we don't really subclass `NotificationDaemonIface` (which is a
 * constant in the code, we just documented it as a "dummy" class), but we
 * do implement all its functions so it just helps with the documentation.
 * @see NotificationDaemonIface
 * @implements NotificationDaemonIface
 * @extends NotificationDaemonIface
 * @class
 */
function NotificationDaemon() {
    this._init();
}

NotificationDaemon.prototype = {
    _init: function() {
        DBus.session.exportObject('/org/freedesktop/Notifications', this);

        this._sources = [];
        this._senderToPid = {};
        this._notifications = {};
        this._busProxy = new Bus();

        Main.statusIconDispatcher.connect('message-icon-added', Lang.bind(this, this._onTrayIconAdded));
        Main.statusIconDispatcher.connect('message-icon-removed', Lang.bind(this, this._onTrayIconRemoved));

        Shell.WindowTracker.get_default().connect('notify::focus-app',
            Lang.bind(this, this._onFocusAppChanged));
        Main.overview.connect('hidden',
            Lang.bind(this, this._onFocusAppChanged));
    },

    /** Creates an icon, using either the input `icon` or `hints`.
     *
     * If `icon` is not specified, we use 'image-data' or 'image-path' hint for an icon
     * and don't show a large image. There are currently many applications that use
     * notify_notification_set_icon_from_pixbuf() from libnotify, which in turn sets
     * the 'image-data' hint. These applications don't typically pass in 'app_icon'
     * argument to Notify() and actually expect the pixbuf to be shown as an icon.
     * So the logic here does the right thing for this case. If both an icon and either
     * one of 'image-data' or 'image-path' are specified, we show both an icon and
     * a large image.
     * @inheritparams {@link #Notify}
     * @param {number} size - size of the icon to create
     * @returns {St.Icon} icon for the notification. As a last resort we use
     * 'gtk-dialog-info' for a NORMAL/LOW urgency notification and 'gtk-dialog-error'
     * for a CRITICAL one.
     */
    _iconForNotificationData: function(icon, hints, size) {
        let textureCache = St.TextureCache.get_default();

        // If an icon is not specified, we use 'image-data' or 'image-path' hint for an icon
        // and don't show a large image. There are currently many applications that use
        // notify_notification_set_icon_from_pixbuf() from libnotify, which in turn sets
        // the 'image-data' hint. These applications don't typically pass in 'app_icon'
        // argument to Notify() and actually expect the pixbuf to be shown as an icon.
        // So the logic here does the right thing for this case. If both an icon and either
        // one of 'image-data' or 'image-path' are specified, we show both an icon and
        // a large image.
        if (icon) {
            if (icon.substr(0, 7) == 'file://')
                return textureCache.load_uri_async(icon, size, size);
            else if (icon[0] == '/') {
                let uri = GLib.filename_to_uri(icon, null);
                return textureCache.load_uri_async(uri, size, size);
            } else
                return new St.Icon({ icon_name: icon,
                                     icon_type: St.IconType.FULLCOLOR,
                                     icon_size: size });
        } else if (hints['image-data']) {
            let [width, height, rowStride, hasAlpha,
                 bitsPerSample, nChannels, data] = hints['image-data'];
            return textureCache.load_from_raw(data, hasAlpha, width, height, rowStride, size);
        } else if (hints['image-path']) {
            return textureCache.load_uri_async(GLib.filename_to_uri(hints['image-path'], null), size, size);
        } else {
            let stockIcon;
            switch (hints.urgency) {
                case Urgency.LOW:
                case Urgency.NORMAL:
                    stockIcon = 'gtk-dialog-info';
                    break;
                case Urgency.CRITICAL:
                    stockIcon = 'gtk-dialog-error';
                    break;
            }
            return new St.Icon({ icon_name: stockIcon,
                                 icon_type: St.IconType.FULLCOLOR,
                                 icon_size: size });
        }
    },

    /** Looks up a {@link Source} within our stored list of {@link Source}s.
     * @see #_getSource
     * @inheritparams #_getSource
     */
    _lookupSource: function(title, pid, trayIcon) {
        for (let i = 0; i < this._sources.length; i++) {
            let source = this._sources[i];
            if (source.pid == pid &&
                (source.initialTitle == title || source.trayIcon || trayIcon))
                return source;
        }
        return null;
    },

    /**
     * Returns the source associated with `ndata.notification` if it is set.
     *
     * Otherwise, returns the source associated with the `title` and `pid` if
     * such source has been created already and the notification is not
     * transient. If the existing or requested source is associated with
     * a tray icon and passed in pid matches a pid of an existing source,
     * the title match is ignored to enable representing a tray icon and
     * notifications from the same application with a single source.
     *
     * If no existing source is found, a new source is created as long as
     * `pid` is provided.
     *
     * **Either a `pid` or `ndata.notification` is needed to retrieve or
     * create a source.**
     * @param {string} title - application name to look up source for.
     * @param {number} pid - PID of the process we want a source for
     * @param {NotificationDaemon.NotificationData} ndata - notification data
     * @param {string} sender - `DBus.getCurrentMessageContext().sender`, the
     * object path to the application sending this notification (I think).
     * @param {?Shell.TrayIcon} trayIcon - tray icon we want a source for.
     * @returns {?NotificationDaemon.Source} the {@link Source} requested,
     * or `null` if `pid` and `ndata.notification` were not provided (couldn't
     * find the source).
     */
    _getSource: function(title, pid, ndata, sender, trayIcon) {
        if (!pid && !(ndata && ndata.notification))
            return null;

        // We use notification's source for the notifications we still have
        // around that are getting replaced because we don't keep sources
        // for transient notifications in this._sources, but we still want
        // the notification associated with them to get replaced correctly.
        if (ndata && ndata.notification)
            return ndata.notification.source;

        let isForTransientNotification = (ndata && ndata.hints['transient'] == true);

        // We don't want to override a persistent notification
        // with a transient one from the same sender, so we
        // always create a new source object for new transient notifications
        // and never add it to this._sources .
        if (!isForTransientNotification) {
            let source = this._lookupSource(title, pid, trayIcon);
            if (source) {
                source.setTitle(title);
                return source;
            }
        }

        let source = new Source(title, pid, sender, trayIcon);
        source.setTransient(isForTransientNotification);

        if (!isForTransientNotification) {
            this._sources.push(source);
            source.connect('destroy', Lang.bind(this,
                function() {
                    let index = this._sources.indexOf(source);
                    if (index >= 0)
                        this._sources.splice(index, 1);
                }));
        }

        Main.messageTray.add(source);
        return source;
    },

    /** Sends a notification to the notification server (service
     * org.gnome.freedesktop.Notifications).
     *
     * In signature 'susssasa{sv}i', out signature 'u'.
     * @inheritparams NotificationDaemonIface#Notify
     *
     * First we filter out chat, presence, calls and invitation notifications from
     * Empathy, since we handle that information from {@link namespace:telepathyClient}.
     * (We use `appName` and `hints.category` to do this). We send out a
     * {@link .NotificationClosed} signal with reason DISMISSED for these and
     * return.
     *
     * Then we use {@link rewriteRules} to rewrite `summary` into a more standard
     * format for XChat messages (example: "XChat: New public message from
     * mathematical.coffee (irc.gnome.org)" to "irc.gnome.org <mathematical.coffee>" (I think...
     * did I get it the wrong way round?).
     *
     * Then we create a standardized {@link NotificationDaemon.NotificationData}
     * for the notification, setting a default urgency to `NORMAL` in `hints`
     * and doing some normalization of different hints (like 'image-data',
     * 'icon_data', 'image-path', 'image_path', ...).
     *
     * We create a {@link Source} for this application to sit in the message
     * tray if there isn't already on with {@link #_getSource}, and then
     * call {@link #_notifyForSource} to send its notification, being careful
     * about `replacesId` (if we should replace an existing notification).
     */
    Notify: function(appName, replacesId, icon, summary, body,
                     actions, hints, timeout) {
        let id;

        // Filter out chat, presence, calls and invitation notifications from
        // Empathy, since we handle that information from telepathyClient.js
        if (appName == 'Empathy' && (hints['category'] == 'im.received' ||
              hints['category'] == 'x-empathy.im.room-invitation' ||
              hints['category'] == 'x-empathy.call.incoming' ||
              hints['category'] == 'x-empathy.call.incoming"' ||
              hints['category'] == 'x-empathy.im.subscription-request' ||
              hints['category'] == 'presence.online' ||
              hints['category'] == 'presence.offline')) {
            // Ignore replacesId since we already sent back a
            // NotificationClosed for that id.
            id = nextNotificationId++;
            Mainloop.idle_add(Lang.bind(this,
                                        function () {
                                            this._emitNotificationClosed(id, NotificationClosedReason.DISMISSED);
                                        }));
            return id;
        }

        let rewrites = rewriteRules[appName];
        if (rewrites) {
            for (let i = 0; i < rewrites.length; i++) {
                let rule = rewrites[i];
                if (summary.search(rule.pattern) != -1)
                    summary = summary.replace(rule.pattern, rule.replacement);
            }
        }

        hints = Params.parse(hints, { urgency: Urgency.NORMAL }, true);

        // Be compatible with the various hints for image data and image path
        // 'image-data' and 'image-path' are the latest name of these hints, introduced in 1.2

        if (!hints['image-path'] && hints['image_path'])
            hints['image-path'] = hints['image_path']; // version 1.1 of the spec

        if (!hints['image-data'])
            if (hints['image_data'])
                hints['image-data'] = hints['image_data']; // version 1.1 of the spec
            else if (hints['icon_data'] && !hints['image-path'])
                // early versions of the spec; 'icon_data' should only be used if 'image-path' is not available
                hints['image-data'] = hints['icon_data'];

        let ndata = { appName: appName,
                      icon: icon,
                      summary: summary,
                      body: body,
                      actions: actions,
                      hints: hints,
                      timeout: timeout };
        if (replacesId != 0 && this._notifications[replacesId]) {
            ndata.id = id = replacesId;
            ndata.notification = this._notifications[replacesId].notification;
        } else {
            replacesId = 0;
            ndata.id = id = nextNotificationId++;
        }
        this._notifications[id] = ndata;

        let sender = DBus.getCurrentMessageContext().sender;
        let pid = this._senderToPid[sender];

        let source = this._getSource(appName, pid, ndata, sender, null);

        if (source) {
            this._notifyForSource(source, ndata);
            return id;
        }

        if (replacesId) {
            // There's already a pending call to GetConnectionUnixProcessID,
            // which will see the new notification data when it finishes,
            // so we don't have to do anything.
            return id;
        }

        this._busProxy.GetConnectionUnixProcessIDRemote(sender, Lang.bind(this,
            function (pid, ex) {
                // The app may have updated or removed the notification
                ndata = this._notifications[id];
                if (!ndata)
                    return;

                source = this._getSource(appName, pid, ndata, sender, null);

                // We only store sender-pid entries for persistent sources.
                // Removing the entries once the source is destroyed
                // would result in the entries associated with transient
                // sources removed once the notification is shown anyway.
                // However, keeping these pairs would mean that we would
                // possibly remove an entry associated with a persistent
                // source when a transient source for the same sender is
                // distroyed.
                if (!source.isTransient) {
                    this._senderToPid[sender] = pid;
                    source.connect('destroy', Lang.bind(this,
                        function() {
                            delete this._senderToPid[sender];
                        }));
                }
                this._notifyForSource(source, ndata);
            }));

        return id;
    },

    /** Creates a notification for a source.
     *
     * If `ndata.notification` exists already, we just updated it with the new
     * summary/boddy.
     *
     * Otherwise, we create a {@link MessageTray.Notification} using `source`
     * as the source, `ndata.summary` as the title, `ndata.body` as the body,
     * and {@link #_iconForNotificationData} in `icon` as the icon.
     *
     * We connect to the notification's ['destroy']({@link MessageTray.Notification.event:destroy}
     * signal in order to emit the {@link .NotificationClosed} signal over DBus
     * with the appropriate reason.
     *
     * We also connect to its ['action-invoked']{@link MessageTray.Notification.action-invoked}
     * signal to emit the {@link .ActionInvoked} signal over DBus with the
     * action.
     *
     * If there is image data/path in the hints (see [Hints specification](http://developer.gnome.org/notification-spec/#hints)) we use this to create it and call
     * {@link MessageTray.Notification#setImage} with it.
     *
     * If there is an urgency in the hints, we set it with
     * {@link MessageTray.Notification#setUrgency}.
     *
     * If the notification is resident (as specified in the hints), we set it with
     * {@link MessageTray.Notification#setResident}.
     *
     * If the notification is transient (as specified in the hints), we sit it with
     * {@link MessageTray.Notification#setTransient}.
     *
     * Finally we call {@link Source#processNotification} which will notify it.
     *
     * @param {NotificationDaemon.Source} source
     * @param {NotificationDaemon.Notification} ndata - data for the notification.
     */
    _notifyForSource: function(source, ndata) {
        let [id, icon, summary, body, actions, hints, notification] =
            [ndata.id, ndata.icon, ndata.summary, ndata.body,
             ndata.actions, ndata.hints, ndata.notification];

        let iconActor = this._iconForNotificationData(icon, hints, source.ICON_SIZE);

        if (notification == null) {
            notification = new MessageTray.Notification(source, summary, body,
                                                        { icon: iconActor,
                                                          bannerMarkup: true });
            ndata.notification = notification;
            notification.connect('destroy', Lang.bind(this,
                function(n, reason) {
                    delete this._notifications[ndata.id];
                    let notificationClosedReason;
                    switch (reason) {
                        case MessageTray.NotificationDestroyedReason.EXPIRED:
                            notificationClosedReason = NotificationClosedReason.EXPIRED;
                            break;
                        case MessageTray.NotificationDestroyedReason.DISMISSED:
                            notificationClosedReason = NotificationClosedReason.DISMISSED;
                            break;
                        case MessageTray.NotificationDestroyedReason.SOURCE_CLOSED:
                            notificationClosedReason = NotificationClosedReason.APP_CLOSED;
                            break;
                    }
                    this._emitNotificationClosed(ndata.id, notificationClosedReason);
                }));
            notification.connect('action-invoked', Lang.bind(this,
                function(n, actionId) {
                    this._emitActionInvoked(ndata.id, actionId);
                }));
        } else {
            notification.update(summary, body, { icon: iconActor,
                                                 bannerMarkup: true,
                                                 clear: true });
        }

        // We only display a large image if an icon is also specified.
        if (icon && (hints['image-data'] || hints['image-path'])) {
            let image = null;
            if (hints['image-data']) {
                let [width, height, rowStride, hasAlpha,
                 bitsPerSample, nChannels, data] = hints['image-data'];
                image = St.TextureCache.get_default().load_from_raw(data, hasAlpha,
                                                                    width, height, rowStride, notification.IMAGE_SIZE);
            } else if (hints['image-path']) {
                image = St.TextureCache.get_default().load_uri_async(GLib.filename_to_uri(hints['image-path'], null),
                                                                     notification.IMAGE_SIZE,
                                                                     notification.IMAGE_SIZE);
            }
            notification.setImage(image);
        } else {
            notification.unsetImage();
        }

        if (actions.length) {
            notification.setUseActionIcons(hints['action-icons'] == true);
            for (let i = 0; i < actions.length - 1; i += 2) {
                if (actions[i] == 'default')
                    notification.connect('clicked', Lang.bind(this,
                        function() {
                            this._emitActionInvoked(ndata.id, "default");
                        }));
                else
                    notification.addButton(actions[i], actions[i + 1]);
            }
        }
        switch (hints.urgency) {
            case Urgency.LOW:
                notification.setUrgency(MessageTray.Urgency.LOW);
                break;
            case Urgency.NORMAL:
                notification.setUrgency(MessageTray.Urgency.NORMAL);
                break;
            case Urgency.CRITICAL:
                notification.setUrgency(MessageTray.Urgency.CRITICAL);
                break;
        }
        notification.setResident(hints.resident == true);
        // 'transient' is a reserved keyword in JS, so we have to retrieve the value
        // of the 'transient' hint with hints['transient'] rather than hints.transient
        notification.setTransient(hints['transient'] == true);

        let sourceIconActor = source.useNotificationIcon ? this._iconForNotificationData(icon, hints, source.ICON_SIZE) : null;
        source.processNotification(notification, sourceIconActor);
    },

    /** @inheritdoc NotificationDaemonIface#CloseNotification */
    CloseNotification: function(id) {
        let ndata = this._notifications[id];
        if (ndata) {
            if (ndata.notification)
                ndata.notification.destroy(MessageTray.NotificationDestroyedReason.SOURCE_CLOSED);
            delete this._notifications[id];
        }
    },

    /** @inheritdoc NotificationDaemonIface#GetCapabilities */
    GetCapabilities: function() {
        return [
            'actions',
            'action-icons',
            'body',
            // 'body-hyperlinks',
            // 'body-images',
            'body-markup',
            // 'icon-multi',
            'icon-static',
            'persistence',
            // 'sound',
        ];
    },

    /** @inheritdoc NotificationDaemonIface#GetServerInformation */
    GetServerInformation: function() {
        return [
            Config.PACKAGE_NAME,
            'GNOME',
            Config.PACKAGE_VERSION,
            '1.2'
        ];
    },

    /** Callback when the focused application changes.
     * If the user has just focused an application with (transient) notifications pending,
     * we destroy the transient notifications.
     */
    _onFocusAppChanged: function() {
        let tracker = Shell.WindowTracker.get_default();
        if (!tracker.focus_app)
            return;

        for (let i = 0; i < this._sources.length; i++) {
            let source = this._sources[i];
            if (source.app == tracker.focus_app) {
                source.destroyNonResidentNotifications();
                return;
            }
        }
    },

    /** Emits the [NotificationClosed]{@link .NotificationClosed} signal.
     * @inheritparams #NotificationClosed
     * @fires .event:NotificationClosed
     */
    _emitNotificationClosed: function(id, reason) {
        DBus.session.emit_signal('/org/freedesktop/Notifications',
                                 'org.freedesktop.Notifications',
                                 'NotificationClosed', 'uu',
                                 [id, reason]);
    },

    /** Emits the [ActionInvoked]{@link .ActionInvoked} signal.
     * @inheritparams .event:ActionInvoked
     * @fires .event:ActionInvoked
     */
    _emitActionInvoked: function(id, action) {
        DBus.session.emit_signal('/org/freedesktop/Notifications',
                                 'org.freedesktop.Notifications',
                                 'ActionInvoked', 'us',
                                 [id, action]);
    },

    /** Callback when the {@link Main.statusIconDispatcher} emits its
     * [message-icon-added]{@link StatusIconDispatcher.StatusIconDispatcher.message-icon-added}
     * signal, indicating that we are to add this icon to the message tray.
     *
     * We use {@link #_getSource} to create a {@link Source} for the application
     * and place it in the message tray.
     * @inheritparams StatusIconDispatcher.StatusIconDispatcher.status-icon-added
     * @see #_getSource
     */
    _onTrayIconAdded: function(o, icon) {
        let source = this._getSource(icon.title || icon.wm_class || _("Unknown"), icon.pid, null, null, icon);
    },

    /** Callback when the {@link Main.statusIconDispatcher} emits its
     * [message-icon-removed]{@link StatusIconDispatcher.StatusIconDispatcher.message-icon-removed}
     * signal. If we created a {@link Source} for ths application in the bottom
     * message tray, we should remove it now.
     * @inheritparams StatusIconDispatcher.StatusIconDispatcher.status-icon-added
     */
    _onTrayIconRemoved: function(o, icon) {
        let source = this._lookupSource(null, icon.pid, true);
        if (source)
            source.destroy();
    }
};

DBus.conformExport(NotificationDaemon.prototype, NotificationDaemonIface);

/** creates a new {@link Source} for an application that is sending notifications
 * via the {@link NotificationDaemonIface} (over DBus).
 *
 * `sender` is optional, as is `pid` (used to look up the Shell.App backing
 * this Source).
 *
 * `trayIcon` is also optional (if there we use this as the summary icon for
 * the Source).
 * @inheritparams #_getSource
 * @classdesc
 * This is a {@link MessageTray.Source} for a particular application which
 * may or may not have a tray icon.
 *
 * The {@link NotificationDaemon} creates these for applications that use its
 * service to send notifications.
 * @extends MessageTray.Source
 * @class
 */
function Source(title, pid, sender, trayIcon) {
    this._init(title, pid, sender, trayIcon);
}

Source.prototype = {
    __proto__:  MessageTray.Source.prototype,

    _init: function(title, pid, sender, trayIcon) {
        MessageTray.Source.prototype._init.call(this, title);

        this.initialTitle = title;

        /** Process ID of the process backing this Source. Used to look up
         * the correspondin application.
         * @type {number} */
        this.pid = pid;
        if (sender)
            // TODO: dbus-glib implementation of watch_name() doesn’t return an id to be used for
            // unwatch_name() or implement unwatch_name(), however when we move to using GDBus implementation,
            // we should save the id here and call unwatch_name() with it in destroy().
            // Moving to GDBus is the work in progress: https://bugzilla.gnome.org/show_bug.cgi?id=648651
            // and https://bugzilla.gnome.org/show_bug.cgi?id=622921 .
            DBus.session.watch_name(sender,
                                    false,
                                    null,
                                    Lang.bind(this, this._onNameVanished));

        this._setApp();
        if (this.app)
            this.title = this.app.get_name();
        else
            /** Whether to use the notification icon in {@link Source#processNotification},
             * or whether to use either the application icon or tray icon.
             * @type {boolean} */
            this.useNotificationIcon = true;

        /** If this Source is for a tray icon, we store the underlying tray
         * icon here and use its icon as our icon.
         * @type {Shell.TrayIcon} */
        this.trayIcon = trayIcon;
        if (this.trayIcon) {
           this._setSummaryIcon(this.trayIcon);
           this.useNotificationIcon = false;
        }
    },

    /** Callback when the DBus object backing this Source (i.e. application?)
     * is removed. We destroy the notification.
     *
     * Only do so if this.app is set to avoid removing "notify-send" sources, senders
     * of which аre removed from DBus immediately.
     * Sender being removed from DBus would normally result in a tray icon being removed,
     * so allow the code path that handles the tray icon being removed to handle that case.
     */
    _onNameVanished: function() {
        // Destroy the notification source when its sender is removed from DBus.
        // Only do so if this.app is set to avoid removing "notify-send" sources, senders
        // of which аre removed from DBus immediately.
        // Sender being removed from DBus would normally result in a tray icon being removed,
        // so allow the code path that handles the tray icon being removed to handle that case.
        if (!this.trayIcon && this.app)
            this.destroy();
    },

    /** Processes a notification and notifies it from the source if the user
     * is not currently switched to that application, or just append it to
     * our source without notifying if the user is already switched to
     * the source application.
     *
     * We call {@link Source#_setApp} and if we can find the application for this
     * source, our icon is set to that application's icon (unless we have
     * a tray icon in whic case that is used).
     *
     * Otherwise we use `icon` as our icon.
     * @see Source#notify
     * @param {MessageTray.Notification} notification - notification to notify.
     * @param {St.Icon} [icon] - icon to use for the notification if we can't
     * find the application for this Source.
     */
    processNotification: function(notification, icon) {
        if (!this.app)
            this._setApp();
        if (!this.app && icon)
            this._setSummaryIcon(icon);

        let tracker = Shell.WindowTracker.get_default();
        if (notification.resident && this.app && tracker.focus_app == this.app)
            this.pushNotification(notification);
        else
            this.notify(notification);
    },

    /** Overrides the parent function in order to steal clicks and show the
     * application's tray icon menu (if we have a tray icon). For example
     * the dropbox menu from the dropbox tray icon.
     *
     * If we don't have a tray icon, we don't do anything.
     * If it was a left click but there are undismissed notifications we also
     * don't do anything (then the SummaryItem code will handle the click,
     * showing the notifications).
     *
     * If it was a left click and we have no notifications, or if it was a right click,
     * we pass the click through to the tray icon which launches the appropriate
     * action (for example the Dropbox tray icon has left click to launch
     * dropbox, right click to offer an options/actions menu).
     * @override */
    handleSummaryClick: function() {
        if (!this.trayIcon)
            return false;

        let event = Clutter.get_current_event();
        if (event.type() != Clutter.EventType.BUTTON_RELEASE)
            return false;

        // Left clicks are passed through only where there aren't unacknowledged
        // notifications, so it possible to open them in summary mode; right
        // clicks are always forwarded, as the right click menu is not useful for
        // tray icons
        if (event.get_button() == 1 &&
            this.notifications.length > 0)
            return false;

        if (Main.overview.visible) {
            // We can't just connect to Main.overview's 'hidden' signal,
            // because it's emitted *before* it calls popModal()...
            let id = global.connect('notify::stage-input-mode', Lang.bind(this,
                function () {
                    global.disconnect(id);
                    this.trayIcon.click(event);
                }));
            Main.overview.hide();
        } else {
            this.trayIcon.click(event);
        }
        return true;
    },

    /** Tries to work out the Shell.App for this notification and sets the
     * source's icon to the application's icon (unless we are a tray icon
     * in which case that is used). */
    _setApp: function() {
        if (this.app)
            return;

        /** the application backing this source (if {@link Source#pid} was set
         * and we can find out the correspondin application).
         *
         * We use this application's icon as our icon, unless we are a tray
         * icon in which case we use that icon.
         * @type {Shell.App} */
        this.app = Shell.WindowTracker.get_default().get_app_from_pid(this.pid);
        if (!this.app)
            return;

        // Only override the icon if we were previously using
        // notification-based icons (ie, not a trayicon) or if it was unset before
        if (!this.trayIcon) {
            this.useNotificationIcon = false;
            this._setSummaryIcon(this.app.create_icon_texture (this.ICON_SIZE));
        }
    },

    /** Overrides the parent function. This destroys any non resident notifications
     * and launches the application associated with the Source.
     * @override
     */
    open: function(notification) {
        this.destroyNonResidentNotifications();
        this.openApp();
    },

    /** Overrides th parent function. When the last notification is removed,
     * we destroy ourselves **only** if we are **not** a tray icon (tray icons
     * are always present).
     * @override */
    _lastNotificationRemoved: function() {
        if (!this.trayIcon)
            this.destroy();
    },

    /** Opens the most recent window of the application corresponding to this
     * Source, if we could find it. */
    openApp: function() {
        if (this.app == null)
            return;

        let windows = this.app.get_windows();
        if (windows.length > 0) {
            let mostRecentWindow = windows[0];
            Main.activateWindow(mostRecentWindow);
        }
    },

    // same as the parent.
    destroy: function() {
        MessageTray.Source.prototype.destroy.call(this);
    }
};

/** Data for a Notification.
 * @typedef NotificationData
 * @type {Object}
 * @property {string} appName - optional name of the application sending the
 * notification, may be blank.
 * @property {string} icon - program icon of the calling application,
 * see [further information on website](http://developer.gnome.org/notification-spec/#icons-and-images).
 * Blank for none.
 * @property {string} summary - summary text briefly describing the
 * notification.
 * @property {string} body - optional detailed body text. Blank for none.
 * @property {string[]} actions - actions are sent over as a list of
 * pairs. Each even element in the list (starting at index 0) represents
 * the identifier for the action. Each odd element is the localized
 * string that will be displayed to the user.
 * @property {Object.<string, any>} hints - (well, `a{sv}`) Optional hints that can be passed to 
 * the server from the client program.
 * Although clients and servers should never assume each other
 * supports any specific hints, they can be used to pass along
 * information, such as the process PID or window ID, that the
 * server may be able to make use of. See
 * [Hints](http://developer.gnome.org/notification-spec/#hints).
 * Can be empty.
 * @property {number} timeout - milliseconds since the display
 * of the notification at which the notification should automatically
 * close. 0 means never expire, -1 means the server can decide.
 * @property {MessageTray.Notification} [notification] - the {@link MessageTray.Notification}
 * for this notification.
 * @returns {number} an ID for the notification.
 * @see NotificationDaemonIface#Notify
 */
