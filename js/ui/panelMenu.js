// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines various helper functions for items in the panel (notably, the system
 * status icon class, being a button in the top panel with a dropdown menu).
 */

const Clutter = imports.gi.Clutter;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const Main = imports.ui.main;
const Params = imports.misc.params;
const PopupMenu = imports.ui.popupMenu;

/** creates a new ButtonBox.
 * This creates {@link #actor}, the `Shell.GenericContainer` containing
 * the contents of the button, and connects up its sizing signals such that
 * the padding specified in the 'panel-button' style class is applied to it.
 * @param {Object} params - parameters for the button box (used as parameters
 * for the `Shell.GenericContainer`). Also, in particular:
 * @param {string} params.style_class - the style class for the `Shell.GenericContainer`
 * that is {@link #actor}. Default (if not supplied) - 'panel-button'.
 * @classdesc
 * The base button class for buttons in the panel. It incorporates the
 * `-minimum-hpadding` and `-natural-hpadding` attributes in the `panel-button`
 * style class - so whatever you insert into {@link #actor} should get
 * padded uniformly with all the other buttons in the panel for a uniform
 * look across the status area.
 *
 * By default nothing is actually displayed for the button - you have to add
 * the contents (like an icon, or some text) yourself by calling
 * `this.actor.add_actor`.
 *
 * If you want the button to have a dropdown menu, use {@link Button}.
 * @class
 */
function ButtonBox(params) {
    this._init.apply(this, arguments);
};

ButtonBox.prototype = {
    _init: function(params) {
        params = Params.parse(params, { style_class: 'panel-button' }, true);
        /** The UI part of the button. It's empty - up to the user to add the
         * visual part to this (for example {@link SystemStatusButton} adds an
         * icon). **NOTE**: only the first child of the container will be
         * displayed.
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer(params);
        this.actor._delegate = this;

        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));

        this.actor.connect('style-changed', Lang.bind(this, this._onStyleChanged));
        this._minHPadding = this._natHPadding = 0.0;
    },

    /** callback when {@link #actor}'s style changes in any way.
     * Restores the natural and minimum horizontal padding for the button
     * ({@link #_minHPadding}, {@link #_natHPadding}).
     */
    _onStyleChanged: function(actor) {
        let themeNode = actor.get_theme_node();

        /** The minimum horizontal padding for the button, specified in the
         * style class' ('panel-button') '-minimum-hpadding' attribute (default
         * 6 pixels).
         * @type {number} */
        this._minHPadding = themeNode.get_length('-minimum-hpadding');
        /** The natural horizontal padding for the button, specified in the
         * style class' ('panel-button') '-natural-hpadding' attribute (default
         * 12 pixels (! - very wide)).
         * @type {number} */
        this._natHPadding = themeNode.get_length('-natural-hpadding');
    },

    /** callback for the 'get-preferred-width' signal of
     * {@link #actor}. Assigns the width required by the first child
     * inside {@link #actor} and adds {@link #_minHPadding}
     * or {@link #_natHPadding} either side.
     * Subsequent children are not displayed.
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        let children = actor.get_children();
        let child = children.length > 0 ? children[0] : null;

        if (child) {
            [alloc.min_size, alloc.natural_size] = child.get_preferred_width(-1);
        } else {
            alloc.min_size = alloc.natural_size = 0;
        }

        alloc.min_size += 2 * this._minHPadding;
        alloc.natural_size += 2 * this._natHPadding;
    },

    /** callback for the 'get-preferred-height' signal of
     * {@link #actor}. Assigns the height required by the first child
     * inside {@link #actor}. Subsequent children are not displayed.
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        let children = actor.get_children();
        let child = children.length > 0 ? children[0] : null;

        if (child) {
            [alloc.min_size, alloc.natural_size] = child.get_preferred_height(-1);
        } else {
            alloc.min_size = alloc.natural_size = 0;
        }
    },

    /** callback for the 'allocate' signal of {@link #actor}.
     * Assigns the first child the size it requests (up to the maximum avilable
     * height/width), centering the child in the actor and also padding with
     * the natural padding (if it fits) or the minimum padding (otherwise).
     *
     * Subsequent children are not displayed.
     */
    _allocate: function(actor, box, flags) {
        let children = actor.get_children();
        if (children.length == 0)
            return;

        let child = children[0];
        let [minWidth, natWidth] = child.get_preferred_width(-1);
        let [minHeight, natHeight] = child.get_preferred_height(-1);

        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;

        let childBox = new Clutter.ActorBox();
        if (natWidth + 2 * this._natHPadding <= availWidth) {
            childBox.x1 = this._natHPadding;
            childBox.x2 = availWidth - this._natHPadding;
        } else {
            childBox.x1 = this._minHPadding;
            childBox.x2 = availWidth - this._minHPadding;
        }

        if (natHeight <= availHeight) {
            childBox.y1 = Math.floor((availHeight - natHeight) / 2);
            childBox.y2 = childBox.y1 + natHeight;
        } else {
            childBox.y1 = 0;
            childBox.y2 = availHeight;
        }

        child.allocate(childBox, flags);
    },
}

/** creates a new Button, being a {@link ButtonBox} with its own
 * {@link PopupMenu.PopupMenu}.
 *
 * The constructor instantiates this as a {@link ButtonBox} with parameters
 * `reactive: true`, `can_focus: true` and `track_hover: true`.
 *
 * It then creates a popup menu for itself ({@link ButtonBox#menu}).
 * @param {number} menuAlignment - the `arrowAlignment` argument for
 * {@link PopupMenu.PopupMenu} - alignment of the menu arrow with respect to
 * the menu.
 * @classdesc
 * This is a {@link ButtonBox} along with a {@link PopupMenu.PopupMenu}.
 *
 * As with the {@link ButtonBox}, `this.actor` is an empty
 * `Shell.GenericContainer` and must be populated with something visual
 * (only the first child is displayed).
 *
 * For example, the [date menu]{@link DateMenu.DateMenuButton} is an example of
 * this where the visual element is a `St.Label` displaying the time.
 * @class
 * @extends ButtonBox
 */
function Button(menuAlignment) {
    this._init(menuAlignment);
}

Button.prototype = {
    __proto__: ButtonBox.prototype,

    _init: function(menuAlignment) {
        ButtonBox.prototype._init.call(this, { reactive: true,
                                               can_focus: true,
                                               track_hover: true });

        this.actor.connect('button-press-event', Lang.bind(this, this._onButtonPress));
        this.actor.connect('key-press-event', Lang.bind(this, this._onSourceKeyPress));
        /** The menu for this button. The menu drops down from the button.
         * @type {PopupMenu.PopupMenu} */
        this.menu = new PopupMenu.PopupMenu(this.actor, menuAlignment, St.Side.TOP);
        this.menu.actor.add_style_class_name('panel-menu');
        this.menu.connect('open-state-changed', Lang.bind(this, this._onOpenStateChanged));
        this.menu.actor.connect('key-press-event', Lang.bind(this, this._onMenuKeyPress));
        Main.uiGroup.add_actor(this.menu.actor);
        this.menu.actor.hide();
    },

    /** callback when the user presses the button.
     * This toggles open or closed the menu, making sure
     * that it fits vertically on the screen. */
    _onButtonPress: function(actor, event) {
        if (!this.menu.isOpen) {
            // Setting the max-height won't do any good if the minimum height of the
            // menu is higher then the screen; it's useful if part of the menu is
            // scrollable so the minimum height is smaller than the natural height
            let monitor = Main.layoutManager.primaryMonitor;
            this.menu.actor.style = ('max-height: ' +
                                     Math.round(monitor.height - Main.panel.actor.height) +
                                     'px;');
        }
        this.menu.toggle();
    },

    /** callback when the user presses a keyboard button while keyboard focus is on the
     * button (not the popup menu). (To get keyboard focus onto a Button,
     * use the [Ctrl+Alt+Tab popup]{@link CtrlAltTab.ctrlAltTabPopup} to select
     * the top panel and use the keyboard to navigate to the button).
     *
     * If they press space or return, the menu toggles open/closed.
     * If they press escape while the menu is open, the menu closes.
     * If they press the down arrow, the menu is opened (if it is currently
     * closed) and keyboard focus is transferred to the popup menu.
     */
    _onSourceKeyPress: function(actor, event) {
        let symbol = event.get_key_symbol();
        if (symbol == Clutter.KEY_space || symbol == Clutter.KEY_Return) {
            this.menu.toggle();
            return true;
        } else if (symbol == Clutter.KEY_Escape && this.menu.isOpen) {
            this.menu.close();
            return true;
        } else if (symbol == Clutter.KEY_Down) {
            if (!this.menu.isOpen)
                this.menu.toggle();
            this.menu.actor.navigate_focus(this.actor, Gtk.DirectionType.DOWN, false);
            return true;
        } else
            return false;
    },

    /** callback when the user presses a keyboard button while focus is on its
     * popup menu. If they press left or right, focus is transferred to the
     * item to the left or right of the current button. For example if focus
     * is on one of the status buttons in the top panel, pressing left or right
     * transfers focus to the button in the top panel that is to the left or
     * right of the current one.
     */
    _onMenuKeyPress: function(actor, event) {
        let symbol = event.get_key_symbol();
        if (symbol == Clutter.KEY_Left || symbol == Clutter.KEY_Right) {
            let focusManager = St.FocusManager.get_for_stage(global.stage);
            let group = focusManager.get_group(this.actor);
            if (group) {
                let direction = (symbol == Clutter.KEY_Left) ? Gtk.DirectionType.LEFT : Gtk.DirectionType.RIGHT;
                group.navigate_focus(this.actor, direction, false);
                return true;
            }
        }
        return false;
    },

    /** callback when the popup menu toggles open or closed - adds or removes
     * the 'active' pseudo style class from the button.
     */
    _onOpenStateChanged: function(menu, open) {
        if (open)
            this.actor.add_style_pseudo_class('active');
        else
            this.actor.remove_style_pseudo_class('active');
    },

    /** Destroys the button and its menu, and emits the 'destroy' signal.
     * @fires .destroy */
    destroy: function() {
        this.actor._delegate = null;

        this.menu.destroy();
        this.actor.destroy();

        this.emit('destroy');
    }
};
Signals.addSignalMethods(Button.prototype);
/** Fired when a Button is destroyed ({@link Button#destroy} is called).
 * @name destroy
 * @event
 * @memberof Button
 */

/** creates a new SystemStatusButton, being a {@link Button} with an icon
 * as the visual actor.
 *
 * This just creates a {@link Button}, then creates
 * {@link #_iconActor} which is a symbolic icon and adds it
 * to `this.actor`. The arrow is aligned to the left of the menu.
 *
 * The icon has style class 'system-status-icon' and the button itself
 * has additional class 'panel-status-button' (on top of 'panel-button').
 *
 * @param {string} iconName - name of the icon to show as the icon
 * @param {string} tooltipText - the tooltip text for the button.
 * @classdesc
 * This class is a {@link Button} where the visual element is an icon.
 *
 * So basically {@link #actor} has a (symbolic) icon added to
 * it - just a convenience.
 *
 * The icon has style class 'system-status-icon' and the button itself
 * has additional class 'panel-status-button' (on top of 'panel-button').
 *
 * Examples are the status indicators (e.g. [power status indicator]{@link Power.Indicator}).
 * @class
 * @extends Button
 */
function SystemStatusButton() {
    this._init.apply(this, arguments);
}

SystemStatusButton.prototype = {
    __proto__: Button.prototype,

    _init: function(iconName,tooltipText) {
        Button.prototype._init.call(this, 0.0);
        /** The icon that is the visual part of the button. It's symbolic.
         * Style class 'system-status-icon'.
         * @type {St.Icon} */
        this._iconActor = new St.Icon({ icon_name: iconName,
                                        icon_type: St.IconType.SYMBOLIC,
                                        style_class: 'system-status-icon' });
        this.actor.add_actor(this._iconActor);
        this.actor.add_style_class_name('panel-status-button');
        this.setTooltip(tooltipText);
    },

    /** updates the icon for the button by icon name.
     * @param {string} iconName - the name of the icon.
     */
    setIcon: function(iconName) {
        this._iconActor.icon_name = iconName;
    },

    /** updates the icon for the button by icon.
     * @param {GIcon} gicon - new icon to set.
     */
    setGIcon: function(gicon) {
        this._iconActor.gicon = gicon;
    },

    /** sets the tooltip text for the button.
     * @param {string} text - tooltip text
     */
    setTooltip: function(text) {
        if (text != null) {
            this.tooltip = text;
            this.actor.has_tooltip = true;
            this.actor.tooltip_text = text;
        } else {
            this.actor.has_tooltip = false;
            this.tooltip = null;
        }
    }
};
