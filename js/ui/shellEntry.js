/**
 * @fileOverview
 * Adds context menus to entry widgets with 'copy' and 'paste' and (if it's a
 * password widget) 'show/hide text'.
 * Examples: in the {@link RunDialog.RunDialog} and
 * {@link PolkitAuthenticationAgent.AuthenticationDialog}.
 *
 * Use {@link addContextMenu} to add a menu to an entry.
 *
 * ![EntryMenu in the run dialog](pics/entryMenu.png)
 */
const Clutter = imports.gi.Clutter;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const St = imports.gi.St;

const Main = imports.ui.main;
const Params = imports.misc.params;
const PopupMenu = imports.ui.popupMenu;

/**
 * Creates a new _EntryMenu
 * @param {St.Entry} entry - the entry you wish to create a menu for
 * @param {Object} params - set of parameters for the entry menu.
 * @param {boolean} params.isPassword - whether the entry is for passwords
 *  (this is currently the only parameter that gets used).
 * @classdesc The `_EntryMenu` class defines a {@link PopupMenu.PopupMenu} that
 * will be attached to a `St.Entry`. It has 'Copy' and 'Paste' items, and
 * additionally (if `params.isPassword` was `true`) a 'Show/Hide text' item
 * (that switches between showing a password and showing the dot symbol ● in
 * the entry).
 *
 * You **never** create this class yourself - instead, use
 * [`addContextMenu(entry)`]{@link addContextMenu} to create the context menu for an entry.
 *
 * This will create the `_EntryMenu` for you and also add right click/long
 * press actions to the entry that handle launching/closing the menu.
 *
 * ![EntryMenu](pics/entryMenu.png)
 *
 * @extends PopupMenu.PopupMenu
 * @class
 */
function _EntryMenu(entry, params) {
    this._init(entry, params);
};

_EntryMenu.prototype = {
    __proto__: PopupMenu.PopupMenu.prototype,

    _init: function(entry, params) {
        params = Params.parse (params, { isPassword: false });

        PopupMenu.PopupMenu.prototype._init.call(this, entry, 0, St.Side.TOP);

        this.actor.add_style_class_name('entry-context-menu');

        /** the entry the menu is for.
         * @type {St.Entry} */
        this._entry = entry;
        /** Clipboard object.
         * @type {St.Clipboard} */
        this._clipboard = St.Clipboard.get_default();

        // Populate menu
        let item;
        item = new PopupMenu.PopupMenuItem(_("Copy"));
        item.connect('activate', Lang.bind(this, this._onCopyActivated));
        this.addMenuItem(item);
        this._copyItem = item;

        item = new PopupMenu.PopupMenuItem(_("Paste"));
        item.connect('activate', Lang.bind(this, this._onPasteActivated));
        this.addMenuItem(item);
        this._pasteItem = item;

        this._passwordItem = null;
        if (params.isPassword) {
            item = new PopupMenu.PopupMenuItem('');
            item.connect('activate', Lang.bind(this,
                                               this._onPasswordActivated));
            this.addMenuItem(item);
            this._passwordItem = item;
        }

        Main.uiGroup.add_actor(this.actor);
        this.actor.hide();
    },

    /** opens the menu, first updating the paste, copy, and password items if
     * necessary (to reflect the state of the clipboard/entry)
     * @see #_updateCopyItem
     * @see #_updatePasteItem
     * @see #_updatePasswordItem
     */
    open: function() {
        this._updatePasteItem();
        this._updateCopyItem();
        if (this._passwordItem)
            this._updatePasswordItem();

        let direction = Gtk.DirectionType.TAB_FORWARD;
        if (!this.actor.navigate_focus(null, direction, false))
            this.actor.grab_key_focus();

        PopupMenu.PopupMenu.prototype.open.call(this);
    },

    /** Updates the 'Copy' item, enabling it if any text in the entry is
     * selected and disabling it otherwise */
    _updateCopyItem: function() {
        let selection = this._entry.clutter_text.get_selection();
        this._copyItem.setSensitive(selection && selection != '');
    },

    /** Updates the 'Paste' item, disabling it if there's
     * no text in the clipboard to paste and enabling it otherwise */
    _updatePasteItem: function() {
        this._clipboard.get_text(Lang.bind(this,
            function(clipboard, text) {
                this._pasteItem.setSensitive(text && text != '');
            }));
    },

    /** Updates the 'Password' item to display either "Show Text" or "Hide Text"
     * (shows the opposite of what is happening so you can switch) */
    _updatePasswordItem: function() {
        let textHidden = (this._entry.clutter_text.password_char);
        if (textHidden)
            this._passwordItem.label.set_text(_("Show Text"));
        else
            this._passwordItem.label.set_text(_("Hide Text"));
    },

    /** callback when the "Copy" item is clicked - copies the current selection
     * in the entry to the clipboard. */
    _onCopyActivated: function() {
        let selection = this._entry.clutter_text.get_selection();
        this._clipboard.set_text(selection);
    },

    /** callback when the "Paste" item is clicked - copies from the clipboard
     * into the Shell.Entry */
    _onPasteActivated: function() {
        this._clipboard.get_text(Lang.bind(this,
            function(clipboard, text) {
                if (!text)
                    return;
                this._entry.clutter_text.delete_selection();
                let pos = this._entry.clutter_text.get_cursor_position();
                this._entry.clutter_text.insert_text(text, pos);
            }));
    },

    /** called when the "Show/Hide Text" item is activated - either sets the
     * password character to '\u25cf' ● or back to plaintext */
    _onPasswordActivated: function() {
        let visible = !!(this._entry.clutter_text.password_char);
        this._entry.clutter_text.set_password_char(visible ? '' : '\u25cf');
    }
};

/** Sets the position the menu's pointer will appear.
 *
 * Transforms this into a fraction of the entry's width and calls
 * {@link BoxPointer.BoxPointer.setSourceAlignment}.
 *
 * @param {St.Entry} entry - the entry the menu is for
 * @param {number} stageX - the x coordinate (relative to the *stage*) that
 * you want the point of the menu's pointer to be.
 */
function _setMenuAlignment(entry, stageX) {
    let [success, entryX, entryY] = entry.transform_stage_point(stageX, 0);
    if (success)
        entry._menu.setSourceAlignment(entryX / entry.width);
};

/** callback for when the entry fires a 'clicked' signal.
 * Closes the menu if it's open. Otherwise if the click was a right click,
 * it opens the menu (positioned on the entry)
 */
function _onClicked(action, actor) {
    let entry = actor._menu ? actor : actor.get_parent();

    if (entry._menu.isOpen) {
        entry._menu.close();
    } else if (action.get_button() == 3) {
        let [stageX, stageY] = action.get_coords();
        _setMenuAlignment(entry, stageX);
        entry._menu.open();
    }
};

/** callback for when the entry fires a 'long-press' signal.
 * Positions the menu at the click and opens it.
 */
function _onLongPress(action, actor, state) {
    let entry = actor._menu ? actor : actor.get_parent();

    if (state == Clutter.LongPressState.QUERY)
        return action.get_button() == 1 && !entry._menu.isOpen;

    if (state == Clutter.LongPressState.ACTIVATE) {
        let [stageX, stageY] = action.get_coords();
        _setMenuAlignment(entry, stageX);
        entry._menu.open();
    }
    return false;
};

/** callback for when the `St.Entry` fires a 'popup-menu' signal.
 * Positions the menu at the end of the text in the entry and opens it.
 */
function _onPopup(actor) {
    let entry = actor._menu ? actor : actor.get_parent();
    let [success, textX, textY, lineHeight] = entry.clutter_text.position_to_coords(-1);
    if (success)
        entry._menu.setSourceAlignment(textX / entry.width);
    entry._menu.open();
};

/** This function adds a context menu to a St.Entry (or St.Label for that
 * matter - just needs to have a property `.clutter_text` pointing to the
 * underlying `Clutter.Text` for the actor).
 *
 * The context menu is a {@link PopupMenu.PopupMenu} that is is launched upon
 * right-clicking or long-pressing the entry, and contains the item "Copy",
 * "Paste", and (if the entry is for entering passwords) "Show/Hide text".
 *
 * If the entry is for entering passwords, copy doesn't work, of course :)
 *
 * You *never* use the {@link _EntryMenu} class itself to create a context
 * menu; instead, you use this function. It will create a {@link _EntryMenu}
 * for the entry, storred in `entry._menu`. It will *also* add clicked and
 * long-press actions to the entry to handle the launching of the menu.
 *
 * ![Example of an EntryMenu in the run dialog](pics/entryMenu.png)
 *
 * Examples of these: in the {@link RunDialog.RunDialog} (picture above) and
 * {@link PolkitAuthenticationAgent.AuthenticationDialog}.
 *
 * @param {St.Entry} entry - the St.Entry to add the context menu to.
 * @param {Object} params - set of parameters for the entry menu.
 * @param {boolean} params.isPassword - whether the entry is for passwords
 *  (this is currently the only parameter that gets used).
 */
function addContextMenu(entry, params) {
    if (entry._menu)
        return;

    entry._menu = new _EntryMenu(entry, params);
    entry._menuManager = new PopupMenu.PopupMenuManager({ actor: entry });
    entry._menuManager.addMenu(entry._menu);

    let clickAction;

    // Add a click action to both the entry and its clutter_text; the former
    // so padding is included in the clickable area, the latter because the
    // event processing of ClutterText prevents event-bubbling.
    clickAction = new Clutter.ClickAction();
    clickAction.connect('clicked', _onClicked);
    clickAction.connect('long-press', _onLongPress);
    entry.clutter_text.add_action(clickAction);

    clickAction = new Clutter.ClickAction();
    clickAction.connect('clicked', _onClicked);
    clickAction.connect('long-press', _onLongPress);
    entry.add_action(clickAction);

    entry.connect('popup-menu', _onPopup);
}
