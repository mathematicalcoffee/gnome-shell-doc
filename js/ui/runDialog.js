// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * The run dialog/command prompt when you press `Alt+F2`. It also has
 * command completion.
 *
 * ![`RunDialog`](pics/runDialog.png)
 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const St = imports.gi.St;
const Shell = imports.gi.Shell;
const Signals = imports.signals;

const FileUtils = imports.misc.fileUtils;
const Main = imports.ui.main;
const ModalDialog = imports.ui.modalDialog;
const ShellEntry = imports.ui.shellEntry;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;
const History = imports.misc.history;

/** @+
 * @const
 * @default */
/** When less than this many files in the user's `$PATH` have been deleted since
 * the last update, calls to {@link CommandCompleter#update} will not do anything.
 * @see CommandCompleter#_onChanged
 * @type {number} */
const MAX_FILE_DELETED_BEFORE_INVALID = 10;
/** @+
 * @type {string} */
/** Gsettings key under org.gnome.shell that will store the command history
 * for the run dialog. See {@link HistoryManager}. */
const HISTORY_KEY = 'command-history';

/** GSettings schema for desktop lockdown - i.e. what the user is allowed to do
 * (are they allowed to print, save, use the command line, ... */
const LOCKDOWN_SCHEMA = 'org.gnome.desktop.lockdown';
/** Gsettings key under {@link LOCKDOWN_SCHEMA} - whether the user is allowed
 * to access the terminal/specify command lines (i.e. whether the run dialog
 * should be avilable to the user). */
const DISABLE_COMMAND_LINE_KEY = 'disable-command-line';

/** GSettings schema for the default terminal application */
const TERMINAL_SCHEMA = 'org.gnome.desktop.default-applications.terminal';
/** Gsettings key under {@link TERMINAL_SCHEMA} containing command to
 * execute the default terminal */
const EXEC_KEY = 'exec';
/** Gsettings key under {@link TERMINAL_SCHEMA} containing argument used to
 * execute commands in the terminal in {@link EXEC_KEY}. */
const EXEC_ARG_KEY = 'exec-arg';
/** @- */

/** Time taken in seconds for the "Error" bit of the run dialog to appear
 * (if you enter in a command that is not recognised).
 *
 * ![Error bit of the run dialog](pics/runDialogError.png)
 * @type {number} */
const DIALOG_GROW_TIME = 0.1;
/** @- */

/** creates a new CommandCompleter
 *
 * This gets all the directories the user's `$PATH` and creates a
 * {@link Gio.FileMonitor} for each, stored in
 * {@link #_paths} and {@link #_monitors}.
 *
 * It then asynchronously creates a list of files in each of the directories
 * in `$PATH`, storing them in {@link #_childs}. These are
 * used as possible completions when the user presses Tab.
 *
 * The file monitors detect changes in the `$PATH` directories and update the
 * list of completions as necessary.
 *
 * @classdesc
 * The command completer is activated by the user pressing 'Tab' in the run
 * dialog and acts in much the same way as the tab completion in the terminal
 * (for example, typing in `ter` and pressing tab completes out to `termina`
 * because I have two valid commands, `terminal`, and `terminator`).
 *
 * It is used in the {@link RunDialog}.
 *
 * It does this by storing all the directories in the user's `$PATH` locally and
 * creating a list of files under each directory, storing these as possible
 * completions. It creates a {@link Gio.FileMonitor} for each directory to keep
 * this list up-to-date.
 *
 * It doesn't display the list of possible commands, though. Just autocompletes.
 *
 * @see RunDialog
 * @class
 */
function CommandCompleter() {
    this._init();
}

CommandCompleter.prototype = {
    _init : function() {
        this._changedCount = 0;
        /** List of directories in the user's `$PATH`.
         * @type {string[]} */
        this._paths = GLib.getenv('PATH').split(':');
        this._paths.push(GLib.get_home_dir());
        this._valid = false;
        this._updateInProgress = false;
        /** Array of arrays of strings. `this._childs[i]` contains an array
         * of files under directory `this._paths[i]`. These are used as the
         * possible completions.
         * @type {string[][]} */
        this._childs = new Array(this._paths.length);
        /** List of {@link Gio.FileMonitor}s,
         * one for each path in {@link #_paths}. Whenever
         * something changes here the list of commands that can complete
         * is updated.
         * @type {Gio.FileMonitor[]}
         */
        this._monitors = new Array(this._paths.length);
        for (let i = 0; i < this._paths.length; i++) {
            this._childs[i] = [];
            let file = Gio.file_new_for_path(this._paths[i]);
            let info;
            try {
                info = file.query_info(Gio.FILE_ATTRIBUTE_STANDARD_TYPE, Gio.FileQueryInfoFlags.NONE, null);
            } catch (e) {
                // FIXME catchall
                this._paths[i] = null;
                continue;
            }

            if (info.get_attribute_uint32(Gio.FILE_ATTRIBUTE_STANDARD_TYPE) != Gio.FileType.DIRECTORY)
                continue;

            this._paths[i] = file.get_path();
            this._monitors[i] = file.monitor_directory(Gio.FileMonitorFlags.NONE, null);
            if (this._monitors[i] != null) {
                this._monitors[i].connect('changed', Lang.bind(this, this._onChanged));
            }
        }
        this._paths = this._paths.filter(function(a) {
            return a != null;
        });
        this._update(0);
    },

    /** Top-level function. Updates the list of autocomplete commands.
     * This will only actually update when more than {@link MAX_FILE_DELETED_BEFORE_INVALID}
     * files in the directories in `$PATH` have been deleted since the last
     * update. */
    update : function() {
        if (this._valid)
            return;
        this._update(0);
    },

    /** Internal command. Asynchronously updates the list of commands under
     * directory [`this._paths[i]`]{@link #_paths}, storing them
     * as an array in [`this._childs[i]`]{@link #_childs}. When
     * complete, it will update the next path in the list.
     * @param {number} i - index of the path to update.
     */
    _update : function(i) {
        if (i == 0 && this._updateInProgress)
            return;
        this._updateInProgress = true;
        this._changedCount = 0;
        this._i = i;
        if (i >= this._paths.length) {
            this._valid = true;
            this._updateInProgress = false;
            return;
        }
        let file = Gio.file_new_for_path(this._paths[i]);
        this._childs[this._i] = [];
        FileUtils.listDirAsync(file, Lang.bind(this, function (files) {
            for (let i = 0; i < files.length; i++) {
                this._childs[this._i].push(files[i].get_name());
            }
            this._update(this._i + 1);
        }));
    },

    /** Callback when any of the file monitors ({@link #_monitor})
     * fires a 'changed' event, i.e. it detects a change in the files in that
     * directory.
     *
     * This updates {@link #_childs} to reflect the changes.
     * If more than {@link MAX_FILE_DELETED_BEFORE_INVALID} files have been
     * deleted since the last update of the CommandCompleter, we are marked
     * as invalid. This means that the next {@link #update}
     * call (or attempt to autocomplete) will trigger a new
     * {@link #_update}, updating every single directory, after
     * which we are marked as valid again.
     * @param {Gio.FileMonitor} m - monitor that fired the signal
     * @param {Gio.File} f - Gio.File for the file in the directory that was changed.
     * @param {?Gio.File} of - if the change is that a file was moved, this will
     * be the Gio.File for the new path and `f` will be for the old path.
     * @param {Gio.FileMonitorEvent} type -
     * the type of file change recorded (`CREATED`, `DELETED`, `UNMOUNTED`, `MOVED` etc).
     */
    _onChanged : function(m, f, of, type) {
        if (!this._valid)
            return;
        let path = f.get_parent().get_path();
        let k = undefined;
        for (let i = 0; i < this._paths.length; i++) {
            if (this._paths[i] == path)
                k = i;
        }
        if (k === undefined) {
            return;
        }
        if (type == Gio.FileMonitorEvent.CREATED) {
            this._childs[k].push(f.get_basename());
        }
        if (type == Gio.FileMonitorEvent.DELETED) {
            this._changedCount++;
            if (this._changedCount > MAX_FILE_DELETED_BEFORE_INVALID) {
                this._valid = false;
            }
            let name = f.get_basename();
            this._childs[k] = this._childs[k].filter(function(e) {
                return e != name;
            });
        }
        if (type == Gio.FileMonitorEvent.UNMOUNTED) {
            this._childs[k] = [];
        }
    },

    /** Gets the completion from {@link #_childs} for the
     * partial text provided.
     *
     * If more than {@link MAX_FILE_DELETED_BEFORE_INVALID} files have been
     * deleted across all the `$PATH` directories since we last updated,
     * '' is returned and an update is triggered.
     *
     * Otherwise, we loop through {@link #_childs} and find
     * all elements that start with the input `text`. For example, a `text` of
     * `ter` matches (for me) both `terminal` and `terminator`.
     *
     * For all elements that match, we find the greatest common prefix of
     * these commands (for example `terminator` and `terminal` have greatest
     * common prefix `termina`).
     *
     * Once the greatest common prefix is found, this is returned (minus
     * the initial `text`). In the example above, the prefix is `termina`
     * and the input is `ter` so `mina` is returned (it will be appended
     * to the `ter` by the calling class).
     *
     * If any of these steps fails, '' is returned.
     *
     * @param {string} text - a partial command to get the completion for.
     * @returns {string} the greatest common prefix of all commands that start
     * with `text`, minus the `text` prefix (e.g. `'termina' - 'ter' == 'mina'`).
     * If there are no matching commands to `text` or there is no greatest
     * common prefix besides `text`, then '' is returned.
     */
    getCompletion: function(text) {
        let common = '';
        let notInit = true;
        if (!this._valid) {
            this._update(0);
            return common;
        }
        function _getCommon(s1, s2) {
            let k = 0;
            for (; k < s1.length && k < s2.length; k++) {
                if (s1[k] != s2[k])
                    break;
            }
            if (k == 0)
                return '';
            return s1.substr(0, k);
        }
        function _hasPrefix(s1, prefix) {
            return s1.indexOf(prefix) == 0;
        }
        for (let i = 0; i < this._childs.length; i++) {
            for (let k = 0; k < this._childs[i].length; k++) {
                if (!_hasPrefix(this._childs[i][k], text))
                    continue;
                if (notInit) {
                    common = this._childs[i][k];
                    notInit = false;
                }
                common = _getCommon(common, this._childs[i][k]);
            }
        }
        if (common.length)
            return common.substr(text.length);
        return common;
    }
};

/** creates a new RunDialog
 *
 * This calls the superclass constructor with style class 'run-dialog' and
 * constructs the dialog. Various elements have style class 'run-dialog-*'.
 *
 * It defines the internal commands {@link #_internalCommands} and
 * adds a context menu to the text entry with {@link ShellEntry.addContextMenu}.
 *
 * It also creates a {@link History.HistoryManager} storing the history in
 * {@link HISTORY_KEY} ({@link #_history}).
 *
 * It makes a local {@link CommandCompleter} for command completion, storing it
 * in {@link #_commandCompleter}.
 * 
 * @classdesc
 * @class
 * The Run Dialog is the command prompt that appears upon pressing `Alt+F2`
 * (or whatever your "Show the run command prompt" keyboard shortcut is).
 *
 * It has command completion thanks to {@link CommandCompleter} and remembers
 * past commands thanks to {@link History.HistoryManager} and remembers
 * past commands thanks to {@link History.HistoryManager}. The prompt also
 * has a right-click copy/paste menu thanks to {@link ShellEntry.addContextMenu}.
 *
 * ![`RunDialog`](pics/runDialog.png)
 * ![Error bit of the run dialog, {@link #_errorBox}](pics/runDialogError.png)
 *
 * It also has extra commands specific to gnome-shell: `lg` (launches the
 * looking glass), `r` and `restart` (restarts the shell), `debugexit`
 * (quits the shell with some debug info), and `rt` (reloads the CSS theme for the
 * shell). These are definied in {@link #_internalCommands}.
 *
 * Pressing enter executes the command; pressing Ctrl executes it in a terminal
 * using gsettings keys in {@link TERMINAL_SCHEMA}
 * (e.g. `gnome-terminal -x command`).
 *
 * @extends ModalDialog.ModalDialog
 */
function RunDialog() {
    this._init();
}

RunDialog.prototype = {
    __proto__: ModalDialog.ModalDialog.prototype,
    _init : function() {
        ModalDialog.ModalDialog.prototype._init.call(this, { styleClass: 'run-dialog' });

        this._lockdownSettings = new Gio.Settings({ schema: LOCKDOWN_SCHEMA });
        this._terminalSettings = new Gio.Settings({ schema: TERMINAL_SCHEMA });
        global.settings.connect('changed::development-tools', Lang.bind(this, function () {
            this._enableInternalCommands = global.settings.get_boolean('development-tools');
        }));
        this._enableInternalCommands = global.settings.get_boolean('development-tools');

        /** List of internal commands that the run dialog accepts.
         * The string key is the command name, and the callback is what gets
         * executed when the user runs that command.
         * @type {Object}
         * @property {function()} lg - opens the looking glass.
         * @see Main.createLookingGlass
         * @see LookingGlass.LookingGlass#open
         * @property {function()} r - restarts gnome-shell (uses `global.reexec_self`).
         * @property {function()} restart - same as `r`.
         * @property {function()} debugexit - quits gnome-shell with an error
         * code and some debug info (?): `Meta.quit(Meta.ExitCode.ERROR)`.
         * @property {function()} rt - "reload theme".
         * Reloads all the CSS stylesheets.
         * @see Main.loadTheme
         */
        this._internalCommands = { 
                                  'lg':
                                   Lang.bind(this, function() {
                                       Main.createLookingGlass().open();
                                   }),

                                   'r': Lang.bind(this, function() {
                                       global.reexec_self();
                                   }),

                                   // Developer brain backwards compatibility
                                   'restart': Lang.bind(this, function() {
                                       global.reexec_self();
                                   }),

                                   'debugexit': Lang.bind(this, function() {
                                       Meta.quit(Meta.ExitCode.ERROR);
                                   }),

                                   // rt is short for "reload theme"
                                   'rt': Lang.bind(this, function() {
                                       Main.loadTheme();
                                   })
                                 };


        let label = new St.Label({ style_class: 'run-dialog-label',
                                   text: _("Please enter a command:") });

        this.contentLayout.add(label, { y_align: St.Align.START });

        let entry = new St.Entry({ style_class: 'run-dialog-entry' });
        ShellEntry.addContextMenu(entry);

        /** The box that you type commands into. Has a context menu care of
         * {@link ShellEntry.addContextMenu}.
         * @type {Clutter.Text} */
        this._entryText = entry.clutter_text;
        this.contentLayout.add(entry, { y_align: St.Align.START });
        this.setInitialKeyFocus(this._entryText);

        /** Box that displays errors.
         * @type {St.BoxLayout} */
        this._errorBox = new St.BoxLayout({ style_class: 'run-dialog-error-box' });

        this.contentLayout.add(this._errorBox, { expand: true });

        let errorIcon = new St.Icon({ icon_name: 'dialog-error', icon_size: 24, style_class: 'run-dialog-error-icon' });

        this._errorBox.add(errorIcon, { y_align: St.Align.MIDDLE });

        this._commandError = false;

        this._errorMessage = new St.Label({ style_class: 'run-dialog-error-label' });
        this._errorMessage.clutter_text.line_wrap = true;

        this._errorBox.add(this._errorMessage, { expand: true,
                                                 y_align: St.Align.MIDDLE,
                                                 y_fill: false });

        this._errorBox.hide();

        /** Used to do file completions (as opposed to command completions).
         * @type {Gio.FilenameCompleter} */
        this._pathCompleter = new Gio.FilenameCompleter();
        /** Provides command completion.
         * @type {RunDialog.CommandCompleter} */
        this._commandCompleter = new CommandCompleter();
        this._group.connect('notify::visible', Lang.bind(this._commandCompleter, this._commandCompleter.update));

        /** Provides command history to {@link #_entryText}
         * (stored in {@link HISTORY_KEY}). Pressing the up and down keys
         * navigates through the history.
         * @type {History.HistoryManager} */
        this._history = new History.HistoryManager({ gsettingsKey: HISTORY_KEY,
                                                     entry: this._entryText });
        this._entryText.connect('key-press-event', Lang.bind(this, function(o, e) {
            let symbol = e.get_key_symbol();
            if (symbol == Clutter.Return || symbol == Clutter.KP_Enter) {
                this.popModal();
                if (Shell.get_event_state(e) & Clutter.ModifierType.CONTROL_MASK)
                    this._run(o.get_text(), true);
                else
                    this._run(o.get_text(), false);
                if (!this._commandError)
                    this.close();
                else {
                    if (!this.pushModal())
                        this.close();
                }
                return true;
            }
            if (symbol == Clutter.Escape) {
                this.close();
                return true;
            }
            if (symbol == Clutter.slash) {
                // Need preload data before get completion. GFilenameCompleter load content of parent directory.
                // Parent directory for /usr/include/ is /usr/. So need to add fake name('a').
                let text = o.get_text().concat('/a');
                let prefix;
                if (text.lastIndexOf(' ') == -1)
                    prefix = text;
                else
                    prefix = text.substr(text.lastIndexOf(' ') + 1);
                this._getCompletion(prefix);
                return false;
            }
            if (symbol == Clutter.Tab) {
                let text = o.get_text();
                let prefix;
                if (text.lastIndexOf(' ') == -1)
                    prefix = text;
                else
                    prefix = text.substr(text.lastIndexOf(' ') + 1);
                let postfix = this._getCompletion(prefix);
                if (postfix != null && postfix.length > 0) {
                    o.insert_text(postfix, -1);
                    o.set_cursor_position(text.length + postfix.length);
                    if (postfix[postfix.length - 1] == '/')
                        this._getCompletion(text + postfix + 'a');
                }
                return true;
            }
            return false;
        }));
    },

    /** Gets the completion of the specified text. If the text has a '/' in it
     * it is assumed to be a path and {@link #_pathCompleter} is used;
     * otherwise it's assumed to be a command and {@link #_commandCompleter}
     * is used.
     * @param {string} text - text to get the completion for
     * @returns {string} either a path completion or command completion.
     * @see CommandCompleter#getCompletion
     */
    _getCompletion : function(text) {
        if (text.indexOf('/') != -1) {
            return this._pathCompleter.get_completion_suffix(text);
        } else {
            return this._commandCompleter.getCompletion(text);
        }
    },

    /** Runs a command. called when the user presses Enter or Ctrl+Enter.
     * The former executes the command on its own, while the latter executes
     * it in a terminal (prepends `gnome-terminal -x` to the command, or whatever
     * is specified by {@link EXEC_KEY} and {@link EXEC_ARG_KEY}).
     *
     * It first tries to execute the command. If this fails, it checks whether
     * `input` is a file and opens the file with
     * [`Gio_app_info_launch_default_for_uri`](http://developer.gnome.org/gio/stable/GAppInfo.html#g-app-info-launch-default-for-uri).
     *
     * If all this fails, {@link #_showError} is called to display
     * the error to the user.
     *
     * @param {string} input - the command to run.
     * @param {boolean} inTerminal - whether to run it in a terminal or not.
     */
    _run : function(input, inTerminal) {
        let command = input;

        this._history.addItem(input);
        this._commandError = false;
        let f;
        if (this._enableInternalCommands)
            f = this._internalCommands[input];
        else
            f = null;
        if (f) {
            f();
        } else if (input) {
            try {
                if (inTerminal) {
                    let exec = this._terminalSettings.get_string(EXEC_KEY);
                    let exec_arg = this._terminalSettings.get_string(EXEC_ARG_KEY);
                    command = exec + ' ' + exec_arg + ' ' + input;
                }
                Util.trySpawnCommandLine(command);
            } catch (e) {
                // Mmmh, that failed - see if @input matches an existing file
                let path = null;
                if (input.charAt(0) == '/') {
                    path = input;
                } else {
                    if (input.charAt(0) == '~')
                        input = input.slice(1);
                    path = GLib.get_home_dir() + '/' + input;
                }

                if (GLib.file_test(path, GLib.FileTest.EXISTS)) {
                    let file = Gio.file_new_for_path(path);
                    try {
                        Gio.app_info_launch_default_for_uri(file.get_uri(),
                                                            global.create_app_launch_context());
                    } catch (e) {
                        // The exception from gjs contains an error string like:
                        //     Error invoking Gio.app_info_launch_default_for_uri: No application
                        //     is registered as handling this file
                        // We are only interested in the part after the first colon.
                        let message = e.message.replace(/[^:]*: *(.+)/, '$1');
                        this._showError(message);
                    }
                } else {
                    this._showError(e.message);
                }
            }
        }
    },

    /** Shows an error in the run dialog.
     * 
     * ![Error in the run dialog](pics/runDialogError.png)
     *
     * This is achieved by expanding the run dialog to show {@link #_errorBox}
     * over a time {@link DIALOG_GROW_TIME}.
     * @param {string} message - error message to display.
     */
    _showError : function(message) {
        this._commandError = true;

        this._errorMessage.set_text(message);

        if (!this._errorBox.visible) {
            let [errorBoxMinHeight, errorBoxNaturalHeight] = this._errorBox.get_preferred_height(-1);

            let parentActor = this._errorBox.get_parent();
            Tweener.addTween(parentActor,
                             { height: parentActor.height + errorBoxNaturalHeight,
                               time: DIALOG_GROW_TIME,
                               transition: 'easeOutQuad',
                               onComplete: Lang.bind(this,
                                                     function() {
                                                         parentActor.set_height(-1);
                                                         this._errorBox.show();
                                                     })
                             });
        }
    },

    /** Opens the run dialog. If the gsettings key {@link DISABLE_COMMAND_LINE_KEY}
     * is true (i.e. user not allowed to use the run dialog), nothing happens.
     * Otherwise, the dialog is launched. 
     * @override */
    open: function() {
        this._history.lastItem();
        this._errorBox.hide();
        this._entryText.set_text('');
        this._commandError = false;

        if (this._lockdownSettings.get_boolean(DISABLE_COMMAND_LINE_KEY))
            return;

        ModalDialog.ModalDialog.prototype.open.call(this);
    },

};
Signals.addSignalMethods(RunDialog.prototype);
