// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Contains the looking glass class (Alt+F2, 'lg').
 */
const Clutter = imports.gi.Clutter;
const Cogl = imports.gi.Cogl;
const GConf = imports.gi.GConf;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Meta = imports.gi.Meta;
const Pango = imports.gi.Pango;
const St = imports.gi.St;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const Lang = imports.lang;
const Mainloop = imports.mainloop;

const History = imports.misc.history;
const ExtensionSystem = imports.ui.extensionSystem;
const Link = imports.ui.link;
const ShellEntry = imports.ui.shellEntry;
const Tweener = imports.ui.tweener;
const Main = imports.ui.main;

/**
 * List of imports/in-built commands for the looking glass - run before
 * each command is executed. Includes assigning various imports (
 * `Meta = imports.gi.Meta`, `GLib = imports.gi.GLib` and so on), as well as
 * special looking glass functions `it` (the previous result) and `r(i)`
 * (retrieves result `i`). For these last two see
 * {@link LookingGlass#getIt} and {@link LookingGlass#getResult}.
 * @const
 * @default
 */
/* Imports...feel free to add here as needed */
var commandHeader = 'const Clutter = imports.gi.Clutter; ' +
                    'const GLib = imports.gi.GLib; ' +
                    'const Gtk = imports.gi.Gtk; ' +
                    'const Mainloop = imports.mainloop; ' +
                    'const Meta = imports.gi.Meta; ' +
                    'const Shell = imports.gi.Shell; ' +
                    'const Tp = imports.gi.TelepathyGLib; ' +
                    'const Main = imports.ui.main; ' +
                    'const Lang = imports.lang; ' +
                    'const Tweener = imports.ui.tweener; ' +
                    /* Utility functions...we should probably be able to use these
                     * in the shell core code too. */
                    'const stage = global.stage; ' +
                    'const color = function(pixel) { let c= new Clutter.Color(); c.from_pixel(pixel); return c; }; ' +
                    /* Special lookingGlass functions */
                       'const it = Main.lookingGlass.getIt(); ' +
                    'const r = Lang.bind(Main.lookingGlass, Main.lookingGlass.getResult); ';
/** GSettings key under 'org.gnome.shell' that the looking glass command history
 * is stored.
 * @const
 * @default
 */
const HISTORY_KEY = 'looking-glass-history';

/** Object storing data for each tab in the notebook. Used internally
 * in the {@link LookingGlass.Notebook} class.
 * @typedef TabData
 * @type {Object}
 * @property {Clutter.Actor} child - the actor representing the content
 * of that tab page (could be a St widget too).
 * @property {St.Button} label - the tab title (i.e. click on it in the list of
 * tab titles to switch to that page).
 * @property {St.BoxLayout} labelBox - contains the tab title (TabData.label)
 * inside it. Has style class 'notebook-tab' along with allowing styling for a
 * ':hover' and ':selected' pseudo style.
 * @property {St.ScrollView} scrollView - the scroll view containing the
 * page content (TabData.child).
 * @property {boolean} _scrollToBottom - whether the tab is scrolled to
 * the bottom (do NOT set this property; use {@link Notebook#scrollToBottom}).
 */

/** creates a new Notebook
 * @classdesc The Looking Glass consists of multiple 'tabs' (Evaluator; Windows;
 * Errors; Memory; Extensions); the notebook is what holds them all. 
 * This class defines the tab controls ({@link #tabControls})
 * ability to add/switch tab pages, and an area where the page content will
 * appear ({@link #actor}).
 *
 * Note - it appears the user is responsible for displaying *both* the actor
 * (page content part) and the tab controls - the tab controls are not included
 * in the actor.
 * @class
 */
function Notebook() {
    this._init();
}

Notebook.prototype = {
    _init: function() {
        /** The current page of the notebook is displayed in `this.actor`,
         * being a St.BoxLayout. Each tab of the notebook is added
         * into its own `St.ScrollView` which is added to `this.actor`, and
         * switching between tabs is a matter of hiding/showing the appropriate
         * scrollview(s).
         * @type {St.BoxLayout}
         */
        this.actor = new St.BoxLayout({ vertical: true });

        /** The list of tab titles is a `St.BoxLayout` with style class
         * 'labels'.
         * @type {St.BoxLayout}
         */
        this.tabControls = new St.BoxLayout({ style_class: 'labels' });

        this._selectedIndex = -1;
        /** Holds the data for the tabs.
         * @type {[TabData]{@link LookingGlass.TabData}[]}
         */
        this._tabs = [];
    },

    /** Adds a page to the notebook. Stores the tab's data in
     * {@link #_tabs}. If this is the first tab of the notebook it will
     * open that tab page, otherwise it will stay on whatever the current tab
     * page is.
     * @param {string} name - the name to be displayed on that tab's title.
     * @param {Clutter.Actor} child - some sort of Clutter actor or St widget
     * containing the tab's content. Will be added to a `St.ScrollView` to make
     * it scrollable.
     */
    appendPage: function(name, child) {
        let labelBox = new St.BoxLayout({ style_class: 'notebook-tab',
                                          reactive: true,
                                          track_hover: true });
        let label = new St.Button({ label: name });
        label.connect('clicked', Lang.bind(this, function () {
            this.selectChild(child);
            return true;
        }));
        labelBox.add(label, { expand: true });
        this.tabControls.add(labelBox);

        let scrollview = new St.ScrollView({ x_fill: true, y_fill: true });
        scrollview.get_hscroll_bar().hide();
        scrollview.add_actor(child);

        let tabData = { child: child,
                        labelBox: labelBox,
                        label: label,
                        scrollView: scrollview,
                        _scrollToBottom: false };
        this._tabs.push(tabData);
        scrollview.hide();
        this.actor.add(scrollview, { expand: true });

        let vAdjust = scrollview.vscroll.adjustment;
        vAdjust.connect('changed', Lang.bind(this, function () { this._onAdjustScopeChanged(tabData); }));
        vAdjust.connect('notify::value', Lang.bind(this, function() { this._onAdjustValueChanged(tabData); }));

        if (this._selectedIndex == -1)
            this.selectIndex(0);
    },

    /** Unselects the current tab. The user should not use this; they should use
     * {@link #selectIndex} or {@link #selectChild} to switch
     * tabs.
     *
     * This removes the 'selected' style from the current tab's 
     * {@link TabData#labelBox} (the notebook tab) and hides the tab's
     * scrollview too */
    _unselect: function() {
        if (this._selectedIndex < 0)
            return;
        let tabData = this._tabs[this._selectedIndex];
        tabData.labelBox.remove_style_pseudo_class('selected');
        tabData.scrollView.hide();
        this._selectedIndex = -1;
    },

    /** Selects a tab by index.
     *
     * Additionally adds the 'selected' pseudo style class to the current tab's
     * title, and emits a signal 'selection' with the new tab's
     * {@link TabData#child}.
     *
     * @param {number} index - (0-based) index of the tab to select.
     */
    selectIndex: function(index) {
        if (index == this._selectedIndex)
            return;
        if (index < 0) {
            this._unselect();
            this.emit('selection', null);
            return;
        }

        // Focus the new tab before unmapping the old one
        let tabData = this._tabs[index];
        if (!tabData.scrollView.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false))
            this.actor.grab_key_focus();

        this._unselect();

        tabData.labelBox.add_style_pseudo_class('selected');
        tabData.scrollView.show();
        this._selectedIndex = index;
        this.emit('selection', tabData.child);
    },

    /** Selects a tab by content.
     * @param {Clutter.Actor} child - the page content of the tab you wish
     * to select. This has to be the same `child` that was passed into
     * {@link #appendPage}.
     *
     * @see #selectIndex
     */
    selectChild: function(child) {
        if (child == null)
            this.selectIndex(-1);
        else {
            for (let i = 0; i < this._tabs.length; i++) {
                let tabData = this._tabs[i];
                if (tabData.child == child) {
                    this.selectIndex(i);
                    return;
                }
            }
        }
    },

    /** Scrolls a tab to the bottom.
     * @param {number} index - (0-based) index of the tab to scroll to the
     * bottom. This does not switch tabs if `index` is not the current tab.
     */
    scrollToBottom: function(index) {
        let tabData = this._tabs[index];
        tabData._scrollToBottom = true;

    },

    /** Callback when the user scrolls vertically in a tab. It updates whether
     * a tab is scrolled to the bottom or not (?)
     * @param {TabData} tabData - data for the tab containing the scrollview
     * that was just scrolled.
     */
    _onAdjustValueChanged: function (tabData) {
        let vAdjust = tabData.scrollView.vscroll.adjustment;
        if (vAdjust.value < (vAdjust.upper - vAdjust.lower - 0.5))
            tabData._scrolltoBottom = false;
    },

    /** Callback when any of the scrollview limits change (?) - I think it
     * basically makes sure that if the tab has `_scrollToBottom` set to `true`,
     * the tab actually does scroll to the bottom.
     *
     * @param {TabData} tabData - data for the tab containing the scrollview
     * that changed.
     */
    _onAdjustScopeChanged: function (tabData) {
        if (!tabData._scrollToBottom)
            return;
        let vAdjust = tabData.scrollView.vscroll.adjustment;
        vAdjust.value = vAdjust.upper - vAdjust.page_size;
    }
};
Signals.addSignalMethods(Notebook.prototype);

/** Returns a string representation of an object.
 * This string representation is what gets returned when you type that object
 * into the looking glass (on the `r(i) = ...` line.
 *
 * @param {any} o - object to convert to string.
 * @returns {string} either "<js function>" if `o` is a function (otherwise the
 * output is too verbose for the looking glass console), or `o.toString()`
 * otherwise.
 */
function objectToString(o) {
    if (typeof(o) == typeof(objectToString)) {
        // special case this since the default is way, way too verbose
        return '<js function>';
    } else {
        return '' + o;
    }
}

/**
 * Creates a new Object Link.
 * @param {Object} o - the object the link is for.
 * @param {string} [title] - the text to display on the link (if not provided,
 * this will be [`objectToString(o)`]{@link objectToString}).
 * @classdesc an ObjLink is a link for a javascript object, and it opens the
 * {@link ObjInspector} for that object when clicked.
 * @extends Link.Link
 * @class
 */
function ObjLink(o, title) {
    this._init(o, title);
}

ObjLink.prototype = {
    __proto__: Link.Link,

    _init: function(o, title) {
        let text;
        if (title)
            text = title;
        else
            text = objectToString(o);
        text = GLib.markup_escape_text(text, -1);
        this._obj = o;
        Link.Link.prototype._init.call(this, { label: text });
        this.actor.get_child().single_line_mode = true;
        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
    },

    /** callback for when the link is clicked. It calls
     * {@link LookingGlass#inspectObject} on the object, which in turn calls
     * {@link ObjInspector} on the object.
     */
    _onClicked: function (link) {
        Main.lookingGlass.inspectObject(this._obj, this.actor);
    }
};

/** creates a new Result
 * @param {string} command - the command that the result is for (the looking
 * glass adds a preceding '>>> ' to these).
 * @param {object} o - the result object (result of the command).
 * @param {number} index - the index of this result in all the results that
 * have been entered into the looking glass so far (the purpose of this
 * is to display the label `r(index) = <o.toString()>` in the looking glass).
 * @see LookingGlass#_pushResult
 * @classdesc ![Looking Glass `Result` object](pics/LookingGlassResult.png)
 *
 * This is the command/result visual element that appears in the
 * looking glass when you type in a command.
 *
 * It consists of the text '>>>' with the command entered, and below that the
 * text 'r(i) =' (`i` being the 0-based index of the command) with an
 * {@link ObjLink} to the result of evaluating that command.
 *
 * The '>>>' with command is a `St.Label`; the 'r(i) =' is a `St.Label`; the
 * result of evaluating the command is a {@link ObjLink}; these are all packed
 * in `this.actor` being a vertical `St.BoxLayout`.
 * @class
 * @see ObjLink
 */
function Result(command, o, index) {
    this._init(command, o, index);
}

Result.prototype = {
    _init : function(command, o, index) {
        this.index = index;
        this.o = o;

        this.actor = new St.BoxLayout({ vertical: true });

        let cmdTxt = new St.Label({ text: command });
        cmdTxt.clutter_text.ellipsize = Pango.EllipsizeMode.END;
        this.actor.add(cmdTxt);
        let box = new St.BoxLayout({});
        this.actor.add(box);
        let resultTxt = new St.Label({ text: 'r(' + index + ') = ' });
        resultTxt.clutter_text.ellipsize = Pango.EllipsizeMode.END;
        box.add(resultTxt);
        let objLink = new ObjLink(o);
        box.add(objLink.actor);
        let line = new Clutter.Rectangle({ name: 'Separator' });
        let padBin = new St.Bin({ name: 'Separator', x_fill: true, y_fill: true });
        padBin.add_actor(line);
        this.actor.add(padBin);
    }
};

/** creates a new WindowList
 * @classdesc This is the Windows tab of the looking glass, displaying
 * information about your currently-open windows.
 * ![Looking Glass `WindowList` tab](pics/LookingGlassWindowList.png)
 * @class
 */
function WindowList() {
    this._init();
}

WindowList.prototype = {
    _init : function () {
        /** the content of the windows tab is all stored here.
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ name: 'Windows', vertical: true, style: 'spacing: 8px' });
        let tracker = Shell.WindowTracker.get_default();
        this._updateId = Main.initializeDeferredWork(this.actor, Lang.bind(this, this._updateWindowList));
        global.display.connect('window-created', Lang.bind(this, this._updateWindowList));
        tracker.connect('tracked-windows-changed', Lang.bind(this, this._updateWindowList));
    },

    /** callback for when a window is created/destroyed. Updates the window list
     * to remain in sync with the current windows.
     *
     * Each entry in the window list consists of:
     *
     * * an {@link ObjLink} to the window's Meta.Window (the text of the link is
     * the window's title);
     * * below that, the `wmclass` (window manager class) of the window's
     * Meta.Window in a `St.Label`;
     * * below that, an {@link ObjLink} to the window's *application* (text
     * being the application's ID), along with the icon for that app.
     */
    _updateWindowList: function() {
        this.actor.get_children().forEach(function (actor) { actor.destroy(); });
        let windows = global.get_window_actors();
        let tracker = Shell.WindowTracker.get_default();
        for (let i = 0; i < windows.length; i++) {
            let metaWindow = windows[i].metaWindow;
            // Avoid multiple connections
            if (!metaWindow._lookingGlassManaged) {
                metaWindow.connect('unmanaged', Lang.bind(this, this._updateWindowList));
                metaWindow._lookingGlassManaged = true;
            }
            let box = new St.BoxLayout({ vertical: true });
            this.actor.add(box);
            let windowLink = new ObjLink(metaWindow, metaWindow.title);
            box.add(windowLink.actor, { x_align: St.Align.START, x_fill: false });
            let propsBox = new St.BoxLayout({ vertical: true, style: 'padding-left: 6px;' });
            box.add(propsBox);
            propsBox.add(new St.Label({ text: 'wmclass: ' + metaWindow.get_wm_class() }));
            let app = tracker.get_window_app(metaWindow);
            if (app != null && !app.is_window_backed()) {
                let icon = app.create_icon_texture(22);
                let propBox = new St.BoxLayout({ style: 'spacing: 6px; ' });
                propsBox.add(propBox);
                propBox.add(new St.Label({ text: 'app: ' }), { y_fill: false });
                let appLink = new ObjLink(app, app.get_id());
                propBox.add(appLink.actor, { y_fill: false });
                propBox.add(icon, { y_fill: false });
            } else {
                propsBox.add(new St.Label({ text: '<untracked>' }));
            }
        }
    }
};
Signals.addSignalMethods(WindowList.prototype);

/** creates a new ObjInspector
 * @classdesc This is what launches whenever you click on an {@link ObjLink}.
 * ![Looking Glass `ObjInspector`](pics/LookingGlassObjInspector.png)
 *
 * It's a popup dialog describing the object you have clicked on - either
 * showing a list of properties (if it is an object), or displaying the type
 * of the item otherwise (like 'string', 'number').
 *
 * It has a limited (1-level) history allowing you to go 'Back', as well as
 * an 'Insert' button letting you insert the currently-inspected object into
 * the Looking Glass console.
 * @class
 */
function ObjInspector() {
    this._init();
}

ObjInspector.prototype = {
    _init : function () {
        this._obj = null;
        this._previousObj = null;

        this._parentList = [];

        /** the top-level actor for the ObjInspector - a ScrollView to allow
         * scrolling of contents.
         * @type {St.ScrollView} */
        this.actor = new St.ScrollView({ x_fill: true, y_fill: true });
        this.actor.get_hscroll_bar().hide();
        /** The "actual" actor for the ObjInspector - a vertical BoxLayout with
         * style 'lg-dialog' to which all the contents are added.
         * @type {St.BoxLayout}
         */
        this._container = new St.BoxLayout({ name: 'LookingGlassPropertyInspector',
                                             style_class: 'lg-dialog',
                                             vertical: true });
        this.actor.add_actor(this._container);
    },

    /** Selects an object to inspect.
     *
     * Also stores the previous object in the history so that pressing 'Back'
     * will take you there (if `skipPrevious` is omitted or `false`).
     *
     * This function:
     *
     * * adds a title for the object "Inspecting: %s: %s", where the first
     * string is `typeof(obj)`, and the second is [`objectToString(obj)`]{@link objectToString}.
     * * adds the 'Insert' and 'Back' buttons (`St.Button`)
     * * adds the 'close' button (being the 'X') (`St.Button`)
     * * if the object has `typeof(obj) === 'object'`, it will loop through the
     * object's properties using a `for .. in` loop and display them all
     * (including inherited properties).
     *
     * All of these elements get added to {@link #_container}.
     *
     * @param {any} obj - the object to inspect.
     * @param {boolean} [skipPrevious] - whether to skip storing the previous
     * object in the history (clicking the 'Back' button).
     */
    selectObject: function(obj, skipPrevious) {
        if (!skipPrevious)
            this._previousObj = this._obj;
        else
            this._previousObj = null;
        this._obj = obj;

        this._container.get_children().forEach(function (child) { child.destroy(); });

        let hbox = new St.BoxLayout({ style_class: 'lg-obj-inspector-title' });
        this._container.add_actor(hbox);
        let label = new St.Label({ text: 'Inspecting: %s: %s'.format(typeof(obj),
                                                                     objectToString(obj)) });
        label.single_line_mode = true;
        hbox.add(label, { expand: true, y_fill: false });
        let button = new St.Button({ label: 'Insert', style_class: 'lg-obj-inspector-button' });
        button.connect('clicked', Lang.bind(this, this._onInsert));
        hbox.add(button);

        if (this._previousObj != null) {
            button = new St.Button({ label: 'Back', style_class: 'lg-obj-inspector-button' });
            button.connect('clicked', Lang.bind(this, this._onBack));
            hbox.add(button);
        }

        button = new St.Button({ style_class: 'window-close' });
        button.connect('clicked', Lang.bind(this, this.close));
        hbox.add(button);
        if (typeof(obj) == typeof({})) {
            for (let propName in obj) {
                let valueStr;
                let link;
                try {
                    let prop = obj[propName];
                    link = new ObjLink(prop).actor;
                } catch (e) {
                    link = new St.Label({ text: '<error>' });
                }
                let hbox = new St.BoxLayout();
                let propText = propName + ': ' + valueStr;
                hbox.add(new St.Label({ text: propName + ': ' }));
                hbox.add(link);
                this._container.add_actor(hbox);
            }
        }
    },

    /** Opens the object inspector.
     *
     * Note - the object inspector always opens on the middle of your screen
     * (unless you have multiple in which case it appears to open in the middle
     * of all your screens, which can be annoying).
     *
     * However whenever {@link #open} is called it looks like the
     * *anchor point* of the object inspector is shifted to be centred over
     * `sourceActor`. This **does not** modify the position of the object
     * inspector; all it does is make sure that position `(0, 0)` points to
     * the centre of `sourceActor` (`clutter_actor_move_anchor_point` changes
     * the anchor point of the actor and adjusts its 'x' and 'y' values such
     * that the actor *does not change its visual position*).
     *
     * I have **no idea** why the class does this as it seems to be used
     * nowhere else. It might be to do with how the object inspector tweens in -
     * perhaps it is meant to look like it grows out of the source actor up
     * to full size?
     *
     * If the `sourceActor` argument is not provided, then instead of the
     * object inspector tweening in, it will simply appear.
     *
     * @param {Clutter.Actor} [sourceActor] - the actor over which to open the
     * object inspector. Optional.
     */
    open: function(sourceActor) {
        if (this._open)
            return;
        this._previousObj = null;
        this._open = true;
        this.actor.show();
        if (sourceActor) {
            this.actor.set_scale(0, 0);
            let [sourceX, sourceY] = sourceActor.get_transformed_position();
            let [sourceWidth, sourceHeight] = sourceActor.get_transformed_size();
            this.actor.move_anchor_point(Math.floor(sourceX + sourceWidth / 2),
                                         Math.floor(sourceY + sourceHeight / 2));
            Tweener.addTween(this.actor, { scale_x: 1, scale_y: 1,
                                           transition: 'easeOutQuad',
                                           time: 0.2 });
        } else {
            this.actor.set_scale(1, 1);
        }
    },

    /** closes the object inspector and clears its object history. */
    close: function() {
        if (!this._open)
            return;
        this._open = false;
        this.actor.hide();
        this._previousObj = null;
        this._obj = null;
    },

    /** callback for when the user clicks the 'Insert' button. This inserts the
     * currently-inspected object into the looking glass (see 
     * {@link LookingGlass#insertObject}).
     */
    _onInsert: function() {
        let obj = this._obj;
        this.close();
        Main.lookingGlass.insertObject(obj);
    },

    /** callback when the user clicks the 'Back' button. This causes the
     * inspector to inspect the previous object. */
    _onBack: function() {
        this.selectObject(this._previousObj, true);
    }
};

/** This draws a red, 76% transparent border around the given actor.
 *
 * It is used by the {@link Inspector} to indicate what actor is currently
 * being hovered over, and by the {@link LookingGlass} to outline what
 * an actor if it is the result of a command and also on the global stage.
 *
 * The reason we can't just set 'style = border: 1px solid red' on the
 * actor is that only St widgets support this style stuff, whereas this function
 * will work on *any* `Clutter.Actor` (including St ones).
 *
 * This function connects to the actor's 'paint' signal, and after the
 * actor has finished painting, draws a border around the actor (using Cogl).
 *
 * @param {Clutter.Actor} actor - actor to outline in red.
 * @returns {number} signal ID returned by calling 
 * `actor.connect_after('paint', ...`.
 */
function addBorderPaintHook(actor) {
    let signalId = actor.connect_after('paint',
        function () {
            let color = new Cogl.Color();
            color.init_from_4ub(0xff, 0, 0, 0xc4);
            Cogl.set_source_color(color);

            let geom = actor.get_allocation_geometry();
            let width = 2;

            // clockwise order
            Cogl.rectangle(0, 0, geom.width, width);
            Cogl.rectangle(geom.width - width, width,
                           geom.width, geom.height);
            Cogl.rectangle(0, geom.height,
                           geom.width - width, geom.height - width);
            Cogl.rectangle(0, geom.height - width,
                           width, width);
        });

    actor.queue_redraw();
    return signalId;
}

/** creates a new Inspector (![Looking Glass `Inspector` icon](pics/LookingGlassInspectorIcon.png)).
 *
 * To capture all button-press/key events, we create `Shell.GenericContainer`.
 * Inside this generic container (which is not stored in the
 * class), a reactive `St.BoxLayout` ({@link #_eventHandler} uses
 * `Clutter.grab_pointer` and `Clutter.grab_keyboard` to grab all events to it.
 *
 * @classdesc
 * The `Inspector` is the 'picker' icon in the top-left of the looking glass
 * (![Looking Glass Inspector icon](pics/LookingGlassInspectorIcon.png)).
 *
 * Clicking on it closes the looking glass and lets the user examine any
 * Clutter/St actor from the stage. Clicking on an actor will close the
 * inspector and reopen the looking glass with that object inserted in as
 * the most recent command so that the user can then play with it.
 *
 * ![Looking Glass Inspector whilst inspecting](pics/LookingGlassInspector.png)
 *
 * Whilst the user is hovering their mouse over an actor, that actor has a
 * red border drawn around it (see {@link addBorderPaintHook}).
 *
 * Scrolling the use up while over an actor will select the currently-selected
 *  actor's ancestor; scrolling down will select its descendant.
 *
 * In the middle of the screen some information about the currently-selected
 * actor is shown.
 * @class
 */
function Inspector() {
    this._init();
}

Inspector.prototype = {
    _init: function() {
        let container = new Shell.GenericContainer({ width: 0,
                                                     height: 0 });
        container.connect('allocate', Lang.bind(this, this._allocate));
        Main.uiGroup.add_actor(container);

        let eventHandler = new St.BoxLayout({ name: 'LookingGlassDialog',
                                              vertical: false,
                                              reactive: true });
        /** This object is used to both display the label in the middle of the
         * screen ({@link #_displayText}) and to capture all mouse/
         * keyboard events (it covers the entire screen).
         *
         * This lives in a `Shell.GenericContainer` that is connected to
         * {@link #_allocate} but otherwise is not stored.
         * @type {St.BoxLayout} */
        this._eventHandler = eventHandler;
        container.add_actor(eventHandler);
        /** This is the label in the middle of the screen that shows information
         * about the currently-selected actor.
         * @type {St.Label} */
        this._displayText = new St.Label();
        eventHandler.add(this._displayText, { expand: true });

        this._borderPaintTarget = null;
        this._borderPaintId = null;
        eventHandler.connect('destroy', Lang.bind(this, this._onDestroy));
        eventHandler.connect('key-press-event', Lang.bind(this, this._onKeyPressEvent));
        eventHandler.connect('button-press-event', Lang.bind(this, this._onButtonPressEvent));
        eventHandler.connect('scroll-event', Lang.bind(this, this._onScrollEvent));
        eventHandler.connect('motion-event', Lang.bind(this, this._onMotionEvent));
        Clutter.grab_pointer(eventHandler);
        Clutter.grab_keyboard(eventHandler);

        // this._target is the actor currently shown by the inspector.
        // this._pointerTarget is the actor directly under the pointer.
        // Normally these are the same, but if you use the scroll wheel
        // to drill down, they'll diverge until you either scroll back
        // out, or move the pointer outside of _pointerTarget.
        /** The actor currently shown by the inspector. Normally the same as
         * {@link #_pointerTarget}, except in the case where the scroll
         * wheel has been used to select an anscestor/descendant of
         * {@link #_pointerTarget}. (in which case `this._target`
         * is the ancestor/descendant, whereas `this._pointerTarget` remains
         * the same).
         * @type {Clutter.Actor} */
        this._target = null;
        /** The actor directly under the pointer. Normally the same as
         * {@link #_target}, except in the case where the scroll
         * wheel has been used to select an anscestor/descendant of
         * {@link #_pointerTarget}.
         * @type {Clutter.Actor} */
        this._pointerTarget = null;
    },

    /** callback for 'allocate' signal of the `Shell.GenericContainer` that
     * contains {@link #_eventHandler}.
     *
     * It just allocates the size of {@link #_displayText} to the
     * {@link #_eventHandler} and centres it on the primary monitor.
     */
    _allocate: function(actor, box, flags) {
        if (!this._eventHandler)
            return;

        let primary = Main.layoutManager.primaryMonitor;

        let [minWidth, minHeight, natWidth, natHeight] =
            this._eventHandler.get_preferred_size();

        let childBox = new Clutter.ActorBox();
        childBox.x1 = primary.x + Math.floor((primary.width - natWidth) / 2);
        childBox.x2 = childBox.x1 + natWidth;
        childBox.y1 = primary.y + Math.floor((primary.height - natHeight) / 2);
        childBox.y2 = childBox.y1 + natHeight;
        this._eventHandler.allocate(childBox, flags);
    },

    /** stops the Inspector. This ungrabs the pointer and keyboard from
     * {@link #_eventHandler}, destroys it, and emits a 'closed'
     * signal.
     * @see #_onDestroy
     */
    _close: function() {
        Clutter.ungrab_pointer(this._eventHandler);
        Clutter.ungrab_keyboard(this._eventHandler);
        this._eventHandler.destroy();
        this._eventHandler = null;
        this.emit('closed');
    },

    /** callback when {@link #_eventHandler} is destroyed.
     * Removes disconnects the signal ID that is causing the red border to be
     * drawn around the currently-selected actor.
     * @see addBorderPaintHook
     */
    _onDestroy: function() {
        if (this._borderPaintTarget != null)
            this._borderPaintTarget.disconnect(this._borderPaintId);
    },

    /** callback when a key press event is captured whilst inspecting.
     * If the key pressed was 'Escape' the inspector is closed. All other
     * key presses are ignored.
     * @see #_close
     */
    _onKeyPressEvent: function (actor, event) {
        if (event.get_key_symbol() == Clutter.Escape)
            this._close();
        return true;
    },

    /** callback when a button press event is captured whilst inspecting.
     *
     * If there is a currently-selected actor, this emits a 'target' signal
     * with the selected actor and its x/y coordinates (relative to the stage).
     * (the LookingGlass deals with the 'target' signal by inserting the target
     * to the looking glass console).
     *
     * Then the inspector is closed.
     * @see #_close
     * @see LookingGlass
     */
    _onButtonPressEvent: function (actor, event) {
        if (this._target) {
            let [stageX, stageY] = event.get_coords();
            this.emit('target', this._target, stageX, stageY);
        }
        this._close();
        return true;
    },

    /** callback when the user uses the scroll wheel whilst inspecting.
     *
     * If they scrolled up, the currently selected object's immediate parent is
     * selected as the new selected object.
     *
     * If they scrolled down, the descendant is used instead.
     *
     * So - updates {@link #_target}.
     *
     * Then {@link #_update} is called which updates the red border
     * to be around the right actor, as well as updating the label describing
     * the currently-selected actor.
     *
     * @see #_update
     */
    _onScrollEvent: function (actor, event) {
        switch (event.get_scroll_direction()) {
        case Clutter.ScrollDirection.UP:
            // select parent
            let parent = this._target.get_parent();
            if (parent != null) {
                this._target = parent;
                this._update(event);
            }
            break;

        case Clutter.ScrollDirection.DOWN:
            // select child
            if (this._target != this._pointerTarget) {
                let child = this._pointerTarget;
                while (child) {
                    let parent = child.get_parent();
                    if (parent == this._target)
                        break;
                    child = parent;
                }
                if (child) {
                    this._target = child;
                    this._update(event);
                }
            }
            break;

        default:
            break;
        }
        return true;
    },

    /** callback when the user moves the mouse whilst inspecting.
     *
     * Simply calls {@link #_update} to update the currently-selected
     * actor.
     */
    _onMotionEvent: function (actor, event) {
        this._update(event);
        return true;
    },

    /** callback to update the currently-selected actor.
     * @param {Clutter.Event} event - some sort of event from which we will
     * retrieve the coordinates via `event.get_coords()` in order to pick
     * the actor at those coordinates.
     *
     * This:
     *
     * * updates {@link #_pointerTarget} to be the actor directly
     * underneath the mouse (event coordinates);
     * * updates {@link #_displayText} to show the X and Y coordinates
     * of the pointer (or from the event) as well as the string representation
     * of the current target ({@link #_target});
     * * draws a red border around the current target ({@link #_target});
     * see {@link addBorderPaintHook}.
     * @see addBorderPaintHook
     */
    _update: function(event) {
        let [stageX, stageY] = event.get_coords();
        let target = global.stage.get_actor_at_pos(Clutter.PickMode.ALL,
                                                   stageX,
                                                   stageY);

        if (target != this._pointerTarget)
            this._target = target;
        this._pointerTarget = target;

        let position = '[inspect x: ' + stageX + ' y: ' + stageY + ']';
        this._displayText.text = '';
        this._displayText.text = position + ' ' + this._target;

        if (this._borderPaintTarget != this._target) {
            if (this._borderPaintTarget != null)
                this._borderPaintTarget.disconnect(this._borderPaintId);
            this._borderPaintTarget = this._target;
            this._borderPaintId = addBorderPaintHook(this._target);
        }
    }
};

Signals.addSignalMethods(Inspector.prototype);

/** creates a new ErrorLog (tab)
 * @classdesc This is the 'Errors' tab of the looking glass.
 * ![Looking Glass `ErrorLog` tab](pics/LookingGlassErrorLog.png)
 *
 * It uses {@link Main._getAndClearErrorStack} to update its list of errors
 * and display them (called whenever the tab is switched to).
 * @see Main._getAndClearErrorStack
 * @class
 */
function ErrorLog() {
    this._init();
}

ErrorLog.prototype = {
    _init: function() {
        /** The actor for this tab's content.
         * @type {St.BoxLayout}
         */
        this.actor = new St.BoxLayout();
        /** The label that actually holds all the errors. 
         * @type {St.Label} */
        this.text = new St.Label();
        this.actor.add(this.text);
        // We need to override StLabel's default ellipsization when
        // using line_wrap; otherwise ClutterText's layout is going
        // to constrain both the width and height, which prevents
        // scrolling.
        this.text.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this.text.clutter_text.line_wrap = true;
        this.actor.connect('notify::mapped', Lang.bind(this, this._renderText));
    },

    /** formats a Date to a string time (UTC).
     *
     * Example '2012-10-15T01:46:16Z'
     * @param {Date} d - the date to format.
     * @returns {string} a time (I don't know enough about date specifiers to
     * give it to you): example '2012-10-15T01:46:16Z'. Something like
     * 'yyyy-mm-ddThh:mm:ssZ', all in UTC.
     */
    _formatTime: function(d){
        function pad(n) { return n < 10 ? '0' + n : n; }
        return d.getUTCFullYear()+'-'
            + pad(d.getUTCMonth()+1)+'-'
            + pad(d.getUTCDate())+'T'
            + pad(d.getUTCHours())+':'
            + pad(d.getUTCMinutes())+':'
            + pad(d.getUTCSeconds())+'Z';
    },

    /** called whenever the user switches to the error tab.
     *
     * This uses {@link Main._getAndClearErrorStack} to retreive all errors
     * from when this function was last called, and adds an entry for
     * each of them into {@link #text}.
     *
     * The format for each error is "%s t=%s %s", where the first string is
     * the error's category ('info', 'error', 'debug'), the second is
     * {@link #_formatTime} called on the error's timestamp, and
     * the last is the error message.
     * @see Main._getAndClearErrorStack
     * @TODO add a typedef for items in the error stack?
     */
    _renderText: function() {
        if (!this.actor.mapped)
            return;
        let text = this.text.text;
        let stack = Main._getAndClearErrorStack();
        for (let i = 0; i < stack.length; i++) {
            let logItem = stack[i];
            text += logItem.category + ' t=' + this._formatTime(new Date(logItem.timestamp)) + ' ' + logItem.message + '\n';
        }
        this.text.text = text;
    }
};

/** creates a new Memory (tab).
 * @classdesc This is the 'Memory' tab in the Looking Glass.
 *
 * ![Looking Glass `Memory` tab](pics/LookingGlassMemoryTab.png)
 *
 * It shows various memory statistics from `global.get_memory_info()`, updated
 * whenever the user switches to that tab.
 *
 * It also has a button `Full GC` that calls `global.gc()` and then re-updates
 * the memory info.
 * @class
 */
function Memory() {
    this._init();
}

Memory.prototype = {
    _init: function() {
        /** The content for the tab is a vertical box layout.
         *
         * Each memory statistic ('js bytes', 'gjs_boxed', ...) is a `St.Label`
         * added to this.
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ vertical: true });
        this._glibc_uordblks = new St.Label();
        this.actor.add(this._glibc_uordblks);

        this._js_bytes = new St.Label();
        this.actor.add(this._js_bytes);

        this._gjs_boxed = new St.Label();
        this.actor.add(this._gjs_boxed);

        this._gjs_gobject = new St.Label();
        this.actor.add(this._gjs_gobject);

        this._gjs_function = new St.Label();
        this.actor.add(this._gjs_function);

        this._gjs_closure = new St.Label();
        this.actor.add(this._gjs_closure);

        this._last_gc_seconds_ago = new St.Label();
        this.actor.add(this._last_gc_seconds_ago);

        this._gcbutton = new St.Button({ label: 'Full GC',
                                         style_class: 'lg-obj-inspector-button' });
        this._gcbutton.connect('clicked', Lang.bind(this, function () { global.gc(); this._renderText(); }));
        this.actor.add(this._gcbutton, { x_align: St.Align.START,
                                         x_fill: false });

        this.actor.connect('notify::mapped', Lang.bind(this, this._renderText));
    },

    /** called whenever the user switches to this tab - calls
     * `global.get_memory_info()` and updates all the labels with the new
     * data. */
    _renderText: function() {
        if (!this.actor.mapped)
            return;
        let memInfo = global.get_memory_info();
        this._glibc_uordblks.text = 'glibc_uordblks: ' + memInfo.glibc_uordblks;
        this._js_bytes.text = 'js bytes: ' + memInfo.js_bytes;
        this._gjs_boxed.text = 'gjs_boxed: ' + memInfo.gjs_boxed;
        this._gjs_gobject.text = 'gjs_gobject: ' + memInfo.gjs_gobject;
        this._gjs_function.text = 'gjs_function: ' + memInfo.gjs_function;
        this._gjs_closure.text = 'gjs_closure: ' + memInfo.gjs_closure;
        this._last_gc_seconds_ago.text = 'last_gc_seconds_ago: ' + memInfo.last_gc_seconds_ago;
    }
};

/** creates a new Extensions tab.
 * @classdesc This is the "Extensions" tab in the looking glass.
 *
 * ![Looking Glass `Extensions` tab](pics/LookingGlassExtensions.png)
 *
 * It shows a list of installed extensions along with their descriptions,
 * status (enabled/disabled), and links to their web page (from `metadata.json`)
 * and to view the source (opens the extension folder in your file browser).
 *
 * It interacts with {@link namespace:ExtensionSystem} to determine
 * extension information.
 *
 * @see namespace:ExtensionSystem
 * @class
 */
function Extensions() {
    this._init();
}

Extensions.prototype = {
    _init: function() {
        this.actor = new St.BoxLayout({ vertical: true,
                                        name: 'lookingGlassExtensions' });
        this._noExtensions = new St.Label({ style_class: 'lg-extensions-none',
                                             text: _("No extensions installed") });
        this._numExtensions = 0;
        this._extensionsList = new St.BoxLayout({ vertical: true,
                                                  style_class: 'lg-extensions-list' });
        this._extensionsList.add(this._noExtensions);
        this.actor.add(this._extensionsList);

        for (let uuid in ExtensionSystem.extensionMeta)
            this._loadExtension(null, uuid);

        ExtensionSystem.connect('extension-loaded',
                                Lang.bind(this, this._loadExtension));
    },

    /** callback when an extension is loaded (the 'extension-loaded' signal
     * of {@link namespace:ExtensionSystem}).
     *
     * This adds an entry for the newly-loaded extension to the page content.
     * @param {Object} o - object that emitted the signal
     * @param {string} uuid - UUID of the newly-loaded extension.
     * @see {@link #_createExtensionDisplay}.
     */
    _loadExtension: function(o, uuid) {
        let extension = ExtensionSystem.extensionMeta[uuid];
        // There can be cases where we create dummy extension metadata
        // that's not really a proper extension. Don't bother with these.
        if (!extension.name)
            return;

        let extensionDisplay = this._createExtensionDisplay(extension);
        if (this._numExtensions == 0)
            this._extensionsList.remove_actor(this._noExtensions);

        this._numExtensions ++;
        this._extensionsList.add(extensionDisplay);
    },

    /** callback when the user clicks 'view source' for an extension.
     *
     * This opens the extension directory in the default file browser and
     * closes the looking glass.
     *
     * @param {Clutter.Actor} actor - actor that fired the signal
     * ({@link Link.Link#actor}). In particular it has a property
     * `_extensionMeta` containing the metadata for the extension so that the
     * function knows which extension's source to view.
     */
    _onViewSource: function (actor) {
        let meta = actor._extensionMeta;
        let file = Gio.file_new_for_path(meta.path);
        let uri = file.get_uri();
        Gio.app_info_launch_default_for_uri(uri, global.create_app_launch_context());
        Main.lookingGlass.close();
    },

    /** callback when the user clicks 'web page' to see an extension's webpage.
     *
     * This launches the extension's URL (given by the url in the metadata.json
     * file) in a browser and closes the looking glass.
     *
     * @param {Clutter.Actor} actor - actor that fired the signal
     * ({@link Link.Link#actor}). In particular it has a property
     * `_extensionMeta` containing the metadata for the extension so that the
     * function knows which extension's URL to open.
     */
    _onWebPage: function (actor) {
        let meta = actor._extensionMeta;
        Gio.app_info_launch_default_for_uri(meta.url, global.create_app_launch_context());
        Main.lookingGlass.close();
    },

    /** Converts an extension state to a string.
     * @param {ExtensionSystem.ExtensionState} extensionState - the extension
     * state to convert to string
     * @returns {string} string representation of the extension state
     * (translated) - for example "Enabled" (translated) for
     * `ExtensionSystem.ExtensionState.ENABLED`.
     */
    _stateToString: function(extensionState) {
        switch (extensionState) {
            case ExtensionSystem.ExtensionState.ENABLED:
                return _("Enabled");
            case ExtensionSystem.ExtensionState.DISABLED:
                return _("Disabled");
            case ExtensionSystem.ExtensionState.ERROR:
                return _("Error");
            case ExtensionSystem.ExtensionState.OUT_OF_DATE:
                return _("Out of date");
            case ExtensionSystem.ExtensionState.DOWNLOADING:
                return _("Downloading");
        }
        return 'Unknown'; // Not translated, shouldn't appear
    },

    /** Creates the summary item for a particular extension.
     * This contains:
     *
     * * a `St.Label` (style class 'lg-extension-name') with the extension's
     * name (`metadata.name`);
     * * a `St.Label` (style class 'lg-extension-description') with the
     * extension's description (`metadata.description`);
     * * the extension's state (`metadata.state`, see
     * {@link #_stateToString}) in a `St.Label`;
     * * a {@link Link.Link} with label "View Source" allowing the user to
     * view the extension's source (see {@link #_onViewSource});
     * * a {@link Link.Link} with the label "Web Page" allowing the user to
     * view the extension's homepage (if any) (see
     * {@link #_onWebPage}).
     *
     * @param {ExtensionSystem.ExtensionMeta} meta - the metadata for the
     * extension to create a summary item for.
     * @returns {St.BoxLayout} a St.BoxLayout containing all the information
     * about that extension as described (style class 'lg-extension').
     */
    _createExtensionDisplay: function(meta) {
        let box = new St.BoxLayout({ style_class: 'lg-extension', vertical: true });
        let name = new St.Label({ style_class: 'lg-extension-name',
                                   text: meta.name });
        box.add(name, { expand: true });
        let description = new St.Label({ style_class: 'lg-extension-description',
                                         text: meta.description || 'No description' });
        box.add(description, { expand: true });

        let metaBox = new St.BoxLayout({ style_class: 'lg-extension-meta' });
        box.add(metaBox);
        let stateString = this._stateToString(meta.state);
        let state = new St.Label({ style_class: 'lg-extension-state',
                                   text: this._stateToString(meta.state) });
        metaBox.add(state);

        let viewsource = new Link.Link({ label: _("View Source") });
        viewsource.actor._extensionMeta = meta;
        viewsource.actor.connect('clicked', Lang.bind(this, this._onViewSource));
        metaBox.add(viewsource.actor);

        if (meta.url) {
            let webpage = new Link.Link({ label: _("Web Page") });
            webpage.actor._extensionMeta = meta;
            webpage.actor.connect('clicked', Lang.bind(this, this._onWebPage));
            metaBox.add(webpage.actor);
        }

        return box;
    }
};

/** creates a new LookingGlass.
 *
 * It essentially has a {@link Notebook} ({@link #_notebook}) with
 * the following tabs:
 *
 * * 'Evaluator' (defined in the constructor);
 * * 'Windows' ({@link WindowList}; {@link #_windowList});
 * * 'Errors' ({@link ErrorLog}; {@link #_errorLog});
 * * 'Memory' ({@link Memory}; {@link #_memory});
 * * 'Extensions' ({@link Extensions}; {@link #_extensions});
 *
 * The 'Evaluator' tab consists of a place for all the past commands and
 * results to go (@{link #_resultsArea}), along with a
 * `St.Entry` where the user can type commands ({@link LookinGlass#_entry}).
 *
 * It displays the notebook's tabs ({@link Notebook#tabControls}) in a toolbar
 * up the top, along with an icon that launches the {@link Inspector}.
 * A new Inspector is created each time the user clicks on the inspector icon,
 * and the callbacks for when the user closes/selects an item with the inspector
 * are all defined in the constructor (insert the selected object if the
 * user selected one; give key focus to the command prompt
 * {@link Notebook#_entry} upon the inspector closing).
 *
 * It also keeps an instance of the {@link ObjInspector}
 * ({@link #_objInspector}) for when the user
 * clicks on {@link Result}s in the 'Evaluator' tab.
 *
 * The constructor also creates {@link #_history}, a manager for
 * the command history of the looking glass.
 *
 * The looking glass is parented to {@link Main.panelBox}; this makes it
 * appear to slide out from underneath the top panel.
 *
 * Its default size is 70% of the primary monitor's width and 70% of the
 * primary monitor's height, adjusted to fit if this is too large.
 *
 * @classdesc The looking glass, opened when you type 'lg' into the run dialog.
 * ![Looking Glass](pics/LookingGlass.png)
 *
 * It essentially has a {@link Notebook} ({@link #_notebook}) with
 * the following tabs:
 *
 * * 'Evaluator' (defined in the constructor);
 * * 'Windows' ({@link WindowList}; {@link #_windowList});
 * * 'Errors' ({@link ErrorLog}; {@link #_errorLog});
 * * 'Memory' ({@link Memory}; {@link #_memory});
 * * 'Extensions' ({@link Extensions}; {@link #_extensions});
 *
 * It displays the notebook's tabs ({@link Notebook#tabControls}) in a toolbar
 * up the top, along with an icon that launches the {@link Inspector}.
 *
 * It also keeps an instance of the {@link ObjInspector} for when the user
 * clicks on {@link Result}s in the 'Evaluator' tab.
 *
 * The looking glass is parented to {@link Main.panelBox}; this makes it
 * appear to slide out from underneath the top panel.
 *
 * Its default size is 70% of the primary monitor's width and 70% of the
 * primary monitor's height, adjusted to fit if this is too large.
 *
 * @class
 */
function LookingGlass() {
    this._init();
}

LookingGlass.prototype = {
    _init : function() {
        this._borderPaintTarget = null;
        this._borderPaintId = 0;
        this._borderDestroyId = 0;

        this._open = false;

        this._offset = 0;
        this._results = [];

        /** Maximum results shown in the looking glass (comment from the code
         * on how the number was chosen: "Sort of magic, but...eh.") */
        this._maxItems = 150;

        /** Main actor for the looking glass. style class 'lg-dialog' (which is
         * what causes all the text to be that terrible green colour on
         * GNOME 3.2).
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ name: 'LookingGlassDialog',
                                        style_class: 'lg-dialog',
                                        vertical: true,
                                        visible: false });
        this.actor.connect('key-press-event', Lang.bind(this, this._globalKeyPressEvent));

        this._interfaceSettings = new Gio.Settings({ schema: 'org.gnome.desktop.interface' });
        this._interfaceSettings.connect('changed::monospace-font-name',
                                        Lang.bind(this, this._updateFont));
        this._updateFont();

        // We want it to appear to slide out from underneath the panel
        Main.layoutManager.panelBox.add_actor(this.actor);
        this.actor.lower_bottom();
        Main.layoutManager.panelBox.connect('allocation-changed',
                                            Lang.bind(this, this._queueResize));
        Main.layoutManager.keyboardBox.connect('allocation-changed',
                                               Lang.bind(this, this._queueResize));

        /** The Looking Glass's ObjInspector.
         * @type {LookingGlass.ObjInspector} */
        this._objInspector = new ObjInspector();
        Main.uiGroup.add_actor(this._objInspector.actor);
        this._objInspector.actor.hide();

        let toolbar = new St.BoxLayout({ name: 'Toolbar' });
        this.actor.add_actor(toolbar);
        let inspectIcon = new St.Icon({ icon_name: 'gtk-color-picker',
                                        icon_type: St.IconType.FULLCOLOR,
                                        icon_size: 24 });
        toolbar.add_actor(inspectIcon);
        inspectIcon.reactive = true;
        inspectIcon.connect('button-press-event', Lang.bind(this, function () {
            let inspector = new Inspector();
            inspector.connect('target', Lang.bind(this, function(i, target, stageX, stageY) {
                this._pushResult('<inspect x:' + stageX + ' y:' + stageY + '>',
                                 target);
            }));
            inspector.connect('closed', Lang.bind(this, function() {
                this.actor.show();
                global.stage.set_key_focus(this._entry);
            }));
            this.actor.hide();
            return true;
        }));

        let notebook = new Notebook();
        /** The Notebook that holds all the tabs in the looking glass.
         * @type {LookingGlass.Notebook} */
        this._notebook = notebook;
        this.actor.add(notebook.actor, { expand: true });

        let emptyBox = new St.Bin();
        toolbar.add(emptyBox, { expand: true });
        toolbar.add_actor(notebook.tabControls);

        /** The "Evaluator" page has all its content put into this.
         *
         * Contains the results area {@link #_resultsArea} and a
         * `St.Entry` with a context menu ({@link #_entry},
         * {@link shellEntry.addContextMenu}) for the user to enter commands.
         * @type {St.BoxLayout} */
        this._evalBox = new St.BoxLayout({ name: 'EvalBox', vertical: true });
        notebook.appendPage('Evaluator', this._evalBox);

        /** The {@link Result}s of evaluating commands are all added in here,
         * which forms part of the "Evaluator" tab -- this gets added to
         * {@link #_evalBox} */
        this._resultsArea = new St.BoxLayout({ name: 'ResultsArea', vertical: true });
        this._evalBox.add(this._resultsArea, { expand: true });

        let entryArea = new St.BoxLayout({ name: 'EntryArea' });
        this._evalBox.add_actor(entryArea);

        let label = new St.Label({ text: 'js>>> ' });
        entryArea.add(label);

        /** The entry on the 'Evaluator' tab that the user can type commands
         * into.
         * @type {St.Entry} */
        this._entry = new St.Entry({ can_focus: true });
        ShellEntry.addContextMenu(this._entry);
        entryArea.add(this._entry, { expand: true });

        /** the "Windows" tab.
         * @type {LookingGlass.WindowList} */
        this._windowList = new WindowList();
        this._windowList.connect('selected', Lang.bind(this, function(list, window) {
            notebook.selectIndex(0);
            this._pushResult('<window selection>', window);
        }));
        notebook.appendPage('Windows', this._windowList.actor);

        /** the "Errors" tab.
         * @type {LookingGlass.ErrorLog} */
        this._errorLog = new ErrorLog();
        notebook.appendPage('Errors', this._errorLog.actor);

        /** the "Memory" tab.
         * @type {LookingGlass.Memory} */
        this._memory = new Memory();
        notebook.appendPage('Memory', this._memory.actor);

        /** the "Extensions" tab.
         * @type {LookingGlass.Extensions} */
        this._extensions = new Extensions();
        notebook.appendPage('Extensions', this._extensions.actor);

        this._entry.clutter_text.connect('activate', Lang.bind(this, function (o, e) {
            let text = o.get_text();
            // Ensure we don't get newlines in the command; the history file is
            // newline-separated.
            text.replace('\n', ' ');
            // Strip leading and trailing whitespace
            text = text.replace(/^\s+/g, '').replace(/\s+$/g, '');
            if (text == '')
                return true;
            this._evaluate(text);
            return true;
        }));

        /** Manages the command history for the looking glass.
         * @see HISTORY_KEY
         * @type {History.HistoryManager} */
        this._history = new History.HistoryManager({ gsettingsKey: HISTORY_KEY, 
                                                     entry: this._entry.clutter_text });

        this._resize();
    },

    /** Updates the looking glass' font if the gsettings key
     * 'org.gnome.desktop.interface.monospace-font-name' changes. */
    _updateFont: function() {
        let fontName = this._interfaceSettings.get_string('monospace-font-name');
        // This is mishandled by the scanner - should by Pango.FontDescription_from_string(fontName);
        // https://bugzilla.gnome.org/show_bug.cgi?id=595889
        let fontDesc = Pango.font_description_from_string(fontName);
        // We ignore everything but size and style; you'd be crazy to set your system-wide
        // monospace font to be bold/oblique/etc. Could easily be added here.
        this.actor.style =
            'font-size: ' + fontDesc.get_size() / 1024. + (fontDesc.get_size_is_absolute() ? 'px' : 'pt') + ';'
            + 'font-family: "' + fontDesc.get_family() + '";';
    },

    /** called to add a command with its result to the Evaluator tab. This:
     *
     * * creates a new {@link Result} for the command/result, and adds it to
     * the results area {@link #_resultsArea};
     * * if the result `obj` is a `Clutter.Actor`, draws a red border
     * around the actor using {@link addBorderPaintHook};
     * * scrolls to the bottom of the evaluator tab.
     *
     * @param {string} command - command that was executed
     * @param {any} obj - the result of evaluating that command.
     */
    _pushResult: function(command, obj) {
        let index = this._results.length + this._offset;
        let result = new Result('>>> ' + command, obj, index);
        this._results.push(result);
        this._resultsArea.add(result.actor);
        if (this._borderPaintTarget != null) {
            this._borderPaintTarget.disconnect(this._borderPaintId);
            this._borderPaintTarget = null;
        }
        if (obj instanceof Clutter.Actor) {
            this._borderPaintTarget = obj;
            this._borderPaintId = addBorderPaintHook(obj);
            this._borderDestroyId = obj.connect('destroy', Lang.bind(this, function () {
                this._borderDestroyId = 0;
                this._borderPaintTarget = null;
            }));
        }
        let children = this._resultsArea.get_children();
        if (children.length > this._maxItems) {
            this._results.shift();
            children[0].destroy();
            this._offset++;
        }
        this._it = obj;

        // Scroll to bottom
        this._notebook.scrollToBottom(0);
    },

    /** evaluates a command typed into the command prompt, and adds it
     * to the history.
     *
     * The list of imports/special command definitions defined in
     * {@link commandHeader} are prepended to the command, and all of them
     * are executed using `eval`.
     *
     * If the command executes successfully, the output of the command is
     * stored. Otherwise, the string '&lt;exception (error message) &gt;' is
     * stored as the result.
     *
     * Then {@link #_pushResult} is called with `command` and the
     * result.
     *
     * @param {string} command - command to evaluate
     */
    _evaluate : function(command) {
        this._history.addItem(command);

        let fullCmd = commandHeader + command;

        let resultObj;
        try {
            resultObj = eval(fullCmd);
        } catch (e) {
            resultObj = '<exception ' + e + '>';
        }

        this._pushResult(command, resultObj);
        this._entry.text = '';
    },

    /** Returns the result of the previous command.
     *
     * If the user uses `it` in their command, it is substituted with
     * the value of this function.
     *
     * @returns {any} the result of the previous command. */
    getIt: function () {
        return this._it;
    },

    /** Returns the result of a particular command. If the user uses `r(i)` in
     * their command, the `i`th result is substituted in (i.e. `r` is aliased to
     * this function).
     *
     * @param {number} idx - (0-based) index of the command to retrieve the
     * result of.
     * @returns {any} the result of command `idx`.
     */
    getResult: function(idx) {
        return this._results[idx - this._offset].o;
    },

    /** Toggles the looking glass open and closed */
    toggle: function() {
        if (this._open)
            this.close();
        else
            this.open();
    },

    /** Queues a resize of the looking glass. Called when the panel or
     * on-screen keyboard (if you are using one) change size, in order to
     * make sure the looking glass fits on the screen.
     * @see #_resize */
    _queueResize: function() {
        Meta.later_add(Meta.LaterType.BEFORE_REDRAW,
                       Lang.bind(this, function () { this._resize(); }));
    },

    /** Sets the size of the looking glass/object inspector.
     *
     * The width of the looking glass is always 70% of the primary monitor width.
     * The height of the looking glass is the minimum of 70% of the primary
     * monitor height and 90% of (the primary monitor's height -  the on-screen
     * keyboard's height (if any)) - this makes sure that the looking glass
     * always fits vertically on the screen.
     *
     * The looking glass is positioned to the centre of the primary monitor.
     *
     * The object inspector has height being 80% of the looking glass' height,
     * and width being 80% of the looking glass' width, and is supposed to
     * be centred relative to the looking glass
     * (except that on GNOME 3.2 and 3.4 at least, when you have multiple
     * monitors with the primary one not being the left-most, the object
     * inspector is positioned relative to the left-most monitor and hence often
     * goes across the border between monitors - annoying).
     */
    _resize: function() {
        let primary = Main.layoutManager.primaryMonitor;
        let myWidth = primary.width * 0.7;
        let availableHeight = primary.height - Main.layoutManager.keyboardBox.height;
        let myHeight = Math.min(primary.height * 0.7, availableHeight * 0.9);
        this.actor.x = (primary.width - myWidth) / 2;
        this._hiddenY = this.actor.get_parent().height - myHeight - 4; // -4 to hide the top corners
        this._targetY = this._hiddenY + myHeight;
        this.actor.y = this._hiddenY;
        this.actor.width = myWidth;
        this.actor.height = myHeight;
        this._objInspector.actor.set_size(Math.floor(myWidth * 0.8), Math.floor(myHeight * 0.8));
        this._objInspector.actor.set_position(this.actor.x + Math.floor(myWidth * 0.1),
                                              this._targetY + Math.floor(myHeight * 0.1));
    },

    /** inserts an object as the most recent result in the looking glass.
     * Called when the user selects 'Insert' from the {@link ObjInspector}, or
     * when the user selects an item using the {@link Inspector}.
     *
     * Calls {@link #_pushResult} with command '<insert>' and result
     * `obj`.
     * @param {any} obj - object to insert to the looking glass.
     * @see #_pushResult
     */
    insertObject: function(obj) {
        this._pushResult('<insert>', obj);
    },

    /** Opens the {@link ObjInspector} to inspect a particular object.
     * @param {any} obj - object to inspect with the obj inspector.
     * @param {Clutter.Actor} [sourceActor] - actor over which to open
     * the inspector (?? see {@link ObjInspector.selectObject}).
     */
    inspectObject: function(obj, sourceActor) {
        this._objInspector.open(sourceActor);
        this._objInspector.selectObject(obj);
    },

    /** Handles key events which are relevant for all tabs of the LookingGlass.
     *
     * If the user presses 'Escape' from any tab of the looking glass, if the
     * object inspector was open, it will close; otherwise, the looking glass
     * itself will close.
     */
    _globalKeyPressEvent : function(actor, event) {
        let symbol = event.get_key_symbol();
        if (symbol == Clutter.Escape) {
            if (this._objInspector.actor.visible) {
                this._objInspector.close();
            } else {
                this.close();
            }
            return true;
        }
        return false;
    },

    /** Opens the looking glass.
     *
     * Calls {@link Main.pushModal} on {@link #_entry} to make the
     * looking glass modal, switches to the 'Evaluator' tab, and tweens
     * the looking glass in (slides it in vertically)
     */
    open : function() {
        if (this._open)
            return;

        if (!Main.pushModal(this._entry))
            return;

        this._notebook.selectIndex(0);
        this.actor.show();
        this._open = true;
        this._history.lastItem();

        Tweener.removeTweens(this.actor);

        // We inverse compensate for the slow-down so you can change the factor
        // through LookingGlass without long waits.
        Tweener.addTween(this.actor, { time: 0.5 / St.get_slow_down_factor(),
                                       transition: 'easeOutQuad',
                                       y: this._targetY
                                     });
    },

    /** Closes the looking glass.
     *
     * Closes the object inspector (if it was open), removes any red borders
     * currently painted, pops itself from the modal stack
     * ({@link Main.popModal}), and slides the actor back up until it's hidden.
     */
    close : function() {
        if (!this._open)
            return;

        this._objInspector.actor.hide();

        this._open = false;
        Tweener.removeTweens(this.actor);

        if (this._borderPaintTarget != null) {
            this._borderPaintTarget.disconnect(this._borderPaintId);
            this._borderPaintTarget.disconnect(this._borderDestroyId);
            this._borderPaintTarget = null;
        }

        Main.popModal(this._entry);

        Tweener.addTween(this.actor, { time: 0.5 / St.get_slow_down_factor(),
                                       transition: 'easeOutQuad',
                                       y: this._hiddenY,
                                       onComplete: Lang.bind(this, function () {
                                           this.actor.hide();
                                       })
                                     });
    }
};
Signals.addSignalMethods(LookingGlass.prototype);
