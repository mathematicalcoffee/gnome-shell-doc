// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Diverts some message-tray icons into standard status area icons (for example
 * 'gnome-sound-control-applet' and 'gnome-volume-control-applet' will be
 * removed from the message tray as they are handled by the 'volume' status
 * icon).
 */

const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Signals = imports.signals;

const MessageTray = imports.ui.messageTray;
const NotificationDaemon = imports.ui.notificationDaemon;
const Util = imports.misc.util;

/**
 * An object mapping tray icon names to gnome-shell status icon names.
 * @const
 * @default
 * @type Object.<string, string>
 */
const STANDARD_TRAY_ICON_IMPLEMENTATIONS = {
    'bluetooth-applet': 'bluetooth',
    'gnome-volume-control-applet': 'volume', // renamed to gnome-sound-applet
                                             // when moved to control center
    'gnome-sound-applet': 'volume',
    'nm-applet': 'network',
    'gnome-power-manager': 'battery',
    'keyboard': 'keyboard',
    'a11y-keyboard': 'a11y',
    'kbd-scrolllock': 'keyboard',
    'kbd-numlock': 'keyboard',
    'kbd-capslock': 'keyboard',
    'ibus-ui-gtk': 'input-method'
};

/** creates a new StatusIconDispatcher
 * @classdesc The StatusIconDispatcher listens to icons being added to the tray,
 * (where application want to have an indicator in the status area), and
 * redirects them to be placed either in the top panel, or in the message tray.
 *
 * If the application appears in {@link STANDARD_TRAY_ICON_IMPLEMENTATIONS}, the
 * status indicator corresponding to it will be placed in the top panel.
 * For example, when the 'gnome-sound-applet' creates its tray icon,
 * since `STANDARD_TRAY_ICON_IMPLEMENTATIONS['gnome-sound-applet']` is
 * 'volume', this will cause the [volume indicator]{@link Volume.Indicator} to
 * be created in the top panel for it.
 *
 * All other applications (i.e. not in {@link STANDARD_TRAY_ICON_IMPLEMENTATIONS})
 * will instead have icons (well, {@link NotificationDaemon.Source}s) created for
 * them to sit in the message tray.
 *
 * To detect when applications want tray icons, we use a
 * {@link Shell.TrayManager}.
 *
 * This is instantiated in `main.js` - just one instance is created/needed.
 * It is actually *used* by the {@link Panel.Panel} and {@link NotificationDaemon.NotificationDaemon}
 * classes (to make top panel and message tray indicators respectively).
 * @see Panel.Panel#_onTrayIconAdded
 * @see Panel.Panel#_onTrayIconRemoved
 * @see NotificationDaemon.NotificationDaemon#_onTrayIconAdded
 * @see NotificationDaemon.NotificationDaemon#_onTrayIconRemoved
 * @class
 */
function StatusIconDispatcher() {
    this._init();
}

StatusIconDispatcher.prototype = {
    _init: function() {
        this._traymanager = new Shell.TrayManager();
        this._traymanager.connect('tray-icon-added', Lang.bind(this, this._onTrayIconAdded));
        this._traymanager.connect('tray-icon-removed', Lang.bind(this, this._onTrayIconRemoved));

        // Yet-another-Ubuntu-workaround - we have to kill their
        // app-indicators, so that applications fall back to normal
        // status icons
        // http://bugzilla.gnome.org/show_bug.cgi=id=621382
        Util.killall('indicator-application-service');
    },

    /** starts the icon dispatcher.
     * @param {St.Widget} themeWidget - widget to get style information from.
     */
    start: function(themeWidget) {
        this._traymanager.manage_stage(global.stage, themeWidget);
    },

    /** callback for 'tray-icon-added', when an item is added to the tray.
     * All this does is emit either 'status-icon-added' (if the icon was found
     * in {@link STANDARD_TRAY_ICON_IMPLEMENTATIONS}) or 'message-icon-added'
     * otherwise.
     *
     * These indicate that either a status icon (in the top panel) should be made
     * for the indicator, or that a message tray icon should be mae for the
     * inicator.
     *
     * The {@link Panel.Panel} uses 'status-icon-added' in order to create
     * indicators in the top panel for these icons. For example upon 'nm-applet'
     * putting its icon in the tray, we create a 'network' status indicator
     * for it (see {@link STANDARD_TRAY_ICON_IMPLEMENTATIONS}).
     *
     * The {@link NotificationDaemon.NotificationDaemon} uses 'message-icon-added'
     * to create icons in the mesage tray for all other icons that should not
     * be put in the top panel.
     * @inheritparams #_onTrayIconRemoved
     * @see Panel.Panel#_onTrayIconAdded
     * @see NotificationDaemon.NotificationDaemon#_onTrayIconAdded
     * @fires .message-icon-added
     * @fires .status-icon-added
     */
    _onTrayIconAdded: function(o, icon) {
        let wmClass = (icon.wm_class || 'unknown').toLowerCase();
        let role = STANDARD_TRAY_ICON_IMPLEMENTATIONS[wmClass];
        if (role)
            this.emit('status-icon-added', icon, role);
        else
            this.emit('message-icon-added', icon);
    },

    /** callback for 'tray-icon-removed', when an icon is removed from the tray.
     * All this does is emit either 'status-icon-removed' (if the icon was found
     * in {@link STANDARD_TRAY_ICON_IMPLEMENTATIONS}) or 'message-icon-removed'
     * otherwise.
     *
     * If 'status-icon-removed' is emitted this indicates to the {@link Panel.Panel}
     * that it should remove its corresponding indicator.
     *
     * If 'message-icon-removed' is emitted this inicates to the {@link NotifcationDaemon.NotificationDaemon}
     * that it should remove that application's icon from the message tray.
     *
     * @param {Shell.TrayManager} o - the tray manager that fired the signal.
     * @param {Shell.TrayIcon} icon - a Clutter.Actor with some extra properties
     * (like `pid`, the process ID for the application with this icon).
     * @see Panel.Panel#_onTrayIconRemoved
     * @see NotificationDaemon.NotificationDaemon#_onTrayIconRemoved
     * @fires .message-icon-removed
     * @fires .status-icon-removed
     */
    _onTrayIconRemoved: function(o, icon) {
        let wmClass = (icon.wm_class || 'unknown').toLowerCase();
        let role = STANDARD_TRAY_ICON_IMPLEMENTATIONS[wmClass];
        if (role)
            this.emit('status-icon-removed', icon);
        else
            this.emit('message-icon-removed', icon);
    }
};
Signals.addSignalMethods(StatusIconDispatcher.prototype);
/** Emitted when we receive an indicator that should have a status icon created
 * for it (i.e. indicators with `wm_class` in {@link STANDARD_TRAY_ICON_IMPLEMENTATIONS}).
 *
 * The {@link Panel.Panel} catches these and creates {@link PanelMenu.ButtonBox}es
 * for them in the top panel.
 * @see Panel.Panel#_onTrayIconAdded
 * @event
 * @param {StatusIconDispatcher.StatusIconDispatcher} o - the StatusIconDispatcher
 * instance that emitted the signal.
 * @param {Shell.TrayIcon} icon - a Clutter.Actor with some extra properties
 * (like `pid`, the process ID for the application with this icon).
 * @param {string} role - a string identifying the icon that was added
 * (e.g. 'a11y' for the [accessibility indicator]{@link Accessibility.ATIndicator})
 * @memberof StatusIconDispatcher
 * @name status-icon-added
 */
/** Emitted when we receive an indicator that should have a message tray icon
 * created for it (i.e. indicators not in {@link STANDARD_TRAY_ICON_IMPLEMENTATIONS};
 * instead of creating an icon up the top we create one down the bottom).
 *
 * The {@link NotificationDaemon.NotificationDaemon} catches these and creates
 * {@link NotificationDaemon.Source}s for them to sit in the message tray.
 * @event
 * @see NotificationDaemon.NotificationDaemon#_onTrayIconAdded
 * @memberof StatusIconDispatcher
 * @param {StatusIconDispatcher.StatusIconDispatcher} o - the StatusIconDispatcher
 * instance that emitted the signal.
 * @param {Shell.TrayIcon} icon - a Clutter.Actor with some extra properties
 * (like `pid`, the process ID for the application with this icon).
 * @name message-icon-added
 */
/** Emitted when an indicator is removed that we have created a status icon for
 * via {@link StatusIconDispatcher.status-icon-added}.
 *
 * The {@link Panel.Panel} catches these and removes the {@link PanelMenu.ButtonBox}es
 * it made for them from the top panel.
 *
 * @inheritparams StatusIconDispatcher.message-icon-added
 * @event
 * @memberof StatusIconDispatcher
 * @name status-icon-removed
 */
/** Emitted when an indicator is removed that we have created a message tray
 * icon for.
 *
 * The {@link NotificationDaemon.NotificationDaemon} catches these and removes
 * the {@link NotificationDaemon.Source} it made for them from the message tray.
 *
 * @inheritparams StatusIconDispatcher.message-icon-added
 * @event
 * @memberof StatusIconDispatcher
 * @name message-icon-removed
 */
