// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file contains classes for layout of icons in a grid (e.g. the Overview
 * search results)
 */

const Clutter = imports.gi.Clutter;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Lang = imports.lang;
const Params = imports.misc.params;

/** Default size for the icon component of a {@link BaseIcon}.
 * @const
 * @type {number}
 * @default */
const ICON_SIZE = 48;


/** creates a new BaseIcon
 * @param {string} label - text for the icon (control whether it is displayed
 * with `params.showLabel`).
 * @param {Object} [params] - parameters for the icon.
 * @param {function(size)} [params.createIcon] - function taking in an icon
 * size and producing an icon.
 * @param {boolean} [setSizeManually=false] - whether the icon's size is determined
 * manually (must use {@link #setIconSize}) or automatically (uses the
 * 'icon-size' property of the CSS style ('overview-icon', 96px if the icon
 * is in a {@link IconGrid.IconGrid}). If set to `false` and {@link #setIconSize}
 * is not called, the default size is {@link ICON_SIZE} (48).
 * @param {boolean} [showLabel=true] - whether to show a label (with the
 * text `label`) underneath the icon. If `false`, just the icon is shown.
 * @classdesc
 *
 * ![Example of a BaseIcon (search result in the overview)](pics/BaseIcon.png)
 *
 * An Icon class. This is a class that displays an icon and optionally a
 * label underneath. It is meant to be used for icons that will be displayed
 * in the overview (for example, app icons in the Dash and app icons that
 * appear in the 'Applications' tab extend this class, and most the icons that
 * appear when searching in the overview are these as well).
 *
 * The entire actor (containing both the icon and label) has style class
 * 'overview-icon'.
 *
 * Either {@link #createIcon} must be implemented (if subclassing), or
 * `params.createIcon` must be provided in the constructor.
 * @class
 */
function BaseIcon(label, createIcon) {
    this._init(label, createIcon);
}

BaseIcon.prototype = {
    _init : function(label, params) {
        params = Params.parse(params, { createIcon: null,
                                        setSizeManually: false,
                                        showLabel: true });
        /** Top-level actor for the BaseIcon. Style class 'overview-icon'.
         * It contains a `Shell.GenericContainer` with the icon and its label.
         * @type {St.Bin} */
        this.actor = new St.Bin({ style_class: 'overview-icon',
                                  x_fill: true,
                                  y_fill: true });
        this.actor._delegate = this;
        this.actor.connect('style-changed',
                           Lang.bind(this, this._onStyleChanged));

        this._spacing = 0;

        let box = new Shell.GenericContainer();
        box.connect('allocate', Lang.bind(this, this._allocate));
        box.connect('get-preferred-width',
                    Lang.bind(this, this._getPreferredWidth));
        box.connect('get-preferred-height',
                    Lang.bind(this, this._getPreferredHeight));
        this.actor.set_child(box);

        this.iconSize = ICON_SIZE;
        this._iconBin = new St.Bin();

        box.add_actor(this._iconBin);

        if (params.showLabel) {
            this.label = new St.Label({ text: label });
            box.add_actor(this.label);
        } else {
            this.label = null;
        }

        if (params.createIcon)
            this.createIcon = params.createIcon;
        this._setSizeManually = params.setSizeManually;

        this.icon = null;
    },

    /** Callback for an 'allocate' signal for the BaseIcon - lays out the
     * label at the bottom of the space, horizontally centred (if it is to be shown)
     * and centres the icon vertically and horizontally within the remaining
     * space.
     */
    _allocate: function(actor, box, flags) {
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;

        let iconSize = availHeight;

        let [iconMinHeight, iconNatHeight] = this._iconBin.get_preferred_height(-1);
        let [iconMinWidth, iconNatWidth] = this._iconBin.get_preferred_width(-1);
        let preferredHeight = iconNatHeight;

        let childBox = new Clutter.ActorBox();

        if (this.label) {
            let [labelMinHeight, labelNatHeight] = this.label.get_preferred_height(-1);
            preferredHeight += this._spacing + labelNatHeight;

            let labelHeight = availHeight >= preferredHeight ? labelNatHeight
                                                             : labelMinHeight;
            iconSize -= this._spacing + labelHeight;

            childBox.x1 = 0;
            childBox.x2 = availWidth;
            childBox.y1 = iconSize + this._spacing;
            childBox.y2 = childBox.y1 + labelHeight;
            this.label.allocate(childBox, flags);
        }

        childBox.x1 = Math.floor((availWidth - iconNatWidth) / 2);
        childBox.y1 = Math.floor((iconSize - iconNatHeight) / 2);
        childBox.x2 = childBox.x1 + iconNatWidth;
        childBox.y2 = childBox.y1 + iconNatHeight;
        this._iconBin.allocate(childBox, flags);
    },

    /** callback for the 'get-preferred-width' signal for the BaseIcon - calls
     * {@link #_getPreferredHeight} and asks for a width equal to that
     * height (i.e. it makes sure the entire icon + label is a square).
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        this._getPreferredHeight(actor, -1, alloc);
    },

    /** callback for the 'get-preferred-height' signal for BaseIcon - asks for
     * the height requested by the icon, and if the label is to be shown it
     * adds the the height requested by the label and the value of the 'spacing'
     * property of the CSS style for {@link #actor} (default 0px, from
     * style class 'overview-icon').
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        let [iconMinHeight, iconNatHeight] = this._iconBin.get_preferred_height(forWidth);
        alloc.min_size = iconMinHeight;
        alloc.natural_size = iconNatHeight;

        if (this.label) {
            let [labelMinHeight, labelNatHeight] = this.label.get_preferred_height(forWidth);
            alloc.min_size += this._spacing + labelMinHeight;
            alloc.natural_size += this._spacing + labelNatHeight;
        }
    },

    /** Creates the BaseIcon's icon.
     *
     * This can be overridden by a subclass, or by the createIcon
     * parameter to {@link IconGrid}. It must be implemented.
     * @inheritparams #setIconSize
     * @returns {?} an icon, for example a `St.Icon`.
     */
    createIcon: function(size) {
        throw new Error('no implementation of createIcon in ' + this);
    },

    /** Sets the icon's size.
     *
     * This can only be called if the user set `params.setSizeManually=true`
     * in the constructor. Otherwise the icon's size is determined by the
     * 'icon-size' attribute in {@link #actor}'s CSS style.
     * @param {number} size - size of the icon.
     */
    setIconSize: function(size) {
        if (!this._setSizeManually)
            throw new Error('setSizeManually has to be set to use setIconsize');

        if (size == this.iconSize)
            return;

        this._createIconTexture(size);
    },

    /** creates an icon of the appropriate size and renders it (uses
     * {@link #createIcon}).
     * @inheritparams #setIconSize
     */
    _createIconTexture: function(size) {
        if (this.icon)
            this.icon.destroy();
        this.iconSize = size;
        this.icon = this.createIcon(this.iconSize);

        // The icon returned by createIcon() might actually be smaller than
        // the requested icon size (for instance StTextureCache does this
        // for fallback icons), so set the size explicitly.
        this.icon.set_size(this.iconSize, this.iconSize);

        this._iconBin.child = this.icon;
    },

    /** Callback when {@link #actor}'s style class(es) change.
     * This re-looks up the 'spacing' (spacing between label and icon) and
     * 'icon-size' attributes (note that 'icon-size' is only paid attention
     * to if the user set `params.setSizeManually=false` in the constructor).
     */
    _onStyleChanged: function() {
        let node = this.actor.get_theme_node();
        this._spacing = node.get_length('spacing');

        let size;
        if (this._setSizeManually) {
            size = this.iconSize;
        } else {
            let [found, len] = node.lookup_length('icon-size', false);
            size = found ? len : ICON_SIZE;
        }

        // don't create icons unnecessarily
        if (size == this.iconSize &&
            this._iconBin.child)
            return;

        this._createIconTexture(size);
    }
};

/** creates a new IconGrid
 * @param {Object} params - parameters for the IconGrid.
 * @param {?number} params.rowLimit - the maximum number of rows to display
 * in the results (`null` to not limit this).
 * @param {?number} params.columnLimit - the maximum number of columns to display
 * in the results (`null` to not limit this).
 * @param {?number} xAlign - the X alignment of the grid within the available
 * space. One of `St.Align.START`, `St.Align.MIDDLE`, `St.Align.END`.
 * @classdesc
 * An IconGrid is a grid of icons. The user can specify a row limit and/or
 * column limit, and although the IconGrid may have more than that many
 * items in it, only that many will be displayed.
 *
 * The grid of application icons in the 'Applications' tab of the overview
 * is an example of this, as is the grid of search results in the overview
 * (one per category of results).
 *
 * The spacing between icons is loaded from {@link #actor}'s CSS style
 * class' 'spacing' property (default 36px, style class 'icon-grid').
 *
 * The size of grid elements is taken from {@link #actor}'s CSS style
 * class '-shell-grid-item-size' property (default 118px, style class 'icon-grid').
 * @class
 */
function IconGrid(params) {
    this._init(params);
}

IconGrid.prototype = {
    _init: function(params) {
        params = Params.parse(params, { rowLimit: null,
                                        columnLimit: null,
                                        xAlign: St.Align.MIDDLE });
        /** Max number of rows to show.
         * @type {number} */
        this._rowLimit = params.rowLimit;
        /** Max number of columns to show.
         * @type {number} */
        this._colLimit = params.columnLimit;
        this._xAlign = params.xAlign;

        /** The top-level actor for the IconGrid.
         * This has style class `icon-grid` and is a vertical St.BoxLayout
         * (but the *actual* parent of all the items is {@link #_grid},
         * the `Shell.GenericContainer`)
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ style_class: 'icon-grid',
                                        vertical: true });
        // Pulled from CSS, but hardcode some defaults here
        /** Spacing between items in the grid. Taken from
         * {@link #actor}'s 'spacing' CSS style property.
         * Default style 'icon-grid' has value 36px. If not found, 0 is used.
         * @type {number} */
        this._spacing = 0;
        /** Spacing between items in the grid. Taken from
         * {@link #actor}'s '-shell-grid-item-size' CSS style property.
         * Default style 'icon-grid' has value 118px. If not found,
         * {@link ICON_SIZE} is used (48px).
         * @type {number} */
        this._item_size = ICON_SIZE;
        /** The container that actually contains all the items in the grid
         * and lays them out in a grid.
         * @type {Shell.GenericContainer} */
        this._grid = new Shell.GenericContainer();
        this.actor.add(this._grid, { expand: true, y_align: St.Align.START });
        this.actor.connect('style-changed', Lang.bind(this, this._onStyleChanged));

        this._grid.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this._grid.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this._grid.connect('allocate', Lang.bind(this, this._allocate));
    },

    /** Callback for {@link #_grid}'s 'get-preferred-width' signal -
     * calculates the width needed to display {@link #_colLimit}
     * icons of size {@link #_itemSize} spaced by
     * {@link #_spacing}.
     */
    _getPreferredWidth: function (grid, forHeight, alloc) {
        let children = this._grid.get_children();
        let nColumns = this._colLimit ? Math.min(this._colLimit,
                                                 children.length)
                                      : children.length;
        let totalSpacing = Math.max(0, nColumns - 1) * this._spacing;
        // Kind of a lie, but not really an issue right now.  If
        // we wanted to support some sort of hidden/overflow that would
        // need higher level design
        alloc.min_size = this._item_size;
        alloc.natural_size = nColumns * this._item_size + totalSpacing;
    },

    /** Gets the items in the grid that have `child.visible = true`.
     * This is nothing to do with whether the child actually fits within
     * the icon grid (to see how many children fit in the grid, use
     * {@link #getVisibleItemsCount}).
     *
     * For example, if there are 15 visible children in the IconGrid but a limit
     * of 3 rows and 4 columns, only 12 will be displayed, but all 15 will
     * count as visible children (so all 15 will be returned).
     *
     * This is just used internally by the allocate function to determine which
     * children to even display in the first place.
     * @returns {Clutter.Actor[]} array of children that are visible. */
    _getVisibleChildren: function() {
        let children = this._grid.get_children();
        children = children.filter(function(actor) {
            return actor.visible; 
        });
        return children;
    },

    /** Callback for {@link #_grid}'s 'get-preferred-height' signal.
     * This calculates the height required by:
     *
     * 1. Calculate the number of columns of items that will fit in the
     *    available space `forWidth` (using {@link #_computeLayout).
     * 2. Get the children that are candidates to be displayed ({@link #_getVisibleChildren}).
     * 3. Calculate the number of rows we will display, being the minimum of
     * {@link #_rowLimit} and `number of visible children / number of columns`
     * 4. Calculate the height required to display this many rows with
     * {@link #_spacing} in between.
     * @param {number} forWidth - the width we are to compute the preferred height
     * for.
     */
    _getPreferredHeight: function (grid, forWidth, alloc) {
        let children = this._getVisibleChildren();
        let [nColumns, usedWidth] = this._computeLayout(forWidth);
        let nRows;
        if (nColumns > 0)
            nRows = Math.ceil(children.length / nColumns);
        else
            nRows = 0;
        if (this._rowLimit)
            nRows = Math.min(nRows, this._rowLimit);
        let totalSpacing = Math.max(0, nRows - 1) * this._spacing;
        let height = nRows * this._item_size + totalSpacing;
        alloc.min_size = height;
        alloc.natural_size = height;
    },

    /** Callback for {@link #_grid}'s 'allocate' signal. This lays
     * out the children in a grid format, respecting both {@link #_rowLimit},
     * {@link #_colLimit}, as well as what will fit in the available space.
     *
     * It aligns item grid within the available space according to the `xAlign`
     * parameter passed into the constructor, and centres each individual item
     * within its allocation.
     */
    _allocate: function (grid, box, flags) {
        let children = this._getVisibleChildren();
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;

        let [nColumns, usedWidth] = this._computeLayout(availWidth);

        let leftPadding;
        switch(this._xAlign) {
            case St.Align.START:
                leftPadding = 0;
                break;
            case St.Align.MIDDLE:
                leftPadding = Math.floor((availWidth - usedWidth) / 2);
                break;
            case St.Align.END:
                leftPadding = availWidth - usedWidth;
        }

        let x = box.x1 + leftPadding;
        let y = box.y1;
        let columnIndex = 0;
        let rowIndex = 0;
        for (let i = 0; i < children.length; i++) {
            let [childMinWidth, childMinHeight, childNaturalWidth, childNaturalHeight]
                = children[i].get_preferred_size();

            /* Center the item in its allocation horizontally */
            let width = Math.min(this._item_size, childNaturalWidth);
            let childXSpacing = Math.max(0, width - childNaturalWidth) / 2;
            let height = Math.min(this._item_size, childNaturalHeight);
            let childYSpacing = Math.max(0, height - childNaturalHeight) / 2;

            let childBox = new Clutter.ActorBox();
            if (St.Widget.get_default_direction() == St.TextDirection.RTL) {
                let _x = box.x2 - (x + width);
                childBox.x1 = Math.floor(_x - childXSpacing);
            } else {
                childBox.x1 = Math.floor(x + childXSpacing);
            }
            childBox.y1 = Math.floor(y + childYSpacing);
            childBox.x2 = childBox.x1 + width;
            childBox.y2 = childBox.y1 + height;

            if (this._rowLimit && rowIndex >= this._rowLimit) {
                this._grid.set_skip_paint(children[i], true);
            } else {
                children[i].allocate(childBox, flags);
                this._grid.set_skip_paint(children[i], false);
            }

            columnIndex++;
            if (columnIndex == nColumns) {
                columnIndex = 0;
                rowIndex++;
            }

            if (columnIndex == 0) {
                y += this._item_size + this._spacing;
                x = box.x1 + leftPadding;
            } else {
                x += this._item_size + this._spacing;
            }
        }
    },

    /** Returns the number of children that will fit in a row of length
     * `rowWidth` pixels.
     *
     * Just calls {@link #_computeLayout} and returns the first element.
     * @param {number} rowWidth - the width of the row (in pixels).
     * @returns {number} the number of columns that will fit in the specified
     * `rowWidth`, capping at {@link #_colLimit} if that is set.
     */
    childrenInRow: function(rowWidth) {
        return this._computeLayout(rowWidth)[0];
    },

    /** Calculates the number of columns of items that will fit in the
     * specified width, returning both the number of columns that will fit and
     * the space taken by all the items (including spacing).
     *
     * If {@link #_colLimit} is set, the returned number of columns
     * is at most this (can be less if not that many will actually fit in
     * `forWidth`).
     * @param {number} forWidth - the available width to fit the items.
     * @returns {Array.<number, number>} first being the number of columns
     * that will fit in `forWidth`, the second being the actual width (in pixels)
     * they will all take up.
     */
    _computeLayout: function (forWidth) {
        let nColumns = 0;
        let usedWidth = 0;
        while ((this._colLimit == null || nColumns < this._colLimit) &&
               (usedWidth + this._item_size <= forWidth)) {
            usedWidth += this._item_size + this._spacing;
            nColumns += 1;
        }

        if (nColumns > 0)
            usedWidth -= this._spacing;

        return [nColumns, usedWidth];
    },

    /** Callback when {@link #actor}'s style class changes. This
     * just updates the spacing between rows/columns of items and the
     * size of each item (the 'spacing' and '-shell-grid-item-size' properties
     * of the CSS style class). */
    _onStyleChanged: function() {
        let themeNode = this.actor.get_theme_node();
        this._spacing = themeNode.get_length('spacing');
        this._item_size = themeNode.get_length('-shell-grid-item-size');
        this._grid.queue_relayout();
    },

    /** Removes **and destroys** all children in the grid. */
    removeAll: function () {
        this._grid.get_children().forEach(Lang.bind(this, function (child) {
            child.destroy();
        }));
    },

    /** Adds a child to the grid.
     * @param {Clutter.Actor} actor - item to add to the grid.
     */
    addItem: function(actor) {
        this._grid.add_actor(actor);
    },

    /** Gets the actor at a particular (linear) index. Items are ordered in
     * a row-major fashion (so laid out horizontally and then vertically).
     * @param {number} index - index of the child to retrieve
     * @returns {Clutter.Actor} - item at index `index` in the grid.
     */
    getItemAtIndex: function(index) {
        return this._grid.get_children()[index];
    },

    /** Gets the number of items that actually fit in the grid (based on
     * {@link #_rowLimit}, {@link #_colLimit}, and the space
     * available to the grid).
     *
     * So if the grid is restricted to 3 rows and 4 columns and there are 15
     * children, then `visibleItemsCount` will return 12.
     * @returns {number} the number of items that are to be painted in the
     * grid. */
    visibleItemsCount: function() {
        return this._grid.get_children().length - this._grid.get_n_skip_paint();
    }
};
