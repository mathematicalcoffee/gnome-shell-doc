// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file handles the Accessibility switcher which lets you select UI
 * elements (top panel, dash, ...) so that you can navigate around them with
 * the keyboard.
 *
 * (In the overview you get more items to choose than outside of it).
 *
 * The toplevel function that should be used to show the popup is
 * [Main.ctrlAltTabManager.popup]{@link CtrlAltTabManager#popup}.
 */

const Clutter = imports.gi.Clutter;
const Gdk = imports.gi.Gdk;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const AltTab = imports.ui.altTab;
const Main = imports.ui.main;
const Params = imports.misc.params;
const Tweener = imports.ui.tweener;

/** @+
 * @const
 * @default
 * @type {number} */
/** Size of icons in the {@link CtrlAltTabSwitcher}. */
const POPUP_APPICON_SIZE = 96;
/** Time taken in seconds for the {@link CtrlAltTabPopup} to  fade in and out. */
const POPUP_FADE_TIME = 0.1; // seconds
/** @- */

/** Which area of the screen an item is - in the top (like the top panel),
 * bottom (message tray), or middle (everything else). Used to sort items
 * in the {@link CtrlAltTabManager}.
 * @enum
 * @const */
const SortGroup = {
    TOP:    0,
    MIDDLE: 1,
    BOTTOM: 2
};

/** Object describing an item in the CtrlAltTab popup.
 * @typedef CtrlAltTabItem
 * @type {Object}
 * @property {Meta.Window} [window] - if the item for the popup is in fact a
 * window, then populate this property.
 * @property {string} name - name of the item to display in the switcher
 * @property {GIcon} [icon] - icon for the item to display in the switcher
 * (e.g. a `St.Icon`). Either this or the `iconName` property must be provided.
 * @property {GIcon} [iconName] - name of the icon to display in the switcher. 
 * Either this or the `icon` property must be provided.
 * @property {CtrlAltTab.SortGroup} sortGroup - what group the item belongs
 * to (only used for sorting within the popup). `TOP` (e.g. top panel),
 * `BOTTOM` (e.g. message tray), or `MIDDLE` (everything else).
 */
/** Object describing an item within the CtrlAltTabManager.
 * It's just a {@link CtrlAltTabItem} with additional properties `root`,
 * `proxy` and `focusCallback`.
 * @typedef CtrlAltTabManagerItem
 * @type {Object}
 * @property {St.Widget} root - the actor to add to the Ctrl Alt Tab popup
 * (example {@link Main.panel.actor} for the top panel). This is what will be
 * navigated through.
 *
 * If not a `St` widget, `focusCallback` must be specified.
 * @property {Clutter.Actor} [proxy=root] - proxy for the item (for example
 * tabs in the overview have `root` being the page content, but their `proxy`
 * is the tab title). Items will not be added to the ctrl alt tab popup
 * unless the `proxy` is visible. Also, sorting (by location) is done
 * using the `proxy`. If not supplied, the `root` will be assumed.
 * @property {function()} [focusCallback=null] - called when the user chooses
 * this item in the ctrl alt tab popup. If `null`,
 * [`St.Widget.navigate_focus`](http://developer.gnome.org/st/stable/StWidget.html#st-widget-navigate-focus)
 * is used on `root`.
 * 
 * @property {Meta.Window} [window] - if the item for the popup is in fact a
 * window, then populate this property.
 * (as in {@link CtrlAltTabItem}).
 * @property {string} name - name of the item to display in the switcher
 * (as in {@link CtrlAltTabItem}).
 * @property {GIcon} [iconName] - name of the icon to display in the switcher
 * Either this or the `icon` property must be provided.
 * (as in {@link CtrlAltTabItem}).
 * @property {GIcon} [icon] - icon for the item to display in the switcher
 * (e.g. a `St.Icon`). Either this or the `iconName` property must be provided.
 * (as in {@link CtrlAltTabItem}).
 * @property {CtrlAltTab.SortGroup} [sortGroup=CtrlAltTab.SortGroup.MIDDLE] -
 * what group the item belongs
 * to (only used for sorting within the popup). `TOP` (e.g. top panel),
 * `BOTTOM` (e.g. message tray), or `MIDDLE` (everything else).
 * (as in {@link CtrlAltTabItem}).
 */

/** creates a new CtrlAltTabManager
 *
 * This stores a local `St.FocusManager` to work out how to navigate between
 * items in (e.g.) the top panel.
 * @classdesc
 * This controls ctrl-alt-tab behaviour; that is, the ability to give keyboard
 * focus to a gnome-shell object (like the top panel, or the dash).
 *
 * Only one instance need be made; it is stored in {@link Main.ctrlAltTabManager}
 * and used by the {@link WindowManager.WindowManager}.
 *
 * To add an object to the list of browsable objects, use
 * [Main.ctrlAltTabManager.addGroup]{@link #addGroup}.
 * @class
 */
function CtrlAltTabManager() {
    this._init();
}

CtrlAltTabManager.prototype = {
    _init: function() {
        this._items = [];
        /** A local St.FocusManager instance to wrap around.
         * @type {St.FocusManager} */
        this._focusManager = St.FocusManager.get_for_stage(global.stage);
    },

    /** Adds an object to the CtrlAltTabManager, i.e. marks this item as
     * some sort of gnome-shell container object that can have focus given
     * to it in order to navigate through its children. Example - top panel.
     *
     * This creates a {@link CtrlAltTabItem} from the inputs with the additional
     * properties `root`, `proxy`, and `focusCallback`.
     *
     * @param {St.Widget} root - the actor to add to the Ctrl Alt Tab popup
     * (example {@link Main.panel.actor} for the top panel).
     * If not a `St` widget, it must have `focusCallback` specified.
     * @param {string} name - label for that item in the ctrl alt tab popup
     * (e.g. "Top bar" for the top panel).
     * @param {string} icon - name of the icon to use for the item in the
     * ctrl alt tab popup ('start-here', the GNOME footprint icon, for the
     * top panel).
     * @param {Object} [params] - additional parameters.
     * @param {CtrlAltTab.SortGroup} [sortGroup=CtrlAltTabSortGroup.MIDDLE] - see {@link CtrlAltTabManagerItem}.
     * @param {St.Widget} [proxy=root] - see {@link CtrlAltTabManagerItem}.
     * @param {function()} [focusCallback=null] - see {@link CtrlAltTabManagerItem}.
     * @see CtrlAltTabManagerItem
     * @example
     * // in Panel._init to add top panel to the ctrl alt tab manager.
     * Main.ctrlAltTabManager.addGroup(Main.panel.actor, _("Top Bar"),
     *                                 'start-here'
     *                                 { sortGroup: CtrlAltTab.SortGroup.TOP });
     * // in Dash.init to add the dash to the ctrl alt tab manager.
     * Main.ctrlAltTabManager.addGroup(Main.overview._dash.actor, _("Dash"),
     *                                 'user-bookmarks');
     * // in ViewSelector.BaseTab#_init to add the various overview tabs
     * Main.ctrlAltTabManager.addGroup(tab.page, tab.name, tab.a11yIcon,
     *                                 { proxy: tab.title });
     */
    addGroup: function(root, name, icon, params) {
        let item = Params.parse(params, { sortGroup: SortGroup.MIDDLE,
                                          proxy: root,
                                          focusCallback: null });

        item.root = root;
        item.name = name;
        item.iconName = icon;

        this._items.push(item);
        root.connect('destroy', Lang.bind(this, function() { this.removeGroup(root); }));
        this._focusManager.add_group(root);
    },

    /** Removes an object from the CtrlAltTabManager.
     * @param {St.Widget} root - the actor to remove (as in
     * {@link #addGroup}'s `root` parameter).
     */
    removeGroup: function(root) {
        this._focusManager.remove_group(root);
        for (let i = 0; i < this._items.length; i++) {
            if (this._items[i].root == root) {
                this._items.splice(i, 1);
                return;
            }
        }
    },

    /** Transfers keyboard focus to a widget.
     *
     * If `item.window` exists, it will be activated.
     * Otherwise if `focusCallback` exists, it is called.
     * Otherwise if none of the above apply, `item.root.navigate_focus` is called.
     * 
     * @param {CtrlAltTabManagerItem} item - item to give focus to.
     */
    focusGroup: function(item) {
        if (global.stage_input_mode == Shell.StageInputMode.NONREACTIVE ||
            global.stage_input_mode == Shell.StageInputMode.NORMAL)
            global.set_stage_input_mode(Shell.StageInputMode.FOCUSED);

        if (item.window)
            Main.activateWindow(item.window);
        else if (item.focusCallback)
            item.focusCallback();
        else
            item.root.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
    },

    /** Sort the items into a consistent order (by
     * [`CtrlAltTabManagerItem.sortGroup`]({@link CtrlAltTabManagerItem}));
     * panel first, tray last,
     * and everything else in between, sorted by X coordinate, so that
     * they will have the same left-to-right ordering in the
     * Ctrl-Alt-Tab dialog as they do onscreen.
     *
     * When calculating X/Y position, [`CtrlAltTabManagerItem.proxy`]{@link CtrlAltTabManagerItem}
     * is used as opposed to `CtrlAltTabManagerItem.root` (if you did not specify
     * a proxy in the `params` argument of {@link #addGroup},
     * the proxy is the root item).
     *
     * This is fed into `Array.sort`; actually used in {@link #popup}.
     * @param {CtrlAltTabManagerItem} a - an item to compare
     * @param {CtrlAltTabManagerItem} b - second item to compare
     * @returns {number} negative if `a` is before `b`, 0 if they're the same,
     * positive if `a` is after `b`.
     */
    _sortItems: function(a, b) {
        if (a.sortGroup != b.sortGroup)
            return a.sortGroup - b.sortGroup;

        let y;
        if (a.x == undefined) {
            if (a.window)
                a.x = a.window.get_compositor_private().x;
            else
                [a.x, y] = a.proxy.get_transformed_position();
        }
        if (b.x == undefined) {
            if (b.window)
                b.x = b.window.get_compositor_private().x;
            else
                [b.x, y] = b.proxy.get_transformed_position();
        }

        return a.x - b.x;
    },

    /** This is the function to be used by external code to show the ctrl
     * alt tab popup.
     *
     * This:
     * 
     * 1. Filters all the items that have been added with {@link #addGroup}
     * to include just those that are visible.
     * 2. If the overview is visible, it adds the "Windows" option which lets
     * the user navigate through windows.
     * 3. Sorts the items ({@link #_sortItems})
     * 4. Creates a {@link CtrlAltTabPopup} and shows it with the items.
     *
     * @param {boolean} backwards - whether we are cycling through the
     * items backwards or forwards.
     * @param {int} mask - the (key) modifier mask that launched the popup
     * (Ctrl+Alt). (I think you can compare these with Clutter.ModifierType)
     */
    popup: function(backwards, mask) {
        // Start with the set of focus groups that are currently mapped
        let items = this._items.filter(function (item) { return item.proxy.mapped; });

        // And add the windows metacity would show in its Ctrl-Alt-Tab list
        if (!Main.overview.visible) {
            let screen = global.screen;
            let display = screen.get_display();
            let windows = display.get_tab_list(Meta.TabList.DOCKS, screen, screen.get_active_workspace ());
            let windowTracker = Shell.WindowTracker.get_default();
            let textureCache = St.TextureCache.get_default();
            for (let i = 0; i < windows.length; i++) {
                let icon;
                let app = windowTracker.get_window_app(windows[i]);
                if (app)
                    icon = app.create_icon_texture(POPUP_APPICON_SIZE);
                else
                    icon = textureCache.bind_pixbuf_property(windows[i], 'icon');
                items.push({ window: windows[i],
                             name: windows[i].title,
                             iconActor: icon,
                             sortGroup: SortGroup.MIDDLE });
            }
        }

        if (!items.length)
            return;

        items.sort(Lang.bind(this, this._sortItems));

        if (!this._popup) {
            this._popup = new CtrlAltTabPopup();
            this._popup.show(items, backwards, mask);

            this._popup.actor.connect('destroy',
                                      Lang.bind(this, function() {
                                          this._popup = null;
                                      }));
        }
    }
};

function mod(a, b) {
    return (a + b) % b;
}

/** creates a new CtrlAltTabPopup
 *
 * Added to {@link Main.uiGroup}.
 * @classdesc
 * ![Ctrl Alt Tab Popup with the switcher list inside.](https://live.gnome.org/GnomeShell/CheatSheet?action=AttachFile&do=get&target=shortcuts-ctrl-alt-tab.png)
 *
 * This is an accessibility popup. It appears when you press Ctrl+Alt+Tab
 * and allows the user to select eligible items to receive keyboard focus
 * (for example the top panel). Selecting an item transfers keyboard focus
 * to that item.
 *
 * For example, selecting the top panel from the popup allows the user to
 * select various items in the top panel by using the arrow keys.
 *
 * (Outside of the overview you just have 'Top panel'; inside you get extra
 * options like the dash, and tabs in the view selector).
 *
 * @class
 */
function CtrlAltTabPopup() {
    this._init();
}

CtrlAltTabPopup.prototype = {
    _init : function() {
        /** Top-level actor for the CtrlAltTab Popup. It's actually clear
         * and covers the entire screen; the visible part of the popup is
         * actually its {@link CtrlAltTabSwitcher}.
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer({ name: 'ctrlAltTabPopup',
                                                  reactive: true });

        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        this._haveModal = false;
        this._modifierMask = 0;
        this._selection = 0;

        Main.uiGroup.add_actor(this.actor);
    },

    /** callback for the 'get-preferred-width' signal of {@link #actor}.
     * The CtrlAltTabPopup gets the entire width of the primary monitor.
     */
    _getPreferredWidth: function (actor, forHeight, alloc) {
        let primary = Main.layoutManager.primaryMonitor;

        alloc.min_size = primary.width;
        alloc.natural_size = primary.width;
    },

    /** callback for the 'get-preferred-height' signal of {@link #actor}.
     * The CtrlAltTabPopup gets the entire height of the primary monitor.
     */
    _getPreferredHeight: function (actor, forWidth, alloc) {
        let primary = Main.layoutManager.primaryMonitor;

        alloc.min_size = primary.height;
        alloc.natural_size = primary.height;
    },

    /** callback for the 'allocate' signal of {@link #actor}.
     * This doesn't allocate any space to the `CtrlAltTabPopup` itself, but
     * instead centres {@link #_switcher} in the middle of the
     * screen, giving it whatever space it needs
     * (this is the actual visible part of the popup that one might
     * call the "popup".
     */
    _allocate: function (actor, box, flags) {
        let childBox = new Clutter.ActorBox();
        let primary = Main.layoutManager.primaryMonitor;

        let leftPadding = this.actor.get_theme_node().get_padding(St.Side.LEFT);
        let vPadding = this.actor.get_theme_node().get_vertical_padding();
        let hPadding = this.actor.get_theme_node().get_horizontal_padding();

        let [childMinHeight, childNaturalHeight] = this._switcher.actor.get_preferred_height(primary.width - hPadding);
        let [childMinWidth, childNaturalWidth] = this._switcher.actor.get_preferred_width(childNaturalHeight);
        childBox.x1 = Math.max(primary.x + leftPadding, primary.x + Math.floor((primary.width - childNaturalWidth) / 2));
        childBox.x2 = Math.min(primary.width - hPadding, childBox.x1 + childNaturalWidth);
        childBox.y1 = primary.y + Math.floor((primary.height - childNaturalHeight) / 2);
        childBox.y2 = childBox.y1 + childNaturalHeight;
        this._switcher.actor.allocate(childBox, flags);
    },

    /** Shows the ctrl-alt-tab popup, fading it on over a delay of
     * {@link POPUP_FADE_TIME}.
     *
     * This also listens to key presses (for the user cycling to the next/
     * previous item).
     *
     * @param {CtrlAltTab.CtrlAltTabItem[]} items - array of
     * {@link CtrlAltTab.CtrlAltTabItem}s describing the items to display (see
     * the {@link CtrlAltTabManager#popup}).
     * @param {boolean} startBackwards - whether we are cycling through the
     * items backwards or forwards.
     * @param {int} mask - the (key) modifier mask that launched the popup.
     */
    show : function(items, startBackwards, mask) {
        if (!Main.pushModal(this.actor))
            return false;
        this._haveModal = true;
        this._modifierMask = AltTab.primaryModifier(mask);

        this._keyPressEventId = this.actor.connect('key-press-event', Lang.bind(this, this._keyPressEvent));
        this._keyReleaseEventId = this.actor.connect('key-release-event', Lang.bind(this, this._keyReleaseEvent));

        this._items = items;
        this._switcher = new CtrlAltTabSwitcher(items);
        this.actor.add_actor(this._switcher.actor);

        if (startBackwards)
            this._selection = this._items.length - 1;
        this._select(this._selection);

        let [x, y, mods] = global.get_pointer();
        if (!(mods & this._modifierMask)) {
            this._finish();
            return false;
        }

        this.actor.opacity = 0;
        this.actor.show();
        Tweener.addTween(this.actor,
                         { opacity: 255,
                           time: POPUP_FADE_TIME,
                           transition: 'easeOutQuad'
                         });

        return true;
    },

    /** Returns the index of the next item (wraps around).
     * @returns {number} index of the next item */
    _next : function() {
        return mod(this._selection + 1, this._items.length);
    },

    /** Returns the index of the previous item (wraps around).
     * @returns {number} index of the next item */
    _previous : function() {
        return mod(this._selection - 1, this._items.length);
    },

    /** Handler for key-press event while the ctrl alt tab popup is showing.
     * If the key pressed was:
     * 
     * - Escape: the popup stops showing (destroys itself).
     * - Tab, while Shift is held down or left key: goes to the previous item.
     * - Tab/right key: goes to the next item.
     */
    _keyPressEvent : function(actor, event) {
        let keysym = event.get_key_symbol();
        let shift = (Shell.get_event_state(event) & Clutter.ModifierType.SHIFT_MASK);
        if (shift && keysym == Clutter.KEY_Tab)
            keysym = Clutter.ISO_Left_Tab;

        if (keysym == Clutter.KEY_Escape)
            this.destroy();
        else if (keysym == Clutter.KEY_Tab)
            this._select(this._next());
        else if (keysym == Clutter.KEY_ISO_Left_Tab)
            this._select(this._previous());
        else if (keysym == Clutter.KEY_Left)
            this._select(this._previous());
        else if (keysym == Clutter.KEY_Right)
            this._select(this._next());

        return true;
    },

    /** Handler for key-release event while the ctrl alt tab popup is showing.
     * If the user let go of any of the keys they used to launch the popup
     * (i.e. ctrl+alt or ctrl+alt+shift), the popup is closed and the selected
     * item is given focus.
     */
    _keyReleaseEvent : function(actor, event) {
        let [x, y, mods] = global.get_pointer();
        let state = mods & this._modifierMask;

        if (state == 0)
            this._finish();

        return true;
    },

    /** Closes the popup and gives keyboard focus to the currently-selected
     * item (see {@link CtrlAltManager#focusGroup}). */
    _finish : function() {
        this.destroy();

        Main.ctrlAltTabManager.focusGroup(this._items[this._selection]);
    },

    /** Pops the dialog from the modal stack.
     * Called from {@link CtrlAltTabManager#destroy}. */
    _popModal: function() {
        if (this._haveModal) {
            Main.popModal(this.actor);
            this._haveModal = false;
        }
    },

    /** Destroys the dialog, after fading it out over time {@link POPUP_FADE_TIME}. */
    destroy : function() {
        this._popModal();
        Tweener.addTween(this.actor,
                         { opacity: 0,
                           time: POPUP_FADE_TIME,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this,
                               function() {
                                   this.actor.destroy();
                               })
                         });
    },

    /** Callback when {@link CtrlAltTabManager#actor} is destroyed.
     * Cleans up by popping the dialog off the modal stack and disconnecting
     * from the key press/release signals. */
    _onDestroy : function() {
        this._popModal();
        if (this._keyPressEventId)
            this.actor.disconnect(this._keyPressEventId);
        if (this._keyReleaseEventId)
            this.actor.disconnect(this._keyReleaseEventId);
    },

    /** Selects the specified item in the popup (see
     * {@link AltTab.SwitcherList#highlight}).
     * @param {number} num - the index of the item to highlight.
     */
    _select : function(num) {
        this._selection = num;
        this._switcher.highlight(num);
    }
};

/** creates a new CtrlAltTabSwitcher.
 *
 * This adds all the `items` to the switcher (showing both an icon and the name
 * for each item on a label underneath).
 *
 * All items will be allocated a square space.
 * @param {CtrlAltTab.CtrlAltTabItem[]} items - array of
 * {@link CtrlAltTab.CtrlAltTabItem}s describing the items to add to the switcher.
 * @classdesc
 * ![Ctrl Alt Tab Popup with the switcher list inside.](https://live.gnome.org/GnomeShell/CheatSheet?action=AttachFile&do=get&target=shortcuts-ctrl-alt-tab.png)
 *
 * The `CtrlAltTabSwitcher` is the entire visible bit of the Ctrl+Alt+Tab popup;
 * the {@link CtrlAltTabPopup} is actually a clear object that covers the entire
 * screen and has the Switcher centred inside it.
 *
 * @class
 * @extends AltTab.SwitcherList
 */
function CtrlAltTabSwitcher(items) {
    this._init(items);
}

CtrlAltTabSwitcher.prototype = {
    __proto__ : AltTab.SwitcherList.prototype,

    _init : function(items) {
        AltTab.SwitcherList.prototype._init.call(this, true);

        for (let i = 0; i < items.length; i++)
            this._addIcon(items[i]);
    },

    /** Adds a {@link CtrlAtTab.CtrlAltTabItem} to the switcher list.
     * @param {CtrlAltTab.CtrlAltTabItem} item - item to add. Its icon
     * (either from `item.icon` or `item.iconName`) and a label for its name
     * (from `item.name`) will be included in the popup.
     */
    _addIcon : function(item) {
        let box = new St.BoxLayout({ style_class: 'alt-tab-app',
                                     vertical: true });

        let icon = item.iconActor;
        if (!icon) {
            icon = new St.Icon({ icon_name: item.iconName,
                                 icon_type: St.IconType.SYMBOLIC,
                                 icon_size: POPUP_APPICON_SIZE });
        }
        box.add(icon, { x_fill: false, y_fill: false } );

        let text = new St.Label({ text: item.name });
        box.add(text, { x_fill: false });

        this.addItem(box, text);
    }
};
