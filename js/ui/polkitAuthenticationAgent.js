// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Handles popping up a password dialog on receiving Polkit authentication requests
 * (e.g. on updating software).
 *
 * See also the [Polkit documentation](http://www.freedesktop.org/software/polkit/docs/latest/index.html).
 *
 * The module is very similar to {@link namespace:NetworkAgent} in that there
 * is an Agent class that wraps around its corresponding `Shell` class and
 * relays authentication requests from these to the user by creating Dialogs
 * for each request.
 */
/*
 * Copyright 2010 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
*
 * Author: David Zeuthen <davidz@redhat.com>
 */

const Lang = imports.lang;
const Signals = imports.signals;
const Shell = imports.gi.Shell;
const AccountsService = imports.gi.AccountsService;
const Clutter = imports.gi.Clutter;
const St = imports.gi.St;
const Pango = imports.gi.Pango;
const Gio = imports.gi.Gio;
const Mainloop = imports.mainloop;
const Polkit = imports.gi.Polkit;
const PolkitAgent = imports.gi.PolkitAgent;

const ModalDialog = imports.ui.modalDialog;
const ShellEntry = imports.ui.shellEntry;

/** creates a new AuthenticationDialog
 *
 * This calls the parent constructor with style class 'polkit-dialog' and
 * the style classes for its contents are also 'polkit-dialog-*'.
 *
 * It creates a title for the dialog ("Authentication Required") as well as
 * a label for the `message`, a password box with a context menu
 * (see {@link ShellEntry.addContextMenu}) and a place for any errors to go.
 *
 * "Cancel" and "Authenticate" buttons are created.
 *
 * Then, a `Polkit.UnixUser` is created for the first identity in `userNames`
 * and a {@link PolkitAgent.Session} is created to do all the requests in.
 * @inheritparams AuthenticationAgent#_onInitiate
 * @classdesc
 * This is the password request dialog that pops up when the {@link AuthenticationAgent}
 * receives an authentication request.
 *
 * ![AuthenticationDialog](pics/polkitAuthenticationDialog.png)
 *
 * The creation and destroying of these dialogs is controlled by the
 * {@link AuthenticationAgent}.
 *
 * It wraps around a {@link PolkitAgent.Session} which does all the work.
 *
 * @class
 * @extends ModalDialog.ModalDialog
 */
function AuthenticationDialog(actionId, message, cookie, userNames) {
    this._init(actionId, message, cookie, userNames);
}

AuthenticationDialog.prototype = {
    __proto__: ModalDialog.ModalDialog.prototype,

    _init: function(actionId, message, cookie, userNames) {
        ModalDialog.ModalDialog.prototype._init.call(this, { styleClass: 'polkit-dialog' });

        this.actionId = actionId;
        this.message = message;
        this.userNames = userNames;
        this._wasDismissed = false;
        this._completed = false;

        let mainContentBox = new St.BoxLayout({ style_class: 'polkit-dialog-main-layout',
                                                vertical: false });
        this.contentLayout.add(mainContentBox,
                               { x_fill: true,
                                 y_fill: true });

        let icon = new St.Icon({ icon_name: 'dialog-password-symbolic' });
        mainContentBox.add(icon,
                           { x_fill:  true,
                             y_fill:  false,
                             x_align: St.Align.END,
                             y_align: St.Align.START });

        let messageBox = new St.BoxLayout({ style_class: 'polkit-dialog-message-layout',
                                            vertical: true });
        mainContentBox.add(messageBox,
                           { y_align: St.Align.START });

        this._subjectLabel = new St.Label({ style_class: 'polkit-dialog-headline',
                                            text: _("Authentication Required") });

        messageBox.add(this._subjectLabel,
                       { y_fill:  false,
                         y_align: St.Align.START });

        this._descriptionLabel = new St.Label({ style_class: 'polkit-dialog-description',
                                                text: message });
        this._descriptionLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._descriptionLabel.clutter_text.line_wrap = true;

        messageBox.add(this._descriptionLabel,
                       { y_fill:  true,
                         y_align: St.Align.START });

        if (userNames.length > 1) {
            log('polkitAuthenticationAgent: Received ' + userNames.length +
                ' identities that can be used for authentication. Only ' +
                'considering the first one.');
        }

        let userName = userNames[0];

        /** Store the current user to monitor it for changes (in avatar and so on).
         * @type {AccountsService.User} */
        this._user = AccountsService.UserManager.get_default().get_user(userName);
        let userRealName = this._user.get_real_name()
        this._userLoadedId = this._user.connect('notify::is_loaded',
                                                Lang.bind(this, this._onUserChanged));
        this._userChangedId = this._user.connect('changed',
                                                 Lang.bind(this, this._onUserChanged));

        // Special case 'root'
        let userIsRoot = false;
        if (userName == 'root') {
            userIsRoot = true;
            userRealName = _("Administrator");
        }

        if (userIsRoot) {
            let userLabel = new St.Label(({ style_class: 'polkit-dialog-user-root-label',
                                            text: userRealName }));
            messageBox.add(userLabel);
        } else {
            let userBox = new St.BoxLayout({ style_class: 'polkit-dialog-user-layout',
                                             vertical: false });
            messageBox.add(userBox);
            this._userIcon = new St.Icon();
            this._userIcon.hide();
            userBox.add(this._userIcon,
                        { x_fill:  true,
                          y_fill:  false,
                          x_align: St.Align.END,
                          y_align: St.Align.START });
            let userLabel = new St.Label(({ style_class: 'polkit-dialog-user-label',
                                            text: userRealName }));
            userBox.add(userLabel,
                        { x_fill:  true,
                          y_fill:  false,
                          x_align: St.Align.END,
                          y_align: St.Align.MIDDLE });
        }

        this._onUserChanged();

        this._passwordBox = new St.BoxLayout({ vertical: false });
        messageBox.add(this._passwordBox);
        this._passwordLabel = new St.Label(({ style_class: 'polkit-dialog-password-label' }));
        this._passwordBox.add(this._passwordLabel);
        this._passwordEntry = new St.Entry({ style_class: 'polkit-dialog-password-entry',
                                             text: "",
                                             can_focus: true});
        ShellEntry.addContextMenu(this._passwordEntry, { isPassword: true });
        this._passwordEntry.clutter_text.connect('activate', Lang.bind(this, this._onEntryActivate));
        this._passwordBox.add(this._passwordEntry,
                              {expand: true });
        this.setInitialKeyFocus(this._passwordEntry);
        this._passwordBox.hide();

        this._errorMessageLabel = new St.Label({ style_class: 'polkit-dialog-error-label' });
        this._errorMessageLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._errorMessageLabel.clutter_text.line_wrap = true;
        messageBox.add(this._errorMessageLabel);
        this._errorMessageLabel.hide();

        this._infoMessageLabel = new St.Label({ style_class: 'polkit-dialog-info-label' });
        this._infoMessageLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._infoMessageLabel.clutter_text.line_wrap = true;
        messageBox.add(this._infoMessageLabel);
        this._infoMessageLabel.hide();

        /* text is intentionally non-blank otherwise the height is not the same as for
         * infoMessage and errorMessageLabel - but it is still invisible because
         * gnome-shell.css sets the color to be transparent
         */
        this._nullMessageLabel = new St.Label({ style_class: 'polkit-dialog-null-label',
                                                text: 'abc'});
        this._nullMessageLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._nullMessageLabel.clutter_text.line_wrap = true;
        messageBox.add(this._nullMessageLabel);
        this._nullMessageLabel.show();

        this.setButtons([{ label: _("Cancel"),
                           action: Lang.bind(this, this.cancel),
                           key:    Clutter.Escape
                         },
                         { label:  _("Authenticate"),
                           action: Lang.bind(this, this._onAuthenticateButtonPressed)
                         }]);

        this._doneEmitted = false;

        this._identityToAuth = Polkit.UnixUser.new_for_name(userName);
        this._cookie = cookie;

        /** A local `PolkitAgent.Session` to do all the requests with.
         * @type {PolkitAgent.Session} */
        this._session = new PolkitAgent.Session({ identity: this._identityToAuth,
                                                  cookie: this._cookie });
        this._session.connect('completed', Lang.bind(this, this._onSessionCompleted));
        this._session.connect('request', Lang.bind(this, this._onSessionRequest));
        this._session.connect('show-error', Lang.bind(this, this._onSessionShowError));
        this._session.connect('show-info', Lang.bind(this, this._onSessionShowInfo));
    },

    /** Starts authentication (calls [`polkit_agent_session_initiate`](http://www.freedesktop.org/software/polkit/docs/latest/PolkitAgentSession.html#polkit-agent-session-initiate_)). */
    startAuthentication: function() {
        this._session.initiate();
    },

    /** Ensures that the dialog is open if it is not already.
     * If it failed to open, {@link #_emitDone} is called.
     */
    _ensureOpen: function() {
        // NOTE: ModalDialog.open() is safe to call if the dialog is
        // already open - it just returns true without side-effects
        if (!this.open(global.get_current_time())) {
            // This can fail if e.g. unable to get input grab
            //
            // In an ideal world this wouldn't happen (because the
            // Shell is in complete control of the session) but that's
            // just not how things work right now.
            //
            // One way to make this happen is by running 'sleep 3;
            // pkexec bash' and then opening a popup menu.
            //
            // We could add retrying if this turns out to be a problem

            log('polkitAuthenticationAgent: Failed to show modal dialog.' +
                ' Dismissing authentication request for action-id ' + this.actionId +
                ' cookie ' + this._cookie);
            this._emitDone(false, true);
        }
    },

    /** Emits the 'done' signal from the dialog with booleans defining whether
     * to keep the dialog open for a bit longer before closing it (for example
     * to see an error message), and whether the dialog was dismissed by the
     * user or not.
     *
     * This is listened to and managed by the {@link AuthenticationAgent}.
     * @see AuthenticationAgent#_onDialogDone
     * @inheritparams AuthenticationAgent#_onDialogDone
     * @fires .done
     */
    _emitDone: function(keepVisible, dismissed) {
        if (!this._doneEmitted) {
            this._doneEmitted = true;
            this.emit('done', keepVisible, dismissed);
        }
    },

    /** Callback when the user presses enter on the password entry.
     * It sends the password to the Polkit Session
     * {@link #_session}. (The outcome of the request
     * will be emitted via signals from the session that were already connected
     * to in the constructor). */
    _onEntryActivate: function() {
        let response = this._passwordEntry.get_text();
        this._session.response(response);
        // When the user responds, dismiss already shown info and
        // error texts (if any)
        this._errorMessageLabel.hide();
        this._infoMessageLabel.hide();
        this._nullMessageLabel.show();
    },

    /** Callbac when the user presses the "authenticate" button. This just calls
     * {@link #_onEntryActivate} to start the
     * authentication.*/
    _onAuthenticateButtonPressed: function() {
        this._onEntryActivate();
    },

    /** callback for the 'complete' signal of {@link #_session}.
     * Emitted when the session has been completed or cancelled.
     *
     * If the authorization was not successfully obtained and an error message
     * is not already showing, this displays an error message "Sorry, that
     * didn't work. Please try again."
     *
     * Regardless of whether authorization was obtained or not, this calls
     * {@link #_emitDone} with `keepOpen=true` if there
     * is an error message the user needs to see, and `dismissed=false`.
     *
     * @param {PolkitAgent.Session} session - the session that emitted the signal
     * ({@link #_session}).
     * @param {boolean} gainedAuthorization - whether the user successfully
     * authenticated.
     */
    _onSessionCompleted: function(session, gainedAuthorization) {
        if (this._completed)
            return;

        this._completed = true;

        if (!gainedAuthorization) {
            /* Unless we are showing an existing error message from the PAM
             * module (the PAM module could be reporting the authentication
             * error providing authentication-method specific information),
             * show "Sorry, that didn't work. Please try again."
             */
            if (!this._errorMessageLabel.visible && !this._wasDismissed) {
                /* Translators: "that didn't work" refers to the fact that the
                 * requested authentication was not gained; this can happen
                 * because of an authentication error (like invalid password),
                 * for instance. */
                this._errorMessageLabel.set_text(_("Sorry, that didn\'t work. Please try again."));
                this._errorMessageLabel.show();
                this._infoMessageLabel.hide();
                this._nullMessageLabel.hide();
            }
        }
        this._emitDone(!gainedAuthorization, false);
    },

    /** callback for the 'request' signal of {@link #_session}.
     * Emitted when the user is requested to answer a question.
     *
     * This uses {@link #_ensureOpen} to make sure the 
     * dialog is open (it probably is already) and shows the password box.
     *
     * @inheritparams #_onSessionCompleted
     * @param {string} request - request to show the user, e.g. 'Name:' or 'Password:'.
     * @param {boolean} echo_on - whether to show characters or ●  (dot) in the
     * password box.
     */
    _onSessionRequest: function(session, request, echo_on) {
        // Cheap localization trick
        if (request == 'Password:')
            this._passwordLabel.set_text(_("Password:"));
        else
            this._passwordLabel.set_text(request);

        if (echo_on)
            this._passwordEntry.clutter_text.set_password_char('');
        else
            this._passwordEntry.clutter_text.set_password_char('\u25cf'); // ● U+25CF BLACK CIRCLE

        this._passwordBox.show();
        this._passwordEntry.set_text('');
        this._passwordEntry.grab_key_focus();
        this._ensureOpen();
    },

    /** Callback for the 'show-error' signal of {@link #_session}.
     * Emitted when there is an error to be displayed to the user.
     *
     * This displays the message and makes sure the dialog is open (see
     * {@link #_ensureOpen}) but does nothing else.
     *
     * @inheritparams #_onSessionCompleted
     * @param {string} text - the message to show the user.
     */
    _onSessionShowError: function(session, text) {
        this._passwordEntry.set_text('');
        this._errorMessageLabel.set_text(text);
        this._errorMessageLabel.show();
        this._infoMessageLabel.hide();
        this._nullMessageLabel.hide();
        this._ensureOpen();
    },

    /** Callback for the 'show-info' signal of {@link #_session}.
     * Emitted when there is information to be displayed to the user.
     *
     * This displays the message and makes sure the dialog is open (see
     * {@link #_ensureOpen}) but does nothing else.
     *
     * @inheritparams #_onSessionShowError
     */
    _onSessionShowInfo: function(session, text) {
        this._passwordEntry.set_text('');
        this._infoMessageLabel.set_text(text);
        this._infoMessageLabel.show();
        this._errorMessageLabel.hide();
        this._nullMessageLabel.hide();
        this._ensureOpen();
    },

    /** Destroys the session {@link #_session}.
     * After this the dialog is useless, so you should call it with
     * {@link #close}. */
    destroySession: function() {
        if (this._session) {
            if (!this._completed)
                this._session.cancel();
            this._session = null;
        }
    },

    /** Callback when the user details change or are loaded. (Fired on the
     * `AccountsService.UserManager` 'changed' and 'notify::is-loaded' signals).
     *
     * This updates the avatar shown on the dialog.
     * @see #_user
     */
    _onUserChanged: function() {
        if (this._user.is_loaded) {
            if (this._userIcon) {
                let iconFileName = this._user.get_icon_file();
                let iconFile = Gio.file_new_for_path(iconFileName);
                let icon;
                if (iconFile.query_exists(null)) {
                    icon = new Gio.FileIcon({file: iconFile});
                } else {
                    icon = new Gio.ThemedIcon({name: 'avatar-default'});
                }
                this._userIcon.set_gicon (icon);
                this._userIcon.show();
            }
        }
    },

    /** Callback when the user clicks the "Cancel" button. Closes the dialog and
     * calls {@link #_emitDone} with `keepVisible=false`
     * and `dismissed=true` (hence causes the
     * [done signal]{@link .event:done} to be fired.
     *
     * Note this is the only time the dialog closes itself; all other times,
     * the actual closing of the dialog is done by the {@link AuthenticationAgent}
     * when it catches the dialog's 'done' signal. */
    cancel: function() {
        this._wasDismissed = true;
        this.close(global.get_current_time());
        this._emitDone(false, true);
    },

};
Signals.addSignalMethods(AuthenticationDialog.prototype);
/** Emitted when the dialog is closed, either because of an authentication
 * or the user cancelled it.
 * @event
 * @memberof AuthenticationDialog
 * @name done
 * @param {PolkitAuthenticationAgent.AuthenticationDialog} o - instance that emitted the signal
 * @param {boolean} keepVisible - whether to keep the dialog visible
 * for a couple of seconds before closing it (for example if they had
 * an authentication failure then they need a chance to read it).
 * @param {boolean} dismissed - whether the user dismissed the dialog and
 * cancelled the request (as opposed to a 'cancel' signal being received
 * which is not done by the user).
 * /

/** creates a new AuthenticationAgent
 * @classdesc
 * Wraps around a {@link Shell.PolkitAuthenticationAgent}
 * instance to listen for authentication requests and create
 * {@link AuthenticationDialog}s for them.
 *
 * Only one instance needs to be made; this is created when `main.js` calls
 * {@link init}.
 *
 * Very similar to the {@link NetworkAgent.NetworkAgent} except instead of
 * a Shell.NetworkAgent, it's a Shell.PolkitAuthenticationAgent.
 *
 * See also the [Polkit Agent documentation](http://www.freedesktop.org/software/polkit/docs/latest/PolkitAgentListener.html).
 * @class
 */
function AuthenticationAgent() {
    this._init();
}

AuthenticationAgent.prototype = {
    _init: function() {
        /** Local {@link Shell.PolkitAuthenticationAgent`}
         * instance that this class wraps around.
         * We listen for the 'initiate' and 'cancel' requests to create
         * {@link AuthenticationDialog}s for them.
         * @type {Shell.PolkitAuthenticationAgent}. */
        this._native = new Shell.PolkitAuthenticationAgent();
        this._native.connect('initiate', Lang.bind(this, this._onInitiate));
        this._native.connect('cancel', Lang.bind(this, this._onCancel));
        /** Authentication dialog for the current request (you only get
         * one at a time as far as I can tell).
         * @type {?PolkitAuthenticationAgent.AuthenticationDialog} */
        this._currentDialog = null;
        this._isCompleting = false;
    },

    /** Callback for the 'initiate' signal for the Shell.PolkitAuthenticationAgent,
     * when an authentication request is initiated.
     *
     * This creates a new {@link AuthenticationDialog} for the request, starts
     * the authentication ({@link AuthenticationDialog#startAuthentication}),
     * and waits on its ['done' signal]{@link AuthenticationDialog.event:done} in order
     * to then dismiss the dialog.
     *
     * @param {Shell.PolkitAuthenticationAgent} nativeAgent - agent emitting
     * the signal {@link #_native}.
     * @param {string} actionId - the action we are seeking authentication for.
     * @param {string} message - the message to present to the user
     * @param {?string} iconName - icon name representing the action (will be
     * displayed on the dialog).
     * @param {string} cookie - cookie for the authentication request.
     * @param {Array.<??>} userNames - an array of [PolkitIdentity](http://www.freedesktop.org/software/polkit/docs/latest/PolkitIdentity.html) objects that the user can choose to authenticate as
     * (the {@link AuthenticationDialog} only considers the first).
     */
    _onInitiate: function(nativeAgent, actionId, message, iconName, cookie, userNames) {
        this._currentDialog = new AuthenticationDialog(actionId, message, cookie, userNames);

        // We actually don't want to open the dialog until we know for
        // sure that we're going to interact with the user. For
        // example, if the password for the identity to auth is blank
        // (which it will be on a live CD) then there will be no
        // conversation at all... of course, we don't *know* that
        // until we actually try it.
        //
        // See https://bugzilla.gnome.org/show_bug.cgi?id=643062 for more
        // discussion.

        this._currentDialog.connect('done', Lang.bind(this, this._onDialogDone));
        this._currentDialog.startAuthentication();
    },

    /** Callback for the 'cancel' signal for the Shell.PolkitAuthenticationAgent,
     * triggered when an authentication request is canceled.
     *
     * This calls {@link #_completeRequest} to dismiss
     * the dialog.
     * @inheritparams #_onInitiate
     */
    _onCancel: function(nativeAgent) {
        this._completeRequest(false, false);
    },

    /** Callback when {@link #_currentDialog} (i.e. the
     * dialog for the current authentication request) emits the
     * ['done']{@link AuthenticationDialog.done} signal (user clicked one of
     * the buttons). This calls {@link #_completeRequest}
     * to dismiss the dialog/show the authentication error and then dismiss
     * the dialog.
     * @param {PolkitAuthenticationAgent.AuthenticationDialog} dialog - dialog emitting
     * the signal ({@link #_currentDialog}).
     * @param {boolean} keepVisible - whether to keep the dialog visible
     * for a couple of seconds before closing it (for example if they had
     * an authentication failure then they need a chance to read it).
     * @param {boolean} dismissed - whether the user dismissed the dialog and
     * cancelled the request (as opposed to a 'cancel' signal being received
     * which is not done by the user).
     */
    _onDialogDone: function(dialog, keepVisible, dismissed) {
        this._completeRequest(keepVisible, dismissed);
    },

    /** This actually finishes an authentication request, by calling
     * [the `complete` method of {@link #_native}](http://developer.gnome.org/shell/unstable/shell-shell-polkit-authentication-agent.html#shell-polkit-authentication-agent-complete), and closing
     * the dialog/destroying its session.
     * @see AuthenticationDialog#destroySession
     * @see AuthenticationDialog#close
     * @inheritparams #_onDialogDone
     */
    _reallyCompleteRequest: function(dismissed) {
        this._currentDialog.close();
        this._currentDialog.destroySession();
        this._currentDialog = null;
        this._isCompleting = false;

        this._native.complete(dismissed)
    },

    /** This starts to complete an authentication request.
     * 
     * If `keepVisible` is true, {@link #_reallyCompleteRequest}
     * is called after a 2-second delay (this is to let the user read 'Authentication Failure'
     * before the dialog disappears if say they entered the wrong password).
     * Otherwise, it is called instantly (closing the dialog).
     * @inheritparams #_onDialogDone
     */
    _completeRequest: function(keepVisible, wasDismissed) {
        if (this._isCompleting)
            return;

        this._isCompleting = true;

        if (keepVisible) {
            // Give the user 2 seconds to read 'Authentication Failure' before
            // dismissing the dialog
            Mainloop.timeout_add(2000,
                                 Lang.bind(this,
                                           function() {
                                               this._reallyCompleteRequest(wasDismissed);
                                           }));
        } else {
            this._reallyCompleteRequest(wasDismissed);
        }
    }
}

/** Called to create a new Polkit Authentication Agent to listen for authentication
 * requests. Sets everything up. Called from {@link Main.start}.
 * @see AuthenticationAgent
 */
function init() {
    let agent = new AuthenticationAgent();
}
