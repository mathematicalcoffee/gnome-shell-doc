// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines a class that gives search results for 'recent items'.
 * @see DocInfo
 * @see DocManager
 */

const DocInfo = imports.misc.docInfo;
const Params = imports.misc.params;
const Search = imports.ui.search;


/** creates a new DocSearchProvider
 * @param {string} name - not used.
 * @classdesc A DocSearchProvider is a {@link Search.SearchProvider} that
 * searches through recent items.
 * It gathers item info via the helper class {@link DocInfo.DocManager} in
 * [`misc/docInfo.js`]{@link namespace:DocInfo}.
 * @extends Search.SearchProvider
 * @class
 */
function DocSearchProvider() {
    this._init();
}

DocSearchProvider.prototype = {
    __proto__: Search.SearchProvider.prototype,

    _init: function(name) {
        Search.SearchProvider.prototype._init.call(this, _("RECENT ITEMS"));
        /** A {@link DocInfo.DocManager} used to get files. The global
         * instance is used (see {@link DocInfo.getDocManager}) */
        this._docManager = DocInfo.getDocManager();
    },

    /** Implements {@link Search.SearchProvider#getResultMeta}, returning an
     * object describing a document.
     * @param {string} resultId - id of the result (file URI).
     * @returns {Search.ResultMeta} an object describing the search result, with
     * `id` being the document URI, `name` being the document name, and icon
     * corresponding to the document's type.
     * @see Search.SearchProvider#getResultMeta
     * @override
     */
    getResultMeta: function(resultId) {
        let docInfo = this._docManager.lookupByUri(resultId);
        if (!docInfo)
            return null;
        return { 'id': resultId,
                 'name': docInfo.name,
                 'createIcon': function(size) {
                                   return docInfo.createIcon(size);
                               }
               };
    },

    /** Implements {@link Search.SearchProvider#activateResult}, launching a
     * document upon activation.
     * @param {string} id - ID (URL) of the result to activate.
     * @param {{workspace: number, timestamp: number}} params - only the
     * 'workspace' member is used - the workspace on which to open the document.
     * @see Search.SearchProvider#activateResult
     * @override
     */
    activateResult: function(id, params) {
        params = Params.parse(params, { workspace: -1,
                                        timestamp: 0 });

        let docInfo = this._docManager.lookupByUri(id);
        docInfo.launch(params.workspace);
    },

    /** Implements {@link Search.SearchProvider#getInitialResultSet}.
     * @param {string[]} terms - array of terms to search.
     * @see DocInfo.DocManager#initialSearch
     * @override
     */
    getInitialResultSet: function(terms) {
        return this._docManager.initialSearch(terms);
    },

    /** Implements {@link Search.SearchProvider#getSubsearchResultSet} to return
     * documents within the current results matching the terms.
     * @param {string[]} previousResults - Array of item identifiers
     * @param {string[]} newTerms - Updated search terms
     * @return {string[]} an array of result identify strings as in
     * {@link Search.SearchProvider#getInitialResultSet}.
     * @see DocInfo.DocManager#subsearch
     * @override
     */
    getSubsearchResultSet: function(previousResults, terms) {
        return this._docManager.subsearch(previousResults, terms);
    }
};
