// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Shell magnifier (for accessibility): provide the DBus service
 * for magnifying in gnome-shell (org.gnome.Magnifier, org.gnome.Magnifier.ZoomRegion).
 * objects on DBus.
 *
 * These functions mainly wrap around the {@link Magnifier.Magnifier} stored as
 * {@link Main.magnifier}.
 */

const DBus = imports.dbus;
const Main = imports.ui.main;

/** @+
 * @const
 * @default
 * @type {string} */
/** DBus interface name for gnome-shell magnifier. */
const MAG_SERVICE_NAME = 'org.gnome.Magnifier';
/** DBus path for gnome-shell magnifier. */
const MAG_SERVICE_PATH = '/org/gnome/Magnifier';
/** DBus Interface name for gnome-shell magnifier zoom regions. */
const ZOOM_SERVICE_NAME = 'org.gnome.Magnifier.ZoomRegion';
/** DBus path for gnome-shell magnifier zoom regions (an integer will
 * be appended on the end to make them unique). */
const ZOOM_SERVICE_PATH = '/org/gnome/Magnifier/ZoomRegion';
/** @- */

/** Subset of gnome-mag's Magnifier dbus interface -- to be expanded.  See:
 * http://git.gnome.org/browse/archive/gnome-mag/tree/xml/org.freedesktop.gnome.Magnifier.xml
 *
 * The service is provided by {@link ShellMagnifier}.
 *
 * Only one instance for all of gnome-shell needs to be made to make this
 * service available over DBus. This is stored as
 * {@link Magnifier.magDBusService}.
 * @const
 * @see ShellMagnifier
 */
// methods documented in ShellMagnifier.
const MagnifierIface = {
    name: MAG_SERVICE_NAME,
    methods: [
                { name: 'setActive', inSignature: 'b', outSignature: '' },
                { name: 'isActive', inSignature: '', outSignature: 'b' },
                { name: 'showCursor', inSignature: '', outSignature: '' },
                { name: 'hideCursor', inSignature: '', outSignature: ''  },
                { name: 'createZoomRegion', inSignature: 'ddaiai', outSignature: 'o' },
                { name: 'addZoomRegion', inSignature: 'o', outSignature: 'b' },
                { name: 'getZoomRegions', inSignature: '', outSignature: 'ao' },
                { name: 'clearAllZoomRegions', inSignature: '', outSignature: '' },
                { name: 'fullScreenCapable', inSignature: '', outSignature: 'b' },

                { name: 'setCrosswireSize', inSignature: 'i', outSignature: '' },
                { name: 'getCrosswireSize', inSignature: '', outSignature: 'i' },
                { name: 'setCrosswireLength', inSignature: 'i', outSignature: '' },
                { name: 'getCrosswireLength', inSignature: '', outSignature: 'i' },
                { name: 'setCrosswireClip', inSignature: 'b', outSignature: '' },
                { name: 'getCrosswireClip', inSignature: '', outSignature: 'b' },
                { name: 'setCrosswireColor', inSignature: 'u', outSignature: '' },
                { name: 'getCrosswireColor', inSignature: '', outSignature: 'u' }
             ],
    signals: [],
    properties: []
};

/** Subset of gnome-mag's ZoomRegion dbus interface -- to be expanded.  See:
 * http://git.gnome.org/browse/archive/gnome-mag/tree/xml/org.freedesktop.gnome.ZoomRegion.xml
 *
 * The implementation of this object is {@link ShellMagnifierZoomRegion}; each
 * one is made available over DBus at path {@link ZOOM_SERVICE_PATH} (with
 * numbers appended to make each zoom region unique).
 *
 * These are created and managed the {@link ShellMagnifier}; the external
 * user probably shouldn't use them directly.
 *
 * @const
 * @see ShellMagnifierZoomRegion
 */
// Methods documented in ShellMagnifierZoomRegion
const ZoomRegionIface = {
    name: ZOOM_SERVICE_NAME,
    methods: [
                { name: 'setMagFactor', inSignature: 'dd', outSignature: ''},
                { name: 'getMagFactor', inSignature: '', outSignature: 'dd' },
                { name: 'setRoi', inSignature: 'ai', outSignature: '' },
                { name: 'getRoi', inSignature: '', outSignature: 'ai' },
                { name: 'shiftContentsTo', inSignature: 'ii', outSignature: 'b' },
                { name: 'moveResize', inSignature: 'ai', outSignature: '' }
             ],
    signals: [],
    properties: []
};

/** Used by {@link ShellMagnifier} for making unique ZoomRegion DBus proxy
 * object paths of the form:
 * '/org/gnome/Magnifier/ZoomRegion/zoomer0',
 * '/org/gnome/Magnifier/ZoomRegion/zoomer1', etc.
 * @type {number}
 */
let _zoomRegionInstanceCount = 0;

/** creates a new ShellMagnifier
 * @classdesc
 * This provides the 'org.gnome.Magnifier' service over DBus so that other
 * programs can connect to the bus and call its methods in order to use the
 * magnifier.
 *
 * The constructor exports this instance to {@link MAG_SERVICE_PATH}.
 *
 * @class
 * This provides the 'org.gnome.Magnifier' service over DBus so that other
 * programs can connect to the bus and call its methods in order to use the
 * magnifier. (ie it implements the Magnifier interface, see {@link MagnifierIface}).
 *
 * It is exported to path {@link MAG_SERVICE_PATH} ('/org/gnome/Magnifier').
 *
 * Almost all of the functions just wrap around {@link Magnifier.Magnifier}.
 * @see Magnifier.Magnifier
 */
function ShellMagnifier() {
    this._init();
}

ShellMagnifier.prototype = {
    _init: function() {
        /* list of zoom regions */
        this._zoomers = {};
        DBus.session.exportObject(MAG_SERVICE_PATH, this);
    },

    /** Activate or deactivate the magnifier.
     * @param {boolean} activate - whether to activate or de-activate the magnifier.
     * @see Magnifier.Magnifier#setActive
     */
    setActive: function(activate) {
        Main.magnifier.setActive(activate);
    },

    /** Ask whether the magnifier is active.
     * @returns {boolean}  Whether the magnifier is active.
     * @see Magnifier.Magnifier#isActive
     */
    isActive: function() {
        return Main.magnifier.isActive();
    },

    /**
     * Show the system mouse pointer.
     * @see Magnifier.Magnifier#showSystemCursor
     */
    showCursor: function() {
        Main.magnifier.showSystemCursor();
    },

    /**
     * Hide the system mouse pointer.
     * @see Magnifier.Magnifier#hideSystemCursor
     */
    hideCursor: function() {
        Main.magnifier.hideSystemCursor();
    },

    /**
     * Create a new ZoomRegion and return its object path (input signature 'ddaiai').
     * @see ShellMagnifierZoomRegion
     * @see Magnifier.ZoomRegion
     * @param {number} xMagFactor - The power to set horizontal magnification of the
     *                  ZoomRegion.  A value of 1.0 means no magnification.  A
     *                  value of 2.0 doubles the size.
     * @param {number} yMagFactor - The power to set the vertical magnification of the
     *                  ZoomRegion.
     * @param {[number, number, number, number]} roi - Array of integers
     *                  defining the region of the
     *                  screen/desktop to magnify.  The array has the form
     *                  `[left, top, right, bottom]`.
     * @param {[number, number, number, number]} viewPort - Array of integers,
     *                  `[left, top, right, bottom]` that defines
     *                  the position of the ZoomRegion on screen.
     *
     * FIXME: The arguments here are redundant, since the width and height of
     *   the ROI are determined by the viewport and magnification factors.
     *   We ignore the passed in width and height.
     *
     * @returns {string} DBus object path to the newly created ZoomRegion (signature 'o').
     */
    createZoomRegion: function(xMagFactor, yMagFactor, roi, viewPort) {
        let ROI = { x: roi[0], y: roi[1], width: roi[2] - roi[0], height: roi[3] - roi[1] };
        let viewBox = { x: viewPort[0], y: viewPort[1], width: viewPort[2] - viewPort[0], height: viewPort[3] - viewPort[1] };
        let realZoomRegion = Main.magnifier.createZoomRegion(xMagFactor, yMagFactor, ROI, viewBox);
        let objectPath = ZOOM_SERVICE_PATH + '/zoomer' + _zoomRegionInstanceCount;
        _zoomRegionInstanceCount++;

        let zoomRegionProxy = new ShellMagnifierZoomRegion(objectPath, realZoomRegion);
        let proxyAndZoomRegion = {};
        proxyAndZoomRegion.proxy = zoomRegionProxy;
        proxyAndZoomRegion.zoomRegion = realZoomRegion;
        this._zoomers[objectPath] = proxyAndZoomRegion;
        return objectPath;
    },

    /**
     * Append the given ZoomRegion to the magnifier's list of ZoomRegions.
     * @param {string} zoomerObjectPath - The object path for the zoom region proxy (signature 'o').
     * @returns {boolean} whether the zoom region was added. It will only be
     * added if it was created with {@link #createZoomRegion}.
     * @see Magnifier.Magnifier#addZoomRegion
     */
    addZoomRegion: function(zoomerObjectPath) {
        let proxyAndZoomRegion = this._zoomers[zoomerObjectPath];
        if (proxyAndZoomRegion && proxyAndZoomRegion.zoomRegion) {
            Main.magnifier.addZoomRegion(proxyAndZoomRegion.zoomRegion);
            return true;
        }
        else
            return false;
    },

    /**
     * Return a list of ZoomRegion object paths for this Magnifier.
     * @returns {string[]} - The Magnifier's zoom region list as an array of DBus object
     *              paths (signature 'ao').
     */
    getZoomRegions: function() {
        // There may be more ZoomRegions in the magnifier itself than have
        // been added through dbus.  Make sure all of them are associated with
        // an object path and proxy.
        let zoomRegions = Main.magnifier.getZoomRegions();
        let objectPaths = [];
        let thoseZoomers = this._zoomers;
        zoomRegions.forEach (function(aZoomRegion, index, array) {
            let found = false;
            for (let objectPath in thoseZoomers) {
                let proxyAndZoomRegion = thoseZoomers[objectPath];
                if (proxyAndZoomRegion.zoomRegion === aZoomRegion) {
                    objectPaths.push(objectPath);
                    found = true;
                    break;
                }
            }
            if (!found) {
                // Got a ZoomRegion with no DBus proxy, make one.
                let newPath =  ZOOM_SERVICE_PATH + '/zoomer' + _zoomRegionInstanceCount;
                _zoomRegionInstanceCount++;
                let zoomRegionProxy = new ShellMagnifierZoomRegion(newPath, aZoomRegion);
                let proxyAndZoomer = {};
                proxyAndZoomer.proxy = zoomRegionProxy;
                proxyAndZoomer.zoomRegion = aZoomRegion;
                thoseZoomers[newPath] = proxyAndZoomer;
                objectPaths.push(newPath);
            }
        });
        return objectPaths;
    },

    /**
     * Remove all the zoom regions from this Magnfier's ZoomRegion list and
     * unexport them.
     */
    clearAllZoomRegions: function() {
        Main.magnifier.clearAllZoomRegions();
        for (let objectPath in this._zoomers) {
            let proxyAndZoomer = this._zoomers[objectPath];
            proxyAndZoomer.proxy = null;
            proxyAndZoomer.zoomRegion = null;
            delete this._zoomers[objectPath];
            DBus.session.unexportObject(proxyAndZoomer);
        }
        this._zoomers = {};
    },

    /**
     * Consult if the Magnifier can magnify in full-screen mode.
     * @returns {boolean} Always returns true.
     */
    fullScreenCapable: function() {
        return true;
    },

    /**
     * @inheritdoc Magnifier.Magnifier#setCrosshairsThickness
     */
     setCrosswireSize: function(size) {
        Main.magnifier.setCrosshairsThickness(size);
     },

    /**
     * @inheritdoc Magnifier.Magnifier#getCrosshairsThickness
     */
     getCrosswireSize: function() {
        return Main.magnifier.getCrosshairsThickness();
     },

    /**
     * @inheritdoc Magnifier.Magnifier#setCrosshairsLength
     */
     setCrosswireLength: function(length) {
        Main.magnifier.setCrosshairsLength(length);
     },

    /**
     * @inheritdoc Magnifier.Magnifier#getCrosshairsLength
     */
     getCrosswireLength: function() {
        return Main.magnifier.getCrosshairsLength();
     },

    /**
     * @inheritdoc Magnifier.Magnifier#setCrosshairsClip
     */
     setCrosswireClip: function(clip) {
        Main.magnifier.setCrosshairsClip(clip);
     },

    /**
     * @inheritdoc Magnifier.Magnifier#getCrosshairsClip
     */
     getCrosswireClip: function() {
        return Main.magnifier.getCrosshairsClip();
     },

    /**
     * Set the crosswire color of all ZoomRegions.
     * @param {number} color - Unsigned int of the form rrggbbaa.
     * @see Magnifier.Magnifier#setCrosshairsColor
      */
     setCrosswireColor: function(color) {
        Main.magnifier.setCrosshairsColor('#%08x'.format(color));
     },

    /**
     * Get the crosswire color of all ZoomRegions.
     * @returns {number} The crosswire color as an unsigned int in the form rrggbbaa.
     * @see Magnifier.Magnifier#getCrosshairsColor
     */
     getCrosswireColor: function() {
        let colorString = Main.magnifier.getCrosshairsColor();
        // Drop the leading '#'.
        return parseInt(colorString.slice(1), 16);
     }
};

/**
 * Creates a new ShellMagnifierZoomRegion.
 *
 * Also exports it to DBus path `zoomerObjectPath` (which is {@link ZOOM_SERVICE_PATH}
 * with an integer appended to make the path unique) and adds it on bus
 * {@link ZOOM_SERVICE_NAME}.
 * @param {string} zoomerObjectPath - String that is the path to a DBus ZoomRegion.
 * @param {Magnifier.ZoomRegion} zoomRegion - The actual zoom region associated with the object path.
 * @classdesc
 * Object that implements the DBus ZoomRegion interface ({@link ZoomRegionIface}).
 *
 * It is really just a wrapper around a {@link Magnifier.ZoomRegion}.
 * @see Magnifier.ZoomRegion
 * @class
 */
function ShellMagnifierZoomRegion(zoomerObjectPath, zoomRegion) {
    this._init(zoomerObjectPath, zoomRegion);
}

ShellMagnifierZoomRegion.prototype = {
    _init: function(zoomerObjectPath, zoomRegion) {
        this._zoomRegion = zoomRegion;
        DBus.session.proxifyObject(this, ZOOM_SERVICE_NAME, zoomerObjectPath);
        DBus.session.exportObject(zoomerObjectPath, this);
    },

    /** @inheritdoc Magnifier.ZoomRegion#setMagFactor */
    setMagFactor: function(xMagFactor, yMagFactor) {
        this._zoomRegion.setMagFactor(xMagFactor, yMagFactor);
    },

    /** @inheritdoc Magnifier.ZoomRegion#getMagFactor */
    getMagFactor: function() {
        return this._zoomRegion.getMagFactor();
    },

    /**
     * Sets the "region of interest" that the ZoomRegion is magnifying.
     * @param {number[]} roi - Array,
     *          `[left, top, right, bottom]`, defining the region of the
     *          screen to magnify. The values are in screen (unmagnified)
     *          coordinate space.
     * @see Magnifier.ZoomRegion#setROI
     */
    setRoi: function(roi) {
        let roiObject = { x: roi[0], y: roi[1], width: roi[2] - roi[0], height: roi[3] - roi[1] };
        this._zoomRegion.setROI(roiObject);
    },

    /** @inheritdoc Magnifier.ZoomRegion#getROI */
    getRoi: function() {
        let roi = this._zoomRegion.getROI();
        roi[2] += roi[0];
        roi[3] += roi[1];
        return roi;
    },

    /**
     * @inheritdoc Magnifier.ZoomRegion#scrollContentsTo
     * @returns {boolean} Whether the shift was successful (for GS-mag, this is
     *                    always true).
     */
    shiftContentsTo: function(x, y) {
        this._zoomRegion.scrollContentsTo(x, y);
        return true;
    },

    /**
     * Sets the position and size of the ZoomRegion on screen.
     * @param {number[]} viewPort - Array,
     *             `[left, top, right, bottom]`, defining the position and
     *              size on screen to place the zoom region.
     * @see Magnifier.ZoomRegion#setViewPort
     */
    moveResize: function(viewPort) {
        let viewRect = { x: viewPort[0], y: viewPort[1], width: viewPort[2] - viewPort[0], height: viewPort[3] - viewPort[1] };
        this._zoomRegion.setViewPort(viewRect);
    }
};

DBus.conformExport(ShellMagnifier.prototype, MagnifierIface);
DBus.conformExport(ShellMagnifierZoomRegion.prototype, ZoomRegionIface);
