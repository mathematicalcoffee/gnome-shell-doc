// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file has classes to do with laying out actors on the stage (panel,
 * message tray, hot corners, ...).
 *
 * The main class here is the {@link LayoutManager}. This organises the
 * main UI elements of the shell - the top panel, message tray, hot corners
 * (to toggle the overview), and on-screen keyboard (if it is being used).
 *
 * ![LayoutManager manages positioning of hot corners and UI elements; Chrome makes sure they receive events]{@link pics/layout.svg}
 *
 * The {@link LayoutManager} positions the top panel, message tray, and keyboard box
 * such that the top panel is always on the primary monitor and the message
 * tray and keyboard box are on the bottom-most.
 *
 * It also ensures that each monitor with a top-left corner (i.e. no monitors
 * above or to the left of them) are given a {@link HotCorner} (blue)
 * to toggle the overview with.
 *
 * Finally it creates panel barriers on either side of the top panel and on the
 * bottom-right of the bottom monitor to aid in finding the hot corners and
 * clicking on buttons in the top panel (red lines in the picture above).
 *
 * The {@link Chrome} maintains a region on the global stage such that events
 * that fall in this region (like clicks) are passed through to the global
 * stage, rather than windows. Any top-level reactive element like the
 * keyboard box, message tray, hot corners, and top panel *must* be included
 * in this region in order to respond to user interaction.
 *
 * The {@link Chrome} also works out what struts should be in effect. Struts
 * define regions where windows are not allowed to go - the windows should
 * see the "screen" as not including the struts. The top panel is a strut;
 * that is how all the windows know to avoid being placed there.
 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const Main = imports.ui.main;
const Params = imports.misc.params;
const ScreenSaver = imports.misc.screenSaver;
const Tweener = imports.ui.tweener;

/**@+
 * @const
 * @type {number}
 * @default */
/** When the hot corner has been triggered by mousing over it, this is
 * the number of seconds later that a click on the hot corner will trigger
 * the hot corner again. */
const HOT_CORNER_ACTIVATION_TIMEOUT = 0.5;
/** The time for the initial sliding-panel-down animation (seconds).
 * @see LayoutManager#_startupAnimation */
const STARTUP_ANIMATION_TIME = 0.2;
/** The time to slide the keyboard up or down (seconds).
 * @see LayoutManager#showKeyboard */
const KEYBOARD_ANIMATION_TIME = 0.5;
/** @- */

/** creates a new LayoutManager
 *
 * We store the monitor geometry in {@link #monitors}. For convenience,
 * we also store the primary monitor in {@link #primaryMonitor} and
 * {@link #primaryIndex}, and the bottom monitor in {@link #bottomMonitor} and
 * {@link #bottomIndex}.
 *
 * We then create a {@link Chrome} instance stored in {@link #_chrome}, which
 * will be used to ensure that the global stage's input region (the region
 * of the stage that will pass events such as clicks through to the actors
 * underneath as opposed to any windows underneath) remains up to date, as
 * well as the stage's struts. (See the {@link Chrome} documentation for
 * more information).
 *
 * Then we create some St.BoxLayouts to hold various standard gnome-shell UI
 * elements and add them to the chrome via {@link #addChrome}:
 *
 * * {@link #panelBox}, which holds the [top panel]{@link Panel.Panel}.
 * This has `affectsStruts` set to `true` (see {@link ChromeParameters});
 * * {@link #trayBox} which holds the [message tray]{@link MessageTray.MessageTray}.
 * This has `visibleInFullscreen` set to `true` but does not affect struts.
 * * {@link #keyboardBox} which holds the [on-screen keyboard]{@link Keyboard.Keyboard},
 * if the user has turned this on (it's an a11y setting).
 * This has `visibleInFullscreen` set to `true` but does not affect struts.
 *
 * @classdesc
 * The Layout Monitor manages laying out of the main UI elements of gnome-shell;
 * namely, the [top panel]{@link Panel.Panel},
 * [message tray]{@link MessageTray.MessageTray}, and
 * [on-screen keyboard]{@link Keyboard.Keyboard}.
 *
 * ![LayoutManager manages positioning of hot corners and UI elements; Chrome makes sure they receive events]{@link pics/layout.svg}
 *
 * There need only be one instance; it is stored in {@link Main.layoutManager}.
 *
 * It has members {@link #panelBox}, {@link #trayBox}, and {@link #keyboardBox}
 * that these items are placed in and it ensures that:
 *
 * * the top panel should always be placed on the top of the primary monitor;
 * * the keyboard box should always be on the bottom monitor;
 * * the message tray should always be on the bottom monitor.
 *
 * It also:
 *
 * * keeps track of monitor geometry ({@link #monitors}, {@link #primaryMonitor}
 * and {@link #bottomMonitor}) for other classes to use;
 * * ensures that each appropriate monitor has a [hot corner]{@link HotCorner}
 * for toggling the overview (the primary monitor has a hot corner in its
 * top panel [activities button]{@link Panel.ActivitiesButton}, and any
 * monitor with no monitor above or to the right of it (i.e. a monitor with
 * a "top-left corner") gets a hot corner);
 * * adds pointer barriers to the left and right edges of the top panel to
 * aid the user in trying to trigger the hot corner or click on the user menu
 * button (so it's harder to "overshoot" these targets when there are
 * multiple monitors);
 * * adds a pointer barrier to the bottom monitor, right corner, to make
 * it easier to trigger the message tray (the user doesn't have to worry
 * about the pointer overshooting to the next monitor).
 * * finally, the layout monitor has a {@link Chrome} that is used to ensure
 * that the stage input region (part of the global stage that should pass
 * events such as clicks to the actors on the stage) and the struts (e.g.
 * the top panel is a strut that stops windows being there) are up to date.
 *
 * @class
 */
function LayoutManager() {
    this._init.apply(this, arguments);
}

LayoutManager.prototype = {
    _init: function () {
        this._rtl = (St.Widget.get_default_direction() == St.TextDirection.RTL);
        /** Stores the geometry of all the monitors. Each element of the
         * array is an object with properties 'x', 'y', 'width' and 'height'.
         * @type {Meta.Rectangle[]}
         */
        this.monitors = [];
        /** The Meta.Rectangle (object with properties 'x', 'y', 'width', 'height')
         * corresponding to the primary monitor.
         * @see #primaryIndex
         * @see #bottomMonitor
         * @see #bottomIndex
         * @type {Meta.Rectangle} */
        this.primaryMonitor = null;
        /** The index of the primary monitor.
         * @see #primaryMonitor
         * @see #bottomMonitor
         * @see #bottomIndex
         * @type {number} */
        this.primaryIndex = -1;
        /** Holds any additional hot corners that toggle the overview other
         * than the one that is built in to the
         * [activities button]{@link Panel.ActivitiesButton} in the top panel.
         *
         * One per non-primary monitor with a top-left corner, i.e. with no monitor directly
         * above or to the right of it. Top-right corner for RTL text directions.
         * (The primary monitor already has a hot corner in the activities
         * button of its panel).
         * @type {Layout.HotCorner[]} */
        this._hotCorners = [];
        this._leftPanelBarrier = 0;
        this._rightPanelBarrier = 0;
        this._trayBarrier = 0;

        /** The {@link Chrome} for this layout manager.
         *
         * If the user wants to add an actor to the stage and additionally wants
         * it to be able to receive events (such as clicking), they must use
         * {@link #addChrome}.
         *
         * This will both add the actor to the stage and ensure that the
         * stage's input region is updated to always include this actor.
         * @see Chrome#addActor
         * @see Chrome
         * @type {Layout.Chrome}
         */
        this._chrome = new Chrome(this);

        /** This box holds the [top panel]{@link Panel.Panel}.
         * This has `affectsStruts` set to `true` (see {@link ChromeParameters}).
         * Its CSS name is 'panelBox'.
         * @type {St.BoxLayout}
         */
        this.panelBox = new St.BoxLayout({ name: 'panelBox',
                                           vertical: true });
        this.addChrome(this.panelBox, { affectsStruts: true });
        this.panelBox.connect('allocation-changed',
                              Lang.bind(this, this._updatePanelBarriers));

        /** This box holds the [message tray]{@link MessageTray.MessageTray}.
         * This has `visibleInFullscreen` set to `true` but does not affect struts.
         * Its CSS name is 'trayBox'.
         * @type {St.BoxLayout} */
        this.trayBox = new St.BoxLayout({ name: 'trayBox' }); 
        this.addChrome(this.trayBox, { visibleInFullscreen: true });
        this.trayBox.connect('allocation-changed',
                             Lang.bind(this, this._updateTrayBarrier));

        /** This holds the [on-screen keyboard]{@link Keyboard.Keyboard},
         * if the user has turned this on (it's an a11y setting).
         * This has `visibleInFullscreen` set to `true` but does not affect struts.
         * Its CSS name is 'keyboardBox'.
         * @type {St.BoxLayout} */
        this.keyboardBox = new St.BoxLayout({ name: 'keyboardBox',
                                              reactive: true,
                                              track_hover: true });
        this.addChrome(this.keyboardBox, { visibleInFullscreen: true });
        this._keyboardHeightNotifyId = 0;

        global.screen.connect('monitors-changed',
                              Lang.bind(this, this._monitorsChanged));
        this._monitorsChanged();
    },

    /** Initialises the layout manager, calling {@link Chrome#init} on
     * {@link #_chrome} and also {@link #_startupAnimation}.
     *
     * This is called by {@link Main.start} after everything else is constructed;
     * {@link Chrome#init} needs access to {@link Main.overview}, which didn't exist
     * yet when the LayoutManager was constructed.
     * @see Chrome#init
     * @see #_startupAnimation
     */
    init: function() {
        this._chrome.init();

        this._startupAnimation();
    },

    /** Updates {@link #monitors}, {@link #primaryMonitor},
     * {@link #bottomIndex} and {@link #bottomMonitor}
     * to reflect the user's monitors. */
    _updateMonitors: function() {
        let screen = global.screen;

        this.monitors = [];
        let nMonitors = screen.get_n_monitors();
        for (let i = 0; i < nMonitors; i++)
            this.monitors.push(screen.get_monitor_geometry(i));

        if (nMonitors == 1) {
            this.primaryIndex = this.bottomIndex = 0;
        } else {
            // If there are monitors below the primary, then we need
            // to split primary from bottom.
            this.primaryIndex = this.bottomIndex = screen.get_primary_monitor();
            for (let i = 0; i < this.monitors.length; i++) {
                let monitor = this.monitors[i];
                if (this._isAboveOrBelowPrimary(monitor)) {
                    if (monitor.y > this.monitors[this.bottomIndex].y)
                    /** The index of the bottom monitor.
                     * @see #bottomMonitor
                     * @see #primaryMonitor
                     * @see #primaryIndex
                     * @type {Meta.Rectangle} */
                        this.bottomIndex = i;
                }
            }
        }
        this.primaryMonitor = this.monitors[this.primaryIndex];
        /** The Meta.Rectangle (object with properties 'x', 'y', 'width', 'height')
         * corresponding to the bottom monitor.
         *
         * The "bottom" monitor is the one with the lowest y coordinate that
         * would still have some horizontal overlap with the primary monitor
         * were they side-by-side; in the case of multiple monitors with the
         * same lowest y coordinate that both line up vertically with the
         * primary monitor, the first is used.
         * @see #bottomIndex
         * @see #primaryMonitor
         * @see #primaryIndex
         * @type {Meta.Rectangle} */
        this.bottomMonitor = this.monitors[this.bottomIndex];
    },

    /** Creates additional top-left [hot corners]{@link HotCorner}
     * (that launches the overview) for each "top-left" monitor that isn't
     * the primary monitor,
     * i.e. monitors with no monitors directly above or to the left of them.
     *
     * The primary monitor doesn't need one because the primary monitor has
     * the top panel on it which has the
     * [activities button]{@link Panel.ActivitiesButton} which already
     * has a hot corner.
     *
     * They are all added to the chrome (as they are top-level actors and
     * should receive events).
     *
     * (top-right if the text direction is RTL).
     */
    _updateHotCorners: function() {
        // destroy old hot corners
        for (let i = 0; i < this._hotCorners.length; i++)
            this._hotCorners[i].destroy();
        this._hotCorners = [];

        // build new hot corners
        for (let i = 0; i < this.monitors.length; i++) {
            if (i == this.primaryIndex)
                continue;

            let monitor = this.monitors[i];
            let cornerX = this._rtl ? monitor.x + monitor.width : monitor.x;
            let cornerY = monitor.y;

            let haveTopLeftCorner = true;

            // Check if we have a top left (right for RTL) corner.
            // I.e. if there is no monitor directly above or to the left(right)
            let besideX = this._rtl ? monitor.x + 1 : cornerX - 1;
            let besideY = cornerY;
            let aboveX = cornerX;
            let aboveY = cornerY - 1;

            for (let j = 0; j < this.monitors.length; j++) {
                if (i == j)
                    continue;
                let otherMonitor = this.monitors[j];
                if (besideX >= otherMonitor.x &&
                    besideX < otherMonitor.x + otherMonitor.width &&
                    besideY >= otherMonitor.y &&
                    besideY < otherMonitor.y + otherMonitor.height) {
                    haveTopLeftCorner = false;
                    break;
                }
                if (aboveX >= otherMonitor.x &&
                    aboveX < otherMonitor.x + otherMonitor.width &&
                    aboveY >= otherMonitor.y &&
                    aboveY < otherMonitor.y + otherMonitor.height) {
                    haveTopLeftCorner = false;
                    break;
                }
            }

            if (!haveTopLeftCorner)
                continue;

            let corner = new HotCorner();
            this._hotCorners.push(corner);
            corner.actor.set_position(cornerX, cornerY);
            this._chrome.addActor(corner.actor);
        }
    },

    /** Updates the positions and sizes of {@link #panelBox},
     * {@link #keyboardBox} and {@link #trayBox}.
     *
     * * The panel box is placed at the top of the primary monitor, taking
     * up the entire width.
     * * The keyboard box is placed at the bottom of the bottom monitor, taking up the
     * entire width.
     * * The tray box is also placed at the bottom of the bottom monitor,
     * taking up the entire width.
     * Its clip is set to show things above it but not below it (so that it's not
     * visible behind the keyboard).
     */
    _updateBoxes: function() {
        this.panelBox.set_position(this.primaryMonitor.x, this.primaryMonitor.y);
        this.panelBox.set_size(this.primaryMonitor.width, -1);

        this.keyboardBox.set_position(this.bottomMonitor.x,
                                      this.bottomMonitor.y + this.bottomMonitor.height);
        this.keyboardBox.set_size(this.bottomMonitor.width, -1);

        this.trayBox.set_position(this.bottomMonitor.x,
                                  this.bottomMonitor.y + this.bottomMonitor.height);
        this.trayBox.set_size(this.bottomMonitor.width, -1);

        // Set trayBox's clip to show things above it, but not below
        // it (so it's not visible behind the keyboard). The exact
        // height of the clip doesn't matter, as long as it's taller
        // than any Notification.actor.
        this.trayBox.set_clip(0, -this.bottomMonitor.height,
                              this.bottomMonitor.width, this.bottomMonitor.height);
    },

    /** Updates the pointer barriers for the top panel
     * {@link #_leftPanelBarrier} and {@link #_rightPanelBarrier}.
     *
     * Pointer barriers stop the pointer from passing through them.
     *
     * We add one each to the left and right edges of the top panel to prevent
     * the user from passing horizontally out of either edge.
     *
     * This is so that if the user has a monitor to the side of the primary
     * one, the user *cannot* move their pointer out either side of the top
     * panel to the next monitor over.
     *
     * This is to make it easier to hit the hot corner in the top-left of the
     * top panel, and also to make it easier to move the cursor to any of the
     * buttons in the top panel (like the user button in the top-right).
     *
     * (This is to make the top panel nicer with respect to
     * [Fitt's law](http://en.wikipedia.org/wiki/Fitt's_Law)).
     *
     * The pointer barriers are XFixes pointer barriers.
     * @see [more information on pointer barriers](http://cgit.freedesktop.org/xorg/proto/fixesproto/tree/fixesproto.txt#n569)
     */
    _updatePanelBarriers: function() {
        if (this._leftPanelBarrier)
            global.destroy_pointer_barrier(this._leftPanelBarrier);
        if (this._rightPanelBarrier)
            global.destroy_pointer_barrier(this._rightPanelBarrier);

        if (this.panelBox.height) {
            let primary = this.primaryMonitor;
            this._leftPanelBarrier =
                global.create_pointer_barrier(primary.x, primary.y,
                                              primary.x, primary.y + this.panelBox.height,
                                              1 /* BarrierPositiveX */);
            this._rightPanelBarrier =
                global.create_pointer_barrier(primary.x + primary.width, primary.y,
                                              primary.x + primary.width, primary.y + this.panelBox.height,
                                              4 /* BarrierNegativeX */);
        } else {
            this._leftPanelBarrier = 0;
            this._rightPanelBarrier = 0;
        }
    },

    /** Updates the pointer barrier for the bottom message tray.
     *
     * Pointer barriers stop the pointer from passing through them.
     *
     * We add one to the right hand side of the message tray.
     *
     * This is so that if the user has a monitor to the right of the
     * bottom one (that the message tray is on) and they try to open
     * the message tray by hitting the bottom-right hot corner with their
     * cursor, they can't miss by sliding out the side of the message tray
     * and onto the next screen.
     *
     * Instead they can just quickly move their cursor to the bottom-right
     * corner of the primary monitor and know they don't have to slow down
     * towards the end to aim for the hot corner without overshooting.
     *
     * (This is to make the hot corner be nicer with respect to
     * [Fitt's law](http://en.wikipedia.org/wiki/Fitt's_Law)).
     *
     * The pointer barriers are XFixes pointer barriers.
     * @see [more information on pointer barriers](http://cgit.freedesktop.org/xorg/proto/fixesproto/tree/fixesproto.txt#n569)
     */
    _updateTrayBarrier: function() {
        let monitor = this.bottomMonitor;

        if (this._trayBarrier)
            global.destroy_pointer_barrier(this._trayBarrier);

        if (Main.messageTray) {
            this._trayBarrier =
                global.create_pointer_barrier(monitor.x + monitor.width, monitor.y + monitor.height - Main.messageTray.actor.height,
                                              monitor.x + monitor.width, monitor.y + monitor.height,
                                              4 /* BarrierNegativeX */);
        } else {
            this._trayBarrier = 0;
        }
    },

    /** Callback when the user's monitors change in any way.
     *
     * This:
     *
     * * updates our snapshot of monitor geometry ({@link #_updateMonitors});
     * * updates the location of the top panel, message tray, and keyboard
     * boxes ({@link #_updateBoxes});
     * * updates the locations of the hot corners ({@link #_updateHotCorners});
     * * emits a 'monitors-changed' signal (used by the {@link Chrome} to
     * update the stage input region and struts).
     * @fires .monitors-changed
     */
    _monitorsChanged: function() {
        this._updateMonitors();
        this._updateBoxes();
        this._updateHotCorners();

        this.emit('monitors-changed');
    },

    /** Checks whether `monitor` is above or below the primary monitor (that
     * is, it lines up vertically in some sense with the primary monitor).
     *
     * This means (for this function) that *if* `monitor` and the primary
     * monitor were placed at the *same y coordinate*, they would
     * **overlap horizontally** with a non-zero overlap.
     * (They don't have to *actually* overlap vertically at all).
     *
     * Basically this would return `false` if `monitor` were entirely to the
     * left or to the right of the primary monitor and `true` otherwise.
     *
     * I.e.:
     *
     * * `monitor`'s left coordinate is between the primary's left and right bounds; or
     * * `monitor`'s right coordinate is between the primary's left and right bounds; or
     * * the primary's left coordinate is between `monitor`'s left and right bounds; or
     * * the primary's right coordinate is between `monitor`'s left and right bounds;
     *
     * The second set of two conditions seem to be the same as the first set,
     * but it's just to ensure that the horizontal overlap is non-zero.
     * @param {Meta.Rectangle} monitor - object representing the monitor,
     * with properties 'x', 'y', 'width' and 'height'.
     * @returns {boolean} whether `monitor` is above or below the primary
     * monitor one.
     */
    _isAboveOrBelowPrimary: function(monitor) {
        let primary = this.monitors[this.primaryIndex];
        let monitorLeft = monitor.x, monitorRight = monitor.x + monitor.width;
        let primaryLeft = primary.x, primaryRight = primary.x + primary.width;

        if ((monitorLeft >= primaryLeft && monitorLeft < primaryRight) ||
            (monitorRight > primaryLeft && monitorRight <= primaryRight) ||
            (primaryLeft >= monitorLeft && primaryLeft < monitorRight) ||
            (primaryRight > monitorLeft && primaryRight <= monitorRight))
            return true;

        return false;
    },

    /** Gets the index of the monitor that the currently-focused window is on.
     *
     * This is determined by which monitor the *top-left corner* of the window
     * is on.
     *
     * If the top-left corner is not on any monitor, we return
     * the index of the primary monitor.
     * @type {number}
     * @see #focusMonitor
     */
    get focusIndex() {
        let focusWindow = global.display.focus_window;

        if (focusWindow) {
            let wrect = focusWindow.get_outer_rect();
            for (let i = 0; i < this.monitors.length; i++) {
                let monitor = this.monitors[i];

                if (monitor.x <= wrect.x && monitor.y <= wrect.y &&
                    monitor.x + monitor.width > wrect.x &&
                    monitor.y + monitor.height > wrect.y)
                    return i;
            }
        }

        return this.primaryIndex;
    },

    /** Gets the geometry of the monitor the currently-focused window
     * is on (determined by the monitor on which the window's top-left corner
     * is located, or the primary monitor otherwise).
     * @see #focusIndex
     * @type {Meta.Rectangle}
     */
    get focusMonitor() {
        return this.monitors[this.focusIndex];
    },

    /** Shows the top panel by sliding it down.
     * 
     * We freeze the chrome strut calculation while this is happening until
     * the animation is complete (since this animation is only done
     * upon {@link #init} once at startup).
     *
     * The animation takes place over time {@link STARTUP_ANIMATION_TIME}.
     * @see #_startupAnimationComplete
     * @see Chrome#freezeUpdateRegions
     */
    _startupAnimation: function() {
        // Don't animate the strut
        this._chrome.freezeUpdateRegions();

        this.panelBox.anchor_y = this.panelBox.height;
        Tweener.addTween(this.panelBox,
                         { anchor_y: 0,
                           time: STARTUP_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._startupAnimationComplete,
                           onCompleteScope: this
                         });
    },

    /** Called when {@link #_startupAnimation} (sliding the top panel
     * down) is complete.
     *
     * This unfreezes the freeze we placed on the chrome calculating
     * struts.
     * @see Chrome#thawUpdateRegions
     */
    _startupAnimationComplete: function() {
        this._chrome.thawUpdateRegions();
    },

    /** Shows the on-screen keyboard by sliding {@link #keyboardBox} up
     * vertically from the bottom of the screen.
     *
     * It also slides the message tray box {@link #trayBox} up to sit
     * on top of the keyboard box (while the on-screen keyboard is open,
     * the message tray should sit vertically above that rather than
     * still being in the bottom corner overlapping the keyboard).
     *
     * Both animations take {@link KEYBOARD_ANIMATION_TIME} seconds.
     *
     * Calls {@link #_showKeyboardComplete} when done.
     * @todo screenshot
     */
    showKeyboard: function () {
        Main.messageTray.hide();
        this.keyboardBox.raise_top();
        Tweener.addTween(this.keyboardBox,
                         { anchor_y: this.keyboardBox.height,
                           time: KEYBOARD_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._showKeyboardComplete,
                           onCompleteScope: this
                         });
        Tweener.addTween(this.trayBox,
                         { anchor_y: this.keyboardBox.height,
                           time: KEYBOARD_ANIMATION_TIME,
                           transition: 'easeOutQuad'
                         });
    },

    /** Called when we are finished sliding the keyboard up.
     *
     * We listen to changes in height of {@link #keyboardBox} and
     * make sure that the {@link #trayBox} and {@link #keyboardBox} are
     * positioned so that they are visible.
     *
     * @see #showKeyboard */
    _showKeyboardComplete: function() {
        // Poke Chrome to update the input shape; it doesn't notice
        // anchor point changes
        this._chrome.updateRegions();

        this._keyboardHeightNotifyId = this.keyboardBox.connect('notify::height', Lang.bind(this, function () {
            this.keyboardBox.anchor_y = this.keyboardBox.height;
            this.trayBox.anchor_y = this.keyboardBox.height;
        }));
    },

    /** Hides the on-screen keyboard by sliding {@link #keyboardBox} back down to the
     * bottom of the screen over time {@link KEYBOARD_ANIMATION_TIME} or instantly
     * if `immediate` is `true`.
     *
     * The message tray box {@link #trayBox} is also slidden down back to the bottom
     * of the screen.
     *
     * Upon completion {@link #_hideKeyboardComplete} is called to update
     * the chrome.
     * @param {boolean} immediate - hide the keyboard immediately, or
     * animate it over time {@link KEYBOARD_ANIMATION_TIME}?
     */
    hideKeyboard: function (immediate) {
        Main.messageTray.hide();
        if (this._keyboardHeightNotifyId) {
            this.keyboardBox.disconnect(this._keyboardHeightNotifyId);
            this._keyboardHeightNotifyId = 0;
        }
        Tweener.addTween(this.keyboardBox,
                         { anchor_y: 0,
                           time: immediate ? 0 : KEYBOARD_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._hideKeyboardComplete,
                           onCompleteScope: this
                         });
        Tweener.addTween(this.trayBox,
                         { anchor_y: 0,
                           time: immediate ? 0 : KEYBOARD_ANIMATION_TIME,
                           transition: 'easeOutQuad'
                         });
    },

    /** Callback on completion of the hiding the keyboard animation.
     * Updates the chrome {@link Chrome#updateRegions}.
     * @see Chrome#updateRegions
     * @see #hideKeyboard */
    _hideKeyboardComplete: function() {
        this._chrome.updateRegions();
    },

    /** Adds an actor to the chrome, meaning it should be added to the stage
     * and wants to receive events (like clicks) or affect the struts (like
     * the top panel).
     *
     * This just calls {@link Chrome#addActor} on {@link #_chrome} (if the
     * user wants to add an actor to the chrome they should use this method
     * since the layout manager is publicly accessible as `Main.layoutManager`
     * while the chrome isn't).
     * @inheritparams Chrome#addActor
     * @see Chrome#addActor
     */
    addChrome: function(actor, params) {
        this._chrome.addActor(actor, params);
    },

    /** Tells the chrome to track an actor that should be either affecting
     * the struts of the stage or receiving events.
     *
     * It should be the *descendant* of an actor already added via
     * {@link #addChrome} (if it isn't, then use {@link #addChrome}
     * instead of {@link #trackChrome}.
     *
     * This just calls {@link Chrome#trackActor} but the user should use
     * this method because the chrome is not publicly accessible whereas
     * the layout manager is ({@link Main.layoutManager}).
     * @inheritparams Chrome#trackActor
     * @see Chrome#trackActor
     */
    trackChrome: function(actor, params) {
        this._chrome.trackActor(actor, params);
    },

    /** Undoes the effect of {@link #trackChrome} meaning the actor will
     * no longer be taken into account when calculating the stage input
     * region or struts.
     * @inheritparams Chrome#untrackActor
     * @see Chrome#untrackActor
     */
    untrackChrome: function(actor) {
        this._chrome.untrackActor(actor);
    },

    /** Removes the actor from the chrome (undoes the effect of
     * {@link #addChrome}).
     * @inheritparams Chrome#removeActor
     * @see Chrome#removeActor
     */
    removeChrome: function(actor) {
        this._chrome.removeActor(actor);
    },

    /** Finds the monitor on which `actor`'s centre is located.
     * @inheritparams Chrome#findMonitorForActor
     * @see Chrome#findMonitorForActor
     */
    findMonitorForActor: function(actor) {
        return this._chrome.findMonitorForActor(actor);
    }
};
Signals.addSignalMethods(LayoutManager.prototype);
/** Emitted whenever the monitors change and the layout manager has finished
 * updating all its stored monitor geometry information, hot corners,
 * and position of the top panel/message tray/on-screen keyboard boxes.
 * @name monitors-changed
 * @event
 * @memberof LayoutManager
 */


/** creates a new HotCorner
 *
 * We create the top-level actor {@link #actor}, a 3x3 reactive clutter group
 * with style name 'hot-corner-environs'.
 *
 * Inside this we place the corner actor {@link #_corner}, a 1x1
 * transparent reactive clutter rectangle. This is placed in the top-left
 * of {@link #actor} if the text direction is left-to-right and the top-right
 * otherwise, and the gravity set accordingly (NW vs NE).
 *
 * We also create the three ripples that will form the ripple animation that
 * plays when the hot corner is triggered, stored in `this._ripple1`,
 * `this._ripple2`, and `this._ripple3`. These are St.BoxLayouts with
 * style class 'ripple-box' which essentially gives them one of the
 * following two pictures:
 *
 * ![ripple, right-to-left](pics/corner-ripple-rtl.png)
 * ![ripple, left-to-right](pics/corner-ripple-ltr.png)
 *
 * We also connect up these events which may trigger the hot corner:
 *
 * * clicking on the corner or environs;
 * * entering the hot corner;
 * * leaving the hot corner or environs.
 *
 * The reason for the larger environs that contains the corner is (according
 * to the code) to avoid triggering the hot corner multiple times
 * due to an accidental jitter (as it is quite small). (But why couldn't
 * the corner just be made bigger then? It's transparent anyway...).
 *
 * I think this means that if I trigger the hot corner by moving my
 * cursor over the 1x1 hot corner and then accidentally jitter the mouse one
 * pixel away and back, I don't want to trigger the hot corner the second
 * time (?).
 * @classdesc
 * This class manages a "hot corner" that can toggle switching to
 * overview.
 *
 * One of these is in the [Activities Button]{@link Panel.ActivitiesButton}
 * in the top panel, and the {@link LayoutMonitor} creates one more for each
 * monitor with a top-left corner (no monitor directly above or to the left
 * of it).
 *
 * If the text direction is right-to-left, it's placed in the top-right corner
 * instead.
 *
 * When the corner is triggered it has a "ripple" animation, consisting
 * of three ripplse emanating outwards from the corner.
 *
 * ![Hot corner's ripple animation](pics/HotCornerRipple.gif)
 *
 * Note - the hot corner for the message tray is not one of these; it is
 * [Main.messageTray._corner]{@link MessageTray#_corner} and has no ripple
 * animation.
 *
 * This class is really for the convenience of the animation and specificly
 * for toggling the Overview.
 * @class
 */
function HotCorner() {
    this._init();
}

HotCorner.prototype = {
    _init : function() {
        // We use this flag to mark the case where the user has entered the
        // hot corner and has not left both the hot corner and a surrounding
        // guard area (the "environs"). This avoids triggering the hot corner
        // multiple times due to an accidental jitter.
        this._entered = false;

        /** Top-level actor for the hot corner. Style name 'hot-corner-environs',
         * 3x3 reactive rectangle.
         *
         * The corner itself {@link #_corner} is 1x1 and lives inside the
         * environs.
         * @type {Clutter.Group} */
        this.actor = new Clutter.Group({ name: 'hot-corner-environs',
                                         width: 3,
                                         height: 3,
                                         reactive: true });

        /** The hot corner. 1x1 transparent reactive rectangle, style name
         * 'hot-corner'.
         *
         * It lives within {@link #actor} which is 3x3.
         * @type {Clutter.Rectangle} */
        this._corner = new Clutter.Rectangle({ name: 'hot-corner',
                                               width: 1,
                                               height: 1,
                                               opacity: 0,
                                               reactive: true });
        this._corner._delegate = this;

        this.actor.add_actor(this._corner);

        if (St.Widget.get_default_direction() == St.TextDirection.RTL) {
            this._corner.set_position(this.actor.width - this._corner.width, 0);
            this.actor.set_anchor_point_from_gravity(Clutter.Gravity.NORTH_EAST);
        } else {
            this._corner.set_position(0, 0);
        }

        this._activationTime = 0;

        this.actor.connect('leave-event',
                           Lang.bind(this, this._onEnvironsLeft));

        // Clicking on the hot corner environs should result in the
        // same behavior as clicking on the hot corner.
        this.actor.connect('button-release-event',
                           Lang.bind(this, this._onCornerClicked));

        // In addition to being triggered by the mouse enter event,
        // the hot corner can be triggered by clicking on it. This is
        // useful if the user wants to undo the effect of triggering
        // the hot corner once in the hot corner.
        this._corner.connect('enter-event',
                             Lang.bind(this, this._onCornerEntered));
        this._corner.connect('button-release-event',
                             Lang.bind(this, this._onCornerClicked));
        this._corner.connect('leave-event',
                             Lang.bind(this, this._onCornerLeft));

        // Cache the three ripples instead of dynamically creating and destroying them.
        this._ripple1 = new St.BoxLayout({ style_class: 'ripple-box', opacity: 0 });
        this._ripple2 = new St.BoxLayout({ style_class: 'ripple-box', opacity: 0 });
        this._ripple3 = new St.BoxLayout({ style_class: 'ripple-box', opacity: 0 });

        Main.uiGroup.add_actor(this._ripple1);
        Main.uiGroup.add_actor(this._ripple2);
        Main.uiGroup.add_actor(this._ripple3);
    },

    /** Destroys this hot corner. */
    destroy: function() {
        this.actor.destroy();
    },

    /** This animates a single ripple coming out from the hot corner and expanding
     * outwards. The ripple animation for the hot corners consists of three
     * ripples; {@link #rippleAnimation} does that.
     *
     * @param {St.BoxLayout} ripple - the ripple to animate (it's a St.BoxLayout
     * with style class 'ripple-box', picture ![ripple, ltr](pics/corner-ripple-ltr.png)
     * or ![ripple, rtl](pics/corner-ripple-rtl.png))
     * @param {number} delay - the time (seconds) after which the animation should start
     * @param {number} time - the time (seconds) the animation should take, not
     * including the delay.
     * @param {number} startScale - the scale that the ripple should start at
     * (usually it starts at a small scale and grows to a larger one).
     * @param {number} startOpacity - the opacity the ripple should start
     * at (it will fade to 0 opacity)
     * @param {number} finalScale - the scale at which the ripple should end.
     * @see #rippleAnimation
     */
    _animRipple : function(ripple, delay, time, startScale, startOpacity, finalScale) {
        // We draw a ripple by using a source image and animating it scaling
        // outwards and fading away. We want the ripples to move linearly
        // or it looks unrealistic, but if the opacity of the ripple goes
        // linearly to zero it fades away too quickly, so we use Tweener's
        // 'onUpdate' to give a non-linear curve to the fade-away and make
        // it more visible in the middle section.

        ripple._opacity = startOpacity;

        if (ripple.get_direction() == St.TextDirection.RTL)
            ripple.set_anchor_point_from_gravity(Clutter.Gravity.NORTH_EAST);

        ripple.visible = true;
        ripple.opacity = 255 * Math.sqrt(startOpacity);
        ripple.scale_x = ripple.scale_y = startScale;

        let [x, y] = this._corner.get_transformed_position();
        ripple.x = x;
        ripple.y = y;

        Tweener.addTween(ripple, { _opacity: 0,
                                   scale_x: finalScale,
                                   scale_y: finalScale,
                                   delay: delay,
                                   time: time,
                                   transition: 'linear',
                                   onUpdate: function() { ripple.opacity = 255 * Math.sqrt(ripple._opacity); },
                                   onComplete: function() { ripple.visible = false; } });
    },

    /** This plays the "ripple" animation that happens when you trigger a hot corner.
     *
     * ![Hot corner's ripple animation](pics/HotCornerRipple.gif)
     *
     * Show three concentric ripples expanding outwards; the exact
     * parameters were found by trial and error, so don't look
     * for them to make perfect sense mathematically.
     *
     * Parameters for the ripples (delay, time, scale, opacity, final scale):
     *
     * * `this._ripple1`: 0s delay, 0.83s animation, scale from 0.25 to 1.5, start opacity 1.
     * * `this._ripple2`: 0.05s delay, 1s animation, scale from 0 to 1.25, start opacity 0.7.
     * * `this._ripple3`: 0.35s delay, 1s animation, scale from 0.3 to 1, start opacity 0.3.
     * @see #_animRipple
     * @todo picture
     */
    rippleAnimation: function() {

        //                              delay  time  scale opacity => scale
        this._animRipple(this._ripple1, 0.0,   0.83,  0.25,  1.0,     1.5);
        this._animRipple(this._ripple2, 0.05,  1.0,   0.0,   0.7,     1.25);
        this._animRipple(this._ripple3, 0.35,  1.0,   0.0,   0.3,     1);
    },

    /** This function is part of the drag and drop interface, specifying what
     * the hot corner should do if something is dragged over it.
     *
     * If the item being dragged comes from Xdnd (so for example a file
     * dragged from nautilus) and it is dragged over the hot corner,
     * we:
     *
     * 1. play the ripple animation {@link #rippleAnimation};
     * 2. temporarily show the overview {@link Overview.Overview#showTemporarily};
     * 3. call {@link Overview.Overview#beginItemDrag} to start the drag.
     *
     * This allows the user to do something like drag a file from nautilus
     * over the hot corner to launch the overview, and hover it over
     * a window there to activate that window such that it can be dropped there.
     *
     * @todo mark as part of DND and inheritparams
     * @see rippleAnimation
     */
    handleDragOver: function(source, actor, x, y, time) {
        if (source != Main.xdndHandler)
            return;

        if (!Main.overview.visible && !Main.overview.animationInProgress) {
            this.rippleAnimation();
            Main.overview.showTemporarily();
            Main.overview.beginItemDrag(actor);
        }
    },

    /** Callback when the user's pointer enters the hot corner.
     *
     * This plays the ripple animation and toggles the overview,
     * **unless** the pointer has not left the environs {@link #actor} since
     * last leaving the corner {@link #_corner}.
     *
     * This is to prevent accidental jitter of the cursor triggering the
     * hot corner multiple times - if the user triggers the hot corner,
     * they must move their cursor all the way out of the environs {@link #actor}
     * before the overview can be triggered by moving into {@link #_corner}
     * again.
     * @see rippleAnimation
     */
    _onCornerEntered : function() {
        if (!this._entered) {
            this._entered = true;
            if (!Main.overview.animationInProgress) {
                this._activationTime = Date.now() / 1000;

                this.rippleAnimation();
                Main.overview.toggle();
            }
        }
        return false;
    },

    /** Callback when the hot corner or environs are clicked.
     *
     * If {@link #shouldToggleOverviewOnClick} is true (that is it has been
     * at least {@link HOT_CORNER_ACTIVATION_TIMEOUT} since we last triggered
     * the hot corner) we toggle the overview.
     */
    _onCornerClicked : function() {
        if (this.shouldToggleOverviewOnClick())
            Main.overview.toggle();
        return true;
    },

    /** Callback when the cursor leaves the hot corner {@link #_corner}.
     *
     * We make a note of whether the cursor has left the hot corner
     * {@link #_corner} but is still in the environs {@link #actor}.
     * (If the pointer enters the cursor again but hasn't yet left the
     * environs, the corner will not trigger - this is to avoid accidental
     * jitters from triggering the hot corner multiple times).
     *
     */
    _onCornerLeft : function(actor, event) {
        if (event.get_related() != this.actor)
            this._entered = false;
        // Consume event, otherwise this will confuse onEnvironsLeft
        return true;
    },

    /** Callback when the cursor leaves the hot corner environs {@link #actor}.
     *
     * If the cursor has left {@link #actor} and hasn't entered {@link #_corner}
     * we unlock the hot corner so that when the cursor enters the hot corner
     * next time, it will trigger it.
     */
    _onEnvironsLeft : function(actor, event) {
        if (event.get_related() != this._corner)
            this._entered = false;
        return false;
    },

    /** Checks if the Activities button is currently sensitive to
     * clicks. The first call to this function within the
     * {@link HOT_CORNER_ACTIVATION_TIMEOUT} of the hot corner being
     * triggered will return false. This avoids opening and closing
     * the overview if the user both triggered the hot corner and
     * clicked the Activities button.
     * @returns {boolean} false if the overview is currently animating or
     * this is the first call within {@link HOT_CORNER_ACTIVATION_TIMEOUT}
     * of the hot corner triggered; true otherwise.
     */
    shouldToggleOverviewOnClick: function() {
        if (Main.overview.animationInProgress)
            return false;
        if (this._activationTime == 0 || Date.now() / 1000 - this._activationTime > HOT_CORNER_ACTIVATION_TIMEOUT)
            return true;
        return false;
    }
};


// This manages the shell "chrome"; the UI that's visible in the
// normal mode (ie, outside the Overview), that surrounds the main
// workspace content.
/** Default parameters to add an actor to the chrome.
 * @typedef ChromeParameters
 * @see Chrome#addActor
 * @see LayoutManager#addChrome
 * @type {Object}
 * @property {boolean} [visibleInFullscreen=false] - should this actor be visible
 * even when a fullscreen window is covering it (for example the message tray
 * and on-screen keyboard are)?
 * @property {boolean} [affectsStruts=false] - if `true` (and the actor
 * added is along a screen edge), then the actor's size and position will
 * also affect the window manager struts. Changes to `actor`'s visibility
 * will NOT affect whether or not the strut is present, however. The top
 * panel is an example of an actor that affects struts.
 * @property {boolean} [affectsInputRegion=true] - if true, the input
 * region of the global stage is extended to include this actor. That means
 * that this actor should be able to receive user events like clicks. Changes
 * in the actor's size, position, and visibility will automatically
 * result in appropriate changes to the input region. I can't find any
 * in-built gnome-shell objects that do *not* affect the input region (why
 * would you add it to the chrome if you didn't want it to receive any events
 * anyway? Then you could just add it to the stage...)
 */
const defaultParams = {
    visibleInFullscreen: false,
    affectsStruts: false,
    affectsInputRegion: true
};

/** creates a new Chrome.
 *
 * We store `layoutManager` locally, connect to changes in monitors or
 * windows (in order to detect whether particular monitors are fullscreen),
 * connect to the overview show/hidden state and screensaver state, all of
 * these to ensure that the stage input region remains correct and up to date.
 * @param {Layout.LayoutManager} layoutManager - the layout manager that owns
 * this chrome (we store it just to use its list of monitors).
 * @classdesc
 * The shell "chrome" is the UI that's visible in the
 * normal mode (ie, outside the Overview), that surrounds the main
 * workspace content.
 *
 * Basically the purpose of this class is to keep an up-to-date snapshot
 * of:
 *
 * * the input region for the stage; and
 * * the struts for the screen/workspace.
 *
 * Hence it is not a visual class in any way (you can't *see* it); but it
 * is very crucial for making sure windows and so on know what space is available
 * to them.
 *
 * To explain what this class done, first we must explain what it means to
 * set the input mode of the global stage. Much of the following comes
 * from `shell-global.c` (the documentation for function `shell_global_set_stage_input_mode`,
 * or `global.set_stage_input_mode`).
 *
 * When the stage's mode is set to Shell.StageInputMode.NONREACTIVE, the stage
 * doesn't absorb *any* clicks, but passes through to the underlying windows.
 *
 * When it is set to Shell.StageInputMode.NORMAL (the usual), the stage accepts
 * clicks in its "input region" but passes through all other clicks to the
 * underlying windows.
 *
 * (There are a couple of other modes but not relevant to this explanation).
 *
 * So what this "input region" means is that for *any* actor (St or Clutter)
 * added to the global stage, for it to receive *any* events *at all* (clicks,
 * button presses, ...), it must be added to the stage's input region.
 *
 * If the actor is added to the stage but its location/size is not added to the
 * stage's input region, it won't receive any events at all.
 *
 * ![Chrome makes sure the top panel, keyboard box, message tray, and hot corners
 * receive to events, and manages the struts]{@link pics/layout.svg}
 *
 * One component of what this class does is maintain an up-to-date snapshot
 * of the stage's input region (i.e. locations that should allow clicks to
 * be passed to the actors there).
 *
 * To ensure that a particular actor is included in the input region, you
 * add it with {@link #addActor} or {@link #trackActor} (well, the end-user
 * will add it with [Main.layoutManager.addChrome]{@link LayoutManager#addChrome}).
 *
 * This class then tracks changes in size, position or visibility of the
 * actors it is told to track, and makes sure the stage's input region reflects
 * those actors.
 *
 * The other component that the Chrome manages are **struts**.
 * Struts affect the area of a workspace available to windows. For example,
 * the top panel in gnome-shell is interactive so is part of the stage's
 * input region but windows should treat it as space that they are not allowed
 * to use, so the top panel's area should be included in the struts for all
 * workspaces.
 *
 * The user does not use the chrome directly (it is stored as
 * [`Main.layoutManager._chrome`]{@link LayoutManager#_chrome}); they add
 * actors to the layout manager with {@link LayoutManager#addChrome} which will
 * add them to the chrome.
 *
 * When adding actors to the layout manager/chrome a number of parameters can
 * be given specifying whether the actor should receive events, whether it
 * should receive events in fullscreen, and whether struts should be added
 * to reserve space for it; see {@link Layout.ChromeParameters} for further
 * information.
 *
 * There need only be one instance of the Chrome; it is stored as
 * `Main.layoutManager._chrome`.
 * @class
 */
function Chrome() {
    this._init.apply(this, arguments);
}

Chrome.prototype = {
    _init: function(layoutManager) {
        this._layoutManager = layoutManager;

        this._monitors = [];
        this._inOverview = false;
        this._updateRegionIdle = 0;
        this._freezeUpdateCount = 0;

        this._trackedActors = [];

        this._layoutManager.connect('monitors-changed',
                                    Lang.bind(this, this._relayout));
        global.screen.connect('restacked',
                              Lang.bind(this, this._windowsRestacked));

        // Need to update struts on new workspaces when they are added
        global.screen.connect('notify::n-workspaces',
                              Lang.bind(this, this._queueUpdateRegions));

        this._screenSaverActive = false;
        this._screenSaverProxy = new ScreenSaver.ScreenSaverProxy();
        this._screenSaverProxy.connect('ActiveChanged', Lang.bind(this, this._onScreenSaverActiveChanged));
        this._screenSaverProxy.GetActiveRemote(Lang.bind(this,
            function(result, err) {
                if (!err)
                    this._onScreenSaverActiveChanged(this._screenSaverProxy, result);
            }));

        this._relayout();
    },

    /** Initialises the chrome.
     * Performed after the Overview has been created - connects `Main.overview`'s
     * 'showing' and 'hidden' signals to {@link #_overviewShowing} and
     * {@link #_overviewHidden}.
     */
    init: function() {
        Main.overview.connect('showing',
                             Lang.bind(this, this._overviewShowing));
        Main.overview.connect('hidden',
                             Lang.bind(this, this._overviewHidden));
    },

    /** Adds an actor to the chrome.
     * This adds it to {@link Main.uiGroup} and tracks it via
     * {@link #_trackActor}.
     *
     * To add an actor to the chrome that is a descendant of an actor
     * already in the chrome (hence it is already added to
     * {@link Main.uiGroup}), you can use {@link #trackActor}. This uses
     * the `params` for the ancestor (unless overridden in the `params`
     * argument of that function).
     * @param {Clutter.Actor} actor - actor.
     * @param {Layout.ChromeParameters} [params] - parameters to add the actor
     * to the chrome with - 'visibleInFullscreen' (default false),
     * 'affectsStruts' (default false) and 'affectsInputRegion' (default true).
     * See {@link ChromeParameters} for further details.
     * @see #_trackActor
     * @see #trackActor
     * @see #removeActor
     */
    addActor: function(actor, params) {
        Main.uiGroup.add_actor(actor);
        this._trackActor(actor, params);
    },

    /** Tracks an actor that is the descendent of an already-tracked
     * chrome actor (i.e. an actor added with {@link #addActor}), i.e.
     * it is already in {@link Main.uiGroup}.
     *
     * Calls {@link #_trackActor} with the same parameters as `actor`'s
     * already-tracked ancestor (overidden with values from `params` if
     * supplied).
     *
     * If you're adding a new actor to the chrome (i.e. any of its ancestors
     * have not yet been added), use {@link #addActor} instead.
     *
     * Reverse with {@link #untrackActor}.
     * @inheritparams #addActor
     * @see #_trackActor
     * @see #untrackActor
     * @see #addActor
     */
    trackActor: function(actor, params) {
        let ancestor = actor.get_parent();
        let index = this._findActor(ancestor);
        while (ancestor && index == -1) {
            ancestor = ancestor.get_parent();
            index = this._findActor(ancestor);
        }
        if (!ancestor)
            throw new Error('actor is not a descendent of a chrome actor');

        let ancestorData = this._trackedActors[index];
        if (!params)
            params = {};
        // We can't use Params.parse here because we want to drop
        // the extra values like ancestorData.actor
        for (let prop in defaultParams) {
            if (!params.hasOwnProperty(prop))
                params[prop] = ancestorData[prop];
        }

        this._trackActor(actor, params);
    },

    /** Untracks an actor (doesn't remove it from
     * {@link Main.uiGruop}).
     * Calls {@link #_untrackActor}.
     * @inheritparams #addActor
     * @see #trackActor
     * @see #_untrackActor
     */
    untrackActor: function(actor) {
        this._untrackActor(actor);
    },

    /** Removes an actor from {@link Main.uiGroup} and also untracks it.
     * @inheritparams #addActor
     */
    removeActor: function(actor) {
        Main.uiGroup.remove_actor(actor);
        this._untrackActor(actor);
    },

    /** Finds the index of an actor in our tracked actors, any actor added via
     * {@link #addActor} or {@link #trackActor}.
     * @inheritparams #addActor
     * @returns {number} the index of `actor` in our list of tracked actors,
     * or -1 if not found.
     */
    _findActor: function(actor) {
        for (let i = 0; i < this._trackedActors.length; i++) {
            let actorData = this._trackedActors[i];
            if (actorData.actor == actor)
                return i;
        }
        return -1;
    },

    /** Internal method that tracks a chrome actor.
     *
     * We:
     *
     * * store whether the actor is a top-level one (i.e. parent is
     * {@link Main.uiGroup});
     * * track the visibility of the actor;
     * * track size changes to the actor;
     * * track changes in the parent of the actor (which also tracks when
     * an actor is destroyed).
     *
     * We then add it to our list of tracked actors and call {@link #_queueUpdateRegions}.
     * @todo what does that do
     * @see #_queueUpdateRegions
     * @inheritparams #addChrome
     */
    _trackActor: function(actor, params) {
        if (this._findActor(actor) != -1)
            throw new Error('trying to re-track existing chrome actor');

        let actorData = Params.parse(params, defaultParams);
        actorData.actor = actor;
        actorData.isToplevel = actor.get_parent() == Main.uiGroup;
        actorData.visibleId = actor.connect('notify::visible',
                                            Lang.bind(this, this._queueUpdateRegions));
        actorData.allocationId = actor.connect('notify::allocation',
                                               Lang.bind(this, this._queueUpdateRegions));
        actorData.parentSetId = actor.connect('parent-set',
                                              Lang.bind(this, this._actorReparented));
        // Note that destroying actor will unset its parent, so we don't
        // need to connect to 'destroy' too.

        this._trackedActors.push(actorData);
        this._queueUpdateRegions();
    },

    /** Internal method to untrack an actor.
     *
     * We disconnect any signals we were listening to (connected in
     * {@link #_trackActor}), remove it from our list of tracked actors,
     * and call {@link #_queueUpdateRegions}
     * @todo what does that do?
     * @inheritparams #addActor
     * @see #_queueUpdateRegions
     */
    _untrackActor: function(actor) {
        let i = this._findActor(actor);

        if (i == -1)
            return;
        let actorData = this._trackedActors[i];

        this._trackedActors.splice(i, 1);
        actor.disconnect(actorData.visibleId);
        actor.disconnect(actorData.allocationId);
        actor.disconnect(actorData.parentSetId);

        this._queueUpdateRegions();
    },

    /** Callback when an actor we are tracking has its parent changed.
     *
     * If `actor` has been unparented or destroyed (i.e. no new parent)
     * we untrack it.
     *
     * Otherwise we just refresh whether the actor is top-level (i.e.
     * the parent is {@link Main.uiGroup}).
     * @inheritparams #addActor
     * @param {?Clutter.Actor} oldParent - the old parent of the actor
     */
    _actorReparented: function(actor, oldParent) {
        let newParent = actor.get_parent();
        if (!newParent)
            this._untrackActor(actor);
        else
            actorData.isToplevel = (newParent == Main.uiGroup);
    },

    /** Checks whether each of our tracked actors should be visible.
     *
     * For example, if the screensaver is active none should be visible;
     * if we're in fullscreen and the actor had `visibleInFullscreen` set
     * to `false` then we shouldn't be visible and so on.
     *
     * We then update each actor's visibility according to what it should be
     * (well, we use `set_skip_paint`).
     */
    _updateVisibility: function() {
        for (let i = 0; i < this._trackedActors.length; i++) {
            let actorData = this._trackedActors[i], visible;
            if (!actorData.isToplevel)
                continue;

            if (this._screenSaverActive)
                visible = false;
            else if (this._inOverview)
                visible = true;
            else if (!actorData.visibleInFullscreen &&
                     this.findMonitorForActor(actorData.actor).inFullscreen)
                visible = false;
            else
                visible = true;
            Main.uiGroup.set_skip_paint(actorData.actor, !visible);
        }
    },

    /** Callback when the overview is showing.
     *
     * Calls {@link #_updateVisibility} to see which of our tracked actors
     * is visible and {@link #_queueUpdateRegions} to refresh the stage
     * input region/struts.
     * @see Overview.event:showing
     */
    _overviewShowing: function() {
        this._inOverview = true;
        this._updateVisibility();
        this._queueUpdateRegions();
    },

    /** Callback when the overview is hiding.
     *
     * Calls {@link #_updateVisibility} to see which of our tracked actors
     * is visible and {@link #_queueUpdateRegions} to refresh the stage
     * input region/struts.
     * @see Overview.event:hiding
     */
    _overviewHidden: function() {
        this._inOverview = false;
        this._updateVisibility();
        this._queueUpdateRegions();
    },

    /** Callback whenever the monitors change (added or removed, or
     * change order or geometry). In response to the layout monitor's
     * ['monitors-changed']{@link LayoutMonitor.monitors-changed} signal.
     *
     * This updates our internal list of monitors, re-does the fullscreen
     * and visibility checks ({@link #_updateFullscreen},
     * {@link #_updateVisibility}) and queues an update to the stage
     * input region/struts.
     */
    _relayout: function() {
        this._monitors = this._layoutManager.monitors;
        this._primaryMonitor = this._layoutManager.primaryMonitor;

        this._updateFullscreen();
        this._updateVisibility();
        this._queueUpdateRegions();
    },

    /** Callback when the screensaver is activated.
     *
     * This sees what actors are visible ({@link #_updateVisibility}) and
     * queues an update of our regions ({@link #_queueUpdateRegions}).
     * @see #updateRegions
     */
    _onScreenSaverActiveChanged: function(proxy, screenSaverActive) {
        this._screenSaverActive = screenSaverActive;
        this._updateVisibility();
        this._queueUpdateRegions();
    },

    /** Finds the monitor that the given rectangle is on.
     *
     * This means:
     *
     * * find the monitor that the rectangle's *centre* is on;
     * * if the centre is not on any monitor, return the first monitor that
     * overlaps the rectangle;
     * * if all else fails, return `null`.
     * @param {number} x - the x coordinate of the rectangle (top-left corner).
     * @param {number} y - the y coordinate of the rectangle (top-left corner).
     * @param {number} w - the width of the rectangle.
     * @param {number} h - the height of the rectangle.
     * @returns {Meta.Rectangle} an object with properties 'x', 'y', 'width',
     * 'height' describing the monitor on which the rectangle (actor, window)
     * is located.
     */
    _findMonitorForRect: function(x, y, w, h) {
        // First look at what monitor the center of the rectangle is at
        let cx = x + w/2;
        let cy = y + h/2;
        for (let i = 0; i < this._monitors.length; i++) {
            let monitor = this._monitors[i];
            if (cx >= monitor.x && cx < monitor.x + monitor.width &&
                cy >= monitor.y && cy < monitor.y + monitor.height)
                return monitor;
        }
        // If the center is not on a monitor, return the first overlapping monitor
        for (let i = 0; i < this._monitors.length; i++) {
            let monitor = this._monitors[i];
            if (x + w > monitor.x && x < monitor.x + monitor.width &&
                y + h > monitor.y && y < monitor.y + monitor.height)
                return monitor;
        }
        // otherwise on no monitor
        return null;
    },

    /** Finds the monitor a window is on, using the same methodology
     * as {@link #_findMonitorForRect}. That is, we return the monitor that
     * the window's *centre* is on, or the first overlapping monitor if the
     * centre is not on a monitor, or finally `null` if we still couldn't
     * find it.
     * @see #_findMonitorForRect
     * @inheritparams #_findMonitorForRect
     * @param {Meta.WindowActor} window - window to find the monitor for (really
     * any object with properties 'x', 'y', 'width' and 'height' would work).
     */
    _findMonitorForWindow: function(window) {
        return this._findMonitorForRect(window.x, window.y, window.width, window.height);
    },

    // This call guarantees that we return some monitor to simplify usage of it
    // In practice all tracked actors should be visible on some monitor anyway
    /** Finds the monitor that an actor is on (or the primary monitor if
     * we couldn't find it).
     *
     * An actor is "on" a monitor if its centre is on that monitor.
     * If the centre is not on any monitor we return the first monitor that
     * overlaps `actor`. (If all of these fail we just return the primary
     * monitor to simplify its usage).
     * @inheritparams #addActor
     * @inheritparams #_findMonitorForRect
     * @see #_findMonitorForRect
     */
    findMonitorForActor: function(actor) {
        let [x, y] = actor.get_transformed_position();
        let [w, h] = actor.get_transformed_size();
        let monitor = this._findMonitorForRect(x, y, w, h);
        if (monitor)
            return monitor;
        return this._primaryMonitor; // Not on any monitor, pretend its on the primary
    },

    /** Queues an update of our regions {@link #updateRegions}
     * (if this has not been frozen with {@link #freezeUpdateRegions}.
     *
     * This will ensure that the stage's struts and input regions are up-to-date.
     *
     * The reason you would queue an update rather than just do the update
     * immediately is that there is often a slight delay between an actor
     * emitting a 'notify::property_name' signal and `actor.property_name`
     * becoming in sync with that, so we wait a bit before querying the properties
     * to ensure the input region/struts are set according to the updated
     * values.
     * @see #updateRegions
     */
    _queueUpdateRegions: function() {
        if (!this._updateRegionIdle && !this._freezeUpdateCount)
            this._updateRegionIdle = Mainloop.idle_add(Lang.bind(this, this.updateRegions),
                                                       Meta.PRIORITY_BEFORE_REDRAW);
    },

    /** Freezes updating of the regions until an equal number of
     * {@link #thawUpdateRegions} has been called. */
    freezeUpdateRegions: function() {
        if (this._updateRegionIdle)
            this.updateRegions();
        this._freezeUpdateCount++;
    },

    /** Decreases the freeze count on the updating of the regions (when
     * the number of calls to {@link #thawUpdateRegions} equals that of
     * {@link #freezeUpdateRegions} then we will update our regions).
     * @see #freezeUpdateRegions
     */
    thawUpdateRegions: function() {
        this._freezeUpdateCount--;
        this._queueUpdateRegions();
    },

    /** Checks whether any of our monitors is in fullscreen and stores it
     * for use in {@link #updateRegions}.
     *
     * Tracked actors with `visibleInFullscreen` false should be visible
     * unless there is a window with layer FULLSCREEN, 
     * or a window with layer OVERRIDE_REDIRECT that covers the whole screen.
     */
    _updateFullscreen: function() {
        let windows = Main.getWindowActorsForWorkspace(global.screen.get_active_workspace_index());

        // Reset all monitors to not fullscreen
        for (let i = 0; i < this._monitors.length; i++)
            this._monitors[i].inFullscreen = false;

        // Ordinary chrome should be visible unless there is a window
        // with layer FULLSCREEN, or a window with layer
        // OVERRIDE_REDIRECT that covers the whole screen.
        // ('override_redirect' is not actually a layer above all
        // other windows, but this seems to be how mutter treats it
        // currently...) If we wanted to be extra clever, we could
        // figure out when an OVERRIDE_REDIRECT window was trying to
        // partially overlap us, and then adjust the input region and
        // our clip region accordingly...

        // @windows is sorted bottom to top.

        for (let i = windows.length - 1; i > -1; i--) {
            let window = windows[i];
            let layer = window.get_meta_window().get_layer();

            // Skip minimized windows
            if (!window.showing_on_its_workspace())
                continue;

            if (layer == Meta.StackLayer.FULLSCREEN) {
                let monitor = this._findMonitorForWindow(window);
                if (monitor)
                    monitor.inFullscreen = true;
            }
            if (layer == Meta.StackLayer.OVERRIDE_REDIRECT) {
                // Check whether the window is screen sized
                let isScreenSized =
                    (window.x == 0 && window.y == 0 &&
                    window.width == global.screen_width &&
                    window.height == global.screen_height);

                if (isScreenSized) {
                    for (let i = 0; i < this._monitors.length; i++)
                        this._monitors[i].inFullscreen = true;
                }

                // Or whether it is monitor sized
                let monitor = this._findMonitorForWindow(window);
                if (monitor &&
                    window.x <= monitor.x &&
                    window.x + window.width >= monitor.x + monitor.width &&
                    window.y <= monitor.y &&
                    window.y + window.height >= monitor.y + monitor.height)
                    monitor.inFullscreen = true;
            } else
                break;
        }
    },

    /** Callback when windows are restacked on the screen (e.g. raised/lowered).
     *
     * We check whether whether any of the monitors is in fullscreen
     * ({@link #_updateFullscreen}), and if any of them have changed fullscreen
     * state we update the visibility of all the tracked actors
     * ({@link #_updateVisibility}) and call {@link #_queueUpdateRegions}
     * to update our input region/struts in response.
     */
    _windowsRestacked: function() {
        let wasInFullscreen = [];
        for (let i = 0; i < this._monitors.length; i++)
            wasInFullscreen[i] = this._monitors[i].inFullscreen;

        this._updateFullscreen();

        let changed = false;
        for (let i = 0; i < wasInFullscreen.length; i++) {
            if (wasInFullscreen[i] != this._monitors[i].inFullscreen) {
                changed = true;
                break;
            }
        }
        if (changed) {
            this._updateVisibility();
            this._queueUpdateRegions();
        }
    },

    /** Updates the stage's struts and input region to reflect all the actors
     * we are tracking.
     *
     * We build up an input region (collection of rectangles that input to
     * the stage is allowed, as opposed to it being passed directly to
     * any windows) and use `global.set_stage_input_regions` to
     * communicate this to the rest of the shell.
     *
     * We also build up a list of struts (these are part of the input region
     * but affect the space available to windows, e.g. the top panel) and
     * communicate these to Metacity using `Metacity.Workspace.set_builtin_struts`.
     *
     * See the class documentation for more information on the struts/input
     * regions.
     *
     * Building up these regions involves looping through all of our tracked
     * actors and taking into account the parameters they were
     * {@link #addActor}'d with. If `affectsInputRegion` was true then
     * the rectangle for that actor is added to our input region; if
     * `affectsStruts` was true we add the rectangle to the list of struts.
     *
     * If the actor is not visible or has `visibleInFullscreen` to `false`
     * and we are in fullscreen, it is ignored.
     * @see Layout.ChromeParameters
     * @see global.set_stage_input_regions
     * @see Meta.Workspace.set_builtin_struts
     */
    updateRegions: function() {
        let rects = [], struts = [], i;

        if (this._updateRegionIdle) {
            Mainloop.source_remove(this._updateRegionIdle);
            delete this._updateRegionIdle;
        }

        for (i = 0; i < this._trackedActors.length; i++) {
            let actorData = this._trackedActors[i];
            if (!actorData.affectsInputRegion && !actorData.affectsStruts)
                continue;

            let [x, y] = actorData.actor.get_transformed_position();
            let [w, h] = actorData.actor.get_transformed_size();
            x = Math.round(x);
            y = Math.round(y);
            w = Math.round(w);
            h = Math.round(h);
            let rect = new Meta.Rectangle({ x: x, y: y, width: w, height: h});

            if (actorData.affectsInputRegion &&
                actorData.actor.get_paint_visibility() &&
                !Main.uiGroup.get_skip_paint(actorData.actor))
                rects.push(rect);

            if (!actorData.affectsStruts)
                continue;

            // Limit struts to the size of the screen
            let x1 = Math.max(x, 0);
            let x2 = Math.min(x + w, global.screen_width);
            let y1 = Math.max(y, 0);
            let y2 = Math.min(y + h, global.screen_height);

            // NetWM struts are not really powerful enought to handle
            // a multi-monitor scenario, they only describe what happens
            // around the outer sides of the full display region. However
            // it can describe a partial region along each side, so
            // we can support having the struts only affect the
            // primary monitor. This should be enough as we only have
            // chrome affecting the struts on the primary monitor so
            // far.
            //
            // Metacity wants to know what side of the screen the
            // strut is considered to be attached to. If the actor is
            // only touching one edge, or is touching the entire
            // border of the primary monitor, then it's obvious which
            // side to call it. If it's in a corner, we pick a side
            // arbitrarily. If it doesn't touch any edges, or it spans
            // the width/height across the middle of the screen, then
            // we don't create a strut for it at all.
            let side;
            let primary = this._primaryMonitor;
            if (x1 <= primary.x && x2 >= primary.x + primary.width) {
                if (y1 <= primary.y)
                    side = Meta.Side.TOP;
                else if (y2 >= primary.y + primary.height)
                    side = Meta.Side.BOTTOM;
                else
                    continue;
            } else if (y1 <= primary.y && y2 >= primary.y + primary.height) {
                if (x1 <= 0)
                    side = Meta.Side.LEFT;
                else if (x2 >= global.screen_width)
                    side = Meta.Side.RIGHT;
                else
                    continue;
            } else if (x1 <= 0)
                side = Meta.Side.LEFT;
            else if (y1 <= 0)
                side = Meta.Side.TOP;
            else if (x2 >= global.screen_width)
                side = Meta.Side.RIGHT;
            else if (y2 >= global.screen_height)
                side = Meta.Side.BOTTOM;
            else
                continue;

            // Ensure that the strut rects goes all the way to the screen edge,
            // as this really what mutter expects.
            switch (side) {
            case Meta.Side.TOP:
                y1 = 0;
                break;
            case Meta.Side.BOTTOM:
                y2 = global.screen_height;
                break;
            case Meta.Side.LEFT:
                x1 = 0;
                break;
            case Meta.Side.RIGHT:
                x2 = global.screen_width;
                break;
            }

            let strutRect = new Meta.Rectangle({ x: x1, y: y1, width: x2 - x1, height: y2 - y1});
            let strut = new Meta.Strut({ rect: strutRect, side: side });
            struts.push(strut);
        }

        global.set_stage_input_region(rects);

        let screen = global.screen;
        for (let w = 0; w < screen.n_workspaces; w++) {
            let workspace = screen.get_workspace_by_index(w);
            workspace.set_builtin_struts(struts);
        }

        return false;
    }
};
