// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Wrappers around `Shell.MountOperation` which itself wraps aroundz
 * Defines the {@link ShellMountOperation} class, which is a wrapper around a
 * {@link Shell.MountOperation},
 * which is itself a wrapper around a {@link Gio.MountOperation}.
 *
 * When one wishes to perform a mount/unmount/eject/etc with a function like
 * `g_drive_start`, `g_drive_stop`, `g_volume_mount`, they may pass in a
 * `Gio.MountOperation` as an argument to use for user interaction.
 *
 * This means that if the operation needs to ask the user some questions (for
 * a password, say), signals are emitted from the MountOperation that should be
 * caught and dealt with.
 *
 * The {@link ShellMountOperation} listens to these signals and:
 *
 * * if the mount operation wishes to ask the user a question, a
 * {@link SHellMountQuestionDialog} is created for it, offering the user some
 * choices to select from,
 * * if the mount operation requires a password, a {@link ShellMountPasswordSource}
 * and {@link ShellMountPasswordNotification} are created to get the password.
 * * if the mount operation is to unmount/eject a drive but there are processes
 * still using that drive, a {@link ShellProcessesDialog} is shown to the user.
 *
 * To *use* a `ShellMountOperation`, you pass in {@link ShellMountOperation#mountOp}
 * into a function like `g_drive_start` or `g_drive_stop` or `g_volume_mount`,
 * and the mount operation will be used for that operation only (if `null`
 * where provided instead of the mount operation, no user interaction will occur).
 *
 * The functions {@link AutorunManager.AutorunManager#ejectMount} and
 * {@link AutomountManager.AutomountManager#_mountVolume} both use ShellMountOperations
 * in this manner.
 */
const Lang = imports.lang;
const Signals = imports.signals;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Pango = imports.gi.Pango;
const St = imports.gi.St;
const Shell = imports.gi.Shell;

const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const ModalDialog = imports.ui.modalDialog;
const Params = imports.misc.params;

/** Default icon size for a {@link ListItem}.
 * @type {number}
 * @default
 * @const */
const LIST_ITEM_ICON_SIZE = 48;

/* ------ Common Utils ------- */
/** Utility function - sets the text on a St.Label or hides it if the text
 * is `null`.
 * @param {St.Label} label - label to set the text of
 * @param {?string} text - text to put on the label, or `null` to hide the
 * label. */
function _setLabelText(label, text) {
    if (text) {
        label.set_text(text);
        label.show();
    } else {
        label.set_text('');
        label.hide();
    }
}

/** Convenience function - adds buttons to a dialog from an array of labels,
 * emitting the 'response' signal when the button is clicked with the index
 * of the button clicked (same as the index in `choices`).
 *
 * Only used by the {@link ShellProcessesDialog} and
 * {@link ShellMountQuestionDialog}.
 * @param {ModalDialog.ModalDialog} dialog - dialog to add buttons to.
 * @param {string[]} choices - array of labels to display on the buttons.
 * @see ModalDialog.ModalDialog#setButtons
 * @fires ShellMountOperationDialog.event:response
 */
function _setButtonsForChoices(dialog, choices) {
    let buttons = [];

    for (let idx = 0; idx < choices.length; idx++) {
        let button = idx;
        buttons.unshift({ label: choices[idx],
                          action: Lang.bind(dialog, function() {
                              dialog.emit('response', button);
                          })});
    }

    dialog.setButtons(buttons);
}

/** Utility function - sets a dialog's subject label and description label
 * from a message. The first line goes into the subject label, and the
 * second line (if any) goes into the description label.
 *
 * Only the {@link ShellProcessesDialog} and {@link ShellMountQuestionDialog}
 * have the `subjectLabel` and `descriptionLabel` members.
 * 
 * @param {ShellMountOperation.ShellProcessesDialog|ShellMountOperation.ShellMountQuestionDialog} dialog -
 * dialog to set the message(s) of.
 * @param {message} - message to set. The first line will be set as the subject,
 * and the second (if any) is set as the message.
 */
function _setLabelsForMessage(dialog, message) {
    let labels = message.split('\n');

    _setLabelText(dialog.subjectLabel, labels[0]);
    if (labels.length > 1)
        _setLabelText(dialog.descriptionLabel, labels[1]);
}

/** Fired when the user clicks any of the buttons on a
 * {@link ShellProcessesDialog} or {@link ShellMountQuestionDialog}.
 * @name response
 * @event
 * @memberof ShellMountOperationDialog
 * @param {ShellMountOperation.ShellProcessesDialog|ShellMountOperation.ShellMountQuestionDialog} dialog -
 * the dialog that emitted the signal.
 * @param {number} the 0-based index of the button selected, or -1 if the operation
 * is to be aborted.
 */

/* -------------------------------------------------------- */

/** creates a new ListItem
 * Very similar to the {@link EndSessionDialog.ListItem}.
 *
 * The top-level actor {@link ListItem#actor} is a `St.Button` with style
 * class 'show-processes-dialog-app-list-item'.
 *
 * Inside that actor is a horizontal St.BoxLayout containing an icon for
 * the application ({@link ListItem#_icon}) as well as a St.Label with the
 * application's name ({@link ListItem#_nameLabel}).
 *
 * The various style classes are 'show-processes-dialog-app-list-list-item-*'.
 * @param {Shell.App} app - application for the list item.
 * @classdesc
 * When you try to unmount/eject a volume that is busy, you are presented
 * with a {@link ShellProcessesDialog} saying "Volume is busy" showing
 * the applications that are using the volume.
 *
 * ![ShellProcessesDialog with one ListItem (for evince) in it](pics/shellProcessesDialog.png)
 *
 * The `ListItem` is the app icon + app name that appears in the dialog for
 * each application using the device.
 *
 * Upon clicking the item, it emits a 'activate' signal and activates that application
 * (so the user can save their changes and close it before trying to
 * unmount again, for example).
 *
 * Very similar to the {@link EndSessionDialog.ListItem}.
 * @class
 */
function ListItem(app) {
    this._init(app);
}

ListItem.prototype = {
    _init: function(app) {
        /** Application for this ListItem.
         * @type {Shell.App} */
        this._app = app;

        let layout = new St.BoxLayout({ vertical: false});

        /** Top-level actor for the ListItem. A button with the application icon
         * and name in a layout in it. Style class 'show-processes-dialog-app-list-item'.
         * @type {St.Button} */
        this.actor = new St.Button({ style_class: 'show-processes-dialog-app-list-item',
                                     can_focus: true,
                                     child: layout,
                                     reactive: true,
                                     x_align: St.Align.START,
                                     x_fill: true });

        /** The icon for the application. Uses `shell_app_create_icon_texture`
         * and creates an icon of size {@link LIST_ITEM_ICON_SIZE} (48 pixels).
         *
         * The St.Bin containing it has style class
         * 'show-processes-dialog-app-list-item-icon'.
         * @type {Clutter.Actor} */
        this._icon = this._app.create_icon_texture(LIST_ITEM_ICON_SIZE);

        let iconBin = new St.Bin({ style_class: 'show-processes-dialog-app-list-item-icon',
                                   child: this._icon });
        layout.add(iconBin);

        /** The label for the application name. Style class
         * 'show-processes-dialog-app-list-item-name'.
         * @type {St.Label} */
        this._nameLabel = new St.Label({ text: this._app.get_name(),
                                         style_class: 'show-processes-dialog-app-list-item-name' });
        let labelBin = new St.Bin({ y_align: St.Align.MIDDLE,
                                    child: this._nameLabel });
        layout.add(labelBin);

        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
    },

    /** callback when a ListItem is clicked.
     * This emits the 'activate' signal (caught by the parent dialog) and
     * activates the app.
     * @fires ListItem.activate */
    _onClicked: function() {
        this.emit('activate');
        this._app.activate();
    }
};
Signals.addSignalMethods(ListItem.prototype);
/** Event fired whenever the user clicks on a ListItem.
 * Used in the {@link ShellProcessesDialog} and in particular
 * {@link ShellProcessesDialog#_setAppsForPids} to close the dialog
 * when the user clicks on the item.
 * @event
 * @name activate
 * @memberof ListItem */

/** creates a new ShellMountOperation
 *
 * We create a `Shell.MountOperation` and store it and connect to its
 * 'ask-password', 'ask-question', 'show-processes-2' and 'aborted' signals.
 * @param {Gio.Mount} source - a Gio.Mount or Gio.Volume or Gio.Drive that the
 * mount operation is for.
 * @param {Object} [params] - parameters for the MountOperation.
 * @param {boolean} [params.reaskPassword=false] - whether this mount operation
 * is the result of re-asking a password after a failed attempt (used for
 * the {@link ShellMountPasswordNotification} and nowhere else, it seems).
 * @classdesc
 * This is a wrapper around a {@link Shell.MountOperation},
 * which is itself a wrapper around a {@link Gio.MountOperation}.
 *
 * A MountOperation provides a mechanism for interacting with the user.
 * It can be used for authenticating mountable operations, or to ask the user
 * questions, or show a list of applications preventing unmount or eject operations
 * from completing.
 *
 * It creates and shows the appropriate dialogs for the mount operation's
 * request (for example a {@link ShellProcessesDialog} for the 'show-processes-2'
 * signal) and handles responses.
 * @todo how is Shell.MountOperation's 'show-processes-2' signal different
 * from Gio.MountOperation's 'show-processes' signal and why should we be
 * using the former?
 *
 * To *use* a `ShellMountOperation`, you pass in {@link ShellMountOperation#mountOp}
 * into a function like `g_drive_start` or `g_drive_stop` or `g_volume_mount`,
 * and the mount operation will be used for that operation only (if `null`
 * where provided instead of the mount operation, no user interaction will occur).
 *
 * The functions {@link AutorunManager.AutorunManager#ejectMount} and
 * {@link AutomountManager.AutomountManager#_mountVolume} both use ShellMountOperations
 * in this manner.
 * @class
 */
function ShellMountOperation(source, params) {
    this._init(source, params);
}

ShellMountOperation.prototype = {
    _init: function(source, params) {
        params = Params.parse(params, { reaskPassword: false });

        /** Whether this operation is reasking for a password because of a failed
         * attempt the first time.
         *
         * Only used in creating {@link ShellMountPasswordNotification}s for the
         * request.
         * @type {boolean} */
        this._reaskPassword = params.reaskPassword;

        /** A ShellMountQuestionDialog for this mount operation, or `null` if
         * we haven't had to make one yet (it's only created on demand).
         * @type {?ShellMountOperation.ShellMountQuestionDialog} */
        this._dialog = null;
        /** A ShellProcessesDialog for this mount operation, or `null` if
         * we haven't had to make one yet (it's only created on demand).
         * @type {?ShellMountOperation.ShellProcessesDialog} */
        this._processesDialog = null;

        /** The MountOperation we are wrapping around.
         * @type {Shell.MountOperation} */
        this.mountOp = new Shell.MountOperation();

        this.mountOp.connect('ask-question',
                             Lang.bind(this, this._onAskQuestion));
        this.mountOp.connect('ask-password',
                             Lang.bind(this, this._onAskPassword));
        this.mountOp.connect('show-processes-2',
                             Lang.bind(this, this._onShowProcesses2));
        this.mountOp.connect('aborted',
                             Lang.bind(this, this._onAborted));

        /** Icon for the volume, used in the dialogs and for notifications.
         * Style class 'shell-mount-operation-icon'.
         * @type {St.Icon} */
        this._icon = new St.Icon({ gicon: source.get_icon(),
                                   style_class: 'shell-mount-operation-icon' });
    },

    /** Callback for the 'ask-question' signal of {@link ShellMountOperation#mountOp}.
     *
     * A new {@link ShellMountQuestionDialog} is created displaying the input
     * `message` and showing the `choices` as buttons. Upon clicking a button
     * (i.e. dialog emits the ['response' signal]{@link ShellMountOperationDialog.event:response}
     * signal), the mount operation is sent the choice and the dialog is closed.
     * @param {Shell.MountOperation} op - the operation that emitted the signal
     * {@link ShellMountOperation#mountOp}
     * @param {string} message - the message to give to the user (like
     * "Volume is busy\nOne or more applications are keeping the volume busy.").
     * @param {string[]} choices - array of strings for each possible choice
     * (like "Cancel" and "Unmount Anyway").
     */
    _onAskQuestion: function(op, message, choices) {
        this._dialog = new ShellMountQuestionDialog(this._icon);

        this._dialog.connect('response',
                               Lang.bind(this, function(object, choice) {
                                   this.mountOp.set_choice(choice);
                                   this.mountOp.reply(Gio.MountOperationResult.HANDLED);

                                   this._dialog.close(global.get_current_time());
                                   this._dialog = null;
                               }));

        this._dialog.update(message, choices);
        this._dialog.open(global.get_current_time());
    },

    /** Callback for the 'ask-password' signal of {@link ShellMountOperation#mountOp}.
     *
     * A new {@link ShellMountPasswordSource} is created in the message tray to
     * notify the user that they need to enter in a password.
     *
     * Upon the ['password-ready']{@link ShellMountPasswordSource.password-ready}
     * signal being emitted (i.e. user has entered a password), the mount
     * operation is sent the password and the source is destroyed.
     * @inheritparams ShellMountOperation#_onAskQuestion
     */
    _onAskPassword: function(op, message) {
        this._notificationShowing = true;
        this._source = new ShellMountPasswordSource(message, this._icon, this._reaskPassword);

        this._source.connect('password-ready',
                             Lang.bind(this, function(source, password) {
                                 this.mountOp.set_password(password);
                                 this.mountOp.reply(Gio.MountOperationResult.HANDLED);

                                 this._notificationShowing = false;
                                 this._source.destroy();
                             }));

        this._source.connect('destroy',
                             Lang.bind(this, function() {
                                 if (!this._notificationShowing)
                                     return;

                                 this._notificationShowing = false;
                                 this.mountOp.reply(Gio.MountOperationResult.ABORTED);
                             }));
    },

    /** Callback for the 'aborted' signal of {@link ShellMountOperation#mountOp},
     * emitted when the device becomes unavailable while the mount operation is
     * in progress. If a dialog is currently open, it is closed.
     * @inheritparams ShellMountOperation#_onAskQuestion
     */
    _onAborted: function(op) {
        if (!this._dialog)
            return;

        this._dialog.close(global.get_current_time());
        this._dialog = null;
    },

    /** Callback for the 'show-processes-2' signal of {@link ShellMountOperation#mountOp},
     * emitted when one or more processes are blocking an operation (like
     * unmounting/ejecting).
     *
     * The user is shown a {@link ShellProcessesDialog} with a {@link ListItem}
     * for each process blocking the operation.
     *
     * Upon either a list item being clicked or a button being clicked (all
     * handled by the dialog's ['response' signal]{@link ShellProcessesDialog.event:response}):
     *
     * * if the user clicked a ListItem, the mount operation is aborted
     * * if the user clicked one of the buttons (cancel/unmount anyway), the
     * mount operation is told the user's choice and finishes off the operation.
     *
     * Then the dialog is closed.
     * @inheritparams ShellMountOperation#_onAskQuestion */
    _onShowProcesses2: function(op) {
        let processes = op.get_show_processes_pids();
        let choices = op.get_show_processes_choices();
        let message = op.get_show_processes_message();

        if (!this._processesDialog) {
            this._processesDialog = new ShellProcessesDialog(this._icon);
            this._dialog = this._processesDialog;

            this._processesDialog.connect('response', 
                                          Lang.bind(this, function(object, choice) {
                                              if (choice == -1) {
                                                  this.mountOp.reply(Gio.MountOperationResult.ABORTED);
                                              } else {
                                                  this.mountOp.set_choice(choice);
                                                  this.mountOp.reply(Gio.MountOperationResult.HANDLED);
                                              }

                                              this._processesDialog.close(global.get_current_time());
                                              this._dialog = null;
                                          }));
            this._processesDialog.open(global.get_current_time());
        }

        this._processesDialog.update(message, processes, choices);
    },
}

/** creates a new ShellMountQuestionDialog.
 *
 * The [parent constructor]{@link ModalDialog.ModalDialog} is called with style
 * class 'mount-question-dialog'.
 *
 * The contents of the dialog consist of:
 *
 * * an icon, being the input `icon`;
 * * a place for the subect to go, style class 'mount-question-dialog-subject'
 * ({@link #subjectLabel});
 * * a place for the message to go, style class 'mount-question-dialog-description'
 * ({@link #descriptionLabel}).
 *
 * Note that the constructor does not display the subject/message; the user is
 * expected to set this (and the dialog buttons) themselves via
 * {@link #update}.
 * @param {Clutter.Actor} icon - the icon to display in the dialog.
 * @classdesc
 * A ShellMountQuestionDialog is a modal dialog spawned in response to the
 * 'ask-question' signal of a {@link Gio.MountOperation}.
 * (The {@link ShellMountOperation} class wraps around one of these and is what
 * is actually responsible for creating the dialog).
 *
 * This happens when the user mounts or unmounts (or ejects) a drive/volume/mount
 * and user interaction is required (**TODO**: example of a question that could
 * be asked from this signal?).
 *
 * The dialog presents the message from the mount operation as well as a list of
 * choices that the user has to choose from, one button for each ({@link _setButtonsForChoices}).
 *
 * Upon selecting a choice, the dialog emits a
 * ['response' signal]{@link ShellMountOperationDialog.event:response} that is
 * caught by the parent {@link ShellMountOperation} and communicated to the
 * underlying `Gio.MountOperation`.
 *
 * Note that the user is responsible for setting the message/buttons of the dialog;
 * to do this, use {@link #update} with the message
 * and choices to create buttons for.
 * @class
 * @todo what is an example of a 'ask-question'? screenshot of the dialog?
 * @extends ModalDialog.ModalDialog
 * @fires ShellMountOperationDialog.event:response
 */
function ShellMountQuestionDialog(icon) {
    this._init(icon);
}

ShellMountQuestionDialog.prototype = {
    __proto__: ModalDialog.ModalDialog.prototype,

    _init: function(icon) {
        ModalDialog.ModalDialog.prototype._init.call(this, { styleClass: 'mount-question-dialog' });

        let mainContentLayout = new St.BoxLayout();
        this.contentLayout.add(mainContentLayout, { x_fill: true,
                                                    y_fill: false });

        this._iconBin = new St.Bin({ child: icon });
        mainContentLayout.add(this._iconBin,
                              { x_fill:  true,
                                y_fill:  false,
                                x_align: St.Align.END,
                                y_align: St.Align.MIDDLE });

        let messageLayout = new St.BoxLayout({ vertical: true });
        mainContentLayout.add(messageLayout,
                              { y_align: St.Align.START });

        /** Where the subject/title goes. Style class 'mount-question-dialog-subject'.
         * @type {St.Label} */
        this.subjectLabel = new St.Label({ style_class: 'mount-question-dialog-subject' });

        messageLayout.add(this.subjectLabel,
                          { y_fill:  false,
                            y_align: St.Align.START });

        /** Where the description/message goes. Style class
         * 'mount-question-dialog-description'.
         * @type {St.Label} */
        this.descriptionLabel = new St.Label({ style_class: 'mount-question-dialog-description' });
        this.descriptionLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this.descriptionLabel.clutter_text.line_wrap = true;

        messageLayout.add(this.descriptionLabel,
                          { y_fill:  true,
                            y_align: St.Align.START });
    },

    /** Sets the message (subject/description) on the dialog as well as creating
     * buttons for each of the choices.
     *
     * {@link _setLabelsForMessage} is used to set the message; the first line of
     * `message` is set as the subject ({@link #subjectLabel})
     * and the second (if any) is set as the message
     * ({@link #descriptionLabel}).
     *
     * One button for each element of `choices` (displaying the text) is created
     * using {@link _setButtonsForChoices}.
     * @see _setButtonsForChoices
     * @see _setLabelsForMessage
     * @inheritparams _setButtonsForChoices
     * @inheritparams _setLabelsForMessage
     */
    update: function(message, choices) {
        _setLabelsForMessage(this, message);
        _setButtonsForChoices(this, choices);
    }
}
Signals.addSignalMethods(ShellMountQuestionDialog.prototype);

/** creates a new ShellMountPasswordSource.
 *
 * This splits `message` into its heading and subject and calls the
 * [parent constructor]{@link MessageTray.Source} with the heading.
 *
 * It then creates a {@link ShellMountPasswordNotification}, passing through
 * the message/subject, `icon`, and `reaskPassword`.
 *
 * Finally the source is added to the message tray {@link MessageTray.MessageTray#add}
 * and the notification is shown {@link MessageTray.MessageTray#notify}.
 *
 * @param {string} message - message for the password request. First line is the
 * heading and the second (if any) is the message.
 * @param {Clutter.Actor} icon - relevant icon for the drive, used for the Notification
 * (and hence the Source too).
 * @param {boolean} reaskPassword - whether this mount operation is the result of
 * re-asking for a password after a failed attempt (only used to modify the message
 * in the {@Link ShellMountPasswordNotification}.
 * @classdesc
 * A ShellMountPasswordSource is a source in the message tray that asks for a
 * password that is required in order to mount a drive.
 *
 * It is spawned from a {@link ShellMountOperation} in response to its
 * Shell.MountOperation's 'ask-password' signal.
 *
 * Its corresponding notification is a {@link ShellMountPasswordNotification}.
 *
 * When a password is entered into the notification, the source will emit
 * a 'password-ready' signal that is caught by the {@link ShellMountOperation}
 * and handled.
 * @class
 * @todo get a screenshot
 * @extends MessageTray.Source
 * @fires .password-ready
 */
function ShellMountPasswordSource(message, icon, reaskPassword) {
    this._init(message, icon, reaskPassword);
}

ShellMountPasswordSource.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function(message, icon, reaskPassword) {
        let strings = message.split('\n');
        MessageTray.Source.prototype._init.call(this, strings[0]);

        this._notification = new ShellMountPasswordNotification(this, strings, icon, reaskPassword);

        // add ourselves as a source, and popup the notification
        Main.messageTray.add(this);
        this.notify(this._notification);
    },
}
Signals.addSignalMethods(ShellMountPasswordSource.prototype);
/** Fired from the {@link ShellMountPasswordSource} when the user enters a
 * password into its {@link ShellMountPasswordNotification}.
 * @name password-ready
 * @event
 * @param {ShellMountOperation.ShellMountPasswordSource} source - the source that
 * emitted the signal.
 * @param {string} text - the password (plaintext??!!)
 * @see ShellMountOperation#_onAskPassword
 */

/** creates a new ShellMountPasswordNotification.
 *
 * This calls the [parent constructor]{@link MessageTray.Notification} with
 * `source` as the source, `strings[0]` as the title, no banner, custom
 * content (message + password box), and `icon` as the icon.
 *
 * We set the notification to be transient ({@link #setTransient})
 * and urgent ({@link #setUrgency},
 * {@link MessageTray.Urgency.CRITICAL}).
 *
 * Then the message text (`strings[1]`) is added to the content area
 * ({@link #addBody}), along with a label
 * "Wrong password, please try again" if `reaskPassword` is true.
 *
 * Then we create a St.Entry for the user to enter the password
 * ({@link #_responseEntry}) and set this as the action
 * area of the notification
 * ({@link #setActionArea}).
 *
 * Finally we grab keyboard focus to the password entry box.
 *
 * @param {ShellMountOperation.ShellMountPasswordSource} source - the source for
 * the request.
 * @param {string[]} strings - message for the password request. First element
 * is the heading, second is the subject.
 * @param {Clutter.Actor} icon - icon for the notification, being the relevant
 * icon for the drive.
 * @param {boolean} reaskPassword - whether this mount operation is the result of
 * re-asking for a password after a failed attempt (only used to add a message
 * "Wrong password, please try again").
 * @classdesc
 * A ShellMountPasswordNotification is a notification that appears from a
 * {@link ShellMountPasswordSource}. It appears when a password that is required
 * in order to mount a drive.
 *
 * @class
 * @todo get a screenshot
 * @extends MessageTray.Notification
 */
function ShellMountPasswordNotification(source, strings, icon, reaskPassword) {
    this._init(source, strings, icon, reaskPassword);
}

ShellMountPasswordNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, strings, icon, reaskPassword) {
        MessageTray.Notification.prototype._init.call(this, source,
                                                      strings[0], null,
                                                      { customContent: true,
                                                        icon: icon });

        // set the notification to transient and urgent, so that it
        // expands out
        this.setTransient(true);
        this.setUrgency(MessageTray.Urgency.CRITICAL);

        if (strings[1])
            this.addBody(strings[1]);

        if (reaskPassword) {
            let label = new St.Label({ style_class: 'mount-password-reask',
                                       text: _("Wrong password, please try again") });

            this.addActor(label);
        }

        /** The password box for the notification, style class 'mount-password-entry'.
         * @type {St.Entry} */
        this._responseEntry = new St.Entry({ style_class: 'mount-password-entry',
                                             can_focus: true });
        this.setActionArea(this._responseEntry);

        this._responseEntry.clutter_text.connect('activate',
                                                 Lang.bind(this, this._onEntryActivated));
        this._responseEntry.clutter_text.set_password_char('\u25cf'); // ● U+25CF BLACK CIRCLE

        this._responseEntry.grab_key_focus();
    },

    /** Callback when the user activates the password entry box.
     * This fires the *source*'s 'password-ready' signal with the password
     * which will then be caught by the {@link ShellMountOperation}.
     * @fires ShellMountPasswordSource.password-ready */
    _onEntryActivated: function() {
        let text = this._responseEntry.get_text();
        if (text == '')
            return;

        this.source.emit('password-ready', text);
    }
}

/** creates a new ShellProcessesDialog.
 *
 * The [parent constructor]{@link ModalDialog.ModalDialog} is called with style
 * class 'show-processes-dialog'.
 *
 * The contents of the dialog consist of:
 *
 * * an icon, being the input `icon`;
 * * a place for the subect to go, style class 'show-processes-dialog-subject'
 * ({@link #subjectLabel});
 * * a place for the message to go, style class 'show-processes-dialog-description'
 * ({@link #descriptionLabel}).
 * * a ScrollView containing a St.BoxLayout which will contain a {@link ListItem}
 * for each application/process that is inhibiting the mount operation. The
 * ScrollView has style class 'show-processes-dialog-app-list'.
 *
 * Note that the constructor does not display the subject/message/processes; the
 * user expected to set these themselves via a call to
 * {@link #update}.
 * @param {Clutter.Actor} icon - the icon to display in the dialog.
 * @classdesc
 * ![ShellProcessesDialog with one ListItem for evince](pics/shellProcessesDialog.png)
 *
 * A ShellProcessesDialog is a modal dialog spawned in response to the
 * 'show-processes-2' signal of a {@link ShellMountOperation}.
 *
 * This happens when the user mounts or unmounts (or ejects) a drive/volume/mount
 * but there are processes inhibiting the operation.
 *
 * The dialog presents displays a {@link ListItem} per inhibiting process plus
 * a number of choices ("Unmount Anyway"/"Cancel").
 *
 * Upon selecting a choice, the dialog emits a
 * ['response' signal]{@link ShellMountOperationDialog.event:response} that is
 * caught by the parent {@link ShellMountOperation} and communicated to the
 * underlying `Gio.MountOperation`.
 *
 * Upon selecting one of the ListItems, the 'response' signal is emitted and
 * caught by the parent {@link ShellMountOperation} and sent to the underlying
 * Gio.MountOperation as an aborted operation. The inhibiting application is then
 * activated (brought to focus) for the user.
 *
 * Note that the user is responsible for setting the message/buttons/inhibiting
 * processes for the dialog; to do this, use {@link #update}
 * with the message, inhibiting processes, and choices.
 * @class
 * @extends ModalDialog.ModalDialog
 * @fires ShellMountOperationDialog.event:response
 */
function ShellProcessesDialog(icon) {
    this._init(icon);
}

ShellProcessesDialog.prototype = {
    __proto__: ModalDialog.ModalDialog.prototype,

    _init: function(icon) {
        ModalDialog.ModalDialog.prototype._init.call(this, { styleClass: 'show-processes-dialog' });

        let mainContentLayout = new St.BoxLayout();
        this.contentLayout.add(mainContentLayout, { x_fill: true,
                                                    y_fill: false });

        this._iconBin = new St.Bin({ child: icon });
        mainContentLayout.add(this._iconBin,
                              { x_fill:  true,
                                y_fill:  false,
                                x_align: St.Align.END,
                                y_align: St.Align.MIDDLE });

        let messageLayout = new St.BoxLayout({ vertical: true });
        mainContentLayout.add(messageLayout,
                              { y_align: St.Align.START });

        /** Where the subject/title goes. Style class 'show-processes-dialog-subject'.
         * @type {St.Label} */
        this.subjectLabel = new St.Label({ style_class: 'show-processes-dialog-subject' });

        messageLayout.add(this.subjectLabel,
                          { y_fill:  false,
                            y_align: St.Align.START });

        /** Where the description/message goes. Style class
         * 'show-processes-dialog-description'.
         * @type {St.Label} */
        this.descriptionLabel = new St.Label({ style_class: 'show-processes-dialog-description' });
        this.descriptionLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this.descriptionLabel.clutter_text.line_wrap = true;

        messageLayout.add(this.descriptionLabel,
                          { y_fill:  true,
                            y_align: St.Align.START });

        let scrollView = new St.ScrollView({ style_class: 'show-processes-dialog-app-list'});
        scrollView.set_policy(Gtk.PolicyType.NEVER,
                              Gtk.PolicyType.AUTOMATIC);
        this.contentLayout.add(scrollView,
                               { x_fill: true,
                                 y_fill: true });
        scrollView.hide();

        this._applicationList = new St.BoxLayout({ vertical: true });
        scrollView.add_actor(this._applicationList,
                             { x_fill:  true,
                               y_fill:  true,
                               x_align: St.Align.START,
                               y_align: St.Align.MIDDLE });

        /** Vertical BoxLayout that all the {@link ListItem}s get added to.
         * It's in a St.ScrollView so that it scrolls...
         * @type {St.BoxLayout} */
        this._applicationList.connect('actor-added',
                                      Lang.bind(this, function() {
                                          if (this._applicationList.get_children().length == 1)
                                              scrollView.show();
                                      }));

        this._applicationList.connect('actor-removed',
                                      Lang.bind(this, function() {
                                          if (this._applicationList.get_children().length == 0)
                                              scrollView.hide();
                                      }));
    },

    /** Creates a {@link ListItem} for each PID impeding the mount operation.
     *
     * For each PID, we get the corresponding app using
     * [`Shell.WindowTracker.get_default().get_app_from_pid(pid)`](http://developer.gnome.org/shell/unstable/shell-ShellWindowTracker.html#shell-window-tracker-get-app-from-pid),
     * and then we create a {@link ListItem} for that app and add it to
     * {@link #_applicationList}.
     *
     * We also connect to its ['activate' signal]{@link ListItem.activate} (i.e.
     * it's been clicked) and re-transmit this as our
     * ['response' signal]{@link ShellMountOperationDialog.event:response} with
     * choice index '-1' indicating that the operation is to be canceled.
     * @param {number[]} pids - processes IDs for each process impeding the
     * mount operation.
     */
    _setAppsForPids: function(pids) {
        // remove all the items
        this._applicationList.destroy_children();

        pids.forEach(Lang.bind(this, function(pid) {
            let tracker = Shell.WindowTracker.get_default();
            let app = tracker.get_app_from_pid(pid);

            if (!app)
                return;

            let item = new ListItem(app);
            this._applicationList.add(item.actor, { x_fill: true });

            item.connect('activate',
                         Lang.bind(this, function() {
                             // use -1 to indicate Cancel
                             this.emit('response', -1);
                         }));
        }));
    },

    /** Sets the message (subject/description) on the dialog as well as creating
     * buttons for each of the choices and {@link ListItem}s for each impeding
     * process.
     *
     * {@link _setLabelsForMessage} is used to set the message; the first line of
     * `message` is set as the subject ({@link ShellMountQuestionDialog#subjectLabel})
     * and the second (if any) is set as the message
     * ({@link ShellMountQuestionDialog#descriptionLabel}).
     *
     * One button for each element of `choices` (displaying the text) is created
     * using {@link _setButtonsForChoices}.
     *
     * {@link #_setAppsForPids} is used to create the
     * {@link ListItem}s for each impeding process.
     * @see #_setAppsForPids
     * @see _setButtonsForChoices
     * @see _setLabelsForMessage
     * @inheritparams _setButtonsForChoices
     * @inheritparams _setLabelsForMessage
     * @param {number[]} processes - array of PIDs that are inhibiting the
     * operation.
     */
    update: function(message, processes, choices) {
        this._setAppsForPids(processes);
        _setLabelsForMessage(this, message);
        _setButtonsForChoices(this, choices);
    }
}
Signals.addSignalMethods(ShellProcessesDialog.prototype);
