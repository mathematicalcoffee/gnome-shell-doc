// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the message tray (bottom of the screen showing notifications).
 *
 * As far as I can tell things work like this:
 *
 * * A {@link Notification} is what pops up to the user notifying them of
 * something.
 * * {@link Notification}s *must* be notified from a {@link Source}.
 * A {@link Source} is a source of notifications.
 * A {@link Source} must also provide an icon to be used for it (in the
 * message tray) and its notifications. Use `source.notify(notification)` to
 * both add a notification to a source and pop it up to the user.
 * * A {@link SummaryItem} is the graphical representation of a {@link Source}
 * in the message tray. It consists of an icon (taken from the
 * {@link Source}) and text, and clicking on it will bring up pending
 * {@link Notification}s for that source.
 * * The {@link MessageTray} is the thing at the bottom of the screen that
 * contains the {@link SummaryItem}s for the notifications/sources.
 *
 * Note that while *every* notification *must* belong to a source, it is possible
 * for a source to have no notifications (i.e. just have a presence in the
 * message tray), and it is possible for multiple notifications to come
 * from one source.
 *
 * There are also various behaviours notifications and sources can have.
 *
 * By default, notifications that are notified to the user then live with
 * their source's summary item in the message tray (if they are not
 * dismissed when they are notified) - clicking on the summary item will
 * show all those notifications, and clicking on the notification will
 * destroy it.
 *
 * A *transient* notification is destroyed as soon as it finishes popping
 * up to the user, so a source with only transient notifications won't
 * appear in the message tray. Make a notification transient with
 * {@link Notification#setTransient}
 *
 * A *resident* notification is not destroyed when it is clicked on. It
 * stays with its source until it is destroyed by other means (for example
 * the nautilus file transfer notification stays until the underlying file
 * transfer is complete, and then destroys itself). Make a notification
 * resident by using {@link Notification#setResident}.
 *
 * Note that a single {@link Source} may spawn any combination of
 * resident, transient, and default notifications. The transient ones won't
 * appear in the notification list that appears on clicking the source's
 * {@link SummaryItem}. Clicking on the default notifications in the summary
 * item's list will remove them, while clicking on resident ones won't.
 *
 * If you want a Source to remove itself as *soon* as it its Summary Item
 * has been clicked on, make it transient using {@link Source#setTransient}.
 * In GNOME 3.4+, the on-screen keyboard uses a transient source so that clicking
 * on the keyboard icon in the message tray launches the on-screen keyboard
 * and removes the icon.
 *
 * I'm not sure if it makes sense for a transient source to have notifications
 * and whether its notifications should be transient/resident/default.
 * If a transient source has notifications and you click on it, it will bring
 * up a list of all the current notifications. Hiding the list will immediately
 * destroy that source and all its notifications whether or not all the
 * notifications have been dismissed, whereas a non-transient source will
 * only destroy itself once its notifications have been dismissed.
 */

const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Pango = imports.gi.Pango;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const BoxPointer = imports.ui.boxpointer;
const GnomeSession = imports.misc.gnomeSession;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const Params = imports.misc.params;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;

/** @+
 * @const
 * @default
 * @type {number} */
/** Time taken (seconds) to expand a notification to show its banner, and for
 * the message tray to slide up/down when it shows/hdies.
 * @see MessageTray#_showTray
 * @see MessageTray#_hideTray
 * @see Notification#expand */
const ANIMATION_TIME = 0.2;
/** After a notification has been expanded, after this many seconds it will
 * hide again. */
const NOTIFICATION_TIMEOUT = 4;
/** Time taken in seconds to show a notification summary
 * @see MessageTray#_showSummary */
const SUMMARY_TIMEOUT = 1;
/** Time taken in seconds to show a notification summary - longer than
 * {@link SUMMARY_TIMEOUT}.
 * @see MessageTray#_showSummary */
const LONGER_SUMMARY_TIMEOUT = 4;

/** Delay (seconds) before we hide the message tray after the mouse has left it */
const HIDE_TIMEOUT = 0.2;
/** Delay (seconds) before we hide the message tray after the mouse has left it,
 * longer than {@link HIDE_TIMEOUT} */
const LONGER_HIDE_TIMEOUT = 0.6;

/** Maximum title width a source can have (in the message tray) */
const MAX_SOURCE_TITLE_WIDTH = 180;

/** We delay hiding of the tray if the mouse is within MOUSE_LEFT_ACTOR_THRESHOLD
 * range from the point where it left the tray. */
const MOUSE_LEFT_ACTOR_THRESHOLD = 20;
/** @- */

/** @+
 * @const
 * @enum */

/** Visibility state for notifications/message tray/message tray components. */
const State = {
    /** it is hidden */
    HIDDEN:  0,
    /** in the process of showing */
    SHOWING: 1,
    /** has been shown */
    SHOWN:   2,
    /** in the process of hiding */
    HIDING:  3
};

// TODO: update in lieu of notificationDaemon
/** The reason a notification was destroyed.
 *
 * These reasons are useful when we destroy the notifications received through
 * the notification daemon.
 *
 * Designed to be in sync with {@link NotificationDaemon.NotificationClosedReason}.
 * @see NotificationDaemon.NotificationClosedReason
 */
const NotificationDestroyedReason = {
    /** Transient notifications that th euser did not interact with */
    EXPIRED: 1,
    /** All other notifications destroyed as a result of a user action */
    DISMISSED: 2,
    /** Notifications that were requested to be destroyed by the associated
     * source */
    SOURCE_CLOSED: 3
};

/** Urgency of a notification in the message tray.
 * LOW, NORMAL and CRITICAL
 * urgency values map to the corresponding values for the notifications received
 * through the notification daemon. HIGH urgency value is used for chats received
 * through the Telepathy client.
 *
 * @see NotificationDaemon.Urgency
 */
const Urgency = {
    /** Low urgency */
    LOW: 0,
    /** Normal urgency */
    NORMAL: 1,
    /** Used for chats received through the telepathy client */
    HIGH: 2,
    /** Used for critical things (e.g. {@link ShellMountOperation.ShellMountPasswordNotification}). */
    CRITICAL: 3
}
/** @- */

/** Processes text to allow markup or remove it.
 *
 * If `allowMarkup` is true:
 *
 * * replace '&' by '&amp;', unless it's '&quot;', '&apos;', '&lt;', '&gt;';
 * * replace '<' by '&lt;', unless it's `<b>`, `<i>`, `<u>`.
 *
 * The processed text is then validated with `Pango.parse_markup`. If validation
 * succeeded, the processed text is returned.
 *
 * If it failed, or `allowMarkup` is false, `GLib.markup_escape_text(text)` is
 * returned, which escapes all special entities.
 * @param {string} text - text to process
 * @param {boolean} allowMarkup - whether to allow markup in the text.
 * @returns {string} if the text is valid and `allowMarkup` is true, the marked-up
 * text (with characters like `&` and `<` that are not used as part of the
 * markup converted to their HTML entities). Otherwise, the input `text` with
 * *all* special characters escaped.
 * @todo check that markdown/jsdoc processed this markup ok...
 */
function _fixMarkup(text, allowMarkup) {
    if (allowMarkup) {
        // Support &amp;, &quot;, &apos;, &lt; and &gt;, escape all other
        // occurrences of '&'.
        let _text = text.replace(/&(?!amp;|quot;|apos;|lt;|gt;)/g, '&amp;');

        // Support <b>, <i>, and <u>, escape anything else
        // so it displays as raw markup.
        _text = _text.replace(/<(?!\/?[biu]>)/g, '&lt;');

        try {
            Pango.parse_markup(_text, -1, '');
            return _text;
        } catch (e) {}
    }

    // !allowMarkup, or invalid markup
    return GLib.markup_escape_text(text, -1);
}

/** creates a new URLHighlighter
 *
 * We create the top-level actor {@link #actor}, a St.Label, which will hold
 * `text` with the URLs marked up.
 *
 * We call `this.setMarkup(text, allowMarkup)` to find the URLs in the
 * text and highlight them.
 *
 * Then we connect up a number of signals of {@link UrlHighlighter#actor}:
 *
 * * 'style-changed': if the 'link-color' attribute has changed we update the
 * colour of the URLs.
 * * 'button-press-event': if the user clicked on a URL in the text we block
 * the event from progressing to the parent {@link Notification}.
 * * 'button-release-event': if the user clicked on a URL we actually launch
 * the URL.
 * * 'motion-event': if the user moves their cursor over a URL their cursor
 * should change to the 'hand' pointer.
 * * 'leave-event': if the user moves their cursor away from a URL their cursor
 * should reset to the normal pointer.
 *
 * @param {string} text - the text we wish to create a URL-highlighted label for.
 * @param {boolean} lineWrap - whether to allow line wrapping, or just use
 * ellipsizing ('...' at the end if too long)
 * @param {boolean} allowMarkup - whether to allow markup in the text.
 * @classdesc
 * ![Example of a {@link URLHighlighter} in action](pics/MessageTrayNotificationTransient.png)
 *
 * This is a utility class that makes a St.Label for a string of text `text`,
 * highlighting any URLs it finds by underlining them and rendering them in
 * a different colour.
 *
 * It also allows the user to click on such a "link" and launches the browser
 * with that link.
 *
 * It does the styling by adding HTML tags `<span foreground="link_colour"><u>URL</u></span>`
 * around detected URLs, to make them look like clickable URLs.
 *
 * It then connects up a whole bunch of events making sure that when the user's
 * cursor goes over a URL the pointer changes to the clicking "hand" to indicate
 * the URL is clickable. It also handles launching the browser with a URL if
 * it is clicked.
 *
 * Since all the text is just placed into the label {@link #actor} and detected
 * URLs do not get their own labels (all the text is part of one label), we can't actually
 * detect click/hover events for individual URLs. Instead, we detect click/hover
 * events on the label {@link #actor} and then use the mouse's position to determine
 * if it is currently over a URL (and which one it is over).
 *
 * {@link #actor} is the St.Label containing the URL-highlighted text.
 *
 * The banner of a {@link Notification} and any text
 * added via {@link Notification#addBody} are passed through URLHighlighters.
 * @class
 */
function URLHighlighter(text, lineWrap, allowMarkup) {
    this._init(text, lineWrap, allowMarkup);
}

URLHighlighter.prototype = {
    _init: function(text, lineWrap, allowMarkup) {
        if (!text)
            text = '';
        /** Top-level actor for the URL highlighter. It is reactive and
         * has style class 'url-highlighter'.
         * @type {St.Label} */
        this.actor = new St.Label({ reactive: true, style_class: 'url-highlighter' });
        this._linkColor = '#ccccff';
        this.actor.connect('style-changed', Lang.bind(this, function() {
            let [hasColor, color] = this.actor.get_theme_node().lookup_color('link-color', false);
            if (hasColor) {
                let linkColor = color.to_string().substr(0, 7);
                if (linkColor != this._linkColor) {
                    this._linkColor = linkColor;
                    this._highlightUrls();
                }
            }
        }));
        if (lineWrap) {
            this.actor.clutter_text.line_wrap = true;
            this.actor.clutter_text.line_wrap_mode = Pango.WrapMode.WORD_CHAR;
            this.actor.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        }

        this.setMarkup(text, allowMarkup);
        this.actor.connect('button-press-event', Lang.bind(this, function(actor, event) {
            // Don't try to URL highlight when invisible.
            // The MessageTray doesn't actually hide us, so
            // we need to check for paint opacities as well.
            if (!actor.visible || actor.get_paint_opacity() == 0)
                return false;

            // Keep Notification.actor from seeing this and taking
            // a pointer grab, which would block our button-release-event
            // handler, if an URL is clicked
            return this._findUrlAtPos(event) != -1;
        }));
        this.actor.connect('button-release-event', Lang.bind(this, function (actor, event) {
            if (!actor.visible || actor.get_paint_opacity() == 0)
                return false;

            let urlId = this._findUrlAtPos(event);
            if (urlId != -1) {
                let url = this._urls[urlId].url;
                if (url.indexOf(':') == -1)
                    url = 'http://' + url;
                try {
                    Gio.app_info_launch_default_for_uri(url, global.create_app_launch_context());
                    return true;
                } catch (e) {
                    // TODO: remove this after gnome 3 release
                    Util.spawn(['gvfs-open', url]);
                    return true;
                }
            }
            return false;
        }));
        this.actor.connect('motion-event', Lang.bind(this, function(actor, event) {
            if (!actor.visible || actor.get_paint_opacity() == 0)
                return false;

            let urlId = this._findUrlAtPos(event);
            if (urlId != -1 && !this._cursorChanged) {
                global.set_cursor(Shell.Cursor.POINTING_HAND);
                this._cursorChanged = true;
            } else if (urlId == -1) {
                global.unset_cursor();
                this._cursorChanged = false;
            }
            return false;
        }));
        this.actor.connect('leave-event', Lang.bind(this, function() {
            if (!this.actor.visible || this.actor.get_paint_opacity() == 0)
                return;

            if (this._cursorChanged) {
                this._cursorChanged = false;
                global.unset_cursor();
            }
        }));
    },

    /** Sets the text of the label ({@link #actor}) to `text` (after sanitising
     * through {@link _fixMarkup} with `allowMarkup`), and calls
     * {@link Util.findUrls} and {@link #_highlightUrls} to find and highlight
     * any URLs contained wihtin.
     * @inheritparams URLHighlighter
     */
    setMarkup: function(text, allowMarkup) {
        text = text ? _fixMarkup(text, allowMarkup) : '';
        /** The text to find the URLs of.
         * @type {string} */
        this._text = text;

        this.actor.clutter_text.set_markup(text);
        /* clutter_text.text contain text without markup */
        this._urls = Util.findUrls(this.actor.clutter_text.text);
        this._highlightUrls();
    },

    /** Highlights URLs within the text. Uses {@link Util.findUrls} to locate
     * the URLs, and then for each URL it finds, adds some markup to it to
     * underline the text and change its colour according to the CSS style class.
     *
     * (`<span foreground="FOREGROUND_COLOUR"><u>url</u></span>`).
     */
    _highlightUrls: function() {
        // text here contain markup
        let urls = Util.findUrls(this._text);
        let markup = '';
        let pos = 0;
        for (let i = 0; i < urls.length; i++) {
            let url = urls[i];
            let str = this._text.substr(pos, url.pos - pos);
            markup += str + '<span foreground="' + this._linkColor + '"><u>' + url.url + '</u></span>';
            pos = url.pos + url.url.length;
        }
        markup += this._text.substr(pos);
        this.actor.clutter_text.set_markup(markup);
    },

    /** Finds the URL at the position specified by `event.get_coords()` (if any).
     *
     * @param {Clutter.Event} event - event whose coordinates we will use to
     * see if there are any URLs under.
     * @returns {number} the index of the URL in the list of URLs found for the
     * text (i.e. 0 is the 0th URL found, ...). -1 if the event did not occur
     * over any URL.
     */
    _findUrlAtPos: function(event) {
        let success;
        let [x, y] = event.get_coords();
        [success, x, y] = this.actor.transform_stage_point(x, y);
        let find_pos = -1;
        for (let i = 0; i < this.actor.clutter_text.text.length; i++) {
            let [success, px, py, line_height] = this.actor.clutter_text.position_to_coords(i);
            if (py > y || py + line_height < y || x < px)
                continue;
            find_pos = i;
        }
        if (find_pos != -1) {
            for (let i = 0; i < this._urls.length; i++)
            if (find_pos >= this._urls[i].pos &&
                this._urls[i].pos + this._urls[i].url.length > find_pos)
                return i;
        }
        return -1;
    }
};

/** creates a new FocusGrabber
 * @classdesc
 * This is a helper class used by the message tray to help grab focus
 * to various elements of it - the summary item right- and left-click popup menus,
 * or the notification that is currently being notified.
 *
 * I'm not entirely sure why it is necessary but that's how it's used...
 * @see MessageTray#_focusGrabber
 *
 * This is how it is used in the message tray:
 *
 * When the focus grabber grabs focus and some sort of menu is being shown
 * from a summary item (right click menu or notification stack), we lock
 * the message tray.
 *
 * If the user has clicked on a summary item (so we're showing either the
 * notification stack or right click menu for it) and they then click outside,
 * we close the menu and ungrab focus.
 *
 * If the user presses Escape while focus is being grabbed, we close the
 * tray ({@link MessageTray#_escapeTray}).
 *
 * Whenever a notification is expanded we grab focus to its actor.
 * Whenever 
 * @class
 */
function FocusGrabber() {
    this._init();
}

FocusGrabber.prototype = {
    _init: function() {
        this.actor = null;

        this._hasFocus = false;
        // We use this._prevFocusedWindow and this._prevKeyFocusActor to return the
        // focus where it previously belonged after a focus grab, unless the user
        // has explicitly changed that.
        this._prevFocusedWindow = null;
        this._prevKeyFocusActor = null;

        this._focusActorChangedId = 0;
        this._stageInputModeChangedId = 0;
        this._capturedEventId = 0;
        this._togglingFocusGrabMode = false;

        Main.overview.connect('showing', Lang.bind(this,
            function() {
                this._toggleFocusGrabMode();
            }));
        Main.overview.connect('hidden', Lang.bind(this,
            function() {
                this._toggleFocusGrabMode();
            }));
    },

    /** Causes focus to be given to `actor`.
     * 1. Store the actor to grab focus for in {@link #actor}.
     * 2. Set the global stage's input mode to FOCUSED. This is the same as
     * normal, except that the stage has keyboard focus. Once the stage
     * loses the keyboard focus (for example the user clicks on a window),
     * the input mode changes back to NORMAL. If this happens, we ungrab focus.
     * 3. Listen to all events via 'captured-event' to detect clicks outside
     * the focused actor without consuming them.
     * 4. Give focus to `actor` (using `actor.navigate_focus`).
     * @param {Clutter.Actor} actor - the actor to grab focus to.
     * 5. Emit 'focus-grabbed'.
     * @fires .focus-grabbed
     */
    grabFocus: function(actor) {
        if (this._hasFocus)
            return;

        this.actor = actor;

        this._prevFocusedWindow = global.display.focus_window;
        this._prevKeyFocusActor = global.stage.get_key_focus();

        if (global.stage_input_mode == Shell.StageInputMode.NONREACTIVE ||
            global.stage_input_mode == Shell.StageInputMode.NORMAL)
            global.set_stage_input_mode(Shell.StageInputMode.FOCUSED);

        // Use captured-event to notice clicks outside the focused actor
        // without consuming them.
        this._capturedEventId = global.stage.connect('captured-event', Lang.bind(this, this._onCapturedEvent));

        this._stageInputModeChangedId = global.connect('notify::stage-input-mode', Lang.bind(this, this._stageInputModeChanged));
        this._focusActorChangedId = global.stage.connect('notify::key-focus', Lang.bind(this, this._focusActorChanged));

        this._hasFocus = true;

        this.actor.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
        this.emit('focus-grabbed');
    },

    /** Emitted when key focus changes from the original actor.
     * If the new key focus is not in the original actor {@link #actor},
     * we ungrab focus.
     * @see #ungrabFocus
     */
    _focusActorChanged: function() {
        let focusedActor = global.stage.get_key_focus();
        if (!focusedActor || !this.actor.contains(focusedActor)) {
            this._prevKeyFocusActor = null;
            this.ungrabFocus();
        }
    },

    /** Callback when global.stage's input mode changes (for example
     * back to NORMAL or to NONREACTIVE).
     *
     * This causes focus to be released ({@link #ungrabFocus}).
     */
    _stageInputModeChanged: function() {
        this.ungrabFocus();
    },

    /** Callback when an event is captured while we are in a focus grab.
     *
     * If the event was a button press *not* clicked on any child of
     * {@link #actor} or the on-screen keyboard, we emit signal
     * ['button-pressed']{@link .button-pressed} with the target actor.
     * We then return false, i.e. we allow the event to continue on to the
     * next handler.
     *
     * If the event was the user pressing the Escape key, we emit
     * ['escape-pressed']{@link escape-pressed}.
     * @fires .button-pressed
     * @fires .escape-pressed
     */
    _onCapturedEvent: function(actor, event) {
        let source = event.get_source();
        switch (event.type()) {
            case Clutter.EventType.BUTTON_PRESS:
                if (!this.actor.contains(source) &&
                    !Main.layoutManager.keyboardBox.contains(source))
                    this.emit('button-pressed', source);
                break;
            case Clutter.EventType.KEY_PRESS:
                let symbol = event.get_key_symbol();
                if (symbol == Clutter.Escape) {
                    this.emit('escape-pressed');
                    return true;
                }
                break;
        }

        return false;
    },

    /** End a focus grab.
     * We disconnect the signals we were listening to and emit
     * ['focus-ungrabbed']{@link .focus-ungrabbed}.
     *
     * We also restore window focus to the window that had focus before the grab,
     * and restore key focus to whichever actor had it before the grab.
     * @see grabFocus
     * @fires .focus-ungrabbed
     */
    ungrabFocus: function() {
        if (!this._hasFocus)
            return;

        if (this._focusActorChangedId > 0) {
            global.stage.disconnect(this._focusActorChangedId);
            this._focusActorChangedId = 0;
        }

        if (this._stageInputModeChangedId) {
            global.disconnect(this._stageInputModeChangedId);
            this._stageInputModeChangedId = 0;
        }

        if (this._capturedEventId > 0) {
            global.stage.disconnect(this._capturedEventId);
            this._capturedEventId = 0;
        }

        this._hasFocus = false;
        this.emit('focus-ungrabbed');

        if (this._prevFocusedWindow && !global.display.focus_window) {
            global.display.set_input_focus_window(this._prevFocusedWindow, false, global.get_current_time());
            this._prevFocusedWindow = null;
        }
        if (this._prevKeyFocusActor) {
            global.stage.set_key_focus(this._prevKeyFocusActor);
            this._prevKeyFocusActor = null;
        } else {
            // We don't want to keep any actor inside the previously focused actor focused.
            let focusedActor = global.stage.get_key_focus();
            if (focusedActor && this.actor.contains(focusedActor))
                global.stage.set_key_focus(null);
        }
        if (!this._togglingFocusGrabMode)
            this.actor = null;
    },

    /** Ungrabs and regrabs focus. Called whenever the overview is
     * hidden or shown.
     *
     * Because we grab focus differently in the overview
     * and in the main view, we need to change how it is
     * done when we move between the two.
     */
    _toggleFocusGrabMode: function() {
        if (this._hasFocus) {
            this._togglingFocusGrabMode = true;
            this.ungrabFocus();
            this.grabFocus(this.actor);
            this._togglingFocusGrabMode = false;
        }
    }
}
Signals.addSignalMethods(FocusGrabber.prototype);
/** Emitted when the focus grabber successfully grabs focus of an actor.
 * Use `focusGrabber.actor` to get the actor we grabbed.
 * @param {MessageTray.FocusGrabber} focusGrabber - the focus grabber instance
 * that emitted the signal.
 * @event
 * @name focus-grabbed
 * @memberof FocusGrabber */
/** Emitted when the focus grabber successfully grabs focus of an actor.
 * @event
 * @name focus-ungrabbed
 * @memberof FocusGrabber */
/** Emitted when the user presses the Escape key while a focus grab is current.
 * @event
 * @name escape-pressed
 * @memberof FocusGrabber */
/** Emitted when the clicks a mouse button while a focus grab is current
 * (and the click was not on the on-screen keyboard or any child of
 * the original focus-grab actor).
 * @param {MessageTray.FocusGrabber} focusGrabber - the focus grabber instance
 * that emitted the signal.
 * @param {Clutter.Actor} source - the actor clicked on.
 * @event
 * @name button-pressed
 * @memberof FocusGrabber */

// TODO: diagram with banner, body, source, summaryItem anatomy.
/** Creates a new Notification.
 *
 * In the banner mode, the notification
 * will show an icon, `title` (in bold) and `banner`, all on a single
 * line (with `banner` ellipsized if necessary).
 *
 * The notification will be expandable if either it has additional
 * elements that were added to it or if the `banner` text did not
 * fit fully in the banner mode. When the notification is expanded,
 * the `banner` text from the top line is always removed. The complete
 * `banner` text is added as the first element in the content section,
 * unless 'customContent' parameter with the value 'true' is specified
 * in `params`.
 *
 * Additional notification content can be added with {@link #addActor} and
 * {@link #addBody} methods. The notification content is put inside a
 * scrollview, so if it gets too tall, the notification will scroll
 * rather than continue to grow. In addition to this main content
 * area, there is also a single-row action area, which is not
 * scrolled and can contain a single actor. The action area can
 * be set by calling {@link #setActionArea} method. There is also a
 * convenience method {@link #addButton} for adding a button to the action
 * area.
 * @param {MessageTray.Source} source - the notification's Source
 * @param {string} title - the title
 * @param {?string} banner - the banner text, if any
 * @param {Object} [params] - optional additional params
 * @param {boolean} [params.customContent=false] - if `true`,
 * then `banner` will not be shown in the body of the notification when the
 * notification is expanded and calls to {@link #update} will not
 * clear the content unless 'clear' parameter with value `true` is explicitly
 * specified.
 * @param {string} [params.body=null] - if present, that text will be added to the
 * content area (as with {@link #addBody}.
 * @param {Clutter.Actor} [params.icon=null] -
 * By default, the icon shown is created by calling
 * [`source.createNotificationIcon()`]{@link Source#createNotificationIcon}.
 * However, if this parameter is present, the passed in icon will be used instead.
 * @param {boolean} [params.titleMarkup=false] - if `true`, then the corresponding
 * element is assumed to use pango markup. If `false`, then all markup-looking
 * characters (ampersand, angle brackets) will appear literally in the output.
 * @param {boolean} [params.bannerMarkup=false] - As for `titleMarkup` but in the banner.
 * @param {boolean} [params.bodyMarkup=false] - As for `titleMarkup` but for the body text.
 * @param {boolean} [params.clear=false] - if `true`, then the content and action area
 * of the notification will be cleared.
 * The content area is also always cleared if `customContent` is false
 * because it might contain the `banner` that didn't fit in the banner mode.
 *
 * @see #update
 * @classdesc 
 * ![A transient {@link Notification} in banner mode](pics/NotificationBannerMode.png)
 * ![A transient {@link Notification} in expanded mode](pics/NotificationExpandedMode.png)
 * ![A resident {@link Notification} cannot be dismissed; e.g. Rhythmbox and Nautilus file transfer notifications](pics/Notification.Resident.png)
 * ![A default (persistent) {@link Notification} from a {@link SystemNotificationSource}; clicking on the notification will dismiss it](pics/Notification.Persistent.png)
 *
 *
 * Logically, notifications fall into one of three categories, defined by
 * calls to {@link #setResident} and {@link #setTransient}.
 *
 * * Persistent: the default. Non-resident, and non-transient. This means the
 * notification comes from a Source/SummaryItem in the message tray, and can
 * be dismissed by clicking on it/its buttons. Notifications are persistent
 * by default.
 * * Resident: Use [`setResident(true)`]{@link #setResident}. A resident
 * notification cannot be killed or removed by clicking on it.
 * Examples of these are rhythmbox and nautilus file transfer notifications.
 * {@link TelepathyClient.ChatNotification}s and
 * {@link AutorunManager.AutorunResidentNotification}s are more examples of these.
 * * Transient: Use [`setTransient(true)`]{@link #setTransient}.
 * A transient notification has *no summary icon in the message tray at all*.
 * They pop up on your screen and once they're gone you can't get them back
 * Notifications spawned by {@link Overview.Overview#setMessage} and
 * {@link Main.notify} are examples.
 *
 * In the banner mode, the notification
 * will show an icon, `title` (in bold) and `banner`, all on a single
 * line (with `banner` ellipsized if necessary).
 *
 * The notification will be expandable if either it has additional
 * elements that were added to it or if the `banner` text did not
 * fit fully in the banner mode. When the notification is expanded,
 * the `banner` text from the top line is always removed. The complete
 * `banner` text is added as the first element in the content section,
 * unless 'customContent' parameter with the value 'true' is specified
 * in `params`.
 *
 * Additional notification content can be added with {@link #addActor} and
 * {@link #addBody} methods. The notification content is put inside a
 * scrollview, so if it gets too tall, the notification will scroll
 * rather than continue to grow. In addition to this main content
 * area, there is also a single-row action area, which is not
 * scrolled and can contain a single actor. The action area can
 * be set by calling {@link #setActionArea} method. There is also a
 * convenience method {@link #addButton} for adding a button to the action
 * area.
 * @example
 * // example of creating a default (i.e. "persistent") notification.
 * const MessageTray = imports.ui.messageTray;
 * // step 1. Create a Source (subclass that implements createNotificationIcon).
 * // We'll use a SystemNotificationSource as an example
 * let source = new MessageTray.SystemNotificationSource();
 *
 * // step 2. Create the notification
 * let not = new MessageTray.Notification(source, "notification title",
 *                                        "summary of the notification");
*
 * // step 3. add the Source to the message tray
 * Main.messageTray.add(source);
 *
 * // Step 4. Add the notification to the source, either by using
 * // source.notify(not) or source.pushNotification(not).
 * // The former additionally pops the notification up in the middle of the
 * // screen.
 * source.notify(not);
 *
 * // This pops up a little notification from the bottom of the screen (if you
 * // used source.notify), and also adds this source/notification to the message
 * // tray:
 * ![Notification created by our example](pics/Notification.Persistent.png)
 * @see #setActionArea
 * @see #addButton
 * @see #addBody
 * @see #addActor
 * @class
 * @todo anatomy of a notification (icon, banner, body)
 */
function Notification(source, title, banner, params) {
    this._init(source, title, banner, params);
}

Notification.prototype = {
    /** Handy constant - size of a "large image" in a chat?
     * If you're going to use {@link #setImage}, make the image this size.
     * @TODO: an example/screenshot
     */
    IMAGE_SIZE: 125,

    _init: function(source, title, banner, params) {
        /** The Source that this notification comes from.
         * @type {MessageTray.Source} */
        this.source = source;
        /** The title of the notification.
         * @type {string} */
        this.title = title;
        /** The urgency of the notification
         * @type {MessageTray.urgency} */
        this.urgency = Urgency.NORMAL;
        /** Whether the notification is resident or not.
         *
         * A resident notification can't be removed or destroyed.
         * For example, the Rhythmbox notification showing the current song
         * and Nautilus file transfer notifications are resident.
         *
         * (A non-resident notification may be dismissed/destroyed by clicking
         * on it or one of its buttons).
         * @todo make sure I haven't used them as opposites before), and what about
         * source.isTransient vs notification.isTransient?
         * @default
         * @type {boolean} */
        this.resident = false;
        // 'transient' is a reserved keyword in JS, so we have to use an alternate variable name
        /** Whether the notification is transient or not.
         *
         * A transient notification's {@link SummaryItem}
         * **does not appear** in the message tray - the notification pops
         * up and if you miss it it's gone forever.
         *
         * Notifications from {@link Main.notify} are transient, for example.
         */
        this.isTransient = false;
        /** Whether the notification is currently expanded. */
        this.expanded = false;
        this._destroyed = false;
        this._useActionIcons = false;
        this._customContent = false;
        this._bannerBodyText = null;
        this._bannerBodyMarkup = false;
        this._titleFitsInBannerMode = true;
        this._titleDirection = St.TextDirection.NONE;
        this._spacing = 0;
        this._scrollPolicy = Gtk.PolicyType.AUTOMATIC;
        this._imageBin = null;

        source.connect('destroy', Lang.bind(this,
            function (source, reason) {
                this.destroy(reason);
            }));

        /** Top-level actor for a notification is a St.Button (mainly for the
         * convenient 'clicked' signal).
         *
         * The actual thing that we pack our contents into is a St.Table
         * {@link #_table}.
         * @type {St.Button} */
        this.actor = new St.Button();
        this.actor._delegate = this;
        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        /** St.Table that we pack the notification contents into.
         * Name is 'notification'.
         * @type {St.Table} */
        this._table = new St.Table({ name: 'notification',
                                     reactive: true });
        this._table.connect('style-changed', Lang.bind(this, this._styleChanged));
        this.actor.set_child(this._table);

        this._buttonFocusManager = St.FocusManager.get_for_stage(global.stage);

        /** Actor that holds the notification banner (packed into {@link #_table}).
         *
         * The first line should have the title, followed by the
         * banner text, but ellipsized if they won't both fit. We can't
         * make St.Table or St.BoxLayout do this the way we want (don't
         * show banner at all if title needs to be ellipsized), so we
         * use Shell.GenericContainer.
         * @type {Shell.GenericContainer} */
        this._bannerBox = new Shell.GenericContainer();
        this._bannerBox.connect('get-preferred-width', Lang.bind(this, this._bannerBoxGetPreferredWidth));
        this._bannerBox.connect('get-preferred-height', Lang.bind(this, this._bannerBoxGetPreferredHeight));
        this._bannerBox.connect('allocate', Lang.bind(this, this._bannerBoxAllocate));
        this._table.add(this._bannerBox, { row: 0,
                                           col: 1,
                                           col_span: 2,
                                           x_expand: false,
                                           y_expand: false,
                                           y_fill: false });

        // This is an empty cell that overlaps with this._bannerBox cell to ensure
        // that this._bannerBox cell expands horizontally, while not forcing the
        // this._imageBin that is also in col: 2 to expand horizontally.
        this._table.add(new St.Bin(), { row: 0,
                                        col: 2,
                                        y_expand: false,
                                        y_fill: false });

        this._titleLabel = new St.Label();
        this._bannerBox.add_actor(this._titleLabel);
        this._bannerUrlHighlighter = new URLHighlighter();
        this._bannerLabel = this._bannerUrlHighlighter.actor;
        this._bannerBox.add_actor(this._bannerLabel);

        this.update(title, banner, params);
    },

    /**
     * Updates the notification by regenerating its icon and updating
     * the title/banner. If `params.clear` is `true`, it will also
     * remove any additional actors/action buttons previously added.
     *
     * The banner is passed through a {@link URLHighlighter} to create a St.Label
     * for it that has URLs highlighted and clickable.
     * @param {string} title - the new title
     * @param {string} banner - the new banner
     * @inheritparams Notification
     */
    update: function(title, banner, params) {
        params = Params.parse(params, { customContent: false,
                                        body: null,
                                        icon: null,
                                        titleMarkup: false,
                                        bannerMarkup: false,
                                        bodyMarkup: false,
                                        clear: false });

        this._customContent = params.customContent;

        let oldFocus = global.stage.key_focus;

        if (this._icon && (params.icon || params.clear)) {
            this._icon.destroy();
            this._icon = null;
        }

        // We always clear the content area if we don't have custom
        // content because it might contain the @banner that didn't
        // fit in the banner mode.
        if (this._scrollArea && (!this._customContent || params.clear)) {
            if (oldFocus && this._scrollArea.contains(oldFocus))
                this.actor.grab_key_focus();

            this._scrollArea.destroy();
            this._scrollArea = null;
            this._contentArea = null;
        }
        if (this._actionArea && params.clear) {
            if (oldFocus && this._actionArea.contains(oldFocus))
                this.actor.grab_key_focus();

            this._actionArea.destroy();
            this._actionArea = null;
            this._buttonBox = null;
        }
        if (this._imageBin && params.clear)
            this.unsetImage();

        if (!this._scrollArea && !this._actionArea && !this._imageBin)
            this._table.remove_style_class_name('multi-line-notification');

        if (!this._icon) {
            this._icon = params.icon || this.source.createNotificationIcon();
            this._table.add(this._icon, { row: 0,
                                          col: 0,
                                          x_expand: false,
                                          y_expand: false,
                                          y_fill: false,
                                          y_align: St.Align.START });
        }

        this.title = title;
        title = title ? _fixMarkup(title.replace(/\n/g, ' '), params.titleMarkup) : '';
        this._titleLabel.clutter_text.set_markup('<b>' + title + '</b>');

        if (Pango.find_base_dir(title, -1) == Pango.Direction.RTL)
            this._titleDirection = St.TextDirection.RTL;
        else
            this._titleDirection = St.TextDirection.LTR;

        // Let the title's text direction control the overall direction
        // of the notification - in case where different scripts are used
        // in the notification, this is the right thing for the icon, and
        // arguably for action buttons as well. Labels other than the title
        // will be allocated at the available width, so that their alignment
        // is done correctly automatically.
        this._table.set_direction(this._titleDirection);

        // Unless the notification has custom content, we save this._bannerBodyText
        // to add it to the content of the notification if the notification is
        // expandable due to other elements in its content area or due to the banner
        // not fitting fully in the single-line mode.
        this._bannerBodyText = this._customContent ? null : banner;
        this._bannerBodyMarkup = params.bannerMarkup;

        banner = banner ? banner.replace(/\n/g, '  ') : '';

        this._bannerUrlHighlighter.setMarkup(banner, params.bannerMarkup);
        this._bannerLabel.queue_relayout();

        // Add the bannerBody now if we know for sure we'll need it
        if (this._bannerBodyText && this._bannerBodyText.indexOf('\n') > -1)
            this._addBannerBody();

        if (params.body)
            this.addBody(params.body, params.bodyMarkup);
        this._updated();
    },

    /** Sets whether the notification's icon is visible or not (default yes).
     * @param {boolean} visible - whether the notification's icon is visible or not.
     */
    setIconVisible: function(visible) {
        this._icon.visible = visible;
    },

    /** Enable scrolling on the notification? (default yes)
     * @param {boolean} enableScrolling - whether to enable scrolling.
     */
    enableScrolling: function(enableScrolling) {
        this._scrollPolicy = enableScrolling ? Gtk.PolicyType.AUTOMATIC : Gtk.PolicyType.NEVER;
        if (this._scrollArea)
            this._scrollArea.vscrollbar_policy = this._scrollPolicy;
    },

    /** Creates a St.ScrollView to add to {@link #_table} allowing the notification's
     * contents to be scrolled.
     *
     * {@link #_table} gets additional style class 'multi-line-notification' and
     * the scrollview gets style 'notification-scrollview'.
     */
    _createScrollArea: function() {
        this._table.add_style_class_name('multi-line-notification');
        this._scrollArea = new St.ScrollView({ name: 'notification-scrollview',
                                               vscrollbar_policy: this._scrollPolicy,
                                               hscrollbar_policy: Gtk.PolicyType.NEVER,
                                               style_class: 'vfade' });
        this._table.add(this._scrollArea, { row: 1,
                                            col: 2 });
        this._updateLastColumnSettings();
        this._contentArea = new St.BoxLayout({ name: 'notification-body',
                                               vertical: true });
        this._scrollArea.add_actor(this._contentArea);
        // If we know the notification will be expandable, we need to add
        // the banner text to the body as the first element.
        this._addBannerBody();
    },

    /**
     * Appends `actor` to the notification's body.
     * @param {Clutter.Actor|St.Widget} actor - actor to add to the body of the notification
     *
     * @param {Object} [style] - style to use when adding the child to the
     * container, same as if you were adding to any St container
     * (for example see [St.Bin](http://developer.gnome.org/st/stable/StBin.html#StBin--x-align)).
     * @param {boolean} [style.x_fill=false] - whether to fill the container in the x direction
     * @param {boolean} [style.x_align=St.Align.MIDDLE] - how to align horizontally within its allocation
     * @param {boolean} [style.y_fill=false] - whether to fill the container in the y direction
     * @param {boolean} [style.y_align=St.Align.MIDDLE] - how to align vertically within its allocation
     * @param {boolean} [style.expand=false] - whether to allocate the child *extra* space (somehow
     * different to `x_fill` and `y_fill`? If the fill option is not set but
     * expand is, then the alignment properties can be used.)
     */
    addActor: function(actor, style) {
        if (!this._scrollArea) {
            this._createScrollArea();
        }

        this._contentArea.add(actor, style ? style : {});
        this._updated();
    },

    /**
     * Adds a multi-line label containing `text` to the notification (a
     * {@link URLHighlighter} is made for `text`).
     * @param {string} text - the text
     * @param {boolean} markup - `true` if `text` contains pango markup
     * @inheritparams #addActor
     * @returns {St.Label} the newly-added label (actually {@link URLHighlighter#actor}).
     */
    addBody: function(text, markup, style) {
        let label = new URLHighlighter(text, true, markup);

        this.addActor(label.actor, style);
        return label.actor;
    },

    /** Adds the `banner` text (from the constructor) to the notification.
     * Will only actually add if `params.customContent` is `false` (in the
     * constructor).
     * @see #addBody */
    _addBannerBody: function() {
        if (this._bannerBodyText) {
            let text = this._bannerBodyText;
            this._bannerBodyText = null;
            this.addBody(text, this._bannerBodyMarkup);
        }
    },

    /**
     * Scrolls the content area (if scrollable) to the indicated edge
     * @param {St.Side} side - St.Side.TOP or St.Side.BOTTOM.
     */
    scrollTo: function(side) {
        let adjustment = this._scrollArea.vscroll.adjustment;
        if (side == St.Side.TOP)
            adjustment.value = adjustment.lower;
        else if (side == St.Side.BOTTOM)
            adjustment.value = adjustment.upper;
    },

    /**
     * Puts `actor` into the action area of the notification, replacing
     * the previous contents
     * @param {Clutter.Actor} actor - the actor
     * @param {Object} [props] - St.Table child properties (I took these and
     * the documentation from the source - `src/st/st-table-child.c` as the
     * documentation doesn't appear elsewhere).
     * @param {number} [col=2] - the column to put the child in
     * @param {number} [props.row=2] - the row to put the child in
     * @param {number} [props.col_span=1] - the column span of the child
     * @param {number} [props.row_span=1] - the row span of the child
     * @param {boolean} [props.x_expand=true] whether the child should receive
     * priority when the container is allocating spare space horizontally
     * @param {boolean} [props.y_expand=true] whether the child should receive
     * priority when the container is allocating spare space horizontally
     * @param {boolean} [props.x_fill=true] - whether to allocate the child its
     * entire available space, or to squash and align it.
     * @param {boolean} [props.x_align=St.Align.MIDDLE] - how to align horizontally in the cell
     * @param {boolean} [props.y_fill=true] - whether to allocate the child its
     * entire available space, or to squash and align it.
     * @param {boolean} [props.y_align=St.Align.MIDDLE] - how to align vertically in the cell
     * @param {boolean} [props.allocate_hidden=true] - whether th child should be
     * allocated even if it is hidden
     */
    setActionArea: function(actor, props) {
        if (this._actionArea) {
            this._actionArea.destroy();
            this._actionArea = null;
            if (this._buttonBox)
                this._buttonBox = null;
        } else {
            this._addBannerBody();
        }
        this._actionArea = actor;

        if (!props)
            props = {};
        props.row = 2;
        props.col = 2;

        this._table.add_style_class_name('multi-line-notification');
        this._table.add(this._actionArea, props);
        this._updateLastColumnSettings();
        this._updated();
    },

    /** Internal function. Updates the column within {@link #_table} of various
     * parts of the notification
     * according to whether the notification has an image set.
     * @see #setImage
     */
    _updateLastColumnSettings: function() {
        if (this._scrollArea)
            this._table.child_set(this._scrollArea, { col: this._imageBin ? 2 : 1,
                                                      col_span: this._imageBin ? 1 : 2 });
        if (this._actionArea)
            this._table.child_set(this._actionArea, { col: this._imageBin ? 2 : 1,
                                                      col_span: this._imageBin ? 1 : 2 });
    },

    /** Adds an image to the notification. Use size {@link #IMAGE_SIZE} for
     * the image.
     *
     * This adds style class 'notification-with-image' to the notification,
     * sets the image's opacity to 230, and puts the image in the notificaiton.
     * TODO screenshot
     * @param {Clutter.Actor} image - any actor that can be embedded into a
     * St.Bin.
     * @see #unsetImage
     */
    setImage: function(image) {
        if (this._imageBin)
            this.unsetImage();
        this._imageBin = new St.Bin();
        this._imageBin.child = image;
        this._imageBin.opacity = 230;
        this._table.add_style_class_name('multi-line-notification');
        this._table.add_style_class_name('notification-with-image');
        this._addBannerBody();
        this._updateLastColumnSettings();
        this._table.add(this._imageBin, { row: 1,
                                          col: 1,
                                          row_span: 2,
                                          x_expand: false,
                                          y_expand: false,
                                          x_fill: false,
                                          y_fill: false });
    },

    /** Removes an image added with {@link #setImage} from the notification. */
    unsetImage: function() {
        if (this._imageBin) {
            this._table.remove_style_class_name('notification-with-image');
            this._table.remove_actor(this._imageBin);
            this._imageBin = null;
            this._updateLastColumnSettings();
            if (!this._scrollArea && !this._actionArea)
                this._table.remove_style_class_name('multi-line-notification');
        }
    },

    /**
     * Adds a button with the given `label` to the notification. All
     * action buttons will appear in a single row at the bottom of
     * the notification.
     *
     * If {@link #setUseActionIcons} has been set to `true` prior to this,
     * the buttons will display the *icon* identified by `id`, rather than
     * the text `label`.
     *
     * If the button is clicked, the notification will emit the
     * ['action-invoked']{@link .action-invoked} signal with `id` as a parameter.
     *
     * @param {string} id - the action ID
     * @param {string} label - the label for the action's button
     */
    addButton: function(id, label) {
        if (!this._buttonBox) {

            let box = new St.BoxLayout({ name: 'notification-actions' });
            this.setActionArea(box, { x_expand: false,
                                      y_expand: false,
                                      x_fill: false,
                                      y_fill: false,
                                      x_align: St.Align.END });
            this._buttonBox = box;
        }

        let button = new St.Button({ can_focus: true });

        if (this._useActionIcons && Gtk.IconTheme.get_default().has_icon(id)) {
            button.add_style_class_name('notification-icon-button');
            button.child = new St.Icon({ icon_name: id });
        } else {
            button.add_style_class_name('notification-button');
            button.label = label;
        }

        if (this._buttonBox.get_children().length > 0)
            this._buttonFocusManager.remove_group(this._buttonBox);

        this._buttonBox.add(button);
        this._buttonFocusManager.add_group(this._buttonBox);
        button.connect('clicked', Lang.bind(this, this._onActionInvoked, id));

        this._updated();
    },

    /** Sets the urgency of the notification to `urgency`.
     * @param {MessageTray.Urgency} urgency - urgency to set (LOW, NORMAL,
     * HIGH, CRITICAL).
     */
    setUrgency: function(urgency) {
        this.urgency = urgency;
    },

    /** Sets whether this notification is resident.
     *
     * If a notification is resident, it cannot be dismissed by the usual means
     * (clicking on it); Rhythmbox notifications or Nautilus
     * file transfer notifications or the notification showing connected
     * drives (USB sticks etc) ({@link AutorunManager.AutorunResidentNotification}s)
     * are examples of these.
     *
     * A single Source can trigger/notify both resident and non-resident and
     * transient notifications; clicking on the non-resident one will remove
     * it, while clicking on the resident one does nothing (and transient
     * ones don't appear in the summary item at all).
     * @param {boolean} resident - whether the notification is resident.
     * @see {@link #setTransient}
     * @see {@link Source#setTransient}
     * @see Note in the constructor about transient, resident, and combinations of.
     */
    setResident: function(resident) {
        this.resident = resident;
    },

    /** Sets whether this notification is transient.
     *
     * Transient notifications pop up when you notify them from a {@link Source}
     * like normal notifications, but once they disappear they're gone forever.
     *
     * Notifications created via {@link Main.notify} or 
     * {@link Overview.Overview#setMessage} are examples of these.
     * @param {boolean} isTransient - whether the notification is transient.
     * @see {@link #setResident}
     * @see {@link Source#setTransient}
     * @see Note in the constructor about transient, resident, and combinations of.
     */
    setTransient: function(isTransient) {
        this.isTransient = isTransient;
    },

    /** Whether to display icons on the notification buttons (added with
     * {@link #addButton}) instead of text.
     *
     * For example, Rhythmbox notifications show forward/back/pause/play
     * buttons instead of the text "Forward", "Back", "Pause", "Play".
     * {@link NotificationDaemon.NotificationDaemon} if the notification's
     * hints have 'action-icons' as true.
     * @param {boolean} useIcons - whether to use icons for notification buttons
     * instead of labels.
     * @see #setUseActionIcons
     */
    setUseActionIcons: function(useIcons) {
        this._useActionIcons = useIcons;
    },

    /** Callback when {@link #_table}'s CSS style changes. This updates the
     * spacing between the title and banner text. */
    _styleChanged: function() {
        this._spacing = this._table.get_theme_node().get_length('spacing-columns');
    },

    /** Callback for the 'get-preferred-width' signal of {@link #_bannerBox},
     * which contains a label for the title and another label for the banner.
     *
     * The minimum preferred width is the minimum width required by the title;
     * the natural preferred width is the natural width required by the title
     * and the banner (with spacing in between as specified by the 'spacing-columns'
     * attribute of {@link #_table}'s CSS style class).
     */
    _bannerBoxGetPreferredWidth: function(actor, forHeight, alloc) {
        let [titleMin, titleNat] = this._titleLabel.get_preferred_width(forHeight);
        let [bannerMin, bannerNat] = this._bannerLabel.get_preferred_width(forHeight);

        alloc.min_size = titleMin;
        alloc.natural_size = titleNat + this._spacing + bannerNat;
    },

    /** Callback for the 'get-preferred-height' signal of {@link #_bannerBox},
     * which contains a label for the title and another label for the banner.
     *
     * Allocates the minimum and natural heights requested by the title label.
     */
    _bannerBoxGetPreferredHeight: function(actor, forWidth, alloc) {
        [alloc.min_size, alloc.natural_size] =
            this._titleLabel.get_preferred_height(forWidth);
    },

    /** Callback for the 'allocate' signal of {@link #_bannerBox},
     * which contains a label for the title and another label for the banner.
     *
     * If the title and banner text fit within the notification, then they
     * are allocated with the banner next to the title.
     *
     * If they don't fit (banner is too long), the title is given as much
     * space as it requires, up to the maximum available width. It is ellipsized
     * if it doesn't fit.
     * 
     * Then if there is any remaining space for the banner text, that is placed
     * next to the title and ellipsized if it doesn't fit.
     *
     * If the banner does not completely fit on the same line with the title,
     * it is also added to the body of the notification (so that expanding
     * the notification will show the whole thing).
     *
     * @see #_addBannerBody
     */
    _bannerBoxAllocate: function(actor, box, flags) {
        let availWidth = box.x2 - box.x1;

        let [titleMinW, titleNatW] = this._titleLabel.get_preferred_width(-1);
        let [titleMinH, titleNatH] = this._titleLabel.get_preferred_height(availWidth);
        let [bannerMinW, bannerNatW] = this._bannerLabel.get_preferred_width(availWidth);

        let titleBox = new Clutter.ActorBox();
        let titleBoxW = Math.min(titleNatW, availWidth);
        if (this._titleDirection == St.TextDirection.RTL) {
            titleBox.x1 = availWidth - titleBoxW;
            titleBox.x2 = availWidth;
        } else {
            titleBox.x1 = 0;
            titleBox.x2 = titleBoxW;
        }
        titleBox.y1 = 0;
        titleBox.y2 = titleNatH;
        this._titleLabel.allocate(titleBox, flags);
        this._titleFitsInBannerMode = (titleNatW <= availWidth);

        let bannerFits = true;
        if (titleBoxW + this._spacing > availWidth) {
            this._bannerLabel.opacity = 0;
            bannerFits = false;
        } else {
            let bannerBox = new Clutter.ActorBox();

            if (this._titleDirection == St.TextDirection.RTL) {
                bannerBox.x1 = 0;
                bannerBox.x2 = titleBox.x1 - this._spacing;

                bannerFits = (bannerBox.x2 - bannerNatW >= 0);
            } else {
                bannerBox.x1 = titleBox.x2 + this._spacing;
                bannerBox.x2 = availWidth;

                bannerFits = (bannerBox.x1 + bannerNatW <= availWidth);
            }
            bannerBox.y1 = 0;
            bannerBox.y2 = titleNatH;
            this._bannerLabel.allocate(bannerBox, flags);

            // Make _bannerLabel visible if the entire notification
            // fits on one line, or if the notification is currently
            // unexpanded and only showing one line anyway.
            if (!this.expanded || (bannerFits && this._table.row_count == 1))
                this._bannerLabel.opacity = 255;
        }

        // If the banner doesn't fully fit in the banner box, we possibly need to add the
        // banner to the body. We can't do that from here though since that will force a
        // relayout, so we add it to the main loop.
        if (!bannerFits && this._canExpandContent())
            Meta.later_add(Meta.LaterType.BEFORE_REDRAW,
                           Lang.bind(this,
                                     function() {
                                        if (this._canExpandContent()) {
                                            this._addBannerBody();
                                            this._table.add_style_class_name('multi-line-notification');
                                            this._updated();
                                        }
                                        return false;
                                     }));
    },

    /** Whether this notification can expand (e.g. when hovered over).
     *
     * It can expand if:
     *
     * * the banner didn't fit on a single line with the title so had to be
     * added to the body; OR
     * * {@link #_table} *doesn't* have style class name 'multi-line-notification'
     * (i.e. is not scrollable, has nothing in its action area, has no image?)
     * *and* the title doesn't fit. (this makes no sense?!)
     *
     * I *think* what it means is if the title and/or the banner didn't fit
     * in a single line on the notification then it can be expanded.
     *
     * @returns {boolean} whether this notification can expand.
     */
    _canExpandContent: function() {
        return this._bannerBodyText ||
               (!this._titleFitsInBannerMode && !this._table.has_style_class_name('multi-line-notification'));
    },

    /** Called when the notification is modified (e.g. {@link #update},
     * {@link #addBody}, {@link #addButton}).
     *
     * If the notification is currently expanded, {@link #expand} is called
     * again (with no animation) to refresh the notification.
     * @see #expand
     */
    _updated: function() {
        if (this.expanded)
            this.expand(false);
    },

    /** Expand the notification.
     *
     * This can happen for example where the user moves their mouse over the
     * notification (that has just popped up) and it expands to show the rest
     * of the details (???)
     *
     * * If the title didn't fit in a single line while the notification was
     * unexpanded, the ellipsization (trailing '...') is removed and the title
     * is line-wrapped to fit.
     * * If the title fit and the banner only partially fit, the banner is moved
     * down to its own line (if the notification doesn't have custom content)
     *
     * **Note**: this appears not to control the notification *visually* moving
     * up and down from the bottom of the screen; instead it appears to just
     * adjust the layout of the title/banner within the notification, possibly
     * tweening out the banner.
     *
     * The function that moves the notification up and down happens after this
     * function has completed, and is {@link MessageTray#_onNotificationExpanded}.
     * {@link MessageTray#_updateShowingNotification} also does this.
     *
     * @param {boolean} animate - whether to animate removing the banner from
     * the notification.
     * @see MessageTray#_updateShowingNotification
     * @see MessageTray#_onNotificationExpanded
     */
    expand: function(animate) {
        this.expanded = true;
        // The banner is never shown when the title did not fit, so this
        // can be an if-else statement.
        if (!this._titleFitsInBannerMode) {
            // Remove ellipsization from the title label and make it wrap so that
            // we show the full title when the notification is expanded.
            this._titleLabel.clutter_text.line_wrap = true;
            this._titleLabel.clutter_text.line_wrap_mode = Pango.WrapMode.WORD_CHAR;
            this._titleLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        } else if (this._table.row_count > 1 && this._bannerLabel.opacity != 0) {
            // We always hide the banner if the notification has additional content.
            //
            // We don't need to wrap the banner that doesn't fit the way we wrap the
            // title that doesn't fit because we won't have a notification with
            // row_count=1 that has a banner that doesn't fully fit. We'll either add
            // that banner to the content of the notification in _bannerBoxAllocate()
            // or the notification will have custom content.
            if (animate)
                Tweener.addTween(this._bannerLabel,
                                 { opacity: 0,
                                   time: ANIMATION_TIME,
                                   transition: 'easeOutQuad' });
            else
                this._bannerLabel.opacity = 0;
        }
        this.emit('expanded');
    },

    /** Call when collapsing of a notification is done.
     * We revert the changes of {@link #expand}, i.e. re-ellipsize the title/banner
     * and so on to convert the notification back into banner mode. */
    collapseCompleted: function() {
        if (this._destroyed)
            return;
        this.expanded = false;
        // Make sure we don't line wrap the title, and ellipsize it instead.
        this._titleLabel.clutter_text.line_wrap = false;
        this._titleLabel.clutter_text.ellipsize = Pango.EllipsizeMode.END;
        // Restore banner opacity in case the notification is shown in the
        // banner mode again on update.
        this._bannerLabel.opacity = 255;
        this.emit('collapsed');
    },

    /** Callback when the user clicks on a button on the notification that was
     * added using {@link #addButton}.
     *
     * This emits the {@link .action-invoked} signal with the string `id`
     * telling the handler which button was clicked.
     *
     * The notification is then destroyed and the
     * ['done-displaying']{@link .done-displaying} signal is emitted, *unless*
     * the notification is resident (it is common for resident notifications
     * to update themselves with new information based on the action, so rather
     * than popping up a new notification these ones can update themselves
     * in-place).
     * @param {St.Button} actor - the button clicked
     * @param {number} mouseButtonClicked - what button was clicked (1 left,
     * 2 middle, 3 right).
     * @param {string} id
     * @filres .action-invoked
     * @filres .done-displaying
     */
    _onActionInvoked: function(actor, mouseButtonClicked, id) {
        this.emit('action-invoked', id);
        if (!this.resident) {
            // We don't hide a resident notification when the user invokes one of its actions,
            // because it is common for such notifications to update themselves with new
            // information based on the action. We'd like to display the updated information
            // in place, rather than pop-up a new notification.
            this.emit('done-displaying');
            this.destroy();
        }
    },

    /** Callback when the notification is clicked.
     *
     * We hide all types of notifications once the user clicks on them because
     * the common outcome of clicking should be the relevant window being
     * brought forward and the user's attention switching to the window.
     *
     * This emits the ['clicked']{@link .clicked} signal and the
     * ['done-displaying']{@link .done-displaying} signal.
     *
     * Then the notification is destroyed, *unless* it is resident.
     * @fires .done-displaying
     * @fires .clicked
     */
    _onClicked: function() {
        this.emit('clicked');
        // We hide all types of notifications once the user clicks on them because the common
        // outcome of clicking should be the relevant window being brought forward and the user's
        // attention switching to the window.
        this.emit('done-displaying');
        if (!this.resident)
            this.destroy();
    },

    /** Callback when the notification is destroyed.
     * This emits the ['destroy']{@link .event:destroy} signal with the
     * reason the notification was destroyed (default DISMISSED).
     * @see NotificationDestroyedReason
     * @fires .event:destroy
     */
    _onDestroy: function() {
        if (this._destroyed)
            return;
        this._destroyed = true;
        if (!this._destroyedReason)
            this._destroyedReason = NotificationDestroyedReason.DISMISSED;
        this.emit('destroy', this._destroyedReason);
    },

    /** Destroys this notification.
     *
     * This will trigger {@link #_onDestroy} which will emit the
     * ['destroy']{@link .event:destroy} signal with `reason` (or
     * {@link MessageTray.NotificationDestroyedReason.DISMISSED}
     * if not specified).
     *
     * @param {MessageTray.NotificationDestroyedReason} [reason] - the reason
     * the notification was destroyed.
     * @see #_onDestroy
     */
    destroy: function(reason) {
        this._destroyedReason = reason;
        this.actor.destroy();
        this.actor._delegate = null;
    }
};
Signals.addSignalMethods(Notification.prototype);
/** Emitted when the user selects a button on a {@link Notification} as added
 * by {@link Notification#addButton}.
 *
 * `id` is the ID of the action so that the handler can tell which button
 * the user pressed (this is the same as what was given to {@link Notification#addButton}).
 * @param {MessageTray.Notification} notification - the notification that
 * emitted the signal
 * @param {string} id - the ID of the action invoked (as specified by
 * {@link Notification#addButton}).
 * @name action-invoked
 * @memberof Notification
 * @event
 */
/** Emitted when a notification is done displaying itself.
 * This happens when:
 *
 * * the notification is clicked; or
 * * a button on the notification is clicked (*if* the notification is not resident).
 *
 * @name done-displaying
 * @memberof Notification
 * @event
 */
/** Emitted when the notification itself is clicked.
 * @name clicked
 * @memberof Notification
 * @event
 */
/** Emitted when the notification is destroyed.
 * @param {MessageTray.Notification} notification - the notification that
 * emitted the signal
 * @param {MessageTray.NotificationDestroyedReason} reason -
 * the reason the notification was destroyed (DISMISSED, EXPIRED,
 * SOURCE_CLOSED).
 * @name destroy
 * @memberof Notification
 * @event
 */

/** creates a new Source
 * This sets the source's title and creates its actors.
 * @param {string} title - the title for the Source.
 * @classdesc
 * ![[SummaryItem]{@link SummaryItem} in yellow; [Source]{@link Source} in red](pics/messageTraySummaryItemYellowSourceRed.png)
 *
 * A Source can be thought of as a source of notifications in the message tray.
 * To add notifications to the message tray, one uses {@link #notify} to
 * add the notification to a particular source and then pop it up.
 *
 * Graphically, a Source is just an icon along with a counter displaying the
 * number of notifications currently owned by the Source (outlined in
 * red in the picture above).
 *
 * Logically, the Source handles updating this counter when notifications are
 * added or destroyed, and makes sure the counter only displays when there are
 * at least two notifications for the Source.
 *
 * Related class - the {@link SummaryItem}, which is responsible for
 * displaying the Source's icon/counter along with a label for the Source's
 * title (outlined in yellow above).
 *
 * A subclass must call {@link #_setSummaryIcon} at some point (probably
 * in the constructor) to set the source's icon.
 *
 * The subclass must either implement {@link #createNotificationIcon}
 * or pass the icon explicitly to the [Notification() constructor]{@link Notification}.
 *
 * The subclass may optionally override {@link #open},
 * {@link #_lastNotificationRemoved}, or {@link #handleSummaryClick}.
 *
 * A good example of the most basic implementation of a {@link Source} is the
 * {@link NMMessageTraySource}, a source used for network manager notifications.
 * All it does is set its title to "Network Manager" and its icon to
 * 'network-transmit-receive' (symbolic).
 *
 * A source can also be transient (by default it isn't, use
 * {@link #setTransient} with argument `true` to make it transient).
 * A transient source:
 *
 * * doesn't pop up the message tray when it is added to it (non-transient ones do);
 * * is removed once its summary item is clicked on to toggle its stack of
 * notifications off (regardless of whether those notifications' resident/transient
 * status).
 *
 * As far as I know, the only gnome-shell transient source is the
 * {@link Keyboard.KeyboardSource}, in GNOME 3.4+. This is what shows in the
 * message tray when the user is using the on-screen keyboard and it is
 * currently hidden. It disappears as soon as the user clicks on it to launch
 * the on-screen keyboard.
 *
 * As to why you would use a transient source (and what exactly the purpose
 * of one is) and whether you are meant to use notifications from it (and
 * whether those notifications should be themselves transient),
 * see the following transcript:
 *
 *     mathematicalcoffee: Jasper one last question - what's the point of a transient source if it doesn't show up in the tray? Do notifications notified from a transient source automatically become transient, or are you not meant to notify from a transient source?
 *     Jasper: mathematicalcoffee, notifications need a source
 *     Jasper: mathematicalcoffee, but I actually don't know
 *     Jasper: maybe transient sources and transient notifications are separate
 *     mathematicalcoffee: ah well I'll make a note about it in the documentation. cheers
 *
 * i.e. - don't know! can anyone clarify?
 * @class
 */
function Source(title) {
    this._init(title);
}

Source.prototype = {
    /** Icon size for the relevant summary icon in the message tray. */
    ICON_SIZE: 24,

    _init: function(title) {
        /** Title for the Source. Not used directly in the Source,
         * but used by the {@link SummaryItem} containing this source.
         * @type {string} */
        this.title = title;

        /** Top-level actor.
         * This handles positioning the icon and (if relevant) the counter for
         * the number of notifications on top of it.
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer();
        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));
        this.actor.connect('destroy', Lang.bind(this,
            function() {
                this._actorDestroyed = true;
            }));
        this._actorDestroyed = false;

        /** Displays the number of notifications coming from this
         * source. Nested into a St.Bin with style class 'summary-source-counter'.
         * @type {St.Label} */
        this._counterLabel = new St.Label();
        this._counterBin = new St.Bin({ style_class: 'summary-source-counter',
                                        child: this._counterLabel });
        this._counterBin.hide();

        /** Holds the icon.
         * @type {St.Bin} */
        this._iconBin = new St.Bin({ width: this.ICON_SIZE,
                                     height: this.ICON_SIZE,
                                     x_fill: true,
                                     y_fill: true });

        this.actor.add_actor(this._iconBin);
        this.actor.add_actor(this._counterBin);

        /** Whether the source is transient
         * @type {boolean} */
        this.isTransient = false;
        /** Whether the source is for chat messages
         * @type {boolean} */
        this.isChat = false;

        /** Array of notifications for this source.
         * @type {MessageTray.Notification[]} */
        this.notifications = [];
    },

    /** Callback for the 'get-preferred-width' signal of {@link #actor}.
     * Allocates the width asked for by the icon.
     */
    _getPreferredWidth: function (actor, forHeight, alloc) {
        let [min, nat] = this._iconBin.get_preferred_width(forHeight);
        alloc.min_size = min; alloc.nat_size = nat;
    },

    /** Callback for the 'get-preferred-height' signal of {@link #actor}.
     * Allocates the height asked for by the icon.
     */
    _getPreferredHeight: function (actor, forWidth, alloc) {
        let [min, nat] = this._iconBin.get_preferred_height(forWidth);
        alloc.min_size = min; alloc.nat_size = nat;
    },

    /** Callback for the 'allocate' signal of {@link #actor}.
     * This allocates the icon the entire space available, and allocates
     * the counter of notifications the space it asks for,
     * positioning it over the bottom-right corner of the icon (bottom-left for
     * right-to-left setups). Note that the count is not actually visible
     * unless the number of notifications is greater than 1.
     */
    _allocate: function(actor, box, flags) {
        // the iconBin should fill our entire box
        this._iconBin.allocate(box, flags);

        let childBox = new Clutter.ActorBox();

        let [minWidth, minHeight, naturalWidth, naturalHeight] = this._counterBin.get_preferred_size();
        let direction = this.actor.get_direction();

        if (direction == St.TextDirection.LTR) {
            // allocate on the right in LTR
            childBox.x1 = box.x2 - naturalWidth;
            childBox.x2 = box.x2;
        } else {
            // allocate on the left in RTL
            childBox.x1 = 0;
            childBox.x2 = naturalWidth;
        }

        childBox.y1 = box.y2 - naturalHeight;
        childBox.y2 = box.y2;

        this._counterBin.allocate(childBox, flags);
    },

    /** Sets the count of notifications on {@link #_counterLabel}.
     *
     * Used internally by the Source; the external user shouldn't really use
     * this.
     * @see #_updateCount
     * @param {number|string} count - number of unread notifications
     * @param {boolean} visible - whether to make the count label visible.
     */
    _setCount: function(count, visible) {
        if (isNaN(parseInt(count)))
            throw new Error("Invalid notification count: " + count);

        if (this._actorDestroyed)
            return;

        this._counterBin.visible = visible;
        this._counterLabel.set_text(count.toString());
    },

    /** Updates the count label based on {@link #notifications}.
     * If the number of notifications is at least 2, the label is shown.
     * @see #_setCount
     */
    _updateCount: function() {
        let count = this.notifications.length;
        this._setCount(count, count > 1);
    },

    /** Sets whether the source is transient.
     * 
     * **NOTE**: As far as I can tell, a transient source still appears in the
     * message tray, but clicking on its summary item to hide the list of
     * notifications, or clicking on any of the Source's notifications
     * causes the source and all its notifications to be destroyed,
     * regardless of whether the notifications are resident or not.
     *
     * Contrast this to a non-transient source where clicking on the summary item
     * simply toggles the notification list open and closed, and clicking on
     * the notification will only destroy the source if the notification is
     * destroyed as a result of the click (e.g. it's non-resident) and itz
     * happens to be the last notification for that source.
     *
     * In GNOME 3.2 there are no transient sources (in-built).
     * In GNOME 3.4 and 3.6, the {@link Keyboard.KeyboardSource} (the 'keyboard'
     * icon that appears in the message tray when the on-screen keyboard is
     * enabled but hidden) is transient.
     * @param {boolean} isTransient - whether the source is transient.
     */
    setTransient: function(isTransient) {
        this.isTransient = isTransient;
    },

    /** Sets the title of the source, emitting the 'title-changed' signal to
     * notify the parent {@link SummaryItem} to update the title it displays in
     * the message tray.
     * @param {string} newTitle - the new title for the source.
     * @fires .title-changed
     */
    setTitle: function(newTitle) {
        this.title = newTitle;
        this.emit('title-changed');
    },

    /** Called to create a new icon actor (of size {@link #ICON_SIZE}).
     * Must be overridden by the subclass if you do not pass icons
     * explicitly to the [Notification() constructor]{@link Notification}.
     * @returns {Clutter.Actor} icon to display for that source.
     */
    createNotificationIcon: function() {
        throw new Error('no implementation of createNotificationIcon in ' + this);
    },

    /** Unlike {@link #createNotificationIcon}, this always returns the
     * same actor; there is only one summary icon actor for a Source.
     * @returns {St.Button} {@link #actor} which is the St.Button containing
     * the icon and count of notifications. */
    getSummaryIcon: function() {
        return this.actor;
    },

    /** Adds a notification to this source. This:
     *
     * * fires ['notification-added']{@link .notification-added}
     * * adds the notification to {@link #notifications}
     * * connects to its clicked signal to call {@link #open}
     * * connects to its destroyed signal to update the notification count
     * ({@link #_updateCount})
     * * updates the count of notifications ({@link #_updateCount})
     * @param {MessageTray.Notification} notification - notification to add to
     * this source
     * @fires .notification-added
     */
    pushNotification: function(notification) {
        if (this.notifications.indexOf(notification) < 0) {
            this.notifications.push(notification);
            this.emit('notification-added', notification);
        }

        notification.connect('clicked', Lang.bind(this, this.open));
        notification.connect('destroy', Lang.bind(this,
            function () {
                let index = this.notifications.indexOf(notification);
                if (index < 0)
                    return;

                this.notifications.splice(index, 1);
                if (this.notifications.length == 0)
                    this._lastNotificationRemoved();

                this._updateCount();
            }));

        this._updateCount();
    },

    /** Adds a notification to the source, additionally emitting the 'notify'
     * signal (caught by the message tray in order to ensure that the message
     * tray is open and pop up the notification).
     * @inheritparams #pushNotification
     * @see MessageTray#_onNotify
     * @see #pushNotification
     * @fires .event:notify
     */
    notify: function(notification) {
        this.pushNotification(notification);
        this.emit('notify', notification);
    },

    /** Destroys this source with a reason. Emits 'destroy' with the reason.
     * @param {MessageTray.NotificationDestroyedReason} reason - reason the
     * source was destroyed (EXPIRED, DISMISSED, SOURCE_CLOSED).
     * @fires .event:destroy
     */
    destroy: function(reason) {
        this.emit('destroy', reason);
    },

    /** A subclass can redefine this to "steal" clicks from the
     * summaryitem; Use Clutter.get_current_event() to get the
     * details, return true to prevent the default handling from
     * ocurring.
     * @returns {boolean} `true` if we handled the click and do not wish to
     * propogate it further; `false` otherwise (the message tray will then
     * display the right click menu for that item if it was a right click,
     * or the list of notifications for that item if it was a left click).
     */
    handleSummaryClick: function() {
        return false;
    },

    //// Protected methods ////

    /** The subclass must call this at least once to set the summary icon.
     * Sets the summary icon.
     * @param {Clutter.Actor} icon - the icon for the source.
     */
    _setSummaryIcon: function(icon) {
        if (this._iconBin.child)
            this._iconBin.child.destroy();
        this._iconBin.child = icon;
    },

    /** Default implementation is to do nothing, but subclasses can override.
     *
     * Called whenever the user clicks on the {@link SummaryItem} for
     * this source.
     *
     * For example, {@link SystemNotificationSource} destroys itself as
     * soon as it is opened.
     * @param {MessageTray.Notification} notification - UNUSED !
     */
    open: function(notification) {
    },

    /** Destroys all notifications that are not resident and updates the count
     * of notifications.
     * @see Notification#resident
     */
    destroyNonResidentNotifications: function() {
        for (let i = this.notifications.length - 1; i >= 0; i--)
            if (!this.notifications[i].resident)
                this.notifications[i].destroy();

        this._updateCount();
    },

    /** Called when all notifications have been removed.
     * Default implementation is to destroy this source (hence emitting
     * {@link .event:destroy}, but with no reason), but subclasses can override.
     * @see #destroy
     * @todo example of an overidden
     */
    _lastNotificationRemoved: function() {
        this.destroy();
    }
};
Signals.addSignalMethods(Source.prototype);
/** Emitted whenever a notification is added in {@link Source#pushNotification}
 * @name notification-added
 * @param {MessageTray.Source} - source that emitted the signal
 * @param {MessageTray.Notification} - notification that was added.
 * @event
 * @memberof Source */
/** Emitted when a source is destroyed, possibly with a reason why.
 * From {@link Source#destroy}.
 * @name destroy
 * @param {MessageTray.Source} - source that emitted the signal
 * @param {MessageTray.NotificationDestroyedReason} - reason the source was
 * destroyed (EXPIRED, DISMISSED, SOURCE_CLOSED).
 * @event
 * @memberof Source */
/** Emitted whenever {@link Source#notify} is called.
 * Used by the {@link MessageTray} to open the message tray/pop up the
 * notification.
 * @name notify
 * @param {MessageTray.Source} - source that emitted the signal
 * @param {MessageTray.Notification} - notification that was notified.
 * @event
 * @memberof Source */
/** Emitted when the title of a Source changes.
 * Used by the {@link SummaryItem} containing this source to update the
 * title label.
 * @name title-changed
 * @event
 * @memberof Source */

/** creates a new SummaryItem
 *
 * The top-level actor {@link #actor} is a St.Button with style class
 * 'summary-source-button'.
 *
 * Within the icon a horizontal St.BoxLayout with style class 'summary-source'
 * holds both `source.getSummaryIcon()` (the source's icon) and the source's title
 * `source.title` (St.Label, style class 'source-title'). We make sure our
 * title always remains in sync with the source's (using its
 * ['title-changed']{@link Source.title-changed} signal).
 *
 * The St widgets that will show the list of outstanding notifications are
 * also made: outstanding notification summaries
 * are placed in a vertical BoxLayout {@link #notificationStack}
 * which is embedded in a ScrollView {@link #notificationStackView}.
 *
 * Then the right click menu {@link #rightClickMenu} is made (it's just
 * a St.BoxLayout, not a PopupMenu) and the "Open" and "Remove"
 * {@link PopupMenu.PopupMenuItem}s are created, added to it, and connected
 * up ("Open" calls {@link Source#open} on {@link #source}, "Remove" calls
 * {@link Source#destroy}). Clicking on either emits signal
 * ['done-displaying-content']{@link .done-displaying-content}
 *
 * @param {MessageTray.Source} source - the underlying source for this
 * summary item.
 * @classdesc
 * A SummaryItem is the representation of a {@link Source} (a source of notifications)
 * in the Message Tray.
 *
 * ![{@link SummaryItem} is yellow; its {@link Source} is red.](pics/messageTraySummaryItemYellowSourceRed.png)
 *
 * ![{@link SummaryItem} showing its [notification stack]{@link #notificationStack}](pics/SummaryItemStack.png)
 *
 * You initialise the item with an underlying {@link Source}. The Source
 * provides both the icon and the count of notifications (outlined in red
 * above) to its summary item.
 *
 * Left-clicking on a summary item calls {@link Source#handleSummaryClick}.
 *
 * If that returns `false` (i.e. didn't handle the click), it
 * brings up the list ("stack") of notifications
 * for the item's source, i.e. all notifications that have not yet been
 * dismissed. These are placed in a vertical BoxLayout {@link #notificationStack}
 * which is embedded in a ScrollView {@link #notificationStackView}.
 * 
 * However the *displaying* of the notification stack is handled by the
 * {@link MessageTray}, which essentially has one popup menu (a {@link BoxPointer.BoxPointer})
 * for all the summary items and it swaps the contents of that menu to be
 * the {@link #notificationStack} of the appropriate summary item (see
 * {@link MessageTray#_summaryBoxPointer}).
 *
 * A SummaryItem also has a right-click menu {@link #rightClickMenu} with
 * "Open" and "Remove" items (calls {@link Source#open} or {@link Source#destroy}).
 *
 * However all the click/hover handling (to expand/collapse the item in the
 * message tray or show/hide the right click menu or notification stack)
 * is handled by the {@link MessageTray}, not by the summay item.
 *
 * SummaryItems can be expanded (title showing) or collapsed (title hidden).
 *
 * When you add a source to the message tray with {@link MessageTray#add},
 * a {@link SummaryItem} is automatically made for it.
 * @class
 */
function SummaryItem(source) {
    this._init(source);
}

SummaryItem.prototype = {
    _init: function(source) {
        /** The underlying {@link Source} for the summary item.
         * @type {MessageTray.Source}
         */
        this.source = source;
        this.source.connect('notification-added', Lang.bind(this, this._notificationAddedToSource));

        /** Top-level actor for the summary item. Style class 'summay-source-button',
         * responsive to both left, right and middle clicks.
         * @type {St.Button} */
        this.actor = new St.Button({ style_class: 'summary-source-button',
                                     y_fill: true,
                                     reactive: true,
                                     button_mask: St.ButtonMask.ONE | St.ButtonMask.TWO | St.ButtonMask.THREE,
                                     track_hover: true });

        this._sourceBox = new St.BoxLayout({ style_class: 'summary-source' });

        this._sourceIcon = source.getSummaryIcon();
        this._sourceTitleBin = new St.Bin({ y_align: St.Align.MIDDLE,
                                            x_fill: true,
                                            clip_to_allocation: true });
        this._sourceTitle = new St.Label({ style_class: 'source-title',
                                           text: source.title });
        this._sourceTitle.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._sourceTitleBin.child = this._sourceTitle;
        this._sourceTitleBin.width = 0;

        this.source.connect('title-changed',
                            Lang.bind(this, function() {
                                this._sourceTitle.text = source.title;
                            }));

        this._sourceBox.add(this._sourceIcon, { y_fill: false });
        this._sourceBox.add(this._sourceTitleBin, { expand: true, y_fill: false });
        this.actor.child = this._sourceBox;

        /** The scroll view that holds {@link #notificationStack}, displaying
         * a list of outstanding notifications.
         * Style class `summary-notification-stack-scrollview`, *unless*
         * `source.isChat` is true, in which case there is no style class.
         * @type {St.ScrollView}. */
        this.notificationStackView = new St.ScrollView({ name: source.isChat ? '' : 'summary-notification-stack-scrollview',
                                                         vscrollbar_policy: source.isChat ? Gtk.PolicyType.NEVER : Gtk.PolicyType.AUTOMATIC,
                                                         hscrollbar_policy: Gtk.PolicyType.NEVER,
                                                         style_class: 'vfade' });
        /** vertical St.BoxLayout containing summaries of all the outstanding
         * notifications. Style class 'summary-notification-stack'.
         * @type {St.BoxLayout} */
        this.notificationStack = new St.BoxLayout({ name: 'summary-notification-stack',
                                                     vertical: true });
        this.notificationStackView.add_actor(this.notificationStack);
        this._stackedNotifications = [];

        this._oldMaxScrollAdjustment = 0;

        this.notificationStackView.vscroll.adjustment.connect('changed', Lang.bind(this, function(adjustment) {
            let currentValue = adjustment.value + adjustment.page_size;
            if (currentValue == this._oldMaxScrollAdjustment)
                this.scrollTo(St.Side.BOTTOM);
            this._oldMaxScrollAdjustment = adjustment.upper;
        }));

        /** the right-click menu for the summary item.
         *
         * Vertical St.BoxLayout with style class 'summary-right-click-menu'.
         * @type {St.BoxLayout} */
        this.rightClickMenu = new St.BoxLayout({ name: 'summary-right-click-menu',
                                                 vertical: true });

        let item;

        item = new PopupMenu.PopupMenuItem(_("Open"));
        item.connect('activate', Lang.bind(this, function() {
            source.open();
            this.emit('done-displaying-content');
        }));
        this.rightClickMenu.add(item.actor);

        item = new PopupMenu.PopupMenuItem(_("Remove"));
        item.connect('activate', Lang.bind(this, function() {
            source.destroy();
            this.emit('done-displaying-content');
        }));
        this.rightClickMenu.add(item.actor);

        let focusManager = St.FocusManager.get_for_stage(global.stage);
        focusManager.add_group(this.rightClickMenu);
    },

    // getTitleNaturalWidth, getTitleWidth, and setTitleWidth include
    // the spacing between the icon and title (which is actually
    // _sourceTitle's padding-left) as part of the width.

    /** Retrieves the natural width of the St.Label holding the source's
     * title, capped at {@link MAX_SOURCE_TITLE_WIDTH}.
     *
     * Includes the spacing between the icon and title which is actually
     * its 'padding-left' property (but not the width
     * of the icon itself).
     * @returns {number} minimum of the St.Label's preferred width and
     * {@link MAX_SOURCE_TITLE_WIDTH}
     */
    getTitleNaturalWidth: function() {
        let [minWidth, naturalWidth] = this._sourceTitle.get_preferred_width(-1);

        return Math.min(naturalWidth, MAX_SOURCE_TITLE_WIDTH);
    },

    /** Retrieves the current width of the St.Bin holding the
     * St.Label holding the source's title.
     * 
     * Note that this might be *less* than the title label's width, if the
     * summary item is currently expanding, collapsing, or collapsed.
     *
     * Includes the spacing between the icon and title which is actually
     * its 'padding-left' property (but not the width
     * of the icon itself).
     * @returns {number} current width of the St.Bin holding
     * the St.Label's with the title. Could be less than the St.Label's width,
     * if the summary item is currently "collapsed".
     */
    getTitleWidth: function() {
        return this._sourceTitleBin.width;
    },

    /** Sets the width of the summary item's title label.
     * Includes the spacing between the icon and title which is actually
     * its 'padding-left' property (but not the width
     * of the icon itself).
     * @param {} width
     */
    setTitleWidth: function(width) {
        width = Math.round(width);
        if (width != this._sourceTitleBin.width)
            this._sourceTitleBin.width = width;
    },

    /** Sets the ellipsization mode of the title label (what happens when
     * the text is too long for the table).
     * @param {Pango.EllipsizeMode} mode - (END, MIDDLE, NONE, START)
     */
    setEllipsization: function(mode) {
        this._sourceTitle.clutter_text.ellipsize = mode;
    },

    /** Called before showing the notifications for this summary item.
     *  Ensures that {@link #notificationStack} has all of {@link #source}'s
     * notifications added to it.
     * @see #_appendNotificationToStack
     * @see MessageTray#_showSummaryBoxPointer */
    prepareNotificationStackForShowing: function() {
        if (this.notificationStack.get_children().length > 0)
            return;

        for (let i = 0; i < this.source.notifications.length; i++) {
            this._appendNotificationToStack(this.source.notifications[i]);
        }
    },

    /** Called when the list of notifications for this source is done showing
     * (say the user clicked on the source to toggle it off).
     *
     * For each notification we call {@link Notification#collapseCompleted}
     * and remove it from the list of notifications {@link #notificationStack}.
     *
     * We also call {@link Notification#setIconVisible} and
     * {@link Notification#enableScrolling} (set both `true`) to convert the
     * notification back into its normal form.
     * @see MessageTray#_hideSummaryBoxPointerCompleted
     */
    doneShowingNotificationStack: function() {
        let notificationActors = this.notificationStack.get_children();
        for (let i = 0; i < this._stackedNotifications.length; i++) {
            let stackedNotification = this._stackedNotifications[i];
            let notification = stackedNotification.notification;
            notification.collapseCompleted();
            notification.disconnect(stackedNotification.notificationExpandedId);
            notification.disconnect(stackedNotification.notificationDoneDisplayingId);
            notification.disconnect(stackedNotification.notificationDestroyedId);
            if (notification.actor.get_parent() == this.notificationStack)
                this.notificationStack.remove_actor(notification.actor);
            notification.setIconVisible(true);
            notification.enableScrolling(true);
        }
        this._stackedNotifications = [];
    },

    /** Callback for {@link #source}'s ['notification-added']{@link Source.notification-added}
     * signal, when a notification is added to the source.
     *
     * If the notification stack (list of notifications) is currently being
     * shown, we add `notification` with {@link #_appendNotificationToStack}.
     * @param {MessageTray.Source} source - source emitting the signal
     * ({@link #source}).
     * @param {MessageTray.Notification} notification - the notification
     * added to the source.
     */
    _notificationAddedToSource: function(source, notification) {
        if (this.notificationStack.mapped)
            this._appendNotificationToStack(notification);
    },

    /** This appends a notification to our notifications "stack" (the popup
     * showing all the notification summaries).
     *
     * ![The notification stack for a {@link SummaryItem}](pics/SummaryItemStack.png)
     *
     * This:
     *
     * * disable scrolling (unless {@link #source} is for  chat);
     * * hides the icon of all notifications but the first (top) one in the stack;
     * * calls {@link Notification#expand} to show the notification's content
     * * connects to its ['expanded']{@link Notification.expanded},
     * ['done-displaying']{@link Notification.done-displaying} and
     * ['destroy']{@link Notification.event:destroy} signals in order to
     * re-transmit some of these signals from the summary item.
     * @see #_contentUpdated
     * @see #_notificationDoneDisplaying
     * @see #_notificationDestroyed
     * @param {MessageTray.Notification} notification - notification to add.
     */
    _appendNotificationToStack: function(notification) {
        let stackedNotification = {};
        stackedNotification.notification = notification;
        stackedNotification.notificationExpandedId = notification.connect('expanded', Lang.bind(this, this._contentUpdated));
        stackedNotification.notificationDoneDisplayingId = notification.connect('done-displaying', Lang.bind(this, this._notificationDoneDisplaying));
        stackedNotification.notificationDestroyedId = notification.connect('destroy', Lang.bind(this, this._notificationDestroyed));
        this._stackedNotifications.push(stackedNotification);
        if (!this.source.isChat)
            notification.enableScrolling(false);
        if (this.notificationStack.get_children().length > 0)
            notification.setIconVisible(false);
        this.notificationStack.add(notification.actor);
        notification.expand(false);
    },

    /** Scrolls the notifiction stack to the indicated edge
     * @param {St.Side} side - St.Side.TOP or St.Side.BOTTOM.
     */
    scrollTo: function(side) {
        let adjustment = this.notificationStackView.vscroll.adjustment;
        if (side == St.Side.TOP)
            adjustment.value = adjustment.lower;
        else if (side == St.Side.BOTTOM)
            adjustment.value = adjustment.upper;
    },

    /** Callback when any notification in the stack emits its
     * ['expanded']{@link Notification.event:expanded} signal.
     *
     * All this does is emit 'content-updated' from the summary item.
     *
     * This is caught by the message tray which re-adjusts the location of the
     * box pointer showing the notification stack (so that it's aligned in
     * the centre of the popup, if it fits).
     * @fires .content-updated
     * @see MessageTray#_onSummaryBoxPointerContentUpdated
     */
    _contentUpdated: function() {
        this.emit('content-updated');
    },

    /** Callback when any notification in the stack emits its
     * ['done-displaying']{@link Notification.done-displaying} signal.
     *
     * All this does is emit 'done-displaying-content' from the summary item.
     *
     * This is caught by the message tray which as far as I can tell just updates
     * the tray's state according to what's been happening in the meantime
     * (e.g. hides the message tray again if the pointer has moved
     * outside of it).
     * @fires .done-displaying-content
     * @see MessageTray#_escapeTray
     */
    _notificationDoneDisplaying: function() {
        this.emit('done-displaying-content');
    },

    /** Callback when any notification in the stack is destroyed (emits
     * the ['destroy']{@link Notification.event:destroy} signal).
     *
     * This removes the notification from the stack and updates the stack visually.
     *
     * @param {MessageTray.Notification} notification - the notification that
     * emitted the signal.
     */
    _notificationDestroyed: function(notification) {
        for (let i = 0; i < this._stackedNotifications.length; i++) {
            if (this._stackedNotifications[i].notification == notification) {
                let stackedNotification = this._stackedNotifications[i];
                notification.disconnect(stackedNotification.notificationExpandedId);
                notification.disconnect(stackedNotification.notificationDoneDisplayingId);
                notification.disconnect(stackedNotification.notificationDestroyedId);
                this._stackedNotifications.splice(i, 1);
                this._contentUpdated();
                break;
            }
        }

        if (this.notificationStack.get_children().length > 0)
            this.notificationStack.get_children()[0]._delegate.setIconVisible(true);
    }
};
Signals.addSignalMethods(SummaryItem.prototype);
/** Emitted when a summary item is done showing its list of notifications
 * (when "Open" or "Remove" is clicked on its right click menu, or when
 * a notification emits its
 * ['done-displaying']{@link Notification.done-displaying} signal).
 * @event
 * @name done-displaying-content
 * @memberof SummaryItem
 */
/** Emitted when the content of one of the notifications in the summary item's
 * notification stack changes (for example through a {@link Notification#addButton}
 * call).
 *
 * This is caught by the message tray which just makes sure that the notification
 * stack popup looks pretty (the box pointer remains in the centre).
 * @event
 * @name content-updated
 * @memberof SummaryItem
 */
/** Emitted when the user clicks on a summary item and its source does not
 * handle it.
 * @event
 * @name summary-item-clicked
 * @memberof SummaryItem
 * @param {MessageTray.SummaryItem} summaryItem - item emitting the signal
 * @param {number} button - the button that was clicked (1 is left, 2 is
 * middle, 3 is right).
 */

/** creates a new MessageTray.
 *
 * Some terminology - the "summary box pointer" refers to the popup menu showing
 * the list of notifications for a summary item.
 *
 * We set up the UI elements:
 *
 * * the top-level actor {@link #actor}, a St.Group;
 * * a horizontal box layout {@link #_summary} into which we
 * will place the {@link SummaryItem}s for all our {@link Source}s;
 * * {@link #_notificationBin}, where we place the notification currently
 * being notified (popped up from the middle-bottom of the screen);
 * * {@link #_corner}, an (invisible) Clutter.Rectangle used as the hot corner
 * for the message tray;
 * * others.
 *
 * We initialise the "state" variables:
 *
 * * {@link #_summaryBoxPointerItem}, the {@link SummaryItem} that is currently
 * showing its list of notifications (in a popup menu).
 * * {@link #_clickedSummaryItem}, the {@link SummaryItem} that has just
 * received a click.
 * * {@link #_expandedSummaryItem}, the {@link SummaryItem} that is currently
 * expanded (if the mouse hovers over a summary item it expands to show its
 * title)
 * * {@link #_trayState}, {@link #_summaryState}, {@link #_notificationState},
 * {@link #_summaryBoxPointerState} - the current state of the tray, container
 * of summary items, the currently-pending notification (as from a call to
 * {@link Source#notify}), and the summary box pointer (popup menu showing
 * the list of notifications for a summary item). Hidden, showing,
 * shown, hiding (see {@link State}).
 * * others.
 *
 * We also connect to the overview's 'showing' signal to ensure the message
 * tray is showing in the overview, and to its 'hiding' signal to hide it back.
 * @todo add more detail about other variables I haven't documented and so on
 * @classdesc
 * This is the horizontal tray down the bottom of the screen that is triggered
 * by moving the cursor to the bottom-right corner of the screen.
 *
 * In terms of code, it's a *huge* fat class because it has to handle many things:
 *
 * * showing {@link SummaryItem}s;
 * * expanding and collapsing its {@link SummaryItem}s according to the cursor;
 * * listening to clicks on its {@link SummaryItem}s to show either the right-click
 * menu or the list of notifications for that item;
 * * when the tray should be expanded or hidden (for example it is always expanded
 * in the overview);
 * * showing notifications - while notifying and when opened from a summary item,
 * taking into account notification type (resident, transient, ...) and urgency;
 * * interaction with notifications (e.g. chat ones - the tray should be open
 * while we are chatting);
 * * care taken with {@link Source}s for chat - these are always grouped
 * first on the message tray before other summary items, and have slightly
 * different focus rules (?)
 * * lots more.
 *
 * According to a comment in the code most of the functions update a set of
 * state variables in the message tray, and the function
 * {@link #_updateState} is what does most the work in terms of animating,
 * showing, hiding, etc.
 *
 * Some terminology - the "summary box pointer" refers to the popup menu showing
 * the list of notifications for a summary item.
 * @class
 */
function MessageTray() {
    this._init();
}

MessageTray.prototype = {
    _init: function() {
        /** Used to monitor the user's chat presence.
         * @type {GnomeSession.Presence}
         */
        this._presence = new GnomeSession.Presence();
        this._userStatus = GnomeSession.PresenceStatus.AVAILABLE;
        this._busy = false;
        /** Whether we've just come back from being away
         * (i.e. status changes from IDLE to something else that isn't offline)
         * @type {boolean}
         */
        this._backFromAway = false;
        this._presence.connect('StatusChanged', Lang.bind(this, this._onStatusChanged));
        this._presence.getStatus(Lang.bind(this, this._onStatusChanged));

        /** Top-level actor for the message tray. A St.Group with name 'message-tray'.
         * @type {St.Group} */
        this.actor = new St.Group({ name: 'message-tray',
                                    reactive: true,
                                    track_hover: true });
        this.actor.connect('notify::hover', Lang.bind(this, this._onTrayHoverChanged));

        /** This will hold the notification currently being notified (popped
         * up from the middle-bottom of the screen, as caused by
         * {@link Source#notify}).
         * @type {St.Bin} */
        this._notificationBin = new St.Bin();
        this.actor.add_actor(this._notificationBin);
        this._notificationBin.hide();
        /** This is the queue of notifications that are waiting to be
         * notified (i.e. popped up from the bottom of the screen).
         *
         * It is sorted by notification urgency.
         *
         * They are popped up from the screen in succession until they're
         * all done.
         *
         * The one currently being shown (i.e. first in the queue)
         * is stored in {@link #_notification}.
         *
         * @see Source#notify
         * @type {MessageTray.Notification[]}
         */
        this._notificationQueue = [];
        /** This is the first notification in the {@link #_notificationQueue}.
         *
         * That is, the notification that is currently popping up from the
         * bottom of the screen.
         * @type {MessageTray.Notification}
         */
        this._notification = null;
        this._notificationClickedId = 0;

        this._summaryBin = new St.Bin({ x_align: St.Align.END });
        this.actor.add_actor(this._summaryBin);
        /** The St.BoxLayout that will hold all the {@link SummaryItem}s for
         * the tray. Name 'summary-mode'.
         * @type {St.BoxLayout} */
        this._summary = new St.BoxLayout({ name: 'summary-mode',
                                           reactive: true,
                                           track_hover: true });
        this._summary.connect('notify::hover', Lang.bind(this, this._onSummaryHoverChanged));
        this._summaryBin.child = this._summary;
        this._summaryBin.opacity = 0;

        this._summaryMotionId = 0;
        this._trayMotionId = 0;

        /** The {@link BoxPointer.BoxPointer} for displaying the notification
         * stack (list of notifications for a Source,
         * {@link SummaryItem#notificationStackView}) of a {@link SummaryItem}.
         *
         * Rather than creating one {@link PopupMenu.PopupMenu} for each summary
         * item, we instead use one popup menu for the *entire* message tray
         * (as you can only show the notification stack for one summary item
         * at a time anyway), and switch in and out the contents of that
         * popup menu to {@link SummaryItem#notificationStackView} and position
         * it appropriately.
         *
         * To do that we make a {@link BoxPointer.BoxPointer} and replace
         * its `.bin.child` with the appropriate `summaryItem.notificationStackView`.
         * @type {BoxPointer.BoxPointer */
        this._summaryBoxPointer = new BoxPointer.BoxPointer(St.Side.BOTTOM,
                                                            { reactive: true,
                                                              track_hover: true });
        this._summaryBoxPointer.actor.style_class = 'summary-boxpointer';
        this._summaryBoxPointer.actor.hide();
        Main.layoutManager.addChrome(this._summaryBoxPointer.actor, { visibleInFullscreen: true });

        /** If the user has clicked on a {@link SummaryItem} to show its popup
         * menu of notifications, this is that item.
         * @type {?MessageTray.SummaryItem} */
        this._summaryBoxPointerItem = null;
        this._summaryBoxPointerContentUpdatedId = 0;
        this._summaryBoxPointerDoneDisplayingId = 0;
        /** If the user has clicked on a {@link SummaryItem}, this is it.
         * @type {?MessageTray.SummaryItem} */
        this._clickedSummaryItem = null;
        /** If the user has clicked on a {@link SummaryItem}, this is the
         * button they clicked with. -1 is unknown/unset, 1 is left, 2
         * is middle, 3 is right click.
         * @type {number} */
        this._clickedSummaryItemMouseButton = -1;
        this._clickedSummaryItemAllocationChangedId = 0;
        /** If one of the summary items is expanded (because the user's
         * mouse is hovering over it), this is it.
         * @type {?MessageTray.SummaryItem} */
        this._expandedSummaryItem = null;
        this._summaryItemTitleWidth = 0;
        this._pointerBarrier = 0;

        // To simplify the summary item animation code, we pretend
        // that there's an invisible SummaryItem to the left of the
        // leftmost real summary item, and that it's expanded when all
        // of the other items are collapsed.
        this._imaginarySummaryItemTitleWidth = 0;

        /** Focus Grabber for the message tray.
         *
         * As far as I can tell it's used to help grab focus to the summary
         * item popup menu (right click menu or left click notification stack)
         * when that pops up, and also to {@link #_notification} when a
         * notification is popped up from the bottom of the screen.
         *
         * Examples I could find:
         * When the focus grabber grabs focus and some sort of menu is being shown
         * from a summary item (right click menu or notification stack), we lock
         * the message tray (not entirely sure what locking does).
         *
         * If the user has clicked on a summary item (so we're showing either the
         * notification stack or right click menu for it) and they then click outside,
         * we close the menu and ungrab focus.
         *
         * If the user presses Escape while focus is being grabbed, we close the
         * tray ({@link #_escapeTray}).
         * @type {MessageTray.FocusGrabber}
         */
        this._focusGrabber = new FocusGrabber();
        this._focusGrabber.connect('focus-grabbed', Lang.bind(this,
            function() {
                if (this._summaryBoxPointer.bin.child)
                    this._lock();
            }));
        this._focusGrabber.connect('focus-ungrabbed', Lang.bind(this, this._unlock));
        this._focusGrabber.connect('button-pressed', Lang.bind(this,
           function(focusGrabber, source) {
               if (this._clickedSummaryItem && !this._clickedSummaryItem.actor.contains(source))
                   this._unsetClickedSummaryItem();
               this._focusGrabber.ungrabFocus();
           }));
        this._focusGrabber.connect('escape-pressed', Lang.bind(this, this._escapeTray));

        Main.layoutManager.keyboardBox.connect('notify::hover', Lang.bind(this, this._onKeyboardHoverChanged));

        // TODO: I don't know some of these. Is it necessary to document
        // them all/should I not document them?
        /** The state of the message tray (hidden, showing, hiding, shown).
         * @type {MessageTray.State} */
        this._trayState = State.HIDDEN;
        /** Whether the message tray is currently locked
         * @type {boolean} */
        this._locked = false;
        this._traySummoned = false;
        this._useLongerTrayLeftTimeout = false;
        this._trayLeftTimeoutId = 0;
        /** Whether the mouse pointer is currently over the message tray.
         * @type {boolean} */
        this._pointerInTray = false;
        /** Whether the mouse pointer is currently over the on-screen keyboard,
         * if that is enabled.
         * @type {boolean} */
        this._pointerInKeyboard = false;
        /** The state of the box holding the summary items (hidden, showing, hiding, shown).
         * @type {MessageTray.State} */
        this._summaryState = State.HIDDEN;
        this._summaryTimeoutId = 0;
        /** Whether the pointer is currently over the summary item box.
         * @type {boolean} */
        this._pointerInSummary = false;
        /** The state of the currently-being-notified notification
         * (as from a call to {@link Source#notify})
         * (hidden, showing, hiding, shown).
         * @type {MessageTray.State} */
        this._notificationState = State.HIDDEN;
        this._notificationTimeoutId = 0;
        this._notificationExpandedId = 0;
        /** The state of the summary box pointer (hidden, showing, hiding, shown).
         * @type {MessageTray.State} */
        this._summaryBoxPointerState = State.HIDDEN;
        this._summaryBoxPointerTimeoutId = 0;
        /** Whether the overview is visible (the message tray shows itself
         * when the overview is visible)
         * @type {boolean} */
        this._overviewVisible = Main.overview.visible;
        this._notificationRemoved = false;
        this._reNotifyAfterHideNotification = null;

        /** The hot-corner for the message tray (bottom right of the screen).
         * @type {Clutter.Rectangle} */
        this._corner = new Clutter.Rectangle({ width: 1,
                                               height: 1,
                                               opacity: 0,
                                               reactive: true });
        this._corner.connect('enter-event', Lang.bind(this, this._onCornerEnter));
        Main.layoutManager.trayBox.add_actor(this._corner);
        Main.layoutManager.trackChrome(this._corner);

        Main.layoutManager.trayBox.add_actor(this.actor);
        this.actor.y = this.actor.height;
        Main.layoutManager.trackChrome(this.actor);
        Main.layoutManager.trackChrome(this._notificationBin);

        Main.layoutManager.connect('monitors-changed', Lang.bind(this, this._setSizePosition));

        this._setSizePosition();

        Main.overview.connect('showing', Lang.bind(this,
            function() {
                this._overviewVisible = true;
                if (this._locked) {
                    this._unsetClickedSummaryItem();
                    this._unlock();
                } else {
                    this._updateState();
                }
            }));
        Main.overview.connect('hiding', Lang.bind(this,
            function() {
                this._overviewVisible = false;
                if (this._locked) {
                    this._unsetClickedSummaryItem();
                    this._unlock();
                } else {
                    this._updateState();
                }
            }));

        /** List of summary items currently in the message tray.
         * {@type MessageTray.SummaryItem[]} */
        this._summaryItems = [];
        /** We keep a list of new summary items that were added to the summary
         * since the last time it was shown to the user.
         *
         * We automatically show the summary to the user if there are items in
         * this list once the notifications are done showing or once an item gets
         * added to the summary without a notification being shown.
         * {@type MessageTray.SummaryItem[]} */
        this._newSummaryItems = [];
        /** The summary item with the longest natural title width
         * ({@link SummaryItem#getTitleNaturalWidth}), used for laying everything
         * out nicely.
         * @type {?MessageTray.SummaryItem} */
        this._longestSummaryItem = null;
        /** The number of summary items we have with `summaryItem.source.isChat`
         * set to `true` (i.e. the number of summary items for chat). Chat
         * items *always* appear first (left-most) in the message tray with
         * non-chat summary items appearing after.
         * @type {number} */
        this._chatSummaryItemsCount = 0;
    },

    /** Callback when the user's mouse enters the hot corner on the bottom-right
     * of the screen ({@link #_corner}).
     *
     * This sets {@link #_pointerInSummary} to `true` and calls
     * {@link #_updateState} (which will handle things like opening the
     * message tray if it isn't already open).
     */
    _onCornerEnter: function(actor, event) {
        this._pointerInSummary = true;
        this._updateState();
    },

    /** Sets the size and position of the message tray.
     * We position ourselves on the bottom of the bottom monitor, taking
     * up the full width of that monitor.
     *
     * We position the hot corner on the bottom-left corner of that
     * monitor if the text direction is rigt-to-left and on the
     * bottom-right otherwise.
     */
    _setSizePosition: function() {
        let monitor = Main.layoutManager.bottomMonitor;
        this._notificationBin.x = 0;
        this._notificationBin.width = monitor.width;
        this._summaryBin.x = 0;
        this._summaryBin.width = monitor.width;

        if (St.Widget.get_default_direction() == St.TextDirection.RTL)
            this._corner.x = 0;
        else
            this._corner.x = Main.layoutManager.trayBox.width - 1;

        this._corner.y = Main.layoutManager.trayBox.height - 1;
    },

    /** Whether the message tray currently has a {@link SummaryItem}
     * for `source`.
     * @param {MessageTray.Source} source - the source to look up.
     * @returns {boolean} whether `source` is currently in the message tray.
     */
    contains: function(source) {
        return this._getIndexOfSummaryItemForSource(source) >= 0;
    },

    /** Returns the index of `source`'s summary item in the message tray.
     * @inheritparams #contains
     * @returns {number} the index of `source`'s summary item in the
     * message tray ({@link #_summaryItems}), or -1 if not found.
     */
    _getIndexOfSummaryItemForSource: function(source) {
        for (let i = 0; i < this._summaryItems.length; i++) {
            if (this._summaryItems[i].source == source)
                return i;
        }
        return -1;
    },

    /** Adds a source to the message tray.
     *
     * If it's already in the message tray we quit.
     *
     * Otherwise, a {@link SummaryItem} is made for the source (even if it
     * or its notification is transient; it's just that the summary item may
     * not be shown).
     *
     * If the source is for chat ({@link Source#isChat}), we add it to the
     * front (left end) of the message tray with the other chat items. Otherwise,
     * we add it to the front (left end) of the non-chat items.
     *
     * We add the summary item into {@link #_summaryItems}, and also into
     * {@link #_newSummaryItems} if it's not a chat or transient source
     * (this is a list of summary items added since the last time the user
     * saw them - after all the notifications are done we pop up the message
     * tray to show them to the user).
     *
     * We connect to the summary item's actor's 'notify::hover'
     * signal ({@link #_onSummaryItemHoverChanged}) and its
     * 'clicked' signal ({@link #_onSummaryItemClicked}) and
     * the source's 'destroy' signal ({@link #_onSourceDestroy}) to
     * adjust our behaviour when these events occur. For example, hovering
     * over a summary item expands it.
     *
     * Finally we call {@link #_updateState} after a short delay in order
     * to show the newly-added summary item (if relevant).
     *
     * @param {MessageTray.Source} source - the relevant source
     */
    add: function(source) {
        if (this.contains(source)) {
            log('Trying to re-add source ' + source.title);
            return;
        }

        let summaryItem = new SummaryItem(source);

        if (source.isChat) {
            this._summary.insert_actor(summaryItem.actor, 0);
            this._chatSummaryItemsCount++;
        } else {
            this._summary.insert_actor(summaryItem.actor, this._chatSummaryItemsCount);
        }

        let titleWidth = summaryItem.getTitleNaturalWidth();
        if (titleWidth > this._summaryItemTitleWidth) {
            this._summaryItemTitleWidth = titleWidth;
            if (!this._expandedSummaryItem)
                this._imaginarySummaryItemTitleWidth = titleWidth;
            this._longestSummaryItem = summaryItem;
        }

        this._summaryItems.push(summaryItem);

        // We keep this._newSummaryItems to track any new sources that were added to the
        // summary and show the summary with them to the user for a short period of time
        // after notifications are done showing. However, we don't want that to happen for
        // transient sources, which are removed after the notification is shown, but are
        // not removed fast enough because of the callbacks to avoid the summary popping up.
        // So we just don't add transient sources to this._newSummaryItems.
        // We don't want that to happen for chat sources neither, because they
        // can be added when the user starts a chat from Empathy and they are not transient.
        // The notification will popup on incoming message anyway. See bug URLHighlighter#657249.
        if (!source.isTransient && !source.isChat)
            this._newSummaryItems.push(summaryItem);

        source.connect('notify', Lang.bind(this, this._onNotify));

        summaryItem.actor.connect('notify::hover', Lang.bind(this,
            function () {
                this._onSummaryItemHoverChanged(summaryItem);
            }));

        summaryItem.actor.connect('clicked', Lang.bind(this,
            function (actor, button) {
                this._onSummaryItemClicked(summaryItem, button);
            }));

        source.connect('destroy', Lang.bind(this, this._onSourceDestroy));

        // We need to display the newly-added summary item, but if the
        // caller is about to post a notification, we want to show that
        // *first* and not show the summary item until after it hides.
        // So postpone calling _updateState() a tiny bit.
        Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this, function() { this._updateState(); return false; }));
    },

    /** Callback when a {@link Source} is destroyed.
     *
     * This:
     *
     * * removes the corresponding {@link SummaryItem} from
     * {@link #_newSummaryItems} and {@link #_summaryItems}
     * * updates state variables {@link #_chatSummaryItemsCount},
     * {@link #_longestSummaryItem}, and
     * {@link #_expandedSummaryItem} (if `source`'s summary item was one of
     * these).
     * * destroys the summary item
     * * if one of this source's notifications is currently being notified,
     * or the currently-clicked summary item is the one corresponding to
     * `source` (i.e. its right-click menu or notification stack is showing),
     * call {@link #_updateState}
     *
     * @inheritparams #add
     */
    _onSourceDestroy: function(source) {
        let index = this._getIndexOfSummaryItemForSource(source);
        if (index == -1)
            return;

        let summaryItemToRemove = this._summaryItems[index];

        let newSummaryItemsIndex = this._newSummaryItems.indexOf(this._summaryItems[index]);
        if (newSummaryItemsIndex != -1)
            this._newSummaryItems.splice(newSummaryItemsIndex, 1);

        this._summaryItems.splice(index, 1);

        if (source.isChat)
            this._chatSummaryItemsCount--;

        if (this._expandedSummaryItem == summaryItemToRemove)
            this._expandedSummaryItem = null;

        if (this._longestSummaryItem.source == source) {
            let newTitleWidth = 0;
            this._longestSummaryItem = null;
            for (let i = 0; i < this._summaryItems.length; i++) {
                let summaryItem = this._summaryItems[i];
                let titleWidth = summaryItem.getTitleNaturalWidth();
                if (titleWidth > newTitleWidth) {
                    newTitleWidth = titleWidth;
                    this._longestSummaryItem = summaryItem;
                }
            }

            this._summaryItemTitleWidth = newTitleWidth;
            if (!this._expandedSummaryItem)
                this._imaginarySummaryItemTitleWidth = newTitleWidth;
        }

        let needUpdate = false;

        if (this._notification && this._notification.source == source) {
            this._updateNotificationTimeout(0);
            this._notificationRemoved = true;
            needUpdate = true;
        }
        if (this._clickedSummaryItem == summaryItemToRemove) {
            this._unsetClickedSummaryItem();
            needUpdate = true;
        }

        summaryItemToRemove.actor.destroy();

        if (needUpdate)
            this._updateState();
    },

    /** Callback when a notification that is being notified (i.e.
     * {@link Source#notify} as opposed to {@link Source#pushNotification})
     * is destroyed.
     *
     * If the notification destroyed is the one currently showing,
     * it is removed, {@link #_updateState} is called (to tween it out),
     * and we return.
     *
     * Otherwise it must be in {@link #_notificationQueue} hence not
     * currently showing (waiting to be shown), so it's just removed from
     * the queue.
     * @param {MessageTray.Notification} notification - notification destroyed.
     */
    _onNotificationDestroy: function(notification) {
        if (this._notification == notification && (this._notificationState == State.SHOWN || this._notificationState == State.SHOWING)) {
            this._updateNotificationTimeout(0);
            this._notificationRemoved = true;
            this._updateState();
            return;
        }

        let index = this._notificationQueue.indexOf(notification);
        notification.destroy();
        if (index != -1)
            this._notificationQueue.splice(index, 1);
    },

    /** Locks the message tray.
     * Just sets {@link #_locked} to `true`.
     * @TODO: what does this do?
     */
    _lock: function() {
        this._locked = true;
    },

    /** Unlocks the message tray - updates {@link #_pointerInTray},
     * calls {@link #_updateState}, and sets {@link #_locked} back to `false`.
     * @TODO: what does this do?
     */
    _unlock: function() {
        if (!this._locked)
            return;
        this._locked = false;
        this._pointerInTray = this.actor.hover;
        this._updateState();
    },

    /** Toggles the message tray (switches {@link #_traySummoned} and calls
     * {@link #_updateState}) */
    toggle: function() {
        this._traySummoned = !this._traySummoned;
        this._updateState();
    },

    /** Hides the message tray (sets {@link #_traySummoned} to false and
     * calls {@link #_updateState}). */
    hide: function() {
        this._traySummoned = false;
        this.actor.set_hover(false);
        this._summary.set_hover(false);
        this._updateState();
    },

    /** Callback when any source emits its ['notify']{@link Source.event:notify}
     * signal via {@link Source#notify}.
     *
     * If we are in the process of hiding the summary box pointer and the pointer
     * is for the summary item for `source`:
     *
     * If there is an update for one of the notifications or
     * a new notification to be added to the notification stack
     * while it is in the process of being hidden, we show it as
     * a new notification. However, we first wait till the hide
     * is complete. This is especially important if one of the
     * notifications in the stack was updated because we will
     * need to be able to re-parent its actor to a different
     * part of the stage.
     *
     * Otherwise if `notification` is {@link #_notification}, i.e. the notification
     * currently being popped up from the bottom of the screen, then the
     * notification is showing and has been updated.
     *
     * In this case, we call {@link #_updateShowingNotification} which re-expands
     * according to the new contents and extends the time until it auto-hides.
     *
     * Otherwise if the notification is not yet in {@link #_notificationQueue},
     * we add it there, making sure to sort by urgency.
     *
     * Finally we call {@link #_updateState} to make all the changes happen.
     * @inheritparams Source.event:notify
     */
    _onNotify: function(source, notification) {
        if (this._summaryBoxPointerItem && this._summaryBoxPointerItem.source == source) {
            if (this._summaryBoxPointerState == State.HIDING)
                // We are in the process of hiding the summary box pointer.
                // If there is an update for one of the notifications or
                // a new notification to be added to the notification stack
                // while it is in the process of being hidden, we show it as
                // a new notification. However, we first wait till the hide
                // is complete. This is especially important if one of the
                // notifications in the stack was updated because we will
                // need to be able to re-parent its actor to a different
                // part of the stage.
                this._reNotifyAfterHideNotification = notification;
            return;
        }

        if (this._notification == notification) {
            // If a notification that is being shown is updated, we update
            // how it is shown and extend the time until it auto-hides.
            // If a new notification is updated while it is being hidden,
            // we stop hiding it and show it again.
            this._updateShowingNotification();
        } else if (this._notificationQueue.indexOf(notification) < 0) {
            notification.connect('destroy',
                                 Lang.bind(this, this._onNotificationDestroy));
            this._notificationQueue.push(notification);
            this._notificationQueue.sort(function(notification1, notification2) {
                return (notification2.urgency - notification1.urgency);
            });
        }
        this._updateState();
    },

    /** Callback when the user hovers over a {@link SummaryItem}.
     * This sets `summaryItem` as the expanded summary item (which
     * in turn animates it).
     * @see #_setExpandedSummaryItem
     * @param {MessageTray.SummaryItem} summaryItem - summary item to expand
     */
    _onSummaryItemHoverChanged: function(summaryItem) {
        if (summaryItem.actor.hover)
            this._setExpandedSummaryItem(summaryItem);
    },

    /** Sets the expanded summary item to `summaryItem`, i.e. expands it
     * (when you hover your mouse over a summary item in the message tray
     * it expands, showing its title).
     *
     * 1. Turn off ellipsization for the previous expanded item and
     * for the new expanded item while they are expanding/collapsing (it looks
     * better).
     * 2. Remove the 'expanded' pseudo style class from the old summary item,
     * add it to the new one.
     * 3. Tween {@link #_expandedSummaryItemTitleWidth}, which represents the
     * current title width of the expanded/expanding item.
     *
     * (If `summaryItem` is null we do the same but just collapse all the
     * items rather than collapsing all but one and expanding that one).
     *
     * @param {?MessageTray.SummaryItem} summaryItem - summary item to expand,
     * or `null` if we just want to re-collapse everything.
     */
    _setExpandedSummaryItem: function(summaryItem) {
        if (summaryItem == this._expandedSummaryItem)
            return;

        // We can't just animate individual summary items as the
        // pointer moves in and out of them, because if they don't
        // move in sync you get weird-looking wobbling. So whenever
        // there's a change, we have to re-tween the entire summary
        // area.

        // Turn off ellipsization for the previously expanded item that is
        // collapsing and for the item that is expanding because it looks
        // better that way.
        if (this._expandedSummaryItem) {
            // Ideally, we would remove 'expanded' pseudo class when the item
            // is done collapsing, but we don't track when that happens.
            this._expandedSummaryItem.actor.remove_style_pseudo_class('expanded');
            this._expandedSummaryItem.setEllipsization(Pango.EllipsizeMode.NONE);
        }

        this._expandedSummaryItem = summaryItem;
        if (this._expandedSummaryItem) {
            this._expandedSummaryItem.actor.add_style_pseudo_class('expanded');
            this._expandedSummaryItem.setEllipsization(Pango.EllipsizeMode.NONE);
        }

        // We tween on a "_expandedSummaryItemTitleWidth" pseudo-property
        // that represents the current title width of the
        // expanded/expanding item, or the width of the imaginary
        // invisible item if we're collapsing everything.
        Tweener.addTween(this,
                         { _expandedSummaryItemTitleWidth: this._summaryItemTitleWidth,
                           time: ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._expandSummaryItemCompleted,
                           onCompleteScope: this });
    },

    /** The width of the expanded summary item (or the remaining space not
     * taken up by summary items if there is no expanded one).
     *
     * This is a special setter/getter; setting this value will cause the
     * currently-expanded item ({@link #_expandedSummaryItem})'s width to
     * go to this value (it's used in {@link #_setExpandedSummaryItem}
     * to animate the collapsing/expanding of summary items).
     *
     * @see SummaryItem#getTitleWidth
     * @type {number} */
    get _expandedSummaryItemTitleWidth() {
        if (this._expandedSummaryItem)
            return this._expandedSummaryItem.getTitleWidth();
        else
            return this._imaginarySummaryItemTitleWidth;
    },

    set _expandedSummaryItemTitleWidth(expansion) {
        expansion = Math.round(expansion);

        // Expand the expanding item to its new width
        if (this._expandedSummaryItem)
            this._expandedSummaryItem.setTitleWidth(expansion);
        else
            this._imaginarySummaryItemTitleWidth = expansion;

        // Figure out how much space the other items are currently
        // using, and how much they need to be shrunk to keep the
        // total width (including the width of the imaginary item)
        // constant.
        let excess = this._summaryItemTitleWidth - expansion;
        let oldExcess = 0, shrinkage;
        if (excess) {
            for (let i = 0; i < this._summaryItems.length; i++) {
                if (this._summaryItems[i] != this._expandedSummaryItem)
                    oldExcess += this._summaryItems[i].getTitleWidth();
            }
            if (this._expandedSummaryItem)
                oldExcess += this._imaginarySummaryItemTitleWidth;
        }
        if (excess && oldExcess)
            shrinkage = excess / oldExcess;
        else
            shrinkage = 0;

        // Now shrink each one proportionately
        for (let i = 0; i < this._summaryItems.length; i++) {
            if (this._summaryItems[i] == this._expandedSummaryItem)
                continue;

            let oldWidth = this._summaryItems[i].getTitleWidth();
            let newWidth = Math.floor(oldWidth * shrinkage);
            excess -= newWidth;
            this._summaryItems[i].setTitleWidth(newWidth);
        }
        if (this._expandedSummaryItem) {
            let oldWidth = this._imaginarySummaryItemTitleWidth;
            let newWidth = Math.floor(oldWidth * shrinkage);
            excess -= newWidth;
            this._imaginarySummaryItemTitleWidth = newWidth;
        }

        // If the tray as a whole is fully-expanded, make sure the
        // left edge doesn't wobble during animation due to rounding.
        if (this._imaginarySummaryItemTitleWidth == 0 && excess != 0) {
            for (let i = 0; i < this._summaryItems.length; i++) {
                if (this._summaryItems[i] == this._expandedSummaryItem)
                    continue;

                let oldWidth = this._summaryItems[i].getTitleWidth();
                if (oldWidth != 0) {
                    this._summaryItems[i].setTitleWidth (oldWidth + excess);
                    break;
                }
            }
        }
    },

    /** Callback when we're finished expanding a summary item.
     *
     * This sets the ellipsization back to END on the item (it is set to
     * NONE during the animation because that looks better)
     */
    _expandSummaryItemCompleted: function() {
        if (this._expandedSummaryItem)
            this._expandedSummaryItem.setEllipsization(Pango.EllipsizeMode.END);
    },

    /** Callback when a summary item is clicked.
     *
     * First [`summaryItem.source.handleSummaryClick()`]{@link Source#handleSummaryClick}
     * is called to give the source a chance to "steal" the click.
     *
     * If that returns `true` (i.e. handled the click) we unset the clicked
     * summary item and return.
     *
     * If there is already a clicked summary item (e.g. they
     * clicked once to show the summary item's notification stack, and this
     * is the second click), we call {@link #_unsetClickedSummaryItem}
     * to undo the change.
     *
     * Otherwise we set {@link #_clickedSummaryItem} to `summaryItem`,
     * {@link #_clickedSummaryItemMouseButton} to `button`, and emit
     * ['summary-item-clicked']{@link SummaryItem.summary-item-clicked}
     * from `summaryItem` (**not** the message tray). (The only class that
     * uses this in 3.2 is the {@link TelepathyClient.ChatSource} which
     * uses it to mean that the user has read all pending chats).
     *
     * Finally regardless of all of the above we call {@link #_updateState}.
     *
     * @param {MessageTray.SummaryItem} summaryItem - the summary item that
     * was clicked.
     * @param {number} button - type of click. 1 is left, 2 is middle, 3
     * is right.
     */
    _onSummaryItemClicked: function(summaryItem, button) {
        if (summaryItem.source.handleSummaryClick())
            this._unsetClickedSummaryItem();
        else if (!this._clickedSummaryItem ||
                 this._clickedSummaryItem != summaryItem ||
                 this._clickedSummaryItemMouseButton != button) {
            this._clickedSummaryItem = summaryItem;
            this._clickedSummaryItemMouseButton = button;

            summaryItem.source.emit('summary-item-clicked', button);
        } else {
            this._unsetClickedSummaryItem();
        }

        this._updateState();
    },

    /** Callback for 'notify::hover' of {@link #_summary}, the box layout
     * that contains all the summary items.
     *
     * This just updates {@link #_pointerInSummary} and calls {@link #_updateState}.
     * (Not sure exactly why this is necessary given the hot corner {@link #_corner}
     * and the notify::hover callback for summary *items* {@link #_onSummaryItemHoverChanged};
     * perhaps for the case where there are no summary items in the message tray?)
     */
    _onSummaryHoverChanged: function() {
        this._pointerInSummary = this._summary.hover;
        this._updateState();
    },

    /** Callback when the hover state of the mouse over the message tray changes.
     *
     * If they have just hovered their mouse over the message tray, we set
     * a number of variables ({@link #_pointerInTray},
     * {@link #_userLongerTrayLeftTimeout}, ...) and call {@link #_updateState}
     * to update the message tray (it might need to be expanded, for
     * example). Details as follows (copied from comments in the code,
     * please see ethe code for further detail):
     *
     * * Don't do anything if the hover is while the tray is hidden (the message
     * tray is one pixel in height when hidden);
     * * Don't do anything if `this._useLongerTrayLeftTimeout` is true, meaning the
     * notification originally popped up under the pointer, but
     * `this._trayLeftTimeoutId is 0`, meaning the pointer didn't leave
     * the tray yet.
     * We need to check for this case because sometimes `_onTrayHoverChanged()`
     * gets called  multiple times while `this.actor.hover` is true.
     * * Don't set `this._pointerInTray` to true if the pointer was initially in
     * the area where the notification popped up.
     * That way we will not be expanding notifications that happen to pop up
     * over the pointer automatically.
     * Instead, the user is able to expand the notification by mousing away from
     * it and then mousing back in.
     * Because this is an expected action, we set the boolean flag that
     * indicates that a longer timeout should be used before popping down the
     * notification.
     *
     * Otherwise if the pointer has just left the message tray:
     * * We record the position of the mouse the moment it leaves the tray. 
     * These coordinates are used in `this._onTrayLeftTimeout()` to determine if
     * the mouse has moved far enough during the initial timeout for us
     * to consider that the user intended to leave the tray and therefore hide
     * the tray. If the mouse is still close to its previous position,
     * we extend the timeout once.
     * * We wait just a little before hiding the message tray in case the user
     * quickly moves the mouse back into it.
     * We wait for a longer period if the notification popped up where the mouse
     * pointer was already positioned.
     * That gives the user more time to mouse away from the notification and
     * mouse back in in order to expand it.
     */
    _onTrayHoverChanged: function() {
        if (this.actor.hover) {
            // Don't do anything if the one pixel area at the bottom is hovered over while the tray is hidden.
            if (this._trayState == State.HIDDEN)
                return;

            // Don't do anything if this._useLongerTrayLeftTimeout is true, meaning the notification originally
            // popped up under the pointer, but this._trayLeftTimeoutId is 0, meaning the pointer didn't leave
            // the tray yet. We need to check for this case because sometimes _onTrayHoverChanged() gets called
            // multiple times while this.actor.hover is true.
            if (this._useLongerTrayLeftTimeout && !this._trayLeftTimeoutId)
                return;

            this._useLongerTrayLeftTimeout = false;
            if (this._trayLeftTimeoutId) {
                Mainloop.source_remove(this._trayLeftTimeoutId);
                this._trayLeftTimeoutId = 0;
                this._trayLeftMouseX = -1;
                this._trayLeftMouseY = -1;
                return;
            }

            if (this._showNotificationMouseX >= 0) {
                let actorAtShowNotificationPosition =
                    global.stage.get_actor_at_pos(Clutter.PickMode.ALL, this._showNotificationMouseX, this._showNotificationMouseY);
                this._showNotificationMouseX = -1;
                this._showNotificationMouseY = -1;
                // Don't set this._pointerInTray to true if the pointer was initially in the area where the notification
                // popped up. That way we will not be expanding notifications that happen to pop up over the pointer
                // automatically. Instead, the user is able to expand the notification by mousing away from it and then
                // mousing back in. Because this is an expected action, we set the boolean flag that indicates that a longer
                // timeout should be used before popping down the notification.
                if (this.actor.contains(actorAtShowNotificationPosition)) {
                    this._useLongerTrayLeftTimeout = true;
                    return;
                }
            }
            this._pointerInTray = true;
            this._updateState();
        } else {
            // We record the position of the mouse the moment it leaves the tray. These coordinates are used in
            // this._onTrayLeftTimeout() to determine if the mouse has moved far enough during the initial timeout for us
            // to consider that the user intended to leave the tray and therefore hide the tray. If the mouse is still
            // close to its previous position, we extend the timeout once.
            let [x, y, mods] = global.get_pointer();
            this._trayLeftMouseX = x;
            this._trayLeftMouseY = y;

            // We wait just a little before hiding the message tray in case the user quickly moves the mouse back into it.
            // We wait for a longer period if the notification popped up where the mouse pointer was already positioned.
            // That gives the user more time to mouse away from the notification and mouse back in in order to expand it.
            let timeout = this._useLongerHideTimeout ? LONGER_HIDE_TIMEOUT * 1000 : HIDE_TIMEOUT * 1000;
            this._trayLeftTimeoutId = Mainloop.timeout_add(timeout, Lang.bind(this, this._onTrayLeftTimeout));
        }
    },

    /** Callback when the user hovers (or unhovers) their mouse over
     * the on-screen keyboard, if they are using it
     * ({@link Layout.LayoutManager#keyboardBox}).
     *
     * We call {@link #_updateState}, unless the mouse has moved from the
     * keyboard to another actor in the message tray.
     * {@link Layout.LayoutManager#keyboardBox}.
     */
    _onKeyboardHoverChanged: function(keyboard) {
        this._pointerInKeyboard = keyboard.hover;

        if (!keyboard.hover) {
            let event = Clutter.get_current_event();
            if (event && event.type() == Clutter.EventType.LEAVE) {
                let into = event.get_related();
                if (into && this.actor.contains(into)) {
                    // Don't call _updateState, because pointerInTray is
                    // still false
                    return;
                }
            }
        }

        this._updateState();
    },

    /** Called whenever the user's chat status changes.
     *
     * This sets whether we've just come back from being idle
     * ({@link #_backFromAway}) as well as your current status.
     *
     * Then we call {@link #_updateState} (if you're just back from being
     * away I think it shows you the summary items so you can review notifications
     * you missed, and if you're busy it doesn't show notifications unless
     * they're urgent).
     * @param {GnomeSession.Presence} presence - object emitting the signal
     * ([`this._presence`]{@link #_presence})
     * @param {GnomeSession.PresenceStatus} status - the new status
     */
    _onStatusChanged: function(presence, status) {
        this._backFromAway = (this._userStatus == GnomeSession.PresenceStatus.IDLE && this._userStatus != status);
        this._userStatus = status;

        if (status == GnomeSession.PresenceStatus.BUSY) {
            // remove notification and allow the summary to be closed now
            this._updateNotificationTimeout(0);
            if (this._summaryTimeoutId) {
                Mainloop.source_remove(this._summaryTimeoutId);
                this._summaryTimeoutId = 0;
            }
            this._busy = true;
        } else if (status != GnomeSession.PresenceStatus.IDLE) {
            // We preserve the previous value of this._busy if the status turns to IDLE
            // so that we don't start showing notifications queued during the BUSY state
            // as the screensaver gets activated.
            this._busy = false;
        }

        this._updateState();
    },

    /** Callback either {@link LONGER_HIDE_TIMEOUT} or {@link HIDE_TIMEOUT}
     * after the user's cursor has left the message tray.
     *
     * Essentially if the mouse has moved sufficiently since it first left
     * the message tray we close the message tray, otherwise we hang on for
     * a little longer.
     *
     * If the mouse hasn't moved any further than {@link MOUSE_LEFT_ACTOR_THRESHOLD}
     * left, right or up since it left the message tray, we extend the
     * timeout (i.e. schedule another call to this in {@link LONGER_HIDE_TIMEOUT}
     * seconds) and do nothing else.
     *
     * If the mouse *has* moved over that threshold, we set various state variables
     * to reflect this and call {@link #_updateState} which will hide the
     * message tray.
     */
    _onTrayLeftTimeout: function() {
        let [x, y, mods] = global.get_pointer();
        // We extend the timeout once if the mouse moved no further than MOUSE_LEFT_ACTOR_THRESHOLD to either side or up.
        // We don't check how far down the mouse moved because any point above the tray, but below the exit coordinate,
        // is close to the tray.
        if (this._trayLeftMouseX > -1 &&
            y > this._trayLeftMouseY - MOUSE_LEFT_ACTOR_THRESHOLD &&
            x < this._trayLeftMouseX + MOUSE_LEFT_ACTOR_THRESHOLD &&
            x > this._trayLeftMouseX - MOUSE_LEFT_ACTOR_THRESHOLD) {
            this._trayLeftMouseX = -1;
            this._trayLeftTimeoutId = Mainloop.timeout_add(LONGER_HIDE_TIMEOUT * 1000,
                                                             Lang.bind(this, this._onTrayLeftTimeout));
        } else {
            this._trayLeftTimeoutId = 0;
            this._useLongerTrayLeftTimeout = false;
            this._pointerInTray = false;
            this._pointerInSummary = false;
            this._updateNotificationTimeout(0);
            this._updateState();
        }
        return false;
    },

    /** Called when the user presses "Escape" while the message tray has grabbed
     * focus, or once a notification emits its
     * ['done-displaying']{@link Notification.done-displaying} signal.
     *
     * It basically hides the tray away again (if appropriate; it
     * just sets some state variables and delegates to {@link #_updateState}).
     */
    _escapeTray: function() {
        this._unlock();
        this._pointerInTray = false;
        this._pointerInSummary = false;
        this._updateNotificationTimeout(0);
        this._updateState();
    },

    /** All of the logic for what happens when occurs here; the various
     * event handlers merely update variables such as
     * 'this._pointerInTray', 'this._summaryState', etc, and
     * _updateState() figures out what (if anything) needs to be done
     * at the present time.
     *
     * It handles things like:
     *
     * * showing the next notification from {@link #_notificationQueue}
     * (i.e. {@link Source#notify}), if the user does not currently have
     * their status set to BUSY (or if the notification is critical urgency);
     * * hiding the currently-showing notification if it has timed out;
     * * Showing or hiding the summary items (for example when a notification
     * is notified, the message tray shows but the summary items don't);
     * * Handling the clicked summary item if one was just clicked, by
     *   + showing the summary box pointer (either the right click menu
     *   or the popup menu with the notification list)
     *     for the clicked summary item, if it was a left click and it's not
     *     showing already;
     *   + hiding the summary box pointer if it was a left click and is already
     *     already showing;
     * * showing or hiding the tray if necessary.
     *
     * All of these things have a fair bit of logic to them and usually involve
     * timeouts (wait a little bit before taking action); see the code
     * for further details.
     */
    _updateState: function() {
        // Notifications
        let notificationUrgent = this._notificationQueue.length > 0 && this._notificationQueue[0].urgency == Urgency.CRITICAL;
        let notificationsPending = this._notificationQueue.length > 0 && (!this._busy || notificationUrgent);
        let notificationPinned = this._pointerInTray && !this._pointerInSummary && !this._notificationRemoved;
        let notificationExpanded = this._notificationBin.y < 0;
        let notificationExpired = (this._notificationTimeoutId == 0 && !(this._notification && this._notification.urgency == Urgency.CRITICAL) && !this._pointerInTray && !this._locked && !(this._pointerInKeyboard && notificationExpanded)) || this._notificationRemoved;
        let canShowNotification = notificationsPending && this._summaryState == State.HIDDEN;

        if (this._notificationState == State.HIDDEN) {
            if (canShowNotification)
                this._showNotification();
        } else if (this._notificationState == State.SHOWN) {
            if (notificationExpired)
                this._hideNotification();
            else if (notificationPinned && !notificationExpanded)
                this._expandNotification(false);
            else if (notificationPinned)
                this._ensureNotificationFocused();
        }

        // Summary
        let summarySummoned = this._pointerInSummary || this._overviewVisible ||  this._traySummoned;
        let summaryPinned = this._summaryTimeoutId != 0 || this._pointerInTray || summarySummoned || this._locked;
        let summaryHovered = this._pointerInTray || this._pointerInSummary;
        let summaryVisibleWithNoHover = (this._overviewVisible || this._locked) && !summaryHovered;
        let summaryNotificationIsForExpandedSummaryItem = (this._clickedSummaryItem == this._expandedSummaryItem);

        let notificationsVisible = (this._notificationState == State.SHOWING ||
                                    this._notificationState == State.SHOWN);
        let notificationsDone = !notificationsVisible && !notificationsPending;

        let summaryOptionalInOverview = this._overviewVisible && !this._locked && !summaryHovered;
        let mustHideSummary = (notificationsPending && (notificationUrgent || summaryOptionalInOverview))
                              || notificationsVisible;

        if (this._summaryState == State.HIDDEN && !mustHideSummary) {
            if (this._backFromAway) {
                // Immediately set this to false, so that we don't schedule a timeout later
                this._backFromAway = false;
                if (!this._busy)
                    this._showSummary(LONGER_SUMMARY_TIMEOUT);
            } else if (notificationsDone && this._newSummaryItems.length > 0 && !this._busy) {
                this._showSummary(SUMMARY_TIMEOUT);
            } else if (summarySummoned) {
                this._showSummary(0);
            }
        } else if (this._summaryState == State.SHOWN) {
            if (!summaryPinned || mustHideSummary)
                this._hideSummary();
            else if (summaryVisibleWithNoHover && !summaryNotificationIsForExpandedSummaryItem)
                // If we are hiding the summary, we'll collapse the expanded summary item when we are done
                // so that there is no animation. However, we should collapse the expanded summary item
                // if the summary is visible, but not hovered over, and the summary notification for the
                // expanded summary item is not being shown.
                this._setExpandedSummaryItem(null);
        }

        // Summary notification
        let haveClickedSummaryItem = this._clickedSummaryItem != null;
        let summarySourceIsMainNotificationSource = (haveClickedSummaryItem && this._notification &&
                                                     this._clickedSummaryItem.source == this._notification.source);
        let canShowSummaryBoxPointer = this._summaryState == State.SHOWN;
        // We only have sources with empty notification stacks for legacy tray icons. Currently, we never attempt
        // to show notifications for legacy tray icons, but this would be necessary if we did.
        let requestedNotificationStackIsEmpty = (this._clickedSummaryItemMouseButton == 1 && this._clickedSummaryItem.source.notifications.length == 0);
        let wrongSummaryNotificationStack = (this._clickedSummaryItemMouseButton == 1 &&
                                             this._summaryBoxPointer.bin.child != this._clickedSummaryItem.notificationStackView);
        let wrongSummaryRightClickMenu = (this._clickedSummaryItemMouseButton == 3 &&
                                          this._summaryBoxPointer.bin.child != this._clickedSummaryItem.rightClickMenu);
        let wrongSummaryBoxPointer = (haveClickedSummaryItem &&
                                      (wrongSummaryNotificationStack || wrongSummaryRightClickMenu));

        if (this._summaryBoxPointerState == State.HIDDEN) {
            if (haveClickedSummaryItem && !summarySourceIsMainNotificationSource && canShowSummaryBoxPointer && !requestedNotificationStackIsEmpty)
                this._showSummaryBoxPointer();
        } else if (this._summaryBoxPointerState == State.SHOWN) {
            if (!haveClickedSummaryItem || !canShowSummaryBoxPointer || wrongSummaryBoxPointer || mustHideSummary)
                this._hideSummaryBoxPointer();
        }

        // Tray itself
        let trayIsVisible = (this._trayState == State.SHOWING ||
                             this._trayState == State.SHOWN);
        let trayShouldBeVisible = (!notificationsDone ||
                                   this._summaryState == State.SHOWING ||
                                   this._summaryState == State.SHOWN);
        if (!trayIsVisible && trayShouldBeVisible)
            this._showTray();
        else if (trayIsVisible && !trayShouldBeVisible)
            this._hideTray();
    },

    /** Convenience function to do a tween.
     *
     * This tweens `actor` according to `params` (which are the same as for
     * normal tweening).
     *
     * Upon completion, `statevar` is set to `value` and if `params.onComplete`
     * is present, that is called too (this is all done with convenience function
     * {@link #_tweenComplete}). Also, {@link #_updateState} is called.
     * @param {Clutter.Actor} actor - the actor to tween
     * @param {string} statevar - the property to set upon completion (a string,
     * referenced using `this[statevar] = value`)
     * @param {?} value - the value to set `this[statevar]` to on completion
     * of the tween.
     * @param {Object} params - parameters for the tween, see
     * {@link Tweener.addTween} (with at least properties
     * `time` and `transition`, as well as a property of `actor` to tween
     * to).
     * @see Tweener.addTween
     */
    _tween: function(actor, statevar, value, params) {
        let onComplete = params.onComplete;
        let onCompleteScope = params.onCompleteScope;
        let onCompleteParams = params.onCompleteParams;

        params.onComplete = this._tweenComplete;
        params.onCompleteScope = this;
        params.onCompleteParams = [statevar, value, onComplete, onCompleteScope, onCompleteParams];

        Tweener.addTween(actor, params);

        let valuing = (value == State.SHOWN) ? State.SHOWING : State.HIDING;
        this[statevar] = valuing;
    },

    /** Callback when a tween commenced using {@link #_tween} is completed.
     * This is just a convenience function to set `this[statevar]` to `value`
     * and also execute `params.onComplete` (parameter `onComplete`) if that was
     * specified in {@link #_tween}.
     *
     * Finally {@link #_updateState} is called afterwards.
     * @inheritparams #_tween
     * @param {?function(...)} onComplete - the function that was passed in
     * as `params.onComplete` into {@link #_tween}, if any (i.e. the function
     * the user wanted to run upon tween completion).
     * @param {Object} onCompleteScope - the scope in which to call `onComplete`,
     * which is always `this`.
     * @param {Array} onCompleteParams - array of parameters to call
     * `onComplete` with. Called as in `onComplete.apply(this, onCompleteParams)`.
     * @see #_tween
     */
    _tweenComplete: function(statevar, value, onComplete, onCompleteScope, onCompleteParams) {
        this[statevar] = value;
        if (onComplete)
            onComplete.apply(onCompleteScope, onCompleteParams);
        this._updateState();
    },

    /** Shows the message tray, by sliding it upwards from the bottom of the
     * screen over {@link ANIMATION_TIME}.
     * Upon completion {@link #_trayState} is set to SHOWN and {@link #_updateState}
     * is called to make sure the tray is showing what it should be showing.
     */
    _showTray: function() {
        this._tween(this.actor, '_trayState', State.SHOWN,
                    { y: -this.actor.height,
                      time: ANIMATION_TIME,
                      transition: 'easeOutQuad'
                    });
    },

    /** Hides the message tray by sliding it back down to the bottom of the
     * screen, setting {@link #_trayState} to HIDDEN and calling
     * {@link #_updateState} upon completion.
     */
    _hideTray: function() {
        this._tween(this.actor, '_trayState', State.HIDDEN,
                    { y: this.actor.height,
                      time: ANIMATION_TIME,
                      transition: 'easeOutQuad'
                    });
    },

    /** Shows the first notification in {@link #_notificationQueue}, i.e.
     * notifications that are to be notified via {@link Source#notify}
     * (popped up from the bottom of the screen).
     *
     * This sets that notification to be the child of {@link #_notificationBin}
     * and calls {@link #_updateShowingNotification} which takes care of the
     * "popping up" animation.
     */
    _showNotification: function() {
        this._notification = this._notificationQueue.shift();
        this._notificationClickedId = this._notification.connect('done-displaying',
                                                                 Lang.bind(this, this._escapeTray));
        this._notificationBin.child = this._notification.actor;

        this._notificationBin.opacity = 0;
        this._notificationBin.y = this.actor.height;
        this._notificationBin.show();

        this._updateShowingNotification();

        let [x, y, mods] = global.get_pointer();
        // We save the position of the mouse at the time when we started showing the notification
        // in order to determine if the notification popped up under it. We make that check if
        // the user starts moving the mouse and _onTrayHoverChanged() gets called. We don't
        // expand the notification if it just happened to pop up under the mouse unless the user
        // explicitly mouses away from it and then mouses back in.
        this._showNotificationMouseX = x;
        this._showNotificationMouseY = y;
        // We save the y coordinate of the mouse at the time when we started showing the notification
        // and then we update it in _notifiationTimeout() if the mouse is moving towards the
        // notification. We don't pop down the notification if the mouse is moving towards it.
        this._lastSeenMouseY = y;
    },

    /** Updates what should happen to a notification that has just been notified.
     *
     * We call {@link #_expandNotification} on notifications with CRITICAL
     * urgency and already-expanded notifications (see code for detail in
     * the comments).
     *
     * We tween all notifications to full opacity. This ensures that both new notifications and
     * notifications that might have been in the process of hiding get full opacity.
     *
     * We tween any notification showing in the banner mode to banner height.
     * This ensures that both new notifications and notifications in the banner mode that might
     * have been in the process of hiding are shown with the banner height.
     *
     * We use {@link _showNotificationCompleted} on completion of the tween
     * to extend the time the updated notification is being shown.
     *
     * We don't set the y parameter for the tween for expanded notifications because
     * {@link #_expandNotification} will result in getting
     * `this._notificationBin.y` set to the appropriate
     * fully expanded value.
     */
    _updateShowingNotification: function() {
        Tweener.removeTweens(this._notificationBin);

        // We auto-expand notifications with CRITICAL urgency.
        // We use Tweener.removeTweens() to remove a tween that was hiding the notification we are
        // updating, in case that notification was in the process of being hidden. However,
        // Tweener.removeTweens() would also remove a tween that was updating the position of the
        // notification we are updating, in case that notification was already expanded and its height
        // changed. Therefore we need to call this._expandNotification() for expanded notifications
        // to make sure their position is updated.
        if (this._notification.urgency == Urgency.CRITICAL || this._notification.expanded)
            this._expandNotification(true);

        // We tween all notifications to full opacity. This ensures that both new notifications and
        // notifications that might have been in the process of hiding get full opacity.
        //
        // We tween any notification showing in the banner mode to banner height (this._notificationBin.y = 0).
        // This ensures that both new notifications and notifications in the banner mode that might
        // have been in the process of hiding are shown with the banner height.
        //
        // We use this._showNotificationCompleted() onComplete callback to extend the time the updated
        // notification is being shown.
        //
        // We don't set the y parameter for the tween for expanded notifications because
        // this._expandNotification() will result in getting this._notificationBin.y set to the appropriate
        // fully expanded value.
        let tweenParams = { opacity: 255,
                            time: ANIMATION_TIME,
                            transition: 'easeOutQuad',
                            onComplete: this._showNotificationCompleted,
                            onCompleteScope: this
                          };
        if (!this._notification.expanded)
            tweenParams.y = 0;

        this._tween(this._notificationBin, '_notificationState', State.SHOWN, tweenParams);
   },

    /** Callback upon completion of showing a notification (in
     * {@link #_updateShowingNotification}).
     *
     * If the urgency of the notification is not critical we set its timeout
     * (period after which it will hide/expire) to {@link NOTIFICATION_TIMEOUT}
     * seconds (otherwise it does not have a timeout and will stay until it
     * is dismissed (I think)).
     */
    _showNotificationCompleted: function() {
        if (this._notification.urgency != Urgency.CRITICAL)
            this._updateNotificationTimeout(NOTIFICATION_TIMEOUT * 1000);
    },

    /** Sets the delay before the currently-notified notification times out,
     * i.e. hides itself. {@link #_notificationTimeout} is called once the
     * notification expires (to actually make it expire).
     *
     * @param {number} timeout - milliseconds after which the notification
     * should expire.
     * @see #_notificationTimeout
     */
    _updateNotificationTimeout: function(timeout) {
        if (this._notificationTimeoutId) {
            Mainloop.source_remove(this._notificationTimeoutId);
            this._notificationTimeoutId = 0;
        }
        if (timeout > 0)
            this._notificationTimeoutId =
                Mainloop.timeout_add(timeout,
                                     Lang.bind(this, this._notificationTimeout));
    },

    /** Callback when a notification that has been notified expires
     * (set the expiry time using {@link #_updateNotificationTimeout}).
     *
     * If the mouse is moving towards the notification we don't hide it yet
     * but delay another 1 second.
     *
     * Otherwise, we remove the timeout ID we had stored in
     * {@link #_updateNotificationTimeout} and call {@link #_updateState}
     * which uses this to detect that the notification has expired and
     * removes it (unless it's of critical urgency).
     * @see #_updateNotificationTimeout
     */
    _notificationTimeout: function() {
        let [x, y, mods] = global.get_pointer();
        if (y > this._lastSeenMouseY + 10 && !this.actor.hover) {
            // The mouse is moving towards the notification, so don't
            // hide it yet. (We just create a new timeout (and destroy
            // the old one) each time because the bookkeeping is
            // simpler.)
            this._lastSeenMouseY = y;
            this._updateNotificationTimeout(1000);
        } else {
            this._notificationTimeoutId = 0;
            this._updateState();
        }

        return false;
    },

    /** Hides the just-notified notification.
     *
     * This:
     *
     * * ungrabs focus (see {@link FocusGrabber#ungrabFocus});
     * * stops listening to various signals we were listening to on the
     * notification (like content updates)
     * * slides the notification down and off the screen and fades it out
     * over {@link ANIMATION_TIME}, setting {@link #_notificationState}
     * to HIDDEN upon completion and calling {@link #_updateState} and
     * {@link #_hideNotificationCompleted}.
     * @see #_hideNotificationCompleted
     */
    _hideNotification: function() {
        this._focusGrabber.ungrabFocus();
        if (this._notificationExpandedId) {
            this._notification.disconnect(this._notificationExpandedId);
            this._notificationExpandedId = 0;
        }

        this._tween(this._notificationBin, '_notificationState', State.HIDDEN,
                    { y: this.actor.height,
                      opacity: 0,
                      time: ANIMATION_TIME,
                      transition: 'easeOutQuad',
                      onComplete: this._hideNotificationCompleted,
                      onCompleteScope: this
                    });
    },

    /** Called upon completion of hiding the just-notified notification
     * (ie after it's slidden down/faded away).
     *
     * This calls {@link Notification#collapsCompleted} on the notification
     * and resets {@link #_notification} to 0.
     *
     * Additionally if the notification was transient, it is then destroyed
     * with reason {@link MessageTray.NotificationDestroyedReason.EXPIRED}
     * (recall that a transient notification is destroyed once it has finished
     * being notified and hence cannot be accessed from its source afterwards
     * like non-transient notifications).
     * @see #_hideNotification
     */
    _hideNotificationCompleted: function() {
        this._notificationRemoved = false;
        this._notificationBin.hide();
        this._notificationBin.child = null;
        this._notification.collapseCompleted();
        this._notification.disconnect(this._notificationClickedId);
        this._notificationClickedId = 0;
        let notification = this._notification;
        this._notification = null;
        if (notification.isTransient)
            notification.destroy(NotificationDestroyedReason.EXPIRED);
    },

    /** This expands the notification that is currently being notified (i.e.
     * popped up from the middle of the bottom of the screen)
     *
     * ![A {@link Notification} that's just been notified](pics/NotificationBannerMode.png)
     * ![The {@link Notification} after expansion](pics/NotificationExpanded.png)
     *
     * If the notification is not autoexpanding we grab focus to it.
     *
     * Then we call its {@link Notification#expand} method (not animating if
     * the notification is auto-expanding), calling {@link #_onNotificationExpanded}
     * once complete (which is what slides the notification up to show the
     * expanded content).
     * 
     * @param {boolean} autoExpanding - whether the notfication is auto-expanded.
     * CRITICAL urgency notifications are autoexpanded (and for some reason
     * these are not animated when {@link Notification#expand} is called).
     */
    _expandNotification: function(autoExpanding) {
        // Don't grab focus in notifications that are auto-expanded.
        if (!autoExpanding)
            this._focusGrabber.grabFocus(this._notification.actor);

        if (!this._notificationExpandedId)
            this._notificationExpandedId =
                this._notification.connect('expanded',
                                           Lang.bind(this, this._onNotificationExpanded));
        // Don't animate changes in notifications that are auto-expanding.
        this._notification.expand(!autoExpanding);
    },

    /** Callback to the currently-being-notified notification's
     * ['expanded']{@link Notification.expanded} signal, i.e. when
     * {@link Notification#expand} has been called.
     *
     * This animates sliding the notification up to show its entire expanded
     * content (e.g. the summary text of the notification).
     *
     * ![An expanded {@link Notification}](pics/NotificationExpandedMode.png)
     */
    _onNotificationExpanded: function() {
        let expandedY = this.actor.height - this._notificationBin.height;

        // Don't animate the notification to its new position if it has shrunk:
        // there will be a very visible "gap" that breaks the illusion.

        if (this._notificationBin.y < expandedY)
            this._notificationBin.y = expandedY;
        else if (this._notification.y != expandedY)
            this._tween(this._notificationBin, '_notificationState', State.SHOWN,
                        { y: expandedY,
                          time: ANIMATION_TIME,
                          transition: 'easeOutQuad'
                        });
   },

    /** We use this function to grab focus when the user moves the pointer
     * to a notification with CRITICAL urgency that was already auto-expanded.
     *
     * just makes sure the notification's actor has focus grabbed to it.
     */
    _ensureNotificationFocused: function() {
        this._focusGrabber.grabFocus(this._notification.actor);
    },

    /** Shows the box holding all the summary items by tweening its opacity from
     * 0 to 255, and by "sliding" it upwards into the message tray over
     * {@link ANIMATION_TIME} seconds.
     *
     * {@link #_showSummaryCompleted} is called upon completion, and if
     * `timeout` is provided {@link #_summaryTimeout} will be called that
     * many seconds after the animation is complete (which just calls
     * {@link #_updateState} to refresh the message tray).
     *
     * @inheritparams #_showSummaryCompleted
     */
    _showSummary: function(timeout) {
        this._summaryBin.opacity = 0;
        this._summaryBin.y = this.actor.height;
        this._tween(this._summaryBin, '_summaryState', State.SHOWN,
                    { y: 0,
                      opacity: 255,
                      time: ANIMATION_TIME,
                      transition: 'easeOutQuad',
                      onComplete: this._showSummaryCompleted,
                      onCompleteScope: this,
                      onCompleteParams: [timeout]
                    });
    },

    /** Callback when we are done showing the box with all the summary items.
     *
     * This clears {@link #_newSummaryItems}, and if `timeout` is not 0
     * we schedule a call to {@link #_summaryTimeout} that many seconds later
     * (which itself just calls {@link #_updateState}).
     *
     * @param {number} timeout - seconds after the summary animation has
     * completed after which to call {@link #_summaryTimeout}.
     * @see #_summaryTimeout
     */
    _showSummaryCompleted: function(timeout) {
        this._newSummaryItems = [];

        if (timeout != 0) {
            this._summaryTimeoutId =
                Mainloop.timeout_add(timeout * 1000,
                                     Lang.bind(this, this._summaryTimeout));
        }
    },

    /** Called sum number of seconds after showing the summary is completed.
     * This calls {@link #_updateState} to update the state of elements of
     * the message tray.
     * @see #_showSummaryCompleted
     */
    _summaryTimeout: function() {
        this._summaryTimeoutId = 0;
        this._updateState();
        return false;
    },

    /** This hides box holding the summary items (for example when
     * a notification is being notified the message tray shows but the
     * summary items do not). It does so by tweening the summary items'
     * opacity to 0 over time {@link ANIMATION_TIME}, setting
     * {@link #_summaryState} to HIDDEN on completion (and calling
     * {@link #_hideSummaryCompleted} and then {@link #_updateState}).
     */
    _hideSummary: function() {
        this._tween(this._summaryBin, '_summaryState', State.HIDDEN,
                    { opacity: 0,
                      time: ANIMATION_TIME,
                      transition: 'easeOutQuad',
                      onComplete: this._hideSummaryCompleted,
                      onCompleteScope: this,
                    });
        this._newSummaryItems = [];
    },

    /** Callback when the hiding of the box holding all the summary items is
     * complete. This unsets the expanded summary item.
     * @see #_hideSummary
     */
    _hideSummaryCompleted: function() {
        this._setExpandedSummaryItem(null);
    },

    /** Shows the summary box pointer from the just-clicked summary item.
     * Recall that this is the popup menu displaying either the summary item's
     * notification stack (if the user left-clicked the summary item), or
     * its right click menu (if the user right-clicked).
     *
     * This sets {@link #_summaryBoxPointerItem} to {@link #_clickedSummaryItem},
     * and connects to various of its signals
     * (['content-updated']{@link SummaryItem.content-updated},
     * ['done-displaying-content']{@link SummaryItem.done-displaying-content}
     * to update the visibility/position of the popup menu and so on.
     *
     * It adds style class 'selected' to the selected item and also sets
     * state variable {@link #_summaryBoxPointerItem}.
     *
     * @todo pic of the summary box pointer to remind the user.
     */
    _showSummaryBoxPointer: function() {
        this._summaryBoxPointerItem = this._clickedSummaryItem;
        this._summaryBoxPointerContentUpdatedId = this._summaryBoxPointerItem.connect('content-updated',
                                                                                      Lang.bind(this, this._onSummaryBoxPointerContentUpdated));
        this._summaryBoxPointerDoneDisplayingId = this._summaryBoxPointerItem.connect('done-displaying-content',
                                                                                      Lang.bind(this, this._escapeTray));
        if (this._clickedSummaryItemMouseButton == 1) {
            this._notificationQueue = this._notificationQueue.filter( Lang.bind(this,
                function(notification) {
                    return this._summaryBoxPointerItem.source != notification.source;
                }));
            this._summaryBoxPointerItem.prepareNotificationStackForShowing();
            this._summaryBoxPointer.bin.child = this._summaryBoxPointerItem.notificationStackView;
            this._summaryBoxPointerItem.scrollTo(St.Side.BOTTOM);
        } else if (this._clickedSummaryItemMouseButton == 3) {
            this._summaryBoxPointer.bin.child = this._clickedSummaryItem.rightClickMenu;
        }

        this._focusGrabber.grabFocus(this._summaryBoxPointer.bin.child);

        this._clickedSummaryItemAllocationChangedId =
            this._clickedSummaryItem.actor.connect('allocation-changed',
                                                   Lang.bind(this, this._adjustSummaryBoxPointerPosition));
        // _clickedSummaryItem.actor can change absolute position without changing allocation
        this._summaryMotionId = this._summary.connect('allocation-changed',
                                                      Lang.bind(this, this._adjustSummaryBoxPointerPosition));
        this._trayMotionId = Main.layoutManager.trayBox.connect('notify::anchor-y',
                                                                Lang.bind(this, this._adjustSummaryBoxPointerPosition));

        this._summaryBoxPointer.actor.opacity = 0;
        this._summaryBoxPointer.actor.show();
        this._adjustSummaryBoxPointerPosition();

        this._summaryBoxPointerState = State.SHOWING;
        this._clickedSummaryItem.actor.add_style_pseudo_class('selected');
        this._summaryBoxPointer.show(true, Lang.bind(this, function() {
            this._summaryBoxPointerState = State.SHOWN;
        }));
    },

    /** Callback when the summary box pointer item emits a
     * ['content-updated']{@link SummaryItem.content-updated} signal,
     * i.e. when one of its notifications is changed, expanded, or removed.
     *
     * This hides the box pointer if there are no notifications left,
     * and also calls {@link #_adjustSummaryBoxPointerPosition} to make sure
     * the pointer is still centred with respect to the popup/summary item.
     */
    _onSummaryBoxPointerContentUpdated: function() {
        if (this._summaryBoxPointerItem.notificationStack.get_children().length == 0)
            this._hideSummaryBoxPointer();
        this._adjustSummaryBoxPointerPosition();

    },

    /** This function simply ensures that the 'pointer' bit of the summary box
     * pointer is centred with respect to the summary item it's popping up
     * from, as well as centred with respect to the bit popping up.
     *
     * Called whenever the summary item or its popup menu changes size/position.
     */
    _adjustSummaryBoxPointerPosition: function() {
        // The position of the arrow origin should be the same as center of this._clickedSummaryItem.actor
        if (!this._clickedSummaryItem)
            return;

        this._summaryBoxPointer.setPosition(this._clickedSummaryItem.actor, 0, 0.5);
    },

    /** Unset the currently-clicked summary item.
     *
     * This removes the 'selected' style class from its actor, and unsets
     * {@link #_clickedSummaryItem} and
     * {@link #_clickedSummaryItemMouseButton}.
     */
    _unsetClickedSummaryItem: function() {
        if (this._clickedSummaryItemAllocationChangedId) {
            this._clickedSummaryItem.actor.disconnect(this._clickedSummaryItemAllocationChangedId);
            this._summary.disconnect(this._summaryMotionId);
            Main.layoutManager.trayBox.disconnect(this._trayMotionId);
            this._clickedSummaryItemAllocationChangedId = 0;
            this._summaryMotionId = 0;
            this._trayMotionId = 0;
        }

        if (this._clickedSummaryItem)
            this._clickedSummaryItem.actor.remove_style_pseudo_class('selected');
        this._clickedSummaryItem = null;
        this._clickedSummaryItemMouseButton = -1;
    },

    /** Hides the summary box pointer, animating if appropriate.
     *
     * We set {@link #_summaryBoxPointerState} to
     * {@link MessageTray.State.HIDING}.
     *
     * If we are no longer showing the summary or if
     * {@link #_clickedSummaryItem} is still the item associated with the
     * currently showing box pointer, we unset the it
     * ({@link #_unsetClickedSummaryItem}).
     *
     * We ungrab focus if it was grabbed ({@link FocusGrabber#ungrabFocus}).
     *
     * Then if the relevant {@link SummaryItem} has no remaining notifications,
     * we hide it instantly (without animation) and call
     * {@link #_hideSummaryBoxPointerCompleted}.
     *
     * Otherwise we call {@link BoxPointer.BoxPointer#hide} to animate
     * sliding the popup menu closed, and call
     * {@link #_hideSummaryBoxPointerCompleted} on completion of the
     * animation.
     * @see BoxPointer.BoxPointer#hide
     * @see #_hideSummaryBoxPointerCompleted
     */
    _hideSummaryBoxPointer: function() {
        // We should be sure to hide the box pointer if all notifications in it are destroyed while
        // it is hiding, so that we don't show an an animation of an empty blob being hidden.
        if (this._summaryBoxPointerState == State.HIDING &&
            this._summaryBoxPointerItem.notificationStack.get_children().length == 0) {
            this._summaryBoxPointer.actor.hide();
            return;
        }

        this._summaryBoxPointerState = State.HIDING;
        // Unset this._clickedSummaryItem if we are no longer showing the summary or if
        // this._clickedSummaryItem is still the item associated with the currently showing box pointer
        if (this._summaryState != State.SHOWN || this._summaryBoxPointerItem == this._clickedSummaryItem)
            this._unsetClickedSummaryItem();

        this._focusGrabber.ungrabFocus();
        if (this._summaryBoxPointerItem.source.notifications.length == 0) {
            this._summaryBoxPointer.actor.hide();
            this._hideSummaryBoxPointerCompleted();
        } else {
            this._summaryBoxPointer.hide(true, Lang.bind(this, this._hideSummaryBoxPointerCompleted));
        }
    },

    /** Called on completion of hiding the summary notification stack for an item
     * ({@link #_hideSummaryBoxPointer}).
     *
     * This is mostly cleanup - we clear the summary box pointer's content and
     * disconnect its signals. If the source whose notification stack we just
     * viewed was transient, we destroy it.
     *
     * Finally we call {@link #_updateState} in case anything else needs to
     * be done (e.g. the user has clicked some other summary item in the meantime).
     * @see #_hideSummaryBoxPointer
     */
    _hideSummaryBoxPointerCompleted: function() {
        let doneShowingNotificationStack = (this._summaryBoxPointer.bin.child == this._summaryBoxPointerItem.notificationStackView);

        this._summaryBoxPointerState = State.HIDDEN;
        this._summaryBoxPointer.bin.child = null;
        this._summaryBoxPointerItem.disconnect(this._summaryBoxPointerContentUpdatedId);
        this._summaryBoxPointerContentUpdatedId = 0;
        this._summaryBoxPointerItem.disconnect(this._summaryBoxPointerDoneDisplayingId);
        this._summaryBoxPointerDoneDisplayingId = 0;

        let sourceNotificationStackDoneShowing = null;
        if (doneShowingNotificationStack) {
            this._summaryBoxPointerItem.doneShowingNotificationStack();
            sourceNotificationStackDoneShowing = this._summaryBoxPointerItem.source;
        }

        this._summaryBoxPointerItem = null;

        if (sourceNotificationStackDoneShowing) {
            if (sourceNotificationStackDoneShowing.isTransient && !this._reNotifyAfterHideNotification)
                sourceNotificationStackDoneShowing.destroy(NotificationDestroyedReason.EXPIRED);
            if (this._reNotifyAfterHideNotification) {
                this._onNotify(this._reNotifyAfterHideNotification.source, this._reNotifyAfterHideNotification);
                this._reNotifyAfterHideNotification = null;
            }
        }

        if (this._clickedSummaryItem)
            this._updateState();
    }
};

/** creates a new SystemNotificationSource
 *
 * This just calls the parent constructor with title "System Information" and
 * sets the icon to the stock 'dialog-information' (symbolic).
 * @classdesc
 * This is a generic source that just has a 'dialog-information' icon for its
 * icon, and title "System Information". When its notification is opened the
 * whole thing is destroyed.
 *
 * ![Icon for the {@link SystemNotificationSource}, 'dialog-information'](pics/SystemNotificationSource.Icon.png)
 *
 * It is used by {@link Main.notify} amongst others.
 * @class
 * @extends Source
 */
function SystemNotificationSource() {
    this._init();
}

SystemNotificationSource.prototype = {
    __proto__:  Source.prototype,

    _init: function() {
        Source.prototype._init.call(this, _("System Information"));

        this._setSummaryIcon(this.createNotificationIcon());
    },

    /** Implements the parent function.
     * Returns a St.Icon that is the symbolic version of 'dialog-information'.
     * @override */
    createNotificationIcon: function() {
        return new St.Icon({ icon_name: 'dialog-information',
                             icon_type: St.IconType.SYMBOLIC,
                             icon_size: this.ICON_SIZE });
    },

    /** Overrides the parent function. Whenever the source/notification is opened
     * it is destroyed/removed. */
    open: function() {
        this.destroy();
    }
};
