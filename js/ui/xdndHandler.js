// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * GNOME-shell handling of
 * [Xdnd](http://en.wikipedia.org/wiki/X_Window_selection): dragging and
 * dropping things between an X window and a gnome-shell object.
 *
 * For example, if you try to drag a file from Nautilus over the Overview
 * button, `panel.js` uses `xdndHandler` to detect this and trigger the
 * overview.
 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const DND = imports.ui.dnd;

/** creates a new XdndHandler
 * Sets up Xdnd and passes through signals. 
 * When a non-gnome-shell object is first dragged over a gnome-shell object,
 * the handler fires a 'drag-begin' signal. 
 * When the object being dragged leaves the gnome-shell object,
 * the 'drag-end' signal is fired. 
 *
 * I think it somehow incorporates with the `dnd.js` code too whereby a
 * draggable target/object registered with `dnd.js` has the appropriate events
 * called on it (?).
 *
 * Use `Main.xdndHandler` to access the instance of the handler and connect to its signals.
 * @class
 */
function XdndHandler() {
    this._init();
}

XdndHandler.prototype = {
    _init: function() {
        // Used to display a clone of the cursor window when the
        // window group is hidden (like it happens in the overview)
        this._cursorWindowClone = null;

        // Used as a drag actor in case we don't have a cursor window clone
        this._dummy = new Clutter.Rectangle({ width: 1, height: 1, opacity: 0 });
        global.stage.add_actor(this._dummy);
        this._dummy.hide();

        // Mutter delays the creation of the output window as long
        // as possible to avoid flicker. In case a plugin wants to
        // access it directly it has to connect to the stage's show
        // signal. (see comment in compositor.c:meta_compositor_manage_screen)
        global.stage.connect('show', function () {
                                        global.init_xdnd();
                                        return false;
                                      });

        global.connect('xdnd-enter', Lang.bind(this, this._onEnter));
        global.connect('xdnd-position-changed', Lang.bind(this, this._onPositionChanged));
        global.connect('xdnd-leave', Lang.bind(this, this._onLeave));

        this._windowGroupVisibilityHandlerId = 0;
    },

    /** Called when the user cancels the drag (i.e release the button), called
     * when `xdnd-leave` is triggered. */
    _onLeave: function() {
        if (this._windowGroupVisibilityHandlerId != 0) {
            global.window_group.disconnect(this._windowGroupVisibilityHandlerId);
            this._windowGroupVisibilityHandlerId = 0;
        }
        if (this._cursorWindowClone) {
            this._cursorWindowClone.destroy();
            this._cursorWindowClone = null;
        }

        this.emit('drag-end');
    },

    /** Called when `xdnd-enter` is triggered, i.e. when the user *starts* an
     * Xdnd drag.
     */
    _onEnter: function() {
        this._windowGroupVisibilityHandlerId  =
                global.window_group.connect('notify::visible',
                    Lang.bind(this, this._onWindowGroupVisibilityChanged));

        this.emit('drag-begin', global.get_current_time());
    },

    /** callback when `global.window_group`'s visibility changes (unsure
     * what it does) */
    _onWindowGroupVisibilityChanged: function() {
        if (!global.window_group.visible) {
            if (this._cursorWindowClone)
                return;

            let windows = global.get_window_actors();
            let cursorWindow = windows[windows.length - 1];

            // FIXME: more reliable way?
            if (!cursorWindow.is_override_redirect())
                return;

            let constraint_position = new Clutter.BindConstraint({ coordinate : Clutter.BindCoordinate.POSITION,
                                                                   source: cursorWindow});

            this._cursorWindowClone = new Clutter.Clone({ source: cursorWindow });
            global.overlay_group.add_actor(this._cursorWindowClone);
            Shell.util_set_hidden_from_pick(this._cursorWindowClone, true);

            // Make sure that the clone has the same position as the source
            this._cursorWindowClone.add_constraint(constraint_position);
        } else {
            if (this._cursorWindowClone) {
                this._cursorWindowClone.destroy();
                this._cursorWindowClone = null;
            }
        }
    },

    /** callback for 'xdnd-position-changed', i.e. when the position of the
     * cursor/thing being dragged changes while the user is dragging.
     *
     * It first sees if there is any Clutter.Actor that the user is holding the
     * draggable over, and then uses the {@link DND} code to
     * see if the item knows how to accept a drag over
     * ({@link DND._Draggable#handleDragOver})
     *
     * This is how gnome-shell objects interact with dragged Xdnd objects (which
     * are not necessarily gnome-shell objects).
     *
     * @param {?} obj - unsure what this is.
     * @param {number} x - x position of the cursor/draggable (relative to the
     * stage).
     * @param {number} y - y position of the cursor/draggable (relative to the
     * stage).
     */
    _onPositionChanged: function(obj, x, y) {
        let pickedActor = global.stage.get_actor_at_pos(Clutter.PickMode.ALL, x, y);

        // Make sure that the cursor window is on top
        if (this._cursorWindowClone)
             this._cursorWindowClone.raise_top();

        let dragEvent = {
            x: x,
            y: y,
            dragActor: this._cursorWindowClone ? this._cursorWindowClone : this._dummy,
            source: this,
            targetActor: pickedActor
        };

        for (let i = 0; i < DND.dragMonitors.length; i++) {
            let motionFunc = DND.dragMonitors[i].dragMotion;
            if (motionFunc) {
                let result = motionFunc(dragEvent);
                if (result != DND.DragMotionResult.CONTINUE)
                    return;
            }
        }

        while (pickedActor) {
                if (pickedActor._delegate && pickedActor._delegate.handleDragOver) {
                    let result = pickedActor._delegate.handleDragOver(this,
                                                                      dragEvent.dragActor,
                                                                      x,
                                                                      y,
                                                                      global.get_current_time());
                    if (result != DND.DragMotionResult.CONTINUE)
                        return;
                }
                pickedActor = pickedActor.get_parent();
        }
    }
}

Signals.addSignalMethods(XdndHandler.prototype);
