// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Classes that handle searching and providing results (that are then
 * displayed in the overview by `searchDisplay.js` classes).
 *
 * The {@link SearchProvider}s search one source for results matching search
 * terms and should be implemented (see {@link AppDisplay.AppSearchProvider},
 * {@link AppDisplay.SettingsSearchProvider},
 * {@link PlaceDisplay.PlaceSearchProvider} and so on); the
 * {@link SearchSystem} collects results from many search providers.
 *
 * A {@link SearchDisplay.SearchResults} is what *displays* the results from
 * a {@link SearchSystem} (with {@link SearchDisplay.GridSearchResults} displaying
 * results for each provider).
 */

/**
 * The object used in {@link SearchProvider} to describe a result in a search.
 * @typedef ResultMeta
 * @type {Object}
 * @property {string} id - an ID for the search result, should be unique amongst
 * search results (for example document URI for a document, ...)
 * @property {string} name - a name for a search result (e.g. document name).
 * @property {function(number): Clutter.Texture} createIcon - a function that
 * takes in a size (pixels) and returns an icon (some sort of Clutter.Texture,
 * like a St.Icon) representing the search result.
 * @see SearchProvider#getResultMeta
 */
/**
 * The object used to describe an open search provider in an
 * {@link OpenSearchSystem}.
 * @typedef OpenSearchProvider
 * @type {Object}
 * @property {string} name - the name of the search provider ("Wikipedia",
 *  "Google").
 * @property {string} url - the url for the provider.
 * @property {number} id - the index of the provider in the search system.
 * @property {string} icon_uri - the URI to the icon for that provider.
 * @property {string[]} langs - the languages that provider supports (as
 * language codes, like 'en').
 * @see OpenSearchSystem
 */

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Signals = imports.signals;
const Shell = imports.gi.Shell;
const Util = imports.misc.util;

const FileUtils = imports.misc.fileUtils;
const Main = imports.ui.main;

const DISABLED_OPEN_SEARCH_PROVIDERS_KEY = 'disabled-open-search-providers';

// Not currently referenced by the search API, but
// this enumeration can be useful for provider
// implementations.
/**
 * Enum describing the type of search result. Vaguely in ascending order of
 * relevance.
 *
 * Unsure if the following descriptions are correct, kind of short on examples
 * of their use (as far as I can tell, just
 * {@link PlaceDisplay.PlaceInfo#matchTerms} and
 * {@link DocInfo.DocInfo#matchTerms} use this enum).
 *
 * @type {number}
 * @enum
 * @const
 */
const MatchType = {
    /** no match at all */
    NONE: 0,
    /** A term is a substring of the result */
    SUBSTRING: 1,
    /** Multiple terms are substrings of the result */
    MULTIPLE_SUBSTRING: 2,
    /** A term matches the start of the result */
    PREFIX: 3,
    /** Multiple terms match the start of the result?? */
    MULTIPLE_PREFIX: 4
};

/**
 * Creates a new SearchResultDisplay
 * @param {Search.SearchProvider} provider - the provider for the display, some sort
 * of {@link SearchProvider}.
 * @classdesc This is in charge of displaying the results of a search from
 * a provider. It should be subclassed. In particular:
 *
 * * {@link #renderResults}
 * * {@link #getVisibleResultCount}
 * * {@link #activateResults}
 * * {@link #selectIndex}
 *
 * @see SearchDisplay.GridSearchResults
 * @abstract
 * @class
 */
function SearchResultDisplay(provider) {
    this._init(provider);
}

SearchResultDisplay.prototype = {
    _init: function(provider) {
        /** the provider for the results */
        this.provider = provider;
        /** the actor displaying the results (it should be some sort of
         * Clutter.Container like a St.BoxLayout, ... (basically, should have
         * the method `get_children`)).
         */
        this.actor = null;
        /** the index of the selected actor */
        this.selectionIndex = -1;
    },

    /**
     * Display the given search matches which resulted
     * from the given terms.  It's expected that not
     * all results will fit in the space for the container
     * actor; in this case, show as many as makes sense
     * for your result type.
     *
     * The terms are useful for search match highlighting.
     *
     * Should be implemented by the subclass.
     *
     * @param {String[]} results - List of identifier strings
     * @param {String[]} terms - List of search term strings
     */
    renderResults: function(results, terms) {
        throw new Error('Not implemented');
    },

    /** Remove all results from this display and reset the selection index. */
    clear: function() {
        this.actor.get_children().forEach(function (actor) { actor.destroy(); });
        this.selectionIndex = -1;
    },

    /** Returns the index of the selected actor, or -1 if none.
     * @returns {string} `this.selectionIndex` being the index of the currently-
     * selected actor.
     */
    getSelectionIndex: function() {
        return this.selectionIndex;
    },

    /** gets the count of results visible in the container actor.
     *
     * Should be implemented by the subclass.
     * @returns {number} the number of actors visible.
     */
    getVisibleResultCount: function() {
        throw new Error('Not implemented');
    },

    /**
     * Move the selection to the given index.
     * @param {number} index - Integer index of the actor to be selected.
     * @return {boolean} true if successful, false if no more results available.
     *
     * Should be implemented by the subclass.
     */
    selectIndex: function() {
        throw new Error('Not implemented');
    },

    /**
     * Activate the currently selected search result.
     *
     * Should be implemented by the subclass.
     * @see #getSelectionIndex
     */
    activateSelected: function() {
        throw new Error('Not implemented');
    }
};

/**
 * creates a new Search Provider.
 * @param {string} title - the title for that search provider (like 'CONTACTS',
 * 'SETTINGS', 'RECENT DOCUMENTS', ...).
 *
 * @classdesc Provides search results.
 * Subclass this object to add a new result type
 * to the search system, then call {@link SearchSystem#registerProvider} with an
 * instance.
 *
 * These functions must be implemented by the subclass:
 *
 * * {@link #getInitialResultSet}
 * * {@link #getSubsearchResultSet}
 * * {@link #getResultMeta}
 * * {@link #activateResult}
 *
 * These may optionally be implemented:
 *
 * * {@link #createResultContainerActor}
 * * {@link #createResultActor}
 *
 * @todo the 'see' should be for the class documentation, not the constructor.
 * @see DocDisplay.DocSearchProvider
 * @see ContactDisplay.ContactSearchProvider
 * @abstract
 * @class
 */
function SearchProvider(title) {
    this._init(title);
}

SearchProvider.prototype = {
    _init: function(title) {
        /** The title for the SearchProvider as appears in the Overview
         * (like "APPLICATIONS", "SETTINGS", ...)
         * @type {string} */
        this.title = title;
        /** The {@link Search.SearchSystem} that the provider is registered to.
         * `null` if we are not yet registered.
         * @see SearchSystem#registerProvider
         * @type {Search.SearchSystem} */
        this.searchSystem = null;
        this.searchAsync  = false;
    },

    /** called when an asynchronous search is cancelled.
     * Should be implemented by the subclass. */
    _asyncCancelled: function() {
    },

    /** called when an asynchronous search is started (?). */
    startAsync: function() {
        this.searchAsync = true;
    },

    /** called to attempt cancelling an asynchronous search.
     * @see #_asyncCancelled */
    tryCancelAsync: function() {
        if (!this.searchAsync)
            return;
        this._asyncCancelled();
        this.searchAsync = false;
    },

    /** Adds items to the current search results.
     * 
     * This should be used for something that requires a bit more
     * logic; it's designed to be an asyncronous way to add a result
     * to the current search.
     *
     * @param {string[]} items - an array of result identifier strings
     * representing items which match the last given search terms.
     */
    addItems: function(items) {
        if (!this.searchSystem)
            throw new Error('Search provider not registered');

        if (!items.length)
            return;

        this.tryCancelAsync();

        this.searchSystem.addProviderItems(this, items);
    },

    /**
     * Called when the user first begins a search (most likely
     * therefore a single term of length one or two), or when
     * a new term is added.
     *
     * Should be implemented by the subclass.
     *
     * @param {string[]} terms - Array of search terms, treated as logical AND
     * @return {string[]} an array of result identifier strings representing
     * items which match the given search terms.  This is expected to be a
     * substring match on the metadata for a given item.
     * Ordering of returned results is up to the discretion of the provider,
     * but you should follow these heruistics:
     *
     * * Put items where the term matches multiple criteria (e.g. name and
     *    description) before single matches
     * * Put items which match on a prefix before non-prefix substring matches
     *
     * This function should be fast; do not perform unindexed full-text searches
     * or network queries.
     */
    getInitialResultSet: function(terms) {
        throw new Error('Not implemented');
    },

    /**
     * Called when a search is performed which is a "subsearch" of
     * the previous search; i.e. when every search term has exactly
     * one corresponding term in the previous search which is a prefix
     * of the new term.
     *
     * This allows search providers to only search through the previous
     * result set, rather than possibly performing a full re-query.
     *
     * Should be implemented by the subclass.
     *
     * @param {string[]} previousResults - Array of item identifiers
     * @param {string[]} newTerms - Updated search terms
     * @return {string[]} an array of result identifier strings as in
     * {@link #getInitialResultSet}.
     */
    getSubsearchResultSet: function(previousResults, newTerms) {
        throw new Error('Not implemented');
    },

    /**
     * Return an object with 'id', 'name', (both strings) and 'createIcon'
     * (function(size) returning a Clutter.Texture) properties which describe
     * the given search result.
     *
     * Should be implemented by the subclass.
     *
     * @param {string} id - Result identifier string
     * @return {Search.ResultMeta} an object with 'id', 'name', (both strings) and
     * 'createIcon' (function(size) returning a Clutter.Texture) properties
     * which describe the given search result.
     */
    getResultMeta: function(id) {
        throw new Error('Not implemented');
    },

    /**
     * Search providers may optionally override this to render their
     * results in a custom fashion.  The default implementation
     * will create a {@link SearchDisplay.GridSearchResults} (grid of results with maximum 1 row).
     * 
     * @return {?Search.SearchResultDisplay} an instance of {@link SearchResultDisplay}
     * or `null` to return the default implementation.
     */
    createResultContainerActor: function() {
        return null;
    },

    /**
     * Search providers may optionally override this to render a
     * particular serch result in a custom fashion.  The default
     * implementation will use a {@link IconGrid.BaseIcon} which is an icon
     * with the name below it.
     * @see SearchDisplay.SearchResult
     *
     * @param {Search.ResultMeta} resultMeta - Object with result metadata
     * @param {string[]} terms - Array of search terms, should be used for
     * highlighting
     * @return {?St.Widget} an instance of `St.Widget` (or subclass) with style
     * class 'search-result-content', or `null` to use the default implementation.
     */
    createResultActor: function(resultMeta, terms) {
        return null;
    },

    /** Called when the user chooses a given result.
     * @param {string} id - result identifier string
     *
     * Should be implemented by the subclass.
     */
    activateResult: function(id) {
        throw new Error('Not implemented');
    }

    /** Optional function. If present, if the user drags the {@link SearchResult}
     * over a workspace thumbnail (in the sidebar in the overview) or onto the
     * window preview in the overview, this function will be used to activate
     * the result.
     *
     * If not present, {@link #activateResult} will be used instead.
     *
     * In both cases the extra `params` argument will be provided, telling the
     * provider which workspace the result was dragged over (in case it wants to
     * say launch the result on that particular workspace).
     *
     * @name dragActivateResult
     * @inheritparams #activateResult
     * @inheritparams SearchDisplay.SearchResult#shellWorkspaceLaunch
     * @see SearchDisplay.SearchResult#shellWorkspaceLaunch
     * @see WorkspaceThumbnail.WorkspaceThumbnail#handleDragOver
     * @see Workspace.Workspace#handleDragOver
     */
};
Signals.addSignalMethods(SearchProvider.prototype);

/** creates a new OpenSearchSystem
 * @classdesc A search system with multiple providers, loading them from
 * `/usr/share/gnome-shell/search_providers` (by default GNOME 3.2 seems
 * to have Wikipedia and Google).
 *
 * An Open Search Provider/system means that the results are looked up by doing
 * an internet search with the search terms in the URL. For example, to look
 * up the terms `{searchTerms}` on google you'd use the URL
 * `http://www.google.com/search?q={searchTerms}`.
 *
 * You add search providers to the directory `/usr/share/gnome-shell/search_providers`,
 * where each provider is described in an XML file using the
 * [OpenSearch specification](http://www.opensearch.org/Home).
 *
 * Providers can be disabled if they are in gsettings key
 * 'org.gnome.shell.disabled-open-search-providers'.
 * @class
 */
function OpenSearchSystem() {
    this._init();
}

OpenSearchSystem.prototype = {
    _init: function() {
        this._providers = [];
        global.settings.connect('changed::' + DISABLED_OPEN_SEARCH_PROVIDERS_KEY, Lang.bind(this, this._refresh));
        this._refresh();
    },

    /** Gets the providers currently in the search system.
     * @returns {{id: number, name: string}[]} an array of objects, one per
     * provider, with property `id` (number, the order of that provider in the
     * list) and `name` (string, name of the search provider).
     */
    getProviders: function() {
        let res = [];
        for (let i = 0; i < this._providers.length; i++)
            res.push({ id: i, name: this._providers[i].name });

        return res;
    },

    /** Sets the search terms for the providers.
     * @param {string[]} terms
     */
    setSearchTerms: function(terms) {
        this._terms = terms;
    },

    /** Checks whether a particular provider supports *any* languages that
     * gnome-shell (GLib?) also supports.
     * @param {OpenSearchProvider} provider - the provider to check the
     * languages of
     * @returns {boolean} whether any of the provider's supported languages
     * (in {@link OpenSearchProvider.langs}) match any of `GLib`'s supported
     * languages (`GLib.get_language_names()`).
     */
    _checkSupportedProviderLanguage: function(provider) {
        if (provider.url.search(/{language}/) == -1)
            return true;

        let langs = GLib.get_language_names();

        langs.push('en');
        let lang = null;
        for (let i = 0; i < langs.length; i++) {
            for (let k = 0; k < provider.langs.length; k++) {
                if (langs[i] == provider.langs[k])
                    lang = langs[i];
            }
            if (lang)
                break;
        }
        provider.lang = lang;
        return lang != null;
    },

    /** activates a result by looking up the terms 
     * @param {number} id - ID/index of the provider to activate the result in
     * (e.g. whether we open in Google or Wikipedia)
     * @param {?} params - unused
     */
    activateResult: function(id, params) {
        let searchTerms = this._terms.join(' ');

        let url = this._providers[id].url.replace('{searchTerms}', encodeURIComponent(searchTerms));
        if (url.match('{language}'))
            url = url.replace('{language}', this._providers[id].lang);

        try {
            Gio.app_info_launch_default_for_uri(url, global.create_app_launch_context());
        } catch (e) {
            // TODO: remove this after glib will be removed from moduleset
            // In the default jhbuild, gio is in our prefix but gvfs is not
            Util.spawn(['gvfs-open', url])
        }

        Main.overview.hide();
    },

    /** adds an Open Search Provider as described in an XML file.
     *
     * For example `/usr/share/gnome-shell/search_providers/google.xml`
     * describes the Google search provider.
     *
     * For more information on the XML file, see
     * [OpenSearch.org](http://www.opensearch.org/Home).
     *
     * This function stores the search provider as an object described in 
     * {@link OpenSearchProvider}.
     *
     * @param {string} fileName - path to the XML file describing the search
     * provider to add.
     */
    _addProvider: function(fileName) {
        let path = global.datadir + '/search_providers/' + fileName;
        let source = Shell.get_file_contents_utf8_sync(path);
        let [success, name, url, langs, icon_uri] = Shell.parse_search_provider(source);
        let provider ={ name: name,
                        url: url,
                        id: this._providers.length,
                        icon_uri: icon_uri,
                        langs: langs };
        if (this._checkSupportedProviderLanguage(provider)) {
            this._providers.push(provider);
            this.emit('changed');
        }
    },

    /** Reloads the search providers (triggered by a change in the
     * disabled search providers, stored in
     * 'org.gnome.shell.disabled-open-search-providers').
     */
    _refresh: function() {
        this._providers = [];
        let names = global.settings.get_strv(DISABLED_OPEN_SEARCH_PROVIDERS_KEY);
        let file = Gio.file_new_for_path(global.datadir + '/search_providers');
        FileUtils.listDirAsync(file, Lang.bind(this, function(files) {
            for (let i = 0; i < files.length; i++) {
                let enabled = true;
                let name = files[i].get_name();
                for (let k = 0; k < names.length; k++)
                    if (names[k] == name)
                        enabled = false;
                if (enabled)
                    this._addProvider(name);
            }
        }));
    }
}
Signals.addSignalMethods(OpenSearchSystem.prototype);
/** Emitted when the providers in the open search system change.
 * @name changed
 * @event
 * @memberof OpenSearchSystem */

/** creates a new SearchSystem
 * @classdesc A SearchSystem has many {@link SearchProvider}s in it. When the
 * user asks for results matching their search terms, the SearchSystem
 * aggregates the results from all its providers.
 *
 * You add a provider to a SearchSystem using
 * {@link #registerProvider}.
 *
 * You create a {@link SearchDisplay.SearchResults} with a SearchSystem instance
 * in order to actually display the search results (I think...)
 *
 * @class
 */
function SearchSystem() {
    this._init();
}

SearchSystem.prototype = {
    _init: function() {
        this._providers = [];
        this.reset();
    },

    /** Adds a search provider to the search system.
     * @param {Search.SearchProvider} provider - a SearchProvider instance to add.
     * @see SearchProvider
     */
    registerProvider: function (provider) {
        provider.searchSystem = this;
        this._providers.push(provider);
    },

    /** Removes a search provider from the search system.
     * @param {Search.SearchProvider} provider - the SearchProvider instance to remove.
     */
    unregisterProvider: function (provider) {
        let index = this._providers.indexOf(provider);
        if (index == -1)
            return;
        provider.searchSystem = null;
        this._providers.splice(index, 1);
    },

    /** Gets the list of current providers.
     * @returns {Search.SearchProvider[]} an array of the current search
     * providers.
     */
    getProviders: function() {
        return this._providers;
    },

    /** Gets the current search terms.
     * @returns {string[]} array of search terms. */
    getTerms: function() {
        return this._previousTerms;
    },

    /** Resets all the results and search terms. */
    reset: function() {
        this._previousTerms = [];
        this._previousResults = [];
    },

    /** Called to add search results to a search (for instance of the
     * provider searches asynchronously, they may add each result as it
     * is found).
     *
     * Note - all this does is emit the 'search-updated' signal with the
     * provider and added items; when the search system is used with a
     * {@link SearchDisplay.SearchResults} which actually *displays* the
     * results, it is that class' responsibility to listen to the signal and
     * display the extra results.
     *
     * This would be called from {@link SearchProvider#addItems}.
     * @param {Search.SearchProvider} provider - provider that produced the extra
     * results.
     * @param {string[]} items - array of result IDs describing the items.
     * @fires .search-updated
     */
    addProviderItems: function(provider, items) {
        this.emit('search-updated', provider, items);
    },

    /** Updates the current search string. Triggers
     * {@link #updateSearchResults}.
     * @param {string} searchString - new search string.
     */
    updateSearch: function(searchString) {
        searchString = searchString.replace(/^\s+/g, '').replace(/\s+$/g, '');
        if (searchString == '')
            return;

        let terms = searchString.split(/\s+/);
        this.updateSearchResults(terms);
    },

    /**
     * Updates the search results (in particular, in response to an updated
     * search string). It will do a sub-search if it determines that the new
     * search terms are a subset of the previous ones.
     *
     * Will emit a 'search-completed' signal with the results (an array where
     * each element is an array `[SearchProvider, string[]]`, the first element
     * being the provider and the second being its matches for those results).
     *
     * It is up to (e.g.) the {@link SearchDisplay} to respond to the signal.
     * @param {array} terms - the search terms.
     * @fires .search-completed
     */
    updateSearchResults: function(terms) {
        if (!terms)
            return;

        let isSubSearch = terms.length == this._previousTerms.length;
        if (isSubSearch) {
            for (let i = 0; i < terms.length; i++) {
                if (terms[i].indexOf(this._previousTerms[i]) != 0) {
                    isSubSearch = false;
                    break;
                }
            }
        }

        let results = [];
        if (isSubSearch) {
            for (let i = 0; i < this._providers.length; i++) {
                let [provider, previousResults] = this._previousResults[i];
                provider.tryCancelAsync();
                try {
                    let providerResults = provider.getSubsearchResultSet(previousResults, terms);
                    results.push([provider, providerResults]);
                } catch (error) {
                    global.log ('A ' + error.name + ' has occured in ' + provider.title + ': ' + error.message);
                }
            }
        } else {
            for (let i = 0; i < this._providers.length; i++) {
                let provider = this._providers[i];
                provider.tryCancelAsync();
                try {
                    let providerResults = provider.getInitialResultSet(terms);
                    results.push([provider, providerResults]);
                } catch (error) {
                    global.log ('A ' + error.name + ' has occured in ' + provider.title + ': ' + error.message);
                }
            }
        }

        this._previousTerms = terms;
        this._previousResults = results;
        this.emit('search-completed', results);
    },
};
Signals.addSignalMethods(SearchSystem.prototype);
/** Emitted when the results of a search are updated, typically because a
 * provider in the system has asynchronously returned some extra results.
 * @param {Search.SearchSystem} o - search system that emitted the signal
 * @param {Search.SearchProvider} provider - provider that produced the extra
 * results.
 * @param {string[]} items - array of result IDs describing the results (all
 * of them, not just additional ones?)
 * @see SearchSystem#addProviderItems
 * @event
 * @name search-updated
 * @memberof SearchSystem
 */
/** Emitted when a search is completed.
 * @param {Search.SearchSystem} o - search system that emitted the signal
 * @param {Array.<[Search.SearchProvider, string[]]>} results - the results of
 * the search from all the providers. This is an array with one element per
 * provider in the search system. Each element is an array where the first
 * element is a {@link Search.SearchProvider} and the second element is
 * a string of result IDs being the results for that provider.
 * @event
 * @name search-completed
 * @memberof SearchSystem
 * @see SearchSystem#updateSearchResults
 */
