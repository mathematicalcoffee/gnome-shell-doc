// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Handles requests for attention from windows (e.g. when you start up and app
 * and it takes a while to start, when the app is done loading you get a
 * '[application] is ready' notification).
 */

const Lang = imports.lang;
const Shell = imports.gi.Shell;

const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;

/** creates a new WindowAttentionHandler
 * @classdesc This listens to the 'window-demands-attention' signal on
 * `global.display` and creates notifications for such windows if they
 * are not focused when the signal is sent.
 *
 * This can happen (for example) when the user starts an application and it
 * takes a while to open, and meanwhile they have focus on a different window.
 * When the application does eventually start and if it is not immediately
 * given focus when it does, this class will create a notification saying
 * "[application] is ready" to alert the user that the application is ready.
 * @class
 */
function WindowAttentionHandler() {
    this._init();
}

WindowAttentionHandler.prototype = {
    _init : function() {
        this._tracker = Shell.WindowTracker.get_default();
        global.display.connect('window-demands-attention', Lang.bind(this, this._onWindowDemandsAttention));
    },

    /** callback when `global.display` emits a `window-demands-attention`
     * signal. This creates a {@link MessageTray.Notification}
     * saying "[application] is ready" if the application is not focused.
     * @param {Meta.Display} display - the display that emitted the signal.
     * @param {Meta.Window} window - the window that is demanding attention.
     */
    _onWindowDemandsAttention : function(display, window) {
        // We don't want to show the notification when the window is already focused,
        // because this is rather pointless.
        // Some apps (like GIMP) do things like setting the urgency hint on the
        // toolbar windows which would result into a notification even though GIMP itself is
        // focused.
        // We are just ignoring the hint on skip_taskbar windows for now.
        // (Which is the same behaviour as with metacity + panel)

        if (!window || window.has_focus() || window.is_skip_taskbar())
            return;

        let app = this._tracker.get_window_app(window);
        let source = new Source(app, window);
        Main.messageTray.add(source);

        let banner = _("'%s' is ready").format(window.title);
        let title = app.get_name();

        let notification = new MessageTray.Notification(source, title, banner);
        source.notify(notification);

        source.signalIDs.push(window.connect('notify::title',
                                             Lang.bind(this, function() {
                                                 notification.update(title, banner);
                                             })));
    }
};

/** Creates a new Source
 * @param {Shell.App} app - the app demanding attention
 * @param {Meta.Window} window - the window (of the app) demanding attention.
 * @extends MessageTray.Source
 * @classdesc This is a {@link MessageTray.Source} for the
 * {@link WindowAttentionHandler}. The icon for the source is the app's icon.
 * @class
 */
function Source(app, window) {
    this._init(app, window);
}

Source.prototype = {
    __proto__ : MessageTray.Source.prototype,

    _init: function(app, window) {
        MessageTray.Source.prototype._init.call(this, app.get_name());
        this._window = window;
        this._app = app;
        this._setSummaryIcon(this.createNotificationIcon());

        this.signalIDs = [];
        this.signalIDs.push(this._window.connect('notify::demands-attention', Lang.bind(this, function() { this.destroy(); })));
        this.signalIDs.push(this._window.connect('focus', Lang.bind(this, function() { this.destroy(); })));
        this.signalIDs.push(this._window.connect('unmanaged', Lang.bind(this, function() { this.destroy(); })));

        this.connect('destroy', Lang.bind(this, this._onDestroy));
    },

    /** callback when the notification source is destroyed - just disconnects
     * various signal IDs */
    _onDestroy : function() {
        for(let i = 0; i < this.signalIDs.length; i++) {
           this._window.disconnect(this.signalIDs[i]);
        }
        this.signalIDs = [];
    },

    /** Implements {@link MessageTray.Source#createNotificationIcon} creating
     * a notification icon being the app's icon */
    createNotificationIcon : function() {
        return this._app.create_icon_texture(this.ICON_SIZE);
    },

    /** Implements {@link MessageTray.Source#open} causing the source to be
     * destroyed and the window (demanding attention) to be activated upon
     * opening the source.
     * @param {MessageTray.Notification} notification - the notification.
     */
    open : function(notification) {
        Main.activateWindow(this._window);
        this.destroy();
    }
};
