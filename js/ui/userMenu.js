// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the user menu (with your username, chat status, avatar, ...).
 * ![The user menu, {@link UserMenuButton}](pics/userMenu.png)
 */

const AccountsService = imports.gi.AccountsService;
const DBus = imports.dbus;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Pango = imports.gi.Pango;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Tp = imports.gi.TelepathyGLib;
const UPowerGlib = imports.gi.UPowerGlib;

const GnomeSession = imports.misc.gnomeSession;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const ScreenSaver = imports.misc.screenSaver;
const Util = imports.misc.util;

/** @+
 * @const
 * @type {string}
 * @default */
/** GSettings lockdown schema (telling us what the user is allowed to do -
 * log out, switch users, lockscreen, do command line, print, ... */
const LOCKDOWN_SCHEMA = 'org.gnome.desktop.lockdown';
/** Gsettings key under {@link LOCKDOWN_SCHEMA} - whether user switching is
 * disabled. */
const DISABLE_USER_SWITCH_KEY = 'disable-user-switching';
/** Gsettings key under {@link LOCKDOWN_SCHEMA} - whether the lockscreen is
 * disabled. */
const DISABLE_LOCK_SCREEN_KEY = 'disable-lock-screen';
/** Gsettings key under {@link LOCKDOWN_SCHEMA} - whether logging out is
 * disabled. */
const DISABLE_LOG_OUT_KEY = 'disable-log-out';
/** @- */

/** Size (pixels) of the user's avatar in the {@link IMStatusChooserItem}.
 * @type {number}
 * @const
 * @default */
const DIALOG_ICON_SIZE = 64;

/** The user's IM status.
 * @enum
 * @const */
const IMStatus = {
    /** User is available. */
    AVAILABLE: 0,
    /** User is busy. */
    BUSY: 1,
    /** User is online, but invisible. */
    HIDDEN: 2,
    /** User is away. */
    AWAY: 3,
    /** User is idle. */
    IDLE: 4,
    /** User is offline. */
    OFFLINE: 5,
    /** Unused (makes looping through IMStatus numerically more convenient:
     * `for (let i = 0; i < IMStats.LAST; ++i)`. */
    LAST: 6
};

// Adapted from gdm/gui/user-switch-applet/applet.c
//
// Copyright (C) 2004-2005 James M. Cape <jcape@ignore-your.tv>.
// Copyright (C) 2008,2009 Red Hat, Inc.


/** creates a new IMStatusItem
 * We call the [parent method (PopupBaseMenuItem)]{@link PopupMenu.PopupBaseMenuItem}.
 *
 * We add style class 'status-chooser-status-item' to {@link #actor}.
 *
 * Then if `iconName` is provided, we create the icon and {@link #addActor}
 * it to ourselves. Then we create a label displaying the text `label` and
 * {@link #addActor} that to ourselves too.
 * @param {string} label - the text to display on the item ("Available", "Busy", ...)
 * @param {string} iconName - the icon name to display on the item.
 * @classdesc
 * The IMStatusItem is a popup menu item that goes in the status chooser combo
 * box in the user menu.
 *
 * [Two {@link IMStatusItem}s.](pics/IMStatusItem.png)
 *
 * It displays an icon to the left (the status icon) and text ("Available", ...).
 *
 * It's similar to a {@link PopupMenu.PopupImageMenuItem} but with the icon
 * on the left instead of ther ight.
 * @class
 * @extends PopupMenu.PopupBaseMenuItem
 */
function IMStatusItem(label, iconName) {
    this._init(label, iconName);
}

IMStatusItem.prototype = {
    __proto__: PopupMenu.PopupBaseMenuItem.prototype,

    _init: function(label, iconName) {
        PopupMenu.PopupBaseMenuItem.prototype._init.call(this);

        this.actor.add_style_class_name('status-chooser-status-item');

        /** The icon for the item.
         * @type {St.Icon} */
        this._icon = new St.Icon({ style_class: 'popup-menu-icon' });
        this.addActor(this._icon);

        if (iconName)
            this._icon.icon_name = iconName;

        /** The label displaying the text on the item.
         * @type {St.Label} */
        this.label = new St.Label({ text: label });
        this.addActor(this.label);
    }
};

/** creates a new IMUserNameItem
 * We call the [parent constructor]{@link PopupMenu.PopupBaseMenuItem}, making
 * the item non-reactive and giving it style class 'status-chooser-user-name'.
 *
 * We create a label {@link #label} (no text in it for now). The label is placed
 * in a {@link Shell.GenericContainer} ({@link #_wrapper}) which is
 * {@link #addActor}ed to ourselves.
 *
 * (TODO: I have no idea why we put the label into the GenericContainer into the
 * item rather than putting the label directly in the item. Why do we allocate
 * width 1?)
 *
 * @classdesc
 * This is the label displaying the username in the user menu.
 *
 * It's essentially a label and the item has style class 'status-chooser-user-name'.
 * Ellipsization is turned on in case the username is too long.
 *
 * ![The {@link IMUserNameItem} is red](pics/userMenu_IMUserNameItem-blue_IMStatusChooserItem-red.png)
 *
 * @class
 * @extends PopupMenu.PopupBaseMenuItem
 */
function IMUserNameItem() {
    this._init();
}

IMUserNameItem.prototype = {
    __proto__: PopupMenu.PopupBaseMenuItem.prototype,

    _init: function() {
        PopupMenu.PopupBaseMenuItem.prototype._init.call(this,
                                                         { reactive: false,
                                                           style_class: 'status-chooser-user-name' });
        /** The label for the username is placed in here. This is placed in
         * the item.
         * @type {Shell.GenericContainer} */
        this._wrapper = new Shell.GenericContainer();
        this._wrapper.connect('get-preferred-width',
                              Lang.bind(this, this._wrapperGetPreferredWidth));
        this._wrapper.connect('get-preferred-height',
                              Lang.bind(this, this._wrapperGetPreferredHeight));
        this._wrapper.connect('allocate',
                              Lang.bind(this, this._wrapperAllocate));
        this.addActor(this._wrapper, { expand: true, span: -1 });

        /** This is the label for the username. Placed into {@link #_wrapper}.
         * @type {St.Label} */
        this.label = new St.Label();
        this.label.clutter_text.set_line_wrap(true);
        this.label.clutter_text.set_ellipsize(Pango.EllipsizeMode.NONE);
        this._wrapper.add_actor(this.label);
    },

    /** Connected to {@link #_wrapper}'s 'get-preferred-width' signal.
     *
     * We allocate minimum and natural widths 1 (TODO: why?)
     */
    _wrapperGetPreferredWidth: function(actor, forHeight, alloc) {
        alloc.min_size = 1;
        alloc.natural_size = 1;
    },

    /** Connected to {@link #_wrapper}'s 'get-preferred-height' signal.
     *
     * We allocate the label's preferred height.
     */
    _wrapperGetPreferredHeight: function(actor, forWidth, alloc) {
        [alloc.min_size, alloc.natural_size] = this.label.get_preferred_height(forWidth);
    },

    /** Connected to {@link #_wrapper}'s 'allocate' signal.
     * We allocate the entire available space to our label {@link #label}.
     */
    _wrapperAllocate: function(actor, box, flags) {
        this.label.allocate(box, flags);
    }
};

/** creates a new IMStatusChooserItem.
 *
 * ![{@link IMStatusChooserItem} (red), consisting of an avatar, combo box and {@link IMUserNameItem} (blue).](pics/userMenu_IMUserNameItem-blue_IMStatusChooserItem-red.png)
 *
 * First we call the [parent constructor]{@link PopupMenu.PopupBaseMenuItem},
 * making the item non-reactive and giving it style class 'status-chooser'.
 *
 * We create a button to display the user's avatar, {@link #_iconBin}. We
 * connect it up such that clicking on it will call {@link #activate} (emitting
 * the ['activate' signal]{@link .event:activate} from the item, handled by
 * {@link UserMenuButton#_onMyAccountActivate}).
 *
 * The button is {@link #addActor}ed to the item - it is placed to the left.
 *
 * Then we create a {@link PopupMenu.PopupMenuSection} which will hold
 * both the username item ({@link IMUserNameItem}) and the status combo
 * box. This section ({@link #_section}) is also {@link #addActor}ed to the item.
 * It's being used similarly to a vertical BoxLayout in this case.
 *
 * We create a {@link IMUserNameItem} to show the user's username as well
 * as a {@link PopupMenu.PopupComboBoxMenuItem} to show the user's chat
 * status.
 *
 * We create a {@link IMStatusItem} (chat status icon + text) for each status:
 * "Available", "Busy", "Hidden", "Away", "Idle", "Offline" and add them to the
 * combo box.
 *
 * We now store a number of instances of helper classes:
 *
 * * {@link #_proxy}: a proxy for DBus object '/org/gnome/SessionManager/Presence',
 *   allowing us to set/get the user's chat status.
 * * {@link #_accountMgr}: the default Telepathy account manager (for monitoring
 *   status changes across multiple Telepathy chat accounts).
 * * {@link #_userManager}: default user manager instance (for querying user information).
 * * {@link #_user}: information about the current user.
 *
 * Finally, we queue an update to the user details (username, avatar, status)
 * as soon as the item becomes visible (there's no point spending time updating
 * when the item is not even visible), or once the user information is loaded
 * from the user manager, and whenever the user details change ({@link #_updateUser}).
 *
 * @classdesc
 * ![{@link IMStatusChooserItem} (red), consisting of an avatar, combo box and {@link IMUserNameItem} (blue).](pics/userMenu_IMUserNameItem-blue_IMStatusChooserItem-red.png)
 *
 * This is the popup menu item in the user menu that shows information about the
 * user's chat status:
 *
 * * their avatar,
 * * their username (a {@link IMUserNameItem}), and
 * * a {@link PopupComboBoxMenuItem} showing the user's current status (away,
 * available) allowing the user to select a new one.
 *
 * @class
 * @extends PopupMenu.PopupBaseMenuItem
 */
function IMStatusChooserItem() {
    this._init();
}

IMStatusChooserItem.prototype = {
    __proto__: PopupMenu.PopupBaseMenuItem.prototype,

    _init: function() {
        PopupMenu.PopupBaseMenuItem.prototype._init.call (this,
                                                          { reactive: false,
                                                            style_class: 'status-chooser' });

        /** Button displaying the user's avatar. Style class 'status-chooser-user-icon'.
         * Clicking on it causes the item to be activated. This is placed to the
         * left of the item.
         * @type {St.Button} */
        this._iconBin = new St.Button({ style_class: 'status-chooser-user-icon' });
        this.addActor(this._iconBin);

        this._iconBin.connect('clicked', Lang.bind(this,
            function() {
                this.activate();
            }));

        /** Placed on the right of the icon - it's used as a vertical BoxLayout
         * to display the username item ({@link #_name}, a {@link IMUserNameItem})
         * and the status combobox ({@link #_combo}).
         * @type {PopupMenu.PopupMenuSection} */
        this._section = new PopupMenu.PopupMenuSection();
        this.addActor(this._section.actor);

        /** Part of the item displaying the username. Added to {@link #_section}.
         * @type {IMUserNameItem} */
        this._name = new IMUserNameItem();
        this._section.addMenuItem(this._name);

        /** A combo box showing the user's chat status and letting them change it.
         * Style class 'status-chooser-combo'.
         * @type {PopupMenu.PopupComboBoxMenuItem} */
        this._combo = new PopupMenu.PopupComboBoxMenuItem({ style_class: 'status-chooser-combo' });
        this._section.addMenuItem(this._combo);

        let item;

        item = new IMStatusItem(_("Available"), 'user-available');
        this._combo.addMenuItem(item, IMStatus.AVAILABLE);

        item = new IMStatusItem(_("Busy"), 'user-busy');
        this._combo.addMenuItem(item, IMStatus.BUSY);

        item = new IMStatusItem(_("Hidden"), 'user-invisible');
        this._combo.addMenuItem(item, IMStatus.HIDDEN);

        item = new IMStatusItem(_("Away"), 'user-away');
        this._combo.addMenuItem(item, IMStatus.AWAY);

        item = new IMStatusItem(_("Idle"), 'user-idle');
        this._combo.addMenuItem(item, IMStatus.IDLE);

        item = new IMStatusItem(_("Unavailable"), 'user-offline');
        this._combo.addMenuItem(item, IMStatus.OFFLINE);

        this._combo.connect('active-item-changed',
                            Lang.bind(this, this._changeIMStatus));

        /** Proxy for DBus object '/org/gnome/SessionManager/Presence', allowing
         * us to set/get the user's chat status.
         * @type {GnomeSession.Presence} */
        this._presence = new GnomeSession.Presence();
        this._presence.connect('StatusChanged',
                               Lang.bind(this, this._sessionStatusChanged));

        this._sessionPresenceRestored = false;
        this._imPresenceRestored = false;
        /** The user's current chat presence.
         * @type {Tp.ConnectionPresenceType} */
        this._currentPresence = undefined;

        /** Telepathy account manager, used to know when the user's most
         * available chat status (amongst all their accounts) changes.
         * @see #_IMStatusChanged
         * @type {Tp.AccountManager} */
        this._accountMgr = Tp.AccountManager.dup()
        this._accountMgr.connect('most-available-presence-changed',
                                 Lang.bind(this, this._IMStatusChanged));
        this._accountMgr.prepare_async(null, Lang.bind(this,
            function(mgr) {
                let [presence, status, msg] = mgr.get_most_available_presence();

                let savedPresence = global.settings.get_int('saved-im-presence');
                if (savedPresence == presence) {
                    this._IMStatusChanged(mgr, presence, status, msg);
                } else {
                    this._setComboboxPresence(savedPresence);
                    status = this._statusForPresence(savedPresence);
                    msg = msg ? msg : '';
                    mgr.set_all_requested_presences(savedPresence, status, msg);
                }
            }));

        /** default user manager.
         * @type {AccountsService.UserManager} */
        this._userManager = AccountsService.UserManager.get_default();

        /** The current user.
         * @type {AccountsService.User} */
        this._user = this._userManager.get_user(GLib.get_user_name());

        this._userLoadedId = this._user.connect('notify::is-loaded',
                                                Lang.bind(this,
                                                          this._updateUser));
        this._userChangedId = this._user.connect('changed',
                                                 Lang.bind(this,
                                                           this._updateUser));
        this.actor.connect('notify::mapped', Lang.bind(this, function() {
            if (this.actor.mapped)
                this._updateUser();
        }));
    },

    /** We override {@link #getColumnWidths}/{@link #setColumnWidths} to make
     * this item independent from the overall column layout of the menu. (This
     * returns an empty array).
     * @override */
    getColumnWidths: function() {
        return [];
    },

    /** We override {@link #getColumnWidths}/{@link #setColumnWidths} to make
     * this item independent from the overall column layout of the menu.
     * (This function does nothing with `widths`).
     * @override
     */
    setColumnWidths: function(widths) {
    },

    /** Called whenever the user details are changed, or once the user details
     * have finished loading, and whenever the item is mapped to the screen.
     *
     * We update the username and avatar shown on the item.
     */
    _updateUser: function() {
        let iconFile = null;
        if (this._user.is_loaded) {
            this._name.label.set_text(this._user.get_real_name());
            iconFile = this._user.get_icon_file();
            if (!GLib.file_test(iconFile, GLib.FileTest.EXISTS))
                iconFile = null;
        } else {
            this._name.label.set_text("");
        }

        if (iconFile)
            this._setIconFromFile(iconFile);
        else
            this._setIconFromName('avatar-default');
    },

    /** @inheritdoc EndSessionDialog.EndSessionDialog#_setIconFromFile */
    _setIconFromFile: function(iconFile) {
        this._iconBin.set_style('background-image: url("' + iconFile + '");');
        this._iconBin.child = null;
    },

    /** @inheritdoc EndSessionDialog.EndSessionDialog#_setIconFromName */
    _setIconFromName: function(iconName) {
        this._iconBin.set_style(null);

        if (iconName != null) {
            let textureCache = St.TextureCache.get_default();
            let icon = textureCache.load_icon_name(this._iconBin.get_theme_node(),
                                                   iconName,
                                                   St.IconType.SYMBOLIC,
                                                   DIALOG_ICON_SIZE);

            this._iconBin.child = icon;
            this._iconBin.show();
        } else {
            this._iconBin.child = null;
            this._iconBin.hide();
        }
    },

    /** Converts a Telepathy presence (enum) to a string presence.
     * @param {Tp.ConnectionPresenceType} presence - the presence to convert (a
     * number)
     * @returns {string} the corresponding string for the presence - 'available',
     * 'busy', 'offline', 'hidden', 'away', 'xa' (extended away), 'unknown'.
     */
    _statusForPresence: function(presence) {
        switch(presence) {
            case Tp.ConnectionPresenceType.AVAILABLE:
                return 'available';
            case Tp.ConnectionPresenceType.BUSY:
                return 'busy';
            case Tp.ConnectionPresenceType.OFFLINE:
                return 'offline';
            case Tp.ConnectionPresenceType.HIDDEN:
                return 'hidden';
            case Tp.ConnectionPresenceType.AWAY:
                return 'away';
            case Tp.ConnectionPresenceType.EXTENDED_AWAY:
                return 'xa';
            default:
                return 'unknown';
        }
    },

    //UPTO
    /**
     * @param {?} accountMgr
     * @param {?} presence
     * @param {?} status
     * @param {?} message
     */
    _IMStatusChanged: function(accountMgr, presence, status, message) {
        if (!this._imPresenceRestored)
            this._imPresenceRestored = true;

        if (presence == this._currentPresence)
            return;

        this._currentPresence = presence;
        this._setComboboxPresence(presence);

        if (!this._sessionPresenceRestored) {
            this._presence.getStatus(Lang.bind(this, this._sessionStatusChanged));
            return;
        }

        if (presence == Tp.ConnectionPresenceType.AVAILABLE)
            this._presence.setStatus(GnomeSession.PresenceStatus.AVAILABLE);

        // We ignore the actual value of _expectedPresence and never safe
        // the first presence change after an "automatic" change, assuming
        // that it is the response to our request; this is to account for
        // mission control falling back to "similar" presences if an account
        // type does not implement the requested presence.
        if (!this._expectedPresence)
            global.settings.set_int('saved-im-presence', presence);
        else
            this._expectedPresence = undefined;
    },

    /**
     * @param {?} presence
     */
    _setComboboxPresence: function(presence) {
        let activatedItem;

        if (presence == Tp.ConnectionPresenceType.AVAILABLE)
            activatedItem = IMStatus.AVAILABLE;
        else if (presence == Tp.ConnectionPresenceType.BUSY)
            activatedItem = IMStatus.BUSY;
        else if (presence == Tp.ConnectionPresenceType.HIDDEN)
            activatedItem = IMStatus.HIDDEN;
        else if (presence == Tp.ConnectionPresenceType.AWAY)
            activatedItem = IMStatus.AWAY;
        else if (presence == Tp.ConnectionPresenceType.EXTENDED_AWAY)
            activatedItem = IMStatus.IDLE;
        else
            activatedItem = IMStatus.OFFLINE;

        this._combo.setActiveItem(activatedItem);
        for (let i = 0; i < IMStatus.LAST; i++) {
            if (i == IMStatus.AVAILABLE || i == IMStatus.OFFLINE)
                continue;   // always visible

            this._combo.setItemVisible(i, i == activatedItem);
        }
    },

    /**
     * @param {?} menuItem
     * @param {?} id
     */
    _changeIMStatus: function(menuItem, id) {
        let [presence, s, msg] = this._accountMgr.get_most_available_presence();
        let newPresence, status;

        if (id == IMStatus.AVAILABLE) {
            newPresence = Tp.ConnectionPresenceType.AVAILABLE;
        } else if (id == IMStatus.OFFLINE) {
            newPresence = Tp.ConnectionPresenceType.OFFLINE;
        } else
            return;

        status = this._statusForPresence(newPresence);
        msg = msg ? msg : '';
        this._accountMgr.set_all_requested_presences(newPresence, status, msg);
    },

    /**
     * @param {?} sessionStatus
     */
    getIMPresenceForSessionStatus: function(sessionStatus) {
        // Restore the last user-set presence when coming back from
        // BUSY/IDLE (otherwise the last user-set presence matches
        // the current one)
        if (sessionStatus == GnomeSession.PresenceStatus.AVAILABLE)
            return global.settings.get_int('saved-im-presence');

        if (sessionStatus == GnomeSession.PresenceStatus.BUSY) {
            // Only change presence if the current one is "more present" than
            // busy, or if coming back from idle
            if (this._currentPresence == Tp.ConnectionPresenceType.AVAILABLE ||
                this._currentPresence == Tp.ConnectionPresenceType.EXTENDED_AWAY)
                return Tp.ConnectionPresenceType.BUSY;
        }

        if (sessionStatus == GnomeSession.PresenceStatus.IDLE) {
            // Only change presence if the current one is "more present" than
            // idle
            if (this._currentPresence != Tp.ConnectionPresenceType.OFFLINE)
                return Tp.ConnectionPresenceType.EXTENDED_AWAY;
        }

        return this._currentPresence;
    },

    /**
     * @param {?} sessionPresence
     * @param {?} sessionStatus
     */
    _sessionStatusChanged: function(sessionPresence, sessionStatus) {
        if (!this._imPresenceRestored)
            return;

        if (!this._sessionPresenceRestored) {
            let savedStatus = global.settings.get_int('saved-session-presence');
            if (sessionStatus != savedStatus) {
                this._presence.setStatus(savedStatus);
                return;
            }
            this._sessionPresenceRestored = true;
        }

        global.settings.set_int('saved-session-presence', sessionStatus);

        let [presence, s, msg] = this._accountMgr.get_most_available_presence();
        let newPresence, status;

        newPresence = this.getIMPresenceForSessionStatus(sessionStatus);

        if (!newPresence || newPresence == presence)
            return;

        status = this._statusForPresence(newPresence);
        msg = msg ? msg : '';

        this._expectedPresence = newPresence;
        this._accountMgr.set_all_requested_presences(newPresence, status, msg);
    }
};


/** creates a new UserMenuButton
 * @param {?} TODO: document any constructor params
 * @classdesc TODO: document the class itself
 * @class
 */
function UserMenuButton() {
    this._init();
}

UserMenuButton.prototype = {
    __proto__: PanelMenu.Button.prototype,

    _init: function() {
        PanelMenu.Button.prototype._init.call(this, 0.0);
        let box = new St.BoxLayout({ name: 'panelUserMenu' });
        this.actor.add_actor(box);

        this._lockdownSettings = new Gio.Settings({ schema: LOCKDOWN_SCHEMA });

        this._userManager = AccountsService.UserManager.get_default();

        this._user = this._userManager.get_user(GLib.get_user_name());
        this._presence = new GnomeSession.Presence();
        this._session = new GnomeSession.SessionManager();
        this._haveShutdown = true;

        this._account_mgr = Tp.AccountManager.dup()

        this._upClient = new UPowerGlib.Client();
        this._screenSaverProxy = new ScreenSaver.ScreenSaverProxy();
        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        this._iconBox = new St.Bin();
        box.add(this._iconBox, { y_align: St.Align.MIDDLE, y_fill: false });

        let textureCache = St.TextureCache.get_default();
        this._offlineIcon = new St.Icon({ icon_name: 'user-offline',
                                          style_class: 'popup-menu-icon' });
        this._availableIcon = new St.Icon({ icon_name: 'user-available',
                                            style_class: 'popup-menu-icon' });
        this._busyIcon = new St.Icon({ icon_name: 'user-busy',
                                       style_class: 'popup-menu-icon' });
        this._invisibleIcon = new St.Icon({ icon_name: 'user-invisible',
                                            style_class: 'popup-menu-icon' });
        this._awayIcon = new St.Icon({ icon_name: 'user-away',
                                       style_class: 'popup-menu-icon' });
        this._idleIcon = new St.Icon({ icon_name: 'user-idle',
                                       style_class: 'popup-menu-icon' });

        this._presence.connect('StatusChanged',
                               Lang.bind(this, this._updateSwitch));
        this._presence.getStatus(Lang.bind(this, this._updateSwitch));

        this._account_mgr.connect('most-available-presence-changed',
                                  Lang.bind(this, this._updatePresenceIcon));
        this._account_mgr.prepare_async(null, Lang.bind(this,
            function(mgr) {
                let [presence, s, msg] = mgr.get_most_available_presence();
                this._updatePresenceIcon(mgr, presence, s, msg);
            }));

        this._name = new St.Label();
        box.add(this._name, { y_align: St.Align.MIDDLE, y_fill: false });
        this._userLoadedId = this._user.connect('notify::is-loaded', Lang.bind(this, this._updateUserName));
        this._userChangedId = this._user.connect('changed', Lang.bind(this, this._updateUserName));
        this._updateUserName();

        this._createSubMenu();
        this._userManager.connect('notify::is-loaded',
                                  Lang.bind(this, this._updateSwitchUser));
        this._userManager.connect('user-added',
                                  Lang.bind(this, this._updateSwitchUser));
        this._userManager.connect('user-removed',
                                  Lang.bind(this, this._updateSwitchUser));
        this._lockdownSettings.connect('changed::' + DISABLE_USER_SWITCH_KEY,
                                       Lang.bind(this, this._updateSwitchUser));
        this._lockdownSettings.connect('changed::' + DISABLE_LOG_OUT_KEY,
                                       Lang.bind(this, this._updateLogout));

        this._lockdownSettings.connect('changed::' + DISABLE_LOCK_SCREEN_KEY,
                                       Lang.bind(this, this._updateLockScreen));
        this._updateSwitchUser();
        this._updateLogout();
        this._updateLockScreen();

        // Whether shutdown is available or not depends on both lockdown
        // settings (disable-log-out) and Polkit policy - the latter doesn't
        // notify, so we update the menu item each time the menu opens or
        // the lockdown setting changes, which should be close enough.
        this.menu.connect('open-state-changed', Lang.bind(this,
            function(menu, open) {
                if (open)
                    this._updateHaveShutdown();
            }));
        this._lockdownSettings.connect('changed::' + DISABLE_LOG_OUT_KEY,
                                       Lang.bind(this, this._updateHaveShutdown));

        this._upClient.connect('notify::can-suspend', Lang.bind(this, this._updateSuspendOrPowerOff));
    },

    /** description of function */
    _onDestroy: function() {
        this._user.disconnect(this._userLoadedId);
        this._user.disconnect(this._userChangedId);
    },

    /** description of function */
    _updateUserName: function() {
        if (this._user.is_loaded)
            this._name.set_text(this._user.get_real_name());
        else
            this._name.set_text("");
    },

    /** description of function */
    _updateSwitchUser: function() {
        let allowSwitch = !this._lockdownSettings.get_boolean(DISABLE_USER_SWITCH_KEY);
        if (allowSwitch && this._userManager.can_switch ())
            this._loginScreenItem.actor.show();
        else
            this._loginScreenItem.actor.hide();
    },

    /** description of function */
    _updateLogout: function() {
        let allowLogout = !this._lockdownSettings.get_boolean(DISABLE_LOG_OUT_KEY);
        if (allowLogout)
            this._logoutItem.actor.show();
        else
            this._logoutItem.actor.hide();
    },

    /** description of function */
    _updateLockScreen: function() {
        let allowLockScreen = !this._lockdownSettings.get_boolean(DISABLE_LOCK_SCREEN_KEY);
        if (allowLockScreen)
            this._lockScreenItem.actor.show();
        else
            this._lockScreenItem.actor.hide();
    },

    /** description of function */
    _updateHaveShutdown: function() {
        this._session.CanShutdownRemote(Lang.bind(this,
            function(result, error) {
                if (!error) {
                    this._haveShutdown = result;
                    this._updateSuspendOrPowerOff();
                }
            }));
    },

    /** description of function */
    _updateSuspendOrPowerOff: function() {
        this._haveSuspend = this._upClient.get_can_suspend();

        if (!this._suspendOrPowerOffItem)
            return;

        if (!this._haveShutdown && !this._haveSuspend)
            this._suspendOrPowerOffItem.actor.hide();
        else
            this._suspendOrPowerOffItem.actor.show();

        // If we can't suspend show Power Off... instead
        // and disable the alt key
        if (!this._haveSuspend) {
            this._suspendOrPowerOffItem.updateText(_("Power Off..."), null);
        } else if (!this._haveShutdown) {
            this._suspendOrPowerOffItem.updateText(_("Suspend"), null);
        } else {
            this._suspendOrPowerOffItem.updateText(_("Suspend"), _("Power Off..."));
        }
    },

    /**
     * @param {?} presence
     * @param {?} status
     */
    _updateSwitch: function(presence, status) {
        let active = status == GnomeSession.PresenceStatus.AVAILABLE;
        this._notificationsSwitch.setToggleState(active);
    },

    /**
     * @param {?} accountMgr
     * @param {?} presence
     * @param {?} status
     * @param {?} message
     */
    _updatePresenceIcon: function(accountMgr, presence, status, message) {
        if (presence == Tp.ConnectionPresenceType.AVAILABLE)
            this._iconBox.child = this._availableIcon;
        else if (presence == Tp.ConnectionPresenceType.BUSY)
            this._iconBox.child = this._busyIcon;
        else if (presence == Tp.ConnectionPresenceType.HIDDEN)
            this._iconBox.child = this._invisibleIcon;
        else if (presence == Tp.ConnectionPresenceType.AWAY)
            this._iconBox.child = this._awayIcon;
        else if (presence == Tp.ConnectionPresenceType.EXTENDED_AWAY)
            this._iconBox.child = this._idleIcon;
        else
            this._iconBox.child = this._offlineIcon;
    },

    /** description of function */
    _createSubMenu: function() {
        let item;

        item = new IMStatusChooserItem();
        item.connect('activate', Lang.bind(this, this._onMyAccountActivate));
        this.menu.addMenuItem(item);
        this._statusChooser = item;

        item = new PopupMenu.PopupSwitchMenuItem(_("Notifications"));
        item.connect('activate', Lang.bind(this, this._updatePresenceStatus));
        this.menu.addMenuItem(item);
        this._notificationsSwitch = item;

        item = new PopupMenu.PopupSeparatorMenuItem();
        this.menu.addMenuItem(item);

        item = new PopupMenu.PopupMenuItem(_("Online Accounts"));
        item.connect('activate', Lang.bind(this, this._onOnlineAccountsActivate));
        this.menu.addMenuItem(item);

        item = new PopupMenu.PopupMenuItem(_("System Settings"));
        item.connect('activate', Lang.bind(this, this._onPreferencesActivate));
        this.menu.addMenuItem(item);

        item = new PopupMenu.PopupSeparatorMenuItem();
        this.menu.addMenuItem(item);

        item = new PopupMenu.PopupMenuItem(_("Lock Screen"));
        item.connect('activate', Lang.bind(this, this._onLockScreenActivate));
        this.menu.addMenuItem(item);
        this._lockScreenItem = item;

        item = new PopupMenu.PopupMenuItem(_("Switch User"));
        item.connect('activate', Lang.bind(this, this._onLoginScreenActivate));
        this.menu.addMenuItem(item);
        this._loginScreenItem = item;

        item = new PopupMenu.PopupMenuItem(_("Log Out..."));
        item.connect('activate', Lang.bind(this, this._onQuitSessionActivate));
        this.menu.addMenuItem(item);
        this._logoutItem = item;

        item = new PopupMenu.PopupSeparatorMenuItem();
        this.menu.addMenuItem(item);

        item = new PopupMenu.PopupAlternatingMenuItem(_("Suspend"),
                                                      _("Power Off..."));
        this.menu.addMenuItem(item);
        this._suspendOrPowerOffItem = item;
        item.connect('activate', Lang.bind(this, this._onSuspendOrPowerOffActivate));
        this._updateSuspendOrPowerOff();
    },

    /**
     * @param {?} item
     * @param {?} event
     */
    _updatePresenceStatus: function(item, event) {
        let status;

        if (item.state) {
            status = GnomeSession.PresenceStatus.AVAILABLE;
        } else {
            status = GnomeSession.PresenceStatus.BUSY;

            let [presence, s, msg] = this._account_mgr.get_most_available_presence();
            let newPresence = this._statusChooser.getIMPresenceForSessionStatus(status);
            if (newPresence != presence &&
                newPresence == Tp.ConnectionPresenceType.BUSY)
                Main.notify(_("Your chat status will be set to busy"),
                            _("Notifications are now disabled, including chat messages. Your online status has been adjusted to let others know that you might not see their messages."));
        }

        this._presence.setStatus(status);
    },

    /** description of function */
    _onMyAccountActivate: function() {
        Main.overview.hide();
        let app = Shell.AppSystem.get_default().lookup_setting('gnome-user-accounts-panel.desktop');
        app.activate();
    },

    /** description of function */
    _onOnlineAccountsActivate: function() {
        Main.overview.hide();
        let app = Shell.AppSystem.get_default().lookup_setting('gnome-online-accounts-panel.desktop');
        app.activate(-1);
    },

    /** description of function */
    _onPreferencesActivate: function() {
        Main.overview.hide();
        let app = Shell.AppSystem.get_default().lookup_app('gnome-control-center.desktop');
        app.activate();
    },

    /** description of function */
    _onLockScreenActivate: function() {
        Main.overview.hide();
        this._screenSaverProxy.LockRemote();
    },

    /** description of function */
    _onLoginScreenActivate: function() {
        Main.overview.hide();
        // Ensure we only move to GDM after the screensaver has activated; in some
        // OS configurations, the X server may block event processing on VT switch
        this._screenSaverProxy.SetActiveRemote(true, Lang.bind(this, function() {
            this._userManager.goto_login_session();
        }));
    },

    /** description of function */
    _onQuitSessionActivate: function() {
        Main.overview.hide();
        this._session.LogoutRemote(0);
    },

    /** description of function */
    _onSuspendOrPowerOffActivate: function() {
        Main.overview.hide();

        if (this._haveSuspend &&
            this._suspendOrPowerOffItem.state == PopupMenu.PopupAlternatingMenuItemState.DEFAULT) {
            // Ensure we only suspend after the screensaver has activated
            this._screenSaverProxy.SetActiveRemote(true, Lang.bind(this, function() {
                this._upClient.suspend_sync(null);
            }));
        } else {
            this._session.ShutdownRemote();
        }
    }
};
