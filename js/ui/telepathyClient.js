// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Handles listening to chat from telepathy and setting up notifications and
 * sources for these in the message tray, allowing the user to chat through
 * the message tray (nifty!).
 *
 * This defines a Telepathy client class {@link Client}, as well as a number
 * of notifications/sources for chat messages ({@link ChatSource},
 * {@link ChatNotification}). The Telepathy Client is responsible for creating
 * various notifications and sources for room invite requests, chats, file
 * transfers, and so on.
 *
 * Makes use of [Telepathy GLib](http://telepathy.freedesktop.org/wiki/Logger)
 * and [Telapthy Logger](http://telepathy.freedesktop.org/wiki/Logger) libraries.
 * Also, the {@link Shell.TpClient} class.
 *
 * A Telepathy Account has a Connection (if it is online), although a Connection
 * need not have an associated Account. A given connection will have many
 * Channels, each one represented by a DBus object. A Channel is used to
 * exchange data between local applications and remoted servers (like
 * room lists, authentication, chats, calls, file transfer).
 * I *think* a connection has many contacts. (I'm getting this all from
 * the summaries in the [telepathy DBus specification](http://telepathy.freedesktop.org/spec/)).
 * 
 * TODO: I don't know much at all about telepathy.
 * Needs contribution from others.
 *
 * Sources:
 * * {@link ChatSource} - for chat messages
 * * {@link ApproverSource}
 *
 * Notifications:
 * * {@link ChatNotification} - for chat messages
 *
 * @see {@link http://telepathy.freedesktop.org/spec/ DBus specification for Telepathy}
 * @see {@link http://telepathy.freedesktop.org/wiki/Telepathy%20GLib  Telepathy GLib homepage (`const Tp = imports.gi.TelepathyGLib`)}
 * @see {@link http://telepathy.freedesktop.org/doc/telepathy-glib/ Telepathy GLib API docs}
 * @see {@link http://vega.frugalware.org/tmpgit/gnometesting.old/source/lib/telepathy-logger/pkg/usr/share/gtk-doc/html/telepathy-logger/index.html Telepathy Logger docs, all i could find (`const Tpl = imports.gi.TelepathyLogger`)}
 * @see {@link http://telepathy.freedesktop.org/wiki/Logger Telepathy Logger wiki page}
 * @see {@link http://developer.gnome.org/shell/stable/shell-ShellTpClient.html Shell.TpClient API}
 */

const DBus = imports.dbus;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;
const Tpl = imports.gi.TelepathyLogger;
const Tp = imports.gi.TelepathyGLib;

const History = imports.misc.history;
const Params = imports.misc.params;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;


/** @+
 * @const
 * @default
 * @type {number} */
/** @+
 * @see ChatNotification#_filterMessages
 * @see ChatNotification#_append
 */
/** Time (seconds) after a message is received after which we show its timestamp.
 * Default 1 minute, so 1 minute after receiving a chat, an extra message
 * "Sent at [time] on [day]" appears. */
const SCROLLBACK_IMMEDIATE_TIME = 60; // 1 minute
/** Time (seconds) used to determine how many messages we keep in a 
 * {@link ChatNotification}. (15 minutes) */
const SCROLLBACK_RECENT_TIME = 15 * 60; // 15 minutes
/** Number of lines used to determine how many messages we keep in a
 * {@link ChatNotification} if we've been talking within the last
 * {@link SCROLLBACK_RECENT_TIME}. */
const SCROLLBACK_RECENT_LENGTH = 20;
/** Number of lines used to determine how many messages we keep in a
 * {@link ChatNotification} if we've been idle (haven't talked within
 * the last {@link SCROLLBACK_RECENT_TIME}). */
const SCROLLBACK_IDLE_LENGTH = 5;
/** @- */

/** Number of lines of chat history (from that contact) to retrieve upon creating a new
 * {@link ChatSource} for a conversation (?).
 * @see ChatSource#_displayPendingMessages */
const SCROLLBACK_HISTORY_LINES = 10;

/** Time (seconds) after we stop typing that we set our status from
 * `COMPOSING` to `PAUSED` (see {@link ChatNotification#_onEntryChanged}).
 * @see ChatNotification#_onEntryChanged */
const COMPOSING_STOP_TIMEOUT = 5;
/** @- */

/** Whether a chat is being sent or received.
 * The value is a CSS style class to append to messages of that direction.
 * @type {string}
 * @const
 * @enum
 */
const NotificationDirection = {
    SENT: 'chat-sent',
    RECEIVED: 'chat-received'
};

let contactFeatures = [Tp.ContactFeature.ALIAS,
                        Tp.ContactFeature.AVATAR_DATA,
                        Tp.ContactFeature.PRESENCE];

/** GNOME-shell object describing a chat message.
 * @typedef Message
 * @type Object
 * @property {number} messageType - compare against
 * [`imports.gi.TelepathyGLib.ChannelTextMessageType`](http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-enums.html#TpChannelTextMessageType).
 * `NORMAL`, `ACTION`, `NOTICE`, `AUTO_REPLY`, `DELIVERY_REPORT`.
 * @property {string} text - the message
 * @property {string} sender - the sender's alias
 * @property {number} timestamp - the timestamp of the message
 * @property {TelepathyClient.NotificationDirection} the direction of the message.
 */
// This is GNOME Shell's implementation of the Telepathy 'Client'
// interface. Specifically, the shell is a Telepathy 'Observer', which
// lets us see messages even if they belong to another app (eg,
// Empathy).

/** Converts a {@link Tp.Message|Telepathy message} into a local gnome-shell
 * object ({@link Message}).
 * @param {Tp.Message} tpMessage - telepathy message
 * @param {TelepathyClient.NotificationDirection} direction - direction of the message
 * @returns {TelepathyClient.Message} gnome-shell object describing the message.
 */
function makeMessageFromTpMessage(tpMessage, direction) {
    let [text, flags] = tpMessage.to_text();

    let timestamp = tpMessage.get_sent_timestamp();
    if (timestamp == 0)
        timestamp = tpMessage.get_received_timestamp();

    return {
        messageType: tpMessage.get_message_type(),
        text: text,
        sender: tpMessage.sender.alias,
        timestamp: timestamp,
        direction: direction
    };
}


/** Converts a Telepathy Logger Event into a local gnome-shell object ({@link Message}).
 * (TODO: where are the API docs for telepathy logger/telepathy logger events?)
 * @param {Tpl.Event} event - log
 * event to create the message from
 * @returns {TelepathyClient.Message} gnome-shell object describing the message.
 * @todo the link is the only API docs I could find online, but I do not trust
 * it to remain online. We should replace it with a more reliable link.
 */
function makeMessageFromTplEvent(event) {
    let sent = event.get_sender().get_entity_type() == Tpl.EntityType.SELF;
    let direction = sent ? NotificationDirection.SENT : NotificationDirection.RECEIVED;

    return {
        messageType: event.get_message_type(),
        text: event.get_message(),
        sender: event.get_sender().get_alias(),
        timestamp: event.get_timestamp(),
        direction: direction
    };
}

/** creates a new Client
 * Create a {@link Shell.TpClient} instance {@link #_tpClient} to monitor messages.
 *
 * Also, a {@link Tp.AccountManager}.
 * 
 * This is GNOME Shell's implementation of the Telepathy 'Client'
 * interface. Specifically, the shell is a Telepathy 'Observer', which
 * lets us see messages even if they belong to another app (eg,
 * Empathy).
 *
 * It is resonsible for listening to its underlying
 * {@link Shell.TpClient} instance {@link #_tpClient} and relaying chats, calls, requests and so on to the
 * user from it. This is mainly achieved by creating relevant message tray Sources
 * ({@link ApproverSource}, {@link ChatSource}) and notifications
 * ({@link ChatNotification}, {@link AudioVideoNotification}, ...) for the
 * requests.
 * @classdesc
 * This is GNOME Shell's implementation of the Telepathy 'Client'
 * interface. Specifically, the shell is a Telepathy 'Observer', which
 * lets us see messages even if they belong to another app (eg,
 * Empathy).
 *
 * We use a {@link Shell.TpClient} instance {@link #_tpClient} to monitor messages.
 *
 * @class
 */
function Client() {
    this._init();
};

Client.prototype = {
    _init : function() {
        // channel path -> ChatSource
        this._chatSources = {};
        this._chatState = Tp.ChannelChatState.ACTIVE;

        // account path -> AccountNotification
        this._accountNotifications = {};

        // Set up a SimpleObserver, which will call _observeChannels whenever a
        // channel matching its filters is detected.
        // The second argument, recover, means _observeChannels will be run
        // for any existing channel as well.

        /** A Tp.AccountManager...
         * @type {Tp.AccountManager} */
        this._accountManager = Tp.AccountManager.dup();
        /** A Shell.TpClient with {@link #_accountManager} as the account manager.
         * @type {Shell.TpClient} */
        this._tpClient = new Shell.TpClient({ 'account-manager': this._accountManager,
                                              'name': 'GnomeShell',
                                              'uniquify-name': true })
        this._tpClient.set_observe_channels_func(
            Lang.bind(this, this._observeChannels));
        this._tpClient.set_approve_channels_func(
            Lang.bind(this, this._approveChannels));
        this._tpClient.set_handle_channels_func(
            Lang.bind(this, this._handleChannels));

        // Workaround for gjs not supporting GPtrArray in signals.
        // See BGO bug #653941 for context.
        this._tpClient.set_contact_list_changed_func(
            Lang.bind(this, this._contactListChanged));

        // Allow other clients (such as Empathy) to pre-empt our channels if
        // needed
        this._tpClient.set_delegated_channels_callback(
            Lang.bind(this, this._delegatedChannelsCb));

        try {
            this._tpClient.register();
        } catch (e) {
            throw new Error('Couldn\'t register Telepathy client. Error: \n' + e);
        }


        // Watch subscription requests and connection errors
        this._subscriptionSource = null;
        this._accountSource = null;
        let factory = this._accountManager.get_factory();
        factory.add_account_features([Tp.Account.get_feature_quark_connection()]);
        factory.add_connection_features([Tp.Connection.get_feature_quark_contact_list()]);
        factory.add_contact_features([Tp.ContactFeature.SUBSCRIPTION_STATES,
                                      Tp.ContactFeature.ALIAS,
                                      Tp.ContactFeature.AVATAR_DATA]);

        this._accountManager.connect('account-validity-changed',
            Lang.bind(this, this._accountValidityChanged));

        this._accountManager.prepare_async(null, Lang.bind(this, this._accountManagerPrepared));
    },

    /** Called by {@link #_tpClient} when the channels that we are interested in
     * are announced.
     *
     * As far as I can tell this is used to retrieve our presence on that channel
     * and also to create chat sources for contacts we are talking with on
     * that channel ({@link #_finishObserveChannels}).
     * @inheritparams ChatSource
     * @param {Tp.SimpleObserver} - the observer
     * @param {Tp.Account} account - the relevant account
     * @param {Tp.Connection} conn - the relevant connection
 * @param {Tp.Channel[]} channels - array of relevant channels
     * @param {Tp.ChannelDispatchOperation} dispatchOp - the relevant channel
     * dispatch operation.
     * @param {Tp.ChannelRequest[]} requests - the requests that have been passed
     * @param {Tp.ObserveChannelsContext} context - context of an observe channels call.
     * @see #_finishObserveChannels
     * @see http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-client.html#tp-cli-client-observer-call-observe-channels */
    _observeChannels: function(observer, account, conn, channels,
                               dispatchOp, requests, context) {
        // If the self_contact doesn't have the ALIAS, make sure
        // to fetch it before trying to grab the channels.
        let self_contact = conn.get_self_contact();
        if (self_contact.has_feature(Tp.ContactFeature.ALIAS)) {
            this._finishObserveChannels(account, conn, channels, context);
        } else {
            Shell.get_self_contact_features(conn,
                                            contactFeatures,
                                            Lang.bind(this, function() {
                                                this._finishObserveChannels(account, conn, channels, context);
                                            }));
            context.delay();
        }
    },

    /** Called after {@link #_observeChannels} upon a chat channel that we are interested
     * in becoming available.
     * 
     * Creates a chat source for contacts who we are chatting with (?).
     * @see #_createChatSource
     * @inheritparams #_observeChannels
     */
    _finishObserveChannels: function(account, conn, channels, context) {
        let len = channels.length;
        for (let i = 0; i < len; i++) {
            let channel = channels[i];
            let [targetHandle, targetHandleType] = channel.get_handle();

            /* Only observe contact text channels */
            if ((!(channel instanceof Tp.TextChannel)) ||
               targetHandleType != Tp.HandleType.CONTACT)
               continue;

            /* Request a TpContact */
            Shell.get_tp_contacts(conn, [targetHandle],
                    contactFeatures,
                    Lang.bind(this,  function (connection, contacts, failed) {
                        if (contacts.length < 1)
                            return;

                        /* We got the TpContact */
                        this._createChatSource(account, conn, channel, contacts[0]);
                    }), null);
        }

        context.accept();
    },

    /** Creates a chat source for an account/channel/contact, making sure that
     * if the user removes the source we close the channel.
     * @see ChatSource
     * @inheritparams #_observeChannels
     * @inheritparams ChatSource
     */
    _createChatSource: function(account, conn, channel, contact) {
        if (this._chatSources[channel.get_object_path()])
            return;

        let source = new ChatSource(account, conn, channel, contact, this._tpClient);

        this._chatSources[channel.get_object_path()] = source;
        source.connect('destroy', Lang.bind(this,
                       function() {
                           if (this._tpClient.is_handling_channel(channel)) {
                               // The chat box has been destroyed so it can't
                               // handle the channel any more.
                               channel.close_async(function(src, result) {
                                   channel.close_finish(result);
                               });
                           }

                           delete this._chatSources[channel.get_object_path()];
                       }));
    },

    /** The 'handle channels' callback for {@link #_tpClient}, called when this
     * client should handle these channels, or when this client should present
     * channels that are already handling to the user (e.g. bring them to the
     * foreground).
     *
     * Calls {@link #_handlingChannels} to create notifications for channels
     * we are already handling.
     * @inheritparams #_observeChannels
     * @param {Tp.SimpleHandler} handler - the handler
     * @param {number} user_action_time - the time at which user action occured
     * @see #_handlingChannels
     * @see http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-client.html#tp-cli-client-handler-call-handle-channels */
    _handleChannels: function(handler, account, conn, channels,
                              requests, user_action_time, context) {
        this._handlingChannels(account, conn, channels);
        context.accept();
    },

    /** Handles ... channels ??
     * If we are handling a channel we notify from its source.
     * @see ChatSource#notify
     * @inheritparams #_observeChannels
     */
    _handlingChannels: function(account, conn, channels) {
        let len = channels.length;
        for (let i = 0; i < len; i++) {
            let channel = channels[i];

            // We can only handle text channel, so close any other channel
            if (!(channel instanceof Tp.TextChannel)) {
                channel.close_async(null);
                continue;
            }

            if (this._tpClient.is_handling_channel(channel)) {
                // We are already handling the channel, display the source
                let source = this._chatSources[channel.get_object_path()];
                if (source)
                    source.notify();
            }
        }
    },

    /** Displays a room invitation.
     *
     * This retrieves the contact information of the inviter and then calls
     * {@link #_createRoomInviteSource} to make the
     * {@link ApprovierSource} and {@link RoomInviteNotification} for the invite
     * allowing the user to accept/decline.
     * @inheritparams #_observeChannels
     * @inheritparams ChatSource
     */
    _displayRoomInvitation: function(conn, channel, dispatchOp, context) {
        // We can only approve the rooms if we have been invited to it
        let selfHandle = channel.group_get_self_handle();
        if (selfHandle == 0) {
            Shell.decline_dispatch_op(context, 'Not invited to the room');
            return;
        }

        let [invited, inviter, reason, msg] = channel.group_get_local_pending_info(selfHandle);
        if (!invited) {
            Shell.decline_dispatch_op(context, 'Not invited to the room');
            return;
        }

        // Request a TpContact for the inviter
        Shell.get_tp_contacts(conn, [inviter],
                contactFeatures,
                Lang.bind(this, this._createRoomInviteSource, channel, context, dispatchOp));

        context.delay();
     },

    /** Creates a {@link ApproverSource} and {@link RoomInviteNotification} in
     * the message tray for a room invitation and notifies it.
     * @inheritparams #_observeChannels
     * @inheritparams ChatSource
     * @param {Tp.Contact[]} contact - array of contacts
     * @param {Tp.Contact[]} failed - array of failed contacts
     * @param {Tp.Connection} connection - a connection
     * @see Shell.get_tpo_contacts
     */
    _createRoomInviteSource: function(connection, contacts, failed, channel, context, dispatchOp) {
        if (contacts.length < 1) {
            Shell.decline_dispatch_op(context, 'Failed to get inviter');
            return;
        }

        // We got the TpContact

        // FIXME: We don't have a 'chat room' icon (bgo #653737) use
        // system-users for now as Empathy does.
        let source = new ApproverSource(dispatchOp, _("Invitation"),
                                        Gio.icon_new_for_string('system-users'));
        Main.messageTray.add(source);

        let notif = new RoomInviteNotification(source, dispatchOp, channel, contacts[0]);
        source.notify(notif);
        context.accept();
    },

    /** The approve channels function for {@link #_tpClient}.
     * Calls {@link #_approveTextChannel} to approve a text channel,
     * {@link #_approveCall} to approve a call, or {@link #_approveFileTransfer}
     * to approve a file transfer.
     * @see #_approveTextChannel
     * @see #_approveCall
     * @see #_approveFileTransfer
     * @inheritparams #_observeChannels
     * @param {Tp.SimpleApprover} approver - the approver
     */
    _approveChannels: function(approver, account, conn, channels,
                               dispatchOp, context) {
        let channel = channels[0];
        let chanType = channel.get_channel_type();

        if (chanType == Tp.IFACE_CHANNEL_TYPE_TEXT)
            this._approveTextChannel(account, conn, channel, dispatchOp, context);
        else if (chanType == Tp.IFACE_CHANNEL_TYPE_STREAMED_MEDIA ||
                 chanType == 'org.freedesktop.Telepathy.Channel.Type.Call.DRAFT')
            this._approveCall(account, conn, channel, dispatchOp, context);
        else if (chanType == Tp.IFACE_CHANNEL_TYPE_FILE_TRANSFER)
            this._approveFileTransfer(account, conn, channel, dispatchOp, context);
    },

    /** Handles a new text channel (i.e. a chat).
     * If the text channel is from a contact (i.e. chat with a person) we use
     * {@link #_handlingChannels} which creates a notification from the corresponding
     * {@link ChatSource}.
     *
     * Otherwise if it's a room invite we use {@link #_displayRoomInvitation}
     * to create a {@link ApproverSource} and {@link RoomInviteNotification}.
     * @inheritparams #_observeChannels
     * @inheritparams ChatSource
     */
    _approveTextChannel: function(account, conn, channel, dispatchOp, context) {
        let [targetHandle, targetHandleType] = channel.get_handle();

        if (targetHandleType == Tp.HandleType.CONTACT) {
            // Approve private text channels right away as we are going to handle it
            dispatchOp.claim_with_async(this._tpClient,
                                        Lang.bind(this, function(dispatchOp, result) {
                try {
                    dispatchOp.claim_with_finish(result);
                    this._handlingChannels(account, conn, [channel]);
                } catch (err) {
                    throw new Error('Failed to Claim channel: ' + err);
                }}));

            context.accept();
        } else {
            this._displayRoomInvitation(conn, channel, dispatchOp, context);
        }
    },

    /** Handles an incoming call.
     * Calls {@link #_createAudioVideoSource} to create a {@link ApproverSource}
     * with {@link AudioVideoNotification}.
     * @see #_createAudioVideoSource
     * @inheritparams #_observeChannels
     * @inheritparams ChatSource
     */
    _approveCall: function(account, conn, channel, dispatchOp, context) {
        let [targetHandle, targetHandleType] = channel.get_handle();

        Shell.get_tp_contacts(conn, [targetHandle],
                contactFeatures,
                Lang.bind(this, this._createAudioVideoSource, channel, context, dispatchOp));

        context.delay();
    },

    /** Creates a {@link ApproverSource} for an incoming call and notifies an
     * {@link AudioVideoNotification} from it.
     * @inheritparams #_createRoomInviteSource
     */
    _createAudioVideoSource: function(connection, contacts, failed, channel, context, dispatchOp) {
        if (contacts.length < 1) {
            Shell.decline_dispatch_op(context, 'Failed to get inviter');
            return;
        }

        let isVideo = false;

        let props = channel.borrow_immutable_properties();

        if (props['org.freedesktop.Telepathy.Channel.Type.Call.DRAFT.InitialVideo'] ||
            props[Tp.PROP_CHANNEL_TYPE_STREAMED_MEDIA_INITIAL_VIDEO])
          isVideo = true;

        // We got the TpContact
        let source = new ApproverSource(dispatchOp, _("Call"), isVideo ?
                                        Gio.icon_new_for_string('camera-web') :
                                        Gio.icon_new_for_string('audio-input-microphone'));
        Main.messageTray.add(source);

        let notif = new AudioVideoNotification(source, dispatchOp, channel, contacts[0], isVideo);
        source.notify(notif);
        context.accept();
    },

    /** Handles a file transfer by calling {@link #_createFileTransferSource}
     * to create a {@link ApproverSource} and {@link FileTransferNotification}
     * for the request.
     * @inheritparams #_displayRoomInvitation
     */
    _approveFileTransfer: function(account, conn, channel, dispatchOp, context) {
        let [targetHandle, targetHandleType] = channel.get_handle();

        Shell.get_tp_contacts(conn, [targetHandle],
                contactFeatures,
                Lang.bind(this, this._createFileTransferSource, channel, context, dispatchOp));

        context.delay();
    },

    /** Creates a {@link ApproverSource} and notifies a
     * {@link FileTransferNotification} from it. Called when the user
     * receives a file transfer request.
     * @inheritparams #_createAudioVideoSource
     */
    _createFileTransferSource: function(connection, contacts, failed, channel, context, dispatchOp) {
        if (contacts.length < 1) {
            Shell.decline_dispatch_op(context, 'Failed to get file sender');
            return;
        }

        // Use the icon of the file being transferred
        let gicon = Gio.content_type_get_icon(channel.get_mime_type());

        // We got the TpContact
        let source = new ApproverSource(dispatchOp, _("File Transfer"), gicon);AA
        Main.messageTray.add(source);

        let notif = new FileTransferNotification(source, dispatchOp, channel, contacts[0]);
        source.notify(notif);
        context.accept();
    },

    /** The 'delegated channels' callback for {@link #_tpClient}.
     * Not sure what this means. It does nothing ("Nothing to do as we don't
     * make a distinction between observed and handled channels").
     */
    _delegatedChannelsCb: function(client, channels) {
        // Nothing to do as we don't make a distinction between observed and
        // handled channels.
    },

    /** Called when {@link #_accountManager} has finished being prepared/
     * initialised.
     *
     * This calls {@link #_accountValidityChanged} on each valid account to
     * connect up connection-status signals.
     * @param {Tp.AccountManager} am - the account manager {@link #_accountManager}
     * @param {?} result - result.
     */
    _accountManagerPrepared: function(am, result) {
        am.prepare_finish(result);

        let accounts = am.get_valid_accounts();
        for (let i = 0; i < accounts.length; i++) {
            this._accountValidityChanged(am, accounts[i], true);
        }
    },

    /** This listens to changes in the 'connection' and 'connection-status'
     * properties of the account `account`.
     *
     * Upon 'connection-status' changing {@link #_accountConnectionStatusNotifyCb}
     * is called notifying the user with a {@link AccountNotification}.
     *
     * On 'connection' changing {@link #_connectionChanged} is called which
     * monitors the user's contacts for that connection for authorization/subscription
     * status (the user adds or blocks or requests permission from another user).
     * @see #_accountConnectionStatusNotifyCb
     * @see #_connectionChanged
     * @inheritparams #_accountManagerPrepared
     * @inheritparams #_observeChannels
     * @param {boolean} valid - whether the account is valid.
     */
    _accountValidityChanged: function(am, account, valid) {
        if (!valid)
            return;

        // It would be better to connect to "status-changed" but we cannot.
        // See discussion in https://bugzilla.gnome.org/show_bug.cgi?id=654159
        account.connect("notify::connection-status",
                        Lang.bind(this, this._accountConnectionStatusNotifyCb));

        account.connect('notify::connection',
                        Lang.bind(this, this._connectionChanged));
        this._connectionChanged(account);
    },

    /** Callback when the 'connection' property of an account changes.
     * This calls {@link _contactListChanged} on the contacts for the account
     * to monitor for changes in permission levels for the contacts (e.g.
     * a contact blocks or adds you).
     * @inheritparams #_createAudioVideoSource
     */
    _connectionChanged: function(account) {
        let conn = account.get_connection();
        if (conn == null)
            return;

        this._tpClient.grab_contact_list_changed(conn);
        if (conn.get_contact_list_state() == Tp.ContactListState.SUCCESS) {
            this._contactListChanged(conn, conn.dup_contact_list(), []);
        }
    },

    /** Called by {@link #_connectionChanged} and whenever a user's contacts
     * change. This listens for the contact's 'subscription-states-changed'
     * signal and calls {@link #_subscriptionStateChanged}.
     * @see #_subscriptionStateChanged
     * @inheritparams #_observeChannels
     * @param {Tp.Contact[]} added - array of added contacts
     * @param {Tp.Contact[]} added - array of removed contacts
     */
    _contactListChanged: function(conn, added, removed) {
        for (let i = 0; i < added.length; i++) {
            let contact = added[i];

            contact.connect('subscription-states-changed',
                            Lang.bind(this, this._subscriptionStateChanged));
            this._subscriptionStateChanged(contact);
        }
    },

    /** Callback when the subscription state of a contact changes, i.e. the
     * contact is asking for permission to be friends with you or is blocking
     * you or accepting you (and vice versa).
     *
     * This potentially displays a {@link SubscriptionRequestNotification} to the
     * user asking them to accept or reject the contact's request.
     * @inheritparams ChatSource
     */
    _subscriptionStateChanged: function(contact) {
        if (contact.get_publish_state() != Tp.SubscriptionState.ASK)
            return;

        /* Implicitly accept publish requests if contact is already subscribed */
        if (contact.get_subscribe_state() == Tp.SubscriptionState.YES ||
            contact.get_subscribe_state() == Tp.SubscriptionState.ASK) {

            contact.authorize_publication_async(function(src, result) {
                src.authorize_publication_finish(result)});

            return;
        }

        /* Display notification to ask user to accept/reject request */
        let source = this._ensureSubscriptionSource();

        let notif = new SubscriptionRequestNotification(source, contact);
        source.notify(notif);
    },

    /** Creates a {@link MultiNotificationSource} instance and stores it (if
     * we don't have one already) and adds it to the message tray.
     *
     * It is used for subscription requests.
     * @returns {TelepathyClient.MultiNotificationSource} the notification source,
     * also stored in `this._subscriptionSource`. */
    _ensureSubscriptionSource: function() {
        if (this._subscriptionSource == null) {
            this._subscriptionSource = new MultiNotificationSource(
                _("Subscription request"), 'gtk-dialog-question');
            Main.messageTray.add(this._subscriptionSource);
            this._subscriptionSource.connect('destroy', Lang.bind(this, function () {
                this._subscriptionSource = null;
            }));
        }

        return this._subscriptionSource;
    },

    /** Called when the 'connection-status' property of an account changes
     * (e.g. we disconnect or reconnect).
     *
     * If we failed to connect a new {@link AccountNotification} is made
     * notifying the user of this and the reason.
     * @inheritparams #_observeChannels
     */
    _accountConnectionStatusNotifyCb: function(account) {
        let connectionError = account.connection_error;

        if (account.connection_status != Tp.ConnectionStatus.DISCONNECTED ||
            connectionError == Tp.error_get_dbus_name(Tp.Error.CANCELLED)) {
            return;
        }

        let notif = this._accountNotifications[account.get_object_path()];
        if (notif)
            return;

        /* Display notification that account failed to connect */
        let source = this._ensureAccountSource();

        notif = new AccountNotification(source, account, connectionError);
        this._accountNotifications[account.get_object_path()] = notif;
        notif.connect('destroy', Lang.bind(this, function() {
            delete this._accountNotifications[account.get_object_path()];
        }));
        source.notify(notif);
    },

    /** Creates a {@link MultiNotificationSource} instance and stores it (if
     * we don't have one already) and adds it to the message tray.
     *
     * It is used for account notifications (connection errors and so on).
     *
     * @returns {TelepathyClient.MultiNotificationSource} the notification source,
     * also stored in `this._accountSource`.
     */
    _ensureAccountSource: function() {
        if (this._accountSource == null) {
            this._accountSource = new MultiNotificationSource(
                _("Connection error"), 'gtk-dialog-error');
            Main.messageTray.add(this._accountSource);
            this._accountSource.connect('destroy', Lang.bind(this, function () {
                this._accountSource = null;
            }));
        }

        return this._accountSource;
    }
};

/** creates a new ChatSource.
 * We call the [parent constructor]{@link MessageTray.Source} using the contact's
 * alias as the title, and set {@link #isChat} to true.
 *
 * We store the variables `account`, `contact`, `client`, `conn` and `channel`
 * in {@link #_account}, {@link #_contact}, {@link #_client}, {@link #_conn}
 * and {@link #_channel}.
 *
 * We then create our {@link ChatNotification} stored in {@link #_notification}
 * and set its urgency to high (whenever we get a new chat it should pop up).
 *
 * We connect to the notification's expanded and collapsed event,
 * the message-sent, message-received, and pending-message-removed signals
 * of `channel`, and notify::alias, notify::avatar-file and presence-changed for
 * the contact (so that we update the Source accordingly).
 *
 * We set our icon to the contact's avatar, add {@link #_notification} to ourselves
 * without notifying ({@link #pushNotification}) and add ourselves to the
 * message tray ({@link MessageTray.MessageTray#add}).
 *
 * @param {Tp.Account} account - an account
 * @param {Tp.Connection} conn - a connection
 * @param {Tp.Channel} channel - a channel
 * @param {Tp.Contact} contact - a contact
 * @param {TelepathyClient.Client} client - a telepathy {@link Client}.
 * @classdesc
 * ![A {@link ChatSource} manages the avatar next to the username in the summary item/message tray; the {@link ChatNotification} is the notification](pics/telepathyClient.Chat.png)
 * @class
 * A ChatSource is a {@link MessageTray.Source} for a chat. Its icon is the
 * other user's avatar. Its relevant notification is a {@link ChatNotification}.
 *
 * It only has one {@link ChatNotification} as one source is created per conversation,
 * and the one notification displays all the messages from the conversation.
 * @extends MessageTray.Source
 */
function ChatSource(account, conn, channel, contact, client) {
    this._init(account, conn, channel, contact, client);
}

ChatSource.prototype = {
    __proto__:  MessageTray.Source.prototype,

    _init: function(account, conn, channel, contact, client) {
        MessageTray.Source.prototype._init.call(this, contact.get_alias());

        this.isChat = true;

        /** The underlying account this chat source is for.
         * @type {Tp.Account} */
        this._account = account;
        /** The contact this chat is with.
         * @type {Tp.Contact} contact - a contact */
        this._contact = contact;
        /** The {@link Client} controlling this source.
         * @type {TelepathyClient.Client} */
        this._client = client;

        this._pendingMessages = [];

        /** The connection for this conversation.
         * @type {Tp.Connection} */
        this._conn = conn;
        /** The channel for this conversation.
         * @type {Tp.Channel} */
        this._channel = channel;
        this._closedId = this._channel.connect('invalidated', Lang.bind(this, this._channelClosed));

        /** The notification for this source. There is only one (you have one
         * source per contact and its single notification displays all the
         * messages).
         * @type {TelepathyClient.ChatNotification} */
        this._notification = new ChatNotification(this);
        this._notification.setUrgency(MessageTray.Urgency.HIGH);
        this._notifyTimeoutId = 0;

        /** Whether we should acknowledge all chat messages when the notification
         * is closed ({@link #_ackMessages}).
         *
         * We ack messages when the message box is collapsed if user has
         * interacted with it before and so read the messages:
         *
         * - user clicked on it the tray
         * - user expanded the notification by hovering over the toaster notification
         * @see #_summaryItemClicked
         * @see #_notificationExpanded
         * @see #_notificationCollapsed
         * @type {boolean}
         */
        this._shouldAck = false;

        this.connect('summary-item-clicked', Lang.bind(this, this._summaryItemClicked));
        this._notification.connect('expanded', Lang.bind(this, this._notificationExpanded));
        this._notification.connect('collapsed', Lang.bind(this, this._notificationCollapsed));

        this._presence = contact.get_presence_type();

        this._sentId = this._channel.connect('message-sent', Lang.bind(this, this._messageSent));
        this._receivedId = this._channel.connect('message-received', Lang.bind(this, this._messageReceived));
        this._pendingId = this._channel.connect('pending-message-removed', Lang.bind(this, this._pendingRemoved));

        this._setSummaryIcon(this.createNotificationIcon());

        this._notifyAliasId = this._contact.connect('notify::alias', Lang.bind(this, this._updateAlias));
        this._notifyAvatarId = this._contact.connect('notify::avatar-file', Lang.bind(this, this._updateAvatarIcon));
        this._presenceChangedId = this._contact.connect('presence-changed', Lang.bind(this, this._presenceChanged));

        // Add ourselves as a source.
        Main.messageTray.add(this);
        this.pushNotification(this._notification);

        this._getLogMessages();
    },

    /** Called when the contact's alias changes. This sets the title for the
     * source to the new alias and uses {@link ChatNotification#appendAliasChange}
     * to let the user know in the notification ("oldname is now known as newname").
     * @see ChatNotification#appendAliasChange
     */
    _updateAlias: function() {
        let oldAlias = this.title;
        let newAlias = this._contact.get_alias();

        if (oldAlias == newAlias)
            return;

        this.setTitle(newAlias);
        this._notification.appendAliasChange(oldAlias, newAlias);
    },

    /** Implements the parent function to create an icon for the Source.
     *
     * This consists of the contact's avatar (or full colour 'avatar-default'
     * icon if the contact has no avatar) inside a St.Bin with style class
     * 'avatar-box'.
     * @override */
    createNotificationIcon: function() {
        this._iconBox = new St.Bin({ style_class: 'avatar-box' });
        this._iconBox._size = this.ICON_SIZE;
        let textureCache = St.TextureCache.get_default();
        let file = this._contact.get_avatar_file();

        if (file) {
            let uri = file.get_uri();
            this._iconBox.child = textureCache.load_uri_async(uri, this._iconBox._size, this._iconBox._size);
        } else {
            this._iconBox.child = new St.Icon({ icon_name: 'avatar-default',
                                                icon_type: St.IconType.FULLCOLOR,
                                                icon_size: this._iconBox._size });
        }

        return this._iconBox;
    },

    /** Callback when the contact's avatar changes.
     * This recreates the source icon and calls {@link #ChatNotification#update}
     * on our notification to update the avatar there. */
    _updateAvatarIcon: function() {
        this._setSummaryIcon(this.createNotificationIcon());
        this._notification.update(this._notification.title, null, { customContent: true, icon: this.createNotificationIcon() });
    },

    /** Implements the parent function to open the notification. In practice
     * `notification` is a {@link ChatNotification}.
     *
     * According to the comments - 
     * If we are handling the channel, try to pass it to Empathy
     * (using `client_delegate_channels_async`). Otherwise we are not the
     * handler, just ask to present the channel.
     *
     * @override
     */
    open: function(notification) {
          if (this._client.is_handling_channel(this._channel)) {
              // We are handling the channel, try to pass it to Empathy
              this._client.delegate_channels_async([this._channel], global.get_current_time(), '', null);
          }
          else {
              // We are not the handler, just ask to present the channel
              let dbus = Tp.DBusDaemon.dup();
              let cd = Tp.ChannelDispatcher.new(dbus);

              cd.present_channel_async(this._channel, global.get_current_time(), null);
          }
    },

    /** Called from the constructor. I *think* this retrieves the last
     * {@link SCROLLBACK_HISTORY_LINES} of messages from the conversation
     * history and then calls {@link #_displayPendingMessages} to display them. */
    _getLogMessages: function() {
        let logManager = Tpl.LogManager.dup_singleton();
        let entity = Tpl.Entity.new_from_tp_contact(this._contact, Tpl.EntityType.CONTACT);
        Shell.get_contact_events(logManager,
                                 this._account, entity,
                                 SCROLLBACK_HISTORY_LINES,
                                 Lang.bind(this, this._displayPendingMessages));
    },

    /** Displays new and old messages from the contact.
     *
     * We grab messages from `logManager` and use `channel_get_pending_messages`
     * to get pending messages. We update the source's count of unread
     * messages using {@link #_updateCount}.
     *
     * Then we add all old messages from the `logManager` followed by the
     * pending messages to our chat notification using
     * {@link ChatNotification#appendMessage}.
     *
     * Finally if we have any pending messages we notify the user
     * ({@link #notify}).
     *
     * @see makeMessageFromTpmessage
     * @see #_getLogMessages
     */
    _displayPendingMessages: function(logManager, result) {
        let [success, events] = logManager.get_filtered_events_finish(result);

        let logMessages = events.map(makeMessageFromTplEvent);

        let pendingTpMessages = this._channel.get_pending_messages();
        let pendingMessages = [];

        for (let i = 0; i < pendingTpMessages.length; i++) {
            let message = pendingTpMessages[i];

            if (message.get_message_type() == Tp.ChannelTextMessageType.DELIVERY_REPORT)
                continue;

            pendingMessages.push(makeMessageFromTpMessage(message, NotificationDirection.RECEIVED));

            this._pendingMessages.push(message);
        }

        this._updateCount();

        let showTimestamp = false;

        for (let i = 0; i < logMessages.length; i++) {
            let logMessage = logMessages[i];
            let isPending = false;

            // Skip any log messages that are also in pendingMessages
            for (let j = 0; j < pendingMessages.length; j++) {
                let pending = pendingMessages[j];
                if (logMessage.timestamp == pending.timestamp && logMessage.text == pending.text) {
                    isPending = true;
                    break;
                }
            }

            if (!isPending) {
                showTimestamp = true;
                this._notification.appendMessage(logMessage, true, ['chat-log-message']);
            }
        }

        if (showTimestamp)
            this._notification.appendTimestamp();

        for (let i = 0; i < pendingMessages.length; i++)
            this._notification.appendMessage(pendingMessages[i], true);

        if (pendingMessages.length > 0)
            this.notify();
    },

    /** Callback when {@link #_channel} emits 'invalidated', i.e. the channel
     * for this conversation is closed.
     *
     * We disconnect all our outstanding signals and destroy the notification.
     */
    _channelClosed: function() {
        this._channel.disconnect(this._closedId);
        this._channel.disconnect(this._receivedId);
        this._channel.disconnect(this._pendingId);
        this._channel.disconnect(this._sentId);

        this._contact.disconnect(this._notifyAliasId);
        this._contact.disconnect(this._notifyAvatarId);
        this._contact.disconnect(this._presenceChangedId);

        this.destroy();
    },

    /** Overrides the parent function to update the count of pending messages
     * shown over the user's icon in the message tray.
     * We call {@link #_setCount}, setting the count to the number of
     * pending chats, and showing the counter if we have at least 1 pending
     * chat.
     *
     * (The default implementation in {@link MessageTray.Source} shows the
     * number of *notifications* the source has, but this source has only
     * one notification with many messages).
     * @override
     */
    _updateCount: function() {
        this._setCount(this._pendingMessages.length, this._pendingMessages.length > 0);
    },

    /** Callback when a message is receieved ('message-received' on
     * {@link #_channel}).
     *
     * We add the message to our pending messages, update the count of unread
     * messages ({@link #_updateCount}), convert it to a {@link Message}
     * ({@link makeMessageFromTpMessage}) and append it to our notification
     * ({@link ChatNotification#appendMessage}).
     *
     * We wait 500ms before actually notifying from the source just in case
     * they somehow acknowledge it in the meantime (??). See
     * {@link #_notifyTimeout}.
     *
     * @param {Tp.Channel} channel - the channel that emitted the signal
     * ({@link #_channel})
     * @param {Tp.Message} message - the message
     * @see {@link http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-text-channel.html#TpTextChannel-message-received}
     */
    _messageReceived: function(channel, message) {
        if (message.get_message_type() == Tp.ChannelTextMessageType.DELIVERY_REPORT)
            return;

        this._pendingMessages.push(message);
        this._updateCount();

        message = makeMessageFromTpMessage(message, NotificationDirection.RECEIVED);
        this._notification.appendMessage(message);

        // Wait a bit before notifying for the received message, a handler
        // could ack it in the meantime.
        if (this._notifyTimeoutId != 0)
            Mainloop.source_remove(this._notifyTimeoutId);
        this._notifyTimeoutId = Mainloop.timeout_add(500,
            Lang.bind(this, this._notifyTimeout));
    },

    /** Timeout 500ms after receiving a message ({@link #_messageReceived}).
     * Calls {@link #notify} to notify the user, provided there are pending
     * messages (the reason for the timeout is in case the user dismisses/acknowledges
     * the message in between us receiving it and us notifying).
     */
    _notifyTimeout: function() {
        if (this._pendingMessages.length != 0)
            this.notify();

        this._notifyTimeoutId = 0;

        return false;
    },

    /** Callback when a message is receieved ('message-received' on
     * {@link #_channel}).
     *
     * This is called for both messages we send from
     * our client and other clients as well.
     *
     * We create a {@link Message} from `message` ({@link makeMessageFromTpMessage})
     * with direction {@link NotificationDirection.SENT} and append it
     * to our notification.
     * @see ChatNoticiation#appendMessage
     * @inheritparams #_messageReceived
     * @param {number} flags - {@link Tp.MessageSendingFlags} affecting how
     * the message was sent
     * @param {?string} token - "an opaque token used to match any incoming delivery
     * or failure reports against this message, or `null` if the message is not
     * readily identifiable".
     * @see {@link http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-text-channel.html#TpTextChannel-message-sent}
     */
    _messageSent: function(channel, message, flags, token) {
        message = makeMessageFromTpMessage(message, NotificationDirection.SENT);
        this._notification.appendMessage(message);
    },

    /** Overrides the parent function.
     * This just calls the parent function {@link MessageTray.Source#notify}
     * with {@link #_notification} as the `notification` parameter.
     * @override */
    notify: function() {
        MessageTray.Source.prototype.notify.call(this, this._notification);
    },

    /** Send a response to a message (usually via the entry box of the
     * chat notification).
     *
     * This sends the message via {@link #_channel}, setting the type appropriately
     * if it is an action (i.e. starts with '/me ', e.g. '/me nods head').
     * Called from {@link ChatNotification#_onEntryActivated}.
     * @param {string} text - the message to send back
     */
    respond: function(text) {
        let type;
        if (text.slice(0, 4) == '/me ') {
            type = Tp.ChannelTextMessageType.ACTION;
            text = text.slice(4);
        } else {
            type = Tp.ChannelTextMessageType.NORMAL;
        }

        let msg = Tp.ClientMessage.new_text(type, text);
        this._channel.send_message_async(msg, 0, Lang.bind(this, function (src, result) {
            this._channel.send_message_finish(result); 
        }));
    },

    /** Sets the chat state (composing, active, inactive, ...).
     * For example when the user changes the text in the text box on the chat
     * notification, this is called to set the state to 'COMPOSING'.
     * @param {Tp.ChannelChatState} state - the state to set the chat
     * to.
     */
    setChatState: function(state) {
        // We don't want to send COMPOSING every time a letter is typed into
        // the entry. We send the state only when it changes. Telepathy/Empathy
        // might change it behind our back if the user is using both
        // gnome-shell's entry and the Empathy conversation window. We could
        // keep track of it with the ChatStateChanged signal but it is good
        // enough right now.
        if (state != this._chatState) {
          this._chatState = state;
          this._channel.set_chat_state_async(state, null);
        }
    },

    /** Callback when the contact's presence changes.
     *
     * This adds a message to the notification ({@link ChatNotification#appendPresence}),
     * e.g. "username is offline.".
     *
     * If the user has switched from offline to available, or have gone offline,
     * the user is additionally notified of the presence change.
     * @see {@link http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-contact.html#TpContact-presence-changed}
     * @inheritparams ChatSource
     * @param {Tp.Contact} contact - contact whose presence changed
     * ({@link #_contact}).
     * @param {Tp.ConnectionPresenceType} presence - the new
     * presence (`OFFLINE`, `AVAILABLE`, `AWAY`, `EXTENDED_AWAY`,
     * `HIDDEN`, `BUSY`, `UNKNOWN`).
     * @param {string} status - a string representing the status of the contact,
     * e.g. "available".
     * @param {string} message - the contact's status message if they set it,
     * or '' if they haven't.
     */
    _presenceChanged: function (contact, presence, status, message) {
        let msg, shouldNotify, title;

        if (this._presence == presence)
          return;

        title = GLib.markup_escape_text(this.title, -1);

        if (presence == Tp.ConnectionPresenceType.AVAILABLE) {
            msg = _("%s is online.").format(title);
            shouldNotify = (this._presence == Tp.ConnectionPresenceType.OFFLINE);
        } else if (presence == Tp.ConnectionPresenceType.OFFLINE ||
                   presence == Tp.ConnectionPresenceType.EXTENDED_AWAY) {
            presence = Tp.ConnectionPresenceType.OFFLINE;
            msg = _("%s is offline.").format(title);
            shouldNotify = (this._presence != Tp.ConnectionPresenceType.OFFLINE);
        } else if (presence == Tp.ConnectionPresenceType.AWAY) {
            msg = _("%s is away.").format(title);
            shouldNotify = false;
        } else if (presence == Tp.ConnectionPresenceType.BUSY) {
            msg = _("%s is busy.").format(title);
            shouldNotify = false;
        } else
            return;

        this._presence = presence;

        if (message)
            msg += ' <i>(' + GLib.markup_escape_text(message, -1) + ')</i>';

        this._notification.appendPresence(msg, shouldNotify);
        if (shouldNotify)
            this.notify();
    },

    /** Callback for the 'pending-message-removed' signal of {@link #_channel},
     * e.g. when a message has been acknowledged ("acked").
     *
     * This removes the message from our list of pending messages and updates
     * the count of pending messages.
     * @inheritparams #_messageSent
     * @see {@link http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-text-channel.html#TpTextChannel-pending-message-removed}
     */
    _pendingRemoved: function(channel, message) {
        let idx = this._pendingMessages.indexOf(message);

        if (idx >= 0) {
            this._pendingMessages.splice(idx, 1);
            this._updateCount();
        }
        else
            throw new Error('Message not in our pending list: ' + message);
    },

    /** Acknowledges messages (i.e. marks them as read).
     * @see {@link http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-text-channel.html#tp-text-channel-ack-all-pending-messages-async `tp_text_channel_ack_all_pending_messages_async`}
     */
    _ackMessages: function() {
        // Don't clear our messages here, tp-glib will send a
        // 'pending-message-removed' for each one.
        this._channel.ack_all_pending_messages_async(Lang.bind(this, function(src, result) {
            this._channel.ack_all_pending_messages_finish(result);}));
    },

    /** Callback for our {@link .summary-item-clicked} signal, when the user
     * clicks on the summary item for this source in the message tray (which
     * incidentally opens the chat notification).
     *
     * If they left-clicked the item, we set {@link #_shouldAck} to `true`
     * meaning that when the notification is collapsed we acknowledge all the
     * messages using {@link #_ackMessages}. (This communicates back that the
     * messages are no longer pending).
     * @inheritparams .summary-item-clicked
     */
    _summaryItemClicked: function(source, button) {
        if (button != 1)
            return;

        this._shouldAck = true;
    },

    /** Callback when the user expands the notification
     * ({@link ChatNotification#expanded}).
     *
     * We set {@link #_shouldAck} to `true`
     * meaning that when the notification is collapsed we acknowledge all the
     * messages using {@link #_ackMessages}. (This communicates back that we
     * have read the messages and they are no longer pending).
     */
    _notificationExpanded: function() {
        this._shouldAck = true;
    },

    /** Callback when the notification is collapsed
     * ({@link ChatNotification#collapsed}).
     *
     * If {@link #_shouldAck} is `true` we call {@link #_ackMessages} to
     * mark all the messages as read. */
    _notificationCollapsed: function() {
        if (this._shouldAck)
            this._ackMessages();

        this._shouldAck = false;
    }
};

/** creates a new ChatNotification
 *
 * We call the [parent constructor]{@link MessageTray.Notification} with
 * custom content and set ourselves resident with {@link #setResident}.
 *
 * We create a St.Entry for the user to type their messages into
 * {@link #_responseEntry} and use {@link #setActionArea} on it.
 *
 * We also set up a scrollview for the messages so that the use can scroll
 * through past messages.
 *
 * We also use a {@link History.HistoryManager} to give history to the notification
 * (the user can press up and down to retrieve past sent messages). This history
 * is *not* tied to a gsettings key.
 * @param {MessageTray.ChatSource} source - the ChatSource that owns this
 * notification.
 * @classdesc
 * ![A {@link ChatNotification} showing from a {@link ChatSource}](pics/telepathyClient.Chat.png)
 *
 * This is a notification for sending and receiving chat messages. It belongs
 * to a {@link ChatSource} which we use to actually communicate responses
 * to the underlying telepathy objects.
 * @class
 * @extends MessageTray.Notification
 */
function ChatNotification(source) {
    this._init(source);
}

ChatNotification.prototype = {
    __proto__:  MessageTray.Notification.prototype,

    _init: function(source) {
        MessageTray.Notification.prototype._init.call(this, source, source.title, null, { customContent: true });
        this.setResident(true);

        /** The text box in the notification used to type messages.
         * It has style class 'chat-response'.
         * @type {St.Entry} */
        this._responseEntry = new St.Entry({ style_class: 'chat-response',
                                             can_focus: true });
        this._responseEntry.clutter_text.connect('activate', Lang.bind(this, this._onEntryActivated));
        this._responseEntry.clutter_text.connect('text-changed', Lang.bind(this, this._onEntryChanged));
        this.setActionArea(this._responseEntry);

        this._oldMaxScrollAdjustment = 0;
        this._createScrollArea();
        this._lastGroup = null;
        this._lastGroupActor = null;

        this._scrollArea.vscroll.adjustment.connect('changed', Lang.bind(this, function(adjustment) {
            let currentValue = adjustment.value + adjustment.page_size;
            if (currentValue == this._oldMaxScrollAdjustment)
                this.scrollTo(St.Side.BOTTOM);
            this._oldMaxScrollAdjustment = adjustment.upper;
        }));

        /** A history manager for the text box {@link #_responseEntry}.
         * No gsettings key but the entry parameter is set to
         * {@link #_responseEntry}.
         * @type {History.HistoryManager} */
        this._inputHistory = new History.HistoryManager({ entry: this._responseEntry.clutter_text });

        this._history = [];
        this._timestampTimeoutId = 0;
        this._composingTimeoutId = 0;
    },

    /** This appends a message to the chat notification.
     *
     * The message gets style `message.messageType` ('chat-sent' or 'chat-received').
     *
     * If the message type is an ACTION (e.g. '/me nods and stares.') the
     * message is marked up in italic and additionally gets style 'chat-action'.
     *
     * If it is a received message we call {@link #update} with `messagBody` which
     * updates the notification's banner.
     *
     * Regardless we call {@link #_append} with the styles etc to add the
     * message to our notification.
     * @param {TelepathyClient.Message} message - An object describing the
     * message (properties `text`, `messageType`, `sender`, `timestamp` and
     * `direction`, see the type definition).
     * @param {boolean} noTimestamp - Whether to add a timestamp.
     * If `true`, no timestamp will be added, regardless of the difference since the
     *  last timestamp
     */
    appendMessage: function(message, noTimestamp) {
        let messageBody = GLib.markup_escape_text(message.text, -1);
        let styles = [message.direction];

        if (message.messageType == Tp.ChannelTextMessageType.ACTION) {
            let senderAlias = GLib.markup_escape_text(message.sender, -1);
            messageBody = '<i>%s</i> %s'.format(senderAlias, messageBody);
            styles.push('chat-action');
        }

        if (message.direction == NotificationDirection.RECEIVED) {
            this.update(this.source.title, messageBody, { customContent: true,
                                                          bannerMarkup: true });
        }

        let group = (message.direction == NotificationDirection.RECEIVED ?
                     'received' : 'sent');

        this._append({ body: messageBody,
                       group: group,
                       styles: styles,
                       timestamp: message.timestamp,
                       noTimestamp: noTimestamp });
    },

    /** Destroys messages so that our notification doesn't get too long.
     *
     * If the most
     * recent message (before the one we just added) is within
     * {@link SCROLLBACK_RECENT_TIME}, we will keep
     * {@link SCROLLBACK_RECENT_LENGTH} previous messages. Otherwise
     * we'll keep {@link SCROLLBACK_IDLE_LENGTH} messages.
     */
    _filterMessages: function() {
        if (this._history.length < 1)
            return;

        let lastMessageTime = this._history[0].time;
        let currentTime = (Date.now() / 1000);

        // Keep the scrollback from growing too long. If the most
        // recent message (before the one we just added) is within
        // SCROLLBACK_RECENT_TIME, we will keep
        // SCROLLBACK_RECENT_LENGTH previous messages. Otherwise
        // we'll keep SCROLLBACK_IDLE_LENGTH messages.

        let maxLength = (lastMessageTime < currentTime - SCROLLBACK_RECENT_TIME) ?
            SCROLLBACK_IDLE_LENGTH : SCROLLBACK_RECENT_LENGTH;

        let filteredHistory = this._history.filter(function(item) { return item.realMessage });
        if (filteredHistory.length > maxLength) {
            let lastMessageToKeep = filteredHistory[maxLength];
            let expired = this._history.splice(this._history.indexOf(lastMessageToKeep));
            for (let i = 0; i < expired.length; i++)
                expired[i].actor.destroy();
        }

        let groups = this._contentArea.get_children();
        for (let i = 0; i < groups.length; i ++) {
            let group = groups[i];
            if (group.get_children().length == 0)
                group.destroy();
        }
    },

    /** First we highlight URLs by creating a {@link MessageTray.URLHighlighter}
     * on `props.body` with line wrap and allowing markup.
     *
     * We add the styles in `styles` to the highlighter's actor and add the actor
     * to a St.BoxLayout with style class 'chat-group-`group`'.
     *
     * If `props.noTimestamp` is `false` and `props.timestamp` is
     * at least {@link SCROLLBACK_IMMEDIATE_TIME} (1 minute) before the current
     * time, we add a timestamp to the notification ({@link #appendTimestamp}).
     *
     * If `props.noTimestamp` is `false` but the last message was within
     * {@link SCROLLBACK_IMMEDIATE_TIME} of the current time, we schedule the
     * timestamp to be added in {@link SCROLLBACK_IMMEDIATE_TIME} after the
     * message's timestamp (i.e. a minute after the user sent the message,
     * a timestamp "Sent at [time], [day]" appears on the notification).
     *
     * Finally we call {@link #_filterMessages} to trim the number of messages
     * in the notification.
     *
     * @param {Object} props - an object describing the message to append:
     * @param {string} [props.body=null] - The text of the message.
     * @param {string} [props.group=null] - The group of the message, one of:
     *         'received', 'sent', 'meta'.
     * @param {string[props.]} [styles=[]] - Style class names for the message to have
     * (e.g. 'chat-sent', 'chat-received', 'chat-action').
     * @param {number} [props.timestamp=Date.now()/1000] - The timestamp of the message.
     * @param {boolean} [props.noTimestamp=false] - suppress timestamp signal?
     * @param {Object} [props.childProps=null] - props to add the actor with
     * (the standard St ones):
     * @param {boolean} [props.childProps.x_fill=false] - whether to fill the container in the x direction
     * @param {boolean} [props.childProps.x_align=St.Align.MIDDLE] - how to align horizontally within its allocation
     * @param {boolean} [props.childProps.y_fill=false] - whether to fill the container in the y direction
     * @param {boolean} [props.childProps.y_align=St.Align.MIDDLE] - how to align vertically within its allocation
     * @param {boolean} [props.childProps.expand=false] - whether to allocate the child *extra* space (somehow different to `x_fill` and `y_fill`? If the fill option is not set but
     * expand is, then the alignment properties can be used.)
     */
    _append: function(props) {
        let currentTime = (Date.now() / 1000);
        props = Params.parse(props, { body: null,
                                      group: null,
                                      styles: [],
                                      timestamp: currentTime,
                                      noTimestamp: false,
                                      childProps: null });

        // Reset the old message timeout
        if (this._timestampTimeoutId)
            Mainloop.source_remove(this._timestampTimeoutId);

        let highlighter = new MessageTray.URLHighlighter(props.body,
                                                         true,  // line wrap?
                                                         true); // allow markup?

        let body = highlighter.actor;

        let styles = props.styles;
        for (let i = 0; i < styles.length; i ++)
            body.add_style_class_name(styles[i]);

        let group = props.group;
        if (group != this._lastGroup) {
            let style = 'chat-group-' + group;
            this._lastGroup = group;
            this._lastGroupActor = new St.BoxLayout({ style_class: style,
                                                      vertical: true });
            this.addActor(this._lastGroupActor);
        }

        this._lastGroupActor.add(body, props.childProps);

        let timestamp = props.timestamp;
        this._history.unshift({ actor: body, time: timestamp,
                                realMessage: group != 'meta' });

        if (!props.noTimestamp) {
            if (timestamp < currentTime - SCROLLBACK_IMMEDIATE_TIME)
                this.appendTimestamp();
            else
                // Schedule a new timestamp in SCROLLBACK_IMMEDIATE_TIME
                // from the timestamp of the message.
                this._timestampTimeoutId = Mainloop.timeout_add_seconds(
                    SCROLLBACK_IMMEDIATE_TIME - (currentTime - timestamp),
                    Lang.bind(this, this.appendTimestamp));
        }

        this._filterMessages();
    },

    /** Formats a timestamp:
     *
     * * Show a week day and time if date is in the last week
     *   ("Sent at 15:21:07 on Tuesday").
     * * Otherwise show a week day and date if the message is in the same year
     *   ("Sent on Tuesday, November 6").
     * * Otherwise just show the weekday with a full date
     *   ("Sent on Tuesday, November 6, 2012").
     *
     * @param {Date} date - the date to format.
     * @returns {string} the date formatted as specified above.
     */
    _formatTimestamp: function(date) {
        let now = new Date();

        var daysAgo = (now.getTime() - date.getTime()) / (24 * 60 * 60 * 1000);

        let format;

        // Show a week day and time if date is in the last week
        if (daysAgo < 1 || (daysAgo < 7 && now.getDay() != date.getDay())) {
            /* Translators: this is a time format string followed by a date.
             If applicable, replace %X with a strftime format valid for your
             locale, without seconds. */
            // xgettext:no-c-format
            format = _("Sent at <b>%X</b> on <b>%A</b>");

        } else if (date.getYear() == now.getYear()) {
            /* Translators: this is a time format in the style of "Wednesday, May 25",
             shown when you get a chat message in the same year. */
            // xgettext:no-c-format
            format = _("Sent on <b>%A</b>, <b>%B %d</b>");
        } else {
            /* Translators: this is a time format in the style of "Wednesday, May 25, 2012",
             shown when you get a chat message in a different year. */
            // xgettext:no-c-format
            format = _("Sent on <b>%A</b>, <b>%B %d</b>, %Y");
        }

        return date.toLocaleFormat(format);
    },

    /** Appends a timestamp to the notification.
     *
     * This uses the last message time and calls {@link #_append} and
     * {@link #_formatTimestamp} to add the timestamp message ("Sent at
     * 13:02 on Wednesday") to the message. Style class is 'chat-meta-message',
     * group is 'meta'.
     *
     * Finally we call {@link #_filterMessages} to trim the number of messages
     * in the notification.
     */
    appendTimestamp: function() {
        let lastMessageTime = this._history[0].time;
        let lastMessageDate = new Date(lastMessageTime * 1000);

        let timeLabel = this._append({ body: this._formatTimestamp(lastMessageDate),
                                       group: 'meta',
                                       styles: ['chat-meta-message'],
                                       childProps: { expand: true, x_fill: false,
                                                     x_align: St.Align.END },
                                       noTimestamp: true,
                                       timestamp: lastMessageTime });

        this._filterMessages();

        return false;
    },

    /** Appends a presence message ("username is offline.") to the notification
     * using {@link #_append} with `text` as the body, 'meta' as the group,
     * and 'chat-meta-message' as the style.
     *
     * We also call {@link #update}, with the title being `text` if
     * `asTitle` is `true`, and just our source's title (the contact's
     * username) otherwise (which is the default).
     *
     * Finally we call {@link #_filterMessages} to trim the number of messages
     * in the notification.
     * @param {?} text
     * @param {?} asTitle
     */
    appendPresence: function(text, asTitle) {
        if (asTitle)
            this.update(text, null, { customContent: true, titleMarkup: true });
        else
            this.update(this.source.title, null, { customContent: true });

        let label = this._append({ body: text,
                                   group: 'meta',
                                   styles: ['chat-meta-message'] });

        this._filterMessages();
    },

    /** Appends an alias change message to our notification ("oldname is now
     * known as newname" in italics).
     *
     * We use {@link #_append} with the above message, group 'meta', and style
     * 'chat-meta-message'.
     *
     * We also call {@link #update} to set the title to `newAlias`.
     *
     * Finally we call {@link #_filterMessages} to trim the number of messages
     * in the notification.
     * @param {string} oldAlias - the contact's old alias
     * @param {string} newAlias - the contact's new alias
     */
    appendAliasChange: function(oldAlias, newAlias) {
        oldAlias = GLib.markup_escape_text(oldAlias, -1);
        newAlias = GLib.markup_escape_text(newAlias, -1);

        /* Translators: this is the other person changing their old IM name to their new
           IM name. */
        let message = '<i>' + _("%s is now known as %s").format(oldAlias, newAlias) + '</i>';

        let label = this._append({ body: message,
                                   group: 'meta',
                                   styles: ['chat-meta-message'] });

        this.update(newAlias, null, { customContent: true });

        this._filterMessages();
    },

    /** Callback when the user activates {@link #_responseEntry} (e.g.
     * type a message and press 'Enter').
     *
     * If the message is not empty, it is added to the history
     * {@link #_inputHistory} ({@link History.HistoryManager#addItem}).
     *
     * The text box is cleared, and we use {@link ChatSource#respond} with
     * the message to send a reply.
     * @see ChatSource#respond
     */
    _onEntryActivated: function() {
        let text = this._responseEntry.get_text();
        if (text == '')
            return;

        this._inputHistory.addItem(text);

        // Telepathy sends out the Sent signal for us.
        // see Source._messageSent
        this._responseEntry.set_text('');
        this.source.respond(text);
    },

    /** Callback once the user has stopped typing for
     * {@link COMPOSING_STOP_TIMEOUT} seconds.
     *
     * We set the chat state to PAUSED (from COMPOSING).
     *
     * @see ChatSource#setChatState
     */
    _composingStopTimeout: function() {
        this._composingTimeoutId = 0;

        this.source.setChatState(Tp.ChannelChatState.PAUSED);

        return false;
    },

    /** Callback when the text in {@link #_responseEntry} is changed, i.e. the
     * user is composing a message.
     *
     * If we're typing, we want to send COMPOSING.
     * If we empty the entry, we want to send ACTIVE.
     * If we've stopped typing for {@link COMPOSING_STOP_TIMEOUT}
     *    seconds, we want to send PAUSED.
     *
     * This uses {@link ChatSource#setChatState}.
     * @see ChatSource#setChatState
     */
    _onEntryChanged: function() {
        let text = this._responseEntry.get_text();

        // If we're typing, we want to send COMPOSING.
        // If we empty the entry, we want to send ACTIVE.
        // If we've stopped typing for COMPOSING_STOP_TIMEOUT
        //    seconds, we want to send PAUSED.

        // Remove composing timeout.
        if (this._composingTimeoutId > 0) {
            Mainloop.source_remove(this._composingTimeoutId);
            this._composingTimeoutId = 0;
        }

        if (text != '') {
            this.source.setChatState(Tp.ChannelChatState.COMPOSING);

            this._composingTimeoutId = Mainloop.timeout_add_seconds(
                COMPOSING_STOP_TIMEOUT,
                Lang.bind(this, this._composingStopTimeout));
        } else {
            this.source.setChatState(Tp.ChannelChatState.ACTIVE);
        }
    }
};

/** creates a new ApproverSource
 * We call the [parent constructor]{@link MessageTray.Source} with title
 * `text`.
 *
 * We store the underlying dispatch operation in `this._dispatchIcon` and
 * connect to its {@link http://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-proxy.html#TpProxy-invalidated|'invalidated'} signal, destroying ourselves
 * upon receiving it.
 * @param {Tp.ChannelDispatchOperation} dispatchOp - the dispatch operation.
 * @param {string} text - the title for the source ("Invitation", "Call",
 * "File Transfer").
 * @param {Gio.Icon} gicon - the icon to used in {@link #createNotificationIcon}.
 * @classdesc
 * An ApproverSource is a {@link MessageTray.Source} used for any approve
 * request - from a text channel, call, or file transfer.
 *
 * The icon is set upon creation of the source:
 *
 * * {@link RoomInviteNotification} uses the 'system-users' icon.
 * * {@link AudioVideoNotification} uses the 'camera-web' icon, if it's video,
 * 'audio-input-microphone' otherwise.
 * * {@link FileTransferNotification} uses the icon corresponding to the file's
 * mime type.
 *
 * We listen to the underlying dispatch operation and when it emits its
 * invalidated signal we destroy the source.
 * @see Client#_createFileTransferSource
 * @see Client#_createAudioVideoSource
 * @see Client#_createRoomInviteSource
 * @class
 * @extends MessageTray.Source
 */
function ApproverSource(dispatchOp, text, gicon) {
    this._init(dispatchOp, text, gicon);
}

ApproverSource.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function(dispatchOp, text, gicon) {
        MessageTray.Source.prototype._init.call(this, text);

        this._gicon = gicon;
        this._setSummaryIcon(this.createNotificationIcon());

        this._dispatchOp = dispatchOp;

        // Destroy the source if the channel dispatch operation is invalidated
        // as we can't approve any more.
        this._invalidId = dispatchOp.connect('invalidated',
                                             Lang.bind(this, function(domain, code, msg) {
            this.destroy();
        }));
    },

    /** Calls the parent destroy function, but also disconnects any signals
     * we have on `dispatchOp`.
     * @override */
    destroy: function() {
        if (this._invalidId != 0) {
            this._dispatchOp.disconnect(this._invalidId);
            this._invalidId = 0;
        }

        MessageTray.Source.prototype.destroy.call(this);
    },

    /** Implements the parent function, returning the `gicon` specified
     * in the constructor (full colour).
     * @override */
    createNotificationIcon: function() {
        return new St.Icon({ gicon: this._gicon,
                             icon_type: St.IconType.FULLCOLOR,
                             icon_size: this.ICON_SIZE });
    }
}

/** creates a new RoomInviteNotification
 *
 * This calls the [parent constructor]{@link MessageTray.Notification} with
 * title "Invitation to [roomname]" and custom content.
 *
 * It is set to resident.
 *
 * The body is set to "[inviter] is inviting you to join [roomname]"
 * ({@link #addBody}).
 *
 * Buttons "Decline" and "Accept" (action IDs 'decline' and 'accept') are
 * added ({@link #addButton}).
 *
 * Upon the action-invoked signal, we send the response to `dispatchOp` and destroy
 * ourselves.
 * @param {MessageTray.ApproverSource} source - the source for the notification.
 * @inheritparams Client#_createRoomInviteSource
 * @param {Tp.Contact} inviter - the inviter.
 * @classdesc
 * This is a notification for an invite to a room, with "Decline" and "Accept"
 * buttons.
 * @todo get a picture
 * @see Client#_createRoomInviteSource
 * @class
 * @extends MessageTray.Notification
 */
function RoomInviteNotification(source, dispatchOp, channel, inviter) {
    this._init(source, dispatchOp, channel, inviter);
}

RoomInviteNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, dispatchOp, channel, inviter) {
        MessageTray.Notification.prototype._init.call(this,
                                                      source,
                                                      /* translators: argument is a room name like
                                                       * room@jabber.org for example. */
                                                      _("Invitation to %s").format(channel.get_identifier()),
                                                      null,
                                                      { customContent: true });
        this.setResident(true);

        /* translators: first argument is the name of a contact and the second
         * one the name of a room. "Alice is inviting you to join room@jabber.org
         * for example. */
        this.addBody(_("%s is inviting you to join %s").format(inviter.get_alias(), channel.get_identifier()));

        this.addButton('decline', _("Decline"));
        this.addButton('accept', _("Accept"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            switch (action) {
            case 'decline':
                dispatchOp.leave_channels_async(Tp.ChannelGroupChangeReason.NONE,
                                                '', function(src, result) {
                    src.leave_channels_finish(result)});
                break;
            case 'accept':
                dispatchOp.handle_with_time_async('', global.get_current_time(),
                                                  function(src, result) {
                    src.handle_with_time_finish(result)});
                break;
            }
            this.destroy();
        }));
    }
};

/** creates a new AudioVideoNotification
 *
 * This calls the [parent constructor]{@link MessageTray.Notification} with
 * title "Video call from [contact]" or "Call from [contact]" and custom content.
 *
 * It is set to resident.
 *
 * Buttons "Reject" and "Answer" (action IDs 'reject' and 'answer') are
 * added ({@link #addButton}).
 *
 * Upon the action-invoked signal, we send the response to `dispatchOp` and destroy
 * ourselves.
 * @inheritparams Client#_createAudioVideoSource
 * @param {MessageTray.ApproverSource} source - the source for the notification.
 * @param {boolean} isVideo - whether it's a video call or just audio.
 * @param {Tp.Contact} contact - the caller.
 * @classdesc
 * ![{@link AudioVideoNotification}](pics/telepathyClient.AudioVideoNotification.png)
 *
 * This is a notification to accept or reject an audio or video call, with
 * "Reject" and "Answer" buttons.
 * @see Client#_createAudioVideoSource
 * @class
 * @extends MessageTray.Notification
 */
function AudioVideoNotification(source, dispatchOp, channel, contact, isVideo) {
    this._init(source, dispatchOp, channel, contact, isVideo);
}

AudioVideoNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, dispatchOp, channel, contact, isVideo) {
        let title = '';

        if (isVideo)
             /* translators: argument is a contact name like Alice for example. */
            title = _("Video call from %s").format(contact.get_alias());
        else
             /* translators: argument is a contact name like Alice for example. */
            title = _("Call from %s").format(contact.get_alias());

        MessageTray.Notification.prototype._init.call(this,
                                                      source,
                                                      title,
                                                      null,
                                                      { customContent: true });
        this.setResident(true);

        this.addButton('reject', _("Reject"));
        /* translators: this is a button label (verb), not a noun */
        this.addButton('answer', _("Answer"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            switch (action) {
            case 'reject':
                dispatchOp.leave_channels_async(Tp.ChannelGroupChangeReason.NONE,
                                                '', function(src, result) {
                    src.leave_channels_finish(result)});
                break;
            case 'answer':
                dispatchOp.handle_with_time_async('', global.get_current_time(),
                                                  function(src, result) {
                    src.handle_with_time_finish(result)});
                break;
            }
            this.destroy();
        }));
    }
};

// File Transfer
/** creates a new FileTransferNotification
 *
 * This calls the [parent constructor]{@link MessageTray.Notification} with
 * title "[contact] is sending your [filename]" and custom content.
 *
 * It is set to resident.
 *
 * Buttons "Decline" and "Accept" (action IDs 'decline' and 'accept') are
 * added ({@link #addButton}).
 *
 * Upon the action-invoked signal, we send the response to `dispatchOp` and destroy
 * ourselves.
 * @inheritparams Client#_createFileTransferSource
 * @param {MessageTray.ApproverSource} source - the source for the notification.
 * @param {Tp.Contact} contact - the sender.
 * @classdesc
 * This is a notification to accept or reject an audio or video call, with
 * "Decline" and "Accept" buttons.
 * @see Client#_createFileTransferSource
 * @class
 * @extends MessageTray.Notification
 * @todo get a pic
 */
function FileTransferNotification(source, dispatchOp, channel, contact) {
    this._init(source, dispatchOp, channel, contact);
}

FileTransferNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, dispatchOp, channel, contact) {
        MessageTray.Notification.prototype._init.call(this,
                                                      source,
                                                      /* To translators: The first parameter is
                                                       * the contact's alias and the second one is the
                                                       * file name. The string will be something
                                                       * like: "Alice is sending you test.ogg"
                                                       */
                                                      _("%s is sending you %s").format(contact.get_alias(),
                                                                                       channel.get_filename()),
                                                      null,
                                                      { customContent: true });
        this.setResident(true);

        this.addButton('decline', _("Decline"));
        this.addButton('accept', _("Accept"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            switch (action) {
            case 'decline':
                dispatchOp.leave_channels_async(Tp.ChannelGroupChangeReason.NONE,
                                                '', function(src, result) {
                    src.leave_channels_finish(result)});
                break;
            case 'accept':
                dispatchOp.handle_with_time_async('', global.get_current_time(),
                                                  function(src, result) {
                    src.handle_with_time_finish(result)});
                break;
            }
            this.destroy();
        }));
    }
};

/** Creates a new MultiNotificationSource.
 * We call the [parent constructor]{@link MessageTray.Source} with title
 * `title` and set the summary icon to `icon`.
 * @param {string} title - title for the source (e.g. "Connection error",
 * "Subscription request").
 * @param {string} icon - name of the icon to use ('gtk-dialog-question',
 * 'gti-dialog-error'), used in {@link #createNotificationIcon}.
 * Will be full colour.
 * @classdesc
 * A notification source that can embed multiple notifications.
 *
 * Used by:
 * * Account notifications ({@link AccountNotification})
 * * Subscription requests ({@link SubscriptionRequestNotification})
 * @todo get a picture
 * @see Client#_ensureAccountSource
 * @see Client#_ensureSubscriptionSource
 * @see AccountNotification
 * @see SubscriptionRequestNotification
 * @class
 * @extends MessageTray.Source
 */
function MultiNotificationSource(title, icon) {
    this._init(title, icon);
}

MultiNotificationSource.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function(title, icon) {
        MessageTray.Source.prototype._init.call(this, title);

        this._icon = icon;
        this._setSummaryIcon(this.createNotificationIcon());
        this._nbNotifications = 0;
    },

    /** Extends the parent implementation. We call {@link MessageTray.Source#notify}
     * with `notification` and monitor the notification's 'destroy' signal
     * such that when there are no notifications left, we destroy ourselves.
     * @override
     */
    notify: function(notification) {
        MessageTray.Source.prototype.notify.call(this, notification);

        this._nbNotifications += 1;

        // Display the source while there is at least one notification
        notification.connect('destroy', Lang.bind(this, function () {
            this._nbNotifications -= 1;

            if (this._nbNotifications == 0)
                this.destroy();
        }));
    },

    /** Implements the parent function, creating a full-colour icon from
     * the `icon` parameter passed to the [constructor.]{@link MultiNotificationSource}.
     * @override
     */
    createNotificationIcon: function() {
        return new St.Icon({ gicon: Shell.util_icon_from_string(this._icon),
                             icon_type: St.IconType.FULLCOLOR,
                             icon_size: this.ICON_SIZE });
    }
};

// Subscription request
/** Creates a new SubscriptionRequestNotification.
 *
 * This calls the [parent constructor]{@link MessageTray.Notification} with
 * title "[contact] would like permission to see when you are online" and
 * custom content.
 *
 * We add the user's avatar to the notification along with their subscription
 * request method if any, adding all of this with
 * {@link #addActor}.
 *
 * We add buttons 'decline' and 'accept' ({@link #addButton}).
 *
 * On our 'action-invoked' signal (the user clicked one of the buttons) we
 * either remove the contact if the user declined, or authorize the contact
 * if they accepted.
 *
 * @param {TelepathyClient.MultiNotificationSource} source - the source for
 * this notification, in this case a {@link MultiNotificationSource}.
 * @param {Tp.Contact} contact - the contact
 * @class
 * @classdesc
 * This is a subscription request notification when a contact wants permission
 * to see your online status ("otheruser would like permission to see when you
 * are online").
 *
 * The user gets "Decline" and "Accept" buttons to choose from.
 * @todo get a picture
 * @see Client#_subscriptionStateChanged
 * @extends MessageTray.Notification
 */
function SubscriptionRequestNotification(source, contact) {
    this._init(source, contact);
}

SubscriptionRequestNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, contact) {
        MessageTray.Notification.prototype._init.call(this, source,
            /* To translators: The parameter is the contact's alias */
            _("%s would like permission to see when you are online").format(contact.get_alias()),
            null, { customContent: true });

        this._contact = contact;
        this._connection = contact.get_connection();

        let layout = new St.BoxLayout({ vertical: false });

        // Display avatar
        let iconBox = new St.Bin({ style_class: 'avatar-box' });
        iconBox._size = 48;

        let textureCache = St.TextureCache.get_default();
        let file = contact.get_avatar_file();

        if (file) {
            let uri = file.get_uri();
            iconBox.child = textureCache.load_uri_async(uri, iconBox._size, iconBox._size);
        }
        else {
            iconBox.child = new St.Icon({ icon_name: 'avatar-default',
                                          icon_type: St.IconType.FULLCOLOR,
                                          icon_size: iconBox._size });
        }

        layout.add(iconBox);

        // subscription request message
        let label = new St.Label({ style_class: 'subscription-message',
                                   text: contact.get_publish_request() });

        layout.add(label);

        this.addActor(layout);

        this.addButton('decline', _("Decline"));
        this.addButton('accept', _("Accept"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            switch (action) {
            case 'decline':
                contact.remove_async(function(src, result) {
                    src.remove_finish(result)});
                break;
            case 'accept':
                // Authorize the contact and request to see his status as well
                contact.authorize_publication_async(function(src, result) {
                    src.authorize_publication_finish(result)});

                contact.request_subscription_async('', function(src, result) {
                    src.request_subscription_finish(result)});
                break;
            }

            // rely on _subscriptionStatesChangedCb to destroy the
            // notification
        }));

        this._changedId = contact.connect('subscription-states-changed',
            Lang.bind(this, this._subscriptionStatesChangedCb));
        this._invalidatedId = this._connection.connect('invalidated',
            Lang.bind(this, this.destroy));
    },

    /** Extends the parent function to destroy the notification.
     * It additionally disconnects outstanding signals we have on the contact
     * and connection.
     * @override */
    destroy: function() {
        if (this._changedId != 0) {
            this._contact.disconnect(this._changedId);
            this._changedId = 0;
        }

        if (this._invalidatedId != 0) {
            this._connection.disconnect(this._invalidatedId);
            this._invalidatedId = 0;
        }

        MessageTray.Notification.prototype.destroy.call(this);
    },

    /** Callback when the subscription state with the contact changes. We
     * destroy the notification if the subscription request has been answered
     * (i.e. `publish != Tp.SubscriptionState.ASK`).
     */
    _subscriptionStatesChangedCb: function(contact, subscribe, publish, msg) {
        // Destroy the notification if the subscription request has been
        // answered
        if (publish != Tp.SubscriptionState.ASK)
            this.destroy();
    }
};


/** creates a new AccountNotification
 *
 * We call the [parent constructor]{@link MessageTray.notification} with
 * title "Connection to [account] failed" and custom content.
 *
 * We display the appropriate error message using {@link _connectionErrorMessages}
 * and `connectionError`.
 *
 * We add buttons "Reconnect" and "Edit account" using
 * {@link #addButton}.
 *
 * We connect to our 'action-invoked' signal (one of the buttons has
 * been clicked) and try to reconnect if that's what the user chose,
 * or launch program `empath-accounts` with the account if the user wishes
 * to edit the account, either way destroying the notification.
 *
 * If we notice that the account has been disabled or invalidated or we have
 * connected successfully, we destroy this notification.
 *
 * @inheritparams SubscriptionRequestNotification
 * @inheritparams Client#_accountConnectionStatusNotifyCb
 * @param {string} connectionError - the connection error
 * (e.g. 'org.freedesktop.Telepathy.Error.AuthenticationFailed').
 * @classdesc
 * This notification is presented when there is an error connecting an account.
 *
 * It pops up a notification displaying the error, giving the user the option
 * to reconnect or edit the account details.
 * @class
 * @todo a picture
 * @extends MessageTray.Notification
 * @see Client#_accountConnectionStatusNotifyCb
 */
function AccountNotification(source, account, connectionError) {
    this._init(source, account, connectionError);
}

// Messages from empathy/libempathy/empathy-utils.c
// create_errors_to_message_hash()

/** Hash between dbus error names and human-readable strings.
 *
 * Example: 'org.freedesktop.Telepathy.Error.AuthenticationFailed' turns into
 * "Authentication failed".
 * @type {Object.<string, string>}
 */
/* Translator note: these should be the same messages that are
 * used in Empathy, so just copy and paste from there. */
let _connectionErrorMessages = {};
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.NETWORK_ERROR)]
  = _("Network error");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.AUTHENTICATION_FAILED)]
  = _("Authentication failed");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.ENCRYPTION_ERROR)]
  = _("Encryption error");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_NOT_PROVIDED)]
  = _("Certificate not provided");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_UNTRUSTED)]
  = _("Certificate untrusted");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_EXPIRED)]
  = _("Certificate expired");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_NOT_ACTIVATED)]
  = _("Certificate not activated");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_HOSTNAME_MISMATCH)]
  = _("Certificate hostname mismatch");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_FINGERPRINT_MISMATCH)]
  = _("Certificate fingerprint mismatch");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_SELF_SIGNED)]
  = _("Certificate self-signed");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CANCELLED)]
  = _("Status is set to offline");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.ENCRYPTION_NOT_AVAILABLE)]
  = _("Encryption is not available");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_INVALID)]
  = _("Certificate is invalid");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CONNECTION_REFUSED)]
  = _("Connection has been refused");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CONNECTION_FAILED)]
  = _("Connection can't be established");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CONNECTION_LOST)]
  = _("Connection has been lost");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.ALREADY_CONNECTED)]
  = _("This resource is already connected to the server");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CONNECTION_REPLACED)]
  = _("Connection has been replaced by a new connection using the same resource");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.REGISTRATION_EXISTS)]
  = _("The account already exists on the server");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.SERVICE_BUSY)]
  = _("Server is currently too busy to handle the connection");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_REVOKED)]
  = _("Certificate has been revoked");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_INSECURE)]
  = _("Certificate uses an insecure cipher algorithm or is cryptographically weak");
_connectionErrorMessages[Tp.error_get_dbus_name(Tp.Error.CERT_LIMIT_EXCEEDED)]
  = _("The length of the server certificate, or the depth of the server certificate chain, exceed the limits imposed by the cryptography library");

AccountNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, account, connectionError) {
        MessageTray.Notification.prototype._init.call(this, source,
            /* translators: argument is the account name, like
             * name@jabber.org for example. */
            _("Connection to %s failed").format(account.get_display_name()),
            null, { customContent: true });

        this._label = new St.Label();
        this.addActor(this._label);
        this._updateMessage(connectionError);

        this._account = account;

        this.addButton('reconnect', _("Reconnect"));
        this.addButton('edit', _("Edit account"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            switch (action) {
            case 'reconnect':
                // If it fails again, a new notification should pop up with the
                // new error.
                account.reconnect_async(null, null);
                break;
            case 'edit':
                let cmd = '/usr/bin/empathy-accounts'
                        + ' --select-account=%s'
                        .format(account.get_path_suffix());
                let app_info = Gio.app_info_create_from_commandline(cmd, null, 0,
                    null);
                app_info.launch([], null, null);
                break;
            }
            this.destroy();
        }));

        this._enabledId = account.connect('notify::enabled',
                                          Lang.bind(this, function() {
                                              if (!account.is_enabled())
                                                  this.destroy();
                                          }));

        this._invalidatedId = account.connect('invalidated',
                                              Lang.bind(this, this.destroy));

        this._connectionStatusId = account.connect('notify::connection-status',
            Lang.bind(this, function() {
                let status = account.connection_status;
                if (status == Tp.ConnectionStatus.CONNECTED) {
                    this.destroy();
                } else if (status == Tp.ConnectionStatus.DISCONNECTED) {
                    this._updateMessage(account.connection_error);
                }
            }));
    },

    /** Updates the message displayed on the notification according to the
     * connection error.
     *
     * We basically look up the error message on {@link _connectionErrorMessages}
     * and use that.
     * @inheritparams AccountNotification
     * @see _connectionErrorMessages
     */
    _updateMessage: function(connectionError) {
        let message;
        if (connectionError in _connectionErrorMessages) {
            message = _connectionErrorMessages[connectionError];
        } else {
            message = _("Unknown reason");
        }
        this._label.set_text(message);
    },

    /** Extends the parent function to destroy this notification.
     * This additionally disconnects the various account signals we were
     * listening to.
     * @override */
    destroy: function() {
        if (this._enabledId != 0) {
            this._account.disconnect(this._enabledId);
            this._enabledId = 0;
        }

        if (this._invalidatedId != 0) {
            this._account.disconnect(this._invalidatedId);
            this._invalidatedId = 0;
        }

        if (this._connectionStatusId != 0) {
            this._account.disconnect(this._connectionStatusId);
            this._connectionStatusId = 0;
        }

        MessageTray.Notification.prototype.destroy.call(this);
    }
};
