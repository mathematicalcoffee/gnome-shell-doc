// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Essentially the 'Windows' tab in the Overview.
 *
 * A {@link WorkspacesView} has many {@link Workspace.Workspace}s (this is
 * the window previous in the overview in the windows tab).
 *
 * A {@link WorkspacesDisplay} is the actual windows tab of the overview,
 * containing one {@link WorkspacesView} in the centre and one
 * {@link WorkspaceThumbnail.ThumbnailsBox} to the right.
 *
 * So: {@link WorkspacesDisplay} has a {@link WorkspaceThumbnails.ThumbnailsBox}
 * and a {@link WorkspacesView}.
 *
 * A {@link WorkspacesThumbnail.ThumbnailsBox} has a {@link WorkspaceThumbnail.WorkspaceThumbnail}
 * per workspace, each of which have {@link WorkspaceThumbnail.WindowClone}s.
 *
 * A {@link WorkspaceView} has a {@link Workspace.Workspace} for each workspace,
 * each of which have {@link Workspace.WindowClone}s and {@link Workspace.WindowOverlay}s.
 * @todo pic with outlines
 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Signals = imports.signals;

const DND = imports.ui.dnd;
const Main = imports.ui.main;
const Overview = imports.ui.overview;
const Tweener = imports.ui.tweener;
const Workspace = imports.ui.workspace;
const WorkspaceThumbnail = imports.ui.workspaceThumbnail;

/** Time taken to switch between {@link Workspace.Workspace}s in the
 * {@link WorkspacesView}.
 *
 * Also the time taken
 * to animate the thumbnails sidebar from collapsed to expanded and vice versa
 * in the {@link WorkspacesDisplay}.
 * @see WorkspacesView#_updateWorkspaceActors
 * @const
 * @default
 * @type {number}
 */
const WORKSPACE_SWITCH_TIME = 0.25;
// Note that mutter has a compile-time limit of 36
// UNUSED
const MAX_WORKSPACES = 16;
// UNUSED
const CONTROLS_POP_IN_TIME = 0.1;


/** creates a new WorkspacesView.
 *
 * We create a top-level actor {@link #actor} which is a St.Group
 * with style class 'workspaces-view'.
 *
 * We make sure it has size 0 since it's not a drop target (our child
 * {@link Workspace.Workspace}s have the drag/drop code in them so they should
 * receive all the events).
 *
 * We then store the input `workspaces` and reparent each `workspace.actor` to
 * our top-level actor, making sure that the {@link Workspace.Workspace} for the
 * currently active workspace is raised to the front.
 *
 * **NOTE**: we do not make a *copy* of `workspaces`; rather, we use the same
 * copy as what was passed in (by {@link WorkspacesDisplay}). So if
 * {@link WorkspacesDisplay} adds some workspaces, we have them too.
 *
 * We *then* create a new {@link Workspace.Workspace} for *each* monitor
 * *other* than the primary one, with a `null` workspace.
 * However we do not add these to our top-level actor, but instead add
 * them to `global.overlay_group`. (From this, I gather that the input `workspaces`
 * should all be for the primary monitor only??) This possibly has something
 * to do with the "workspaces only on primary monitor" setting that GNOME has.
 *
 * We connect to the Overview's 'showing' signal to animate zooming in
 * the workspaces ({@link Workspac.Workspace#zoomToOverview}), and connect
 * to its 'shown' signal to set our clip rectangle so we fit on the screen.
 *
 * We create a `St.Adjustment` and store it in {@link #_scrollAdjustment}
 * to manage "scrolling" between workspaces.
 *
 * We also connect to a bunch of overview dragging signals and the
 * switch-workspace signal.
 *
 * @param {Workspace.Workspace[]} workspaces - array of {@link Workspace.Workspace}s
 * this WorkspacesView is to manage. I *think* that these should all only
 * be for the primary monitor.
 * @classdesc
 * @todo picture
 * A WorkspacesView handles the *display* of multiple {@link Workspace.Workspace}s,
 * handling the animation for scrolling between {@link Workspace.Workspace}s
 * (in particular from a swipe-scroll) and so on.
 *
 * Typically it will have a {@link Workspace.Workspace} per metacity workspace
 * which is constrained to showing workspaces on the primary monitor, plus an
 * additional {@link Workspace} per non-primary monitor (I think this has
 * to do with the "workspaces only on primary monitor" setting GNOME has).
 *
 * However the WorkspacesView is not itself responsible for creating and
 * removing {@link Workspace.Workspace}s (except for the extra one per
 * non-primary monitor); instead the parent {@link WorkspacesDisplay} does that.
 * The way this works is that the workspaces display makes a bunch of
 * {@link Workspace.Workspace}s and stores them in {@link WorkspacesDisplay#_workspaces}, and
 * then passes this to the WorkspacesView constructor *causing
 * {@link #_workspaces} to be the **same array** as {@link WorkspacesDisplay#_workspaces}.
 * Hence when {@link WorkspacesDisplay} adds more workspaces to its
 * {@link WorkspacesDisplay#_workspaces}, they are added to the workspaces *view* too.
 *
 * The WorkspacesView, along with a {@link WorkspaceThumbnails.ThumbnailsBox},
 * makes up the {@link WorkspacesDisplay} (windows tab of the overview).
 *
 * @todo pic of th filmstrip
 * @class
 */
function WorkspacesView(workspaces) {
    this._init(workspaces);
}

WorkspacesView.prototype = {
    _init: function(workspaces) {
        /** Top-level actor, contains all the
         * child {@link Workspace}s. Style class 'workspaces-view'.
         * @type {St.Group} */
        this.actor = new St.Group({ style_class: 'workspaces-view' });

        // The actor itself isn't a drop target, so we don't want to pick on its area
        this.actor.set_size(0, 0);

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        this.actor.connect('style-changed', Lang.bind(this,
            function() {
                let node = this.actor.get_theme_node();
                this._spacing = node.get_length('spacing');
                this._updateWorkspaceActors(false);
            }));
        this.actor.connect('notify::mapped',
                           Lang.bind(this, this._onMappedChanged));

        this._width = 0;
        this._height = 0;
        this._x = 0;
        this._y = 0;
        this._clipX = 0;
        this._clipY = 0;
        this._clipWidth = 0;
        this._clipHeight = 0;
        this._workspaceRatioSpacing = 0;
        this._spacing = 0;
        this._animating = false; // tweening
        this._scrolling = false; // swipe-scrolling
        this._animatingScroll = false; // programatically updating the adjustment
        this._zoomOut = false; // zoom to a larger area
        this._inDrag = false; // dragging a window

        let activeWorkspaceIndex = global.screen.get_active_workspace_index();
        /** The {@link Workspace.Workspace}s we are managing, one per
         * actual workspace, and each for the primary monitor.
         *
         * Note that this is **the same** as the input `workspaces` array,
         * not a copy.
         * @type {Workpsace.Workspace[]} */
        this._workspaces = workspaces;

        // Add workspace actors
        for (let w = 0; w < global.screen.n_workspaces; w++)
            this._workspaces[w].actor.reparent(this.actor);
        this._workspaces[activeWorkspaceIndex].actor.raise_top();

        /** Extra {@link Workspace.Workspace}s we are managing in addition to
         * {@link #_workspaces}, one per monitor that
         * is *not* the primary one (!), with no particular workspace.
         * @type {Workpsace.Workspace[]} */
        this._extraWorkspaces = [];
        let monitors = Main.layoutManager.monitors;
        let m = 0;
        for (let i = 0; i < monitors.length; i++) {
            if (i == Main.layoutManager.primaryIndex)
                continue;
            let ws = new Workspace.Workspace(null, i);
            this._extraWorkspaces[m++] = ws;
            ws.setGeometry(monitors[i].x, monitors[i].y, monitors[i].width, monitors[i].height);
            global.overlay_group.add_actor(ws.actor);
        }

        // Position/scale the desktop windows and their children after the
        // workspaces have been created. This cannot be done first because
        // window movement depends on the Workspaces object being accessible
        // as an Overview member.
        this._overviewShowingId =
            Main.overview.connect('showing',
                                 Lang.bind(this, function() {
                for (let w = 0; w < this._workspaces.length; w++)
                    this._workspaces[w].zoomToOverview();
                for (let w = 0; w < this._extraWorkspaces.length; w++)
                    this._extraWorkspaces[w].zoomToOverview();
        }));
        this._overviewShownId =
            Main.overview.connect('shown',
                                 Lang.bind(this, function() {
                this.actor.set_clip(this._clipX, this._clipY,
                                    this._clipWidth, this._clipHeight);
        }));

        /** An adjustment used for scrolling between {@link Workspace}s (think of the
         * {@link Workspace}s for each metacity workspace as "pages" and we want
         * to flip through them?)
         * Page increment 1, step increment 0, page size 1, from 0 to
         * the {@link #_workspaces}.length.
         * @see #_onScroll
         * @type {St.Adjustment} */
        this._scrollAdjustment = new St.Adjustment({ value: activeWorkspaceIndex,
                                                     lower: 0,
                                                     page_increment: 1,
                                                     page_size: 1,
                                                     step_increment: 0,
                                                     upper: this._workspaces.length });
        this._scrollAdjustment.connect('notify::value',
                                       Lang.bind(this, this._onScroll));

        this._switchWorkspaceNotifyId =
            global.window_manager.connect('switch-workspace',
                                          Lang.bind(this, this._activeWorkspaceChanged));

        this._itemDragBeginId = Main.overview.connect('item-drag-begin',
                                                      Lang.bind(this, this._dragBegin));
        this._itemDragEndId = Main.overview.connect('item-drag-end',
                                                     Lang.bind(this, this._dragEnd));
        this._windowDragBeginId = Main.overview.connect('window-drag-begin',
                                                        Lang.bind(this, this._dragBegin));
        this._windowDragEndId = Main.overview.connect('window-drag-end',
                                                      Lang.bind(this, this._dragEnd));
        this._swipeScrollBeginId = 0;
        this._swipeScrollEndId = 0;
    },

    /** Sets the geometry of each {@link Workspace.Workspace} in
     * {@link #_workspaces} (i.e. the ones tied to the primary
     * monitor and an actual metacity workspace as opposed to the ones
     * tied to the other monitors in {@link WorkspacessView#_extraWorkspaces}).
     * 
     * This is used to position each workspace on the screen.
     *
     * Used by {@link WorkspacesDisplay#_updateWorkspacesGeometry} to
     * position the workspaces view in the windows tab of the overview.
     * @inheritparams Workspace.Workspace#setGeometry
     * @see Workspace.Workspace#setGeometry
     * @param {number} spacing - ?? (I think it's the difference between the
     * full allocation's height and this workspace view's height. For example
     * if the available space is not of the same aspect ratio as us, we just
     * fit ourselves in as best we can, and there may be some gaps either
     * vertically or horizontally. We store the vertical gap).
     */
    setGeometry: function(x, y, width, height, spacing) {
      if (this._x == x && this._y == y &&
          this._width == width && this._height == height)
          return;
        this._width = width;
        this._height = height;
        this._x = x;
        this._y = y;
        this._workspaceRatioSpacing = spacing;

        for (let i = 0; i < this._workspaces.length; i++)
            this._workspaces[i].setGeometry(x, y, width, height);
    },

    /** Sets the clip rectangle for ourselves.
     * @param {number} x - x coordinate
     * @param {number} y - y coordinate
     * @param {number} width - width
     * @param {number} height - height
     */
    setClipRect: function(x, y, width, height) {
        this._clipX = x;
        this._clipY = y;
        this._clipWidth = width;
        this._clipHeight = height;
    },

    /** Looks up the {@link Workspace} that contains a particular metacity
     * window (only in {@link #_workspaces}, not
     * {@link #_extraWorkspaces}).
     * @param {Meta.Window} metaWindow - workspace to look up.
     * @returns {?Workspace.Workspace} the {@link Workspace.Workspace} containing
     * `metaWindow`, or `null` if not found.
     */
    _lookupWorkspaceForMetaWindow: function (metaWindow) {
        for (let i = 0; i < this._workspaces.length; i++) {
            if (this._workspaces[i].containsMetaWindow(metaWindow))
                return this._workspaces[i];
        }
        return null;
    },

    /** Gets the {@link Workspace.Workspace} for the current active
     * workspace.
     * (only in {@link #_workspaces}, not {@link #_extraWorkspaces}).
     * @returns {Workspace.Workspace} the corresponding {@link Workspace.Workspace}.
     */
    getActiveWorkspace: function() {
        let active = global.screen.get_active_workspace_index();
        return this._workspaces[active];
    },

    /** Gets the {@link Workspace.Workspace} at index `index`.
     * (only in {@link #_workspaces}, not {@link #_extraWorkspaces}).
     * @param {number} index - inex to look up.
     * @returns {Workspace.Workspace} the corresponding {@link Workspace.Workspace}.
     */
    getWorkspaceByIndex: function(index) {
        return this._workspaces[index];
    },

    /** Hides the WorkspacesView and all its child {@link Workspace.Workspace}s,
     * i.e. animating out from the overview.
     * @see Workspace.Workspace#zoomFromOverview
     */
    hide: function() {
        let activeWorkspaceIndex = global.screen.get_active_workspace_index();
        let activeWorkspace = this._workspaces[activeWorkspaceIndex];

        activeWorkspace.actor.raise_top();

       this.actor.remove_clip(this._x, this._y, this._width, this._height);

        for (let w = 0; w < this._workspaces.length; w++)
            this._workspaces[w].zoomFromOverview();
        for (let w = 0; w < this._extraWorkspaces.length; w++)
            this._extraWorkspaces[w].zoomFromOverview();
    },

    /** Destroys the top-level actor {@link #actor}, triggering
     * {@link #_onDestroy} */
    destroy: function() {
        this.actor.destroy();
    },

    /** Synchronises the stacking of all the windows on all the child
     * {@link Workspace}s to reflect the current window stackin.
     * @inheritparams Workspace.Workspace#syncStacking
     * @see Workspace.Workspace#syncStacking
     */
    syncStacking: function(stackIndices) {
        for (let i = 0; i < this._workspaces.length; i++)
            this._workspaces[i].syncStacking(stackIndices);
        for (let i = 0; i < this._extraWorkspaces.length; i++)
            this._extraWorkspaces[i].syncStacking(stackIndices);
    },

    /** Updates the positioning of the windows on each workspace in
     * {@link #_workspaces}, animatin them.
     * @see Workspace.Workspace#positionWindows
     */
    updateWindowPositions: function() {
        for (let w = 0; w < this._workspaces.length; w++)
            this._workspaces[w].positionWindows(Workspace.WindowPositionFlags.ANIMATE);
    },

    /** Scrolls to show the {@link Workspace.Workspace} for the currently-active
     * workspace.
     * @param {boolean} showAnimation - whether to animate the scrolling
     * @see #_updateWorkspaceActors
     * @see #_updateScrollAdjustment
     */
    _scrollToActive: function(showAnimation) {
        let active = global.screen.get_active_workspace_index();

        this._updateWorkspaceActors(showAnimation);
        this._updateScrollAdjustment(active, showAnimation);
    },

    /** Shows the {@link Workspace.Workspace} for the current active workspace,
     * potentially animating it too.
     * (only for {@link #_workspaces}, not {@link #_extraWorkspaces}).
     *
     * This:
     *
     * * Removes all existing tweens from the {@link Workspace.Workspace}s
     * * tweens the Y position of each {@link Workspace.Workspace} to vertically
     * scroll it to wherever it is supposed to be (even if this is off screen).
     *
     * (think of all of our {@link #_workspaces} as a vertically
     * laid out film strip of {@link Workspace.Workspace}s and we just scroll
     * it up and down to show the one we're interested in).
     * @param {boolean} showAnimation - whether to animate everything.
     */
    _updateWorkspaceActors: function(showAnimation) {
        let active = global.screen.get_active_workspace_index();

        this._animating = showAnimation;

        for (let w = 0; w < this._workspaces.length; w++) {
            let workspace = this._workspaces[w];

            Tweener.removeTweens(workspace.actor);

            let y = (w - active) * (this._height + this._spacing + this._workspaceRatioSpacing);

            if (showAnimation) {
                let params = { y: y,
                               time: WORKSPACE_SWITCH_TIME,
                               transition: 'easeOutQuad'
                             };
                // we have to call _updateVisibility() once before the
                // animation and once afterwards - it does not really
                // matter which tween we use, so we pick the first one ...
                if (w == 0) {
                    this._updateVisibility();
                    params.onComplete = Lang.bind(this,
                        function() {
                            this._animating = false;
                            this._updateVisibility();
                        });
                }
                Tweener.addTween(workspace.actor, params);
            } else {
                workspace.actor.set_position(0, y);
                if (w == 0)
                    this._updateVisibility();
            }
        }
    },

    /** Updates each {@link Workspace.Workspace}'s visibility according to
     * whether it is showing.
     *
     * If we are currently scrolling between workspaces, all workspaces and
     * their window overlays are
     * set to visible (because we can simultaneously have two {@link Workspace.Workspace}s
     * on the screen at one while we are scrolling).
     *
     * Otherwise we only set to visible the active {@link Workspace.Workspace}.
     * @see Workspace.Workspace#hideWindowsOverlays
     * @see Workspace.Workspace#showWindowsOverlays
     */
    _updateVisibility: function() {
        let active = global.screen.get_active_workspace_index();

        for (let w = 0; w < this._workspaces.length; w++) {
            let workspace = this._workspaces[w];
            if (this._animating || this._scrolling) {
                workspace.hideWindowsOverlays();
                workspace.actor.show();
            } else {
                workspace.showWindowsOverlays();
                if (this._inDrag)
                    workspace.actor.visible = (Math.abs(w - active) <= 1);
                else
                    workspace.actor.visible = (w == active);
            }
        }
    },

    /** Updates {@link #_scrollAdjustment}, possibly animating it,
     * in response to us scrolling between {@link Workspace.Workspace}s.
     * @param {number} index - index of Workspace to scroll to.
     * @inheritparams #_scrollToActive
     */
    _updateScrollAdjustment: function(index, showAnimation) {
        if (this._scrolling)
            return;

        this._animatingScroll = true;

        if (showAnimation) {
            Tweener.addTween(this._scrollAdjustment, {
               value: index,
               time: WORKSPACE_SWITCH_TIME,
               transition: 'easeOutQuad',
               onComplete: Lang.bind(this,
                   function() {
                       this._animatingScroll = false;
                   })
            });
        } else {
            this._scrollAdjustment.value = index;
            this._animatingScroll = false;
        }
    },

    /** Call this to update this WorkspaceView's {@link Workspace.Workspace}s when the number
     * of workspaces changes (i.e. one is added or removed).
     *
     * We do not actually make new {@link Workspace.Workspace}s or remove old
     * ones to update the changes ourselves, because that is done
     * in {@link WorkspacesDisplay#_workspacesChanged} (it updates
     * {@link WorkspacesDisplay#_workspaces} which is the same instance as our
     * {@link #_workspaces}, so we can expect the new workspaces
     * to be there already/the old ones to be gone).
     *
     * However we update our {@link #_scrollAdjustment}, animating it.
     *
     * If the new number of workspaces is more than the old, we
     * call {@link Workspace.Workspace#setGeometry} for the newly added workspaces
     * adding their actors to our {@link #actor}, and
     * call {@link #_updateWorkspaceActors}.
     *
     * Finally regardless of the new/old number of workspaces, we scroll to the
     * active workspace, animating it.
     *
     * Called from {@link WorkspacesDisplay#_workspacesChanged}.
     * @param {number} oldNumWorkspaces - the old number of workspaces
     * @param {number} newNumWorkspaces - the new number of workspaces
     */
    updateWorkspaces: function(oldNumWorkspaces, newNumWorkspaces) {
        let active = global.screen.get_active_workspace_index();

        Tweener.addTween(this._scrollAdjustment,
                         { upper: newNumWorkspaces,
                           time: WORKSPACE_SWITCH_TIME,
                           transition: 'easeOutQuad'
                         });

        if (newNumWorkspaces > oldNumWorkspaces) {
            for (let w = oldNumWorkspaces; w < newNumWorkspaces; w++) {
                this._workspaces[w].setGeometry(this._x, this._y,
                                                this._width, this._height);
                this.actor.add_actor(this._workspaces[w].actor);
            }

            this._updateWorkspaceActors(false);
        }

        this._scrollToActive(true);
    },

    /** Callback when the active workspace changes.
     * If we are not already scrolling, we scroll to the active workspace.
     * @see #_scrollToActive
     */
    _activeWorkspaceChanged: function(wm, from, to, direction) {
        if (this._scrolling)
            return;

        this._scrollToActive(true);
    },

    /** Callback when {@link #actor} is destroyed, for example
     * as a result of calling {@link #destroy}.
     *
     * We destroy all our extra workspaces in {@link #_extraWorkspaces},
     * disconnect our outstanding signals, cancel any drags in process, and
     * destroy our scroll adjustment.
     */
    _onDestroy: function() {
        for (let i = 0; i < this._extraWorkspaces.length; i++)
            this._extraWorkspaces[i].destroy();
        this._scrollAdjustment.run_dispose();
        Main.overview.disconnect(this._overviewShowingId);
        Main.overview.disconnect(this._overviewShownId);
        global.window_manager.disconnect(this._switchWorkspaceNotifyId);

        if (this._inDrag)
            this._dragEnd();

        if (this._itemDragBeginId > 0) {
            Main.overview.disconnect(this._itemDragBeginId);
            this._itemDragBeginId = 0;
        }
        if (this._itemDragEndId > 0) {
            Main.overview.disconnect(this._itemDragEndId);
            this._itemDragEndId = 0;
        }
        if (this._windowDragBeginId > 0) {
            Main.overview.disconnect(this._windowDragBeginId);
            this._windowDragBeginId = 0;
        }
        if (this._windowDragEndId > 0) {
            Main.overview.disconnect(this._windowDragEndId);
            this._windowDragEndId = 0;
        }
    },

    /** Callback when our toplevel actor is mapped or unmapped.
     *
     * If we are mapped, we call {@link Overview.Overview#setScrollAdjustment}
     * with a vertical direction, and connect to its swipe-scroll-begin
     * and swipe-scroll-end signals.
     *
     * If we have been unmapped, we disconnect these signals.
     */
    _onMappedChanged: function() {
        if (this.actor.mapped) {
            let direction = Overview.SwipeScrollDirection.VERTICAL;
            Main.overview.setScrollAdjustment(this._scrollAdjustment,
                                              direction);
            this._swipeScrollBeginId = Main.overview.connect('swipe-scroll-begin',
                                                             Lang.bind(this, this._swipeScrollBegin));
            this._swipeScrollEndId = Main.overview.connect('swipe-scroll-end',
                                                           Lang.bind(this, this._swipeScrollEnd));
        } else {
            Main.overview.disconnect(this._swipeScrollBeginId);
            Main.overview.disconnect(this._swipeScrollEndId);
        }
    },

    /** Callback for the overview's item-drag-begin and window-drag-begin signals.
     *
     * We just use this to detect the user dragging something between
     * our {@link Workspace.Workspace}s.
     *
     * @see #_onDragMotion
     * @see Overview.Overview.window-drag-begin
     * @see Overview.Overview.item-drag-begin */
    _dragBegin: function() {
        if (this._scrolling)
            return;

        this._inDrag = true;
        this._firstDragMotion = true;

        this._dragMonitor = {
            dragMotion: Lang.bind(this, this._onDragMotion)
        };
        DND.addDragMonitor(this._dragMonitor);
    },

    /** This is our drag monitor for items and windows being dragged in
     * the overview.
     *
     * We use this to detect the user dragging something between
     * our {@link Workspace.Workspace}s and if it's a {@link Workspace.WindowClone}
     * we use {@link Workspace.Workspace#setReservedSlot} to make a space for
     * it on the other workspace.
     * @returns {DND.DragMotionResult} {@link DND.DragMotionResult.CONTINUE}
     * always (the child
     * {@link Workspace.Workspace}s have the DND interface implemented so
     * they will manage the rest).
     * @see Workspace.Workspace#setReservedSlot
     */
    _onDragMotion: function(dragEvent) {
        if (Main.overview.animationInProgress)
             return DND.DragMotionResult.CONTINUE;

        if (this._firstDragMotion) {
            this._firstDragMotion = false;
            for (let i = 0; i < this._workspaces.length; i++)
                this._workspaces[i].setReservedSlot(dragEvent.dragActor._delegate);
            for (let i = 0; i < this._extraWorkspaces.length; i++)
                this._extraWorkspaces[i].setReservedSlot(dragEvent.dragActor._delegate);
        }

        return DND.DragMotionResult.CONTINUE;
    },

    /** Callback for the overview's item-drag-end and window-drag-end signals.
     * 
     * We remove our drag monitor and remove any reserved slots from our
     * {@link Workspace.Workspace}s.
     * @see #_onDragMotion
     * @see Overview.Overview.window-drag-end
     * @see Overview.Overview.item-drag-end */
    _dragEnd: function() {
        DND.removeDragMonitor(this._dragMonitor);
        this._inDrag = false;

        for (let i = 0; i < this._workspaces.length; i++)
            this._workspaces[i].setReservedSlot(null);
        for (let i = 0; i < this._extraWorkspaces.length; i++)
            this._extraWorkspaces[i].setReservedSlot(null);
    },

    /** Callback for the overview's swipe-scroll-begin signal. We just
     * note internally that we are scrolling.
     * @see Overview.Overview.swipe-scroll-begin
     * @see #_swipeScrollEnd
     */
    _swipeScrollBegin: function() {
        this._scrolling = true;
    },

    /** Callback for the overview's swipe-scroll-end signal.
     *
     * If `result` indicates that the swipe-scroll was in fact a click and
     * the user clicked on an empty {@link Workspace.Workspace}, we switch
     * to that workspace.
     *
     * Otherwise if they clicked on a {@link Workspace.Workspace} but it *wasn't*
     * empty, we do nothing (because it'd be too easy to try to click a
     * {@link Workspace.WindowClone} and miss causing the workspace to activate
     * when you don't want it to).
     *
     * @inheritparams Overview.Overview.swipe-scroll-end
     * @see Overview.Overview.swipe-scroll-end
     */
    _swipeScrollEnd: function(overview, result) {
        this._scrolling = false;

        if (result == Overview.SwipeScrollResult.CLICK) {
            let [x, y, mod] = global.get_pointer();
            let actor = global.stage.get_actor_at_pos(Clutter.PickMode.ALL,
                                                      x, y);

            // Only switch to the workspace when there's no application
            // windows open. The problem is that it's too easy to miss
            // an app window and get the wrong one focused.
            let active = global.screen.get_active_workspace_index();
            if (this._workspaces[active].isEmpty() &&
                this.actor.contains(actor))
                Main.overview.hide();
        }

        // Make sure title captions etc are shown as necessary
        this._updateVisibility();
    },

    /** Callback when {@link #_scrollAdjustment}'s value changes.
     *
     * Sync the workspaces' positions to the value of the scroll adjustment
     * and change the active workspace if appropriate
     */
    _onScroll: function(adj) {
        if (this._animatingScroll)
            return;

        let active = global.screen.get_active_workspace_index();
        let current = Math.round(adj.value);

        if (active != current) {
            let metaWorkspace = this._workspaces[current].metaWorkspace;
            metaWorkspace.activate(global.get_current_time());
        }

        let last = this._workspaces.length - 1;
        let firstWorkspaceY = this._workspaces[0].actor.y;
        let lastWorkspaceY = this._workspaces[last].actor.y;
        let workspacesHeight = lastWorkspaceY - firstWorkspaceY;

        if (adj.upper == 1)
            return;

        let currentY = firstWorkspaceY;
        let newY =  - adj.value / (adj.upper - 1) * workspacesHeight;

        let dy = newY - currentY;

        for (let i = 0; i < this._workspaces.length; i++) {
            this._workspaces[i].hideWindowsOverlays();
            this._workspaces[i].actor.visible = Math.abs(i - adj.value) <= 1;
            this._workspaces[i].actor.y += dy;
        }
    },

    /** Gets the current active workspace index..
     * @returns {number} the active workspace index.
     */
    _getWorkspaceIndexToRemove: function() {
        return global.screen.get_active_workspace_index();
    }
};
Signals.addSignalMethods(WorkspacesView.prototype);


/** creates a new WorkspacesDisplay
 * We create a top-level actor {@link #actor} which is a
 * Shell.GenericContainer.
 *
 * Then we make a container for the thumbnails box, {@link #_controls}.
 * Into this we place a {@link WorkspaceThumbnails.ThumbnailsBox}, stored in
 * {@link #_thumbnailsBox}.
 *
 * We initialise a bunch of variables and work out whether our thumbnails sidebar should
 * initially be collapsed to the side, or fully expanded.
 *
 * @classdesc
 * The WorkspacesDisplay is the "Windows" tab of the Overview.
 *
 * It contains a thumbnails sidebar to the right, a {@link WorkspaceThumbnail.ThumbnailsBox}.
 *
 * In the rest of the space is a {@link WorkspacesView} which has many
 * {@link Workspace}s, one per workspace and one per non-primary monitor.
 * The {@link WorkspacesView} just shows the {@link Workspace}
 * current workspace, which contains windows thumbnails for windows on the
 * current workspace along with window captions for each.
 * @class
 * @todo pictures
 */
function WorkspacesDisplay() {
    this._init();
}

WorkspacesDisplay.prototype = {
    _init: function() {
        /** Top-level actor is a Shell.GenericContainer, we allocate the child
         * thumbanails box and workspaces view their space in
         * {@link #_allocate}.
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer();
        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));
        this.actor.set_clip_to_allocation(true);

        let controls = new St.Bin({ style_class: 'workspace-controls',
                                    request_mode: Clutter.RequestMode.WIDTH_FOR_HEIGHT,
                                    y_align: St.Align.START,
                                    y_fill: true });
        /** The container for the thumbnails box {@link #_thumbnailsBox}.
         * Style class 'workspace-controls', fills the y direction.
         * @type {St.Bin}
         */
        this._controls = controls;
        this.actor.add_actor(controls);

        controls.reactive = true;
        controls.track_hover = true;
        controls.connect('notify::hover',
                         Lang.bind(this, this._onControlsHoverChanged));
        controls.connect('scroll-event',
                         Lang.bind(this, this._onScrollEvent));

        this._monitorIndex = Main.layoutManager.primaryIndex;

        /** The container for the thumbnails box {@link #_thumbnailsBox}.
         * @type {WorkspaceThumnbails.ThumbnailsBox}
         */
        this._thumbnailsBox = new WorkspaceThumbnail.ThumbnailsBox();
        controls.add_actor(this._thumbnailsBox.actor);

        /** Our {@link WorkspacesView}. It is destroyed every time the
         * workspaces display is hidden, and created fresh every time the
         * workspaces display is shown.
         * @type {?WorkspacesView.WorkspacesView} */
        this.workspacesView = null;

        this._inDrag = false;
        this._cancelledDrag = false;

        this._alwaysZoomOut = false;
        this._zoomOut = false;
        this._zoomFraction = 0;

        this._updateAlwaysZoom();

        Main.layoutManager.connect('monitors-changed', Lang.bind(this, this._updateAlwaysZoom));

        Main.xdndHandler.connect('drag-begin', Lang.bind(this, function(){
            this._alwaysZoomOut = true;
        }));

        Main.xdndHandler.connect('drag-end', Lang.bind(this, function(){
            this._alwaysZoomOut = false;
            this._updateAlwaysZoom();
        }));

        this._switchWorkspaceNotifyId = 0;

        this._nWorkspacesChangedId = 0;
        this._itemDragBeginId = 0;
        this._itemDragCancelledId = 0;
        this._itemDragEndId = 0;
        this._windowDragBeginId = 0;
        this._windowDragCancelledId = 0;
        this._windowDragEndId = 0;
    },

    /** Shows the workspaces display.
     *
     * This shows the thumbnails box.
     *
     * We also create a new {@link Workspace.Workspace} for each workspace
     * and for the *primary* monitor, storing them in {@link #_workspaces}.
     *
     * We destroy our {@link #workspacesView} if it exists and create a new
     * one with {@link #_workspaces} (so when we update {@link #_workspaces},
     * `this.workspacesView._workspaces` updates too).
     *
     * We update the geometry of our workspaces ({@link #_updateWorkspacesGeometry})
     * and connect to the overview dragging signals and 'notify::n-workspaces' signals.
     *
     * Finally we make sure the stacking order of all the windows in our thumbnails box
     * and workspaces view is correct ({@link #_onRestacked}).
     */
    show: function() {
        this._zoomOut = this._alwaysZoomOut;
        this._zoomFraction = this._alwaysZoomOut ? 1 : 0;
        this._updateZoom();

        this._controls.show();
        this._thumbnailsBox.show();

        /** An array of {@link Workspace.Workspace}s, one per
         * actual workspace, and each for the primary monitor.
         *
         * This is passed in to the {@link WorkspacesView} and is the *same*
         * array as {@link WorkspacesView#_workspaces}, so when we modify this,
         * that is modified too.
         * @type {Workpsace.Workspace[]} */
        this._workspaces = [];
        for (let i = 0; i < global.screen.n_workspaces; i++) {
            let metaWorkspace = global.screen.get_workspace_by_index(i);
            this._workspaces[i] = new Workspace.Workspace(metaWorkspace, this._monitorIndex);
        }

        if (this.workspacesView)
            this.workspacesView.destroy();
        this.workspacesView = new WorkspacesView(this._workspaces);
        this._updateWorkspacesGeometry();

        this._restackedNotifyId =
            global.screen.connect('restacked',
                                  Lang.bind(this, this._onRestacked));

        if (this._nWorkspacesChangedId == 0)
            this._nWorkspacesChangedId = global.screen.connect('notify::n-workspaces',
                                                               Lang.bind(this, this._workspacesChanged));
        if (this._itemDragBeginId == 0)
            this._itemDragBeginId = Main.overview.connect('item-drag-begin',
                                                          Lang.bind(this, this._dragBegin));
        if (this._itemDragCancelledId == 0)
            this._itemDragCancelledId = Main.overview.connect('item-drag-cancelled',
                                                              Lang.bind(this, this._dragCancelled));
        if (this._itemDragEndId == 0)
            this._itemDragEndId = Main.overview.connect('item-drag-end',
                                                        Lang.bind(this, this._dragEnd));
        if (this._windowDragBeginId == 0)
            this._windowDragBeginId = Main.overview.connect('window-drag-begin',
                                                            Lang.bind(this, this._dragBegin));
        if (this._windowDragCancelledId == 0)
            this._windowDragCancelledId = Main.overview.connect('window-drag-cancelled',
                                                            Lang.bind(this, this._dragCancelled));
        if (this._windowDragEndId == 0)
            this._windowDragEndId = Main.overview.connect('window-drag-end',
                                                          Lang.bind(this, this._dragEnd));

        this._onRestacked();
    },

    /** Hides the workspaces display.
     *
     * We disconnect any signals we were listening to on {@link #show}.
     * We hide our thumbnails box and destroy our workspaces view
     * ({@link WorkspacesView#destroy}).
     * We then destroy all the workspaces in {@link #_workspaces} (see
     * {@link Workspace.Workspace#destroy}).
     */
    hide: function() {
        this._controls.hide();
        this._thumbnailsBox.hide();

        if (this._restackedNotifyId > 0){
            global.screen.disconnect(this._restackedNotifyId);
            this._restackedNotifyId = 0;
        }
        if (this._itemDragBeginId > 0) {
            Main.overview.disconnect(this._itemDragBeginId);
            this._itemDragBeginId = 0;
        }
        if (this._itemDragCancelledId > 0) {
            Main.overview.disconnect(this._itemDragCancelledId);
            this._itemDragCancelledId = 0;
        }
        if (this._itemDragEndId > 0) {
            Main.overview.disconnect(this._itemDragEndId);
            this._itemDragEndId = 0;
        }
        if (this._windowDragBeginId > 0) {
            Main.overview.disconnect(this._windowDragBeginId);
            this._windowDragBeginId = 0;
        }
        if (this._windowDragCancelledId > 0) {
            Main.overview.disconnect(this._windowDragCancelledId);
            this._windowDragCancelledId = 0;
        }
        if (this._windowDragEndId > 0) {
            Main.overview.disconnect(this._windowDragEndId);
            this._windowDragEndId = 0;
        }

        this.workspacesView.destroy();
        this.workspacesView = null;
        for (let w = 0; w < this._workspaces.length; w++) {
            this._workspaces[w].disconnectAll();
            this._workspaces[w].destroy();
        }
    },

    /** The zoom fraction is how far out the thumbnails box is slid out (1 is
     * fully expanded, 0 is slid in so that just a little bit peeks out).
     * 
     * Setting this property will update the layout straight away.
     * @type {number} */
    // zoomFraction property allows us to tween the controls sliding in and out
    set zoomFraction(fraction) {
        this._zoomFraction = fraction;
        this.actor.queue_relayout();
    },

    get zoomFraction() {
        return this._zoomFraction;
    },

    /** This sets whether the thumbnails box should always be slidden out.
     *
     * Called upon construction and the 'monitors-changed' signal of the
     * layout manager (whenever monitors get added/removed/moved around).
     *
     * The thumbnails box should always be slidden out if:
     *
     * * there are more than two workspaces; or
     * * there is another monitor to the right of the primary one (otherwise
     * if it's collapsed it's hard to hover over the sidebar to slide it out
     * without going into the next monitor).
     */
    _updateAlwaysZoom: function()  {
        // Always show the pager if workspaces are actually used,
        // e.g. there are windows on more than one
        this._alwaysZoomOut = global.screen.n_workspaces > 2;

        if (this._alwaysZoomOut)
            return;

        let monitors = Main.layoutManager.monitors;
        let primary = Main.layoutManager.primaryMonitor;

        /* Look for any monitor to the right of the primary, if there is
         * one, we always keep zoom out, otherwise its hard to reach
         * the thumbnail area without passing into the next monitor. */
        for (let i = 0; i < monitors.length; i++) {
            if (monitors[i].x >= primary.x + primary.width) {
                this._alwaysZoomOut = true;
                break;
            }
        }
    },

    /** Callback for the 'get-preferred-width' of the top-level actor
     * {@link #actor}.
     * This just returns the width requested by the container for the
     * thumbnails box {@link #_controls} (!!)
     */
    _getPreferredWidth: function (actor, forHeight, alloc) {
        // pass through the call in case the child needs it, but report 0x0
        this._controls.get_preferred_width(forHeight);
    },

    /** Callback for the 'get-preferred-height' of the top-level actor
     * {@link #actor}.
     * This returns the height requested by the container for the
     * thumbnails box {@link #_controls}.
     */
    _getPreferredHeight: function (actor, forWidth, alloc) {
        // pass through the call in case the child needs it, but report 0x0
        this._controls.get_preferred_height(forWidth);
    },

    /** Callback for the 'allocate' of the top-level actor
     * {@link #actor}.
     *
     * We allocate the thumbnails box/its container {@link #_controls}
     * its preferred width for the available height.
     * We position it respecting the current {@link #zoomFraction} (so that
     * if it's currently slidden in/collapsed we only show it peeking out a little
     * bit).
     *
     * We then call {@link #_updateWorkspacesGeometry}, to position and size
     * the workspaces view, i.e. we don't do
     * any allocation for {@link #workspacesView} here.
     */
    _allocate: function (actor, box, flags) {
        let childBox = new Clutter.ActorBox();

        let totalWidth = box.x2 - box.x1;

        // width of the controls
        let [controlsMin, controlsNatural] = this._controls.get_preferred_width(box.y2 - box.y1);

        // Amount of space on the screen we reserve for the visible control
        let controlsVisible = this._controls.get_theme_node().get_length('visible-width');
        let controlsReserved = controlsVisible * (1 - this._zoomFraction) + controlsNatural * this._zoomFraction;

        let rtl = (St.Widget.get_default_direction () == St.TextDirection.RTL);
        if (rtl) {
            childBox.x2 = controlsReserved;
            childBox.x1 = childBox.x2 - controlsNatural;
        } else {
            childBox.x1 = totalWidth - controlsReserved;
            childBox.x2 = childBox.x1 + controlsNatural;
        }

        childBox.y1 = 0;
        childBox.y2 = box.y2- box.y1;
        this._controls.allocate(childBox, flags);

        this._updateWorkspacesGeometry();
    },

    /** Updates the space/position for {@link #workspacesView}.
     *
     * We basically take up all the space that is available to us, leaving
     * space for the thumbnails sidebar.
     *
     * @see WorkspacesView#setGeometry
     */
    _updateWorkspacesGeometry: function() {
        if (!this.workspacesView)
            return;

        let fullWidth = this.actor.allocation.x2 - this.actor.allocation.x1;
        let fullHeight = this.actor.allocation.y2 - this.actor.allocation.y1;

        let width = fullWidth;
        let height = fullHeight;

        let [controlsMin, controlsNatural] = this._controls.get_preferred_width(height);
        let controlsVisible = this._controls.get_theme_node().get_length('visible-width');

        let [x, y] = this.actor.get_transformed_position();

        let rtl = (St.Widget.get_default_direction () == St.TextDirection.RTL);

        let clipWidth = width - controlsVisible;
        let clipHeight = (fullHeight / fullWidth) * clipWidth;
        let clipX = rtl ? x + controlsVisible : x;
        let clipY = y + (fullHeight - clipHeight) / 2;

        this.workspacesView.setClipRect(clipX, clipY, clipWidth, clipHeight);

        if (this._zoomOut) {
            width -= controlsNatural;
            if (rtl)
                x += controlsNatural;
        } else {
            width -= controlsVisible;
            if (rtl)
                x += controlsVisible;
        }

        height = (fullHeight / fullWidth) * width;
        let difference = fullHeight - height;
        y += difference / 2;

        this.workspacesView.setGeometry(x, y, width, height, difference);
    },

    /** Callback when the stacking order of any windows changes.
     * This calls {@link WorkspacesView#syncStacking} and
     * {@link WorkspaceThumbnails.ThumbnailsBox#syncStacking} to make sure
     * the window clones reflect this change.
     * @see WorkspacesView#syncStacking
     * @see WorkspaceThumbnails.ThumbnailsBox#syncStacking
     */
    _onRestacked: function() {
        let stack = global.get_window_actors();
        let stackIndices = {};

        for (let i = 0; i < stack.length; i++) {
            // Use the stable sequence for an integer to use as a hash key
            stackIndices[stack[i].get_meta_window().get_stable_sequence()] = i;
        }

        this.workspacesView.syncStacking(stackIndices);
        this._thumbnailsBox.syncStacking(stackIndices);
    },

    /** Called when the number of workspaces changes.
     *
     * We update whether the thumbnails sidebar should be slidden out or not
     * ({@link #_updateAlwaysZoom}).
     *
     * If workspaces have been added, we create new {@link Workspace.Workspace}s
     * for them, adding them to the end of {@link #_workspaces}.
     * We adding new workspace thumbnails for the new workspaces with
     * {@link WorkspaceThumbnails.ThumbnailsBox#addThumbnails} (this handles
     * the animation sliding in the new workspace thumbnails).
     *
     * Otherwise if workspaces were removed, we delete them from
     * {@link #_workspaces} (from the back), disconnecting all their signals
     * and destroying them with {@link Workspace.Workspace#destroy}.
     *
     * Finally, we make sure our workspaces view is updated with the new
     * number of workspaces using {@link WorkspacesView#updateWorkspaces}.
     */
    _workspacesChanged: function() {
        let oldNumWorkspaces = this._workspaces.length;
        let newNumWorkspaces = global.screen.n_workspaces;
        let active = global.screen.get_active_workspace_index();

        if (oldNumWorkspaces == newNumWorkspaces)
            return;

        this._updateAlwaysZoom();
        this._updateZoom();

        if (this.workspacesView == null)
            return;

        let lostWorkspaces = [];
        if (newNumWorkspaces > oldNumWorkspaces) {
            // Assume workspaces are only added at the end
            for (let w = oldNumWorkspaces; w < newNumWorkspaces; w++) {
                let metaWorkspace = global.screen.get_workspace_by_index(w);
                this._workspaces[w] = new Workspace.Workspace(metaWorkspace, this._monitorIndex);
            }

            this._thumbnailsBox.addThumbnails(oldNumWorkspaces, newNumWorkspaces - oldNumWorkspaces);
        } else {
            // Assume workspaces are only removed sequentially
            // (e.g. 2,3,4 - not 2,4,7)
            let removedIndex;
            let removedNum = oldNumWorkspaces - newNumWorkspaces;
            for (let w = 0; w < oldNumWorkspaces; w++) {
                let metaWorkspace = global.screen.get_workspace_by_index(w);
                if (this._workspaces[w].metaWorkspace != metaWorkspace) {
                    removedIndex = w;
                    break;
                }
            }

            lostWorkspaces = this._workspaces.splice(removedIndex,
                                                     removedNum);

            for (let l = 0; l < lostWorkspaces.length; l++) {
                lostWorkspaces[l].disconnectAll();
                lostWorkspaces[l].destroy();
            }

            this._thumbnailsBox.removeThumbmails(removedIndex, removedNum);
        }

        this.workspacesView.updateWorkspaces(oldNumWorkspaces,
                                             newNumWorkspaces);
    },

    /** Zooms the thumbnails sidebar in or out according to its current
     * collapsed state and what it should be (the animation occurs over
     * time {@link WORKSPACE_SWITCH_TIME}).
     *
     * After zooming we update the window positioning in our workspaces view
     * to reflect the reduced or increased space it now has.
     * @see WorkspacesView#updateWindowPositions
     */
    _updateZoom : function() {
        if (Main.overview.animationInProgress)
            return;

        let shouldZoom = this._alwaysZoomOut || this._controls.hover;
        if (shouldZoom != this._zoomOut) {
            this._zoomOut = shouldZoom;
            this._updateWorkspacesGeometry();

            if (!this.workspacesView)
                return;

            Tweener.addTween(this,
                             { zoomFraction: this._zoomOut ? 1 : 0,
                               time: WORKSPACE_SWITCH_TIME,
                               transition: 'easeOutQuad' });

            this.workspacesView.updateWindowPositions();
        }
    },

    /** Callback when the user hovers over the thumbnails sidebar.
     * This calls {@link #_updateZoom} to zoom out the sidebar if
     * it is currently collapsed. */
    _onControlsHoverChanged: function() {
        this._updateZoom();
    },

    /** Callback for the overview's item-drag-begin and window-drag-begin signals.
     *
     * This is used to detect something being dragged over the thumbnails sidebar,
     * ensuring that it expands/slides out if it is collapsed.
     *
     * We add {@link #_onDragMotion} as a drag monitor (the 'dragMotion' callback).
     * @see #_onDragMotion
     * @see Overview.Overview.window-drag-begin
     * @see Overview.Overview.item-drag-begin */
    _dragBegin: function() {
        this._inDrag = true;
        this._cancelledDrag = false;
        this._dragMonitor = {
            dragMotion: Lang.bind(this, this._onDragMotion)
        };
        DND.addDragMonitor(this._dragMonitor);
    },

    /** Callback for the overview's item-drag-cancelled and window-drag-cancelled signals.
     *
     * We remove the {@link #_onDragMotion} drag monitor.
     *
     * @see #_onDragMotion
     * @see Overview.Overview.window-drag-cancelled
     * @see Overview.Overview.item-drag-cancelled */
    _dragCancelled: function() {
        this._cancelledDrag = true;
        DND.removeDragMonitor(this._dragMonitor);
    },

    /** This is our drag monitor for items and windows being dragged in
     * the overview.
     *
     * This is used to detect something being dragged over the thumbnails sidebar,
     * ensuring that it expands/slides out if it is collapsed.
     *
     * @returns {DND.DragMotionResult} `CONTINUE`.
     */
    _onDragMotion: function(dragEvent) {
        let controlsHovered = this._controls.contains(dragEvent.targetActor);
        this._controls.set_hover(controlsHovered);

        return DND.DragMotionResult.CONTINUE;
    },

    /** Callback for the overview's item-drag-end and window-drag-end signals.
     * 
     * We remove our drag monitor and collapse back the thumbnails sidebar if it
     * was slidden out during the drag.
     * @see #_onDragMotion
     * @see Overview.Overview.window-drag-end
     * @see Overview.Overview.item-drag-end */
    _dragEnd: function() {
        this._inDrag = false;

        // We do this deferred because drag-end is emitted before dnd.js emits
        // event/leave events that were suppressed during the drag. If we didn't
        // defer this, we'd zoom out then immediately zoom in because of the
        // enter event we received. That would normally be invisible but we
        // might as well avoid it.
        Meta.later_add(Meta.LaterType.BEFORE_REDRAW,
                       Lang.bind(this, this._updateZoom));
    },

    /** Callback when the user scrolls the mouse over the thumbnails sidebar.
     * If they scroll up, we switch to th workspace up.
     * If they scroll down, we switch to th workspace down.
     */
    _onScrollEvent: function (actor, event) {
        switch ( event.get_scroll_direction() ) {
        case Clutter.ScrollDirection.UP:
            Main.wm.actionMoveWorkspaceUp();
            break;
        case Clutter.ScrollDirection.DOWN:
            Main.wm.actionMoveWorkspaceDown();
            break;
        }
    }
};
Signals.addSignalMethods(WorkspacesDisplay.prototype);
