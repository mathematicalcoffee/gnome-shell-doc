// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * The overview (press the windows key).
 *
 * ![Overview](pics/Overview.png)
 */

const Clutter = imports.gi.Clutter;
const Gtk = imports.gi.Gtk;
const Meta = imports.gi.Meta;
const Mainloop = imports.mainloop;
const Signals = imports.signals;
const Lang = imports.lang;
const St = imports.gi.St;
const Shell = imports.gi.Shell;
const Gdk = imports.gi.Gdk;

const AppDisplay = imports.ui.appDisplay;
const ContactDisplay = imports.ui.contactDisplay;
const Dash = imports.ui.dash;
const DND = imports.ui.dnd;
const DocDisplay = imports.ui.docDisplay;
const Lightbox = imports.ui.lightbox;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const Panel = imports.ui.panel;
const Params = imports.misc.params;
const PlaceDisplay = imports.ui.placeDisplay;
const Tweener = imports.ui.tweener;
const ViewSelector = imports.ui.viewSelector;
const WorkspacesView = imports.ui.workspacesView;
const WorkspaceThumbnail = imports.ui.workspaceThumbnail;

/** @+
 * @const
 * @default
 * @type {number} */
/** Time (seconds) for initial animation going into Overview mode */
const ANIMATION_TIME = 0.25;

/** We split the screen vertically between the dash and the view selector.
 * The Dash gets this horizontal fraction of the screen */
const DASH_SPLIT_FRACTION = 0.1;

/** Time (milliseconds) the user has to hover an Xdnd object over a window thumbnail
 * in the Overview before that window is activated (e.g. drag a file from
 * Nautilus and over the Overview button to show the windows tab, and hold it
 * over one of the window thumbnails for this many seconds, and that window will
 * be activated allowing the user to drop on it). */
const DND_WINDOW_SWITCH_TIMEOUT = 1250;
/** @- */

/** @+
 * @const
 * @enum */

/** Direction of a swipe-scroll (when the user clicks on empty overview space
 * and "drags" it up/down to scroll workspaces). */
const SwipeScrollDirection = {
    NONE: 0,
    /** Trying to swipe horizontally (converted to up/down in
     * {@link Overview#_onCapturedEvent}). */
    HORIZONTAL: 1,
    /** Trying to swipe vertically */
    VERTICAL: 2
};

/** Direction of a swipe-scroll attempt (when the user clicks on empty overview space
 * and "drags" it up/down to scroll workspaces). */
const SwipeScrollResult = {
    /** The swipe-scroll was cancelled (the user started to drag but didn't
     * drag enough to scroll to the next workspace) */
    CANCEL: 0,
    /** The swipe-scroll was successful. */
    SWIPE: 1,
    /** The swipe-scroll was actually a click. */
    CLICK: 2
}; 
/** @- */

/** creates a new ShellInfo
 * @classdesc
 * A convenience class used to spawn a notification for an action,
 * where the action optionally has a relevant 'undo' action. Really just
 * used from the overview ({@link Overview#setMessage}).
 *
 * If the 'undo' action is provided, the notification will have an extra
 * button that when clicked performs that action.
 *
 * For example - adding/removing favourites from the dash in the app favourites
 * ({@link AppFavorites.AppFavorites#addFavoriteAtPos} shows a "Favorite has
 * been added" along with a "Remove" button for the undo action), and when
 * you try to eject a device {@link PlaceDisplay.PlaceDeviceInfo#remove} and
 * it fails ("Failed to unmount" + a "Retry" button for the undo action,
 * {@link PlaceDisplay.PlaceDeviceInfo#_removeFinish}).
 *
 * Only one notification at a time is shown - successive calls to
 * {@link #setMessage} or {@link Overview#setMessage} will erase
 * the notifications for previous calls (?).
 *
 * The ShellInfo has one source {@link #_source} being a
 * {@link MessageTray.SystemNotificationSource}.
 *
 * To use it, create one instance (for example the Overview stores this in
 * {@link Overview#_shellInfo}) and then call {@link #setMessage}
 * on it with a message and an undo action and label. It will then
 * handle construction the notification and firing it from the source. The
 * end user would just call {@link Overview#setMessage} which gets passed down
 * to the ShellInfo.
 *
 * @todo screenshot of the add/remove favourites notification/source
 * @class
 */
function ShellInfo() {
    this._init();
}

ShellInfo.prototype = {
    _init: function() {
        /** The Source from which to pop up the notifications.
         * Not null if and only if we have a notification attached to it.
         * @type {?MesssageTray.SystemNotificationSource} */
        this._source = null;
        /** The callback to execute upon the user clicking the "Undo" button
         * (if the `undoCallback` argument is provided to {@link #setMessage}).
         * @function */
        this._undoCallback = null;
    },

    /** Callback when the user clicks the undo button on the notification.
     * This executes {@link #_undoCallback} and then destroys the
     * source. */
    _onUndoClicked: function() {
        if (this._undoCallback)
            this._undoCallback();
        this._undoCallback = null;

        if (this._source)
            this._source.destroy();
    },

    /** Called to create a notification with an optional button for an undo action.
     *
     * If {@link #_source} is null it is created and added to the message
     * tray.
     *
     * If the source has no notifications, a {@link MessageTray.Notification}
     * is created from the source with text `text` and a `null` banner.
     *
     * If the source already has a notification, we instead update the first
     * one with the new `text`, clearing the old message.
     *
     * The notification is set to transient.
     *
     * If `undoCallback` is provided, it is stored in
     * {@link #_undoCallback} and a button with `undoLabel` is added
     * to the notification. Upon it being clicked (i.e. the notification's
     * ['action-invoked']{@link MessageTray.Notification.action-invoked} signal),
     * {@link #_onUndoClicked} is called which performs `undoCallback`.
     *
     * Finally, we notify the notification ({@link MessageTray.SystemNotificationSource#notify}).
     * @param {string} text - text to display as the title of the notification.
     * @param {function()} [undoCallback] - if provided, an extra button will
     * be created on the notification with the label `undoLabel` and clicking on
     * it will execute this callback.
     * @param {string} [undoLabel="Undo"] - the label for the undo button.
     * If not provided, translated string "Undo" is used.
     */
    setMessage: function(text, undoCallback, undoLabel) {
        if (this._source == null) {
            this._source = new MessageTray.SystemNotificationSource();
            this._source.connect('destroy', Lang.bind(this,
                function() {
                    this._source = null;
                }));
            Main.messageTray.add(this._source);
        }

        let notification = null;
        if (this._source.notifications.length == 0) {
            notification = new MessageTray.Notification(this._source, text, null);
        } else {
            notification = this._source.notifications[0];
            notification.update(text, null, { clear: true });
        }

        notification.setTransient(true);

        this._undoCallback = undoCallback;
        if (undoCallback) {
            notification.addButton('system-undo',
                                   undoLabel ? undoLabel : _("Undo"));
            notification.connect('action-invoked',
                                 Lang.bind(this, this._onUndoClicked));
        }

        this._source.notify(notification);
    }
};

/** creates a new Overview.
 *
 * If the overview is a dummy one, {@link #animationInProgress} is set
 * to `false`, {@link #visible} is set to `false`, {@link #workspaces}
 * is set to `null`, and that's it.
 *
 * Otherwise:
 *
 * * Get a background actor (shows the user's current desktop) to use as the
 * background for the overview, {@link #_backgroundActor};
 * * Create a St.Bin {@link #_desktopFade}, as far as I can tell only
 * used when Nautilus is managing the desktop and you fade in and out of
 * the Overview on a workspace with no maximized windows. I think it's just
 * for visual effect so that the desktop shows in the background when we
 * tween in the overview?
 * * Create a top-level actor to hold all the overview components
 * ({@link #_group}), whose delegate is this instance;
 * * initialise various properties ({#animationInProgress} is `false`, 
 * {@link #visible is `false`, ...}).
 * * Create an actor to capture all events while the overview is tweening in
 * and out, to avoid them being passed to the overview ({@link #_coverPane});
 * * Set up a drag monitor ({@link #_dragMonitor}) with 'dragMotion'
 * connected to {@link #_onDragMotion} for use with Xdnd;
 * * Connect up to {@link Main.xdndHandler} and connect the 'drag-begin'
 * and 'drag-end' signals to {@link #_onDragBegin} and {@link #_onDragEnd}
 * so that we support XDND.
 *
 * Note that not all creation of stuff is done here; the rest is done in
 * {@link #init}
 * @see #init
 * @param {Object} [params] - parameters for the Overview.
 * @param {boolean} [params.isDummy=false] - whether to create the full overview that
 * the user interacts with, or just a stub (the bare minimum to let gnome-shell
 * run without error). {@link Main.start} creates a stub overview if the session
 * is not a user session (it might be a GDM one).
 * 
 * A dummy overview has {@link #animationInProgress} as `false`,
 * {@link #visible} as `false`, {@link #workspaces} as `null`,
 * and *nothing else* (no tabs, etc)
 * @classdesc
 * ![Overview](pics/Overview.png)
 *
 * The Overview is what appears when you press the Windows key or click on the
 * Activities button or activate the hot corner in the top-left of th screen.
 *
 * It is composed of a {@link Dash.Dash} and a {@link ViewSelector.ViewSelector}
 * and a {@link WorkspacesView.WorkspacesDisplay}.
 *
 * The Dash on the left shows icons for all the user's {@link AppFavorites.AppFavorites}, as
 * well as an icon for each running application, and allows various actions
 * to launch new instances of applications or switch to the existing instance.
 *
 * The View Selector is the main part of the Overview, containing three
 * tabs (by default) - windows, applications, and a search tab.
 *
 * The Windows tab contains
 * [WorkspacesDisplay.actor]{@link WorkspacesView.WorkspacesDisplay#actor} which shows a thumbnail
 * for each window open on the current workspace, allowing the user to click
 * on them to activate that window, or drag them around to different
 * workspaces/screens. It also contains the
 * [WorkspacesDisplay._thumbnailsBox]{@link WorkspacesView.WorkspacesDisplay#_thumbnailsBox},
 * the right-hand sidebar showing thumbnails of each workspace.
 *
 * The Applications tab is a {@link AppDisplay.AllAppsDisplay} showing a grid
 * of all the applications installed on the computer, along with a sidebar
 * containing application categories to filter what applications are shown.
 *
 * Finally, the entire overview is configured for drag and drop. Starting to
 * drag an item always causes the ViewSelector to switch to the Windows tab.
 *
 * One may drag application icons to the Windows tab to open that application,
 * or to the dash to add it to their favourites, or over the workspaces
 * sidebar to open the application on that workspace/a new one. Windows
 * in the windows tab, search results, and windows in the workspace sidebar
 * thumbnails can all be dragged. Basically, almost all overview elements
 * can be dragged over almost all other overview elements to perform an action
 * (usually launching whatever is being dragged, which is usually tied to
 * an application).
 *
 * Note that it is not enough to simply create a `new Overview()`; one must
 * also call {@link #init} to initialise it all.
 * @class
 */
function Overview() {
    this._init.apply(this, arguments);
}

Overview.prototype = {
    _init : function(params) {
        params = Params.parse(params, { isDummy: false });

        /** Whether the Overview is a dummy one (i.e. just a stub). A dummy
         * overview is created in {@link Main.start} if the session is not
         * a user one (and hence the overview will not be used, but it
         * needs to exist because other objects query its properties).
         * 
         * A dummy overview has {@link #animationInProgress} as `false`,
         * {@link #visible} as `false`, {@link #workspaces} as `null`,
         * and *nothing else* (no tabs, etc)
         * @type {boolean}
         */
        this.isDummy = params.isDummy;

        // We only have an overview in user sessions, so
        // create a dummy overview in other cases
        if (this.isDummy) {
            this.animationInProgress = false;
            this.visible = false;
            this.workspaces = null;
            return;
        }

        /** Background for the overview. The overview looks like it is semi-transparent
         * and on top of the user's current desktop background. This is the
         * desktop background actor.
         *
         * The main BackgroundActor is inside global.window_group which is
         * hidden when displaying the overview, so we create a new
         * one. Instances of this class share a single CoglTexture behind the
         * scenes which allows us to show the background with different
         * rendering options without duplicating the texture data.
         * @type {Meta.BackgroundActor}
         */
        this._background = Meta.BackgroundActor.new_for_screen(global.screen);
        this._background.hide();
        global.overlay_group.add_actor(this._background);

        /** This is a St.Bin that holds a clone of the desktop background, as
         * far as I can tell just used when Nautilus is managing the desktop and
         * the user toggles the overview while on a workspace with no maximized
         * windows - I think it's eye candy to make sure the fading in shows
         * the bits of exposed desktop in the background?
         * @type {St.Bin}
         * @see #_getDesktopClone */
        this._desktopFade = new St.Bin();
        global.overlay_group.add_actor(this._desktopFade);

        this._spacing = 0;

        /** Holds all the overview components - the main container/actor for
         * the overview.
         * @type {St.Group} */
        this._group = new St.Group({ name: 'overview',
                                     reactive: true });
        this._group._delegate = this;
        this._group.connect('style-changed',
            Lang.bind(this, function() {
                let node = this._group.get_theme_node();
                let spacing = node.get_length('spacing');
                if (spacing != this._spacing) {
                    this._spacing = spacing;
                    this._relayout();
                }
            }));

        this._scrollDirection = SwipeScrollDirection.NONE;
        this._scrollAdjustment = null;
        this._capturedEventId = 0;
        this._buttonPressId = 0;

        /** The WorkspacesDisplay, showing all the windows of the current
         * workspace. This is created afresh every time we show the overview
         * and destroyed every time we hide it.
         * @type {WorkspacesView.WorkspacesDisplay} */
        this._workspacesDisplay = null;

        /** @+
         * @type {boolean} */
        /** Whether the overview is currently visible (read-only in the sense
         * that setting it will do nothing). */
        this.visible = false;           // animating to overview, in overview, animating out
        /** Whether we are currently shown
         * @see #show */
        this._shown = false;            // show() and not hide()
        /** Whether we are currently shown temporarily.
         * We are shown temporarily when (say) the user starts an Xdnd drag
         * (drags a non-gnome-shell object) over the Overview button - the
         * overview is temporarily shown that we might drop the object on it.
         * @see #showTemporarily */
        this._shownTemporarily = false; // showTemporarily() and not hideTemporarily()
        /** Whether we currently have a modal grab. */
        this._modal = false;            // have a modal grab
        /** Whether the Overview is currently animating in/out. */
        this.animationInProgress = false;
        /** Whether we are currently doing the hiding animation */
        this._hideInProgress = false;
        /** @- */

        /** During transitions, we raise this to the top to avoid having the overview
         * area be reactive; it causes too many issues such as double clicks on
         * Dash elements, or mouseover handlers in the workspaces.
         * @type {Clutter.Rectangle}
         */
        this._coverPane = new Clutter.Rectangle({ opacity: 0,
                                                  reactive: true });
        this._group.add_actor(this._coverPane);
        this._coverPane.connect('event', Lang.bind(this, function (actor, event) { return true; }));


        this._group.hide();
        global.overlay_group.add_actor(this._group);

        this._coverPane.hide();

        // XDND
        /** The drag monitor for the overview. The `dragMotion` callback is
         * {@link #_onDragMotion}. Used for Xdnd drags.
         *
         * Basically what this does - if the user starts a Xdnd drag (say
         * dragging a file from nautilus) and holds it over a window thumbnail
         * in the windows tab for at least {@link DND_SWITCH_WINDOW_TIMEOUT},
         * that window will be activated (so the user can drop it there).
         * @type {DND.DragMonitor} */
        this._dragMonitor = {
            dragMotion: Lang.bind(this, this._onDragMotion)
        };

        Main.xdndHandler.connect('drag-begin', Lang.bind(this, this._onDragBegin));
        Main.xdndHandler.connect('drag-end', Lang.bind(this, this._onDragEnd));

        this._windowSwitchTimeoutId = 0;
        this._windowSwitchTimestamp = 0;
        this._lastActiveWorkspaceIndex = -1;
        this._lastHoveredWindow = null;
        this._needsFakePointerEvent = false;

        /** This is {@link #_workspacesDisplay}'s `.workspacesView` member.
         * @see WorkspacesView.WorkspacesDisplay#workspacesView
         * @type {WorkspacesView.WorkspacesView}
         */
        this.workspaces = null;
    },

    /** Initialise the overview (make the view selector, workspaces display, ...).
     *
     * The reason this is not done in the constructor is that
     * the members we construct that are implemented in JS might
     * want to access the overview as `Main.overview` to connect
     * signal handlers and so forth. So we create them after
     * construction in this init() method.
     *
     * If this is a dummy Overview ({@link #isDummy}), we quit straight away.
     *
     * Otherwise:
     *
     * * Create a {@link ShellInfo} for use with {@link #setMessage}
     * ({@link #_shellInfo});
     * * Create the {@link ViewSelector.ViewSelector}, store in
     * {@link #_viewSelector};
     * * Create the {@link WorkspacesView.WorkspacesDisplay}, add it to the
     * view selector with tab ID 'windows' and tab title "Windows" translated,
     * store in {@link #_workspacesDisplay};
     * * Create the applications tab {@link AppDisplay.AllAppDisplay} and add
     * it to the view selector with tab ID 'applications' and tab title
     * "Applications" (translated);
     * * Add a {@link AppDisplay.AppSearchProvider}, {@link AppDisplay.SettingsSearchProvider},
     * {@link PlaceDisplay.PlaceSearchProvider}, {@link DocDisplay.DocSearchProvider},
     * and {@link ContactDisplay.ContactSearchProvider} to the overview with
     * {@link #addSearchProvider};
     * * Create the {@link Dash.Dash}, store in {@link #_dash}, and
     * constrain its height/y position to that of the {@link ViewSelector.ViewSelector}
     * using {@link ViewSelector.ViewSelector#constrainY} and
     * {@link ViewSelector.ViewSelector#constrainHeight} (recall that the Dash's
     * top-level actor {@link Dash.Dash#actor} is a clear container that contains
     * the actual visible part of the dash, which is centred vertically within);
     * * Add the dash to the {@link CtrlAltTab.CtrlAltTabManager};
     * * Connect to
     * [Main.layoutManager's 'monitors-changed' signal]{@link Layout.LayoutManager.monitors-changed}
     * in order to relayout all the elements when that happens;
     * * Call {@link #_relayout} to position all the bits.
     */
    init: function() {
        if (this.isDummy)
            return;

        /** Shell Info used with {@link #setMessage} to pop up notifications
         * to the user.
         * @type {Overview.ShellInfo} */
        this._shellInfo = new ShellInfo();

        /** The View Selector for the overview, containing a windows and applications
         * tab as well as a search tab.
         * @type {ViewSelector.ViewSelector} */
        this._viewSelector = new ViewSelector.ViewSelector();
        this._group.add_actor(this._viewSelector.actor);

        /** The workspaces display/windows tab of the overview, showing window thumbnails
         * for each window on the current workspace as well as the workspaces
         * sidebar to the right.
         * @type {WorkspacesView.WorkspacesDisplay} */
        this._workspacesDisplay = new WorkspacesView.WorkspacesDisplay();
        this._viewSelector.addViewTab('windows', _("Windows"), this._workspacesDisplay.actor, 'text-x-generic');

        /** The applications tab of the Overview, allowing the user to browse
         * their currently-installed applications.
         * @type {AppDisplay.AllAppDisplay} */
        let appView = new AppDisplay.AllAppDisplay();
        this._viewSelector.addViewTab('applications', _("Applications"), appView.actor, 'system-run');

        // Default search providers
        this.addSearchProvider(new AppDisplay.AppSearchProvider());
        this.addSearchProvider(new AppDisplay.SettingsSearchProvider());
        this.addSearchProvider(new PlaceDisplay.PlaceSearchProvider());
        this.addSearchProvider(new DocDisplay.DocSearchProvider());
        this.addSearchProvider(new ContactDisplay.ContactSearchProvider());

        // TODO - recalculate everything when desktop size changes
        /** The Dash.
         * @type {Dash.Dash} */
        this._dash = new Dash.Dash();
        this._group.add_actor(this._dash.actor);
        this._dash.actor.add_constraint(this._viewSelector.constrainY);
        this._dash.actor.add_constraint(this._viewSelector.constrainHeight);
        this.dashIconSize = this._dash.iconSize;
        this._dash.connect('icon-size-changed',
                           Lang.bind(this, function() {
                               this.dashIconSize = this._dash.iconSize;
                           }));

        // Translators: this is the name of the dock/favorites area on
        // the left of the overview
        Main.ctrlAltTabManager.addGroup(this._dash.actor, _("Dash"), 'user-bookmarks');

        Main.layoutManager.connect('monitors-changed', Lang.bind(this, this._relayout));
        this._relayout();
    },

    /** Adds a search provider to the view selector, calling
     * [`this._viewSelector.addSearchProvider`]{@link ViewSelector.ViewSelector#addSearchProvider}.
     * @see ViewSelector.ViewSelector#addSearchProvider
     * @param {Search.SearchProvider} provider - a search provider.
     */
    addSearchProvider: function(provider) {
        this._viewSelector.addSearchProvider(provider);
    },

    /** Removes a search provider from view selector, calling
     * [`this._viewSelector.removeSearchProvider`]{@link ViewSelector.ViewSelector#removeSearchProvider}.
     * @see ViewSelector.ViewSelector#removeSearchProvider
     * @param {Search.SearchProvider} provider - a search provider.
     */
    removeSearchProvider: function(provider) {
        this._viewSelector.removeSearchProvider(provider);
    },

    /** Pops up a notification with title `text` and optionally an extra button
     * with label `undoLabel` that will perform action `undoCallback` (if
     * the overview is not a dummy one).
     *
     * This is just passed through to {@link #_shellInfo}.
     * @see ShellInfo#setMessage
     * @inheritparams ShellInfo#setMessage
     */
    setMessage: function(text, undoCallback, undoLabel) {
        if (this.isDummy)
            return;

        this._shellInfo.setMessage(text, undoCallback, undoLabel);
    },

    /** Callback when a XDND drag begins (the user has started dragging some
     * non-gnome-shell object like a file from Nautilus).
     *
     * This adds {@link #_dragMonitor} as a drag monitor.
     *
     * Basically what {@link #_onDragMotion} does is that if the user
     * hovers their dragged object over a window thumbnail for at least
     * {@link DND_SWITCH_WINDOW_TIMEOUT} seconds, that window is activated
     * so that the user can do the drop on that window.
     * @see XdndHandler.XdndHandler.drag-begin
     * @see #_onDragMotion
     * @see DND.addDragMonitor
     */
    _onDragBegin: function() {
        DND.addDragMonitor(this._dragMonitor);
        // Remember the workspace we started from
        this._lastActiveWorkspaceIndex = global.screen.get_active_workspace_index();
    },

    /** Callback when a XDND drag ends in the overview (the user has stopped
     * dragging their non-gnome-shell object like a file from Nautilus).
     * 
     * The only way in which the overview supports Xdnd drags is if the user
     * drags their object over the Activities button to launch the overview and
     * then hovers it over a window thumbnail, causing that window to be activated
     * so that the user can finish their Xdnd drag there.
     *
     * If the user did not do this (get the overview to activate the
     * hovered-over window) and ends the drag (either by dropping it somewhere
     * invalid or pressing Esc), the 'drag-end' signal is emitted and this
     * is what this handler is for - for a cancelled/failed drag.
     *
     * This removes {@link #_dragMonitor} as a drag monitor. If the
     * overview is showing temporarily (as a result of the Xdnd drag),
     * we reverse this effect with {@link #hideTemporarily}.
     *
     * Finally we call {@link #endItemDrag}.
     *
     * @see XdndHandler.XdndHandler.drag-end
     * @see DND.removeDragMonitor
     * @see #endItemDrag
     */
    _onDragEnd: function(time) {
        // In case the drag was canceled while in the overview
        // we have to go back to where we started and hide
        // the overview
        if (this._shownTemporarily)  {
            global.screen.get_workspace_by_index(this._lastActiveWorkspaceIndex).activate(time);
            this.hideTemporarily();
        }
        this._resetWindowSwitchTimeout();
        this._lastHoveredWindow = null;
        DND.removeDragMonitor(this._dragMonitor);
        this.endItemDrag();
    },

    /** Resets the window switch timeout.
     * This is the timeout of {@link DND_WINDOW_SWITCH_TIMEOUT} milliseconds
     * that the user has to hover their Xdnd dragged object over a particular
     * window thumbnail in order for the window to be activated.
     *
     * Calling this cancels the timeout (for example if the user stopped hovering
     * the item or stopped dragging before the timeout completed).
     * @see #_onDragMotion
     */
    _resetWindowSwitchTimeout: function() {
        if (this._windowSwitchTimeoutId != 0) {
            Mainloop.source_remove(this._windowSwitchTimeoutId);
            this._windowSwitchTimeoutId = 0;
            this._needsFakePointerEvent = false;
        }
    },

    /** Causes a fake pointer event - makes sure that all events that are
     * listening to pointer positions are updated with the current pointer event
     *
     * Just gets the current pointer position and moves the pointer to the
     * same position, probably emitting the relevant pointer events to any listeners.
     * 
     * (I think you can sometimes lose track of where the pointer is if there's
     * a pointer grab?) */
    _fakePointerEvent: function() {
        let display = Gdk.Display.get_default();
        let deviceManager = display.get_device_manager();
        let pointer = deviceManager.get_client_pointer();
        let [screen, pointerX, pointerY] = pointer.get_position();

        pointer.warp(screen, pointerX, pointerY);
    },

    /** The 'dragMotion' callback for [our drag monitor]{@link #_dragMonitor},
     * used for Xdnd. Called whenever the drag position is updated (user moves the
     * cursor while dragging).
     *
     * The only way in which the overview supports Xdnd drags is if the user
     * drags their object (say a file from Nautilus) over the Activities button
     * to launch the overview and then hovers it over a window thumbnail in
     * the windows tab.
     *
     * If they hover over it for at least {@link DND_WINDOW_SWITCH_TIMEOUT} seconds,
     * that window will be activated
     * so that the user can finish their Xdnd drag there.
     *
     * If they end the drag in any other way it is not handled and
     * {@link #_onDragEnd} is called.
     *
     * So, this callback handles the logic for the timeout and activating the
     * hovered-over window:
     *
     * * If the actor being dragged over (i.e. the potential drop target) is
     * *not* a {@link Workspace.WindowClone}, we cancel the window switch
     * timeout ({@link #_resetWindowSwitchTimeout}) and quit. Otherwise:
     * * If we've dragged over a window thumbnail for the first time,
     * we store this window and add a timeout of length {@link DND_WINDOW_SWITCH_TIMEOUT}
     * to both hide the overview temporarily {@link #hideTemporarily} and
     * activate the window we're hovered over.
     * * If this is not the first drag motion over our window thumbnail (i.e.
     * we're moving our draggable around but still over the same target as last
     * time without leaving it in between), we return {@link DND.DragMotionResult.CONTINUE}
     * and do nothing else (the timeout has already been added when we first
     * dragged over the window).
     *
     * @see #_dragMonitor
     * @see #_onDragBegin
     * @see #_onDragEnd
     * @param {DND.DragEvent} dragEvent - information about the current drag.
     * @returns {DND.DragMotionResult} {@link DND.DragMotionResult.CONTINUE}, always.
     */
    _onDragMotion: function(dragEvent) {
        let targetIsWindow = dragEvent.targetActor &&
                             dragEvent.targetActor._delegate &&
                             dragEvent.targetActor._delegate.metaWindow &&
                             !(dragEvent.targetActor._delegate instanceof WorkspaceThumbnail.WindowClone);

        this._windowSwitchTimestamp = global.get_current_time();

        if (targetIsWindow &&
            dragEvent.targetActor._delegate.metaWindow == this._lastHoveredWindow)
            return DND.DragMotionResult.CONTINUE;

        this._lastHoveredWindow = null;

        this._resetWindowSwitchTimeout();

        if (targetIsWindow) {
            this._lastHoveredWindow = dragEvent.targetActor._delegate.metaWindow;
            this._windowSwitchTimeoutId = Mainloop.timeout_add(DND_WINDOW_SWITCH_TIMEOUT,
                                            Lang.bind(this, function() {
                                                this._needsFakePointerEvent = true;
                                                Main.activateWindow(dragEvent.targetActor._delegate.metaWindow,
                                                                    this._windowSwitchTimestamp);
                                                this.hideTemporarily();
                                                this._lastHoveredWindow = null;
                                            }));
        }

        return DND.DragMotionResult.CONTINUE;
    },

    /** Sets the scroll adustment for the Overview, used for swipe-scroll
     * calculations.
     *
     * Used by the {@link WorkspacesView.WorkspacesView} (and swipe scrolling
     * only affects that anyway, showing the windows of a different
     * {@link Workspace.Workspace}).
     * @param {St.Adjustment} adjustment - adjustment
     * @param {Overview.SwipeScrollDirection} direction - the scroll direction
     * @see WorkspacesView.WorkspacesView#_onMapped
     */
    setScrollAdjustment: function(adjustment, direction) {
        if (this.isDummy)
            return;

        this._scrollAdjustment = adjustment;
        if (this._scrollAdjustment == null)
            this._scrollDirection = SwipeScrollDirection.NONE;
        else
            this._scrollDirection = direction;
    },

    /** Callback for clicks on the Overview (that are not handled by any of
     * the elements of the overview).
     *
     * The user is able to switch workspaces in the Windows tab by simply
     * "dragging" up/down to the next workspace.
     *
     * This function listens to left click button presses and then intercepts
     * all events from the stage, connecting to {@link #_onCapturedEvent}
     * in an attempt to detect a "swipe"/"scroll".
     *
     * This emits the 'swipe-scroll-begin' event (even if it ends up being
     * just a simple left click).
     */
    _onButtonPress: function(actor, event) {
        if (this._scrollDirection == SwipeScrollDirection.NONE
            || event.get_button() != 1)
            return;

        let [stageX, stageY] = event.get_coords();
        this._dragStartX = this._dragX = stageX;
        this._dragStartY = this._dragY = stageY;
        this._dragStartValue = this._scrollAdjustment.value;
        this._lastMotionTime = -1; // used to track "stopping" while swipe-scrolling
        this._capturedEventId = global.stage.connect('captured-event',
            Lang.bind(this, this._onCapturedEvent));
        this.emit('swipe-scroll-begin');
    },

    /** Callback for any captured event once ['swipe-scroll-begin']{@link .swipe-scroll-begin}
     * has been emitted from the Overview as a result of {@link #_onButtonPress}.
     *
     * This function determines whether the user has done a "swipe up/down"
     * motion, meaning the window thumbnails in the windows tab should start
     * scrolling to show the windows of the next workspace.
     *
     * If the event is a button release, a whole bunch of logic is done to determine
     * whether the distance scrolled since the button press is over the threshold
     * required to swipe to the next workspace in the windows tab (if it isn't
     * we just snap back to the current workspace).
     *
     * This function emits the ['swipe-scroll-end']{@link .swipe-scroll-end}
     * signal with the result of what the swipe was - either a click, a swipe,
     * or a cancelled swipe.
     *
     * If the event is a button release but we didn't move enough to trigger a
     * drag (i.e. it was a left click), we emit 'swipe-scroll-end' with
     * result {@link SwipeScrollResult.CLICK}.
     *
     * If the event is a button release, but we did move enough to trigger a drag
     * (half the distance to the next workspace)
     * but not enough to scroll entirely
     * to the next workspace, we emit 'swipe-scroll-end' with result
     * {@link SwipeScrollResult.CANCEL} and animate "snapping" back to the current
     * workspace.
     *
     * If the event is a button release and we *did* move enough to trigger a
     * drag and enough to scroll entirely to
     * the next workspace, we emit 'swipe-scroll-end' with result
     * {@link SwipeScrollResult.SWIPE} and animate scrolling to the next workspace.
     *
     * If the event is a motion (i.e. we are currently in the middle of the swipe),
     * we animate 'dragging' the current workspace with the cursor.
     *
     * If the event is an enter or leave event we block it to avoid
     * prelights over other Overview elements while were in the middle of a
     * swipe-scroll.
     * @fires swipe-scroll-end
     */
    _onCapturedEvent: function(actor, event) {
        let stageX, stageY;
        let threshold = Gtk.Settings.get_default().gtk_dnd_drag_threshold;

        switch(event.type()) {
            case Clutter.EventType.BUTTON_RELEASE:
                [stageX, stageY] = event.get_coords();

                // default to snapping back to the original value
                let newValue = this._dragStartValue;

                let minValue = this._scrollAdjustment.lower;
                let maxValue = this._scrollAdjustment.upper - this._scrollAdjustment.page_size;

                let direction;
                if (this._scrollDirection == SwipeScrollDirection.HORIZONTAL) {
                    direction = stageX > this._dragStartX ? -1 : 1;
                    if (St.Widget.get_default_direction() == St.TextDirection.RTL)
                        direction *= -1;
                } else {
                    direction = stageY > this._dragStartY ? -1 : 1;
                }

                // We default to scroll a full page size; both the first
                // and the last page may be smaller though, so we need to
                // adjust difference in those cases.
                let difference = direction * this._scrollAdjustment.page_size;
                if (this._dragStartValue + difference > maxValue)
                    difference = maxValue - this._dragStartValue;
                else if (this._dragStartValue + difference < minValue)
                    difference = minValue - this._dragStartValue;

                // If the user has moved more than half the scroll
                // difference, we want to "settle" to the new value
                // even if the user stops dragging rather "throws" by
                // releasing during the drag.
                let distance = this._dragStartValue - this._scrollAdjustment.value;
                let noStop = Math.abs(distance / difference) > 0.5;

                // We detect if the user is stopped by comparing the
                // timestamp of the button release with the timestamp of
                // the last motion. Experimentally, a difference of 0 or 1
                // millisecond indicates that the mouse is in motion, a
                // larger difference indicates that the mouse is stopped.
                if ((this._lastMotionTime > 0 &&
                     this._lastMotionTime > event.get_time() - 2) ||
                    noStop) {
                    if (this._dragStartValue + difference >= minValue &&
                        this._dragStartValue + difference <= maxValue)
                        newValue += difference;
                }

                let result;

                // See if the user has moved the mouse enough to trigger
                // a drag
                if (Math.abs(stageX - this._dragStartX) < threshold &&
                    Math.abs(stageY - this._dragStartY) < threshold) {
                    // no motion? It's a click!
                    result = SwipeScrollResult.CLICK;
                    this.emit('swipe-scroll-end', result);
                } else {
                    if (newValue == this._dragStartValue)
                        result = SwipeScrollResult.CANCEL;
                    else
                        result = SwipeScrollResult.SWIPE;

                    // The event capture handler is disconnected
                    // while scrolling to the final position, so
                    // to avoid undesired prelights we raise
                    // the cover pane.
                    this._coverPane.raise_top();
                    this._coverPane.show();

                    Tweener.addTween(this._scrollAdjustment,
                                     { value: newValue,
                                       time: ANIMATION_TIME,
                                       transition: 'easeOutQuad',
                                       onCompleteScope: this,
                                       onComplete: function() {
                                          this._coverPane.hide();
                                          this.emit('swipe-scroll-end',
                                                    result);
                                       }
                                     });
                }

                global.stage.disconnect(this._capturedEventId);
                this._capturedEventId = 0;

                return result != SwipeScrollResult.CLICK;

            case Clutter.EventType.MOTION:
                [stageX, stageY] = event.get_coords();
                let dx = this._dragX - stageX;
                let dy = this._dragY - stageY;
                let primary = Main.layoutManager.primaryMonitor;

                this._dragX = stageX;
                this._dragY = stageY;
                this._lastMotionTime = event.get_time();

                // See if the user has moved the mouse enough to trigger
                // a drag
                if (Math.abs(stageX - this._dragStartX) < threshold &&
                    Math.abs(stageY - this._dragStartY) < threshold)
                    return true;

                if (this._scrollDirection == SwipeScrollDirection.HORIZONTAL) {
                    if (St.Widget.get_default_direction() == St.TextDirection.RTL)
                        this._scrollAdjustment.value -= (dx / primary.width) * this._scrollAdjustment.page_size;
                    else
                        this._scrollAdjustment.value += (dx / primary.width) * this._scrollAdjustment.page_size;
                } else {
                    this._scrollAdjustment.value += (dy / primary.height) * this._scrollAdjustment.page_size;
                }

                return true;

            // Block enter/leave events to avoid prelights
            // during swipe-scroll
            case Clutter.EventType.ENTER:
            case Clutter.EventType.LEAVE:
                return true;
        }

        return false;
    },

    /** Creates a Clutter.Clone of the Desktop window (will only work if
     * Nautilus is managing the desktop).
     *
     * Used with {@link #_desktopFade} for some animation prettiness,
     * as far as I can tell.
     * @returns {?Clutter.Clone} a Clutter.Clone of the desktop window's
     * texture if we managed to find the desktop window (i.e. Nautilus is
     * managing it); `null` otherwise.
     */
    _getDesktopClone: function() {
        let windows = global.get_window_actors().filter(function(w) {
            return w.meta_window.get_window_type() == Meta.WindowType.DESKTOP;
        });
        if (windows.length == 0)
            return null;

        let clone = new Clutter.Clone({ source: windows[0].get_texture() });
        clone.source.connect('destroy', Lang.bind(this, function() {
            clone.destroy();
        }));
        return clone;
    },

    /** Positions all the overview actors in the overview.
     *
     * First we hide the overview to stop all sorts of flickering business while
     * we update.
     *
     * Then we set {@link #_group} (the top-level container for all
     * the overview actor) such that it fills the entire primary monitor.
     *
     * We allocate {@link #dash} a width of {@link DASH_SPLIT_FRACTION} by the primary
     * monitor's width and position it to the left of the monitor.
     *
     * We allocate {@link #_viewSelector} the remaining width and position
     * it to take up the rest of the space.
     *
     * Called upon initial setup and also whenever the monitor layout changes.
     */
    _relayout: function () {
        // To avoid updating the position and size of the workspaces
        // we just hide the overview. The positions will be updated
        // when it is next shown.
        this.hide();

        let primary = Main.layoutManager.primaryMonitor;
        let rtl = (St.Widget.get_default_direction () == St.TextDirection.RTL);

        let contentY = Main.panel.actor.height;
        let contentHeight = primary.height - contentY - Main.messageTray.actor.height;

        this._group.set_position(primary.x, primary.y);
        this._group.set_size(primary.width, primary.height);

        this._coverPane.set_position(0, contentY);
        this._coverPane.set_size(primary.width, contentHeight);

        let dashWidth = Math.round(DASH_SPLIT_FRACTION * primary.width);
        let viewWidth = primary.width - dashWidth - this._spacing;
        let viewHeight = contentHeight - 2 * this._spacing;
        let viewY = contentY + this._spacing;
        let viewX = rtl ? 0 : dashWidth + this._spacing;

        // Set the dash's x position - y is handled by a constraint
        let dashX;
        if (rtl) {
            this._dash.actor.set_anchor_point_from_gravity(Clutter.Gravity.NORTH_EAST);
            dashX = primary.width;
        } else {
            dashX = 0;
        }
        this._dash.actor.set_x(dashX);

        this._viewSelector.actor.set_position(viewX, viewY);
        this._viewSelector.actor.set_size(viewWidth, viewHeight);
    },

    //// Public methods ////

    /** Call this when an item in the overview has started to be dragged.
     * Emits ['item-drag-begin']{@link .item-drag-begin} but
     * does nothing else.
     * 
     * {@link SearchDisplay.SearchResult} uses this (it registers itself as
     * a draggable and upon the draggable being dragged, it calls this function),
     * as does {@link AppDisplay.AppWellIcon}.
     *
     * It's for the benefit of any listeners. For example the {@link ViewSelector.ViewSelector}
     * connects to this signal in order to switch to the windows tab whenever
     * an item is dragged.
     * @param {Clutter.Actor} source - item being dragged (but it is not
     * emitted or saved so there's no way you can access it!)
     * @fires .item-drag-begin
     * @see #cancelledItemDrag
     * @see #endItemDrag
     */
    beginItemDrag: function(source) {
        this.emit('item-drag-begin');
    },

    /** Call this when an item in the overview that is being dragged, has the
     * drag cancelled.
     * Emits ['item-drag-cancelled']{@link .item-drag-cancelled} but
     * does nothing else.
     * 
     * It's for the benefit of any listeners. For example the
     * {@link Dash.Dash} and {@link WorkspacesView.WorkspacesDisplay} both
     * connect to this signal.
     *
     * @inheritparams #beginItemDrag
     * @fires .item-drag-cancelled
     * @see #beginItemDrag
     * @see #endItemDrag
     */
    cancelledItemDrag: function(source) {
        this.emit('item-drag-cancelled');
    },

    /** Call this when an item in the overview that is being dragged, ends the
     * drag.
     * Emits ['item-drag-end']{@link .item-drag-end} but
     * does nothing else.
     * 
     * It's for the benefit of any listeners. For example the
     * {@link Dash.Dash} and {@link WorkspacesView.WorkspacesDisplay} both
     * connect to this signal.
     *
     * @inheritparams #beginItemDrag
     * @fires .item-drag-end
     * @see #beginItemDrag
     * @see #cancelledItemDrag
     */
    endItemDrag: function(source) {
        this.emit('item-drag-end');
    },

    /** Call this when a window is dragged.
     * Emits ['window-drag-begin']{@link .window-drag-begin} but
     * does nothing else.
     *
     * The {@link Dash.Dash} and {@link WorkspacesView.WorkspacesView} both
     * listen to this.
     *
     * Like the 'item-drag-*' signals.
     *
     * @fires .window-drag-begin
     * @inheritparams #beginItemDrag
     * @see #cancelledWindowDrag
     * @see #endWindowDrag
     */
    beginWindowDrag: function(source) {
        this.emit('window-drag-begin');
    },

    /** Call this when a window that was being dragged has the drag cancelled.
     * Emits ['window-drag-cancelled']{@link .item-drag-cancelled} but
     * does nothing else.
     *
     * The {@link Dash.Dash} and {@link WorkspacesView.WorkspacesView} both
     * listen to this.
     *
     * Like the 'item-drag-*' signals.
     *
     * @fires .window-drag-cancelled
     * @inheritparams #beginItemDrag
     * @see #beginWindowDrag
     * @see #endWindowDrag
     */
    cancelledWindowDrag: function(source) {
        this.emit('window-drag-cancelled');
    },

    /** Call this when a window that was being dragged finishes its drag.
     * Emits ['window-drag-end']{@link .item-drag-end} but
     * does nothing else.
     *
     * The {@link Dash.Dash} and {@link WorkspacesView.WorkspacesView} both
     * listen to this.
     *
     * Like the 'item-drag-*' signals.
     *
     * @fires .window-drag-end
     * @inheritparams #beginItemDrag
     * @see #beginWindowDrag
     * @see #cancelledWindowDrag
     */
    endWindowDrag: function(source) {
        this.emit('window-drag-end');
    },

    /** Animates the overview visible and grabs mouse and keyboard input by
     * pushing {@link #_group} modally and calling
     * {@link #_animateVisible} to do all the fancy animation.
     *
     * We always open to the windows tab.
     */
    show : function() {
        if (this.isDummy)
            return;
        if (this._shown)
            return;
        // Do this manually instead of using _syncInputMode, to handle failure
        if (!Main.pushModal(this._group))
            return;
        this._modal = true;
        this._animateVisible();
        this._shown = true;

        this._buttonPressId = this._group.connect('button-press-event',
            Lang.bind(this, this._onButtonPress));
    },

    /** Shows the overview, doing fancy animations for the windows tab.
     *
     * While we are animatin {@link #animationInProgress} is set to
     * `true`.
     *
     * We hide all the window actors in `global.window_group` (speeds up
     * animation).
     *
     * We show the desktop picture and slowly dim it as we fade in {@link #_group}
     * simultaneously over {@link ANIMATION_TIME}.
     *
     * We also raise {@link #_coverPane} to the front to block any
     * click events while we are animating.
     *
     * We emit a 'showing' signal to tell everyone that we are in the process
     * of showing the Overview.
     *
     * Upon animation complete {@link #_showDone} is called.
     * @fires .showing
     */
    _animateVisible: function() {
        if (this.visible || this.animationInProgress)
            return;

        this.visible = true;
        this.animationInProgress = true;

        // All the the actors in the window group are completely obscured,
        // hiding the group holding them while the Overview is displayed greatly
        // increases performance of the Overview especially when there are many
        // windows visible.
        //
        // If we switched to displaying the actors in the Overview rather than
        // clones of them, this would obviously no longer be necessary.
        //
        // Disable unredirection while in the overview
        Meta.disable_unredirect_for_screen(global.screen);
        global.window_group.hide();
        this._group.show();
        this._background.show();

        this._workspacesDisplay.show();

        this.workspaces = this._workspacesDisplay.workspacesView;
        global.overlay_group.add_actor(this.workspaces.actor);

        if (!this._desktopFade.child)
            this._desktopFade.child = this._getDesktopClone();

        if (!this.workspaces.getActiveWorkspace().hasMaximizedWindows()) {
            this._desktopFade.opacity = 255;
            this._desktopFade.show();
            Tweener.addTween(this._desktopFade,
                             { opacity: 0,
                               time: ANIMATION_TIME,
                               transition: 'easeOutQuad'
                             });
        }

        this._group.opacity = 0;
        Tweener.addTween(this._group,
                         { opacity: 255,
                           transition: 'easeOutQuad',
                           time: ANIMATION_TIME,
                           onComplete: this._showDone,
                           onCompleteScope: this
                         });

        Tweener.addTween(this._background,
                         { dim_factor: 0.4,
                           time: ANIMATION_TIME,
                           transition: 'easeOutQuad'
                         });

        this._coverPane.raise_top();
        this._coverPane.show();
        this.emit('showing');
    },

    /**
     * Animates the overview visible without grabbing mouse and keyboard input;
     * if show() has already been called, this has no immediate effect, but
     * will result in the overview not being hidden until hideTemporarily() is
     * called.
     * 
     * For example the {@link Panel.ActivitiesButton} will use this to
     * show the overview if the user does an Xdnd drag over it (e.g. drag
     * a file from Nautilus over the Activities button will temporarily
     * show the overview so that the user can hover over a window and
     * drop onto it).
     *
     * This still goes through {@link #_animateVisible} so the
     * 'showing' and 'shown' signals will still be emitted.
     *
     * It should be reversed with {@link #hideTemporarily}.
     * @see Panel.ActivitiesButton#_xdndShowOverview
     * @see #hideTemporarily
     */
    showTemporarily: function() {
        if (this.isDummy)
            return;

        if (this._shownTemporarily)
            return;

        this._syncInputMode();
        this._animateVisible();
        this._shownTemporarily = true;
    },

    /** Reverses the effect of {@link #show}.
     * Calls {@link #_animateNotVisible} to fade everything out
     * with fancy animations.
     * @see #_animateNotVisible
     */
    hide: function() {
        if (this.isDummy)
            return;

        if (!this._shown)
            return;

        if (!this._shownTemporarily)
            this._animateNotVisible();

        this._shown = false;
        this._syncInputMode();

        if (this._buttonPressId > 0)
            this._group.disconnect(this._buttonPressId);
        this._buttonPressId = 0;
    },

    /** Reverses the effect of {@link #showTemporarily}.
     * @see #showTemporarily
     */
    hideTemporarily: function() {
        if (this.isDummy)
            return;

        if (!this._shownTemporarily)
            return;

        if (!this._shown)
            this._animateNotVisible();

        this._shownTemporarily = false;
        this._syncInputMode();
    },

    /** Toggles the overview (hides it if it's open, shows it if it's closed).
     */
    toggle: function() {
        if (this.isDummy)
            return;

        if (this._shown)
            this.hide();
        else
            this.show();
    },

    //// Private methods ////

    /** Not entire sure what this does (TODO).
     *
     * From the source:
     *
     * We delay input mode changes during animation so that when removing the
     * overview we don't have a problem with the release of a press/release
     * going to an application.
     */
    _syncInputMode: function() {
        // We delay input mode changes during animation so that when removing the
        // overview we don't have a problem with the release of a press/release
        // going to an application.
        if (this.animationInProgress)
            return;

        if (this._shown) {
            if (!this._modal) {
                if (Main.pushModal(this._group))
                    this._modal = true;
                else
                    this.hide();
            }
        } else if (this._shownTemporarily) {
            if (this._modal) {
                Main.popModal(this._group);
                this._modal = false;
            }
            global.stage_input_mode = Shell.StageInputMode.FULLSCREEN;
        } else {
            if (this._modal) {
                Main.popModal(this._group);
                this._modal = false;
            }
            else if (global.stage_input_mode == Shell.StageInputMode.FULLSCREEN)
                global.stage_input_mode = Shell.StageInputMode.NORMAL;
        }
    },

    /** Called from {@link #hide} and {@link #hideTemporarily}
     * to do fade out the overview with fancy animations.
     *
     * We fade out the background picture {@link #_background} and
     * the overview elements {@link #_group} over time {@link ANIMATION_TIME}.
     *
     * While the animation is happenin we raise {@link #_coverPane}
     * to the top in order to capture all click events and prevent them from
     * screwing up bits of the Overview.
     *
     * This emits the 'hiding' signal.
     *
     * Once animation is complete {@link #_hideDone} is called which
     * will emit the ['hidden']{@link .hidden} signal.
     *
     * @fires .hiding
     */
    _animateNotVisible: function() {
        if (!this.visible || this.animationInProgress)
            return;

        this.animationInProgress = true;
        this._hideInProgress = true;

        if (!this.workspaces.getActiveWorkspace().hasMaximizedWindows()) {
            this._desktopFade.opacity = 0;
            this._desktopFade.show();
            Tweener.addTween(this._desktopFade,
                             { opacity: 255,
                               time: ANIMATION_TIME,
                               transition: 'easeOutQuad' });
        }

        this.workspaces.hide();

        // Make other elements fade out.
        Tweener.addTween(this._group,
                         { opacity: 0,
                           transition: 'easeOutQuad',
                           time: ANIMATION_TIME,
                           onComplete: this._hideDone,
                           onCompleteScope: this
                         });

        Tweener.addTween(this._background,
                         { dim_factor: 1.0,
                           time: ANIMATION_TIME,
                           transition: 'easeOutQuad'
                         });

        this._coverPane.raise_top();
        this._coverPane.show();
        this.emit('hiding');
    },

    /** Called once we are finished fading in all the elements of the overview
     * upon showing it (i.e. when {@link #_animateVisible} is done).
     *
     * This emits the 'shown' signal, and removes {@link #_coverPane}
     * (which was previously intercepting all key press events).
     *
     * If there was a call to {@link #hide} or {@link #hideTemporarily}
     * while we were showing, we process is now (hiding the overview again).
     * @fires .shown
     */
    _showDone: function() {
        this.animationInProgress = false;
        this._desktopFade.hide();
        this._coverPane.hide();

        this.emit('shown');
        // Handle any calls to hide* while we were showing
        if (!this._shown && !this._shownTemporarily)
            this._animateNotVisible();

        this._syncInputMode();
        global.sync_pointer();
    },

    /** Called once we are finished fading out all the elements of the overview
     * upon hiding it (i.e. when {@link #_animateNotVisible} is done).
     *
     * This emits the 'hidden' signal when don, and removes {@link #_coverPane}
     * (which was previously intercepting all key press events).
     *
     * If there was a call to {@link #show} or {@link #showTemporarily}
     * while we were hiding, we process is now (showing the overview again).
     *
     * We also destroy the {@link #_workspacesDisplay} (it is created
     * fresh every time the overview is shown).
     * @fires .hidden
     */
    _hideDone: function() {
        // Re-enable unredirection
        Meta.enable_unredirect_for_screen(global.screen);

        global.window_group.show();

        this.workspaces.destroy();
        this.workspaces = null;

        this._workspacesDisplay.hide();

        this._desktopFade.hide();
        this._background.hide();
        this._group.hide();

        this.visible = false;
        this.animationInProgress = false;
        this._hideInProgress = false;

        this._coverPane.hide();

        this.emit('hidden');
        // Handle any calls to show* while we were hiding
        if (this._shown || this._shownTemporarily)
            this._animateVisible();

        this._syncInputMode();

        // Fake a pointer event if requested
        if (this._needsFakePointerEvent) {
            this._fakePointerEvent();
            this._needsFakePointerEvent = false;
        }
    }
};
Signals.addSignalMethods(Overview.prototype);
// Overview signals...
/** Emitted when the user left-button-presses on the overview (and the click is
 * not handled by any of the other actors in the overview).
 *
 * This is meant to signal the start of an attempt to "scroll" what the
 * currently-displayed workspace in the windows tab of the overview is (if
 * you make a "dragging up" motion on the overview you'll scroll down to the
 * next workspace, try it!)
 * @name swipe-scroll-begin
 * @memberof Overview
 * @event
 */

/** Emitted when the user releases the left button after pressing it on the overview
 * (where the click was not handled byany of the other actors in the overview).
 *
 * I.e., one of these is emitted after its corresponding
 * ['swipe-scroll-begin']{@link Overview.swipe-scroll-begin} signal has been
 * emitted to inform any listeners of the result of the swipe-scroll attempt.
 *
 * @name swipe-scroll-end
 * @memberof Overview
 * @event
 * @param {Overview.Overview} overview - the overview instance that emitted
 * the signal ({@link Main.overview}).
 * @param {Overview.SwipeScrollResult} result - result of the swipe-scroll
 * attempt. Either `CLICK` (it ended up being a left-click), `CANCEL` (we
 * started swiping but didn't swipe far enough to switch to the next
 * workspace), or `SWIPE` (a succesful swipe to the next workspace).
 */

/** Emitted upon {@link Overview#beginItemDrag}.
 *
 * Examples of classes using this signal: {@link ViewSelector.ViewSelector},
 * {@link Dash.Dash}, {@link WorkspacesView.WorkspacesDisplay}.
 * @event
 * @memberof Overview
 * @name item-drag-begin
 */
/** Emitted upon {@link Overview#cancelledItemDrag}.
 *
 * Examples of classes using this signal:
 * {@link Dash.Dash} and {@link WorkspacesView.WorkspacesDisplay}.
 * @event
 * @memberof Overview
 * @name item-drag-cancelled
 */
/** Emitted upon {@link Overview#endItemDrag}.
 *
 * Examples of classes using this signal:
 * {@link Dash.Dash} and {@link WorkspacesView.WorkspacesDisplay}.
 * @event
 * @memberof Overview
 * @name item-drag-end
 */


/** Emitted upon {@link Overview#beginWindowDrag}.
 *
 * Examples of classes using this signal: {@link ViewSelector.ViewSelector},
 * {@link Dash.Dash}, {@link WorkspacesView.WorkspacesDisplay}.
 * @event
 * @memberof Overview
 * @name window-drag-begin
 */
/** Emitted upon {@link Overview#cancelledWindowDrag}.
 *
 * Examples of classes using this signal:
 * {@link Dash.Dash} and {@link WorkspacesView.WorkspacesDisplay}.
 * @event
 * @memberof Overview
 * @name window-drag-cancelled
 */
/** Emitted upon {@link Overview#endWindowDrag}.
 *
 * Examples of classes using this signal:
 * {@link Dash.Dash} and {@link WorkspacesView.WorkspacesDisplay}.
 * @event
 * @memberof Overview
 * @name window-drag-end
 */

/** Emitted while the overview is showing.
 * @see Overview#_animateVisible
 * @event
 * @name showing
 * @memberof Overview
 */
/** Emitted while the overview is hiding.
 * @see Overview#_animateNotVisible
 * @event
 * @name hiding
 * @memberof Overview
 */
/** Emitted one the overview has finished showing.
 * @see Overview#_showDone
 * @event
 * @name shown
 * @memberof Overview
 */
/** Emitted one the overview has finished hiding.
 * @see Overview#_hideDone
 * @event
 * @name hidden
 * @memberof Overview
 */
