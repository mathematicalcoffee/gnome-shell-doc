// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This files defines the on-screen keyboard class.
 *
 * Provides a Keyboard object over DBus, interface 'org.gnome.Caribou.Keyboard'
 * (implemented by {@link Keyboard}).
 *
 * ![On-screen keyboard](pics/keyboard.png)
 *
 * @todo find API docs for Caribou
 */

const Caribou = imports.gi.Caribou;
const Clutter = imports.gi.Clutter;
const DBus = imports.dbus;
const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const BoxPointer = imports.ui.boxpointer;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;

/** @+
 * @type {string}
 * @const
 * @default
 */

/** GSettings schema for the on-screen keyboard configuration */
const KEYBOARD_SCHEMA = 'org.gnome.shell.keyboard';
/** Gsettings key for the keyboard type under {@link KEYBOARD_SCHEMA} */
const KEYBOARD_TYPE = 'keyboard-type';

/** Gsettins schema for a11y */
const A11Y_APPLICATIONS_SCHEMA = 'org.gnome.desktop.a11y.applications';
/** Gsettings key under {@link A11Y_APPLICATIONS_SCHEMA} for getting
 * screen keyboard enabled state. */
const SHOW_KEYBOARD = 'screen-keyboard-enabled';
/** @- */

// Key constants taken from Antler
// FIXME: ought to be moved into libcaribou
/** Object mapping key names to symbols/text that will be displayed for
 * that key on the on-screen keyboard.
 *
 * * Backspace: &#x232B; (U+232B)
 * * space: ' '
 * * Return: &23CE; (U+23CE)
 * * Caribou_Prefs: &2328; (U+2328)
 * * Caribou_ShiftUp: &2B06; (U+2B06)
 * * Caribou_ShiftDown: &2B07; (U+2B07)
 * * Caribou_Emoticons: &263A; (U+263A)
 * * Caribou_Symbols: '123'
 * * Caribou_Symbols_More: '{#*'
 * * Caribou_Alpha: 'Abc'
 * * Tab: 'Tab'
 * * Escape: 'Esc'
 * * Control_L: 'Ctrl'
 * * Alt_L: 'Alt'
 *
 * @type {Object.<string, string>}
 */
const PRETTY_KEYS = {
    'BackSpace': '\u232b',
    'space': ' ',
    'Return': '\u23ce',
    'Caribou_Prefs': '\u2328',
    'Caribou_ShiftUp': '\u2b06',
    'Caribou_ShiftDown': '\u2b07',
    'Caribou_Emoticons': '\u263a',
    'Caribou_Symbols': '123',
    'Caribou_Symbols_More': '{#*',
    'Caribou_Alpha': 'Abc',
    'Tab': 'Tab',
    'Escape': 'Esc',
    'Control_L': 'Ctrl',
    'Alt_L': 'Alt'
};

/** Definition of the 'org.gnome.Caribou.Keyboard' DBus interface. This is
 * implemented by the {@link Keyboard} object.
 * @todo find a link for the API/DBus docs?
 * @todo document functions as part of the Iface
 * @see Keyboard
 */
const CaribouKeyboardIface = {
    name: 'org.gnome.Caribou.Keyboard',
    methods:    [ { name: 'Show',
                    inSignature: 'u',
                    outSignature: ''
                  },
                  { name: 'Hide',
                    inSignature: 'u',
                    outSignature: ''
                  },
                  { name: 'SetCursorLocation',
                    inSignature: 'iiii',
                    outSignature: ''
                  },
                  { name: 'SetEntryLocation',
                    inSignature: 'iiii',
                    outSignature: ''
                  } ],
    properties: [ { name: 'Name',
                    signature: 's',
                    access: 'read' } ]
};

/** creates a new Key.
 * 
 * This creates the top-level actor using {@link #_makeKey}, essentially
 * a St.Button displaying the key's symbol (passed through {@link PRETTY_KEYS}
 * to convert (e.g.) "Return" into &23CE;).
 *
 * We also get all extended keys for this key (eg. '!' for 1) and if there are
 * any, we make a {@link BoxPointer.BoxPointer} for the key. The box pointer's
 * contents are a St.BoxLayout displaying all a St.Button for each extended
 * key (see the picture below).
 *
 * ![The extended keys popup thingy for the 'u' key ({@link #_boxPointer})](pics/KeyboardExtendedKeys.png)
 * 
 * @param {Caribou.KeyModel} key - key to make a {@link Key} for.
 * @classdesc
 * ![Some {@link Key}s](pics/Key.png)
 *
 * ![The extended keys popup thingy for the 'u' key ({@link #_boxPointer})](pics/KeyboardExtendedKeys.png)
 *
 * A {@link Key} is the graphical representation of a key in the
 * on-screen keyboard {@link Keyboard}.
 *
 * It's really just a St.Button with style 'keyboard-key'.
 *
 * If it's an extended key like '123' (bringing up a number pad), or say '1'
 * (which has '!' as an extended key), a {@link BoxPointer.BoxPointer} is created
 * for the key containing a new St.Button for each extra key (like the on-screen
 * keyboard for a smartphone). See the picture above.
 * @class
 */
function Key() {
    this._init.apply(this, arguments);
}

Key.prototype = {
    _init : function(key) {
        /** Underlying key for this key.
         * @type {Caribou.KeyModel} */
        this._key = key;

        /** Top-level actor.
         * A St.Button with style class 'keyboard-key' displaying the key's
         * label (passed through {@link PRETTY_KEYS})
         * @type {St.Button} */
        this.actor = this._makeKey();

        this._extended_keys = this._key.get_extended_keys();
        this._extended_keyboard = null;

        if (this._key.name == 'Control_L' || this._key.name == 'Alt_L')
            this._key.latch = true;

        this._key.connect('key-pressed', Lang.bind(this, function ()
                                                   { this.actor.checked = true }));
        this._key.connect('key-released', Lang.bind(this, function ()
                                                    { this.actor.checked = false; }));

        if (this._extended_keys.length > 0) {
            this._grabbed = false;
            this._eventCaptureId = 0;
            this._key.connect('notify::show-subkeys', Lang.bind(this, this._onShowSubkeysChanged));
            /** Box pointer for the extended keys popup. Its
             * [`.bin` actor]{@link BoxPointer.BoxPointer#bin} contains
             * {@link #_extended_keyboard}, the list of alternate keys.
             * @see #_getExtendedKeys
             * @see #_extended_keyboard
             * ![The extended keys popup thingy for the 'u' key ({@link #_boxPointer})](pics/KeyboardExtendedKeys.png)
             * @type {BoxPointer.BoxPointer */
            this._boxPointer = new BoxPointer.BoxPointer(St.Side.BOTTOM,
                                                         { x_fill: true,
                                                           y_fill: true,
                                                           x_align: St.Align.START });
            // Adds style to existing keyboard style to avoid repetition
            this._boxPointer.actor.add_style_class_name('keyboard-subkeys');
            this._getExtendedKeys();
            this.actor._extended_keys = this._extended_keyboard;
            this._boxPointer.actor.hide();
            Main.layoutManager.addChrome(this._boxPointer.actor, { visibleInFullscreen: true });
        }
    },

    /** Creates the St.Button for this key, connecting up the 'button-press-event'
     * and 'button-release-event' to pass through to {@link #_key}.
     *
     * The text for the button is [`PRETTY_KEYS[this._key.name]`]{@link PRETTY_KEYS},
     * or {@link #_getUnichar(this._key)} if the key is not in `PRETTY_KEYS`.
     * @returns {St.Button} the St.Button for this key. */
    _makeKey: function () {
        let label = this._key.name;

        if (label.length > 1) {
            let pretty = PRETTY_KEYS[label];
            if (pretty)
                label = pretty;
            else
                label = this._getUnichar(this._key);
        }

        label = GLib.markup_escape_text(label, -1);
        let button = new St.Button ({ label: label,
                                      style_class: 'keyboard-key' });

        button.key_width = this._key.width;
        button.connect('button-press-event', Lang.bind(this, function () { this._key.press(); }));
        button.connect('button-release-event', Lang.bind(this, function () { this._key.release(); }));

        return button;
    },

    /** Gets the character corresponding to `key`.
     * @param {Caribou.KeyModel} key - key to get the character for.
     * @returns {string} the character for that key.
     */
    _getUnichar: function(key) {
        let keyval = key.keyval;
        let unichar = Gdk.keyval_to_unicode(keyval);
        if (unichar) {
            return String.fromCharCode(unichar);
        } else {
            return key.name;
        }
    },

    /** Gets the extended keys for this key and creates a menu with a St.Button
     * for each of them, saving this in {@link #_extended_keyboard} and placing
     * it in {@link #_boxPointer}.
     *
     * {@link #_extended_keyboard} is a horizontal St.BoxLayout containing
     * a St.Button for each extended key for this key.
     *
     * ![The extended keys popup thingy for the 'u' key ({@link #_boxPointer})](pics/KeyboardExtendedKeys.png)
     */
    _getExtendedKeys: function () {
        /** The container for all the extended keys to go in. Placed in
         * {@link #_boxPointer} to create a popup menu with extended keys.
         * Horizontal, style class 'keyboard-layout'.
         * @see #_boxPointer
         * @see #_getExtendedKey
         * @type {St.BoxLayout} */
        this._extended_keyboard = new St.BoxLayout({ style_class: 'keyboard-layout',
                                                     vertical: false });
        for (let i = 0; i < this._extended_keys.length; ++i) {
            let extended_key = this._extended_keys[i];
            let label = this._getUnichar(extended_key);
            let key = new St.Button({ label: label, style_class: 'keyboard-key' });
            key.extended_key = extended_key;
            key.connect('button-press-event', Lang.bind(this, function () { extended_key.press(); }));
            key.connect('button-release-event', Lang.bind(this, function () { extended_key.release(); }));
            this._extended_keyboard.add(key);
        }
        this._boxPointer.bin.add_actor(this._extended_keyboard);
    },

    /** Callback when any event is captured while the extended keyboard for this
     * key is showing ({@link #_boxPointer}, {@link #_extended_keyboard}).
     *
     * If the event was a button press or button release of a key in the extended
     * keyboard, then the press/release is passed through to the underlying key.
     *
     * Otherwise if it was a button press but of a key *not* in the extended
     * keyboard, the extended keyboard is hidden.
     */
    _onEventCapture: function (actor, event) {
        let source = event.get_source();
        let type = event.type();

        if ((type == Clutter.EventType.BUTTON_PRESS ||
             type == Clutter.EventType.BUTTON_RELEASE) &&
            this._extended_keyboard.contains(source)) {
            source.extended_key.press();
            source.extended_key.release();
            return false;
        }
        if (type == Clutter.EventType.BUTTON_PRESS) {
            this._boxPointer.actor.hide();
            this._ungrab();
            return true;
        }
        return false;
    },

    /** Ungrabs focus from the extended keyboard {@link #_extended_keyboard}.
     * Calls {@link Main.popModal} and disconnects the event handler
     * {@link #_onEventCapture}. */
    _ungrab: function () {
        global.stage.disconnect(this._eventCaptureId);
        this._eventCaptureId = 0;
        this._grabbed = false;
        Main.popModal(this.actor);
    },

    /** Callback for the 'notify::show-keys' signal of {@link #_key}, i.e. when
     * the extended keyboard should be shown.
     *
     * If this is set to `true`,
     * the extended keyboard {@link #_boxPointer} is shown by adding it to
     * the modal stack, and we listen to any captured event with {@link #_onEventCapture}.
     *
     * If it is set to `false` the extended keyboard is hidden and the grab
     * is released ({@link #_ungrab}).
     */
    _onShowSubkeysChanged: function () {
        if (this._key.show_subkeys) {
            this.actor.fake_release();
            this._boxPointer.actor.raise_top();
            this._boxPointer.setPosition(this.actor, 0.5);
            this._boxPointer.show(true);
            this.actor.set_hover(false);
            if (!this._grabbed) {
                 Main.pushModal(this.actor);
                 this._eventCaptureId = global.stage.connect('captured-event', Lang.bind(this, this._onEventCapture));
                 this._grabbed = true;
            }
            this._key.release();
        } else {
            if (this._grabbed)
                this._ungrab();
            this._boxPointer.hide(true);
        }
    }
};

/** creates a new Keyboard
 *
 * This exports thsi instance as DBus object '/org/gnome/Caribou/Keyboard'
 * and initialises settings {@link KEYBOARD_SCHEMA} and {@link A11Y_APPLICATIONS_SCHEMA}
 * in order to listen for settings changes (such as enabling/disabling the
 * on-screen keyboard).
 *
 * The top-level actor is a vertical St.BoxLayout. We add keys in a row-wise
 * fashion.
 * @classdesc
 * ![On-screen keyboard {@link Keyboard}](pics/keyboard.png)
 *
 * This provides DBus object '/org/gnome/Caribou/Keyboard'.
 * Only one instance is necessary and it is stored in {@link Main.keyboard}.
 *
 * It is the on-screen keyboard.
 *
 * It also has an associated {@link KeyboardSource} that sits in the message
 * tray whenever the keyboard is hdiden so the user can launch the keyboard by
 * clicking on it.
 *
 * Internally we wrap around a {@link Caribou.KeyboardModel}
 * which tells us which buttons to show and contains information about each
 * key.
 * @see KeyboardIface
 * @class
 */
function Keyboard() {
    this._init.apply(this, arguments);
}

Keyboard.prototype = {
    _init: function () {
        DBus.session.exportObject('/org/gnome/Caribou/Keyboard', this);

        this.actor = null;

        this._timestamp = global.get_current_time();
        Main.layoutManager.connect('monitors-changed', Lang.bind(this, this._redraw));

        this._keyboardSettings = new Gio.Settings({ schema: KEYBOARD_SCHEMA });
        this._keyboardSettings.connect('changed', Lang.bind(this, this._settingsChanged));
        this._a11yApplicationsSettings = new Gio.Settings({ schema: A11Y_APPLICATIONS_SCHEMA });
        this._a11yApplicationsSettings.connect('changed', Lang.bind(this, this._settingsChanged));
        this._settingsChanged();
    },

    /** Initialises the keyboard by calling {@link #_redraw}. */
    init: function () {
        this._redraw();
    },

    /** Callback when any of the settings in {@link KEYBOARD_SCHEMA} or
     * {@link A11Y_APPLICATIONS_SCHEMA} changes.
     *
     * If the user has changed {@link SHOW_KEYBOARD} then the on-screen
     * keyboard is hidden or shown as necessary.
     *
     * If the user has changed {@link KEYBOARD_TYPE} then the keyboard is redrawn.
     * @param {Gio.Settings} settings - the Gio.Settings instance that emitted
     * the signal.
     * @param {string} key - the name of the changed key.
     * @see #_setupKeyboard
     */
    _settingsChanged: function (settings, key) {
        this._enableKeyboard = this._a11yApplicationsSettings.get_boolean(SHOW_KEYBOARD);
        if (!this._enableKeyboard && !this._keyboard)
            return;
        if (this._enableKeyboard && this._keyboard &&
            this._keyboard.keyboard_type == this._keyboardSettings.get_string(KEYBOARD_TYPE))
            return;

        if (this._keyboard)
            this._destroyKeyboard();

        if (this._enableKeyboard) {
            // If we've been called because the setting actually just
            // changed to true (as opposed to being called from
            // this._init()), then we want to pop up the keyboard.
            let showKeyboard = (settings != null);

            // However, caribou-gtk-module or this._onKeyFocusChanged
            // will probably immediately tell us to hide it, so we
            // have to fake things out so we'll ignore that request.
            if (showKeyboard)
                this._timestamp = global.display.get_current_time_roundtrip() + 1;
            this._setupKeyboard(showKeyboard);
        } else
            Main.layoutManager.hideKeyboard(true);
    },

    /** Destroys the UI parts of the keyboard (so that it can be recreated again
     * with {@link #_setupKeyboard}). */
    _destroyKeyboard: function() {
        if (this._keyboardNotifyId)
            this._keyboard.disconnect(this._keyboardNotifyId);
        if (this._focusNotifyId)
            global.stage.disconnect(this._focusNotifyId);
        this._keyboard = null;
        this.actor.destroy();
        this.actor = null;

        this._destroySource();
    },

    /** Sets up the UI parts of the keyboard.
     *
     * Creates the top-level actor {@link #actor} and adds it to the
     * layout manager ({@link LayoutManager.LayoutManager.keyboardBox}).
     *
     * We create a new {@link Caribou.KeyboardModel}
     * which contains the information about what keys we should display
     * ({@link #_keyboard}).
     *
     * Then {@link #_addKeys} is called to add the relevant keys to
     * the keyboard.
     *
     * If `show` is true we show the keyboard ({@link #show}), otherwise
     * we create a {@link KeyboardSource} to sit in the message tray allowing
     * the user to open the keyboard.
     *
     * @param {boolean} show - whether to show the keyboard after creating it.
     */
    _setupKeyboard: function(show) {
        /** Top-level actor for the keyboard. It is a vertical St.BoxLayout.
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ name: 'keyboard', vertical: true, reactive: true });
        Main.layoutManager.keyboardBox.add_actor(this.actor);
        Main.layoutManager.trackChrome(this.actor);

        /** This class wraps around a Caribou.KeyboardModel
         * @type {Caribou.KeyboardModel} */
        this._keyboard = new Caribou.KeyboardModel({ keyboard_type: this._keyboardSettings.get_string(KEYBOARD_TYPE) });
        this._groups = {};
        this._current_page = null;

        // Initialize keyboard key measurements
        this._numOfHorizKeys = 0;
        this._numOfVertKeys = 0;

        this._addKeys();

        this._keyboardNotifyId = this._keyboard.connect('notify::active-group', Lang.bind(this, this._onGroupChanged));
        this._focusNotifyId = global.stage.connect('notify::key-focus', Lang.bind(this, this._onKeyFocusChanged));

        if (show)
            this.show();
        else
            this._createSource();
    },

    /** Callback whenever keyboard focus changes for the global stage.
     * If focus is given to a `Clutter.Text`, we show the on-screen keyboard.
     * Otherwise, we hide it.
     * @see #Show
     * @see #Hide
     */
    _onKeyFocusChanged: function () {
        let focus = global.stage.key_focus;

        // Showing an extended key popup and clicking a key from the extended keys
        // will grab focus, but ignore that
        if (focus && (focus._extended_keys || (focus._key && focus._key.extended_key)))
            return;

        let time = global.current_event_time();
        if (focus instanceof Clutter.Text)
            this.Show(time);
        else
            this.Hide(time);
    },

    /** Adds keys ot the keyboard.
     *
     * We basically drill down into {@link #_keyboard} to retrieve
     * {@link Caribou.KeyModel}s for each key that should be displayed and
     * create a {@link Key} for each.
     *
     * To do this we add keys row by row with {@link #_loadRows}.
     * @see #_loadRows
     */
    _addKeys: function () {
        let groups = this._keyboard.get_groups();
        for (let i = 0; i < groups.length; ++i) {
             let gname = groups[i];
             let group = this._keyboard.get_group(gname);
             group.connect('notify::active-level', Lang.bind(this, this._onLevelChanged));
             let layers = {};
             let levels = group.get_levels();
             for (let j = 0; j < levels.length; ++j) {
                 let lname = levels[j];
                 let level = group.get_level(lname);
                 let layout = new St.BoxLayout({ style_class: 'keyboard-layout',
                                                 vertical: true });
                 this._loadRows(level, layout);
                 layers[lname] = layout;
                 this.actor.add(layout, { x_fill: false });

                 layout.hide();
             }
             this._groups[gname] = layers;
        }

        this._setActiveLayer();
    },

    /** Creates a `St.Button` that will show or hide the Message Tray.
     * Added to the keyboard. It is greyed out if the overview
     * is showing (the tray is always on in the overview).
     * @returns {St.Button} button with label "tray" that opens or closes
     * the message tray.
     */
    _getTrayIcon: function () {
        let trayButton = new St.Button ({ label: _("tray"),
                                          style_class: 'keyboard-key' });
        trayButton.key_width = 1;
        trayButton.connect('button-press-event', Lang.bind(this, function () {
            Main.messageTray.toggle();
        }));

        Main.overview.connect('showing', Lang.bind(this, function () {
            trayButton.reactive = false;
            trayButton.add_style_pseudo_class('grayed');
        }));
        Main.overview.connect('hiding', Lang.bind(this, function () {
            trayButton.reactive = true;
            trayButton.remove_style_pseudo_class('grayed');
        }));

        return trayButton;
    },

    /** Creates a {@link Key} for each key in each row of `keys` and adds the into
     * `layout`.
     *
     * For each element in `keys` (one per row), we call `.get_children()` to
     * get the keys in that row.
     *
     * We then create a {@link Key} for each key and add it to the
     * row.
     *
     * Finaly we add each row to `layout`.
     * @param {Caribou.ColumnModel} keys - result of `row.get_columns()`.
     * These rows will be added in to the keyboard (??)
     * element to get the Caribou.KeyModel for each key.
     * @inheritparams #_loadRows
     */
    _addRows : function (keys, layout) {
        let keyboard_row = new St.BoxLayout();
        for (let i = 0; i < keys.length; ++i) {
            let children = keys[i].get_children();
            let right_box = new St.BoxLayout({ style_class: 'keyboard-row' });
            let left_box = new St.BoxLayout({ style_class: 'keyboard-row' });
            for (let j = 0; j < children.length; ++j) {
                if (this._numOfHorizKeys == 0)
                    this._numOfHorizKeys = children.length;
                let key = children[j];
                let button = new Key(key);

                if (key.align == 'right')
                    right_box.add(button.actor);
                else
                    left_box.add(button.actor);
                if (key.name == 'Caribou_Prefs') {
                    key.connect('key-released', Lang.bind(this, this.hide));

                    // Add new key for hiding message tray
                    right_box.add(this._getTrayIcon());
                }
            }
            keyboard_row.add(left_box, { expand: true, x_fill: false, x_align: St.Align.START });
            keyboard_row.add(right_box, { expand: true, x_fill: false, x_align: St.Align.END });
        }
        layout.add(keyboard_row);
    },

    /** Loads a row of a keyboard layout, calling {@link #_addRows}
     * to actually make and add a {@link Key} for each key in the row.
     * @param {Caribou.LevelBody} level - the level whose rows we wish to add.
     * @param {St.BoxLayout} layout - St.BoxLayout (vertical) to add the
     * rows of {@link Key}s into.
     */
    _loadRows : function (level, layout) {
        let rows = level.get_rows();
        for (let i = 0; i < rows.length; ++i) {
            let row = rows[i];
            if (this._numOfVertKeys == 0)
                this._numOfVertKeys = rows.length;
            this._addRows(row.get_columns(), layout);
        }

    },

    /** Redraws the keyboard. Triggered from {@link #init} and also
     * whenever the monitor layout changes.
     *
     * This doesn't reset the keyboard as a result of a keyboard layout/settings change;
     * for that see {@link #_destroyKeyboard} and {@link #_setupKeyboard}.
     *
     * What it *does* do is keep the *current* keyboard layout and:
     *
     * * Ensure that the height of the keyboard is one third of the bottom
     * monitor's height
     * * the width of the keyboard is the bottom monitor's width
     * * the size of each key is set properly according to the 'spacing'
     * and 'padding' style properties and the key's base width (for example
     * the Enter key is wider than the letter keys).
     */
    _redraw: function () {
        if (!this._enableKeyboard)
            return;

        let monitor = Main.layoutManager.bottomMonitor;
        let maxHeight = monitor.height / 3;
        this.actor.width = monitor.width;

        let layout = this._current_page;
        let verticalSpacing = layout.get_theme_node().get_length('spacing');
        let padding = layout.get_theme_node().get_length('padding');

        let box = layout.get_children()[0].get_children()[0];
        let horizontalSpacing = box.get_theme_node().get_length('spacing');
        let allHorizontalSpacing = (this._numOfHorizKeys - 1) * horizontalSpacing;
        let keyWidth = Math.floor((this.actor.width - allHorizontalSpacing - 2 * padding) / this._numOfHorizKeys);

        let allVerticalSpacing = (this._numOfVertKeys - 1) * verticalSpacing;
        let keyHeight = Math.floor((maxHeight - allVerticalSpacing - 2 * padding) / this._numOfVertKeys);

        let keySize = Math.min(keyWidth, keyHeight);
        this.actor.height = keySize * this._numOfVertKeys + allVerticalSpacing + 2 * padding;

        let rows = this._current_page.get_children();
        for (let i = 0; i < rows.length; ++i) {
            let keyboard_row = rows[i];
            let boxes = keyboard_row.get_children();
            for (let j = 0; j < boxes.length; ++j) {
                let keys = boxes[j].get_children();
                for (let k = 0; k < keys.length; ++k) {
                    let child = keys[k];
                    child.width = keySize * child.key_width;
                    child.height = keySize;
                    if (child._extended_keys) {
                        let extended_keys = child._extended_keys.get_children();
                        for (let n = 0; n < extended_keys.length; ++n) {
                            let extended_key = extended_keys[n];
                            extended_key.width = keySize;
                            extended_key.height = keySize;
                        }
                    }
                }
            }
        }
    },

    /** Callback when the active Caribou level changes (I think this is what
     * set of symbols is currently being shown, for example the '123' button
     * shows numbers, the 'Abc' button switches to showing letters).
     *
     * Redraws the keyboard to match. */
    _onLevelChanged: function () {
        this._setActiveLayer();
        this._redraw();
    },

    /** Callback when the active Caribou group changes. (What's a Caribou
     * group? My keyboard just has group 'us', so maybe it's the keyboard
     * layout?)
     *
     * Redraws the keyboard to match. */
    _onGroupChanged: function () {
        this._setActiveLayer();
        this._redraw();
    },

    /** Sets the active layer of the keyboard. For example pressing the '123'
     * button switches the current "layer" of the keyboard from showing letters
     * to showing numbers (this corresponds to a Caribou.Level I think). */
    _setActiveLayer: function () {
        let active_group_name = this._keyboard.active_group;
        let active_group = this._keyboard.get_group(active_group_name);
        let active_level = active_group.active_level;
        let layers = this._groups[active_group_name];

        if (this._current_page != null) {
            this._current_page.hide();
        }

        this._current_page = layers[active_level];
        this._current_page.show();
    },

    /** Creates a {@link KeyboardSource} for this instance if it doesn't already
     * exist. Adds it to the message tray and makes it transient.
     * @see MessageTray.MessageTray#add */
    _createSource: function () {
        if (this._source == null) {
            this._source = new KeyboardSource(this);
            this._source.setTransient(true);
            Main.messageTray.add(this._source);
        }
    },

    /** Destroys this instance's {@link KeyboardSource}. */
    _destroySource: function () {
        if (this._source) {
            this._source.destroy();
            this._source = null;
        }
    },

    /** Shows the keyboard using the layout manager and destroys the
     * {@link KeyboardSource} from the message tray.
     * @see LayoutManager.LayoutManager#showKeyboard
     * @see #_destroySource
     * @see #Show */
    show: function () {
        this._redraw();

        Main.layoutManager.showKeyboard();
        this._destroySource();
    },

    /** Hides the keyboard using the layout manager and creates a
     * {@link KeyboardSource} for the message tray (clicking on it
     * will show the keyboard)
     * @see #Hide
     * @see LayoutManager.LayoutManager#hideKeyboard
     * @see #_createSource
     */
    hide: function () {
        Main.layoutManager.hideKeyboard();
        this._createSource();
    },

    /** Moves the current window vertically (perhaps to avoid the
     * keyboard?)
     *
     * Called from {@link #_setLocation} which is not used...
     * @deprecated */
    _moveTemporarily: function () {
        let currentWindow = global.screen.get_display().focus_window;
        let rect = currentWindow.get_outer_rect();

        let newX = rect.x;
        let newY = 3 * this.actor.height / 2;
        currentWindow.move_frame(true, newX, newY);
    },

    /** If `y` is greater than twice the keyboard's height, the current
     * window is moved upwards to 1.5 times the keyboard's height (I think
     * this is to try make the window dodge the keyboard).
     *
     * Note - as far as I can tell this is not used.
     * @param {number} x - x coordinate
     * @param {number} y - y coordinate
     * @deprecated
     */
    _setLocation: function (x, y) {
        if (y >= 2 * this.actor.height)
            this._moveTemporarily();
    },

    // D-Bus methods
    /** Implements DBus method 'Show'. Shows the keyboard (if it is enabled
     * in the a11y settings) and if the timestamp is greater than the last time
     * this was called.
     * @param {number} timestamp - timestamp.
     * @see #show
     */
    Show: function(timestamp) {
        if (!this._enableKeyboard)
            return;

        if (timestamp - this._timestamp < 0)
            return;

        this._timestamp = timestamp;
        this.show();
    },

    /** Implements DBus method 'Hide'. Hides the keyboard (if it is enabled
     * in the a11y settings) and if the timestamp is greater than the last time
     * this was called.
     * @param {number} timestamp - timestamp.
     * @see #hide
     */
    Hide: function(timestamp) {
        if (!this._enableKeyboard)
            return;

        if (timestamp - this._timestamp < 0)
            return;

        this._timestamp = timestamp;
        this.hide();
    },

    /** DBus method 'SetCursorLocation'. Does nothing (GNOME 3.2). */
    SetCursorLocation: function(x, y, w, h) {
        if (!this._enableKeyboard)
            return;

//        this._setLocation(x, y);
    },

    /** DBus method 'SetEntryLocation'. Does nothing (GNOME 3.2). */
    SetEntryLocation: function(x, y, w, h) {
        if (!this._enableKeyboard)
            return;

//        this._setLocation(x, y);
    },

    /** DBus property 'Name'. Returns 'gnome-shell'.
     * @returns {string} 'gnome-shell'.
     */
    get Name() {
        return 'gnome-shell';
    }
};
DBus.conformExport(Keyboard.prototype, CaribouKeyboardIface);

/** creates a new KeyboardSource.
 *
 * Calls the [parent constructor]{@link MessageTray.Source} with
 * title "Keyboard" and uses {@link #_setSummaryIcon} to set
 * the icon of the source.
 * @param {Keyboard.Keyboard} keyboard - {@link Keyboard} instance that the
 * source is tied to.
 * @classdesc
 * ![KeyboardSource](pics/keyboardSource.png)
 *
 * This implements {@link MessageTray.Source} for the on-screen keyboard.
 *
 * Basically one of these sits in the Message Tray whenever the on-screen
 * keyboard is not showing, and clicking on it will remove the source and
 * show the keyboard. No notifications ever get added to this source, it's
 * really just meant to be a button in the tray.
 *
 * We implement {@link MessageTray.Source#createNotificationIcon} to show
 * the symbolic 'input-keyboard' icon.
 * @class
 * @extends MessageTray.Source
 */
function KeyboardSource() {
    this._init.apply(this, arguments);
}

KeyboardSource.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function(keyboard) {
        this._keyboard = keyboard;
        MessageTray.Source.prototype._init.call(this, _("Keyboard"));

        this._setSummaryIcon(this.createNotificationIcon());
    },

    /** Implements the parent function to provide an icon for the Source,
     * being the 'input-keyboard' icon (symbolic).
     * @override */
    createNotificationIcon: function() {
        return new St.Icon({ icon_name: 'input-keyboard',
                             icon_type: St.IconType.SYMBOLIC,
                             icon_size: this.ICON_SIZE });
    },

     /** Overrides the parent function to "steal" clicks from the summary item;
      * when this source's summary item is clicked, {@link #open}
      * is called to open the on-screen keyboard. Otherwise, the click is
      * passed through to the summary item.
      * @override */
     handleSummaryClick: function() {
        let event = Clutter.get_current_event();
        if (event.type() != Clutter.EventType.BUTTON_RELEASE)
            return false;

        this.open();
        return true;
    },

    /** Opens the on-screen keyboard.
     * @see Keyboard#show */
    open: function() {
        this._keyboard.show();
    }
};
