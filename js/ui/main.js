// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This is what defines most of the global object instances in GNOME shell and
 * sets everything up - the top panel, overview, ...
 *
 * It also defines a bunch of convenience functions.
 *
 * The function that initialises everything is {@link Main.start}, and either
 * sets up the gnome-shell user session, or starts a GDM session (the login
 * screen).
 *
 * Utility functions:
 *
 * * {@link getWindowActorsForWorkspace}
 * * {@link activateWindow}
 * * {@link isWindowActorDisplayedOnWorkspace}
 * * {@link notify}
 *
 * UI-type functions:
 *
 * * {@link initializeDeferredWork}
 * * {@link queueDeferredWork}
 * * {@link popModal}
 * * {@link pushModal}
 *
 * Functions for CSS themes:
 *
 * * {@link setThemeStylesheet}
 * * {@link getThemeStylesheet}
 * * {@link loadTheme}
 *
 * Logging/error functions:
 *
 * * {@link _logError} (accessible globally as `logError`)
 * * {@link _logDebug} (accessible globally as `log`)
 * * {@link notifyError}
 * * {@link notify}
 *
 * Functions for implementing dynamic workspaces (i.e. user always has *exactly* one
 * empty workspace at the end of all of their workspaces).
 *
 * * {@link _checkWorkspaces}
 * * {@link _queueCheckWorkspaces}
 * * {@link _windowLeftMonitor}
 * * {@link _windowEnteredMonitor}
 * * {@link _windowRemoved}
 * * {@link _windowsRestacked}
 * * {@link _nWorkspacesChanged}
 */

const Clutter = imports.gi.Clutter;
const DBus = imports.dbus;
const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GConf = imports.gi.GConf;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const AutomountManager = imports.ui.automountManager;
const AutorunManager = imports.ui.autorunManager;
const CtrlAltTab = imports.ui.ctrlAltTab;
const EndSessionDialog = imports.ui.endSessionDialog;
const PolkitAuthenticationAgent = imports.ui.polkitAuthenticationAgent;
const Environment = imports.ui.environment;
const ExtensionSystem = imports.ui.extensionSystem;
const Keyboard = imports.ui.keyboard;
const MessageTray = imports.ui.messageTray;
const Overview = imports.ui.overview;
const Panel = imports.ui.panel;
const PlaceDisplay = imports.ui.placeDisplay;
const RunDialog = imports.ui.runDialog;
const Layout = imports.ui.layout;
const LookingGlass = imports.ui.lookingGlass;
const NetworkAgent = imports.ui.networkAgent;
const NotificationDaemon = imports.ui.notificationDaemon;
const WindowAttentionHandler = imports.ui.windowAttentionHandler;
const Scripting = imports.ui.scripting;
const ShellDBus = imports.ui.shellDBus;
const TelepathyClient = imports.ui.telepathyClient;
const WindowManager = imports.ui.windowManager;
const Magnifier = imports.ui.magnifier;
const XdndHandler = imports.ui.xdndHandler;
const StatusIconDispatcher = imports.ui.statusIconDispatcher;
const Util = imports.misc.util;

/** Default background colour for the global.stage, #2266bb (a weirdish dirty
 * blue).
 * @type {Clutter.Color}
 * @const */
const DEFAULT_BACKGROUND_COLOR = new Clutter.Color();
DEFAULT_BACKGROUND_COLOR.from_pixel(0x2266bbff);

/** Global automount manager instance (listens to a Gio.VolumeMonitor to
 * automount devices with an interactive {@link ShellMountOperation.ShellMountOperation}).
 * @type {AutomountManager.AutomountManager} */
let automountManager = null;
/** Global AutorunManager instance.
 * @type {AutorunManager.AutorunManager} */
let autorunManager = null;
/** The top panel.
 * @type {Panel.Panel} */
let panel = null;
// unused
let hotCorners = [];
/** Global placesManager instance, keeping track of bookmarked places and drives
 * creating a {@link PlacesDisplay.PlaceInfo} for each.
 * @type {PlacesDisplay.PlacesManager} */
let placesManager = null;
/** The Overview.
 * @type {Overview.Overview} */
let overview = null;
/** The Run Dialog.
 * @type {RunDialog.RunDialog} */
let runDialog = null;
/** The Looking Glass
 * @type {LookingGlass.LookingGlass} */
let lookingGlass = null;
/** Global window manager instance (implements fancy animations).
 * @type {WindowManager.WindowManager} */
let wm = null;
/** The Message Tray.
 * @type {MessageTray.MessageTray} */
let messageTray = null;
/** Global notificationDaemon instance.
 * @type {NotificationDaemon.NotificationDaemon} */
let notificationDaemon = null;
/** Global windowAttentionHandler instance (makes notifications for windows
 * requesting attention).
 * @type {WindowAttentionHandler.WindowAttentionHandler} */
let windowAttentionHandler = null;
/** Global TelepathyClient instance.
 * @type {TelepathyClient.TelepathyClient} */
let telepathyClient = null;
/** Global ctrlAltTabManager instance (for launching the ctrl alt tab dialog
 * and adding accessible items to).
 * @type {CtrlAltTab.CtrlAltTabManager} */
let ctrlAltTabManager = null;
/** Global recorder instance (for recording screencasts).
 * @see _initRecorder
 * @type {Shell.Recorder} */
let recorder = null;
/** Global shellDBusService instance, providing service 'org.gnome.Shell' at
 * '/org/gnome/Shell'.
 * @type {shellDBusService.GnomeShell} */
let shellDBusService = null;

// to do with pushmodal/pop modal
let modalCount = 0;
let modalActorFocusStack = [];

// TODO
/** Global uiGroup.
 * @type {Shell.GenericContainer} */
let uiGroup = null;

/** Global magnifier instance providing the magnifier service
 * {@link MagnifierDBus.shellMagnifier} at '/org/gnome/Shell'.
 * @type {Magnifier.Magnifier} */
let magnifier = null;
/** Global xdndHandler instance for tracking XDND.
 * @type {XdndHandler.XdndHandler} */
let xdndHandler = null;
/** Global statusIconDispatcher instance to make sure that tray
 * applets get hidden if they have a corresponding status indicator.
 * @type {StatusIconDispatcher.StatusIconDispatcher} */
let statusIconDispatcher = null;
/** Global keyboard instance providing on-screen keyboard
 * '/org/gnome/Caribou/Keyboard'
 * @type {Keyboard.Keyboard} */
let keyboard = null;
/** The global LayoutManager.
 * @type {Layout.LayoutManager} */
let layoutManager = null;
/** Global networkAgent instance that listens for network password requests
 * and passes them on.
 * @type {networkAgent.networkAgent} */
let networkAgent = null;
let _errorLogStack = [];
let _startDate;

/** The location of the default CSS style sheet. Probably '/usr/share/gnome-shell/theme/gnome-shell.css'.
 * @type {string} */
let _defaultCssStylesheet = null;
/** Location of the gnome-shell style sheet if we are not using the default one.
 * (e.g. `$HOME/.themes/themename/gnome-shell/gnome-shell.css`). `null` means
 * we are using the default style sheet.
 * @type {?string}
 */
let _cssStylesheet = null;
let _gdmCssStylesheet = null;

// unused
let background = null;

/** Creates a user session by loading the calendar server,
 * and initialising the global places manager ({@link placesManager}),
 * telepathy client ({@link telepathyClient}), automount manager
 * ({@link automountManager}), autorun manager ({@link autorunManager}) and
 * network agent ({@link networkAgent).
 *
 * Called from {@link Main.start}.
 * @see _initUserSession
 */
function _createUserSession() {
    // Load the calendar server. Note that we are careful about
    // not loading any events until the user presses the clock
    global.launch_calendar_server();

    placesManager = new PlaceDisplay.PlacesManager();
    telepathyClient = new TelepathyClient.Client();
    automountManager = new AutomountManager.AutomountManager();
    autorunManager = new AutorunManager.AutorunManager();
    networkAgent = new NetworkAgent.NetworkAgent();
}

/** Creates a GDM session by creating a new {@link LoginDialog.LoginDialog},
 * opening it once it emits its ['loaded']{@link LoginDialog.LoginDialog.loaded}
 * signal.
 */
function _createGDMSession() {
    // We do this this here instead of at the top to prevent GDM
    // related code from getting loaded in normal user sessions
    const LoginDialog = imports.gdm.loginDialog;

    let loginDialog = new LoginDialog.LoginDialog();
    loginDialog.connect('loaded', function() {
                            loginDialog.open();
                        });
}

/** Initialises the gnome-shell recorder - pressing Ctrl+Alt+Shift+R will record
 * a screencast configure where it saves and what it saves as in
 * gsettings schema 'org.gnome.shell.recorder'.
 *
 * Really this just creates a new {@link Shell.Recorder}
 * and stores it into {@link recorder}.
 */
function _initRecorder() {
    let recorderSettings = new Gio.Settings({ schema: 'org.gnome.shell.recorder' });

    global.screen.connect('toggle-recording', function() {
        if (recorder == null) {
            recorder = new Shell.Recorder({ stage: global.stage });
        }

        if (recorder.is_recording()) {
            recorder.pause();
            Meta.enable_unredirect_for_screen(global.screen);
        } else {
            // read the parameters from GSettings always in case they have changed
            recorder.set_framerate(recorderSettings.get_int('framerate'));
            recorder.set_filename('shell-%d%u-%c.' + recorderSettings.get_string('file-extension'));
            let pipeline = recorderSettings.get_string('pipeline');

            if (!pipeline.match(/^\s*$/))
                recorder.set_pipeline(pipeline);
            else
                recorder.set_pipeline(null);

            Meta.disable_unredirect_for_screen(global.screen);
            recorder.record();
        }
    });
}

/** Initialises a user session by:
 *
 * * calling {@link _initRecorder} to initialise the screen recorder;
 * * setting workspace layout to be in columns;
 * * initialising the extension sysetem and loading extensions
 * ({@link ExtensionSystem.init}, {@link ExtensionSystem.loadExtensions});
 * * overriding keybindings 'panel_run_dialog' and 'panel_main_menu' to
 *   show the [run dialog]{@link getRunDialog} and the {@link overview} respectively;
 * * listening to `global.display`'s 'overlay-key' signal to toggle the overview.
 * @see _createUserSession
 */
function _initUserSession() {
    _initRecorder();

    global.screen.override_workspace_layout(Meta.ScreenCorner.TOPLEFT, false, -1, 1);

    ExtensionSystem.init();
    ExtensionSystem.loadExtensions();

    let shellwm = global.window_manager;

    shellwm.takeover_keybinding('panel_run_dialog');
    shellwm.connect('keybinding::panel_run_dialog', function () {
       getRunDialog().open();
    });

    shellwm.takeover_keybinding('panel_main_menu');
    shellwm.connect('keybinding::panel_main_menu', function () {
        overview.toggle();
    });

    global.display.connect('overlay-key', Lang.bind(overview, overview.toggle));

}

/** This is the core function to start gnome-shell.
 *
 * This is what it does (well not all things listed, see the source code to
 * see for yourself):
 *
 * * alias {@link _logError} as `global.logError`, i.e. can be accessed as
 * `logError` with no namespace;
 * * alias {@link _log} as `global.log`, i.e. can be accessed as
 * `log` with no namespace;
 * * chains up async errors reported from C to {@link notifyError};
 * * initialise the gnome-shell DBus service, storing it in {@link shellDBusService},
 * and flush the dbus session bus to make sure everything is started up;
 * * Ensure the Shell Window Tracker and Shell AppUsage (and Shell AppSystem)
 * are initialised.
 * * Call {@link loadTheme} to load the CSS stylesheets for gnome-shell
 * * Set up the stage heirarchy to group all UI actors under one actor,
 * {@link uiGroup}. Allocate all children their preferred size. This involves
 * calls to `St.set_ui_group(global.stage, uiGroup)` and reparenting the
 * window group and overlay group to `uiGroup` and adding `uiGroup` to the
 * global stage.
 * * Create the global [layout manager]{@link Layout.LayoutManager} instance, store in {@link layoutManager}.
 * * Create the global [XDND Handler]{@link XdndHandler.XdndHandler} instance, store in {@link xdndHandler}.
 * * Create the global [Ctrl+Alt+Tab Manager]{@link CtrlAltTab.CtrlAltTabManager} instance, store in {@link ctrlAltTabManager}.
 * * Create the [Overview]{@link Overview.Overview} instance, store in {@link overview}.
 * * Create the [shell magnifier + dbus service]{@link Magnifier.Magnifier}, store in {@link magnifier}.
 * * Create the global [status icon dispatcher]{@link StatusIconDispatcher.StatusIconDispatcher} instance,
 * store in {@link statusIconDispatcher}.
 * * Create the [top panel]{@link Panel.Panel}, store in {@link panel}.
 * * Create the global [window manager instance]{@link WindowManager.WindowManager}, store
 * in {@link wm}.
 * * Create the [message tray]{@link MessageTray.MessageTray}, store in {@link messageTray}
 * * Create the [on-screen keyboard and export it over DBus]{@link Keyboard.Keyboard},
 * store in {@link keyboard}.
 * Create the global [notification daemon]{@link NotificationDaemon.NotificationDaemon} instance,
 * store in {@link notificationDaemon}.
 * * Create the global [window attention handler]{@link WindowAttentionHandler.WindowAttentionHandler}
 * instance, store in {@link windowAttentionHandler}.
 * * If it's a user session, call {@link _createUserSession} (storing a whole
 * bunch more global instances); otherwise (GDM session),
 *  call {@link _createGDMSession}.
 * * start the status area ([`Main.panel.startStatusArea()`]{@link Panel.Panel#startStatusArea}).
 * * Now instances to be stored globally have all been made, initialise the layout
 * manager, keyboard, and overview ({@link Layout.LayoutManager#init},
 * {@link Keyboard.Keyboard#init}, {@link Overview.Overview#init}).
 * * If this is a user session, call {@link _initUserSession} to initialise
 * {@link recorder} and load all the extensions and take over a couple of keybindings
 * to show our run dialog and overview.
 * * Start the status icon dispatcher ({@link StatusIconDispatcher.StatusIconDispatcher#start}).
 * * Initialise the end session dialog, providing the dbus object for gnome-session
 * to initiate logouts ({@link EndSessionDialog.init}).
 * * Start the polkit authentication agent ({@link PolkitAuthenticationAgent.init}).
 * * {@link _log} that gnome-shell has been started with the current timestamp
 * (logging category 'info').
 * * if the environment variable `$SHELL_PERF_MODULE` is set (containing
 * the name of a performance script), it is run with {@link Scripting.runPerfScript}.
 * The output is saved in `$SHELL_PERF_OUTPUT`.
 * * Connect up `global.screen`'s 'notify::n-workspaces', 'window-entered-monitor',
 * 'window-left-monitor' and 'restacked' signals to {@link _nWorkspacesChanged},
 * {@link _windowEnteredMonitor}, {@link _windowLeftMonitor} and
 * {@link _windowRestacked} respectively and then call {@link _nWorkspacesChanged}
 * in order to set up dynamic workspaces.
 *
 */
function start() {
    // Monkey patch utility functions into the global proxy;
    // This is easier and faster than indirecting down into global
    // if we want to call back up into JS.
    
    /** @borrows _logError as logError
     * @todo this has no namespace. */
    global.logError = _logError;
    /** @borrows _logDebug as log
     * @todo this has no namespace. */
    global.log = _logDebug;

    // Chain up async errors reported from C
    global.connect('notify-error', function (global, msg, detail) { notifyError(msg, detail); });

    Gio.DesktopAppInfo.set_desktop_env('GNOME');

    shellDBusService = new ShellDBus.GnomeShell();
    // Force a connection now; dbus.js will do this internally
    // if we use its name acquisition stuff but we aren't right
    // now; to do so we'd need to convert from its async calls
    // back into sync ones.
    DBus.session.flush();

    // Ensure ShellWindowTracker and ShellAppUsage are initialized; this will
    // also initialize ShellAppSystem first.  ShellAppSystem
    // needs to load all the .desktop files, and ShellWindowTracker
    // will use those to associate with windows.  Right now
    // the Monitor doesn't listen for installed app changes
    // and recalculate application associations, so to avoid
    // races for now we initialize it here.  It's better to
    // be predictable anyways.
    Shell.WindowTracker.get_default();
    Shell.AppUsage.get_default();

    // The stage is always covered so Clutter doesn't need to clear it; however
    // the color is used as the default contents for the Mutter root background
    // actor so set it anyways.
    global.stage.color = DEFAULT_BACKGROUND_COLOR;
    global.stage.no_clear_hint = true;

    _defaultCssStylesheet = global.datadir + '/theme/gnome-shell.css';
    _gdmCssStylesheet = global.datadir + '/theme/gdm.css';
    loadTheme();

    // Set up stage hierarchy to group all UI actors under one container.
    uiGroup = new Shell.GenericContainer({ name: 'uiGroup' });
    uiGroup.connect('allocate',
                    function (actor, box, flags) {
                        let children = uiGroup.get_children();
                        for (let i = 0; i < children.length; i++)
                            children[i].allocate_preferred_size(flags);
                    });
    St.set_ui_root(global.stage, uiGroup);
    global.window_group.reparent(uiGroup);
    global.overlay_group.reparent(uiGroup);
    global.stage.add_actor(uiGroup);

    layoutManager = new Layout.LayoutManager();
    xdndHandler = new XdndHandler.XdndHandler();
    ctrlAltTabManager = new CtrlAltTab.CtrlAltTabManager();
    // This overview object is just a stub for non-user sessions
    overview = new Overview.Overview({ isDummy: global.session_type != Shell.SessionType.USER });
    magnifier = new Magnifier.Magnifier();
    statusIconDispatcher = new StatusIconDispatcher.StatusIconDispatcher();
    panel = new Panel.Panel();
    wm = new WindowManager.WindowManager();
    messageTray = new MessageTray.MessageTray();
    keyboard = new Keyboard.Keyboard();
    notificationDaemon = new NotificationDaemon.NotificationDaemon();
    windowAttentionHandler = new WindowAttentionHandler.WindowAttentionHandler();

    if (global.session_type == Shell.SessionType.USER)
        _createUserSession();
    else if (global.session_type == Shell.SessionType.GDM)
        _createGDMSession();

    panel.startStatusArea();

    layoutManager.init();
    keyboard.init();
    overview.init();

    if (global.session_type == Shell.SessionType.USER)
        _initUserSession();
    statusIconDispatcher.start(messageTray.actor);

    // Provide the bus object for gnome-session to
    // initiate logouts.
    EndSessionDialog.init();

    // Attempt to become a PolicyKit authentication agent
    PolkitAuthenticationAgent.init()

    _startDate = new Date();

    global.stage.connect('captured-event', _globalKeyPressHandler);

    _log('info', 'loaded at ' + _startDate);
    log('GNOME Shell started at ' + _startDate);

    let perfModuleName = GLib.getenv("SHELL_PERF_MODULE");
    if (perfModuleName) {
        let perfOutput = GLib.getenv("SHELL_PERF_OUTPUT");
        let module = eval('imports.perf.' + perfModuleName + ';');
        Scripting.runPerfScript(module, perfOutput);
    }

    global.screen.connect('notify::n-workspaces', _nWorkspacesChanged);

    global.screen.connect('window-entered-monitor', _windowEnteredMonitor);
    global.screen.connect('window-left-monitor', _windowLeftMonitor);
    global.screen.connect('restacked', _windowsRestacked);

    _nWorkspacesChanged();
}

let _workspaces = [];
let _checkWorkspacesId = 0;

/**
 * When the last window closed on a workspace is a dialog or splash
 * screen, we assume that it might be an initial window shown before
 * the main window of an application, and give the app a grace period
 * where it can map another window before we remove the workspace.
 * @type {number}
 * @const
 * @default
 */
const LAST_WINDOW_GRACE_TIME = 1000;

/** Checks all the current workspaces, making sure that the user has exactly
 * one empty workspace being the last of all of their workspaces
 * (so empty workspaces are removed and if there are
 * no empty workspaces, an empty one is added to the end).
 *
 * This is called 'dynamic workspaces'.
 *
 * Usually instead of calling this function directly, {@link _queueCheckWorkspaces}
 * is called instead to queue {@link _checkWorkspaces} after a small delay.
 * This is usually because checking for workspaces is done upon windows being
 * removed/added/moving workspaces, and it takes a little while after the
 * relevant signal is emitted for the window lists for each workspace to become
 * up to date.
 * @see _queueCheckWorkspaces
 */
function _checkWorkspaces() {
    let i;
    let emptyWorkspaces = [];

    for (i = 0; i < _workspaces.length; i++) {
        let lastRemoved = _workspaces[i]._lastRemovedWindow;
        if (lastRemoved &&
            (lastRemoved.get_window_type() == Meta.WindowType.SPLASHSCREEN ||
             lastRemoved.get_window_type() == Meta.WindowType.DIALOG ||
             lastRemoved.get_window_type() == Meta.WindowType.MODAL_DIALOG))
                emptyWorkspaces[i] = false;
        else
            emptyWorkspaces[i] = true;
    }

    let windows = global.get_window_actors();
    for (i = 0; i < windows.length; i++) {
        let win = windows[i];

        if (win.get_meta_window().is_on_all_workspaces())
            continue;

        let workspaceIndex = win.get_workspace();
        emptyWorkspaces[workspaceIndex] = false;
    }

    // If we don't have an empty workspace at the end, add one
    if (!emptyWorkspaces[emptyWorkspaces.length -1]) {
        global.screen.append_new_workspace(false, global.get_current_time());
        emptyWorkspaces.push(false);
    }

    let activeWorkspaceIndex = global.screen.get_active_workspace_index();
    let removingCurrentWorkspace = (emptyWorkspaces[activeWorkspaceIndex] &&
                                    activeWorkspaceIndex < emptyWorkspaces.length - 1);
    // Don't enter the overview when removing multiple empty workspaces at startup
    let showOverview  = (removingCurrentWorkspace &&
                         !emptyWorkspaces.every(function(x) { return x; }));

    if (removingCurrentWorkspace) {
        // "Merge" the empty workspace we are removing with the one at the end
        wm.blockAnimations();
    }

    // Delete other empty workspaces; do it from the end to avoid index changes
    for (i = emptyWorkspaces.length - 2; i >= 0; i--) {
        if (emptyWorkspaces[i])
            global.screen.remove_workspace(_workspaces[i], global.get_current_time());
    }

    if (removingCurrentWorkspace) {
        global.screen.get_workspace_by_index(global.screen.n_workspaces - 1).activate(global.get_current_time());
        wm.unblockAnimations();

        if (!overview.visible && showOverview)
            overview.show();
    }

    _checkWorkspacesId = 0;
    return false;
}

/** Callback when a window is removed from a workspace.
 *
 * This saves the last removed window for that workspace for use in
 * {@link checkWorkspaces} and calls {@link _queueCheckWorkspaces}.
 *
 * It also waits {@link LAST_WINDOW_GRACE_TIME}, and if no windows have been
 * removed since this one, we call {@link _queueCheckWorkspaces} again. (Note -
 * why do we double-call it?)
 *
 * The point of {@link LAST_WINDOW_GRACE_TIME} is that if the window removed
 * was a splash screen (say) and the workspace is now empty, it is
 * possible that very soon the main application will launch once the splash
 * screen is closed. So we delay from removing the workspace until we allow
 * a little time for any additional windows to spawn.
 * @param {Meta.Workspace} workspace - workspace the window was removed from.
 * @param {Meta.Window} window - window that was removed.
 * @see _queueCheckWorkspaces
 */
function _windowRemoved(workspace, window) {
    workspace._lastRemovedWindow = window;
    _queueCheckWorkspaces();
    Mainloop.timeout_add(LAST_WINDOW_GRACE_TIME, function() {
        if (workspace._lastRemovedWindow == window) {
            workspace._lastRemovedWindow = null;
            _queueCheckWorkspaces();
        }
    });
}

/** Callback when a window leaves a monitor (e.g. when it moves to another
 * monitor).
 *
 * If the window left the primary monitor, that might make that workspace empty
 * (if the user has workspaces on the primary monitor only ??).
 *
 * So we call {@link _queueCheckWorkspaces}.
 * @param {Meta.Screen} metaScreen - screen the window left.
 * @param {number} monitorIndex - the index of the monitor that the window left
 * @param {Meta.Window} metaWindow - the window that left the monitor
 * @see _queueCheckWorkspaces
 */
function _windowLeftMonitor(metaScreen, monitorIndex, metaWin) {
    // If the window left the primary monitor, that
    // might make that workspace empty
    if (monitorIndex == layoutManager.primaryIndex)
        _queueCheckWorkspaces();
}

/** Callback when a window enters a monitor (e.g. when it moves to another
 * monitor).
 *
 * If the window entered the primary monitor, that might make that workspace
 * non-empty empty
 * (if the user has workspaces on the primary monitor only ??).
 *
 * So we call {@link _queueCheckWorkspaces}.
 * @param {Meta.Screen} metaScreen - screen the window left.
 * @param {number} monitorIndex - the index of the monitor that the window left
 * @param {Meta.Window} metaWindow - the window that left the monitor
 * @see _queueCheckWorkspaces
 */
function _windowEnteredMonitor(metaScreen, monitorIndex, metaWin) {
    // If the window entered the primary monitor, that
    // might make that workspace non-empty
    if (monitorIndex == layoutManager.primaryIndex)
        _queueCheckWorkspaces();
}

/** Callback when windows are restacked.
 *
 * Figure out where the pointer is in case we lost track of
 * it during a grab. (In particular, if a trayicon popup menu
 * is dismissed, see if we need to close the message tray.)
 */
function _windowsRestacked() {
    // Figure out where the pointer is in case we lost track of
    // it during a grab. (In particular, if a trayicon popup menu
    // is dismissed, see if we need to close the message tray.)
    global.sync_pointer();
}

/** Queues a call to {@link _checkWorkspaces} using `Meta.later_add`, if it
 * is not already queued.
 *
 * This is used partially to throttle very close (in time) calls to
 * {@link checkWorkspaces} and also to give changes (like windows being
 * removed or added) time to sync properly (for example if you connect
 * to window-added, the meta window can sometimes be added before its
 * window actor is created so you can't access it until you wait a little bit).
 * @see _checkWorkspaces
 */
function _queueCheckWorkspaces() {
    if (_checkWorkspacesId == 0)
        _checkWorkspacesId = Meta.later_add(Meta.LaterType.BEFORE_REDRAW, _checkWorkspaces);
}

/** Callback when the number of workspaces changes.
 *
 * If a workspace has been added, this listens to the 'window-added' and
 * 'window-removed' events on that workspace, connecting to
 * {@link _queueCheckWorkspaces} and {@link _windowRemoved} respectively in
 * order to make sure the user has exactly one empty workspace at the end
 * of the workspaces.
 *
 * If a workspace has been removed, these signals are disconnected.
 * @see _queueCheckWorkspaces
 */
function _nWorkspacesChanged() {
    let oldNumWorkspaces = _workspaces.length;
    let newNumWorkspaces = global.screen.n_workspaces;

    if (oldNumWorkspaces == newNumWorkspaces)
        return false;

    let lostWorkspaces = [];
    if (newNumWorkspaces > oldNumWorkspaces) {
        let w;

        // Assume workspaces are only added at the end
        for (w = oldNumWorkspaces; w < newNumWorkspaces; w++)
            _workspaces[w] = global.screen.get_workspace_by_index(w);

        for (w = oldNumWorkspaces; w < newNumWorkspaces; w++) {
            let workspace = _workspaces[w];
            workspace._windowAddedId = workspace.connect('window-added', _queueCheckWorkspaces);
            workspace._windowRemovedId = workspace.connect('window-removed', _windowRemoved);
        }

    } else {
        // Assume workspaces are only removed sequentially
        // (e.g. 2,3,4 - not 2,4,7)
        let removedIndex;
        let removedNum = oldNumWorkspaces - newNumWorkspaces;
        for (let w = 0; w < oldNumWorkspaces; w++) {
            let workspace = global.screen.get_workspace_by_index(w);
            if (_workspaces[w] != workspace) {
                removedIndex = w;
                break;
            }
        }

        let lostWorkspaces = _workspaces.splice(removedIndex, removedNum);
        lostWorkspaces.forEach(function(workspace) {
                                   workspace.disconnect(workspace._windowAddedId);
                                   workspace.disconnect(workspace._windowRemovedId);
                               });
    }

    _queueCheckWorkspaces();

    return false;
}

/**
 * Get the theme CSS file that the shell will load. This is
 * {@link _cssStylesheet}.
 * @returns {string} the path to the css style sheet that will be used, `null`
 * if we are using the default ({@link _defaultCssStylesheet}).
 */
function getThemeStylesheet()
{
    return _cssStylesheet;
}

/**
 * Set the theme CSS file that the shell will load. If `null`, the default
 * style sheet {@link _defaultCssStylesheet} (`/usr/share/gnome-shell/themes/gnome-shell.css`)
 * will be used. This would be used if you were using a custom theme.
 * @param {string} cssStylesheet - A file path that contains the theme CSS,
 * set it to null to use the default.
 */
function setThemeStylesheet(cssStylesheet)
{
    _cssStylesheet = cssStylesheet;
}

/**
 * Reloads the theme CSS file ({@link _cssStylesheet}, or if that is null,
 * {@link _defaultCssStylesheet}).
 *
 * (If you are using a custom theme you probably have {@link _cssStylesheet}
 * pointing to `$HOME/.themes/themename/gnome-shell/gnome-shell.css`; otherwise
 * this is null and the default stylesheet {@link _defaultCssStylesheet} at
 * `/usr/share/gnome-shell/theme/gnome-shell.css` is used).
 */
function loadTheme() {
    let themeContext = St.ThemeContext.get_for_stage (global.stage);
    let previousTheme = themeContext.get_theme();

    let cssStylesheet = _defaultCssStylesheet;
    if (_cssStylesheet != null)
        cssStylesheet = _cssStylesheet;

    let theme = new St.Theme ({ application_stylesheet: cssStylesheet });

    if (global.session_type == Shell.SessionType.GDM)
        theme.load_stylesheet(_gdmCssStylesheet);

    if (previousTheme) {
        let customStylesheets = previousTheme.get_custom_stylesheets();

        for (let i = 0; i < customStylesheets.length; i++)
            theme.load_stylesheet(customStylesheets[i]);
    }

    themeContext.set_theme (theme);
}

/** Pops up a transient notification from the message tray to inform the user
 * of something.
 *
 * The notification is a generic {@link MessageTray.Notification} and it gets
 * its own source, a {@link eEssageTray.SystemNotificationSource}.
 *
 * ![See the example for how this was created](pics/MessageTrayNotificationTransient.png)
 *
 * @param {string} msg - A message, used as the title of the notification.
 * @param {string} details - Additional information, used as the banner of
 * the notification.
 * @example
 *  Main.notify('Customise your GNOME-shell experience!',
 *              'Find extensions at https://extensions.gnome.org');
 */
function notify(msg, details) {
    let source = new MessageTray.SystemNotificationSource();
    messageTray.add(source);
    let notification = new MessageTray.Notification(source, msg, details);
    notification.setTransient(true);
    source.notify(notification);
}

/** Calls {@link notify} with the message and details provided.
 * Additionally, this logs the error with {@link _logDebug} to add it to the
 * error stack.
 * @inheritparams notify
 */
function notifyError(msg, details) {
    // Also print to stderr so it's logged somewhere
    if (details)
        log('error: ' + msg + ': ' + details);
    else
        log('error: ' + msg);

    notify(msg, details);
}

/** local representation of an error.
 * This is what lives on the error stack returned by {@link _getAndClearErrorStack}.
 * {@link _log}, {@link _logError} and {@link _logDebug} all add on to this.
 * @typedef Error
 * @type {Object}
 * @property {number} timestamp - timestamp of the error.
 * @property {string} category - category of the error: 'debug', 'error', 'info'.
 * @property {string} message - error message.
 */

/**
 * Log a message into the LookingGlass error
 * stream.  This is primarily intended for use by the
 * extension system as well as debugging.
 *
 * It creates a new {@link Main.Error} for the error and adds it to the error
 * log stack.
 *
 * Get the current error log stack with {@link _getAndClearErrorStack}.
 * @param {string} category - string message type ('info', 'error', 'debug')
 * @param {string} msg - A message string
 * @param {...} - Any further arguments are converted into JSON notation,
 *      and appended to the log message, separated by spaces.
 */
function _log(category, msg) {
    let text = msg;
    if (arguments.length > 2) {
        text += ': ';
        for (let i = 2; i < arguments.length; i++) {
            text += JSON.stringify(arguments[i]);
            if (i < arguments.length - 1)
                text += ' ';
        }
    }
    _errorLogStack.push({timestamp: new Date().getTime(),
                         category: category,
                         message: text });
}

/** Calls {@link _log} with category 'error'.
 * @inheritparams _log
 * @see _log
 */
function _logError(msg) {
    return _log('error', msg);
}

/** Calls {@link _log} with category 'debug'.
 * @inheritparams _log
 * @see _log
 */
function _logDebug(msg) {
    return _log('debug', msg);
}

/** Gets the current error log stack and then clears it.
 * Used by the error display in the looking glass.
 * @see LookingGlass.ErrorLog#_renderTex
 * @returns {Main.Error[]} current error log stack.
 * @see Main.Error
 */
function _getAndClearErrorStack() {
    let errors = _errorLogStack;
    _errorLogStack = [];
    return errors;
}

/** Logs the current stack trace using {@link log}.
 * @param {string} [msg] if provided, logged before the trace.
 */
function logStackTrace(msg) {
    try {
        throw new Error();
    } catch (e) {
        // e.stack must have at least two lines, with the first being
        // logStackTrace() (which we strip off), and the second being
        // our caller.
        let trace = e.stack.substr(e.stack.indexOf('\n') + 1);
        log(msg ? (msg + '\n' + trace) : trace);
    }
}

/** Tests whether a window actor is displayed on workspace `workspaceIndex`.
 * @param {Meta.WindowActor} win - window actor to test
 * @param {number} workspaceIndex - workspace index to see if this window is on.
 * @returns {boolean} true if the window is on workspace `workspaceIndex` or
 * the window is on all workspaces.
 * @see getWindowActorsForWorkspace
 */
function isWindowActorDisplayedOnWorkspace(win, workspaceIndex) {
    return win.get_workspace() == workspaceIndex ||
        (win.get_meta_window() && win.get_meta_window().is_on_all_workspaces());
}

/** Conenience function.
 * Gets the window actors for every window on the specified workspace,
 * (including windows that are on all workspaces).
 *
 * @param {number} workspaceIndex - workspace index to get windows for.
 * @returns {Meta.WindowActor[]} array of window actors for windows that are
 * on the workspace `workspaceIndex`.
 * @see isWindowActorDisplayedOnWorkspace
 */
function getWindowActorsForWorkspace(workspaceIndex) {
    return global.get_window_actors().filter(function (win) {
        return isWindowActorDisplayedOnWorkspace(win, workspaceIndex);
    });
}

/** When a global grab is in progress (e.g. when the overview is showing),
 * metacity keybindings won't work. This intercepts key presses when a global
 * grab is in progress in order to manually get the keybindings working.
 * 
 * This function encapsulates hacks to make certain global keybindings
 * work even when we are in one of our modes where global keybindings
 * are disabled with a global grab. (When there is a global grab, then
 * all key events will be delivered to the stage, so ::captured-event
 * on the stage can be used for global keybindings.)
 *
 * Keybindings grabbed:
 *
 * * screenshot (launches the application in gconf key
 * '/apps/metacity/keybinding_commands/command_screenshot')
 * * windows/super key - toggle the overview
 * * metacity switch panels binding - shows the ctrl alt tab popup
 * * metacity run dialog binding (Alt+F2) - show the run dialog
 * * metacity change workspaces up and down binding - switch workspaces and
 * show the workspace switcher (left/right are disabled since in GNOME
 * workspaces are all vertically laid out)
 * * metacity main menu binding (Alt+F1) - toggle the overview
 */
function _globalKeyPressHandler(actor, event) {
    if (modalCount == 0)
        return false;
    if (event.type() != Clutter.EventType.KEY_PRESS)
        return false;

    let symbol = event.get_key_symbol();
    let keyCode = event.get_key_code();
    let modifierState = Shell.get_event_state(event);

    // This relies on the fact that Clutter.ModifierType is the same as Gdk.ModifierType
    let action = global.display.get_keybinding_action(keyCode, modifierState);

    // The screenshot action should always be available (even if a
    // modal dialog is present)
    if (action == Meta.KeyBindingAction.COMMAND_SCREENSHOT) {
        let gconf = GConf.Client.get_default();
        let command = gconf.get_string('/apps/metacity/keybinding_commands/command_screenshot');
        if (command != null && command != '')
            Util.spawnCommandLine(command);
        return true;
    }

    // Other bindings are only available to the user session when the overview is up and
    // no modal dialog is present.
    if (global.session_type == Shell.SessionType.USER && (!overview.visible || modalCount > 1))
        return false;

    // This isn't a Meta.KeyBindingAction yet
    if (symbol == Clutter.Super_L || symbol == Clutter.Super_R) {
        overview.hide();
        return true;
    }

    if (action == Meta.KeyBindingAction.SWITCH_PANELS) {
        ctrlAltTabManager.popup(modifierState & Clutter.ModifierType.SHIFT_MASK,
                                modifierState);
        return true;
    }

    // None of the other bindings are relevant outside of the user's session
    if (global.session_type != Shell.SessionType.USER)
        return false;

    switch (action) {
        // left/right would effectively act as synonyms for up/down if we enabled them;
        // but that could be considered confusing; we also disable them in the main view.
        //
        // case Meta.KeyBindingAction.WORKSPACE_LEFT:
        //     wm.actionMoveWorkspaceLeft();
        //     return true;
        // case Meta.KeyBindingAction.WORKSPACE_RIGHT:
        //     wm.actionMoveWorkspaceRight();
        //     return true;
        case Meta.KeyBindingAction.WORKSPACE_UP:
            wm.actionMoveWorkspaceUp();
            return true;
        case Meta.KeyBindingAction.WORKSPACE_DOWN:
            wm.actionMoveWorkspaceDown();
            return true;
        case Meta.KeyBindingAction.PANEL_RUN_DIALOG:
        case Meta.KeyBindingAction.COMMAND_2:
            getRunDialog().open();
            return true;
        case Meta.KeyBindingAction.PANEL_MAIN_MENU:
            overview.hide();
            return true;
    }

    return false;
}

/** Finds the index of an actor in the modal stack.
 * @param {Clutter.Actor} - actor to search for in the stack, same as what
 * was passed in to {@link pushModal}.
 * @returns {number} index of an actor in the modal stack.
 * @see pushModal
 */
function _findModal(actor) {
    for (let i = 0; i < modalActorFocusStack.length; i++) {
        if (modalActorFocusStack[i].actor == actor)
            return i;
    }
    return -1;
}

/**
 * Ensure we are in a mode where all keyboard and mouse input goes to
 * the stage, and focus `actor`. Multiple calls to this function act in
 * a stacking fashion; the effect will be undone when an equal number
 * of {@link popModal} invocations have been made.
 *
 * Next, record the current Clutter keyboard focus on a stack. If the
 * modal stack returns to this actor, reset the focus to the actor
 * which was focused at the time {@link pushModal} was invoked.
 *
 * `timestamp` is optionally used to associate the call with a specific user
 * initiated event.  If not provided then the value of
 * `global.get_current_time()` is assumed.
 *
 * @param {Clutter.Actor} actor - Clutter actor which will be given keyboard focus
 * @param {number} [timestamp] - optional timestamp, `global.get_current_time()`
 * will be used if not provided.
 * @returns {boolean} true if and only if we successfully acquired a grab or
 * already had one.
 * @see popModal
 */
function pushModal(actor, timestamp) {
    if (timestamp == undefined)
        timestamp = global.get_current_time();

    if (modalCount == 0) {
        if (!global.begin_modal(timestamp)) {
            log('pushModal: invocation of begin_modal failed');
            return false;
        }
    }

    global.set_stage_input_mode(Shell.StageInputMode.FULLSCREEN);

    modalCount += 1;
    let actorDestroyId = actor.connect('destroy', function() {
        let index = _findModal(actor);
        if (index >= 0)
            modalActorFocusStack.splice(index, 1);
    });
    let curFocus = global.stage.get_key_focus();
    let curFocusDestroyId;
    if (curFocus != null) {
        curFocusDestroyId = curFocus.connect('destroy', function() {
            let index = _findModal(actor);
            if (index >= 0)
                modalActorFocusStack[index].actor = null;
        });
    }
    modalActorFocusStack.push({ actor: actor,
                                focus: curFocus,
                                destroyId: actorDestroyId,
                                focusDestroyId: curFocusDestroyId });

    global.stage.set_key_focus(actor);
    return true;
}

/**
 * Reverse the effect of {@link pushModal}.  If this invocation is undoing
 * the topmost invocation, then the focus will be restored to the
 * previous focus at the time when {@link pushModal} was invoked.
 *
 * `timestamp` is optionally used to associate the call with a specific user
 * initiated event.  If not provided then the value of
 * `global.get_current_time()` is assumed.
 *
 * @param {Clutter.Actor} actor - `Clutter.Actor`passed to original invocation of {@link pushModal}.
 * @param {number} [timestamp] - optional timestamp, current time is used if
 * not provided.
 * @see pushModal
 */
function popModal(actor, timestamp) {
    if (timestamp == undefined)
        timestamp = global.get_current_time();

    let focusIndex = _findModal(actor);
    if (focusIndex < 0) {
        global.stage.set_key_focus(null);
        global.end_modal(timestamp);
        global.set_stage_input_mode(Shell.StageInputMode.NORMAL);

        throw new Error('incorrect pop');
    }

    modalCount -= 1;

    let record = modalActorFocusStack[focusIndex];
    record.actor.disconnect(record.destroyId);

    if (focusIndex == modalActorFocusStack.length - 1) {
        if (record.focus)
            record.focus.disconnect(record.focusDestroyId);
        global.stage.set_key_focus(record.focus);
    } else {
        let t = modalActorFocusStack[modalActorFocusStack.length - 1];
        if (t.focus)
            t.focus.disconnect(t.focusDestroyId);
        // Remove from the middle, shift the focus chain up
        for (let i = modalActorFocusStack.length - 1; i > focusIndex; i--) {
            modalActorFocusStack[i].focus = modalActorFocusStack[i - 1].focus;
            modalActorFocusStack[i].focusDestroyId = modalActorFocusStack[i - 1].focusDestroyId;
        }
    }
    modalActorFocusStack.splice(focusIndex, 1);

    if (modalCount > 0)
        return;

    global.end_modal(timestamp);
    global.set_stage_input_mode(Shell.StageInputMode.NORMAL);
}

/** Returns the global lookin glass instance.
 * @see lookingGlass
 * @returns {LookingGlass.LookingGlass} the looking glass ({@link lookingGlass}).
 */
function createLookingGlass() {
    if (lookingGlass == null) {
        lookingGlass = new LookingGlass.LookingGlass();
    }
    return lookingGlass;
}

/** Returns the global run dialog instance.
 * @see runDialog
 * @returns {RunDialog.RunDialog} the run dialog ({@link runDialog}).
 */
function getRunDialog() {
    if (runDialog == null) {
        runDialog = new RunDialog.RunDialog();
    }
    return runDialog;
}

/**
 * Activates `window`, switching to its workspace first if necessary,
 * and switching out of the overview if it's currently active.
 * @param {Meta.Window} window - the Meta.Window to activate
 * @param {number} [time] - (optional) current event time. Current time will be used
 * if not provided.
 * @param {number} [workspaceNum] - (optional) window's workspace number. If
 * not provided, it will be calculated.
 */
function activateWindow(window, time, workspaceNum) {
    let activeWorkspaceNum = global.screen.get_active_workspace_index();
    let windowWorkspaceNum = (workspaceNum !== undefined) ? workspaceNum : window.get_workspace().index();

    if (!time)
        time = global.get_current_time();

    if (windowWorkspaceNum != activeWorkspaceNum) {
        let workspace = global.screen.get_workspace_by_index(windowWorkspaceNum);
        workspace.activate_with_focus(window, time);
    } else {
        window.activate(time);
    }

    overview.hide();
}

// TODO - replace this timeout with some system to guess when the user might
// be e.g. just reading the screen and not likely to interact.
/** Number of seconds to delay doing deferred work.
 * @see queueDeferredWork
 * @see initializeDeferredWork
 * @type {number}
 * @const
 * @default */
const DEFERRED_TIMEOUT_SECONDS = 20;
var _deferredWorkData = {};
// Work scheduled for some point in the future
var _deferredWorkQueue = [];
// Work we need to process before the next redraw
var _beforeRedrawQueue = [];
// Counter to assign work ids
var _deferredWorkSequence = 0;
var _deferredTimeoutId = 0;

/** Runs the work identified by id `workId`, removing it from the deferred work
 * queue when done.
 * @inheritparams queueDeferredWork
 */
function _runDeferredWork(workId) {
    if (!_deferredWorkData[workId])
        return;
    let index = _deferredWorkQueue.indexOf(workId);
    if (index < 0)
        return;

    _deferredWorkQueue.splice(index, 1);
    _deferredWorkData[workId].callback();
    if (_deferredWorkQueue.length == 0 && _deferredTimeoutId > 0) {
        Mainloop.source_remove(_deferredTimeoutId);
        _deferredTimeoutId = 0;
    }
}

/** Runs all deferred work.
 * @see initializeDeferredWork
 * @see queueDeferredWork */
function _runAllDeferredWork() {
    while (_deferredWorkQueue.length > 0)
        _runDeferredWork(_deferredWorkQueue[0]);
}

/** Runs all the deferred work that is meant to happen before a redraw.
 * @see initializeDeferredWork
 * @see queueDeferredWork */
function _runBeforeRedrawQueue() {
    for (let i = 0; i < _beforeRedrawQueue.length; i++) {
        let workId = _beforeRedrawQueue[i];
        _runDeferredWork(workId);
    }
    _beforeRedrawQueue = [];
}

/** Queues the work identified by {@link workId} to happen before a
 * redraw.
 * @inheritparams queueDeferredWork
 * @see initializeDeferredWork
 * @see queueDeferredWork
 */
function _queueBeforeRedraw(workId) {
    _beforeRedrawQueue.push(workId);
    if (_beforeRedrawQueue.length == 1) {
        Meta.later_add(Meta.LaterType.BEFORE_REDRAW, function () {
            _runBeforeRedrawQueue();
            return false;
        });
    }
}

/**
 * This function sets up a callback to be invoked when either the
 * given actor is mapped, or after some period of time when the machine
 * is idle.  This is useful if your actor isn't always visible on the
 * screen (for example, all actors in the overview), and you don't want
 * to consume resources updating if the actor isn't actually going to be
 * displaying to the user.
 *
 * Note that queueDeferredWork is called by default immediately on
 * initialization as well, under the assumption that new actors
 * will need it.
 *
 * @param {Clutter.Actor} actor - A Clutter Actor - we do the work if it
 * is mapped, but otherwise we defer it and do it {@link DEFERRED_TIMEOUT_SECONDS}
 * later.
 * @param {function()} callback - Function to invoke to perform work
 * @returns {strin} a work identifier (string), use it with {@link queueDeferredWork}.
 */
function initializeDeferredWork(actor, callback, props) {
    // Turn into a string so we can use as an object property
    let workId = '' + (++_deferredWorkSequence);
    _deferredWorkData[workId] = { 'actor': actor,
                                  'callback': callback };
    actor.connect('notify::mapped', function () {
        if (!(actor.mapped && _deferredWorkQueue.indexOf(workId) >= 0))
            return;
        _queueBeforeRedraw(workId);
    });
    actor.connect('destroy', function() {
        let index = _deferredWorkQueue.indexOf(workId);
        if (index >= 0)
            _deferredWorkQueue.splice(index, 1);
        delete _deferredWorkData[workId];
    });
    queueDeferredWork(workId);
    return workId;
}

/**
 * Ensure that the work identified by `workId` will be
 * run on map or timeout.  You should call this function
 * for example when data being displayed by the actor has
 * changed.
 * @param {string} workId - work identifier, obtained with
 * {@link initializeDeferredWork}.
 */
function queueDeferredWork(workId) {
    let data = _deferredWorkData[workId];
    if (!data) {
        global.logError('invalid work id ', workId);
        return;
    }
    if (_deferredWorkQueue.indexOf(workId) < 0)
        _deferredWorkQueue.push(workId);
    if (data.actor.mapped) {
        _queueBeforeRedraw(workId);
        return;
    } else if (_deferredTimeoutId == 0) {
        _deferredTimeoutId = Mainloop.timeout_add_seconds(DEFERRED_TIMEOUT_SECONDS, function () {
            _runAllDeferredWork();
            _deferredTimeoutId = 0;
            return false;
        });
    }
}
