// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/** @autonamespace DND */
/**
 * @fileOverview
 * Handles drag and drop interfaces for gnome-shell objects.
 *
 * ![Example of a draggable](pics/dragEvent.png)
 *
 * See also the corresponding tutorial, {@tutorial DND}.
 * @see {@tutorial DND}, a tutorial on drag and drop.
 */

const Clutter = imports.gi.Clutter;
const Gtk = imports.gi.Gtk;
const St = imports.gi.St;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const Tweener = imports.ui.tweener;
const Main = imports.ui.main;

const Params = imports.misc.params;

/** @+
 * @const
 * @default
 * @type {number} */
/** Time (seconds) to scale down to maxDragActorSize */
const SCALE_ANIMATION_TIME = 0.25;
/** Time (seconds) to animate to original position on cancel */
const SNAP_BACK_ANIMATION_TIME = 0.25;
/** Time (seconds) to animate to original position on success */
const REVERT_ANIMATION_TIME = 0.75;
/** @- */

/** @+
 * @const
 * @enum */
/** Indicates the result of a drag motion.
 * Returned by a [drag monitor's `dragMotion` function]{@link DragMonitor.dragMotion}
 * or a drop target's [`handleDragOver`]{@link DropTarget#handleDragOver} method.
 *
 * The result is used to modify the cursor during a drag to indicate to the user
 * what will happen if they drop their draggable here.
 */
const DragMotionResult = {
    /** Indicates that the target will not handle the drop (same as
     * {@link DragMotionResult.CONTINUE} but we do *not* allow further drag
     * monitors/potential drop targets to see the draggable) */
    NO_DROP:   0,
    /** The draggable will be copied if dropped here. */
    COPY_DROP: 1,
    /** The draggable will be moved if dropped here. */
    MOVE_DROP: 2,
    /** We are not a drop target for this draggable; continue on and let other
     * drag monitors/potential drop targets handle this drag. */
    CONTINUE:  3
};

/** Not really an enum, but maps a {@link DragMotionResult}
 * to a cursor type ({@link Shell.Cursor}), provided it is not
 * {@link DragMotionResult.CONTINUE}.
 * @example
 * global.set_cursor(DRAG_CURSOR_MAP[DragMotionRestul.COPY_DROP]);
 * @type {Shell.Cursor}
 */
const DRAG_CURSOR_MAP = {
    /** {@link DragMotionResult.NO_DROP}'s cursor - `Shell.Cursor.DND_UNSUPPORTED_TARGET`.
     * ![Shell.Cursor.DND_UNSUPPORTED_TARGET](pics/icons/dnd-none.png) */
    0: Shell.Cursor.DND_UNSUPPORTED_TARGET,
    /** {@link DragMotionResult.COPY_DROP}'s cursor - `Shell.Cursor.DND_COPY`.
     * ![Shell.Cursor.DND_COPY](pics/icons/dnd-copy.png) */
    1: Shell.Cursor.DND_COPY,
    /** {@link DragMotionResult.MOVE_DROP}'s cursor - `Shell.Cursor.DND_MOVE`.
     * ![Shell.Cursor.DND_COPY](pics/icons/dnd-move.png) */
    2: Shell.Cursor.DND_MOVE
};

/** The result of a drag drop action.
 * Returned by a [drag monitor's `dragDrop` function]{@link DragMonitor.dragDrop}.
 */
const DragDropResult = {
    /** The drop failed. */
    FAILURE:  0,
    /** The drop succeeded. */
    SUCCESS:  1,
    /** We do not wish to handle this drop - continue on to allow the rest of
     * the drag monitors to try and handle this drop. */
    CONTINUE: 2
};
/** @- */

/** The global event handler actor. Created on demand. Use
 * {@link #_getEventHandlerActor} to retrieve it (which will create it if
 * it doesn't yet exist).
 * @type {?Clutter.Rectangle} */
let eventHandlerActor = null;
/** If a drag is in progress, this is the current draggable.
 * @type {_Draggable} */
let currentDraggable = null;
/** All the existing drag monitors.
 * @type {DragMonitor[]}
 * @see addDragMonitor
 * @see removeDragMonitor */
let dragMonitors = [];

/** To listen to drag and drop events, we place a Clutter.Rectangle on the stage.
 * It has width 0 and height 0. It is used to listen to all events that occur
 * during a grab.
 *
 * When a drag begins, we use Clutter to grab all pointer and keyboard events
 * to the handler. Upon an event being captured {@link _Draggable#_onEvent} is
 * called (of the thing being dragged), to know when the cursor has moved with
 * the draggable, or when the drag has stopped or started.
 *
 * When the drag ends we release the handler.
 *
 * All draggables use this handler to capture events.
 * @returns {Clutter.Rectangle} the clutter rectangle that is used by all
 * draggables to monitor events during a grab.
 */
function _getEventHandlerActor() {
    if (!eventHandlerActor) {
        eventHandlerActor = new Clutter.Rectangle();
        eventHandlerActor.width = 0;
        eventHandlerActor.height = 0;
        Main.uiGroup.add_actor(eventHandlerActor);
        // We connect to 'event' rather than 'captured-event' because the capturing phase doesn't happen
        // when you've grabbed the pointer.
        eventHandlerActor.connect('event',
                                  function(actor, event) {
                                      return currentDraggable._onEvent(actor, event);
                                  });
    }
    return eventHandlerActor;
}

/** Adds a {@link DragMonitor}.
 *
 * This means that `monitor.dragMotion` is called every time the draggable changes position,
 * and `monitor.dragDrop` is called when a draggable is dropped (they are only
 * called if they are present).
 * @param {DragMonitor} monitor - monitor to add.
 * @see {@link removeDragMonitor} to remove a monitor previously added.
 */
function addDragMonitor(monitor) {
    dragMonitors.push(monitor);
}

/** Removes a drag monitor (an object holding callbacks to be called upon
 * drags occuring, i.e. an object that monitors a drag).
 * @param {DragMonitor} monitor - monitor to remove.
 * @see {@link addDragMonitor} to add a monitor.
 */
function removeDragMonitor(monitor) {
    for (let i = 0; i < dragMonitors.length; i++)
        if (dragMonitors[i] == monitor) {
            dragMonitors.splice(i, 1);
            return;
        }
}

/** creates a new _Draggable.
 *
 * We store the source actor (`actor`) as {@link #actor}.
 *
 * If `params.manualMode` was `false` we listen for button presses
 * on `actor` and connect this to {@link #_onButtonPress} which will start the
 * drag automatically on a button pres.
 *
 * We store the other `params` into {@link #_restoreOnSuccess},
 * {@link #_dragActorMaxSize} and {@link #_dragActorOpacity}.
 *
 * We connect to `actor`'s 'destroy' signal so that if the source actor is destroyed
 * while we are dragging we cancel the drag and disconnect all our signals.
 *
 * Then we initialise various state variables that will be used throughout the
 * class: ({@link #_buttonDown}, {@link #_dragInProgress}, {@link #_animationInProgress}.
 * @param {Clutter.Actor} actor - source actor to make draggable.
 * @param {Object} [params] - (optional) object with parameters to affect the
 *                            draggable's behaviour.
 * @param {boolean} [params.manualMode=false] - whether drag and drop is started
 * automagically upon a button press, or manually (use {@link _Draggable#startDrag}
 * to start the drag yourself).
 * @param {boolean} [params.restoreOnSuccess=false] - upon a successful drop,
 * should we fade the drag actor back in in its original position?
 * @param {boolean} [params.dragActorMaxSize] - the maximum size to use for
 * the drag actor (if it is smaller than this it will be left as-is, if it
 * is larger it will be scaled to this size).
 * @param {boolean} [params.dragActorOpacity] - the opacity to use for
 * the drag actor (otherwise it's opaque).
 * @classdesc
 * This class is used to make an actor draggable.
 *
 * You should **not** create an instance of this class yourself; instead, use
 * {@link makeDraggable} to make an actor draggable. This returns an instance
 * of a {@link _Draggable} that can then be stored.
 *
 * The class connects up the actor to all the appropriate signals such that
 * drag and drop is handled properly.
 *
 * One can use {@link #startDrag} to manually start a drag (as opposed to the
 * default click-and-hold that usually starts a drag).
 *
 * There are also signals [drag-begin]{@link .drag-begin}, [drag-end]{@link drag-end}
 * and [drag-cancelled]{@link drag-cancelled} that may be useful.
 *
 * @class
 * @see makeDraggable
 */
function _Draggable(actor, params) {
    this._init(actor, params);
}

_Draggable.prototype = {
    _init : function(actor, params) {
        params = Params.parse(params, { manualMode: false,
                                        restoreOnSuccess: false,
                                        dragActorMaxSize: undefined,
                                        dragActorOpacity: undefined });

        /** The source actor for the draggable.
         *
         * If the actor is an actor for a class implementing the draggable interface,
         * it should have property `._delegate` linking back to that class.
         *
         * That is so that if the {@link Draggable} API is implemented for
         * `actor`'s parent class, we can find the corresponding functions.
         * @type {Clutter.Actor} */
        this.actor = actor;
        if (!params.manualMode)
            this.actor.connect('button-press-event',
                               Lang.bind(this, this._onButtonPress));

        this.actor.connect('destroy', Lang.bind(this, function() {
            this._actorDestroyed = true;
            // If the drag actor is destroyed and we were going to fix
            // up its hover state, fix up the parent hover state instead
            if (this.actor == this._firstLeaveActor)
                this._firstLeaveActor = this._dragOrigParent;
            if (this._dragInProgress)
                this._cancelDrag(global.get_current_time());
            this.disconnectAll();
        }));
        this._onEventId = null;

        /** Whether we should fade the drag actor back into its original position
         * upon a successful drag. If `params.restoreOnSuccess` is not explicitly
         * set to `true` in the constructor, this will be `false`.
         * @type {boolean} */
        this._restoreOnSuccess = params.restoreOnSuccess;
        /** The maximum size to use for the drag actor, if provided as
         * `params.dragActorMaxSize` in the constructor.
         * @type {undefined|number} */
        this._dragActorMaxSize = params.dragActorMaxSize;
        /** The opacity of the drag actor, if provided in `params.dragActorOpacity`
         * in the constructor (otherwise it will be opaque).
         * @type {undefined|number} */
        this._dragActorOpacity = params.dragActorOpacity;
        /** State variable - whether the mouse button has been pressed and
         * has not yet been released.
         * @type {boolean} */
        this._buttonDown = false; 
        /** State variable - whether the drag has been started, and has not been
         * dropped or cancelled yet.
         * @type {boolean} */
        this._dragInProgress = false; 
         /** State variable - whether the drag is over and the item is in the
          * process of animating to its original position (snapping back or
          * reverting).
         * @type {boolean} */
        this._animationInProgress = false;

        /**
         * During the drag, we eat enter/leave events so that actors don't prelight or show
         * tooltips. But we remember the actors that we first left/last entered so we can
         * @see #_lastEnterActor
         * fix up the hover state after the drag ends.
         * @type {?Clutter.Actor} */
        this._firstLeaveActor = null;
        /** @inheritdoc #_firstLeaveActor
         * @see #_firstLeaveActor */
        this._lastEnterActor = null;

        /** Whether we are currently grabbing events (usually during a drag?)
         * @type {boolean} */
        this._eventsGrabbed = false;
    },

    /** Callback when the source actor {@link #actor} is clicked. Handles logic
     * for starting a drag.
     *
     * If the button is not a left click, or the source actor is currently
     * being animated, we return without doing anything.
     *
     * Otherwise we set the state variable {@link #_buttonDown} to `true`.
     *
     * Then, we grab all events to the source actor ({@link #_grabActor}) -
     * depending on what the user does next, the drag could be cancelled
     * (they did a button-release) or started (they moved the cursor by at
     * least some threshold distance without releasing it). See {@link #_onEvent}
     * for details of that.
     *
     * As an exception, if `actor` is a {@link St.Button} we start the drag
     * *manually* when the cursor moves out of the button as opposed to using
     * {@link #_grabActor} to monitor when to start the drag, because
     * {@link St.Button}s apparently break if you try to grab events to them.
     * 
     * If `params.manualMode` was set to `true` in the constructor (i.e. drags
     * should be started manually with {@link #startDrag}) then this will never
     * be connected.
     *
     * Note this always returns `false`, allowing the button press event to
     * be handled by the source actor itself (i.e. this draggable does not interfere
     * with anything else wishing to handle the button press event).
     * @see #_grabActor
     * @see #_onEvent
     */
    _onButtonPress : function (actor, event) {
        if (event.get_button() != 1)
            return false;

        if (Tweener.getTweenCount(actor))
            return false;

        this._buttonDown = true;
        // special case St.Button: grabbing the pointer would mess up the
        // internal state, so we start the drag manually on hover change
        if (this.actor instanceof St.Button)
            this.actor.connect('notify::hover',
                               Lang.bind(this, this._onButtonHoverChanged));
        else
            this._grabActor();

        let [stageX, stageY] = event.get_coords();
        /** The X coordinate of the cursor when the user first clicks on the
         * source actor. If they try to drag their cursor more than
         * a particular threshold without releasing the actor (say 8px), then
         * we start the drag. Hence we have to store the start location to
         * compare the threshold against.
         * @type {number}
         * @see #_dragStartY */
        this._dragStartX = stageX;
        /** The Y coordinate of the cursor when the user first clicks on the
         * source actor. If they try to drag their cursor more than
         * a particular threshold without releasing the actor (say 8px), then
         * we start the drag. Hence we have to store the start location to
         * compare the threshold against.
         * @type {number}
         * @see #_dragStartX */
        this._dragStartY = stageY;

        return false;
    },

    /** Callback when:
     *
     * 1. the source actor is a {@link St.Button};
     * 2. the button has been pressed but not yet released;
     * 3. the user has moved their mouse outside the button without releasing
     *    the cursor yet.
     *
     * This is deemed to be an attempt to drag the button, so we call
     * {@link #startDrag} to start the drag.
     *
     * Note that {@link St.Button} source actors are the only ones that get
     * this special treatment; all other source actors work out how to start
     * a drag by using {@link #_grabActor} and {@link #_onEvent} and listening
     * for the user moving their cursor at least a certain distance while it is
     * pressed.
     *
     * @see #_onButtonPress
     */
    _onButtonHoverChanged: function(button) {
        if (button.hover || !button.pressed)
            return;

        button.fake_release();
        this.startDrag(this._dragStartX, this._dragStartY,
                       global.get_current_time());
    },

    /** Grabs pointer events to the source actor {@link #actor}.
     *
     * Also, we connect signals such that {@link #_onEvent} is called whenever
     * {@link #actor} detects any event whatsoever
     * ({@link #_onEvent} will listen to things like button releases which would
     * end the drag, and moving the mouse which would start the drag).
     *
     * @see #_onEvent
     * @see {@link #_ungrabActor} which reverses {@link #_grabActor}.
     */
    _grabActor: function() {
        Clutter.grab_pointer(this.actor);
        this._onEventId = this.actor.connect('event',
                                             Lang.bind(this, this._onEvent));
    },

    /** Ungrabs pointer events to the source actor after they have been
     * grabbed with {@link #_grabActor}.
     * @see {@link #_grabActor}, the reverse of this. */
    _ungrabActor: function() {
        Clutter.ungrab_pointer();
        if (!this._onEventId)
            return;
        this.actor.disconnect(this._onEventId);
        this._onEventId = null;
    },

    /** Grabs all events (pointer and keyboard) to the global event handler actor
     * (different to {@link #_grabActor} which grabs pointer events to
     * the source actor {@link #actor}).
     *
     * Called from {@link #startDrag} when a drag is started, in order to
     * listen to any pointer or keyboard events that occur whilst the drag
     * is in progress (see {@link #_onEvent} for the handling of these).
     *
     * This additionally sets the state variable {@link #_eventsGrabbed} to
     * `true`.
     * @see {@link _getEventHandlerActor} for details on the global event handler
     *      actor.
     * @see #startDrag
     * @see {@link #_ungrabEvents}, which reverses the effect of this.
     */
    _grabEvents: function() {
        if (!this._eventsGrabbed) {
            Clutter.grab_pointer(_getEventHandlerActor());
            Clutter.grab_keyboard(_getEventHandlerActor());
            this._eventsGrabbed = true;
        }
    },

    /** Ungrabs all events (pointer and keyboard) from the global event handler
     * actor after they have been grabbed with {@link #_grabEvents}.
     *
     * Called from {@link #_dragComplete} when a drag is finished, undoing the
     * {@link #_grabEvents} call made in {@link #startDrag}.
     *
     * This additionally resets the state variable {@link #_eventsGrabbed} to
     * `false`.
     * @see {@link _getEventHandlerActor} for details on the global event handler
     *      actor.
     * @see #_dragComplete
     * @see {@link #_grabEvents}, which this function reverses the effect of.
     */
    _ungrabEvents: function() {
        if (this._eventsGrabbed) {
            Clutter.ungrab_pointer();
            Clutter.ungrab_keyboard();
            this._eventsGrabbed = false;
        }
    },

    /** Callback when any event occurs:
     *
     * * after the user has done a button-press on the draggable's source actor
     *   (but not yet a button release, see {@link #_onButtonPress}), OR
     * * when a drag has actually started ({@link #startDrag}).
     *
     * In the former case, the event come from the draggable's source actor
     * {@link #actor}. In the latter, they come from the global event handler
     * (see {@link #_getEventHandlerActor}).
     *
     * What we do depends on what the event was, but generally handle the
     * logic for starting, continuing, or stopping a drag:
     *
     * * if `event` is a **button-release**:
     *   + if a drag is already in progress, ({@link #_dragInProgress} is `true`,
     *     it means we are dropping the drag actor.
     *     We call {@link #_dragActorDropped} and return its result.
     *   + a drag is *not* in progress, yet we still have a drag actor {@link #_dragActor}
     *     but no animation is occuring (like fading the source actor in), then
     *     the drag must have been cancelled with Esc.
     *     We call {@link #_dragComplete} and return `true` (the event has been
     *     handled).
     *   + otherwise, the drag was never started (the person did a click
     *     on the draggable's source actor and then released it without moving
     *     the mouse). We call {@link #_ungrabActor} to undo our event grabs and
     *     return `false` (allowing actors underneath to process the event).
     * * if `event` is a **motion event**:
     *   + if a drag is in progress we call {@link #_updateDragPosition} (which
     *     draws the drag actor under the pointer) and return its result.
     *   + otherwise, the drag has not yet started. We call {@link #_maybeStartDrag}
     *     which sees if we've moved the cursor enough to trigger starting the
     *     drag and return its result.
     * * if `event` is a **key-press event** and the drag is in progress and the
     *   key pressed was Esc, we cancel the drag ({@link #_cancelDrag}) and
     *   return `true` (the event has been handled).
     * * if `event` is a **leave event** (the cursor has just left an actor),
     *   we store it in {@link #_firstLeaveActor} in order to restore its hover
     *   state/prelight etc properly upon drag completion. We return `false`
     *   to allow other handlers to get to the event.
     * * if `event` is an **enter event** (the cursor has just entered an actor),
     *   we store it in {@link #_lastEnterActor} in order to restore its hover
     *   state/prelight etc properly upon drag completion. We return `false`
     *   to allow other handlers to get to the event.
     * * otherwise (`event` was none of these), we return `false` to let
     *   other handlers get the event.
     *
     * @param {Clutter.Actor} actor - actor emitting the signal. Either the
     * draggable's source actor {@link #actor} or the global event handler
     * (see {@link #_getEventHandlerActor}).
     * @param {Clutter.Event} event - the event that was caught (could be
     * a button release, motion, enter or leave event, keypress, ...)
     * @returns {boolean} whether `event` was handled or not: `true` means
     * no further handlers will handle the event, while `false` will allow it
     * to pass on to the next handler.
     */
    _onEvent: function(actor, event) {
        // We intercept BUTTON_RELEASE event to know that the button was released in case we
        // didn't start the drag, to drop the draggable in case the drag was in progress, and
        // to complete the drag and ensure that whatever happens to be under the pointer does
        // not get triggered if the drag was cancelled with Esc.
        if (event.type() == Clutter.EventType.BUTTON_RELEASE) {
            this._buttonDown = false;
            if (this._dragInProgress) {
                return this._dragActorDropped(event);
            } else if (this._dragActor != null && !this._animationInProgress) {
                // Drag must have been cancelled with Esc.
                this._dragComplete();
                return true;
            } else {
                // Drag has never started.
                this._ungrabActor();
                return false;
            }
        // We intercept MOTION event to figure out if the drag has started and to draw
        // this._dragActor under the pointer when dragging is in progress
        } else if (event.type() == Clutter.EventType.MOTION) {
            if (this._dragInProgress) {
                return this._updateDragPosition(event);
            } else if (this._dragActor == null) {
                return this._maybeStartDrag(event);
            }
        // We intercept KEY_PRESS event so that we can process Esc key press to cancel
        // dragging and ignore all other key presses.
        } else if (event.type() == Clutter.EventType.KEY_PRESS && this._dragInProgress) {
            let symbol = event.get_key_symbol();
            if (symbol == Clutter.Escape) {
                this._cancelDrag(event.get_time());
                return true;
            }
        } else if (event.type() == Clutter.EventType.LEAVE) {
            if (this._firstLeaveActor == null)
                this._firstLeaveActor = event.get_source();
        } else if (event.type() == Clutter.EventType.ENTER) {
            this._lastEnterActor = event.get_source();
        }

        return false;
    },

    /** This function starts a drag. If the user did not explicitly set
     * `params.manualMode` to `true` in the constructor, they will never have
     * to call this - this will be triggered automatically once the user presses
     * the draggable and attempts to move it more than a particular distance
     * (see {@link #_onEvent}).
     *
     * If the user did set `params.manualMode` to true, they should start a drag
     * by calling this function.
     *
     * We:
     *
     * * set the state variable {@link #_dragInProgress} to `true`.
     * * emit the ['drag-begin']{@link .drag-begin} signal with the timestamp `time`.
     * * if we called {@link #_grabActor} previously to determine when to initiate
     *   the drag, we undo thi with {@link #_ungrabActor}.
     * * we grab all keyboard and mouse events to the global event handler with
     *   {@link #_grabEvents} (so that we can know when the drag stops etc).
     * * change the cursor to the drag-and-drop cursor.
     * * store the start/current location of the drag ({@link #_dragStartX},
     *   {@link #_dragStartY}, {@link #_dragX}, {@link #_dragY}) as `stageX`
     *   and `stageY`.
     *
     * Then, we store the **drag actor** in {@link #_dragActor} and its
     * **drag actor source** in {@link #_dragActorSource}.
     *
     * A draggable's **drag actor** is the actor that appears under the cursor (i.e.
     * is dragged by the cursor). By default this is simply the draggable's
     * source actor {@link #actor}. However, if `this.actor._delegate.getDragActor`
     * is implemented, this actor will be used instead.
     *
     * ![AppWellIcon - the top-level actor displays both the icon and the label, but the drag actor is just the icon.](pics/AppWellIcon.png)
     *
     * For example, the
     * {@link AppDisplay.AppWellIcon} (the icon + label that appear in the
     * apps tab of the Overview) has the draggable's actor {@link #actor}
     * being the St.Button that contains both the icon and label. It implements
     * {@link AppDisplay.AppWellIcon#getDragActor} to return *just* the icon
     * as its drag actor. This means that when one tries to drag an AppWellIcon,
     * just the icon will appear under the cursor (as opposed to the icon and
     * the text).
     *
     * A draggable's **drag actor source** is the actor to which the drag actor
     * will "snap back" to if a drag is cancelled. By default the drag actor
     * snaps back to the source actor {@link #actor}.
     *
     * However if the draggable has a custom drag actor, it may be desired to
     * snap it back to a different actor than {@link #actor}. Hence, if
     * the draggable has a custom drag actor *and* `this.actor_delegate.getDragActorSource`
     * is implemented, the result of that will be used as the drag actor's source.
     *
     * In the {@link AppDisplay.AppWellIcon} example above, since the drag actor
     * is the app icon (and not the text too), we want the drag actor source to
     * be just the app icon component of the draggable's source actor.
     * If we just used the draggable's source actor (icon plus label) as the
     * actor to snap back to, the icon (drag actor) would snap back to the
     * AppWellIcon but be misaligned with the icon on the AppWellIcon since
     * it would snap back to the centre of the AppWellIcon. By providing
     * just the icon part of the AppWellIcon as the drag actor's source,
     * we ensure that the icon (drag actor) appears to snap back to the exact
     * same position as the icon of the AppWellIcon.
     *
     * Anyhow, with all this in mind, we set:
     *
     * * {@link #_dragActor} to `this.actor._delegate.getDragActor()` if present,
     *   or {@link #actor} otherwise;
     * * {@link #_dragActorSource} to `this.actor._delegate.getDragActorSource()`
     *   if present *and* `this.actor._delegate.getDragActor` was implemented,
     *   {@link #actor} if `getDragActor` was implemented but `getDragActorSource`
     *   wasn't, or `undefined` otherwise (no custom drag actor).
     * 
     * If {@link #actor} is to be used as the drag actor, we also cache its
     * original scale, position and parent to ensure that we restore it to its
     * proper size and parent upon completion of the drag.
     *
     * Then we reparent {@link #_dragActor} to the global stage and raise it.
     * If {@link #_dragActorOpacity} and {@link #_dragActorMaxSize} were set
     * with parameters in the constructor, we ensure that {@link #_dragActor}'s
     * opacity and size are changed to reflect this.
     *
     * @param {number} stageX - X coordinate of event
     * @param {number} stageY - Y coordinate of event
     * @param {number} time - Event timestamp
     * @fires .drag-begin
     */
    startDrag: function (stageX, stageY, time) {
        currentDraggable = this;
        this._dragInProgress = true;

        this.emit('drag-begin', time);
        if (this._onEventId)
            this._ungrabActor();
        this._grabEvents();
        global.set_cursor(Shell.Cursor.DND_IN_DRAG);

        this._dragX = this._dragStartX = stageX;
        this._dragY = this._dragStartY = stageY;

        if (this.actor._delegate && this.actor._delegate.getDragActor) {
            /** The current drag actor (if we are in a drag). This is the actor
             * that is dragged around by the cursor (i.e. is always under the
             * cursor) as the user drags.
             *
             * Distinguish from {@link #actor}, the *source* actor for the
             * draggable (it is possible for the source actor to also be the
             * drag actor).
             *
             * For example, the drag actor could be an image that's part of the
             * source actor.
             * @type {?Clutter.Actor} */
            this._dragActor = this.actor._delegate.getDragActor(this._dragStartX, this._dragStartY);
            // Drag actor does not always have to be the same as actor. For example drag actor
            // can be an image that's part of the actor. So to perform "snap back" correctly we need
            // to know what was the drag actor source.
            if (this.actor._delegate.getDragActorSource) {
                /** The current drag actor's source. Only present if
                 * `this.actor._delegate.getDragActor` *and*
                 * `this.actor._delegate.getDragActorSource` are implemented.
                 *
                 * This is the actor that the drag actor should "snap back" to
                 * if the drag is cancelled (only relevant if the drag actor
                 * is different to the source actor {@link #actor}, hence
                 * the requirement that `this.actor._delegate.getDragActor`
                 * be implemented).
                 * @type {?Clutter.Actor} */
                this._dragActorSource = this.actor._delegate.getDragActorSource();
                // If the user dragged from the source, then position
                // the dragActor over it. Otherwise, center it
                // around the pointer
                let [sourceX, sourceY] = this._dragActorSource.get_transformed_position();
                let x, y;
                if (stageX > sourceX && stageX <= sourceX + this._dragActor.width &&
                    stageY > sourceY && stageY <= sourceY + this._dragActor.height) {
                    x = sourceX;
                    y = sourceY;
                } else {
                    x = stageX - this._dragActor.width / 2;
                    y = stageY - this._dragActor.height / 2;
                }
                this._dragActor.set_position(x, y);
            } else {
                this._dragActorSource = this.actor;
            }
            this._dragOrigParent = undefined;

            /** X Offset between the drag actor's position and the pointer.
             * @type {number} */
            this._dragOffsetX = this._dragActor.x - this._dragStartX;
            /** Y Offset between the drag actor's position and the pointer.
             * @type {number} */
            this._dragOffsetY = this._dragActor.y - this._dragStartY;
        } else {
            this._dragActor = this.actor;
            this._dragActorSource = undefined;
            /** If {@link #actor} is being also used as the drag actor (i.e.
             * `._delegate.getDragActor` was not implemented), this is its
             * parent. We store it to make sure we restore {@link #actor} back
             * to its proper position and parent upon completion of the drag.
             * @type {?Clutter.Actor} */
            this._dragOrigParent = this.actor.get_parent();
            /** If {@link #actor} is being also used as the drag actor (i.e.
             * `._delegate.getDragActor` was not implemented), this is its
             * X position within its parent before the drag starts.
             * We store it to make sure we restore {@link #actor} back
             * to its proper position and parent upon completion of the drag.
             * @type {number} */
            this._dragOrigX = this._dragActor.x;
            /** If {@link #actor} is being also used as the drag actor (i.e.
             * `._delegate.getDragActor` was not implemented), this is its
             * Y position within its parent before the drag starts.
             * We store it to make sure we restore {@link #actor} back
             * to its proper position and parent upon completion of the drag.
             * @type {number} */
            this._dragOrigY = this._dragActor.y;
            /** If {@link #actor} is being also used as the drag actor (i.e.
             * `._delegate.getDragActor` was not implemented), this is its
             * scale before the drag starts.
             * We store it to make sure we restore {@link #actor} back
             * to its proper size upon completion of the drag.
             * @type {number} */
            this._dragOrigScale = this._dragActor.scale_x;

            let [actorStageX, actorStageY] = this.actor.get_transformed_position();
            this._dragOffsetX = actorStageX - this._dragStartX;
            this._dragOffsetY = actorStageY - this._dragStartY;

            // Set the actor's scale such that it will keep the same
            // transformed size when it's reparented to the uiGroup
            let [scaledWidth, scaledHeight] = this.actor.get_transformed_size();
            this.actor.set_scale(scaledWidth / this.actor.width,
                                 scaledHeight / this.actor.height);
        }

        this._dragActor.reparent(Main.uiGroup);
        this._dragActor.raise_top();
        Shell.util_set_hidden_from_pick(this._dragActor, true);

        this._dragOrigOpacity = this._dragActor.opacity;
        if (this._dragActorOpacity != undefined)
            this._dragActor.opacity = this._dragActorOpacity;

        /** The X coordinate we should snap {@link #_dragActor} back to if
         * a drag is cancelled.
         * @type {number} */
        this._snapBackX = this._dragStartX + this._dragOffsetX;
        /** The Y coordinate we should snap {@link #_dragActor} back to if
         * a drag is cancelled.
         * @type {number} */
        this._snapBackY = this._dragStartY + this._dragOffsetY;
        /** The scale of {@link #_dragActor} before the drag starts, so that when
         * we snap back to our original position if the drag is cancelled, our
         * original scale is also restored.
         * @type {number} */
        this._snapBackScale = this._dragActor.scale_x;

        if (this._dragActorMaxSize != undefined) {
            let [scaledWidth, scaledHeight] = this._dragActor.get_transformed_size();
            let currentSize = Math.max(scaledWidth, scaledHeight);
            if (currentSize > this._dragActorMaxSize) {
                let scale = this._dragActorMaxSize / currentSize;
                let origScale =  this._dragActor.scale_x;
                let origDragOffsetX = this._dragOffsetX;
                let origDragOffsetY = this._dragOffsetY;

                // The position of the actor changes as we scale
                // around the drag position, but we can't just tween
                // to the final position because that tween would
                // fight with updates as the user continues dragging
                // the mouse; instead we do the position computations in
                // an onUpdate() function.
                Tweener.addTween(this._dragActor,
                                 { scale_x: scale * origScale,
                                   scale_y: scale * origScale,
                                   time: SCALE_ANIMATION_TIME,
                                   transition: 'easeOutQuad',
                                   onUpdate: function() {
                                       let currentScale = this._dragActor.scale_x / origScale;
                                       this._dragOffsetX = currentScale * origDragOffsetX;
                                       this._dragOffsetY = currentScale * origDragOffsetY;
                                       this._dragActor.set_position(this._dragX + this._dragOffsetX,
                                                                    this._dragY + this._dragOffsetY);
                                   },
                                   onUpdateScope: this });
            }
        }
    },

    /** Uses `event` to determine whether a drag should be started.
     *
     * Called from {@link #_onEvent} after the user has pressed the source
     * actor {@link #actor} and moved their mouse but not yet released it.
     *
     * If they moved their mouse more than a particular distance in any
     * direction, then we deem them to be trying to start a drag, so we
     * call {@link #startDrag} to start the drag.
     *
     * The threshold is 8 pixels, taken from GTK:
     *
     *     Gtk.Settings.get_default().gtk_dnd_drag_threshold
     *
     * @param {Clutter.Event} event - motion event.
     * @returns {boolean} true, since the event was handled.
     */
    _maybeStartDrag:  function(event) {
        let [stageX, stageY] = event.get_coords();

        // See if the user has moved the mouse enough to trigger a drag
        let threshold = Gtk.Settings.get_default().gtk_dnd_drag_threshold;
        if ((Math.abs(stageX - this._dragStartX) > threshold ||
             Math.abs(stageY - this._dragStartY) > threshold)) {
                this.startDrag(stageX, stageY, event.get_time());
                this._updateDragPosition(event);
        }

        return true;
    },

    /** Called whenevever the mouse position changes during a drag. The two
     * main purposes of this function are to update the drag actor's position
     * to be under the cursor, and to give each drag monitor or potential drop
     * target a chance to indicate to the user that it is a potential drop
     * target by modifying the cursor (see {@link DRAG_CURSOR_MAP}).
     *
     * We ensure that the drag actor's position is updated to reflect this
     * and store the new coordinates in {@link #_dragX} and {@link #_dragY}.
     *
     * Then we create a {@link DragEvent} for the drag, which is an object
     * with the current X and Y position of the cursor (properties `x` and `y`),
     * the current drag actor (`dragActor`), the source actor's delegate
     * (`source: this.actor._delegate`), and `targetActor`, being the actor
     * underneath the cursor (not including the drag actor).
     *
     * For each {@link DragMonitor} added with {@link addDragMonitor},
     * we see if it has a `dragMotion` callback. If it does, we call it.
     * If the result is **NOT** {@link DragMotionResult.CONTINUE}, we set the
     * cursor to match the result and exit (skipping all subsequent monitors).
     *
     * If all of the drag monitors returned CONTINUE to the drag event, then
     * we look at the actor underneath the cursor, `target` (excluding the drag actor).
     *
     * We walk up its parent chain, stopping each time to see if `target._delegate`
     * and `target._delegate.handleDragOver` was implemented. If so, we call it
     * to give it the chance to indicate that the draggable may be dropped here.
     * As before, if the result is **NOT** {@link DragMotionResult.CONTINUE},
     * we set the cursor to match the result (COPY, MOVE or NO_DROP) and exit,
     * preventing us from walking further up `target`'s parent tree.
     *
     * If we manage to walk all the way up `target`'s ancestors and still
     * they all returned `CONTINUE` (or didn't implement `handleDragOver`, we
     * set the cursor to the default `DND_IN_DRAG` one and return.
     *
     * @see {@link DragEvent} for information on the drag event passed to
     *      {@link DragMonitor.dragMotion} or {@link DropTarget.handleDragOver}.
     * @see {@link DragMonitor.dragMotion} and {@link DropTarget.handleDragOver}.
     * @see addDragMonitor
     * @inheritparams #_maybeStartDrag
     */
    _updateDragPosition : function (event) {
        let [stageX, stageY] = event.get_coords();
        /** The current X location of the cursor during a drag.
         * @type {number} */
        this._dragX = stageX;
        /** The current Y location of the cursor during a drag.
         * @type {number} */
        this._dragY = stageY;

        // If we are dragging, update the position
        if (this._dragActor) {
            this._dragActor.set_position(stageX + this._dragOffsetX,
                                         stageY + this._dragOffsetY);

            let target = this._dragActor.get_stage().get_actor_at_pos(Clutter.PickMode.ALL,
                                                                      stageX, stageY);

            // We call observers only once per motion with the innermost
            // target actor. If necessary, the observer can walk the
            // parent itself.
            let dragEvent = {
                x: stageX,
                y: stageY,
                dragActor: this._dragActor,
                source: this.actor._delegate,
                targetActor: target
            };
            for (let i = 0; i < dragMonitors.length; i++) {
                let motionFunc = dragMonitors[i].dragMotion;
                if (motionFunc) {
                    let result = motionFunc(dragEvent);
                    if (result != DragMotionResult.CONTINUE) {
                        global.set_cursor(DRAG_CURSOR_MAP[result]);
                        return true;
                    }
                }
            }
            while (target) {
                if (target._delegate && target._delegate.handleDragOver) {
                    let [r, targX, targY] = target.transform_stage_point(stageX, stageY);
                    // We currently loop through all parents on drag-over even if one of the children has handled it.
                    // We can check the return value of the function and break the loop if it's true if we don't want
                    // to continue checking the parents.
                    let result = target._delegate.handleDragOver(this.actor._delegate,
                                                                 this._dragActor,
                                                                 targX,
                                                                 targY,
                                                                 event.get_time());
                    if (result != DragMotionResult.CONTINUE) {
                        global.set_cursor(DRAG_CURSOR_MAP[result]);
                        return true;
                    }
                }
                target = target.get_parent();
            }
            global.set_cursor(Shell.Cursor.DND_IN_DRAG);
        }

        return true;
    },

    /** Callback when the user drops the draggable, called from {@link #_onEvent}.
     *
     * 1. See if any drag monitors wish to handle the drop.
     * 2. If none of them do, go through the actor(s) that the drop was on to
     *    see if any of them wish to handle the drop.
     * 3. If still nothing does, we cancel the drag.
     *
     * For the first step we create a {@link DropEvent} describing the drop,
     * and loop through the drag monitors seeing if any have implemented
     * {@link DragMonitor.dragDrop}. If so, we apply it and look at the result,
     * a {@link DragDropResult}. If it returned FAILURE or SUCCESS, we exit.
     * Otherwise, we keep going.
     *
     * For the second step, we get the actor directly under the cursor, `target`.
     * If `target._delegate.acceptDrop` has been implemented, we apply it.
     * If it returns `true` (i.e. it has accepted the drop):
     *
     * * if the drag actor is still in the global stage (the drop handler didn't
     *   reparent it), we restore it to its original position (if
     *   {@link #_restoreOnSuccess} says to) using {@link #_restoreDragActor}
     *   and return. Otherwise, we destroy the drag actor.
     * * we set the cursor back to normal and reset our state variables
     * * we emit ['drag-end']{@link .drag-end} with `event`'s timestamp.
     * * we call {@link #_dragComplete} to finsh off the drag.
     *
     * If we haven't returned by now, we then do the same on `target`'s parent.
     *
     * Finally, if none of `target`'s parents or the drag monitors handled the
     * drop, we just cancel the drag as there is no valid drop target here
     * ({@link #_cancelDrag}).
     *
     * @param {Clutter.Event} event - button release event
     * @returns {boolean} whether we handled the event - always `true` in this case.
     * @see DragMonitor.dragDrop
     * @see DropTarget#acceptDrop
     * @fires .drag-end
     */
    _dragActorDropped: function(event) {
        let [dropX, dropY] = event.get_coords();
        let target = this._dragActor.get_stage().get_actor_at_pos(Clutter.PickMode.ALL,
                                                                  dropX, dropY);

        // We call observers only once per motion with the innermost
        // target actor. If necessary, the observer can walk the
        // parent itself.
        let dropEvent = {
            dropActor: this._dragActor,
            targetActor: target,
            clutterEvent: event
        };
        for (let i = 0; i < dragMonitors.length; i++) {
            let dropFunc = dragMonitors[i].dragDrop;
            if (dropFunc)
                switch (dropFunc(dropEvent)) {
                    case DragDropResult.FAILURE:
                    case DragDropResult.SUCCESS:
                        return true;
                    case DragDropResult.CONTINUE:
                        continue;
                }
        }

        while (target) {
            if (target._delegate && target._delegate.acceptDrop) {
                let [r, targX, targY] = target.transform_stage_point(dropX, dropY);
                if (target._delegate.acceptDrop(this.actor._delegate,
                                                this._dragActor,
                                                targX,
                                                targY,
                                                event.get_time())) {
                    if (this._actorDestroyed)
                        return true;
                    // If it accepted the drop without taking the actor,
                    // handle it ourselves.
                    if (this._dragActor.get_parent() == Main.uiGroup) {
                        if (this._restoreOnSuccess) {
                            this._restoreDragActor(event.get_time());
                            return true;
                        } else
                            this._dragActor.destroy();
                    }

                    this._dragInProgress = false;
                    global.unset_cursor();
                    this.emit('drag-end', event.get_time(), true);
                    this._dragComplete();
                    return true;
                }
            }
            target = target.get_parent();
        }

        this._cancelDrag(event.get_time());

        return true;
    },

    /** Gets the position and scale that the drag actor should be snapped back
     * to (on cancellation of a drag) or faded back in at (on the handling of a drag).
     *
     * There's a bit of fancy footwork in case there's a custom drag actor source,
     * or in case the drag actor is positioned within a parent which may have
     * itself moved or changed scale.
     * @returns {number} the stage X coordinate to snap back to (first element of array)
     * @returns {number} the stage Y coordinate to snap back to (second element of array)
     * @returns {number} the (X) scale to snap back to (third element of array)
     */
    _getRestoreLocation: function() {
        let x, y, scale;

        if (this._dragActorSource && this._dragActorSource.visible) {
            // Snap the clone back to its source
            [x, y] = this._dragActorSource.get_transformed_position();
            let [sourceScaledWidth, sourceScaledHeight] = this._dragActorSource.get_transformed_size();
            scale = this._dragActor.width / sourceScaledWidth;
        } else if (this._dragOrigParent) {
            // Snap the actor back to its original position within
            // its parent, adjusting for the fact that the parent
            // may have been moved or scaled
            let [parentX, parentY] = this._dragOrigParent.get_transformed_position();
            let [parentWidth, parentHeight] = this._dragOrigParent.get_size();
            let [parentScaledWidth, parentScaledHeight] = this._dragOrigParent.get_transformed_size();
            let parentScale = 1.0;
            if (parentWidth != 0)
                parentScale = parentScaledWidth / parentWidth;

            x = parentX + parentScale * this._dragOrigX;
            y = parentY + parentScale * this._dragOrigY;
            scale = this._dragOrigScale * parentScale;
        } else {
            // Snap back actor to its original stage position
            x = this._snapBackX;
            y = this._snapBackY;
            scale = this._snapBackScale;
        }

        return [x, y, scale];
    },

    /** Cancels a drag.
     *
     * We emit ['drag-cancelled']{@link .drag-cancelled} with `eventTime`.
     *
     * If {@link #actor} has been destroyed in the meantime, we emit
     * ['drag-end']{@link .drag-end}, and if the mouse is no longer held down
     * we also call {@link #_dragComplete}.
     *
     * Otherwise we "snap" the drag actor back to its original position
     * (as determined by {@link #_getRestoreLocation}), calling {@link #_onAnimationComplete}
     * upon completion which will itself emit the ['drag-end']{@link .drag-end}
     * signal.
     *
     * While we snap the actor back we also make sure to restore its size
     * and opacity to match the source actor.
     * @param {number} eventTime - timestamp.
     * @fires .drag-cancelled
     * @fires .drag-end
     * @see {@link #_onAnimationComplete}, called when the snap back animation
     * is finished.
     */
    _cancelDrag: function(eventTime) {
        this.emit('drag-cancelled', eventTime);
        this._dragInProgress = false;
        let [snapBackX, snapBackY, snapBackScale] = this._getRestoreLocation();

        if (this._actorDestroyed) {
            global.unset_cursor();
            if (!this._buttonDown)
                this._dragComplete();
            this.emit('drag-end', eventTime, false);
            if (!this._dragOrigParent)
                this._dragActor.destroy();

            return;
        }

        this._animationInProgress = true;
        // No target, so snap back
        Tweener.addTween(this._dragActor,
                         { x: snapBackX,
                           y: snapBackY,
                           scale_x: snapBackScale,
                           scale_y: snapBackScale,
                           opacity: this._dragOrigOpacity,
                           time: SNAP_BACK_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._onAnimationComplete,
                           onCompleteScope: this,
                           onCompleteParams: [this._dragActor, eventTime]
                         });
    },

    /** Places the actor back at its original location and fades it in there.
     * Performed upon the successful handling of a drop.
     *
     * We get the target location/scale with {@link #_getRestoreLocation} and
     * put the drag actor there/scale it accordingly.
     *
     * Then we set the drag actor's opacity to 0 and fade it in, calling
     * {@link #_onAnimationComplete} upon completion (which will emit
     * ['drag-end']{@link .drag-end}).
     * @inheritparams #_cancelDrag
     */
    _restoreDragActor: function(eventTime) {
        this._dragInProgress = false;
        [restoreX, restoreY, restoreScale] = this._getRestoreLocation();

        // fade the actor back in at its original location
        this._dragActor.set_position(restoreX, restoreY);
        this._dragActor.set_scale(restoreScale, restoreScale);
        this._dragActor.opacity = 0;

        this._animationInProgress = true;
        Tweener.addTween(this._dragActor,
                         { opacity: this._dragOrigOpacity,
                           time: REVERT_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._onAnimationComplete,
                           onCompleteScope: this,
                           onCompleteParams: [this._dragActor, eventTime]
                         });
    },

    /** Called upon the completion of either the "snap back" animation where we
     * move the drag actor back to its original position upon the cancellation
     * of a drag, or the "restore" animation where we fade the drag actor back
     * in its original position after the handling of a drag ({@link #_cancelDrag}
     * and {@link #_restoreDragActor}).
     *
     * If the drag actor is the source actor, we reparent it back to its original
     * parent. Otherwise, we destroy it.
     *
     * We ensure the cursor is back to normal (rather than the drag cursor).
     *
     * We emit ['drag-end']{@link .drag-end} with `eventTime` and some boolean
     * that I can't work out the purpose of (set to `false`).
     *
     * Finally if {@link #_buttonDown} is `false` (the button has been released)
     * we call {@link #_dragComplete} to reset the state variables and tidy
     * things up.
     * @param {Clutter.Actor} dragActor - the drag actor, {@link #_dragActor}.
     * @inheritparams #_cancelDrag
     * @fires .drag-end
     */
    _onAnimationComplete : function (dragActor, eventTime) {
        if (this._dragOrigParent) {
            dragActor.reparent(this._dragOrigParent);
            dragActor.set_scale(this._dragOrigScale, this._dragOrigScale);
            dragActor.set_position(this._dragOrigX, this._dragOrigY);
        } else {
            dragActor.destroy();
        }
        global.unset_cursor();
        this.emit('drag-end', eventTime, false);

        this._animationInProgress = false;
        if (!this._buttonDown)
            this._dragComplete();
    },

    /** Helper function called from {@link #_dragComplete}. We call
     * `st_widget_sync_hover` on `actor` and all its ancestors where `actor`
     * is an actor we have entered or left during the drag.
     *
     * We do this to ensure the prelight/hover state of `actor` is updated since
     * we gobbled up all enter/leave events during the drag.
     * @param {Clutter.Actor} actor - an actor we have entered or left during
     * the drag.
     */
    _syncHover: function(actor) {
        while (actor) {
            let parent = actor.get_parent();
            if (actor instanceof St.Widget)
                actor.sync_hover();

            actor = parent;
        }
    },

    /** Called upon completion of a drag, to tidy up loose ends -
     *
     * * we release our grab on keyboard/mouse events ({@link #_ungrabEvents}),
     * * we ensure that the actors we started/ended the drag on have their
     *   prelights/hover states fixed up (since we blocked all the enter/leave
     *   events during the drag so they wouldn't be fixed up naturally),
     * * we unset {@link #_dragActor}.
     */
    _dragComplete: function() {
        if (!this._actorDestroyed)
            Shell.util_set_hidden_from_pick(this._dragActor, false);

        this._ungrabEvents();

        if (this._firstLeaveActor) {
            this._syncHover(this._firstLeaveActor);
            this._firstLeaveActor = null;
        }

        if (this._lastEnterActor) {
            this._syncHover(this._lastEnterActor);
            this._lastEnterActor = null;
        }

        this._dragActor = undefined;
        currentDraggable = null;
    }
};
Signals.addSignalMethods(_Draggable.prototype);
/** Emitted when a drag starts ({@link #startDrag}).
 * @param {_Draggable} d - the draggable that emitted the signal.
 * @param {number} time - the timestamp the drag started.
 * @event drag-begin
 * @see #startDrag
 * @memberof _Draggable */
/** Emitted when a drag ends. (is handled? what about drag monitors?)
 * @param {_Draggable} d - the draggable that emitted the signal.
 * @param {number} time - the timestamp the drag ended.
 * @param {boolean} snapback - `false` if the drag actor has been animated
 * snapping back to its original position or fading in to its original
 * position ({@link #_cancelDrag} and {@link #_restoreDragActor})...
 * **TODO:what is it used for???**
 * @event drag-end
 * @memberof _Draggable */
/** Emitted when a drag is cancelled.
 * @param {_Draggable} d - the draggable that emitted the signal.
 * @param {number} time - the timestamp the drag was cancelled.
 * @event drag-cancelled
 * @memberof _Draggable */

/** Create an object which controls drag and drop for the given actor.
 *
 * **This is the main function to be used from this module.**
 *
 * Note that when the drag actor is the source actor and the drop
 * succeeds, the actor scale and opacity aren't reset; if the drop
 * target wants to reuse the actor, it's up to the drop target to
 * reset these values.
 *
 * The function creates a {@link #_Draggable} for `actor` and returns it.
 * One may store this to listen to its signals
 * (['drag-end']{@link _Draggable.drag-end},
 * ['drag-begin']{@link _Draggable.drag-begin},
 * ['drag-cancelled']{@link _Draggable.drag-cancelled}).
 *
 * Also, it is prodent to set `actor._delegate` to the class that owns `actor`.
 * In particular, if the class implements the [Draggable API]{@link Draggable}
 * (functions [getDragActor]{@link Draggable#getDragActor} and
 * [getDragActorSource]{@link Draggable#getDragActorSource}) to make the drag
 * actor different to `actor`, this is **mandatory**.
 *
 * @returns {_Draggable} a {@link _Draggable} for `actor` with `params`.
 * @example<caption>Making a class draggable</caption>
 *     // suppose this.actor is the Clutter.Actor we want to make draggable.
 *     this._draggable = DND.makeDraggable(this.actor);
 *     // you MUST do this if you want getDragActor/getDragActorSource to work:
 *     this.actor._delegate = this;
 *
 * @inheritparams #_Draggable
 */
function makeDraggable(actor, params) {
    return new _Draggable(actor, params);
}

///// Various APIs and typedefs.

//// Drop Target API ////
/** Any class that wishes its actor to be a drop target should implement these functions
 * (well, any combination can be implemented depending on what you want the drop
 * target to respond to).
 *
 * For this to work, the actor that will be the drop target **must** have a
 * `._delegate` property pointing back to the class/object that contains the
 * `handleDragOver`/`acceptDrop` methods.
 *
 * If you wish to know about drags and drops but you **don't** want to be tied to
 * a particular drop target actor, use a [drag monitor]{@link DragMonitor} instead.
 *
 * This is not a real class in that it is not defined in the code of the `dnd.js` file.
 * However, it is documented as a mixin because that's what it is!
 * @mixin DropTarget
 * @example <caption>Allowing MyClass' actor to be a drop target</caption>
 * MyClass.prototype = {
 *     _init: function () {
 *         // suppose we have a this.actor representing MyClass which the user
 *         // can drop something onto.
 *
 *         // We MUST do the following for the handleDragOver/acceptDrop functions
 *         // to be found:
 *         this.actor._delegate = this;
 *     },
 *
 *     // Code implementing the DropTarget API:
 *
 *     handleDragOver: function (source, actor, x, y, time) {
 *         // return a DragMotionResult - NO_DROP, COPY_DROP, MOVE_DROP or CONTINUE
 *     },
 *
 *     acceptDrop: function (source, actor, x, y, time) {
 *         // return true if the drop is handled, false otherwise
 *     },
 *     // rest of code.
 * };
 * @see {@link Workspace.Workspace} for an example of a class that can handle dragovers
 * and drops: dropped window thumbnails will launch on that workspace, and dropped
 * application icons will open an instance on that workspace. Relevant functions
 * {@link Workspace.Workspace#handleDragOver} and {@link Workspace.Workspace#acceptDrop}
 * @see {@tutorial DND} for further information.
 */
/**
 * If the mouse moves during a drag over the actor for a class implementing the
 * {@link DropTarget} API (i.e. `actor._delegate` implements
 * {@link DropTarget#handleDragOver} and {@link DropTarget#acceptDrop}),
 * this function is called with the {@link DragEvent} describing the parameters
 * of the drag.
 *
 * The `handleDragOver` function gives `actor` (our potential drop target) the
 * chance to indicate whether it is a drop target or not. The mouse cursor is
 * updated to reflect this.
 *
 * @function handleDragOver
 * @param {?} source - the class of the drop target actor
 *  as given by `target._delegate` (where `target` is the innermost Clutter
 *  actor directly underneath the cursor). e.g. {@link Dash.Dash}.
 * @param {Clutter.Actor} actor - the actor being dragged.
 * @param {number} x - X coordinate of the pointer
 * @param {number} y - Y coordinate of the pointer
 * @param {number} time - the timestamp of the motion event.
 * @returns {DragMotionResult} a member of {@link DragMotionResult} that will be
 * used to modify the cursor (to indicate to the user whether the drag would
 * be accepted if dropped). Either:
 *
 * * {@link DragMotionResult.NO_DROP} - if the target were dropped here we would refuse to handle it.
 *   Keeps the pointer as the usual drag-and-drop cursor
 *   ![Shell.Cursor.DND_UNSUPPORTED_TARGET](pics/icons/dnd-none.png) **and**
 *   prevents any of {@link DragMotionResult.actor}'s ancestors from detecting the drag.
 * * {@link DragMotionResult.COPY_DROP} - the draggable would be copied here if dropped
 *   ![Shell.Cursor.DND_COPY](pics/icons/dnd-copy.png). An example
 *   is dragging an app's icon to the Dash that is not already in the app favourites -
 *   it will be copied to the app favourites.
 * * {@link DragMotionResult.MOVE_DROP} - the draggable would be moved here if dropped
 *   ![Shell.Cursor.DND_MOVE](pics/icons/dnd-move.png). An example
 *   is dragging an app's icon to the Dash that is already in the app favourites -
 *   it will be moved within the app favourites.
 * * {@link DragMotionResult.CONTINUE} - we are not a drop target for this draggable. Like `NO_DROP`,
 *   but allows ancestors of the current target to be checked for handling the
 *   drop (i.e. we *continue* on to check other potential drop targets).
 *
 * @memberof DropTarget#
 * @see {@link _Draggable#_updateDragPosition} which allows first drag monitors
 * and then the target actor and its ancestors the chance to handle the dragover.
 * @see DragMotionResult
 */
/**
 * If the user *drops* an actor over a target actor `target`, and
 * `target._delegate.acceptDrop` exists (i.e. `target._delegate` implements
 * the {@link DropTarget} API), this is called to handle the drop.
 *
 * If it returns a true, the drop is regarded as handled and we do not let any
 * ancestors of `target` try to handle it. Otherwise, we continue walking up
 * `target`'s ancestors giving them each a chance to handle the drop.
 *
 * This function should do whatever is expected to be done when the draggable
 * is dropped there (for example, when an app icon is dropped onto the dash,
 * its app should be added to the user's favourites).
 *
 * It is not necessary for `acceptDrop` to do anything with the drag actor
 * (though it can if it wants, for example you might reparent it to accept it
 * into the target actor). If `acceptDrop` doesn't reparent the drag actor,
 * {@link _Draggable} class handles the cleanup of the drag actor - be it
 * restoring it at its original position, or destroying it.
 * @function acceptDrop
 * @param {?} Source - the class of the drop target actor
 *  as given by `target._delegate` (where `target` is the innermost Clutter
 *  actor directly underneath the cursor). e.g. {@link Dash.Dash}.
 * @param {Clutter.Actor} actor - the actor being dragged.
 * @param {number} x - X coordinate of the pointer
 * @param {number} y - Y coordinate of the pointer
 * @param {number} time - the timestamp of the motion event.
 * @returns {boolean} `true` if the drop was accepted (we do not give any other
 * classes the chance to handle the drop), or `false` if we wish to continue walking
 * up the drop target's ancestry asking if any of the ancestors wishes to accept
 * the drop.
 *
 * Note that by contrast the {@link DragMonitor.dragDrop} function requires a
 * {@link DragDropResult} to be returned, whereby returning {@link DragDropResult.CONTINUE}
 * continues walking up the drop target's ancestry, and anything else will stop
 * the propogation of the event.
 * @memberof DropTarget#
 * @see {@link _Draggable#_dragActorDropped} which offers first drag monitors
 * and then the target actor and its ancestors the chance to accept the drop.
 */

//// Draggable API ////
/** Any class that has had one if its actors made draggable with {@link makeDraggable}
 * may additionally implement some extra functions to modify the look of the
 * drag actor.
 *
 * ![Example of a draggable](pics/dragEvent.png)
 *
 * By default the drag actor is the same as the source actor given to
 * {@link makeDraggable}.
 *
 * If the user wishes to modify this, they may mix in the
 * [getDragActor]{@link Draggable#getDragActor} and
 * [getDragActorSource]{@link Draggable#getDragActorSource} functions.
 *
 * This is not a real class in that it is not defined in the code of the `dnd.js` file.
 * However, it is documented as a mixin because that's what it is!
 * @mixin Draggable
 * @see {@link AppDisplay.AppWellIcon} as an example of making an icon draggable,
 * and {@link AppDisplay.AppWellIcon#getDragActor} and
 * {@link AppDisplay.AppWellIcon#getDragActorSource}.
 * @see {@tutorial DND} for further information and examples.
 */
/** If present, this will return an actor to be used as the draggable's *drag actor*.
 *
 * This is the actor that appears under the cursor whilst the user drags.
 *
 * The draggable's drag actor and source actor do not have to be the same.
 * For example, the drag actor could be an image that's part of the source actor.
 *
 * The drag actor will have its size/position modified and be reparented to the
 * global stage for the drag, so if you want to use a component of the draggable's
 * source actor as the drag actor (e.g. the AppWellIcon which is text plus icon
 * uses just the icon for the drag actor), you should ensure that the returned
 * actor is a clone or separate instance.
 * @function getDragActor
 * @param {number} stageX - X coordinate the draggable will be placed at, in case
 * this is useful in making the actor.
 * @param {number} stageY - Y coordinate the draggable will be placed at, in case
 * this is useful in making the actor.
 * @returns {Clutter.Actor} the actor to be used as the draggables drag actor.
 * @memberof Draggable#
 * @see {@link _Draggable#_dragActor}, where the draggable's drag actor is stored
 * in a drag.
 */
/** As mentioned in {@link Draggable#getDragActor}, a draggable's drag actor does not
 * necessarily have to be the same as the draggable's source actor.
 *
 * For example, the drag actor could be an image that's part of the source actor.
 *
 * To do the "snap back" animation (where the user cancels the drag and the
 * drag actor is animated moving back to the source actor's (original) position),
 * we need to know which actor to snap back to.
 *
 * By default (if this function is not implemented), the draggable's source actor
 * is used.
 *
 * If this function is implemented, it will override that actor that the drag
 * actor will snap back to.
 *
 * For example, consider the {@link AppDisplay.AppWellIcon} (the app icon and
 * label that appears in the apps tab of the overview).
 *
 * ![AppWellIcon - the top-level actor displays both the icon and the label](pics/AppWellIcon.png)
 *
 * The icon's top-level actor `this.actor` displays both the icon and the label.
 * It is made draggable with `this.actor` as the draggable's source actor.
 *
 * However when the item is dragged, we only wish the app's icon to be dragged/appear
 * under the cursor, not the label too. Hence {@link AppDisplay.AppWellIcon#getDragActor}
 * is implemented to return the application's icon only. Otherwise, the label
 * would have also been dragged with the icon.
 *
 * Then, we implement {@link AppDisplay.AppWellIcon#getDragActorSource} so that
 * when the drag is cancelled, the drag actor (the icon) snaps back to the
 * *icon part* of `this.actor`, as opposed to snapping back to the combined
 * icon and label (which would cause the drag actor (app icon) to be misaligned
 * with the app icon on the AppWellIcon, since `this.actor` has both the icon
 * and label).
 *
 * @function getDragActorSource
 * @param {number} stageX - X coordinate the draggable will be placed at, in case
 * this is useful in making the actor.
 * @param {number} stageY - Y coordinate the draggable will be placed at, in case
 * this is useful in making the actor.
 * @returns {Clutter.Actor}
 * @see {@link _Draggable#_dragActorSOurce}, where the draggable's drag actor's
 * source is stored in a drag.
 * @memberof Draggable#
 * @see {@tutorial DND} for further information and examples.
 */

//// Drag monitors ////
/**
 * Object used to monitor drag and drop operations.
 * This is similar to the [DropTarget API]{@link DropTarget}, but where you
 * want to *always* be given the chance to handle a dragover or accept a drop
 * (as opposed to only being given this chance when the draggable is over a
 * specific target actor).
 *
 * Its properties [dragMotion]{@link DragMonitor.dragMotion} and
 * [dragDrop]{@link DragMonitor.dragDrop} are analogues to {@link DropTarget#handleDragover}
 * and {@link DropTarget#acceptDrop}. They are called whenever a draggable is
 * moved/dragged and whenever a draggable is dropped respectively.
 *
 * To add a monitor, use {@link addDragMonitor}.
 *
 * Note that **drag monitors get a chance to handle dragovers and accept drops *before*
 * anything implementing the [DropTarget API]{@link DropTarget}**.
 * @typedef DragMonitor
 * @type {Object}
 * @example <caption>Making a drag monitor</caption>
 * const DND = imports.ui.dnd;
 * let dragMonitor = {
 *     dragDrop: function(DropEvent) {
 *         // should return a DragDropResult
 *     },
 *     dragMotion: function(DropEvent) {
 *         // should return a DragMotionResult
 *     }
 * };
 *
 * // add the monitor
 * DND.addDragMonitor(dragMonitor);
 * @TODO want this typedef to show up globally.
 * @see {@tutorial DND} for further information and examples.
 */
/**
 * This function is called whenever the position of a draggable changes during
 * a drag (regardless of which actor the draggable is currently over).
 *
 * Depending on the result returned, the mouse cursor is changed to give a hint
 * to the user as to what would happen if they dropped the draggable over the
 * current position.
 *
 * @function dragMotion
 * @memberof DragMonitor
 * @param {DragEvent} dragEvent - an object containing the draggable's current
 * position, a reference to the drag actor, a reference to the class the drag actor
 * represents (e.g. if `MyClass.actor` is the drag actor, then this will be `MyClass`),
 * and a reference to the actor that the draggable is currently hovering over (i.e.
 * the potential drop target).
 *
 * @returns {DragMotionResult} a member of {@link DragMotionResult} that will be
 * used to modify the cursor (to indicate to the user whether the drag would
 * be accepted if dropped). Either:
 *
 * * {@link DragMotionResult.NO_DROP} - if the target were dropped here we would refuse to handle it.
 *   Keeps the pointer as the usual drag-and-drop cursor
 *   ![Shell.Cursor.DND_UNSUPPORTED_TARGET](pics/icons/dnd-none.png) **and**
 *   prevents any of {@link DragMotionResult.actor}'s ancestors from detecting the drag.
 * * {@link DragMotionResult.COPY_DROP} - the draggable would be copied here if dropped
 *   ![Shell.Cursor.DND_COPY](pics/icons/dnd-copy.png). An example
 *   is dragging an app's icon to the Dash that is not already in the app favourites -
 *   it will be copied to the app favourites.
 * * {@link DragMotionResult.MOVE_DROP} - the draggable would be moved here if dropped
 *   ![Shell.Cursor.DND_MOVE](pics/icons/dnd-move.png). An example
 *   is dragging an app's icon to the Dash that is already in the app favourites -
 *   it will be moved within the app favourites.
 * * {@link DragMotionResult.CONTINUE} - we are not a drop target for this draggable. Like `NO_DROP`,
 *   but allows ancestors of the current target to be checked for handling the
 *   drop (i.e. we *continue* on to check other potential drop targets).
 *
 * @see {@link DropTarget#handleDragOver}, the analogue to this function for
 * classes implementing the {@link DropTarget} API.
 * @see {@link _Draggable#_updateDragPosition} which allows first drag monitors
 * and then the target actor and its ancestors the chance to handle the dragover.
 * @see Example: {@link WorkspacesView.WorkspacesDisplay#_onDragMotion}, used
 * as the `dragMotion` property of a drag monitor in that class. If anything
 * is dragged over any actor in the right-hand sidebar of the overview, this
 * ensures that the right-hand sidebar is expanded out.
 * @see Example: {@link Overview#_onDragMotion}, used as the `dragMotion` property of a
 * drag monitor. The drag monitor is added when an XDND drag begins an removed
 * when it ends. The function is used to allow the user to hover an XDND object
 * over a window in the Overview to activate that window without cancelling the
 * drag, allowing the user to drop onto that window.
 */
/**
 * This function is called whenever a draggable is dropped (regardless of the
 * actor the draggable was dropped onto).
 *
 * This function should do whatever is expected to be done when the draggable
 * is dropped there (for example, when an app icon is dropped onto the dash,
 * its app should be added to the user's favourites).
 *
 * It is not necessary for `dragDrop` to do anything with the drag actor
 * (though it can if it wants, for example you might reparent it to accept it
 * into the target actor).
 *
 * If the function returns {@link DragDropResult.FAILURE} or {@link DragDropResult.SUCESS},
 * the drop is considered handled and no other monitors get a chance at it.
 *
 * Otherwise all the other drag monitors are given a chance to handle it and then
 * the target actor and its ancestors, stopping as soon as one of these handles
 * the drop.
 *
 * @function dragDrop
 * @memberof DragMonitor
 * @param {DropEvent} dropEvent - an object with a reference to the drag actor,
 * the target actor, and the button-release event (a Clutter.Event) of the drop.
 * @returns {DragDropResult} a member of {@link DragDropResult} which determines
 * whether the drop was handled:
 *
 * * {@link DragDropResult.SUCCESS} - we accepted the drop and it succeeded.
 *   Stop offering any other monitors or classes the chance to accept the drop.
 * * {@link DragDropResult.FAILURE} - we accepted the drop but it failed.
 *   Stop offering any other monitors or classes the chance to accept the drop.
 * * {@link DragDropResult.CONTINUE} - we do not handle this drop. Move on to the
 *   next drag monitor/DropTarget class and give it the chance to accept the drop.
 *
 * @see {@link DropTarget#acceptDrop}, the analogue to this function for
 * classes implementing the {@link DropTarget} API (except that that returns
 * a boolean and this returns a {@link DragDropResult}).
 * @see {@link _Draggable#_dragActorDropped} which offers first drag monitors
 * and then the target actor and its ancestors the chance to accept the drop.
 */

//// Drag/Drop events ////
/**
 * Object used to describe a drag event between gnome-shell items.
 * Used by the `dragMotion` member of a drag monitor.
 *
 * ![Example of a draggable](pics/dragEvent.png)
 *
 * The above is an example of dragging icons around the Dash.
 * 
 * @typedef DragEvent
 * @type {Object}
 * @property {number} x - the x position of the cursor (relative to the stage).
 * @property {number} y - the y position of the cursor (relative to the stage).
 * @property {Clutter.Actor} dragActor - the actor that is being dragged around
 * by/is following the cursor (labelled as 'drag actor' in the image above).
 *
 * Draggable objects can define a function `getDragActor` if they wish the
 * drag actor to not be the same as the source actor; if the object did not
 * define this function, then the drag actor is simply the source actor.
 * @property {?} source - the draggable's actor's delegate (i.e. `this.actor._delegate`
 * where `this.actor` is {@link _Draggable#actor}). A GNOME-shell class. In the
 * example above, this is {@link AppDisplay.AppWellIcon}.
 * @property {Clutter.Actor} targetActor - the actor that the draggable is
 * currently over/going to be dropped on (in the example/image above, the Dash).
 */
/**
 * Object used to describe a drop event between gnome-shell items.
 * Used by the `dragDrop` member of a drag monitor.
 *
 * @typedef DropEvent
 * @type {Object}
 * @property {Clutter.Actor} dropActor - The drag actor that is to be dropped,
 * {@link _Draggable#_dragActor}.
 * @property {Clutter.Actor} targetActor - the actor that the drag actor is
 * over. (Uses `global.stage.get_actor_at_pos`, so it will return the innermost
 * such actor). Walk up the ancestor tree if you wish.
 * @property {Clutter.Event} clutterEvent - the button-release event that triggered
 * the drop.
 */
