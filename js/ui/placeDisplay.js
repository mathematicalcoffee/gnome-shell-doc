// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Handles searching for Places (Home, Network, mounted volumes, ...) in the
 * overview.
 * @todo pictures
 */

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Shell = imports.gi.Shell;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;
const St = imports.gi.St;

const DND = imports.ui.dnd;
const Main = imports.ui.main;
const Params = imports.misc.params;
const Search = imports.ui.search;
const Util = imports.misc.util;

/**
 * Creates a new PlaceInfo.
 * @classdesc
 * Represents a place object, which is most normally a bookmark entry,
 * a mount/volume, or a special place like the Home Folder, Desktop, or
 * "Connect to...".
 *
 * @param {string} id - ID for the place (like 'special:home' or 'mount:file:///media/MY_USB'
 * or 'bookmark:file:///home/mathematical.coffee/Pictures').
 * @param {string} name - String title for the place ('Home', or 'MY_USB',
 * or 'Pictures').
 * @param {function(number): icon} iconFactory - A JavaScript callback which
 * takes in a size parameter and produces an icon texture of that size.
 * @param {function()} launch - A JavaScript callback to launch the entry
 * (like opening the location in the default browser).
 * @class
 */
function PlaceInfo(id, name, iconFactory, launch) {
    this._init(id, name, iconFactory, launch);
}

PlaceInfo.prototype = {
    _init: function(id, name, iconFactory, launch) {
        /** ID of the Place (e.g. 'special:home').
         * @type {string} */
        this.id = id;
        /** Name of the place (e.g. 'Home').
         * @type {string} */
        this.name = name;
        this._lowerName = name.toLowerCase();
        /** Callback that takes in a number being the size of the icon,
         * and produces an icon. Like the {@link IconGrid.BaseIcon#createIcon}
         * or the 'createIcon' member of {@link Search.ResultMeta}.
         * @function
         * @inheritparams IconGrid.BaseIcon#createIcon
         */
        this.iconFactory = iconFactory;
        /** Callback when the user wants to launch this Place. For example it
         * might open the place in the file browser.
         * @function
         */
        this.launch = launch;
    },

    /** Describes how this place matches the given search terms. This compares
     * against the lower-case place name `this.name.toLower()`.
     *
     * If any term matches the start of the place name, it returns
     * {@link Search.MatchType.PREFIX}. Otherwise if any term matches against
     * part of the place name but none matches from the start, it returns
     * {@link Search.MatchType.SUBSTRING}. If none of these apply it returns
     * {@link Search.MatchType.NONE} (so it doesn't make use of the
     * `MULTIPLE_(PREFIX|SUBSTRING)` options).
     * @param {string[]} terms - search terms to compare against this place.
     * @returns {Search.MatchType} the level of match. Will only return
     * `Search.MatchType.NONE`, `PREFIX` or `SUBSTRING`.
     */
    matchTerms: function(terms) {
        let mtype = Search.MatchType.NONE;
        for (let i = 0; i < terms.length; i++) {
            let term = terms[i];
            let idx = this._lowerName.indexOf(term);
            if (idx == 0) {
                mtype = Search.MatchType.PREFIX;
            } else if (idx > 0) {
                if (mtype == Search.MatchType.NONE)
                    mtype = Search.MatchType.SUBSTRING;
            } else {
                return Search.MatchType.NONE;
            }
        }
        return mtype;
    },

    /** Returns whether this Place is unmountable.
     * False for all bookmarks and special places.
     * @returns {boolean} whether the place is unmountable. False for all
     * bookmarks and special places. */
    isRemovable: function() {
        return false;
    }
};

/** Helper function to translate launch parameters into a
 * {@link Gio.AppLaunchContext}.
 *
 * This creates a AppLaunchContext and sets the timestamp and opening workspace
 * to the specified values, and then returns the launch context.
 *
 * Use it with
 * [`Gio.app_info_launch_default_for_uri`](http://developer.gnome.org/gio/stable/GAppInfo.html#g-app-info-launch-default-for-uri)
 * to launch the appropriate program with these parameters.
 *
 * @inheritparams SearchDisplay.SearchResult#shellWorkspacelaunch
 * @returns {Gio.AppLaunchContext}
 * @see PlaceDeviceInfo#launch
 */
function _makeLaunchContext(params)
{
    params = Params.parse(params, { workspace: -1,
                                    timestamp: 0 });

    let launchContext = global.create_app_launch_context();
    if (params.workspace != -1)
        launchContext.set_desktop(params.workspace);
    if (params.timestamp != 0)
        launchContext.set_timestamp(params.timestamp);

    return launchContext;
}

/** creates a new PlaceDeviceInfo
 * @param {Gio.Mount} mount - the mount
 * to create a {@link PlaceDeviceInfo} for.
 * @classdesc
 * A PlaceDeviceInfo contains information about a mounted volume/device/drive.
 *
 * It is an extension of the {@link PlaceInfo}, where the `iconFactory` function
 * gets the default icon for that file type, and the `launch` function uses
 * `Gio.app_info_launch_default_for_uri` to launch the file with the appropriate
 * program.
 * @class
 * @extends PlaceInfo
 */
function PlaceDeviceInfo(mount) {
    this._init(mount);
}

PlaceDeviceInfo.prototype = {
    __proto__: PlaceInfo.prototype,

    _init: function(mount) {
        /** The Mount backing the PlaceDeviceInfo.
         * @type {Gio.Mount} */
        this._mount = mount;
        this.name = mount.get_name();
        this._lowerName = this.name.toLowerCase();
        this.id = 'mount:' + mount.get_root().get_uri();
    },

    /** 
     * Overrides the parent function (so that it need not be provided as
     * an input argument) to make an icon for the device.
     * Uses `mount.get_icon`.
     * @override
     */
    iconFactory: function(size) {
        let icon = this._mount.get_icon();
        return St.TextureCache.get_default().load_gicon(null, icon, size);
    },

    /** Overrides the parent function to launch the device in the file browser.
     *
     * The helper function {@link _makeLaunchContext} is used to convert the
     * input parameters (timestamp/workspace to launch on) into a
     * {@link Gio.AppLaunchContext}
     * used to open the file browser on a particular workspace, say.
     * @inheritparams SearchDisplay.SearchResult#shellWorkspaceLaunch
     * @override
     */
    launch: function(params) {
        Gio.app_info_launch_default_for_uri(this._mount.get_root().get_uri(),
                                            _makeLaunchContext(params));
    },

    /** Overrides the parent function (which always returns `false`) to return
     * whether the mount is unmountable (can be removed).
     * @returns {boolean} whether the mount can be unmounted (`mount.can_umount()`).
     * @override
     */
    isRemovable: function() {
        return this._mount.can_unmount();
    },

    /** Removes the device (if it is removable). Either calls `eject` if the
     * mount supports it or `unmount` otherwise, calling
     * {@link #_removeFinish} when done (as the operation is
     * asynchronous). */
    remove: function() {
        if (!this.isRemovable())
            return;

        if (this._mount.can_eject())
            this._mount.eject(0, null, Lang.bind(this, this._removeFinish));
        else
            this._mount.unmount(0, null, Lang.bind(this, this._removeFinish));
    },

    /** Callback when the device has finished unmounting/ejecting.
     *
     * Calls `mount.eject_finish` or `mount.unmount_finish`, whichever is relevant.
     *
     * If this fails for some reason, {@link Overview.Overview#setMessage} is
     * used to notify the user with a "Retry" option.
     * @see Overview.Overview#setMessage
     * @param {Gio.Mount} o - mount that was attempted to be ejected/unmounted
     * @param {Gio.AsyncResult} res - result of the operation (need to to call
     * `eject_finish` or `unmount_finish`).
     * @param {?} data - user data
     */
    _removeFinish: function(o, res, data) {
        try {
            if (this._mount.can_eject())
                this._mount.eject_finish(res);
            else
                this._mount.unmount_finish(res);
        } catch (e) {
            let message = _("Failed to unmount '%s'").format(o.get_name());
            Main.overview.setMessage(message,
                                     Lang.bind(this, this.remove),
                                     _("Retry"));
        }
    }
};

/** creates a new PlacesManager
 *
 * This first loads all the default places 'special:home', 'special:desktop'
 * and 'special:connect'. The first is the user's home directory, the second
 * is the desktop directory, and the last is the 'Connect to...' dialog (to connect
 * to a remote drive). A {@link PlaceInfo} is made for each of these, and
 * they are stored in {@link Placesmanager#_defaultPlaces}.
 *
 * Then the default {@link Gio.VolumeMonitor}
 * instance is stored in {@link #_volumeMonitor} and
 * {@link #_updateDevices} is called to populate
 * {@link #_mounts} with {@link PlaceDeviceInfo}s for each attached
 * volume.
 *
 * Finally {@link #_reloadBookmarks} is called to update
 * {@link #_bookmarks} with {@link PlaceInfo}s for each of the
 * user's bookmarks (in `$HOME/.gtk-bookmarks`).
 * @classdesc
 * A PlacesManager manages many places. A place falls into three categories:
 *
 * * special: special places - home, desktop, and connect ("Connect to...").
 * * bookmark: Places that have been bookmarked in your file browser --- stored
 * in the file `$HOME/.gtk-bookmarks`.
 * * mount: mounted volumes/drives.
 *
 * The PlacesManager stores {@link PlaceInfo}s (or {@link PlaceDeviceInfo}s) for
 * each of these places and allows them to be looked up by the place ID.
 *
 * A place ID consists of its category followed by a colon and then the place ID,
 * for example 'special:home', 'bookmark:file:///home/mathematical.coffee/Pictures',
 * or 'mount:file:///media/MY_USB'.
 *
 * You only need one instance of this; it is stored in {@link Main.placesManager}.
 *
 * It makes use of a {@link Gio.VolumeMonitor} to keep track of connected volumes.
 * @class
 */
function PlacesManager() {
    this._init();
}

PlacesManager.prototype = {
    _init: function() {
        /** Array of {@link PlaceInfo}s for the default places - home directory
         * (ID 'special:home'), desktop (ID 'special:desktop') and
         * the 'Connect to...' dialog (ID 'special:connect').
         * @type {PlaceDisplay.PlaceInfo[]} */
        this._defaultPlaces = [];
        /** Array of {@link PlaceDeviceInfo}s for mounted volumes/drives
         * (ID 'mount:[mount URI]') as found by {@link #_volumeMonitor}.
         * @type {PlaceDisplay.PlaceDeviceInfo[]} */
        this._mounts = [];
        /** Array of {@link PlaceInfo}s for each of the user's bookmarked
         * places as appears in `$HOME/.gtk-bookmarks` (ID 'bookmark:[URI]).
         * @type {PlaceDisplay.PlaceInfo[]} */
        this._bookmarks = [];

        let homeFile = Gio.file_new_for_path (GLib.get_home_dir());
        let homeUri = homeFile.get_uri();
        let homeLabel = Shell.util_get_label_for_uri (homeUri);
        let homeIcon = Shell.util_get_icon_for_uri (homeUri);
        /** PlaceInfo for the 'special:home' (home folder) place. The icon
         * is `Shell.util_get_icon_for_uri` (whatever the home folder icon is),
         * and the launch callback opens the home directory using
         * `Gio.app_info_launch_default_for_uri`
         * @type {PlaceDisplay.PlaceInfo} */
        this._home = new PlaceInfo('special:home', homeLabel,
            function(size) {
                return St.TextureCache.get_default().load_gicon(null, homeIcon, size);
            },
            function(params) {
                Gio.app_info_launch_default_for_uri(homeUri, _makeLaunchContext(params));
            });

        let desktopPath = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP);
        let desktopFile = Gio.file_new_for_path (desktopPath);
        let desktopUri = desktopFile.get_uri();
        let desktopLabel = Shell.util_get_label_for_uri (desktopUri);
        let desktopIcon = Shell.util_get_icon_for_uri (desktopUri);
        /** PlaceInfo for the 'special:desktop' (desktop folder) place. The icon
         * is `Shell.util_get_icon_for_uri` (whatever the desktop folder icon is),
         * and the launch callback opens the desktop directory using
         * `Gio.app_info_launch_default_for_uri`
         * @type {PlaceDisplay.PlaceInfo} */
        this._desktopMenu = new PlaceInfo('special:desktop', desktopLabel,
            function(size) {
                return St.TextureCache.get_default().load_gicon(null, desktopIcon, size);
            },
            function(params) {
                Gio.app_info_launch_default_for_uri(desktopUri, _makeLaunchContext(params));
            });

        /** PlaceInfo for the 'special:connect' ("Connect to...") place. The icon
         * is the 'applications-internet' icon in full colour, and the launch
         * callback runs command `nautilus-connect-server`.
         * @type {PlaceDisplay.PlaceInfo} */
        this._connect = new PlaceInfo('special:connect', _("Connect to..."),
            function (size) {
                return new St.Icon({ icon_name: 'applications-internet',
                                     icon_type: St.IconType.FULLCOLOR,
                                     icon_size: size });
            },
            function (params) {
                // BUG: nautilus-connect-server doesn't have a desktop file, so we can't
                // launch it with the workspace from params. It's probably pretty rare
                // and odd to drag this place onto a workspace in any case

                Util.spawn(['nautilus-connect-server']);
            });

        this._defaultPlaces.push(this._home);
        this._defaultPlaces.push(this._desktopMenu);
        this._defaultPlaces.push(this._connect);

        /*
        * Show devices, code more or less ported from nautilus-places-sidebar.c
        */
        /** Instance of the default Gio.VolumeMonitor, used to create
         * {@link PlaceDeviceInfo}s for each volume/drive.
         * @type {Gio.VolumeMonitor} */
        this._volumeMonitor = Gio.VolumeMonitor.get();
        this._volumeMonitor.connect('volume-added', Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('volume-removed',Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('volume-changed', Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('mount-added', Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('mount-removed', Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('mount-changed', Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('drive-connected', Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('drive-disconnected', Lang.bind(this, this._updateDevices));
        this._volumeMonitor.connect('drive-changed', Lang.bind(this, this._updateDevices));
        this._updateDevices();

        this._bookmarksPath = GLib.build_filenamev([GLib.get_home_dir(), '.gtk-bookmarks']);
        this._bookmarksFile = Gio.file_new_for_path(this._bookmarksPath);
        let monitor = this._bookmarksFile.monitor_file(Gio.FileMonitorFlags.NONE, null);
        this._bookmarkTimeoutId = 0;
        monitor.connect('changed', Lang.bind(this, function () {
            if (this._bookmarkTimeoutId > 0)
                return;
            /* Defensive event compression */
            this._bookmarkTimeoutId = Mainloop.timeout_add(100, Lang.bind(this, function () {
                this._bookmarkTimeoutId = 0;
                this._reloadBookmarks();
                return false;
            }));
        }));

        this._reloadBookmarks();
    },

    /** Repopulates {@link #_mounts}, adding first all connected
     * drives, then all volumes, and then mounts via {@link #_addMount}.
     *
     * This is also called whenever the 'volume-added', 'volume-removed',
     * 'volume-changed', 'mount-added', 'mount-removed', 'mount-changed',
     * 'drive-connected', 'drive-disconnected', and 'drive-changed' signals
     * are fired from {@link #_mounts}.
     *
     * We then emit two signals, one for a generic 'all places' update
     * ({@link .places-updated})
     * and the other for one specific to mounts ({@link .mounts-updated}).
     * We do this because
     * clients like PlaceDisplay may only care about places in general
     * being updated while clients like DashPlaceDisplay care which
     * specific type of place got updated.
     *
     * Note for those interested - according to the Gio docs, a
     * 'drive' is a physical piece of hardware connected to the machine which
     * may have multiple Volumes. A Volume can be mounted to produce a Mount,
     * but a Mount doesn't necessarily have an associated volume (ones
     * in `etc/mtab`, 'ftp', 'sftp'). (????)
     *
     * @see #_addMount
     * @fires .mounts-updated
     * @fires .places-updated
     */
    _updateDevices: function() {
        this._mounts = [];

        /* first go through all connected drives */
        let drives = this._volumeMonitor.get_connected_drives();
        for (let i = 0; i < drives.length; i++) {
            let volumes = drives[i].get_volumes();
            for(let j = 0; j < volumes.length; j++) {
                let mount = volumes[j].get_mount();
                if(mount != null) {
                    this._addMount(mount);
                }
            }
        }

        /* add all volumes that is not associated with a drive */
        let volumes = this._volumeMonitor.get_volumes();
        for(let i = 0; i < volumes.length; i++) {
            if(volumes[i].get_drive() != null)
                continue;

            let mount = volumes[i].get_mount();
            if(mount != null) {
                this._addMount(mount);
            }
        }

        /* add mounts that have no volume (/etc/mtab mounts, ftp, sftp,...) */
        let mounts = this._volumeMonitor.get_mounts();
        for(let i = 0; i < mounts.length; i++) {
            if(mounts[i].is_shadowed())
                continue;

            if(mounts[i].get_volume())
                continue;

            this._addMount(mounts[i]);
        }

        /* We emit two signals, one for a generic 'all places' update
         * and the other for one specific to mounts. We do this because
         * clients like PlaceDisplay may only care about places in general
         * being updated while clients like DashPlaceDisplay care which
         * specific type of place got updated.
         */
        this.emit('mounts-updated');
        this.emit('places-updated');

    },

    /** Repopulates {@link #_bookmarks} with a {@link PlaceInfo}
     * for each bookmark in `$HOME/.gtk-bookmarks`. Called upon initialisation
     * and whenever that file changes.
     *
     * We emit two signals, 'places-updated' (any place updated) and
     * 'bookmarks-updated' (bookmarks in particular). See comment in
     * {@link #_updateDevices} for why there are two signals.
     * @fires .bookmarks-updated
     * @fires .places-updated
     */
    _reloadBookmarks: function() {

        this._bookmarks = [];

        if (!GLib.file_test(this._bookmarksPath, GLib.FileTest.EXISTS))
            return;

        let bookmarksContent = Shell.get_file_contents_utf8_sync(this._bookmarksPath);

        let bookmarks = bookmarksContent.split('\n');

        let bookmarksToLabel = {};
        let bookmarksOrder = [];
        for (let i = 0; i < bookmarks.length; i++) {
            let bookmarkLine = bookmarks[i];
            let components = bookmarkLine.split(' ');
            let bookmark = components[0];
            if (bookmark in bookmarksToLabel)
                continue;
            let label = null;
            if (components.length > 1)
                label = components.slice(1).join(' ');
            bookmarksToLabel[bookmark] = label;
            bookmarksOrder.push(bookmark);
        }

        for (let i = 0; i < bookmarksOrder.length; i++) {
            let bookmark = bookmarksOrder[i];
            let label = bookmarksToLabel[bookmark];
            let file = Gio.file_new_for_uri(bookmark);
            if (!file.query_exists(null))
                continue;
            if (label == null)
                label = Shell.util_get_label_for_uri(bookmark);
            if (label == null)
                continue;
            let icon = Shell.util_get_icon_for_uri(bookmark);

            let item = new PlaceInfo('bookmark:' + bookmark, label,
                function(size) {
                    return St.TextureCache.get_default().load_gicon(null, icon, size);
                },
                function(params) {
                    Gio.app_info_launch_default_for_uri(bookmark, _makeLaunchContext(params));
                });
            this._bookmarks.push(item);
        }

        /* See comment in _updateDevices for explanation why there are two signals. */
        this.emit('bookmarks-updated');
        this.emit('places-updated');
    },

    /** Adds a {@link PlaceDeviceInfo} for a mount and adds it to
     * {@link #_mounts}.
     * @param {Gio.Mount} mount - the mount
     * to create a {@link PlaceDeviceInfo} for.
     */
    _addMount: function(mount) {
        let devItem = new PlaceDeviceInfo(mount);
        this._mounts.push(devItem);
    },

    /** Gets a list of all the PlaceInfos in the manager - default places first,
     * then bookmarks, then mounts.
     * @see #getDefaultPlaces
     * @see #getBookmarks
     * @see #getMounts
     * @returns {PlaceDisplay.PlaceInfo[]} array of place infos for the default
     * places, bookmarks, and mounts (the mount PlaceInfos will be
     * {@link PlaceDeviceInfo}s).
     */
    getAllPlaces: function () {
        return this.getDefaultPlaces().concat(this.getBookmarks(), this.getMounts());
    },

    /** Gets all the default PlaceInfos (home, desktop, and 'connect to').
     * @returns {PlaceDisplay.PlaceInfo[]} array of place infos */
    getDefaultPlaces: function () {
        return this._defaultPlaces;
    },

    /** Gets all the bookmark PlaceInfos.
     * @returns {PlaceDisplay.PlaceInfo[]} array of place infos */
    getBookmarks: function () {
        return this._bookmarks;
    },

    /** Gets all the mount PlaceInfos.
     * @returns {PlaceDisplay.PlaceDeviceInfo[]} array of place infos */
    getMounts: function () {
        return this._mounts;
    },

    /** Looks up the index of a Place within an array by the Place's ID.
     *
     * The user should use {@link #lookupPlaceById} instead.
     * @param {PlaceDisplay.PlaceInfo[]} sourceArray - array to look in
     * (typically one of {@link #_defaultPlaces}, {@link #_bookmarks}
     * or {@link #_mounts}).
     * @param {string} id - ID of the place to look up (e.g. 'special:home').
     * @returns {number} -1 if place with ID `id` was not in `sourceArray`, and
     * otherwise the index of that PlaceInfo within `sourceArray` with the
     * same ID as `id`.
     */
    _lookupIndexById: function(sourceArray, id) {
        for (let i = 0; i < sourceArray.length; i++) {
            let place = sourceArray[i];
            if (place.id == id)
                return i;
        }
        return -1;
    },

    /** Gets the PlaceInfo for a place from its ID.
     * @param {string} id - ID of the place to look up (e.g. 'special:home').
     * @returns {PlaceDisplay.PlaceInfo} the PlaceInfo for the place identified
     * by `id`, or `undefined` if not found.
     * @see #_lookupIndexById
     */
    lookupPlaceById: function(id) {
        let colonIdx = id.indexOf(':');
        let type = id.substring(0, colonIdx);
        let sourceArray = null;
        if (type == 'special')
            sourceArray = this._defaultPlaces;
        else if (type == 'mount')
            sourceArray = this._mounts;
        else if (type == 'bookmark')
            sourceArray = this._bookmarks;
        return sourceArray[this._lookupIndexById(sourceArray, id)];
    },

    /** Removes a PlaceInfo from an array of PlaceInfos by its ID.
     *
     * @param {PlaceDisplay.PlaceInfo[]} sourceArray - array to remove from
     * (typically one of {@link #_defaultPlaces}, {@link #_bookmarks}
     * or {@link #_mounts}).
     * @param {string} id - ID of the place to remove (e.g. 'special:home').
     */
    _removeById: function(sourceArray, id) {
        sourceArray.splice(this._lookupIndexById(sourceArray, id), 1);
    }
};
Signals.addSignalMethods(PlacesManager.prototype);
/** Emitted when any of the places managed by the {@link PlacesManager} changes
 * (be it the bookmarks or mounts - special/default places don't change).
 * @event
 * @name places-updated
 * @memberof PlacesManager
 */
/** Emitted when any of the bookmarks managed by the
 * {@link PlacesManager} changes.
 * @event
 * @name bookmarks-updated
 * @memberof PlacesManager
 */
/** Emitted when any of the mounts/drives/volumes managed by the
 * {@link PlacesManager} changes
 * @event
 * @name mounts-updated
 * @memberof PlacesManager
 */


/** creates a new PlaceSearchProvider
 * Calls the parent constructor with title `_("PLACES & DEVICES")`.
 * @classdesc
 * Implements {@link Search.SearchProvider} to search through places and devices
 * (bookmarks, mounted drives/volumes and so on).
 *
 * The searching is all done using the global {@link PlacesManager} instance
 * stored in {@link Main.placesManager}.
 *
 * The result ID strings are prefixed with either 'special:', 'mount:' or
 * 'bookmark:' identifying what kind of Place it is, along with the relevant
 * URI afterwards.
 * @class
 * @extends Search.SearchProvider
 */
function PlaceSearchProvider() {
    this._init();
}

PlaceSearchProvider.prototype = {
    __proto__: Search.SearchProvider.prototype,

    _init: function() {
        Search.SearchProvider.prototype._init.call(this, _("PLACES & DEVICES"));
    },

    /** Implements the parent function to create metadata for a particular place.
     * This looks up the place info in the {@link Main.placesManager} and sets the
     * 'id' member to `resultId`, the 'name' to `placeInfo.name` and the
     * 'createIcon' function to the `placeInfo.iconFactory` function.
     * @param {string} resultId - result identifier string ('(bookmark|special|mount):further_id').
     * @see PlacesManager#lookupPlaceById
     * @see PlaceInfo#iconFactory
     * @override
     */
    getResultMeta: function(resultId) {
        let placeInfo = Main.placesManager.lookupPlaceById(resultId);
        if (!placeInfo)
            return null;
        return { 'id': resultId,
                 'name': placeInfo.name,
                 'createIcon': function(size) {
                                   return placeInfo.iconFactory(size);
                               }
               };
    },

    /** Implements the parent function to activate a place
     * (calls {@link PlaceInfo#launch}) - opens the appropriate program
     * for the place (file browser, "Connect to..." dialog, ...)
     * @inheritparams SearchDisplay.SearchResult#shellWorkspacelaunch
     * @override
     */
    activateResult: function(id, params) {
        let placeInfo = Main.placesManager.lookupPlaceById(id);
        placeInfo.launch(params);
    },

    /** Helper function used to sort {@link Search.ResultMeta}s for places (fed
     * into `sort`).
     * This does a [`String.localeCompare`](https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String/localeCompare)
     * on the names of the two places represented by the input IDs.
     * @param {string} idA - ID of the first result to compare
     * @param {string} idB - ID of the second result to compare
     * @returns {number} negative if `idA` is before `idB`, 0 if they're the same,
     * positive if `idA` is after `idB`.
     */
    _compareResultMeta: function (idA, idB) {
        let infoA = Main.placesManager.lookupPlaceById(idA);
        let infoB = Main.placesManager.lookupPlaceById(idB);
        return infoA.name.localeCompare(infoB.name);
    },

    /** Searches through the provided PlaceInfos to find ones that match
     * the specified terms. It is a case-insensitive search.
     *
     * This makes use of {@link PlaceInfo#matchTerms} to perform the match.
     *
     * @param {PlaceDisplay.PlaceInfo[]} places - array of place infos to look through.
     * @param {string[]} terms - search terms to match against.
     * @returns {string[]} - place IDs of matching places. First the
     * `Search.MatchType.MULTIPLE_PREFIX` results, then the `PREFIX` ones, then
     * the `MULTIPLE_SUBSTRING` ones, and finally the `SUBSTRING` matches, each
     * sorted within category using {@link #_compareResultMeta}.
     * @see PlaceInfo#matchTerms
     */
    _searchPlaces: function(places, terms) {
        let multiplePrefixResults = [];
        let prefixResults = [];
        let multipleSubstringResults = [];
        let substringResults = [];

        terms = terms.map(String.toLowerCase);

        for (let i = 0; i < places.length; i++) {
            let place = places[i];
            let mtype = place.matchTerms(terms);
            if (mtype == Search.MatchType.MULTIPLE_PREFIX)
                multiplePrefixResults.push(place.id);
            else if (mtype == Search.MatchType.PREFIX)
                prefixResults.push(place.id);
            else if (mtype == Search.MatchType.MULTIPLE_SUBSTRING)
                multipleSubstringResults.push(place.id);
            else if (mtype == Search.MatchType.SUBSTRING)
                substringResults.push(place.id);
        }
        multiplePrefixResults.sort(this._compareResultMeta);
        prefixResults.sort(this._compareResultMeta);
        multipleSubstringResults.sort(this._compareResultMeta);
        substringResults.sort(this._compareResultMeta);
        return multiplePrefixResults.concat(prefixResults.concat(multipleSubstringResults.concat(substringResults)));
    },

    /** Implements the parent function to do an initial search.
     * This gets all the places from the places manager ({@link PlacesManager#getAllPlaces})
     * and then calls {@link #_searchPlaces} to return the results.
     * @override
     */
    getInitialResultSet: function(terms) {
        let places = Main.placesManager.getAllPlaces();
        return this._searchPlaces(places, terms);
    },

    /** Implements the parent function to do a subsearch.
     * This grabs all the PlaceInfos for `previousResults` and performs
     * {@link #_searchPlaces} on the results.
     * @override
     */
    getSubsearchResultSet: function(previousResults, terms) {
        let places = previousResults.map(function (id) { return Main.placesManager.lookupPlaceById(id); });
        return this._searchPlaces(places, terms);
    }
};
