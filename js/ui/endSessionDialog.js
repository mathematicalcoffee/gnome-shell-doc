// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the dialog that appears when you log out/shut down/etc.
 *
 * It is
 * exported over DBus to '/org/gnome/SessionManager/EndSessionDialog', hence
 * replacing the default implementation with this GNOME implementation.
 *
 * The user should not use the {@link EndSessionDialog} class directly; instead
 * to open the dialog use its ["Open" method]{@link EndSessionDialogIface#Open} over DBus.
 *
 * ![End Session Dialog for logging out](pics/endSessionDialogLogout.png)
 * ![EndSessionDialog showing an inhibitor's {@link ListItem} in green](pics/endSessionDialogLogoutInhibitor.png)
 * ![End Session Dialog for powering off](pics/endSessionDialogPowerOff.png)
 *
 * Makes heavy use of the classes defined in {@link namespace:GnomeSession}.
 */
/*
 * Copyright 2010 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

const DBus = imports.dbus;
const Lang = imports.lang;
const Signals = imports.signals;

const AccountsService = imports.gi.AccountsService;
const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Pango = imports.gi.Pango;
const St = imports.gi.St;
const Shell = imports.gi.Shell;

const GnomeSession = imports.misc.gnomeSession
const Lightbox = imports.ui.lightbox;
const Main = imports.ui.main;
const ModalDialog = imports.ui.modalDialog;
const Tweener = imports.ui.tweener;

let _endSessionDialog = null;

/** @+
 * @const
 * @default
 * @type {number} */
/** The size of the application icon in a {@link ListItem}. */
const _ITEM_ICON_SIZE = 48;
/** The size of the user's avatar/icon in the {@link EndSessionDialog}. */
const _DIALOG_ICON_SIZE = 32;
/** @- */

const GSM_SESSION_MANAGER_LOGOUT_FORCE = 2;

/** Definition for the EndSessionDialog interface,
 * 'org.gnome.SessionManager.EndSessionDialog'.
 *
 * **Not all the signals are defined here??** It defines the 'Canceled' signal
 * but there are also 'Closed', 'ConfirmedLogout', 'ConfirmedReboot' and
 * 'ConfirmedShutdown' signals that are not defined here...
 *
 * Implemented/provided by the {@link EndSessionDialog}.
 * @property {function(uuuao)} Open - opens the dialog (asynchronous method).
 * Implemented as {@link EndSessionDialog#OpenAsync}.
 * Parameters:
 *
 * * u - number - type of end session dialog to show. 0 is logout,
 * 1 is shutdown, 2 is restart (see {@link DialogContent}).
 * * u - number - timestamp
 * * u - number - number of seconds the dialog
 * should remain open before performing its relevant action ("You will be
 * automatically logged out in `totalSecondsToStayOpen` seconds").
 * * ao - array of strings (object paths) of inhibitors prevent the end session
 * attempt from succeeding (e.g. applications with unsaved documents - gedit
 * will inhibit end session attempts if an unsaved document is open).
 * @see EndSessionDialog
 */
const EndSessionDialogIface = {
    name: 'org.gnome.SessionManager.EndSessionDialog',
    methods: [{ name: 'Open',
                inSignature: 'uuuao',
                outSignature: ''
              }
             ],
    signals: [{ name: 'Canceled',
                inSignature: '',
              }],
    properties: []
};

/** This defines what to show in the {@link EndSessionDialog} - the message,
 * heading, buttons and so on.
 * @see logoutDialogContent
 * @see shutdownDialogContent
 * @see restartDialogContent
 * @typedef EndSessionDialogContent
 * @type {Object}
 * @property {string} subject - the subject (title) for the dialog (e.g.
 * "Log out").
 * @property {string} [subjectWithUser] - like `subject`. If present,
 * `subjectWithUser.format(username)` will be used as the title for the dialog,
 * so put a '%s' in there. E.g. "Log out %s" for "Log out mathematical.coffee".
 * @property {string} inhibitedDescription - if the action is inhibited somehow,
 * this is the message to display (e.g. "Click Log Out to quit these applications
 * and log out of the system").
 * @property {function(seconds): string} uninhibitedDescription - function taking in
 * the number of seconds until the action is performed, and returning a string
 * with the main message for the dialog (for example "You will be logged out
 * automatically in `seconds` seconds") (if no inhibitors to the action exist).
 * @property {function(user, seconds)} [uninhibitedDescriptionWithUser] - like
 * `uninhibitedDesecription`. If present, this is a function taking in a string
 * (`user`) being the username and a number (`seconds`) being the seconds remaining
 * before the action is taken. It should return a string with the description
 * for the dialog (e.g. "`user` will be logged out automatically
 * in `seconds` seconds").
 * @property {string} endDescription - message to show on the dialog as the action
 * is being taken, i.e. once the timeout has finished. e.g. "Logging out".
 * @property {Array.<{signal: string, label: string}>} confirmButtons - buttons
 * to show on the dialog. The `label` property is the label of the button,
 * and the `signal` property is the name of the signal that will be emitted over
 * DBus upon the action being taken. The *last* button in this list will be used
 * as the default action if the end session dialog finishes counting down.
 * @property {string} iconStyleClass - style class name for the icon displayed
 * on the dialog (the user's avatar), e.g. 'end-session-dialog-logout-icon'.
 */

// TODO: want to just see the properties listed with their values
/** Definition of the end session dialog content for the logout dialog.
 * Subject "Log out" ("Log out `username`"), inhibited description
 * "Click Log Out to quit these applications and log out of the system",
 * uninhibited description "{You|`username`} will be logged out automatically
 * in `seconds` seconds", end description "Logging out of the system.", and
 * button "Log Out" sending signal "ConfirmedLogout".
 * @const
 * @type {EndSessionDialog.EndSessionDialogContent}
 */
const logoutDialogContent = {
    subjectWithUser: _("Log Out %s"),
    subject: _("Log Out"),
    inhibitedDescription: _("Click Log Out to quit these applications and log out of the system."),
    uninhibitedDescriptionWithUser: function(user, seconds) {
        return ngettext("%s will be logged out automatically in %d second.",
                        "%s will be logged out automatically in %d seconds.",
                        seconds).format(user, seconds);
    },
    uninhibitedDescription: function(seconds) {
        return ngettext("You will be logged out automatically in %d second.",
                        "You will be logged out automatically in %d seconds.",
                        seconds).format(seconds);
    },
    endDescription: _("Logging out of the system."),
    confirmButtons: [{ signal: 'ConfirmedLogout',
                       label:  _("Log Out") }],
    iconStyleClass: 'end-session-dialog-logout-icon'
};

/** Definition of the end session dialog content for the shutdown dialog.
 * Subject "Power Off", inhibited description
 * "Click Power Off to quit these applications and power off the system",
 * uninhibited description "The system will power off automatically in
 * `seconds` seconds", end description "Powering off the system", and
 * buttons "Power Off" with signal 'ConfirmedShutdown' and "Restart" with
 * signal 'ConfirmedReboot'.
 * @const
 * @type {EndSessionDialog.EndSessionDialogContent}
 */
const shutdownDialogContent = {
    subject: _("Power Off"),
    inhibitedDescription: _("Click Power Off to quit these applications and power off the system."),
    /**
     * @param {?} seconds
     */
    uninhibitedDescription: function(seconds) {
        return ngettext("The system will power off automatically in %d second.",
                        "The system will power off automatically in %d seconds.",
                        seconds).format(seconds);
    },
    endDescription: _("Powering off the system."),
    confirmButtons: [{ signal: 'ConfirmedReboot',
                       label:  _("Restart") },
                     { signal: 'ConfirmedShutdown',
                       label:  _("Power Off") }],
    iconName: 'system-shutdown',
    iconStyleClass: 'end-session-dialog-shutdown-icon'
};

/** Definition of the end session dialog content for the restart dialog.
 * Subject "Restart", inhibited description
 * "Click Restart to quit these applications and restart the system",
 * uninhibited description "The system will restart automatically in
 * `seconds` seconds", end description "Restarting the system", and
 * button "Restart" with signal 'ConfirmedReboot'.
 * @const
 * @type {EndSessionDialog.EndSessionDialogContent}
 */
const restartDialogContent = {
    subject: _("Restart"),
    inhibitedDescription: _("Click Restart to quit these applications and restart the system."),
    /**
     * @param {?} seconds
     */
    uninhibitedDescription: function(seconds) {
        return ngettext("The system will restart automatically in %d second.",
                        "The system will restart automatically in %d seconds.",
                        seconds).format(seconds);
    },
    endDescription: _("Restarting the system."),
    confirmButtons: [{ signal: 'ConfirmedReboot',
                       label:  _("Restart") }],
    iconName: 'system-shutdown',
    iconStyleClass: 'end-session-dialog-shutdown-icon'
};

/** What content to show in the end session dialog.
 * The keys (0, 1, and 2) correspond to
 * `GSM_SHELL_END_SESSION_DIALOG_TYPE_(LOGOUT|SHUTDOWN|RESTART)`.
 * @type {EndSessionDialog.EndSessionDialogContent}
 * @enum
 * @const */
const DialogContent = {
    /** log out dialog content. Key (0) is GSM_SHELL_END_SESSION_DIALOG_TYPE_LOGOUT. */
    0 /* GSM_SHELL_END_SESSION_DIALOG_TYPE_LOGOUT */: logoutDialogContent,
    /** shut down dialog content. Key (1) is GSM_SHELL_END_SESSION_DIALOG_TYPE_SHUTDOWN. */
    1 /* GSM_SHELL_END_SESSION_DIALOG_TYPE_SHUTDOWN */: shutdownDialogContent,
    /** restart dialog content. Key (2) is GSM_SHELL_END_SESSION_DIALOG_TYPE_RESTART. */
    2 /* GSM_SHELL_END_SESSION_DIALOG_TYPE_RESTART */: restartDialogContent
};

/** Finds the `Shell.App` corresponding to the input `inhibitor`.
 * @param {GnomeSession.Inhibitor} inhibitor - the inhibitor to find the app for.
 * @returns {?Shell.App} the app corresponding to the inhibitor (looked up
 * using the inhibitor's `app_id` property), or `null` if not found.
 */
function findAppFromInhibitor(inhibitor) {
    let desktopFile = inhibitor.app_id;

    if (!GLib.str_has_suffix(desktopFile, '.desktop'))
      desktopFile += '.desktop';

    let candidateDesktopFiles = [];

    candidateDesktopFiles.push(desktopFile);
    candidateDesktopFiles.push('gnome-' + desktopFile);

    let appSystem = Shell.AppSystem.get_default();
    let app = null;
    for (let i = 0; i < candidateDesktopFiles.length; i++) {
        try {
            app = appSystem.lookup_app(candidateDesktopFiles[i]);

            if (app)
                break;
        } catch(e) {
            // ignore errors
        }
    }

    return app;
}

/** creates a new ListItem. Very similar to the {@link ShellMountOperation.ListItem}.
 * 
 * The top-level actor is a `St.Button` with style class 'end-session-dialog-app-list-item'.
 * The button contains a horizontal `St.BoxLayout` showing the icon for the
 * inhibiting application (size {@link _ITEM_ICON_SIZE}) as well
 * as labels displaying the application name and `reason`.
 *
 * Various style classes are 'end-session-dialog-app-list-item-*'.
 *
 * @param {Shell.App} app - the application for the inhibitor.
 * @param {string} reason - reason for the inhibit (as in {@link GnomeSession.Inhibitor#reason}).
 * (e.g. "Not responding" - see the picture in the class documentation).
 * @classdesc
 * ![EndSessionDialog showing an inhibitor's {@link ListItem} in green](pics/endSessionDialogLogoutInhibitor.png)
 *
 * A ListItem is created for each inhibitor to ending the session, displayed in
 * the {@link EndSessionDialog}. That is, you would get a ListItem per application
 * that inhibits the logout/restart/shutdown.
 *
 * It displays the icon for the application along with the application name
 * and the reason it is inhibiting ending the session (e.g. "Not responding"
 * in the picture above).
 *
 * When the user clicks on a {@link ListItem} it emits an
 * ['activate' signal]{@link .event:activate} which is caught by the {@link EndSessionDialog}
 * in order to close the dialog, and also activates the application.
 *
 * Very similar to the {@link ShellMountOperation.ListItem}.
 * @class
 */
function ListItem(app, reason) {
    this._init(app, reason);
}

ListItem.prototype = {
    _init: function(app, reason) {
        this._app = app;

        this._reason = reason;

        if (this._reason == null)
          this._reason = '';

        let layout = new St.BoxLayout({ vertical: false});

        /** Top-level actor for a ListItem.
         * This has style class 'end-session-dialog-app-list-item' and has
         * the inhibitor's application's icon, the inhibitor's application's
         * name, and the reason that the inhibitor is an inhibitor displayed
         * inside it.
         * @type {St.Button} */
        this.actor = new St.Button({ style_class: 'end-session-dialog-app-list-item',
                                     can_focus:   true,
                                     child:       layout,
                                     reactive:    true,
                                     x_align:     St.Align.START,
                                     x_fill:      true });

        this._icon = this._app.create_icon_texture(_ITEM_ICON_SIZE);

        let iconBin = new St.Bin({ style_class: 'end-session-dialog-app-list-item-icon',
                                   child:       this._icon });
        layout.add(iconBin);

        let textLayout = new St.BoxLayout({ style_class: 'end-session-dialog-app-list-item-text-box',
                                            vertical:    true });
        layout.add(textLayout);

        this._nameLabel = new St.Label({ text:        this._app.get_name(),
                                         style_class: 'end-session-dialog-app-list-item-name' });
        textLayout.add(this._nameLabel,
                       { expand: false,
                         x_fill: true });

        this._descriptionLabel = new St.Label({ text:        this._reason,
                                                style_class: 'end-session-dialog-app-list-item-description' });
        textLayout.add(this._descriptionLabel,
                       { expand: true,
                         x_fill: true });

        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
    },

    /** Callback when the user clicks this ListItem.
     * It emits the 'activate' signal (caught by the {@link EndSessionDialog}
     * so that it can close itself) and activates the application that is the
     * inhibitor.
     * @fires .activate */
    _onClicked: function() {
        this.emit('activate');
        this._app.activate();
    }
};
Signals.addSignalMethods(ListItem.prototype);
/** Emitted when the user clicks on a {@link ListItem}.
 * @event
 * @name activate
 * @memberof ListItem */

/** Rounds a number of seconds to the nearest `interval` seconds.
 *
 * The logout timer only shows updates every 10 seconds
 * until the last 10 seconds, then it shows updates every
 * second.  This function takes a given time and returns
 * what we should show to the user for that time.
 * @param {number} totalSeconds - total seconds we are counting down from
 * (if we round to an interval and end up with a time greater than this,
 * we cap it at this).
 * @param {number} secondsLeft - the number of seconds left. If less than
 * `interval`, `secondsLeft` will be returned.
 * @param {number} interval - interval to round the seconds left to (for example
 * 10 to round to the nearest 10 seconds).
 * @returns {number} the number of seconds left, rounded to the nearest `interval`
 * seconds, *unless*:
 *
 * * `secondsLeft < interval` - we return `Math.ceil(secondsLeft)`, or
 * * the output time is greater than `totalSeconds` - we return
 * `Math.ceil(totalSeconds)`.
 */
function _roundSecondsToInterval(totalSeconds, secondsLeft, interval) {
    let time;

    time = Math.ceil(secondsLeft);

    // Final count down is in decrements of 1
    if (time <= interval)
        return time;

    // Round up higher than last displayable time interval
    time += interval - 1;

    // Then round down to that time interval
    if (time > totalSeconds)
        time = Math.ceil(totalSeconds);
    else
        time -= time % interval;

    return time;
}

function _setLabelText(label, text) {
    if (text) {
        label.set_text(text);
        label.show();
    } else {
        label.set_text('');
        label.hide();
    }
}

/** creates a new EndSessionDialog.
 *
 * This calls the [parent constructor]{@link ModalDialog.ModalDialog} with
 * style class 'end-session-dialog'.
 *
 * It also stores information about the current user using the
 * [AccountsService API](https://launchpad.net/ubuntu/+source/accountsservice)
 * (I actually cannot find documentation on the AccountsService API *anywhere*).
 * This is used to get the current user's username/avatar to display on the
 * dialog.
 *
 * UI-wise, box layouts are used in order to display the user's avatar,
 * a subject/heading label ("Log out"), a message label ("Click Log Out to..."),
 * and create a ScrollView to display {@link ListItem}s for any inhibitors.
 *
 * Various style classes are 'end-session-dialog-*'.
 *
 * It also exports this object to '/org/gnome/SessionManager/EndSessionDialog',
 * which as far as I can tell, ensures that this dialog is used as the
 * end session dialog.
 *
 * @classdesc
 * ![EndSessionDialog showing an inhibitor's {@link ListItem} in green](pics/endSessionDialogLogoutInhibitor.png)
 * ![End Session Dialog for powering off](pics/endSessionDialogPowerOff.png)
 *
 * The EndSessionDialog implements the {@link EndSessionDialogIface} interface.
 * It is exported over DBus at path '/org/gnome/SessionManager/EndSessionDialog'
 * to provide the end session dialog for the session. It doesn't actually
 * do the shut down/restart/logout itself; instead, it emits signals 'ConfirmedLogout',
 * 'ConfirmedReboot' and 'ConfirmedShutdown' over DBus which (I think) are
 * caught by the gnome session manager, which will then perform the appropriate
 * actions.
 *
 * It is basically the dialog that gets shown when the user tries to end the
 * session by either logging out, shutting down, or restarting.
 *
 * It will display the relevant message along with a countdown ("You will be
 * automatically logged out in `seconds` seconds") and buttons to end the
 * session immediately rather than wait for the countdown.
 * 
 * If the timeout elapses before the user takes any action, the session will
 * be ended.
 *
 * If there are applications inhibiting the ending of the session (e.g. gedit
 * will inhibit if the current document has unsaved changes), thse will be
 * displayed in the dialog and nothing will happen until the user resolves
 * them.
 *
 * The class interacts quite heavily over DBus with various SessionManager
 * objects wrapped in GNOME-shell objects (example {@link GnomeSession.Inhibitor}).
 *
 * You are not meant to use the gnome-shell class directly. You are meant
 * to use the 'Open' method over DBus to show the dialog.
 *
 * @class
 * @extends ModalDialog.ModalDialog
 */
function EndSessionDialog() {
    if (_endSessionDialog == null) {
        this._init();
        DBus.session.exportObject('/org/gnome/SessionManager/EndSessionDialog',
                                  this);
        _endSessionDialog = this;
    }

    return _endSessionDialog;
}

/** Creates the {@link EndSessionDialog} and exports it over DBus to path
 * '/org/gnome/SessionManager/EndSesssionDialog'. You only need one.
 * Called from {@link Main.start}.
 */
function init() {
    // This always returns the same singleton object
    // By instantiating it initially, we register the
    // bus object, etc.
    let dialog = new EndSessionDialog();
}

EndSessionDialog.prototype = {
    __proto__: ModalDialog.ModalDialog.prototype,

    _init: function() {
        ModalDialog.ModalDialog.prototype._init.call(this, { styleClass: 'end-session-dialog' });

        /** The current user. We use it to get the user's username/avatar.
         * @type {AccountsService.User} */
        this._user = AccountsService.UserManager.get_default().get_user(GLib.get_user_name());

        this._secondsLeft = 0;
        this._totalSecondsToStayOpen = 0;
        this._inhibitors = [];

        this.connect('destroy',
                     Lang.bind(this, this._onDestroy));
        this.connect('opened',
                     Lang.bind(this, this._onOpened));

        this._userLoadedId = this._user.connect('notify::is_loaded',
                                                Lang.bind(this, this._updateContent));

        this._userChangedId = this._user.connect('changed',
                                                 Lang.bind(this, this._updateContent));

        let mainContentLayout = new St.BoxLayout({ vertical: false });
        this.contentLayout.add(mainContentLayout,
                               { x_fill: true,
                                 y_fill: false });

        this._iconBin = new St.Bin();
        mainContentLayout.add(this._iconBin,
                              { x_fill:  true,
                                y_fill:  false,
                                x_align: St.Align.END,
                                y_align: St.Align.START });

        let messageLayout = new St.BoxLayout({ vertical: true });
        mainContentLayout.add(messageLayout,
                              { y_align: St.Align.START });

        this._subjectLabel = new St.Label({ style_class: 'end-session-dialog-subject' });

        messageLayout.add(this._subjectLabel,
                          { y_fill:  false,
                            y_align: St.Align.START });

        this._descriptionLabel = new St.Label({ style_class: 'end-session-dialog-description' });
        this._descriptionLabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
        this._descriptionLabel.clutter_text.line_wrap = true;

        messageLayout.add(this._descriptionLabel,
                          { y_fill:  true,
                            y_align: St.Align.START });

        let scrollView = new St.ScrollView({ style_class: 'end-session-dialog-app-list'});
        scrollView.set_policy(Gtk.PolicyType.NEVER,
                              Gtk.PolicyType.AUTOMATIC);
        this.contentLayout.add(scrollView,
                               { x_fill: true,
                                 y_fill: true });
        scrollView.hide();

        this._applicationList = new St.BoxLayout({ vertical: true });
        scrollView.add_actor(this._applicationList,
                             { x_fill:  true,
                               y_fill:  true,
                               x_align: St.Align.START,
                               y_align: St.Align.MIDDLE });

        this._applicationList.connect('actor-added',
                                      Lang.bind(this, function() {
                                          if (this._applicationList.get_children().length == 1)
                                              scrollView.show();
                                      }));

        this._applicationList.connect('actor-removed',
                                      Lang.bind(this, function() {
                                          if (this._applicationList.get_children().length == 0)
                                              scrollView.hide();
                                      }));
    },

    /** Callback when the dialog is destroyed. Just does some tidying up
     * (disconnects signals from {@link #_user} listening to
     * changes of username/avatar/details).
     */
    _onDestroy: function() {
        this._user.disconnect(this._userLoadedId);
        this._user.disconnect(this._userChangedId);
    },

    /** Convenience function to set the user's avatar from a file.
     * @param {string} [iconFile] - URI to the file that is the user's
     * avatar. If not provided, the user's avatar will not be shown.
     * @param {string} [styleClass] - if present, used as the style class for
     * the St.Bin containing the user's avatar.
     * @see #_setIconFromName
     */
    _setIconFromFile: function(iconFile, styleClass) {
        if (styleClass)
            this._iconBin.set_style_class_name(styleClass);
        this._iconBin.set_style(null);

        this._iconBin.child = null;
        if (iconFile) {
            this._iconBin.show();
            this._iconBin.set_style('background-image: url("' + iconFile + '");');
        } else {
            this._iconBin.hide();
        }
    },

    /** Convenience function to set the user's avatar from an icon name.
     * @param {string} [iconName] - icon name to use for the avatar (e.g.
     * 'avatar-default'). Will be loaded as a symbolic icon. If not present,
     * the icon is hidden.
     * @param {string} [styleClass] - if present, used as the style class for
     * the St.Bin containing the user's avatar.
     * @see #_setIconFromFile
     */
    _setIconFromName: function(iconName, styleClass) {
        if (styleClass)
            this._iconBin.set_style_class_name(styleClass);
        this._iconBin.set_style(null);

        if (iconName != null) {
            let textureCache = St.TextureCache.get_default();
            let icon = textureCache.load_icon_name(this._iconBin.get_theme_node(),
                                                   iconName,
                                                   St.IconType.SYMBOLIC,
                                                   _DIALOG_ICON_SIZE);

            this._iconBin.child = icon;
            this._iconBin.show();
        } else {
            this._iconBin.child = null;
            this._iconBin.hide();
        }
    },

    /** Updates the content of the dialog, using {@link #_type}
     * to work out what content to show from {@link DialogContent}.
     *
     * The subject of the dialog is set to `content.subjectWithUser` if it exists
     * and the user information is available, otherwise `content.subject`.
     *
     * The user's avatar is set (using 'avatar-default' if for some reason
     * it couldn't be set).
     *
     * If there are any inhibitors to ending the session, the dialog's content
     * is set to `content.inhibitedDescription` or `content.inhibitedDescriptionWithUser`
     * if user information is available, and the timer is stopped.
     *
     * If the timer has run out, `content.endSession` is displayed.
     *
     * @see logoutDialogContent
     * @see shutdownDialogContent
     * @see restartDialogContent
     * @see EndSessionDialogContent
     */
    _updateContent: function() {
        if (this.state != ModalDialog.State.OPENING &&
            this.state != ModalDialog.State.OPENED)
            return;

        let dialogContent = DialogContent[this._type];

        let subject = dialogContent.subject;
        let description;

        if (this._user.is_loaded && !dialogContent.iconName) {
            let iconFile = this._user.get_icon_file();
            if (GLib.file_test(iconFile, GLib.FileTest.EXISTS))
                this._setIconFromFile(iconFile, dialogContent.iconStyleClass);
            else
                this._setIconFromName('avatar-default', dialogContent.iconStyleClass);
        } else if (dialogContent.iconName) {
            this._setIconFromName(dialogContent.iconName,
                                  dialogContent.iconStyleClass);
        }

        if (this._inhibitors.length > 0) {
            this._stopTimer();
            description = dialogContent.inhibitedDescription;
        } else if (this._secondsLeft > 0 && this._inhibitors.length == 0) {
            let displayTime = _roundSecondsToInterval(this._totalSecondsToStayOpen,
                                                      this._secondsLeft,
                                                      10);

            if (this._user.is_loaded) {
                let realName = this._user.get_real_name();

                if (realName != null) {
                    if (dialogContent.subjectWithUser)
                        subject = dialogContent.subjectWithUser.format(realName);

                    if (dialogContent.uninhibitedDescriptionWithUser)
                        description = dialogContent.uninhibitedDescriptionWithUser(realName, displayTime);
                    else
                        description = dialogContent.uninhibitedDescription(displayTime);
                }
            }

            if (!description)
                description = dialogContent.uninhibitedDescription(displayTime);
        } else {
            description = dialogContent.endDescription;
        }

        _setLabelText(this._subjectLabel, subject);
        _setLabelText(this._descriptionLabel, description);
    },

    /** Updates the buttons on the dialog, using {@link #_type}
     * to work out what content to show from {@link DialogContent}.
     *
     * A button is created for each member of `content.confirmButtons`.
     * Upon clicking a button, {@link #_confirm} is called
     * with the signal specified. Also an extra 'Cancel' button is also made.
     *
     * @see #setButtons
     * @see logoutDialogContent
     * @see shutdownDialogContent
     * @see restartDialogContent
     * @see EndSessionDialogContent
     */
    _updateButtons: function() {
        let dialogContent = DialogContent[this._type];
        let buttons = [{ action: Lang.bind(this, this.cancel),
                         label:  _("Cancel"),
                         key:    Clutter.Escape }];

        for (let i = 0; i < dialogContent.confirmButtons.length; i++) {
            let signal = dialogContent.confirmButtons[i].signal;
            let label = dialogContent.confirmButtons[i].label;
            buttons.push({ action: Lang.bind(this, function() {
                                       this._confirm(signal);
                                   }),
                           label: label });
        }

        this.setButtons(buttons);
    },

    /** Overrides the parent function to close the dialog. It calls the
     * parent function but additionally emits signal 'Closed' from the
     * DBus (object, interface) = ('/org/gnome/SessionManager/EndSessionDialog',
     * 'org.gnome.SessionManager.EndSessionDialog').
     * @override */
    close: function() {
        ModalDialog.ModalDialog.prototype.close.call(this);
        DBus.session.emit_signal('/org/gnome/SessionManager/EndSessionDialog',
                                 'org.gnome.SessionManager.EndSessionDialog',
                                 'Closed', '', []);
    },

    /** Callback when the user clicks the 'Cancel' button.
     *
     * Stops the timer, emits signal 'Canceled' from the
     * DBus (object, interface) = ('/org/gnome/SessionManager/EndSessionDialog',
     * 'org.gnome.SessionManager.EndSessionDialog') and calls
     * {@link #close}.
     */
    cancel: function() {
        this._stopTimer();
        DBus.session.emit_signal('/org/gnome/SessionManager/EndSessionDialog',
                                 'org.gnome.SessionManager.EndSessionDialog',
                                 'Canceled', '', []);
        this.close(global.get_current_time());
    },

    /** Callback when the user clicks any of the buttons of the dialog
     * (except for "Cancel").
     *
     * Stops the timer, emits signal `signal` from the
     * DBus (object, interface) = ('/org/gnome/SessionManager/EndSessionDialog',
     * 'org.gnome.SessionManager.EndSessionDialog').
     *
     * The signal will be either 'ConfirmedLogout', 'ConfirmedReboot' or
     * 'ConfirmedShutdown'. As far as I can tell, the gnome session manager
     * listens to these signals from this interface/object and will then perform
     * the appropriate actions upon receiving them (found this in
     * [`gsm-manager.c`](http://git.gnome.org/browse/gnome-session/tree/gnome-session/gsm-manager.c#n3202)).
     */
    _confirm: function(signal) {
        this._fadeOutDialog();
        this._stopTimer();
        DBus.session.emit_signal('/org/gnome/SessionManager/EndSessionDialog',
                                 'org.gnome.SessionManager.EndSessionDialog',
                                 signal, '', []);
    },

    /** Callback when the dialog is opened. Starts the countdown timer.
     * @see #_startTimer */
    _onOpened: function() {
        if (this._inhibitors.length == 0)
            this._startTimer();
    },

    /** Starts the countdown timer.
     * If the timer reaches 0, the action defined by the *last* button defined in
     * [`content.confirmButton`]{@link EndSessionDialogContent} will be executed.
     *
     * (`content` is the {@link EndSessionDialogContent} specified by
     * {@link #_type}).
     *
     * As the timer counts down the dialog content will be updated to show
     * the correct time remaining.
     */
    _startTimer: function() {
        this._secondsLeft = this._totalSecondsToStayOpen;
        Tweener.addTween(this,
                         { _secondsLeft: 0,
                           time: this._secondsLeft,
                           transition: 'linear',
                           onUpdate: Lang.bind(this, this._updateContent),
                           onComplete: Lang.bind(this, function() {
                                           let dialogContent = DialogContent[this._type];
                                           let button = dialogContent.confirmButtons[dialogContent.confirmButtons.length - 1];
                                           this._confirm(button.signal);
                                       }),
                         });
    },

    /** Stops the timer. */
    _stopTimer: function() {
        Tweener.removeTweens(this);
        this._secondsLeft = 0;
    },

    /** Callback when an inhibitor is found and loaded - called upon
     * the {@link GnomeSession.Inhibitor.is-loaded} signal.
     *
     * This creates a {@link ListItem} for the inhibitor and adds it to the
     * dialog (unless it is a service not an application, in which it is not shown).
     * @param {GnomeSession.Inhibitor} inhibitor - inhibitor that has finished
     * loading.
     */
    _onInhibitorLoaded: function(inhibitor) {
        if (this._inhibitors.indexOf(inhibitor) < 0) {
            // Stale inhibitor
            return;
        }

        let app = findAppFromInhibitor(inhibitor);

        if (app) {
            let item = new ListItem(app, inhibitor.reason);
            item.connect('activate',
                         Lang.bind(this, function() {
                             this.close(global.get_current_time());
                         }));
            this._applicationList.add(item.actor, { x_fill: true });
            this._stopTimer();
        } else {
            // inhibiting app is a service, not an application
            this._inhibitors.splice(this._inhibitors.indexOf(inhibitor), 1);
        }

        this._updateContent();
    },

    /** This implements the 'Open' method of interface 'org.gnome.SessionManager.EndSessionDialog'.
     * @see EndSessionDialogIface
     * @param {number} type - type of end session dialog to show. 0 is logout,
     * 1 is shutdown, 2 is restart (see {@link DialogContent}).
     * @param {number} timestamp - timestamp to open the dialog with..
     * @param {number} totalSecondsToStayOpen - number of seconds the dialog
     * should remain open before performing its relevant action ("You will be
     * automatically logged out in `totalSecondsToStayOpen` seconds").
     * @param {string[]} inhibitorObjectPaths - array of DBus object paths to
     * inhibitors to the end session attempt (e.g. applications preventing
     * you from ending the session).
     * @param {function()} callback - callback to execute once the dialog has opened.
     */
    OpenAsync: function(type, timestamp, totalSecondsToStayOpen, inhibitorObjectPaths, callback) {
        this._totalSecondsToStayOpen = totalSecondsToStayOpen;
        this._inhibitors = [];
        this._applicationList.destroy_children();
        this._type = type;

        if (!(this._type in DialogContent))
            throw new DBus.DBusError('org.gnome.Shell.ModalDialog.TypeError',
                                     "Unknown dialog type requested");

        for (let i = 0; i < inhibitorObjectPaths.length; i++) {
            let inhibitor = new GnomeSession.Inhibitor(inhibitorObjectPaths[i]);

            inhibitor.connect('is-loaded',
                              Lang.bind(this, function() {
                                  this._onInhibitorLoaded(inhibitor);
                              }));
            this._inhibitors.push(inhibitor);
        }

        this._updateButtons();

        if (!this.open(timestamp))
            throw new DBus.DBusError('org.gnome.Shell.ModalDialog.GrabError',
                                     "Cannot grab pointer and keyboard");

        this._updateContent();

        let signalId = this.connect('opened',
                                    Lang.bind(this, function() {
                                        callback();
                                        this.disconnect(signalId);
                                    }));
    }
};
DBus.conformExport(EndSessionDialog.prototype, EndSessionDialogIface);
