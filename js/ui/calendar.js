// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file defines the calendar and events list in the date/clock menu - it listens to a
 * DBus service to retrieve your events list and defines the UI elements that
 * display both your events list and calendar in the date menu.
 *
 * ![The {@link Calendar} is in red; {@link EventsList} in blue](pics/calendar.png)
 *
 * Also has a lot of functions for converting times to fuzzy times
 * (like "Today", "Tomorrow", "This week").
 *
 * The date menu itself is defined in [`dateMenu.js`]{@link namespace:DateMenu}.
 */

const DBus = imports.dbus;
const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const St = imports.gi.St;
const Signals = imports.signals;
const Pango = imports.gi.Pango;
const Gettext_gtk30 = imports.gettext.domain('gtk30');
const Mainloop = imports.mainloop;
const Shell = imports.gi.Shell;

/** @+
 * @const
 * @default */
/** Number of ms in a day.
 * @type {number} */
const MSECS_IN_DAY = 24 * 60 * 60 * 1000;
/** UNUSED (well, it is used in {@link Calendar#_setWeekdateHeaderWidth} but
 * that function does nothing due to a failed `if` check, I think this is a
 * stub from an old version of gnome-shell). */
const WEEKDATE_HEADER_WIDTH_DIGITS = 3;
/** The gsettings key in 'org.gnome.shell.calendar' - whether to show the
 * week number in the {@link Calendar}.
 * @type {string} */
const SHOW_WEEKDATE_KEY = 'show-weekdate';

// in org.gnome.desktop.interface
/** Gsettings key in org.gnome.desktop.interface, what format the clock should
 * have (12h or 24h)
 * @type {string} */
const CLOCK_FORMAT_KEY        = 'clock-format';
/** @- */

/** Utility function - whether `dateA` and `dateB` have the same date, month
 * and year (times are allowed to differ).
 * @param {Date} dateA - first date to compare
 * @param {Date} dateB - second date to compare
 * @returns {boolean} boolean as described. */
function _sameDay(dateA, dateB) {
    return (dateA.getDate() == dateB.getDate() &&
            dateA.getMonth() == dateB.getMonth() &&
            dateA.getYear() == dateB.getYear());
}

/** Utility function - whether `dateA` and `dateB` are in the same year.
 * @inheritparams _sameDay
 */
function _sameYear(dateA, dateB) {
    return (dateA.getYear() == dateB.getYear());
}

/** Returns whether the input date is Mon-Fri inclusive.
 *
 * Note from the code:
 *
 * TODO: maybe needs config - right now we assume that Saturday and
 * Sunday are non-work days (not true in e.g. Israel, it's Sunday and
 * Monday there)
 *
 * @param {Date} date - a date.
 * @returns {boolean} whether the date is Monday, Tuesday, Wednesday, Thursday,
 * or Friday.
 */
function _isWorkDay(date) {
    return date.getDay() != 0 && date.getDay() != 6;
}

/** Returns a date on the same date as the input one but with the time set to midnight
 * (00:00:00 and 0 milliseconds).
 * @inheritparams _isWorkDay
 * @returns {Date} a new date with the time as requested.
 */
function _getBeginningOfDay(date) {
    let ret = new Date(date.getTime());
    ret.setHours(0);
    ret.setMinutes(0);
    ret.setSeconds(0);
    ret.setMilliseconds(0);
    return ret;
}

/** Returns a date on the same date as the input one but with the time set to
 * one millisecond before midnight (i.e. the end of the day, 23:59:59 and 999
 * milliseconds).
 * @inheritparams _getBeginningOfDay
 */
function _getEndOfDay(date) {
    let ret = new Date(date.getTime());
    ret.setHours(23);
    ret.setMinutes(59);
    ret.setSeconds(59);
    ret.setMilliseconds(999);
    return ret;
}

/** Formats a {@link Event}'s time to either 'HH:MM' in 24 hour notation,
 * or 'hours:minutes [AP]M' in 12 hour notation ("%H:%M" and "%l:%M %p"),
 * or "All Day" if it's all day.
 * @see Event#allDay
 * @see Event#date
 * @param {Calendar.Event} event - the event
 * @param {string} clockFormat - the clock format, '24h' or '12h'
 * @returns {string} "All Day" (translated) if {@link Event#allDay} is true,
 * otherwise '%H:%M' if `clockFormat` is '24h' (e.g. "18:00"), or
 * '%l:%M %p' if `clockFormat` is '12h' (e.g. " 6:00 PM").
 */
function _formatEventTime(event, clockFormat) {
    let ret;
    if (event.allDay) {
        /* Translators: Shown in calendar event list for all day events
         * Keep it short, best if you can use less then 10 characters
         */
        ret = C_("event list time", "All Day");
    } else {
        switch (clockFormat) {
        case '24h':
            /* Translators: Shown in calendar event list, if 24h format */
            ret = event.date.toLocaleFormat(C_("event list time", "%H:%M"));
            break;

        default:
            /* explicit fall-through */
        case '12h':
            /* Transators: Shown in calendar event list, if 12h format */
            ret = event.date.toLocaleFormat(C_("event list time", "%l:%M %p"));
            break;
        }
    }
    return ret;
}

/** Gets the calendar week number for a particular date.
 *
 * Based on the algorithms found here:
 * [wikipedia iso week date](http://en.wikipedia.org/wiki/Talk:ISO_week_date).
 *
 * @inheritparams _getEndOfDay
 * @returns {number} the week number corresponding to `date`.
 */
function _getCalendarWeekForDate(date) {
    // Based on the algorithms found here:
    // http://en.wikipedia.org/wiki/Talk:ISO_week_date
    let midnightDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    // Need to get Monday to be 1 ... Sunday to be 7
    let dayOfWeek = 1 + ((midnightDate.getDay() + 6) % 7);
    let nearestThursday = new Date(midnightDate.getFullYear(), midnightDate.getMonth(),
                                   midnightDate.getDate() + (4 - dayOfWeek));

    let jan1st = new Date(nearestThursday.getFullYear(), 0, 1);
    let diffDate = nearestThursday - jan1st;
    let dayNumber = Math.floor(Math.abs(diffDate) / MSECS_IN_DAY);
    let weekNumber = Math.floor(dayNumber / 7) + 1;

    return weekNumber;
}

/** Utility function.
 * Gets the approximate digit width of digits in the input `actor` in
 * Pango units.
 *
 * **NOTE** - used in {@link Calendar#_onStyleChange} only, and that function
 * is a leftover stub that doesn't do anything (see the note there...).
 *
 * @param {St.Widget} actor - the actor to get the digit width of. Really only
 * makes sense if this is a St.Label.
 * @returns {number} the approximate digit width in pango units.
 * @see http://developer.gnome.org/pango/stable/pango-Fonts.html#pango-font-metrics-get-approximate-digit-width
 */
function _getDigitWidth(actor){
    let context = actor.get_pango_context();
    let themeNode = actor.get_theme_node();
    let font = themeNode.get_font();
    let metrics = context.get_metrics(font, context.get_language());
    let width = metrics.get_approximate_digit_width();
    return width;
}

/** Gets the abbreviation for a particular day, 0 being Sunday
 * and 6 being Saturday. This will be displayed in the calendar header so
 * returns an abbreviation one letter long.
 *
 * @param {number} dayNumber - the day number to get the abbreviation for,
 * 0 is Sunday and 6 is Saturday.
 * @returns {string} the abbreviation for that day (translated), for example
 * "S" for Saturday/Sunday, or "W" for Wednesday.
 * @see _getEventDayAbbreviation
 */
function _getCalendarDayAbbreviation(dayNumber) {
    let abbreviations = [
        /* Translators: Calendar grid abbreviation for Sunday.
         *
         * NOTE: These grid abbreviations are always shown together
         * and in order, e.g. "S M T W T F S".
         */
        C_("grid sunday", "S"),
        /* Translators: Calendar grid abbreviation for Monday */
        C_("grid monday", "M"),
        /* Translators: Calendar grid abbreviation for Tuesday */
        C_("grid tuesday", "T"),
        /* Translators: Calendar grid abbreviation for Wednesday */
        C_("grid wednesday", "W"),
        /* Translators: Calendar grid abbreviation for Thursday */
        C_("grid thursday", "T"),
        /* Translators: Calendar grid abbreviation for Friday */
        C_("grid friday", "F"),
        /* Translators: Calendar grid abbreviation for Saturday */
        C_("grid saturday", "S")
    ];
    return abbreviations[dayNumber];
}

/** Gets the abbreviation for a particular day, 0 being Sunday
 * and 6 being Saturday. This will be displayed in the events list so
 * should be just long enough to avoid ambiguity (e.g. you can't have "S"
 * for both Saturday and Sunday as this would be ambiguous; use "Su" or "Sa"
 * instead).
 *
 * @inheritparams _getCalendarDayAbbreviation
 * @param {number} dayNumber - the day number to get the abbreviation for,
 * 0 is Sunday and 6 is Saturday.
 * @returns {string} the abbreviation for that day (translated), for us (Sunday
 * through to Saturday): "Su", "M", "T", "W", "Th", "F", "S".
 * @see _getCalendarDayAbbreviation
 */
function _getEventDayAbbreviation(dayNumber) {
    let abbreviations = [
        /* Translators: Event list abbreviation for Sunday.
         *
         * NOTE: These list abbreviations are normally not shown together
         * so they need to be unique (e.g. Tuesday and Thursday cannot
         * both be 'T').
         */
        C_("list sunday", "Su"),
        /* Translators: Event list abbreviation for Monday */
        C_("list monday", "M"),
        /* Translators: Event list abbreviation for Tuesday */
        C_("list tuesday", "T"),
        /* Translators: Event list abbreviation for Wednesday */
        C_("list wednesday", "W"),
        /* Translators: Event list abbreviation for Thursday */
        C_("list thursday", "Th"),
        /* Translators: Event list abbreviation for Friday */
        C_("list friday", "F"),
        /* Translators: Event list abbreviation for Saturday */
        C_("list saturday", "S")
    ];
    return abbreviations[dayNumber];
}

/**
 * Creates a new CalendarEvent.
 * Just stores the input parameters as class members.
 * @param {Date} date - start date for the event
 * @param {Date} end - end date for the event
 * @param {string} summary - summary of the event
 * @param {boolean} allDay - is the event all day?
 * @classdesec
 * Abstraction for an appointment/event in a calendar
 * @class
 */

function CalendarEvent(date, end, summary, allDay) {
    this._init(date, end, summary, allDay);
}

CalendarEvent.prototype = {
    _init: function(date, end, summary, allDay) {
        /** Date for the event.
         * @type {Date} */
        this.date = date;
        /** Date the event finishes.
         * @type {Date} */
        this.end = end;
        /** Summary of the event.
         * @type {string} */
        this.summary = summary;
        /** Whether the event is all day.
         * @type {boolean} */
        this.allDay = allDay;
    }
};


// Interface for appointments/events - e.g. the contents of a calendar

// First, an implementation with no events
/** creates a new EmptyEventSource
 * (the constructor does nothing).
 * 
 * @classdesc
 * This implements the {@link EventSource} API but is empty - does nothing.
 * @class
 * @see DBusEventSource
 * @see EventSource
 * @extends EventSource
 * @implements EventSource
 */
function EmptyEventSource() {
    this._init();
}

EmptyEventSource.prototype = {
    _init: function() {
    },

    /** Implements the requestRange function. Does nothing.
     * @override */
    requestRange: function(begin, end) {
    },

    /** Implements the getEvents function.
     * Always returns the empty array.
     * @override */
    getEvents: function(begin, end) {
        let result = [];
        return result;
    },

    /** Implements the hasEvents function.
     * Always returns `false`.
     * @override */
    hasEvents: function(day) {
        return false;
    }
};
Signals.addSignalMethods(EmptyEventSource.prototype);

/** Representation of an appointment over DBus (i.e. as an array).
 * Used by {@link CalendarServer#GetEvents}, signature 'sssbxxa{sv}'.
 *
 * (I learned what these were by looking at `gnome-shell-calendar-server.c`,
 * unsure if documentation exists somewhere. If so, we should link it).
 *
 * The elements are:
 *
 * * string ('s') - UID of the event (some sort of string ID)
 * * string ('s') - summary (title) of the event.
 * * string ('s') - description of the event.
 * * boolean ('b') - whether the event goes all day.
 * * timestamp ('x') - start time for the event in seconds (or in any case
 * use `new Date(x * 1000)` to convert to a date).
 * * timestamp ('x') - end time for the event in seconds (or in any case
 * use `new Date(x * 1000)` to convert to a date).
 * * ?? ('a{sv}') - Unused for now. From the code: "the a{sv} is used as an
 * escape hatch in case we want to provide more information in the future without
 * breaking ABI ".
 *
 * @type {[string, string, string, boolean, number, number, ???]}
 * @typedef Appointment
 */
/** Definition of the GNOME Calendar Service DBus interface
 * 'org.gnome.Shell.CalendarServer'.
 *
 * Method "GetEvents" and signal "Changed" (well that's all we bother
 * using anyway).
 *
 * Wrapped by {@link CalendarServer}, see that.
 * @const
 * @see CalendarServer
 */
const CalendarServerIface = {
    name: 'org.gnome.Shell.CalendarServer',
    /** Gets events that occur between `start` and `end` time stamps.
     * @param {number} start - start time in seconds (? -- a `Date() / 1000`)
     * @param {number} end - end time in seconds (? -- a `Date() / 1000`)
     * @param {boolean} forceReload - set to `true` to force the calendar server 
     * to manually reload all its events.
     * @returns {[Calendar.Appointment]{@link Appointment}[]} an array of
     * {@link Calendar.Appointment}s occuring between `start` and `end`.
     * @name GetEvents
     * @function
     * @memberof CalendarServer
     */
    methods: [{ name: 'GetEvents',
                inSignature: 'xxb',
                outSignature: 'a(sssbxxa{sv})' }],
    /** Emitted when any appointment in the calendar server is changed (added,
     * removed, details modified).
     * @name Changed
     * @memberof CalendarServer
     * @event
     */
    signals: [{ name: 'Changed',
                inSignature: '' }]
};

/**
 * Creates a new CalendarServer
 * @classdesc
 * The {@link CalendarServer} is a DBus proxy for object
 * '/org/gnome/Shell/CalendarServer' on bus 'org.gnome.shell.CalendarServer'.
 *
 * Has methods GetEvents and signal Changed.
 *
 * @class
 */
const CalendarServer = function () {
    this._init();
};

CalendarServer.prototype = {
     _init: function() {
         DBus.session.proxifyObject(this, 'org.gnome.Shell.CalendarServer', '/org/gnome/Shell/CalendarServer');
     }
};

DBus.proxifyPrototype(CalendarServer.prototype, CalendarServerIface);

/** creates a new DBusEventSource.
 *
 * This resets our events cache ({@link DBusEventSource#_events}) with {@link DBusEventSource#_resetCache}.
 *
 * We then create a local proxy for the DBus {@link CalendarServer}, storing
 * it in {@link DBusEventSource#_dbusProxy}.
 *
 * We connect to its 'Changed' signal to update our cached set of events whenever
 * requested.
 *
 * Then we watch the bus name 'org.gnome.Shell.CalendarServer' so that if
 * another owner takes it over we refresh our set of events.
 * 
 * @param {?} owner - UNUSED.
 * @classdesc
 * This is a source of events that reads data from a session bus service
 * (in particular, it gets them from the bus 'org.gnome.Shell.CalendarServer',
 * updating whenever this emits a ['Changed']{@link CalendarServer.Changed}
 * signal).
 *
 * To use this events source you **must first call** {@link DBusEventSource#requestRange}
 * **before** you can call either {@link DBusEventSource#getEvents} or {@link DBusEventSource#hasEvents}.
 * @see EmptyEventSource
 * @see EventSource
 * @class
 * @extends EventSource
 * @implements EventSource
 */
function DBusEventSource(owner) {
    this._init(owner);
}

/** Utility function - whether two dates are exactly equal.
 * @param {Date} a - a date
 * @param {Date} b - a date to compare with
 * @returns {boolean} whether the two dates are equal (i.e. `a >= b && b >= a`).
 */
function _datesEqual(a, b) {
    if (a < b)
        return false;
    else if (a > b)
        return false;
    return true;
}

/** Utility function - whether two date intervals overlap.
 * @param {Date} a0 - start date of the first interval
 * @param {Date} a1 - end date of the first interval
 * @param {Date} b0 - start date of the second interval
 * @param {Date} b1 - end date of the second interval
 * @returns {boolean} `true` if the interval from `a0` to `a1` overlaps
 * the interval from `b0` to `b1` with a non-zero overlap.
 */
function _dateIntervalsOverlap(a0, a1, b0, b1)
{
    if (a1 <= b0)
        return false;
    else if (b1 <= a0)
        return false;
    else
        return true;
}


DBusEventSource.prototype = {
    _init: function(owner) {
        this._resetCache();

        /** We store a proxy for the DBus calendar server (/org/gnome/Shell/CalendarServer
         * on bus org.gnome.Shell.CalendarServer), used to load events from.
         * @type {Calendar.CalendarServer} */
        this._dbusProxy = new CalendarServer(owner);
        this._dbusProxy.connect('Changed', Lang.bind(this, this._onChanged));

        DBus.session.watch_name('org.gnome.Shell.CalendarServer',
                                false, // do not launch a name-owner if none exists
                                Lang.bind(this, this._onNameAppeared),
                                Lang.bind(this, this._onNameVanished));
    },

    /** Resets our local cache of events. */
    _resetCache: function() {
        /** A local cache of {@link CalendarEvent}s for each event from the
         * DBus source {@link #_dbusProxy}.
         *
         * This is always sorted from oldest (index 0) to most recent (index n-1)
         * event.
         * @type {Calendar.CalendarEvent[]} */
        this._events = [];
        this._lastRequestBegin = null;
        this._lastRequestEnd = null;
    },

    /** Callback when the name owner of bus 'org.gnome.Shell.CalendarServer'
     * appears (usually because it's changed and we now have a new owner).
     * This resets our cache and forces a reload of events (which will in
     * turn emit the ['changed']{@link .changed} signal).
     * @see #_loadEvents
     */
    _onNameAppeared: function(owner) {
        this._resetCache();
        this._loadEvents(true);
    },

    /** Callback when the name owner of bus 'org.gnome.Shell.CalendarServer'
     * vanishes (usually because it's about to be replaced with a new
     * owner). This resets our cache and emits the 'changed' signal.
     * @fires .changed
     */
    _onNameVanished: function(oldOwner) {
        this._resetCache();
        this.emit('changed');
    },

    /** Callback when we receive the ['Changed']{@link CalendarServer.Changed} signal from
     * {@link #_dbusProxy}.
     * We call {@link #_loadEvents} to update our events but do not force
     * a reload.
     * @see #_loadEvents
     */
    _onChanged: function() {
        this._loadEvents(false);
    },

    /** Called when we receive a list of events as a result of
     * a call to {@link CalendarServer#GetEvents} (i.e. from
     * {@link #_loadEvents}).
     *
     * We create a {@link CalendarEvent} for each event in `appointments`,
     * sort them by time ascending, and *replace* our local list of events
     * {@link #_events} with the new events.
     *
     * Finally we emit the 'changed' signal.
     * @param {Calendar.Appointment[]} appointments - list of appointments
     * received as a result of our request.
     * @fires .changed
     */
    _onEventsReceived: function(appointments) {
        let newEvents = [];
        if (appointments != null) {
            for (let n = 0; n < appointments.length; n++) {
                let a = appointments[n];
                let date = new Date(a[4] * 1000);
                let end = new Date(a[5] * 1000);
                let summary = a[1];
                let allDay = a[3];
                let event = new CalendarEvent(date, end, summary, allDay);
                newEvents.push(event);
            }
            newEvents.sort(function(event1, event2) {
                return event1.date.getTime() - event2.date.getTime();
            });
        }

        this._events = newEvents;
        this.emit('changed');
    },

    /** This reloads events from {@link #_dbusProxy}, but {@link #requestRange}
     * **must** have been called first to set the range of events we ask for.
     *
     * Since the method is asynchronous we call {@link #_onEventsReceived} upon
     * receiving the events.
     *
     * @see CalendarServer#GetEvents
     * @param {boolean} forceReload - should we force a reload of events?
     */
    _loadEvents: function(forceReload) {
        if (this._curRequestBegin && this._curRequestEnd){
            let callFlags = 0;
            if (forceReload)
                callFlags |= DBus.CALL_FLAG_START;
            this._dbusProxy.GetEventsRemote(this._curRequestBegin.getTime() / 1000,
                                            this._curRequestEnd.getTime() / 1000,
                                            forceReload,
                                            Lang.bind(this, this._onEventsReceived),
                                            callFlags);
        }
    },

    /** Sets the date range of events that will be cached in this source.
     * Must be called *before* any of {@link #getEvents}
     * or {@link #hasEvents} which will search within this cache.
     *
     * After the range is set {@link #_loadEvents} is called to update our
     * events cache.
     * @override
     * @inheritparams #_loadEvents
     */
    requestRange: function(begin, end, forceReload) {
        if (forceReload || !(_datesEqual(begin, this._lastRequestBegin) && _datesEqual(end, this._lastRequestEnd))) {
            this._lastRequestBegin = begin;
            this._lastRequestEnd = end;
            this._curRequestBegin = begin;
            this._curRequestEnd = end;
            this._loadEvents(forceReload);
        }
    },

    /** @override */
    getEvents: function(begin, end) {
        let result = [];
        for(let n = 0; n < this._events.length; n++) {
            let event = this._events[n];
            if (_dateIntervalsOverlap (event.date, event.end, begin, end)) {
                result.push(event);
            }
        }
        return result;
    },

    /** @override */
    hasEvents: function(day) {
        let dayBegin = _getBeginningOfDay(day);
        let dayEnd = _getEndOfDay(day);

        let events = this.getEvents(dayBegin, dayEnd);

        if (events.length == 0)
            return false;

        return true;
    }
};
Signals.addSignalMethods(DBusEventSource.prototype);
// Signals for DBusEventSource

/** Creates a new Calendar.
 *
 * We store our `eventSource` in {@link #_eventSource}.
 *
 * Then we initialise a bunch of variables to help in displaying the month
 * title (should we display "November"? "November 2012"? Do we show the
 * week numbers (gsetting {@link SHOW_WEEKDATE_KEY})? and so on).
 *
 * Our top-level actor {@link #actor} is a non-homogenous St.Table
 * which is built in {@link #_buildHeader} (it has the month name plus
 * forward and back buttons up the top, and a button for each date of the
 * month).
 *
 * @param {Calendar.EventSource} [eventSource] - an object implementing the
 * {@link EventSource} API like {@link DBusEventSource}. If not provided,
 * the calendar will be graphical only and not display any special styles for
 * dates with events on them.
 * @classdesc
 * ![The {@link Calendar} is in red](pics/calendar.png)
 *
 * The Calendar displays a graphical calendar in the [date menu]{@link DateMenu.DateMenuButton},
 * along with bells and whistles like giving a different style to days with
 * events on them.
 *
 * Note that the Calendar gets its events from an {@link EventSource} such as
 * {@link DBusEventSource}, and can *only* get events from *one* event source
 * object.
 * @class
 */
function Calendar(eventSource) {
    this._init(eventSource);
}

Calendar.prototype = {
    _init: function(eventSource) {
        if (eventSource) {
            /** The event source for this calendar, providing all the events.
             * @type {Calendar.EventSource} */
            this._eventSource = eventSource;

            this._eventSource.connect('changed', Lang.bind(this,
                                                           function() {
                                                               this._update(false);
                                                           }));
        }

        this._weekStart = Shell.util_get_week_start();
        this._weekdate = NaN;
        this._digitWidth = NaN; // <-- UNUSED
        this._settings = new Gio.Settings({ schema: 'org.gnome.shell.calendar' });

        this._settings.connect('changed::' + SHOW_WEEKDATE_KEY, Lang.bind(this, this._onSettingsChange));
        this._useWeekdate = this._settings.get_boolean(SHOW_WEEKDATE_KEY);

        // Find the ordering for month/year in the calendar heading
        this._headerFormatWithoutYear = '%B';
        switch (Gettext_gtk30.gettext('calendar:MY')) {
        case 'calendar:MY':
            this._headerFormat = '%B %Y';
            break;
        case 'calendar:YM':
            this._headerFormat = '%Y %B';
            break;
        default:
            log('Translation of "calendar:MY" in GTK+ is not correct');
            this._headerFormat = '%B %Y';
            break;
        }

        /** The currently-selected date inthe calendar.
         * @type {Date} */
        this._selectedDate = new Date();

        /** Top-level actor. A non-homogeneous table with style class
         * 'calendar'.
         * @type {St.Label} */
        this.actor = new St.Table({ homogeneous: false,
                                    style_class: 'calendar',
                                    reactive: true });

        this.actor.connect('scroll-event',
                           Lang.bind(this, this._onScroll));

        this._buildHeader ();
    },

    /** Sets the calendar to show a specific date.
     *
     * If `date` is already the same day as our current date, we don't
     * do anything unless `forceReload` is true (in which case we call
     * {@link #_update}).
     *
     * If `date` is *not* the same day as our current date, we
     * update {@link #_selectedDate} and call {@link #_update} with `forceReload`
     * to update the graphical bits of the calendar (including adding an extra
     * pseudo style class to the date button of the selected date).
     *
     * @param {Date} date - date to set the current date to
     * @inheritparams #_update
     * @fires .selected-date-changed
     */
    setDate: function(date, forceReload) {
        if (!_sameDay(date, this._selectedDate)) {
            this._selectedDate = date;
            this._update(forceReload);
            this.emit('selected-date-changed', new Date(this._selectedDate));
        } else {
            if (forceReload)
                this._update(forceReload);
        }
    },

    /** Builds the header (month and forward/back keys plus day titles)
     * of the calendar. The actual date bit is made in {@link #_update}.
     *
     * ![The Calendar outlined in red](pics/calendaronly.png)
     *
     * * Create the top line of the character '<| Month Year |>' (back
     * button, month/year header, forward button). Add it to the table
     * {@link #actor} making it span 7 columns (or 8 if we are showing
     * week numbers, read from gsettings key {@link SHOW_WEEKDATE_KEY}).
     * * the back button and forward button (St.Buttons) have style class
     * 'calendar-change-month-(back|forward); the month label has style class
     * 'calendar-month-label'.
     * * Add the second row of the calendar being the weekday labels
     * ("S M T W T F S"), see {@link _getCalendarDayAbbreviation}. These
     * are St.Labels with style classes 'calendar-day-base calendar-day-heading'.
     * @see #_update
     */
    _buildHeader: function() {
        let offsetCols = this._useWeekdate ? 1 : 0;
        this.actor.destroy_children();

        // Top line of the calendar '<| September 2009 |>'
        this._topBox = new St.BoxLayout();
        this.actor.add(this._topBox,
                       { row: 0, col: 0, col_span: offsetCols + 7 });

        this.actor.connect('style-changed', Lang.bind(this, this._onStyleChange));

        let back = new St.Button({ style_class: 'calendar-change-month-back' });
        this._topBox.add(back);
        back.connect('clicked', Lang.bind(this, this._onPrevMonthButtonClicked));

        this._monthLabel = new St.Label({style_class: 'calendar-month-label'});
        this._topBox.add(this._monthLabel, { expand: true, x_fill: false, x_align: St.Align.MIDDLE });

        let forward = new St.Button({ style_class: 'calendar-change-month-forward' });
        this._topBox.add(forward);
        forward.connect('clicked', Lang.bind(this, this._onNextMonthButtonClicked));

        // Add weekday labels...
        //
        // We need to figure out the abbreviated localized names for the days of the week;
        // we do this by just getting the next 7 days starting from right now and then putting
        // them in the right cell in the table. It doesn't matter if we add them in order
        let iter = new Date(this._selectedDate);
        iter.setSeconds(0); // Leap second protection. Hah!
        iter.setHours(12);
        for (let i = 0; i < 7; i++) {
            // Could use iter.toLocaleFormat('%a') but that normally gives three characters
            // and we want, ideally, a single character for e.g. S M T W T F S
            let customDayAbbrev = _getCalendarDayAbbreviation(iter.getDay());
            let label = new St.Label({ style_class: 'calendar-day-base calendar-day-heading',
                                       text: customDayAbbrev });
            this.actor.add(label,
                           { row: 1,
                             col: offsetCols + (7 + iter.getDay() - this._weekStart) % 7,
                             x_fill: false, x_align: St.Align.MIDDLE });
            iter.setTime(iter.getTime() + MSECS_IN_DAY);
        }

        // All the children after this are days, and get removed when we update the calendar
        this._firstDayIndex = this.actor.get_children().length;
    },

    /** Callback when the style of {@link #actor} changes. **DOESN'T DO ANYTHING**
     * (it calls {@link #_setWeekdateHeaderWidth} which does nothing because
     * it's an old stub of a function).
     * @deprecated since 3.2 (well not deprecated, but doesn't do anything).
     */
    _onStyleChange: function(actor, event) {
        // width of a digit in pango units
        this._digitWidth = _getDigitWidth(this.actor) / Pango.SCALE;
        this._setWeekdateHeaderWidth();
    },

    /** **DOESN'T DO ANYTHING** (it has an `if` check in it that will always
     * fail, because I suspect this is the stub of some older code that
     * was removed).
     * @deprecated since 3.2 (well not deprecated, but doesn't do anything).
     */
    _setWeekdateHeaderWidth: function() {
        if (this.digitWidth != NaN && this._useWeekdate && this._weekdateHeader) {
            this._weekdateHeader.set_width (this._digitWidth * WEEKDATE_HEADER_WIDTH_DIGITS);
        }
    },

    /** Callback when the user scrolls over the calendar. This changes
     * which month we are displaying (UP and LEFT go to the previous month,
     * DOWN and RIGHT go to the next).
     * @see #_onPrevMonthButtonClicked
     * @see #_onNextMonthButtonClicked
     */
    _onScroll : function(actor, event) {
        switch (event.get_scroll_direction()) {
        case Clutter.ScrollDirection.UP:
        case Clutter.ScrollDirection.LEFT:
            this._onPrevMonthButtonClicked();
            break;
        case Clutter.ScrollDirection.DOWN:
        case Clutter.ScrollDirection.RIGHT:
            this._onNextMonthButtonClicked();
            break;
        }
    },

    /** Scroll to the previous month (called when the user clicks the 'previous'
     * button in the calendar header or when they up/left back over the calendar).
     *
     * Calculates the new date to scroll to (the same date in the previous month,
     * with some footwork to choose the closest date if it doesn't exist in
     * the previous month) and calls {@link #setDate} to change the date
     * (which will in turn call #_update to update the graphics).
     */
    _onPrevMonthButtonClicked: function() {
        let newDate = new Date(this._selectedDate);
        let oldMonth = newDate.getMonth();
        if (oldMonth == 0) {
            newDate.setMonth(11);
            newDate.setFullYear(newDate.getFullYear() - 1);
            if (newDate.getMonth() != 11) {
                let day = 32 - new Date(newDate.getFullYear() - 1, 11, 32).getDate();
                newDate = new Date(newDate.getFullYear() - 1, 11, day);
            }
        }
        else {
            newDate.setMonth(oldMonth - 1);
            if (newDate.getMonth() != oldMonth - 1) {
                let day = 32 - new Date(newDate.getFullYear(), oldMonth - 1, 32).getDate();
                newDate = new Date(newDate.getFullYear(), oldMonth - 1, day);
            }
        }

        this.setDate(newDate, false);
   },

    /** Scroll to the next month (called when the user clicks the 'next'
     * button in the calendar header or when they scroll down/right over the calendar).
     *
     * Calculates the new date to scroll to (the same date in the next month,
     * with some footwork to choose the closest date if it doesn't exist in
     * the next month) and calls {@link #setDate} to change the date
     * (which will in turn call #_update to update the graphics).
     */
   _onNextMonthButtonClicked: function() {
        let newDate = new Date(this._selectedDate);
        let oldMonth = newDate.getMonth();
        if (oldMonth == 11) {
            newDate.setMonth(0);
            newDate.setFullYear(newDate.getFullYear() + 1);
            if (newDate.getMonth() != 0) {
                let day = 32 - new Date(newDate.getFullYear() + 1, 0, 32).getDate();
                newDate = new Date(newDate.getFullYear() + 1, 0, day);
            }
        }
        else {
            newDate.setMonth(oldMonth + 1);
            if (newDate.getMonth() != oldMonth + 1) {
                let day = 32 - new Date(newDate.getFullYear(), oldMonth + 1, 32).getDate();
                newDate = new Date(newDate.getFullYear(), oldMonth + 1, day);
            }
        }

       this.setDate(newDate, false);
    },

    /** Callback when the value of {@link SHOW_WEEKDATE_KEY} changes.
     * This rebuilds the calendar to show (or not) week dates (they
     * are displayed one column to the left of the date part of the calendar).
     * @see #_buildHeader
     * @see #_update
     */
    _onSettingsChange: function() {
        this._useWeekdate = this._settings.get_boolean(SHOW_WEEKDATE_KEY);
        this._buildHeader();
        this._update(false);
    },

    /** Creates the buttons on the calendar for each date, laying them out
     * in the table {@link #actor}.
     *
     * Mainly involves a lot of footwork to make sure the dates line up with
     * the appropriate days, with extra care switching years, as well as applying
     * various CSS style classes to dates so they stand out.
     *
     * Each date button is a St.Button with style classes 'calendar-day-base calendar-day'.
     * If it is for a work day (Mon-Fri), it also gets class 'calendar-work-day';
     * otherwise it gets class 'calendar-nonwork-day' (for the default theme
     * non work days get a slightly lighter background colour and the work
     * days get a black background colour).
     *
     * If the date is in the top row or left-most column of the calendar it
     * gets style class 'calendar-day-top' or 'calendar-day-left'; this is just
     * a hack so that it looks like every date button has a 1-pixel border
     * around it (where adjacent date buttons are separated by just 1 pixel).
     *
     * If the date button is for today it gets style class 'calendar-today'
     * (for the default theme the today date gets an underline and highlight).
     *
     * If the date button is for a month not in the month we are displaying
     * (for example at the end of the month it might show the '1' for the next
     * month just to finish off the row), it gets style class 'calendar-other-month-day'.
     *
     * The date button for the current selected date gets *pseudo* style class
     * 'active'.
     *
     * Any date that has events on it is given additional style class
     * 'calendar-day-with-events'.
     *
     * Also, if week numbers are to be shown they are added with style class
     * 'calendar-day-base calendar-week-number'.
     *
     * Finally, we request that {@link #_eventSource} load events from
     * the range being the currently-displayed begin date to the currently-displayed
     * end date ({@link EventSource#requestRange}).
     * 
     * @param {boolean} forceReload - whether to force a reload of events
     * in our underlying {@link #_eventSource} (we will always call
     * {@link EventSource#requestRange}, just what you want its
     * `forceReload` argument to be).
     * @see EventSource#requestRange
     */
    _update: function(forceReload) {
        let now = new Date();

        if (_sameYear(this._selectedDate, now))
            this._monthLabel.text = this._selectedDate.toLocaleFormat(this._headerFormatWithoutYear);
        else
            this._monthLabel.text = this._selectedDate.toLocaleFormat(this._headerFormat);

        // Remove everything but the topBox and the weekday labels
        let children = this.actor.get_children();
        for (let i = this._firstDayIndex; i < children.length; i++)
            children[i].destroy();

        // Start at the beginning of the week before the start of the month
        let beginDate = new Date(this._selectedDate);
        beginDate.setDate(1);
        beginDate.setSeconds(0);
        beginDate.setHours(12);
        let daysToWeekStart = (7 + beginDate.getDay() - this._weekStart) % 7;
        beginDate.setTime(beginDate.getTime() - daysToWeekStart * MSECS_IN_DAY);

        let iter = new Date(beginDate);
        let row = 2;
        while (true) {
            let button = new St.Button({ label: iter.getDate().toString() });

            if (!this._eventSource)
                button.reactive = false;

            let iterStr = iter.toUTCString();
            button.connect('clicked', Lang.bind(this, function() {
                let newlySelectedDate = new Date(iterStr);
                this.setDate(newlySelectedDate, false);
            }));

            let hasEvents = this._eventSource && this._eventSource.hasEvents(iter);
            let styleClass = 'calendar-day-base calendar-day';
            if (_isWorkDay(iter))
                styleClass += ' calendar-work-day'
            else
                styleClass += ' calendar-nonwork-day'

            // Hack used in lieu of border-collapse - see gnome-shell.css
            if (row == 2)
                styleClass = 'calendar-day-top ' + styleClass;
            if (iter.getDay() == this._weekStart)
                styleClass = 'calendar-day-left ' + styleClass;

            if (_sameDay(now, iter))
                styleClass += ' calendar-today';
            else if (iter.getMonth() != this._selectedDate.getMonth())
                styleClass += ' calendar-other-month-day';

            if (_sameDay(this._selectedDate, iter))
                button.add_style_pseudo_class('active');

            if (hasEvents)
                styleClass += ' calendar-day-with-events'

            button.style_class = styleClass;

            let offsetCols = this._useWeekdate ? 1 : 0;
            this.actor.add(button,
                           { row: row, col: offsetCols + (7 + iter.getDay() - this._weekStart) % 7 });

            if (this._useWeekdate && iter.getDay() == 4) {
                let label = new St.Label({ text: _getCalendarWeekForDate(iter).toString(),
                                           style_class: 'calendar-day-base calendar-week-number'});
                this.actor.add(label,
                               { row: row, col: 0, y_align: St.Align.MIDDLE });
            }

            iter.setTime(iter.getTime() + MSECS_IN_DAY);
            if (iter.getDay() == this._weekStart) {
                // We stop on the first "first day of the week" after the month we are displaying
                if (iter.getMonth() > this._selectedDate.getMonth() || iter.getYear() > this._selectedDate.getYear())
                    break;
                row++;
            }
        }
        // Signal to the event source that we are interested in events
        // only from this date range
        if (this._eventSource)
            this._eventSource.requestRange(beginDate, iter, forceReload);
    }
};

Signals.addSignalMethods(Calendar.prototype);
/// calendar events
/** Emitted whenever the user changes the selected date of the calendar
 * (for example they click on another day in the calendar).
 *
 * The {@link DateMenu.DateMenuButton} uses this to make sure that its
 * {@link EventsList} is updated to show events for that date.
 * @name selected-date-changed
 * @event
 * @memberof Calendar
 */

/** creates a new EventsList
 *
 * Our top-level actor {@link #actor} is a vertical St.BoxLayout with
 * style class 'events-header-vbox'.
 * @param {Calendar.EventSource} eventSource - an object implementing the
 * {@link EventSource} API like {@link DBusEventSource}.
 * @classdesc
 * ![{@link EventsList} is in blue](pics/calendar.png)
 *
 * An events list is a list of events for today, tomorrow, and "upcoming".
 * One of these is in the [date menu]{@link DateMenu.DateMenuButton} to the right
 * of the {@link Calendar}.
 *
 * It displays its events from a {@link EventSource}.
 * @class
 */
function EventsList(eventSource) {
    this._init(eventSource);
}

EventsList.prototype = {
    _init: function(eventSource) {
        /** Top-level actor. A vertical St.BoxLayout with style class
         * 'events-header-vbox'.
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ vertical: true, style_class: 'events-header-vbox'});
        this._date = new Date();
        /** The underlying event source we get our events from.
         * {@type Calendar.EventSource} */
        this._eventSource = eventSource;
        this._eventSource.connect('changed', Lang.bind(this, this._update));
        this._desktopSettings = new Gio.Settings({ schema: 'org.gnome.desktop.interface' });
        this._desktopSettings.connect('changed', Lang.bind(this, this._update));
        this._weekStart = Shell.util_get_week_start();

        this._update();
    },

    /** This adds a single event to the events list {@link #actor}.
     *
     * ![An event under its period heading, showing the day name, time, and event title](pics/EventsListEvent.png)
     *
     * We add the event's day name `day` in `dayNameBox` if `includeDayName` is `true`
     * (St.Label with style class 'events-day-dayname').
     *
     * We add the event's time (either a time or "All Day") into `timeBox`
     * (St.Label with style class 'events-day-time').
     *
     * We add the event's title/summary `eventTitleBox`
     * (St.Label with style class 'events-day-task').
     *
     * The reason the BoxLayouts `dayNameBox`, `timeBox` and `eventTitleBox`
     * should be *vertical* is that if there are multiple events for a single
     * period, you would call {@link #_addEvent} with the same boxes each
     * time and then the days, times, and titles would be left-aligned vertically.
     *
     * @param {St.BoxLayout} dayNameBox - the vertical St.BoxLayout we should put
     * the event's day name in.
     * @param {St.BoxLayout} timeBox - the vertical St.BoxLayout we should put
     * the event's time in.
     * @param {St.BoxLayout} eventTitleBox - the vertical St.BoxLayout we should put
     * the event's title/summary in.
     * @param {boolean} includeDayName - show the day name ("Th" in the pic above)?
     * @param {string} day - string with the day of the event to display
     * if `includeDayName` was true (see {@link _getEventDayAbbreviation}) e.g. "Th".
     * @param {string} time - time of the event as a string (see {@link _formatEventTime})
     * e.g. "All Day" or "09:00".
     * @param {string} desc - event description.
     */
    _addEvent: function(dayNameBox, timeBox, eventTitleBox, includeDayName, day, time, desc) {
        if (includeDayName) {
            dayNameBox.add(new St.Label( { style_class: 'events-day-dayname',
                                           text: day } ),
                           { x_fill: true } );
        }
        timeBox.add(new St.Label( { style_class: 'events-day-time',
                                    text: time} ),
                    { x_fill: true } );
        eventTitleBox.add(new St.Label( { style_class: 'events-day-task',
                                          text: desc} ));
    },

    /** Adds events from a particular period to the events list {@link #actor}.
     *
     * ![Each "Period" is the bold heading ("Today", "Tomorrow", ..) with the list of events for that period](pics/EventsListToday.png)
     *
     * This creates a bold title `header` (like "Today" in the picture above).
     * It's a St.Label with style class 'events-day-header'.
     *
     * We then create three vertical box layouts and lay them out in columns,
     * one for the event day name (style class 'events-day-name-box'), one
     * for the event time ('events-time-box'), and one for the event titles
     * ('events-event-box'). This is so that they all line up nicely vertically
     * and don't look jaggedy.
     *
     * We get a list of events in this period ({@link EventSource#getEvents})
     * and for each of them we call {@link #_addEvent} to add it.
     *
     * If there were *no* events for this day and `showNothingScheduled` is true,
     * we add a dummy event that just displays "Nothing Scheduled" under the
     * heading. If `showNothingScheduled` was false then the entire section
     * (heading + contents) is omitted.
     *
     * @param {string} header - heading to show for this period (e.g. "Today").
     * @param {Date} begin - start datetime for the period
     * @param {Date} end - end datetime for the period
     * @param {boolean} includeDayName - include the day name before the time?
     * (in the picture above the "This Week" entry has the day name "Th").
     * @param {boolean} showNothingScheduled - whether to show text "Nothing Scheduled" if there
     * are no events for this period, or just don't show the period (or title) at all.
     * @see _addEvent
     */
    _addPeriod: function(header, begin, end, includeDayName, showNothingScheduled) {
        if (!this._eventSource)
            return;

        let events = this._eventSource.getEvents(begin, end);

        let clockFormat = this._desktopSettings.get_string(CLOCK_FORMAT_KEY);;

        if (events.length == 0 && !showNothingScheduled)
            return;

        let vbox = new St.BoxLayout( {vertical: true} );
        this.actor.add(vbox);

        vbox.add(new St.Label({ style_class: 'events-day-header', text: header }));
        let box = new St.BoxLayout({style_class: 'events-header-hbox'});
        let dayNameBox = new St.BoxLayout({ vertical: true, style_class: 'events-day-name-box' });
        let timeBox = new St.BoxLayout({ vertical: true, style_class: 'events-time-box' });
        let eventTitleBox = new St.BoxLayout({ vertical: true, style_class: 'events-event-box' });
        box.add(dayNameBox, {x_fill: false});
        box.add(timeBox, {x_fill: false});
        box.add(eventTitleBox, {expand: true});
        vbox.add(box);

        for (let n = 0; n < events.length; n++) {
            let event = events[n];
            let dayString = _getEventDayAbbreviation(event.date.getDay());
            let timeString = _formatEventTime(event, clockFormat);
            let summaryString = event.summary;
            this._addEvent(dayNameBox, timeBox, eventTitleBox, includeDayName, dayString, timeString, summaryString);
        }

        if (events.length == 0 && showNothingScheduled) {
            let now = new Date();
            /* Translators: Text to show if there are no events */
            let nothingEvent = new CalendarEvent(now, now, _("Nothing Scheduled"), true);
            let timeString = _formatEventTime(nothingEvent, clockFormat);
            this._addEvent(dayNameBox, timeBox, eventTitleBox, false, "", timeString, nothingEvent.summary);
        }
    },

    /** Shows the events from some date that is not today.
     *
     * ![EventsList for another day](pics/EventsListOther.png)
     *
     * This will only show events for `day`, as opposed to {@link #_showToday}
     * which shows events for today, tomorrow, and this/next week.
     *
     * We achieve this with a call to {@link #_addPeriod} with section title
     * "%A, %B %d" ("Day name, Month name Date") if `day` is in the current
     * year, or we add a year on the end if `day` is in a different year.
     * We show "No events scheduled" if there are no events for that day,
     * and do not display the day name.
     * @param {Date} day - day to show events for.
     * @see #_addPeriod
     */
    _showOtherDay: function(day) {
        this.actor.destroy_children();

        let dayBegin = _getBeginningOfDay(day);
        let dayEnd = _getEndOfDay(day);

        let dayString;
        let now = new Date();
        if (_sameYear(day, now))
            /* Translators: Shown on calendar heading when selected day occurs on current year */
            dayString = day.toLocaleFormat(C_("calendar heading", "%A, %B %d"));
        else
            /* Translators: Shown on calendar heading when selected day occurs on different year */
            dayString = day.toLocaleFormat(C_("calendar heading", "%A, %B %d, %Y"));
        this._addPeriod(dayString, dayBegin, dayEnd, false, true);
    },

    /** Shows events from today, with heading "Today".
     * 
     * ![EventsList for "Today"](pics/EventsListToday.png)
     *
     * This is special in that it *also* shows events for "Tomorrow" and
     * "This week" (if we are within the first five days of the week) or
     * "Next week" (otherwise).
     *
     * We get rid of all the children from {@link #actor}.
     *
     * Then we use {@link #_addPeriod} to add events for the period "Today",
     * not showing day names and showing "Nothing Scheduled" if nothing is
     * scheduled.
     *
     * Then we add a "Tomorrow" section with the same parameters.
     *
     * If we are currently less than 5 days into the week we show events for
     * this week using {@link #_addPeriod} with title "This week", showing
     * day names (see pic above: "Th") but skipping the section entirely if
     * nothing is scheduled.
     *
     * If we are at least 5 days into the week (i.e. almost at the end) we
     * do the same, but with "Next week" instead.
     * @see #_addPeriod
     */
    _showToday: function() {
        this.actor.destroy_children();

        let now = new Date();
        let dayBegin = _getBeginningOfDay(now);
        let dayEnd = _getEndOfDay(now);
        this._addPeriod(_("Today"), dayBegin, dayEnd, false, true);

        let tomorrowBegin = new Date(dayBegin.getTime() + 86400 * 1000);
        let tomorrowEnd = new Date(dayEnd.getTime() + 86400 * 1000);
        this._addPeriod(_("Tomorrow"), tomorrowBegin, tomorrowEnd, false, true);

        if (dayEnd.getDay() <= 4 + this._weekStart) {
            /* If now is within the first 5 days we show "This week" and
             * include events up until and including Saturday/Sunday
             * (depending on whether a week starts on Sunday/Monday).
             */
            let thisWeekBegin = new Date(dayBegin.getTime() + 2 * 86400 * 1000);
            let thisWeekEnd = new Date(dayEnd.getTime() + (6 + this._weekStart - dayEnd.getDay()) * 86400 * 1000);
            this._addPeriod(_("This week"), thisWeekBegin, thisWeekEnd, true, false);
        } else {
            /* otherwise it's one of the two last days of the week ... show
             * "Next week" and include events up until and including *next*
             * Saturday/Sunday
             */
            let nextWeekBegin = new Date(dayBegin.getTime() + 2 * 86400 * 1000);
            let nextWeekEnd = new Date(dayEnd.getTime() + (13 + this._weekStart - dayEnd.getDay()) * 86400 * 1000);
            this._addPeriod(_("Next week"), nextWeekBegin, nextWeekEnd, true, false);
        }
    },

    /** Sets the event list to show events from a specific date
     * If we are not already showing events from `date`, we call
     * {@link #_update} to do so.
     * @inheritparams Calendar#setDate
     */
    setDate: function(date) {
        if (!_sameDay(date, this._date)) {
            this._date = date;
            this._update();
        }
    },

    /** Updates our events list. Called when the date is changed
     * ({@link #setDate}) or our events source emits its ['changed']{@link EventSource.changed}
     * signal, meaning events have been added, removed, or updated.
     *
     * We divert work out to either {@link #_showToday} or {@link #_showOtherDay}.
     */
    _update: function() {
        let today = new Date();
        if (_sameDay (this._date, today)) {
            this._showToday();
        } else {
            this._showOtherDay(this._date);
        }
    }
};

//// VIRTUAL DOCUMENTATION FOR EventSource API -- It's handy ////
/** creates a new EventSource
 * @classdesc
 * This is a dummy "class" (it's not actually a class in the JS) to describe
 * the "Event Source" API, used by the {@link Calendar} to know about the
 * user's events.
 *
 * The EventSource API consists of three functions, requestRange, getEvents,
 * hasEvents, and the 'changed' signal.
 *
 * In general an Event Source provides access to many {@link Calendar.Event}s.
 * Think of it as a big calendar.
 *
 * This API is implemented by {@link EmptyEventSource} and {@link DBusEventSource}.
 *
 * Quick API summary:
 *
 * * {@link EventSource#requestRange} - **call this once before any of the
 * other functions** to specify the range of dates this event source should
 * look at. Should be called before {@link EventSource#getEvents} or
 * {@link EventSource#hasEvents} as these functions will use the request range
 * to search through.
 * * {@link EventSource#getEvents} - return an array of {@link CalendarEvent}s
 * of events between the input dates. Events returned will be a subset of
 * those loaded in {@link EventSource#requestRange}.
 * * {@link EventSource#hasEvents} - whether a particular day has any events
 * that partially or fully occur on it.
 * * {@link EventSource.changed} signal - should be emitted whenever any of
 * the events has been added, removed, or modified.
 *
 * @see EmptyEventSource
 * @see DBusEventSource
 * @interface
 * @class
 * @name EventSource
 */
/** Emitted whenever any of the events from this event source has changed,
 * been added, or removed.
 * @name changed
 * @event
 * @memberof EventSource
 */
/** Must be called *before* any of {@link EventSource#getEvents}
 * or {@link EventSource#hasEvents} to set the range of events
 * this event source will cache (calls to `getEvents` and `hasEvents`
 * will only search in this subset).
 * @param {Date} begin - beginning date to use fr future requests.
 * @param {Date} end - end date to use for future requests.
 * @function
 * @name requestRange
 * @memberof EventSource
 */
/** Gets the events that occur (either partially or fully) within this datetime range.
 *
 * @param {Date} begin - start datetime.
 * @param {Date} end - end datetime.
 * @returns {Calendar.Event[]} array of {@link Calendar.Event}s with start/end
 * date/times that overlap with the `begin` to `end` interval (i.e.
 * those that fall wholly within `begin` to `end`, as well as those that
 * start before `begin` but end after it or those that start before `end`
 * and end after it).
 * @function
 * @name getEvents
 * @memberof EventSource
 */
/** Whether this event source has any events on this day.
 *
 * @param {Date} day - the date to look up events for.
 * @returns {boolean} whether there are any events on the same day as this
 * one.
 * @function
 * @name hasEvents
 * @memberof EventSource
 */
//// END VIRTUAL DOCUMENTATION FOR EventSource API -- It's handy ////
