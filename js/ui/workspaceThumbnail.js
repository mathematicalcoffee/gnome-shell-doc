// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the classes in the workspaces sidebar in the 'Windows' tab of the
 * Overview - the little snapshots of each workspace allowing you to drag
 * windows between them.
 *
 * ![{@link ThumbnailsBox} (yellow), {@link WorkspaceThumbnail} containing {@link WindowClone}s (green)](pics/workspaceThumbnails.png)
 *
 * A {@link ThumbnailsBox} (workspace thumbnail sidebar) contains
 * one {@link WorkspaceThumbnail} per workspace, each showing a {@link WindowClone}
 * per window.
 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const DND = imports.ui.dnd;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Workspace = imports.ui.workspace;
const WorkspacesView = imports.ui.workspacesView;

/** @+
 * @const
 * @default
 * @type {number} */
/** The maximum size of a thumbnail is 1/8 the width and height of the screen */
let MAX_THUMBNAIL_SCALE = 1/8.;

/** When the size of workspace thumbnails in the thumbnails box changes
 * (for example they have to all shrink to add in a new thumbnail), the animation
 * is done over ths time (seconds). */
const RESCALE_ANIMATION_TIME = 0.2;
/** When a new workspace is added or on is removed, its thumbnail is
 * slidden in/out of the thumbnails box over this time (seconds) */
const SLIDE_ANIMATION_TIME = 0.2;
/** @- */

/** creates a new WindowClone
 *
 * First we create the top-level actor {@link #actor} which is a
 * Clutter.Clone of `realWindow`'s texture and set its `_delegate` to point
 * back to this WindowClone instance.
 *
 * We store the window actor and its meta window in {@link #realWindow}
 * and {@link #metaWindow}.
 * @param {Meta.WindowActor} realWindow - the metacity window actor that this
 * is a clone of.
 *
 * @classdesc
 * This is a thumbnail of a window that appears in a {@link WorkspaceThumbnail}.
 *
 * The main actor is a Clutter.Clone of the window's texture so that we have
 * a little up-to-date thumbnail
 *
 * Note - it's not the same as a {@link Workspace.WindowClone} which is the
 * window thumbnail that appears in the overview in the main overview area as
 * opposed to this one which is in the workspace thumbnails sidebar.
 *
 * This {@link WindowClone} is much simpler than the {@link Workspace.WindoClone}:
 *
 * * we don't worry about size changes of the underlying window, but we do match
 * position changes;
 * * no farting around with the invisible border of a meta window actor over its
 * meta window; all we have to do is position the clone at the same place relative
 * to our parent {@link WorkspaceThumbnail} that the window is on its workspace
 * * no scroll-to-zoom on a {@link WindowClone}
 * * this {@link WindowClone} does *not* accept drops, whereas the
 * {@link Workspace.WindowClone) *does* (deferring them to its parent {@link Workspace.Workspace}).
 * * probably other changes too.
 *
 * ![{@link ThumbnailsBox} (yellow), {@link WorkspaceThumbnail} containing {@link WindowClone}s (green)](pics/workspaceThumbnails.png)
 * @todo picture
 * @class
 */
function WindowClone(realWindow) {
    this._init(realWindow);
}

WindowClone.prototype = {
    _init : function(realWindow) {
        /** The top-level actor is a clutter.clone of the underlying
         * metacity window actor's texture.
         * @type {Clutter.Clone} */
        this.actor = new Clutter.Clone({ source: realWindow.get_texture(),
                                         reactive: true });
        this.actor._delegate = this;
        /** The Meta.WindowActor this clone is for.
         * @type {Meta.WindowActor} */
        this.realWindow = realWindow;
        /** The Meta.Window for the window actor this clone is for
         * (`[this.realWindow]{@link #realWindow}.meta_window`).
         * @type {Meta.Window} */
        this.metaWindow = realWindow.meta_window;

        this._positionChangedId = this.realWindow.connect('position-changed',
                                                          Lang.bind(this, this._onPositionChanged));
        this._realWindowDestroyedId = this.realWindow.connect('destroy',
                                                              Lang.bind(this, this._disconnectRealWindowSignals));
        this._onPositionChanged();

        this.actor.connect('button-release-event',
                           Lang.bind(this, this._onButtonRelease));

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        /** The draggable for this actor.
         * We use parameters `restoreOnSuccess=true`, `manualMode=true`,
         * `dragActorMaxSize={@link WINDOW_DND_SIZE}`,
         * `dragActorOpacity={@link DRAGGING_WINDOW_OPACITY}`.
         * @type {DND._Draggable} */
        this._draggable = DND.makeDraggable(this.actor,
                                            { restoreOnSuccess: true,
                                              dragActorMaxSize: Workspace.WINDOW_DND_SIZE,
                                              dragActorOpacity: Workspace.DRAGGING_WINDOW_OPACITY });
        this._draggable.connect('drag-begin', Lang.bind(this, this._onDragBegin));
        this._draggable.connect('drag-end', Lang.bind(this, this._onDragEnd));
        /** Whether we are currently being dragged.
         * @type {boolean} */
        this.inDrag = false;
    },

    /** Makes this actor be stacked above the other actor `actor`,
     * i.e. calls `clutter_actor_raise(self, actor)` to raise this actor to
     * be above `actor`. Use `actor=null` to lower us to the bottom.
     *
     * Used in {@link WorkspacesView.WorkspacesView#_onRestacked}.
     * @see WorkspacesView.WorkspacesView#_onRestacked.
     * @param {?Clutter.Actor} actor - other actor to appear above. If `null`,
     * this actor is lowered to the bottom.
     */
    setStackAbove: function (actor) {
        this._stackAbove = actor;
        if (this._stackAbove == null)
            this.actor.lower_bottom();
        else
            this.actor.raise(this._stackAbove);
    },

    /** Destroys {@link #actor} which triggers {@link #_onDestroy}.
     */
    destroy: function () {
        this.actor.destroy();
    },

    /** Callback for the 'position-changed' signal of {@link #realWindow}
     * (when the underlying window's position changes).
     *
     * This updates {@link #realWindow}'s position to
     * match that of its Meta.Window so that the position of
     * our window thumbnail in its workspace thumbnail is in
     * sync with how the *metacity* window appears on its
     * *metacity* workspace.
     *
     * (Note - couldn't we just have used a clutter constraint
     * on position?)
     */
    _onPositionChanged: function() {
        let rect = this.metaWindow.get_outer_rect();
        this.actor.set_position(this.realWindow.x, this.realWindow.y);
    },

    /** Disconnects from signals we were listening to on
     * {@link #realWindow} (in particular, the 'position-changed'
     * and 'destroy' signals). */
    _disconnectRealWindowSignals: function() {
        if (this._positionChangedId != 0) {
            this.realWindow.disconnect(this._positionChangedId);
            this._positionChangedId = 0;
        }

        if (this._realWindowDestroyedId != 0) {
            this.realWindow.disconnect(this._realWindowDestroyedId);
            this._realWindowDestroyedId = 0;
        }
    },

    /** Callback when our actor {@link #actor} is destroyed, for
     * example when {@link #destroy} is called.
     *
     * This disconnects all our outstanding signal, emits 'drag-end' if we're
     * currently in the middle of a drag, and then disconnects all other signals.
     * @fires .drag-end
     */
    _onDestroy: function() {
        this._disconnectRealWindowSignals();

        this.actor._delegate = null;

        if (this.inDrag) {
            this.emit('drag-end');
            this.inDrag = false;
        }

        this.disconnectAll();
    },

    /** Callback when someone clicks this window clone.
     * Emits the 'selected' signal with the current time.
     *
     * (This is caught by the parent {@link WorkspaceThumbnail} to
     * activate the window).
     * @fires .selected
     */
    _onButtonRelease : function (actor, event) {
        this.emit('selected', event.get_time());

        return true;
    },

    /** Callback when {@link #_draggable} emits 'drag-begin', i.e.
     * this window clone has started to be dragged.
     *
     * This emits a 'drag-begin' signal from the clone
     * so that other classes can listen.
     *
     * Also sets {#inDrag} to true.
     * @fires .drag-begin
     */
    _onDragBegin : function (draggable, time) {
        this.inDrag = true;
        this.emit('drag-begin');
    },

    /** Callback for the 'drag-end' signal of {@link #_draggable}.
     *
     * Sets {@link #inDrag} to false and makes sure our stacking is
     * in sync with the actual window stacking order.
     * Emits the 'drag-end' signal from this clone.
     * @fires .drag-end
     */
    _onDragEnd : function (draggable, time, snapback) {
        this.inDrag = false;

        // We may not have a parent if DnD completed successfully, in
        // which case our clone will shortly be destroyed and replaced
        // with a new one on the target workspace.
        if (this.actor.get_parent() != null) {
            if (this._stackAbove == null)
                this.actor.lower_bottom();
            else
                this.actor.raise(this._stackAbove);
        }


        this.emit('drag-end');
    }
};
Signals.addSignalMethods(WindowClone.prototype);
/** Emitted when the user starts dragging the window clone.
 * @name drag-begin
 * @event
 * @memberof WindowClone
 * @see WindowClone#_onDragBegin
 */
/** Emitted when the user stops dragging the window clone.
 * @see WindowClone#_onDragEnd
 * @name drag-end
 * @event
 * @memberof WindowClone
 */
/** Emitted when the user clicks on a window clone.
 * The {@link WorkspaceThumbnail} listens to this and activates the window.
 * @name selected
 * @param {WorkspaceThumbnail.WindowClone} clone - the window clone that emitted the
 * signal
 * @param {number} time - the time of the selection.
 * @memberof WindowClone
 * @event
 * @see WorkspaceThumbnail#_activate
 */

/** The state of a {@link WorkspaceThumbnail}.
 *
 * The first time the thumbnails box is created, all the thumbnails get state
 * `NORMAL`.
 *
 * Any *additional* thumbnails added *after* the box is already visible get
 * state `NEW` and are then "slidden in" to their position, during which they
 * have state `ANIMATING_IN` and after which they get state `NORMAL`.
 *
 * When a workspace is removed its thumbnail is marked with state `REMOVING`.
 * It is then slidden out of the sidebar during which it has state `ANIMATING_OUT`,
 * and once it has slidden out it gets state `ANIMATED_OUT`. It is then
 * collapsed during which it gets state `COLLAPSING` and after which it is
 * destroyed and gets state `DESTROYED`.
 *
 * The function that takes care of all the animations/state changing is
 * {@link WorkspaceThumbnail#_updateStates}.
 *
 * @see WorkspaceThumbnail#collapseFraction
 * @see WorkspaceThumbnail#slidePosition
 * @see ThumbnailsBox#_updateStates
 * */
const ThumbnailState = {
    /** Whether this thumbnail is newly added to an already-existing
     * {@link ThumbnailsBox}, i.e. this thumbnail is the result of a 'workspace-added'
     * signal. */
    NEW   :         0,
    /** This thumbnail is currently being slidden out to its position. */
    ANIMATING_IN :  1,
    /** Normal state, nothing's happening. */
    NORMAL:         2,
    /** This thumbnail has been marked for removal and will shortly be animated out. */
    REMOVING :      3,
    /** This thumbnail is currently being slidden out to the side. */
    ANIMATING_OUT : 4,
    /** This thumbnail has just finished sliding out to the side (will shortly
     * be collapsed) */
    ANIMATED_OUT :  5,
    /** This thumbnail is being collapsed, upon which it will be destroyed. */
    COLLAPSING :    6,
    /** This thumbnail has been destroyed */
    DESTROYED :     7
};

/** Create a new WorkspaceThumbnail
 *
 * We store the underlying workspace in {@link #metaWorkspace}.
 *
 * We also store the index of the primary monitor in {@link #monitorIndex}.
 * I guess this is a bit like how a {@link Workspace.Workspace} can display
 * windows from a particular workspace and monitor, but we only display from the
 * primary monitor and don't give the user a choice.
 *
 * We create our top-level actor {@link #actor}, a St.Group
 * with style class 'workspace-thumbnail'.
 *
 * Inside that we put a Clutter.Group {@link #_contents} which
 * is what *actually* contains all the {@link WindowClone}s, as well as a
 * background actor (it's basically a clone of your desktop) to use as the
 * background.
 *
 * We connect up {@link #actor}'s button press event to
 * catch them all but do nothing, and the button release event to call
 * {@link #_activate}.
 *
 * We call {@link #setPorthole} to make the workspace thumbnail
 * look at the region matching the primary monitor.
 *
 * Then we create {@link WindowClone}s for each *non-minimized* window on the workspace/monitor
 * with {@link #_addWindowClone}.
 *
 * We also connect to each window on our workspace's 'notify::minimized' signal
 * so that if a window is minimized we remove its clone, and if it is not minimized
 * we add a clone for it. This is to save a little bit of effort - no point making
 * classes for windows that can't be seen!
 *
 * @param {?Meta.Workspace} metaWorkspace - the underlying `Meta.Workspace` that
 * we are going to create a thumbnail for
 * @classdesc
 * ![WorkspaceThumbnail outlined in red, displaying a few {@link WindowClone}s.](pics/WorkspaceThumbnail.png)
 *
 * A Workspace thumbnail is a thumbnail of a workspace...
 *
 * In particular, it's the one that appears in the workspace thumbnails sidebar
 * to the right on the overview in the "windows" tab.
 *
 * It has many {@link WindowClone}s, one per non-minimized window (no point making
 * clones for minimized windows as we can't see these anyway; if the window
 * is unminimized *then* we will create a clone for it).
 *
 * For the moment each thumbnail will *only* show a snapshot of the
 * primary monitor.
 *
 * (The parallel class for the windows display in the middle of the overview
 * is the {@link Workspace.Workspace} which displays {@link Workspace.WindowClone}s. We're pretty similar to that in terms of window management and
 * drag and drop, but we don't have any of the farting around that {@link Workspace.Workspace}
 * does to position all the windows.).
 *
 *
 * @class
 */
function WorkspaceThumbnail(metaWorkspace) {
    this._init(metaWorkspace);
}

WorkspaceThumbnail.prototype = {
    _init : function(metaWorkspace) {
        /** The underlying workspace for this {@link WorkspaceThumbnail}.
         * @type {?Meta.Workspace} */
        this.metaWorkspace = metaWorkspace;
        /** The monitor that this workspace will display window thumbnails for.
         * For now this is hard-coded to the primary monitor's index.
         * @type {number} */
        this.monitorIndex = Main.layoutManager.primaryIndex;

        this._removed = false;

        /** The top-level actor for the workspace thumbnai.
         * Style class 'workspace-thumbnail'.
         * @type {St.Group} */
        this.actor = new St.Group({ reactive: true,
                                    clip_to_allocation: true,
                                    style_class: 'workspace-thumbnail' });
        this.actor._delegate = this;

        /** The actor that actually contains all the {@link WindowClone}s the
         * background actor. Parented in {@link #actor}.
         * @type {Clutter.Group} */
        this._contents = new Clutter.Group();
        this.actor.add_actor(this._contents);

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));
        this.actor.connect('button-press-event', Lang.bind(this,
            function(actor, event) {
                return true;
            }));
        this.actor.connect('button-release-event', Lang.bind(this,
            function(actor, event) {
                this._activate();
                return true;
            }));

        /** A clone of the desktop wallpaper to use as the background for the
         * thumbnail.
         * @type {Meta.BackgroundActor} */
        this._background = Meta.BackgroundActor.new_for_screen(global.screen);
        this._contents.add_actor(this._background);

        let monitor = Main.layoutManager.primaryMonitor;
        this.setPorthole(monitor.x, monitor.y, monitor.width, monitor.height);

        let windows = global.get_window_actors().filter(this._isWorkspaceWindow, this);

        // Create clones for windows that should be visible in the Overview
        /** This holds the {@link WindowClone}s for each of the windows on
         * this workspace/monitor.
         * @see #_addWindowClone
         * @type {WorkspaceThumbnail.WindowClone[]} */
        this._windows = [];
        this._allWindows = [];
        this._minimizedChangedIds = [];
        for (let i = 0; i < windows.length; i++) {
            let minimizedChangedId =
                windows[i].meta_window.connect('notify::minimized',
                                               Lang.bind(this,
                                                         this._updateMinimized));
            this._allWindows.push(windows[i].meta_window);
            this._minimizedChangedIds.push(minimizedChangedId);

            if (this._isMyWindow(windows[i]) && this._isOverviewWindow(windows[i])) {
                this._addWindowClone(windows[i]);
            }
        }

        // Track window changes
        this._windowAddedId = this.metaWorkspace.connect('window-added',
                                                          Lang.bind(this, this._windowAdded));
        this._windowRemovedId = this.metaWorkspace.connect('window-removed',
                                                           Lang.bind(this, this._windowRemoved));
        this._windowEnteredMonitorId = global.screen.connect('window-entered-monitor',
                                                           Lang.bind(this, this._windowEnteredMonitor));
        this._windowLeftMonitorId = global.screen.connect('window-left-monitor',
                                                           Lang.bind(this, this._windowLeftMonitor));

        /** The current state of the workspace thumbnail.
         * @type {WorkspaceThumbnail.ThumbnailState} */
        this.state = ThumbnailState.NORMAL;
        this._slidePosition = 0; // Fully slid in
        this._collapseFraction = 0; // Not collapsed
    },

    /** Sets the "porthole" to the desktop that this workspace thumbnail will
     * display. Usually the primary monitor dimension/position.
     * @param {number} x - x position of the porthole
     * @param {number} y - y position of the porthole
     * @param {number} width - width of the porthole
     * @param {number} height - height of the porthole
     */
    setPorthole: function(x, y, width, height) {
        this._portholeX = x;
        this._portholeY = y;
        this.actor.set_size(width, height);
        this._contents.set_position(-x, -y);
    },

    /** Looks up the index of a meta window in our internal list
     * {@link #_windows}.
     * @param {Meta.Window} metaWindow - a metacity window to look up
     * @returns {number} the index of the clone for that window in {@link #_windows},
     * or -1 if not found.
     */
    _lookupIndex: function (metaWindow) {
        for (let i = 0; i < this._windows.length; i++) {
            if (this._windows[i].metaWindow == metaWindow) {
                return i;
            }
        }
        return -1;
    },

    /** Synchronises the {@link WindowClone}s so that their stacking order
     * represents the stacking order of their Meta.Windows.
     * 
     * @inheritparams Workspace.Workspace#syncStacking
     * @see WorkspacesView.WorkspacesDisplay#_onRestacked
     * @see WorkspacesView.WorkspacesView#syncStacking
     * @see WindowClone#setStackAbove
     */
    syncStacking: function(stackIndices) {
        this._windows.sort(function (a, b) { return stackIndices[a.metaWindow.get_stable_sequence()] - stackIndices[b.metaWindow.get_stable_sequence()]; });

        for (let i = 0; i < this._windows.length; i++) {
            let clone = this._windows[i];
            let metaWindow = clone.metaWindow;
            if (i == 0) {
                clone.setStackAbove(this._background);
            } else {
                let previousClone = this._windows[i - 1];
                clone.setStackAbove(previousClone.actor);
            }
        }
    },

    /** The current slide position of the thumbnail.
     * 
     * When a thumbnail has slide position 0, that is its position within
     * the fully-expanded thumnbnails slidebar.
     *
     * Slide position 0.5 means the workspace has slidden half of its width
     * to the right.
     *
     * So imagine the slidePosition as the workspace thumbnail "sliding" to the
     * right from its original position (with the workspace thumbnail sidebar
     * fully expandin) until it leaves the screen (slide position 1 since
     * the workspaces sidebar is just on column across).
     *
     * This is used to animate the "sliding" out and in of the {@link ThumbnailsBox}
     * when it is collapsed.
     *
     * See {@link ThumbnailsBox#_updateStates}
     * which is in charge of doing all the animation.
     * @see #collapseFraction
     * @type {number} */
    set slidePosition(slidePosition) {
        this._slidePosition = slidePosition;
        this.actor.queue_relayout();
    },

    get slidePosition() {
        return this._slidePosition;
    },

    /** The current collapse fraction of the thumbnail.
     *
     * Imagine that the clip box for the thumbnail is slowly shrinking width-wise
     * from the right edge to the left. The fraction of the thumbnail's width
     * that has been clipped is its collapse fraction.
     *
     * Thumbnails that have been slidden out of the thumbnails sidebar (because
     * that thumbnail has been removed) are then collapsed, see {@link ThumbnailsBox#_updateStates}
     * which is in charge of doing all the animation.
     * @type {number}
     */
    set collapseFraction(collapseFraction) {
        this._collapseFraction = collapseFraction;
        this.actor.queue_relayout();
    },

    get collapseFraction() {
        return this._collapseFraction;
    },

    /** Potentially removes one of our window clones from us.
     *
     * If we currently have a {@link WindowClone} for this window and the window
     * *fails* the {@link #_isMyWindow} and
     * {@link #_isOverviewWindow} checks, we remove it from
     * {@link #_windows} and destroy it.
     * @param {Meta.Window} metaWin - the metacity window
     */
    _doRemoveWindow : function(metaWin) {
        let win = metaWin.get_compositor_private();

        // find the position of the window in our list
        let index = this._lookupIndex (metaWin);

        if (index == -1)
            return;

        // Check if window still should be here
        if (win && this._isMyWindow(win) && this._isOverviewWindow(win))
            return;

        let clone = this._windows[index];
        this._windows.splice(index, 1);

        clone.destroy();
    },

    /** Potentially adds a {@link WindowClone} for a new window to our thumbnail.
     *
     * If we don't currently have a {@link WindowClone} for this window and the window
     * *passes* the {@link #_isMyWindow} and
     * {@link #_isOverviewWindow} checks, we call
     * {@link #_addWindowClone} to add it.
     * @inheritparams #_doRemoveWindow
     * @see #_addWindowClone
     */
    _doAddWindow : function(metaWin) {
        if (this._removed)
            return;

        let win = metaWin.get_compositor_private();

        if (!win) {
            // Newly-created windows are added to a workspace before
            // the compositor finds out about them...
            Mainloop.idle_add(Lang.bind(this,
                                        function () {
                                            if (!this._removed &&
                                                metaWin.get_compositor_private() &&
                                                metaWin.get_workspace() == this.metaWorkspace)
                                                this._doAddWindow(metaWin);
                                            return false;
                                        }));
            return;
        }

        if (this._allWindows.indexOf(metaWin) == -1) {
            let minimizedChangedId = metaWin.connect('notify::minimized',
                                                     Lang.bind(this,
                                                               this._updateMinimized));
            this._allWindows.push(metaWin);
            this._minimizedChangedIds.push(minimizedChangedId);
        }

        // We might have the window in our list already if it was on all workspaces and
        // now was moved to this workspace
        if (this._lookupIndex (metaWin) != -1)
            return;

        if (!this._isMyWindow(win) || !this._isOverviewWindow(win))
            return;

        let clone = this._addWindowClone(win);
    },

    /** Callback for the 'window-added' signal of {@link #metaWorkspace}.
     *
     * Calls {@link #_doAddWindow}
     * @see #_doAddWindow
     */
    _windowAdded : function(metaWorkspace, metaWin) {
        this._doAddWindow(metaWin);
    },

    /** Callback for the 'window-removed' signal of {@link #metaWorkspace}.
     *
     * Calls {@link #_doRemoveWindow}
     * @see #_doRemoveWindow
     */
    _windowRemoved : function(metaWorkspace, metaWin) {
        let index = this._allWindows.indexOf(metaWin);
        if (index != -1) {
            metaWin.disconnect(this._minimizedChangedIds[index]);
            this._allWindows.splice(index, 1);
            this._minimizedChangedIds.splice(index, 1);
        }

        this._doRemoveWindow(metaWin);
    },

    /** Callback for the 'window-entered-monitor' signal of
     * the screen at index {@link #monitorIndex}.
     *
     * Calls {@link #_doAddWindow} (if it's our monitor).
     * @see #_doAddWindow
     */
    _windowEnteredMonitor : function(metaScreen, monitorIndex, metaWin) {
        if (monitorIndex == this.monitorIndex) {
            this._doAddWindow(metaWin);
        }
    },

    /** Callback for the 'window-left-monitor' signal of
     * the screen at index {@link #monitorIndex}.
     *
     * Calls {@link #_doRemoveWindow} (if it has left our monitor)
     * @see #_doRemoveWindow
     */
    _windowLeftMonitor : function(metaScreen, monitorIndex, metaWin) {
        if (monitorIndex == this.monitorIndex) {
            this._doRemoveWindow(metaWin);
        }
    },

    /** Callback when a window's "minimized" state changes (i.e. it's either
     * minimized or unminimized).
     *
     * If it's been minimized, we remove its window clone thumbnail. If it's been
     * unminimized, we add its window clone to the thumbnail.
     * @see #_doRemoveWindow
     * @see #_doAddWindow
     */
    _updateMinimized: function(metaWin) {
        if (metaWin.minimized)
            this._doRemoveWindow(metaWin);
        else
            this._doAddWindow(metaWin);
    },

    /** Destroys this workspace thumbnail by destroying {@link #actor},
     * triggering {@link #_onDestroy}.
     */
    destroy : function() {
        this.actor.destroy();
    },

    /** Disconnects all the signals we were listening to (workspace, screen, and
     * windows being minimized), but does not destroy anything.
     */
    workspaceRemoved : function() {
        if (this._removed)
            return;

        this._removed = true;

        this.metaWorkspace.disconnect(this._windowAddedId);
        this.metaWorkspace.disconnect(this._windowRemovedId);
        global.screen.disconnect(this._windowEnteredMonitorId);
        global.screen.disconnect(this._windowLeftMonitorId);

        for (let i = 0; i < this._allWindows.length; i++)
            this._allWindows[i].disconnect(this._minimizedChangedIds[i]);
    },

    /** Callback when {@link #actor} is destroyed, for
     * example as a result of {@link #destroy}.
     *
     * Calls {@link #workspaceRemoved} and resets our list
     * of window clones {@link #_windows} to [] and sets
     * our actor {@link #actor} to null.
     */
    _onDestroy: function(actor) {
        this.workspaceRemoved();

        this._windows = [];
        this.actor = null;
    },

    /** Tests if `win` belongs to this workspace
     * @param {Meta.WindowActor} win - window actor to test.
     * @returns {boolean} true if `win` passes th test.
     */
    _isWorkspaceWindow : function (win) {
        return Main.isWindowActorDisplayedOnWorkspace(win, this.metaWorkspace.index());
    },
     
    // Tests if `win` belongs to this workspaces and monitor.
    /** @inheritdoc Workspace.Workspace#_isMyWindow */
    _isMyWindow : function (win) {
        return this._isWorkspaceWindow(win) &&
            (!win.get_meta_window() || win.get_meta_window().get_monitor() == this.monitorIndex);
    },

    // Tests if @win should be shown in the Overview
    /** @inheritdoc Workspace.Workspace#_isOverview */
    _isOverviewWindow : function (win) {
        let tracker = Shell.WindowTracker.get_default();
        return tracker.is_window_interesting(win.get_meta_window()) &&
               win.get_meta_window().showing_on_its_workspace();
    },

    /** Create a clone of a (non-desktop) window and add it to the window list
     *
     * We create a {@link WindowClone} from `win` and connect to multiple of
     * its signals:
     *
     * * ['selected' signal]{@link WindowClone.selected} - call
     * {@link #_activate}
     * * clone's ['drag-begin' signal]{@link WindowClone.drag-begin} - call
     * [Main.overview.beginWindowDrag]{@link Overview.Overview#beginWindowDrag}.
     * * clone's ['drag-end' signal]{@link WindowClone.drag-end} - call
     * [Main.overview.endWindowDrag]{@link Overview.Overview#endWindowDrag}.
     * @param {Meta.WindowActor} win - window actor to create a clone for.
     *
     * We add the clone to {@link #_window}.
     * @returns {WorkspaceThumbnail.WindowClone} the created clone.
     */
    _addWindowClone : function(win) {
        let clone = new WindowClone(win);

        clone.connect('selected',
                      Lang.bind(this, this._activate));
        clone.connect('drag-begin',
                      Lang.bind(this, function(clone) {
                          Main.overview.beginWindowDrag();
                      }));
        clone.connect('drag-end',
                      Lang.bind(this, function(clone) {
                          Main.overview.endWindowDrag();
                      }));
        this._contents.add_actor(clone.actor);

        if (this._windows.length == 0)
            clone.setStackAbove(this._background);
        else
            clone.setStackAbove(this._windows[this._windows.length - 1].actor);

        this._windows.push(clone);

        return clone;
    },

    /** Callback for the ['selected']{@link WindowClone.selected} signal of
     * a window clone (i.e. when one is clicked) and also when this workspace
     * thumbnail is clicked.
     *
     * The workspace that this thumbnail is for is activated.
     */
    _activate : function (clone, time) {
        if (this.state > ThumbnailState.NORMAL)
            return;

        // a click on the already current workspace should go back to the main view
        if (this.metaWorkspace == global.screen.get_active_workspace())
            Main.overview.hide();
        else
            this.metaWorkspace.activate(time);
    },

    // Draggable target interface
    /**
    /** Implements a the handleDragOver function which sets this object up
     * as a potential drag target.
     *
     * If the draggable is from an Xdnd drag, we simply activate the workspace
     * and return {@link DND.DragMotionResult.CONTINUE} so the user can continue
     * their Xdnd on that workspace.
     *
     * If this workspace thumbnail is currently animating out or destroyed, we
     * return {@link DND.DragMotionResult.CONTINUE} (i.e. won't accept a drop if
     * the thumbnail is destroyed or being destroyed).
     *
     * If the draggable is a {@link WindowClone} or {@link Workspace.WindowClone}
     * and we don't currently manage this window,
     * we return {@link DND.DragMotionResult.MOVE_DROP} indicating that the user
     * can drop their window here to move it from wherever it was to this workspace.
     * 
     * If th draggable has a `shellWorkspaceLaunch` property (function) like
     * {@link SearchDisplay.SearchResult} or {@link AppDisplay.AppWellIcon},
     * we return {@link DND.DragMotionResult.COPY_DROP} indicating that if dropped
     * here, an instance of that draggable will be launched on this workspace.
     *
     * If nothing else applies we return {@link DND.DragMotionResult.CONTINUE}.
     *
     * @returns {DND.DragMotionResult} `MOVE_DROP` if the draggable is a
     * {@link Workspace.WindowClone} or {@link Window Clone};
     * `COPY_DROP` if it has a `shellWorkspaceLaunch` function;
     * `CONTINUE` otherwise (see notes above).
     * @todo mark as part of the Draggable interface.
     */
    handleDragOver : function(source, actor, x, y, time) {
        if (source == Main.xdndHandler) {
            this.metaWorkspace.activate(time);
            return DND.DragMotionResult.CONTINUE;
        }

        if (this.state > ThumbnailState.NORMAL)
            return DND.DragMotionResult.CONTINUE;

        if (source.realWindow && !this._isMyWindow(source.realWindow))
            return DND.DragMotionResult.MOVE_DROP;
        if (source.shellWorkspaceLaunch)
            return DND.DragMotionResult.COPY_DROP;

        return DND.DragMotionResult.CONTINUE;
    },

    /** Implements the 'acceptDrop' function (for {@link Workspace#_dropRect})
     * indicating that this {@link Workspace} can handle drops from drag and drop.
     *
     * If this thumbnail is currently being destroyed or is destroyed we don't
     * accept the drop (return `false`).
     *
     * If the item being dropped is a {@link Window.Clone} or {@link Workspace.WindowClone} but we already have it
     * in our {@link WorkspaceThumbnail}, we do nothing (return `false`).
     *
     * If it is a Window Clone and is *not* in our thumbnail,
     * we move its window from its old monitor/workspace to ours, which will
     * trigger a 'window-added' event which will add the window clone.
     *
     * If instead the draggable has a `shellWorkspaceLaunch` function
     * (e.g. {@link SearchDisplay.SearchResult}s and
     * {@link AppDisplay.AppWellIcon}s do), we call this function with
     * {@link #metaWorkspace}'s index (or -1 if we have no workspace)
     * and the timestamp of `time`. This typically launches the search
     * result (or the application if it is an AppWellIcon) on that workspace.
     *
     * @see AppDisplay.AppWellIcon#shellWorkspaceLaunch
     * @see SearchDisplay.SearchResult#shellWorkspaceLaunch
     * 
     * @todo link this to Draggable and inherit params
     */
    acceptDrop : function(source, actor, x, y, time) {
        if (this.state > ThumbnailState.NORMAL)
            return false;

        if (source.realWindow) {
            let win = source.realWindow;
            if (this._isMyWindow(win))
                return false;

            let metaWindow = win.get_meta_window();

            // We need to move the window before changing the workspace, because
            // the move itself could cause a workspace change if the window enters
            // the primary monitor
            if (metaWindow.get_monitor() != this.monitorIndex)
                metaWindow.move_to_monitor(this.monitorIndex);

            metaWindow.change_workspace_by_index(this.metaWorkspace.index(),
                                                 false, // don't create workspace
                                                 time);
            return true;
        } else if (source.shellWorkspaceLaunch) {
            source.shellWorkspaceLaunch({ workspace: this.metaWorkspace ? this.metaWorkspace.index() : -1,
                                          timestamp: time });
            return true;
        }

        return false;
    }
};

Signals.addSignalMethods(WorkspaceThumbnail.prototype);


/** creates a new ThumbnailsBox
 * We create the top-level actor {@link #actor} being a
 * Shell.GenericContainer so we can allocate each workspace its appropraite size.
 *
 * We also create the workspace thumbnail indicator being that white border
 * around the selected workspace.
 *
 * Note that workspace thumbnails are created every time {@link #show}
 * is called and destroyed whenever {@link Thumbnailsbox#hide} is called rather
 * than in the constructor.
 *
 * @classdesc
 * A ThumbnailsBox is the sidebar on the right of the overview in the "windows"
 * tab that shows a little thumbnail of each workspace the user has.
 *
 * ![{@link ThumbnailsBox} (yellow), {@link WorkspaceThumbnail} containing {@link WindowClone}s (green)](pics/workspaceThumbnails.png)
 *
 * A ThumbnailsBox has many {@link Workspace}s which in turn has many {@link WindowClone}s.
 *
 * We manage animations for workspaces that are added or removed, as well as
 * supporting the dragging of a window clone over a space *between* adjacent
 * workspaces in order to drop the window on a *new* workspace there.
 *
 * Note that workspace thumbnails are created every time {@link #show}
 * is called and destroyed whenever {@link Thumbnailsbox#hide} is called rather
 * than in the constructor.
 *
 * (Compare to the main part of the windows tab of the overview, which
 * is a {@link WorkspacesView.WorkspacesView} which has many {@link Workspace.Workspace}s which has
 * many {@link Workspace.WindowClon}s. A {@link WorkspacesView.WorkspacesDisplay
 * contains one {@link WorkspacesView} and one {@link ThumbnailsBox}.)
 * @class
 */
function ThumbnailsBox() {
    this._init();
}

ThumbnailsBox.prototype = {
    _init: function() {
        /** Top-level actor is a Shell.GenericContainer, we allocate all the
         * child {@link WorkspaceThumbnail}s their appropriate size in
         * {@link #_allocate}. Style class 'workspace-thumbnails'.
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer({ style_class: 'workspace-thumbnails',
                                                  request_mode: Clutter.RequestMode.WIDTH_FOR_HEIGHT });
        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));

        /** The background of the ThumbnailsBox. Style class
         * 'workspace-thumbnail-background'.
         *
         * When we animate the scale, we don't animate the requested size of the thumbnails, rather
         * we ask for our final size and then animate within that size. This slightly simplifies the
         * interaction with the main workspace windows (instead of constantly reallocating them
         * to a new size, they get a new size once, then use the standard window animation code
         * allocate the windows to their new positions), however it causes problems for drawing
         * the background and border wrapped around the thumbnail as we animate - we can't just pack
         * the container into a box and set style properties on the box since that box would wrap
         * around the final size not the animating size. So instead we fake the background with
         * an actor underneath the content and adjust the allocation of our children to leave space
         * for the border and padding of the background actor.
         * @type {St.Bin}
         */
        this._background = new St.Bin({ style_class: 'workspace-thumbnails-background' });

        this.actor.add_actor(this._background);

        let indicator = new St.Bin({ style_class: 'workspace-thumbnail-indicator' });

        // We don't want the indicator to affect drag-and-drop
        Shell.util_set_hidden_from_pick(indicator, true);

        /** The white border that appears around the {@link WorkspaceThumbnail}
         * that you are currently looking at. St.Bin with style class
         * 'workspace-thumbnail-indicator'.
         * @type {St.Bin} */
        this._indicator = indicator;
        this.actor.add_actor(indicator);

        this._targetScale = 0;
        this._scale = 0;
        this._pendingScaleUpdate = false;
        this._stateUpdateQueued = false;
        this._animatingIndicator = false;
        this._indicatorY = 0; // only used when _animatingIndicator is true

        this._stateCounts = {};
        for (let key in ThumbnailState)
            this._stateCounts[ThumbnailState[key]] = 0;

        this._thumbnails = [];
    },

    /** Shows the thumbnails box, creating a {@link WorkspaceThumbnail} for each
     * workspace with {@link #addThumbnail}.
     *
     * (Note - this is different to "sliding" the box out if it is collapsed
     * in to the side).
     */
    show: function() {
        this._switchWorkspaceNotifyId =
            global.window_manager.connect('switch-workspace',
                                          Lang.bind(this, this._activeWorkspaceChanged));

        this._targetScale = 0;
        this._scale = 0;
        this._pendingScaleUpdate = false;
        this._stateUpdateQueued = false;

        this._stateCounts = {};
        for (let key in ThumbnailState)
            this._stateCounts[ThumbnailState[key]] = 0;

        // The "porthole" is the portion of the screen that we show in the workspaces
        let panelHeight = Main.panel.actor.height;
        let monitor = Main.layoutManager.primaryMonitor;
        this._porthole = {
            x: monitor.x,
            y: monitor.y + panelHeight,
            width: monitor.width,
            height: monitor.height - panelHeight
        };

        this.addThumbnails(0, global.screen.n_workspaces);
    },

    /** Hides the thumbnails box and destroys all the thumbnails.
     *
     * (Note - this is different to "sliding" the box in if it is fully expanded).
     */
    hide: function() {
        if (this._switchWorkspaceNotifyId > 0) {
            global.window_manager.disconnect(this._switchWorkspaceNotifyId);
            this._switchWorkspaceNotifyId = 0;
        }

        for (let w = 0; w < this._thumbnails.length; w++)
            this._thumbnails[w].destroy();
        this._thumbnails = [];
    },

    /** Adds thumbnails to the thumbnails box.
     *
     * This creates `count` {@link WorkspaceThumbnail}s for workspace indices
     * `start`, `start + 1`, ... `start + count - 1` and adds them to
     * {@link #actor}.
     *
     * We also make sure that the thumbnails indicator is raised to the top
     * so that it can be seen.
     * @param {number} start - workspace number to start at
     * @param {number} count - number of workspaces.
     */
    addThumbnails: function(start, count) {
        for (let k = start; k < start + count; k++) {
            let metaWorkspace = global.screen.get_workspace_by_index(k);
            let thumbnail = new WorkspaceThumbnail(metaWorkspace);
            thumbnail.setPorthole(this._porthole.x, this._porthole.y,
                                  this._porthole.width, this._porthole.height);
            this._thumbnails.push(thumbnail);
            this.actor.add_actor(thumbnail.actor);

            if (start > 0) { // not the initial fill
                thumbnail.state = ThumbnailState.NEW;
                thumbnail.slidePosition = 1; // start slid out
                this._haveNewThumbnails = true;
            } else {
                thumbnail.state = ThumbnailState.NORMAL;
            }

            this._stateCounts[thumbnail.state]++;
        }

        this._queueUpdateStates();

        // The thumbnails indicator actually needs to be on top of the thumbnails
        this._indicator.raise_top();
    },

    /** Removes `count` workspace thumbnails, starting at workspace `start`.
     *
     * This calls {@link WorkspaceThumbnail#workspaceRemoved} on the next
     * `count` workspaces from (and including) `start`, and calls
     * {@link #_setThumbnailState} to {@link ThumbnailState.REMOVING}
     * to mark each thumbnail as in the state of removing itself.
     * @inheritparams #addThumbnails
     */
    removeThumbmails: function(start, count) {
        let currentPos = 0;
        for (let k = 0; k < this._thumbnails.length; k++) {
            let thumbnail = this._thumbnails[k];

            if (thumbnail.state > ThumbnailState.NORMAL)
                continue;

            if (currentPos >= start && currentPos < start + count) {
                thumbnail.workspaceRemoved();
                this._setThumbnailState(thumbnail, ThumbnailState.REMOVING);
            }

            currentPos++;
        }

        this._queueUpdateStates();
    },

    /** Makes sure each {@link WorkspaceThumbnail}'s {@link WindowClone}s
     * have the right stacking order (matching the actual stacking order).
     * @inheritparams WorkspaceThumbnail#syncStacking
     * @see WorkspaceThumbnail#syncStacking
     */
    syncStacking: function(stackIndices) {
        for (let i = 0; i < this._thumbnails.length; i++)
            this._thumbnails[i].syncStacking(stackIndices);
    },

    /** This is the scale of each workspace thumbnail relative to the size
     * of the primary monitor.
     *
     * The workspace thumbnails are made as large as possible such that
     * they all fit vertically into the available space (including vertical
     * spacing between them), up to a maximum size of
     * `MAX_THUMBNAIL_SCALE * primary monitor size`.
     *
     * Setting this scale property causes all the thumbnails to change size
     * to reflect the scale.
     */
    set scale(scale) {
        this._scale = scale;
        this.actor.queue_relayout();
    },

    get scale() {
        return this._scale;
    },

    /** The Y position of the current workspace indicator
     * {@link #_indicator}.
     * Setting this causes its position to be updated (it is used to
     * animate the indicator moving around).
     * @type {number}
     */
    set indicatorY(indicatorY) {
        this._indicatorY = indicatorY;
        this.actor.queue_relayout();
    },

    get indicatorY() {
        return this._indicatorY;
    },

    /** Sets the state of a thumbnail (we keep track of how many thumbnails
     * we have in each state).
     * @param {WorkspaceThumbnail.WorkspaceThumbnail} thumbnail - thumbnail to
     * update the state of
     * @param {WorkspaceThumbnail.ThumbnailState} state - state to set for the
     * thumbnail.
     * @see ThumbnailState
     * @see WorkspaceThumbnail#state
     */
    _setThumbnailState: function(thumbnail, state) {
        this._stateCounts[thumbnail.state]--;
        thumbnail.state = state;
        this._stateCounts[thumbnail.state]++;
    },

    /** Applies a callback to all thumbnails with a particular state (for example
     * slide out all thumbnails that are in the `REMOVING` state).
     * 
     * Used in {@link #_updateStates}, for example.
     * @param {WorkspaceThumbnail.ThumbnailState} state - state to get thumbnails
     * for
     * @param {function(WorkspaceThumbnail)} callback - callback to apply to
     * each thumbnail with state `state`.
     */
    _iterateStateThumbnails: function(state, callback) {
        if (this._stateCounts[state] == 0)
            return;

        for (let i = 0; i < this._thumbnails.length; i++) {
            if (this._thumbnails[i].state == state)
                callback.call(this, this._thumbnails[i]);
        }
    },

    /** Tweens our scale {@link #scale} until we hit
     * {@link #_targetScale}.
     *
     * This is used when we have to adjust the scale of all of our thumbnails,
     * for example if a new thumbnail is added and we have to shrink to make
     * space for it, or a thumbnail is removed so we can grow to take up the
     * remaining space.
     */
    _tweenScale: function() {
        Tweener.addTween(this,
                         { scale: this._targetScale,
                           time: RESCALE_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._queueUpdateStates,
                           onCompleteScope: this });
    },

    /** Updates all the thumbnails according to their states and then updates
     * their states:
     *
     * * For any thumbnail that has {@link ThumbnailState.REMOVING}, we set
     * its state to {@link ThumbnailState.ANIMATING_OUT} and slide it out
     * to the right over {@link SLIDE_ANIMATION_TIME}.
     * Upon completion we set its state to {@link ThumbailState.ANIMATED_OUT}.
     * * For any thumbnail that has state {@link ANIMATED_OUT} (not including
     * those ones we just set), we set the thumbnail state to
     * {@link ThumbnailState.COLLAPSING} and collapse the thumbnail until it
     * disappears over time {@link RESCALE_ANIMATION_TIME} (see
     * {@link WorkspaceThumbnail#collapseFraction}). When that's complete
     * we set its state to {@link ThumbnailState.DESTROYED} and then destroy
     * it with {@link WorkspaceThumbnail#destroy}.
     * * If there's any pending scale updates we do that.
     * * If that's all done, we slide *in* any thumbnails with state
     * {@link ThumbnailState.NEW}, setting their state to
     * {@link ThumbnailState.ANIMATING_IN} while they are sliding in, and to
     * {@link ThumbnailState.NORMAL} upon completion.
     *
     * @see WorkspaceThumbnail#slidePosition
     * @see WorkspaceThumbnail#collapseFraction
     */
    _updateStates: function() {
        this._stateUpdateQueued = false;

        // If we are animating the indicator, wait
        if (this._animatingIndicator)
            return;

        // Then slide out any thumbnails that have been destroyed
        this._iterateStateThumbnails(ThumbnailState.REMOVING,
            function(thumbnail) {
                this._setThumbnailState(thumbnail, ThumbnailState.ANIMATING_OUT);

                Tweener.addTween(thumbnail,
                                 { slidePosition: 1,
                                   time: SLIDE_ANIMATION_TIME,
                                   transition: 'linear',
                                   onComplete: function() {
                                       this._setThumbnailState(thumbnail, ThumbnailState.ANIMATED_OUT);
                                       this._queueUpdateStates();
                                   },
                                   onCompleteScope: this
                                 });
            });

        // As long as things are sliding out, don't proceed
        if (this._stateCounts[ThumbnailState.ANIMATING_OUT] > 0)
            return;

        // Once that's complete, we can start scaling to the new size and collapse any removed thumbnails
        this._iterateStateThumbnails(ThumbnailState.ANIMATED_OUT,
            function(thumbnail) {
                this.actor.set_skip_paint(thumbnail.actor, true);
                this._setThumbnailState(thumbnail, ThumbnailState.COLLAPSING);
                Tweener.addTween(thumbnail,
                                 { collapseFraction: 1,
                                   time: RESCALE_ANIMATION_TIME,
                                   transition: 'easeOutQuad',
                                   onComplete: function() {
                                       this._stateCounts[thumbnail.state]--;
                                       thumbnail.state = ThumbnailState.DESTROYED;

                                       let index = this._thumbnails.indexOf(thumbnail);
                                       this._thumbnails.splice(index, 1);
                                       thumbnail.destroy();

                                       this._queueUpdateStates();
                                   },
                                   onCompleteScope: this
                                 });
                });

        if (this._pendingScaleUpdate) {
            this._tweenScale();
            this._pendingScaleUpdate = false;
        }

        // Wait until that's done
        if (this._scale != this._targetScale || this._stateCounts[ThumbnailState.COLLAPSING] > 0)
            return;

        // And then slide in any new thumbnails
        this._iterateStateThumbnails(ThumbnailState.NEW,
            function(thumbnail) {
                this._setThumbnailState(thumbnail, ThumbnailState.ANIMATING_IN);
                Tweener.addTween(thumbnail,
                                 { slidePosition: 0,
                                   time: SLIDE_ANIMATION_TIME,
                                   transition: 'easeOutQuad',
                                   onComplete: function() {
                                       this._setThumbnailState(thumbnail, ThumbnailState.NORMAL);
                                   },
                                   onCompleteScope: this
                                 });
            });
    },

    /** Queues a call to {@link #_updateStates} to do all the
     * animations for the thumbnails.
     * @see #_updateStates
     */
    _queueUpdateStates: function() {
        if (this._stateUpdateQueued)
            return;

        Meta.later_add(Meta.LaterType.BEFORE_REDRAW,
                       Lang.bind(this, this._updateStates));

        this._stateUpdateQueued = true;
    },

    /** Callback for the 'get-preferred-height' signal of the top-level actor
     * {@link #actor}.
     *
     * Workspace thumbnails are made as big as possible such that they all
     * fit vertically in the available space (they have the same aspect ratio
     * as the primary monitor), up to a maximum size {@link MAX_THUMBNAIL_SCALE}.
     *
     * Making sure we incorporate the vertical spacing between workspaces,
     * we work this out and return it.
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        // See comment about this._background in _init()
        let themeNode = this._background.get_theme_node();

        forWidth = themeNode.adjust_for_width(forWidth);

        // Note that for getPreferredWidth/Height we cheat a bit and skip propagating
        // the size request to our children because we know how big they are and know
        // that the actors aren't depending on the virtual functions being called.

        if (this._thumbnails.length == 0)
            return;

        let spacing = this.actor.get_theme_node().get_length('spacing');
        let nWorkspaces = global.screen.n_workspaces;
        let totalSpacing = (nWorkspaces - 1) * spacing;

        [alloc.min_size, alloc.natural_size] =
            themeNode.adjust_preferred_height(totalSpacing,
                                              totalSpacing + nWorkspaces * this._porthole.height * MAX_THUMBNAIL_SCALE);
    },

    /** Callback for the 'get-preferred-width' signal of the top-level actor
     * {@link #actor}.
     *
     * Workspace thumbnails are made as big as possible such that they all
     * fit vertically in the available space (they have the same aspect ratio
     * as the primary monitor), up to a maximum size {@link MAX_THUMBNAIL_SCALE}.
     *
     * We work out what width this is and return it.
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        // See comment about this._background in _init()
        let themeNode = this._background.get_theme_node();

        if (this._thumbnails.length == 0)
            return;

        // We don't animate our preferred width, which is always reported according
        // to the actual number of current workspaces, we just animate within that

        let spacing = this.actor.get_theme_node().get_length('spacing');
        let nWorkspaces = global.screen.n_workspaces;
        let totalSpacing = (nWorkspaces - 1) * spacing;

        let avail = forHeight - totalSpacing;

        let scale = (avail / nWorkspaces) / this._porthole.height;
        scale = Math.min(scale, MAX_THUMBNAIL_SCALE);

        let width = Math.round(this._porthole.width * scale);
        [alloc.min_size, alloc.natural_size] =
            themeNode.adjust_preferred_width(width, width);
    },

    /** Callback for the 'allocate' signal of the top-level actor
     * {@link #actor}.
     *
     * We work out what scale to apply to each workspace thumbnail such that they
     * all fit vertically in the space provided, up to a maximum scale of
     * {@link MAX_THUMBNAIL_SCALE}.
     *
     * Then, being careful to take into account the padding and spacing between
     * each workspace thumbnail, we allocate everthing its position and size.
     *
     * If the thumbnails sidebar is currently collapsed, or in the process
     * of sliding in/out, or if thumbnails are currently being collapsed/slidden
     * in/out, we have to be careful to allocate the appropriate position/scale.
     *
     * We also make sure the current workspace indicator
     * {@link #_indicator} gets placed over the currently-active
     * workspace thumbnail.
     *
     * All in all, there's a lot of things we have to be mindful of ! (thumbnail
     * scale, thumbnail collapse fraction, thumbnail slide fraction, ...)
     */
    _allocate: function(actor, box, flags) {
        let rtl = (St.Widget.get_default_direction () == St.TextDirection.RTL);

        // See comment about this._background in _init()
        let themeNode = this._background.get_theme_node();
        let contentBox = themeNode.get_content_box(box);

        if (this._thumbnails.length == 0) // not visible
            return;

        let portholeWidth = this._porthole.width;
        let portholeHeight = this._porthole.height;
        let spacing = this.actor.get_theme_node().get_length('spacing');

        // Compute the scale we'll need once everything is updated
        let nWorkspaces = global.screen.n_workspaces;
        let totalSpacing = (nWorkspaces - 1) * spacing;
        let avail = (contentBox.y2 - contentBox.y1) - totalSpacing;

        let newScale = (avail / nWorkspaces) / portholeHeight;
        newScale = Math.min(newScale, MAX_THUMBNAIL_SCALE);

        if (newScale != this._targetScale) {
            if (this._targetScale > 0) {
                // We don't do the tween immediately because we need to observe the ordering
                // in queueUpdateStates - if workspaces have been removed we need to slide them
                // out as the first thing.
                this._targetScale = newScale;
                this._pendingScaleUpdate = true;
            } else {
                this._targetScale = this._scale = newScale;
            }

            this._queueUpdateStates();
        }

        let thumbnailHeight = portholeHeight * this._scale;
        let thumbnailWidth = Math.round(portholeWidth * this._scale);
        let roundedHScale = thumbnailWidth / portholeWidth;

        let slideOffset; // X offset when thumbnail is fully slid offscreen
        if (rtl)
            slideOffset = - (thumbnailWidth + themeNode.get_padding(St.Side.LEFT));
        else
            slideOffset = thumbnailWidth + themeNode.get_padding(St.Side.RIGHT);

        let childBox = new Clutter.ActorBox();

        // The background is horizontally restricted to correspond to the current thumbnail size
        // but otherwise covers the entire allocation
        if (rtl) {
            childBox.x1 = box.x1;
            childBox.x2 = box.x2 - ((contentBox.x2 - contentBox.x1) - thumbnailWidth);
        } else {
            childBox.x1 = box.x1 + ((contentBox.x2 - contentBox.x1) - thumbnailWidth);
            childBox.x2 = box.x2;
        }
        childBox.y1 = box.y1;
        childBox.y2 = box.y2;
        this._background.allocate(childBox, flags);

        let indicatorY = this._indicatorY;
        // when not animating, the workspace position overrides this._indicatorY
        let indicatorWorkspace = !this._animatingIndicator ? global.screen.get_active_workspace() : null;

        let y = contentBox.y1;

        for (let i = 0; i < this._thumbnails.length; i++) {
            let thumbnail = this._thumbnails[i];

            if (i > 0)
                y += spacing - Math.round(thumbnail.collapseFraction * spacing);

            // We might end up with thumbnailHeight being something like 99.33
            // pixels. To make this work and not end up with a gap at the bottom,
            // we need some thumbnails to be 99 pixels and some 100 pixels height;
            // we compute an actual scale separately for each thumbnail.
            let y1 = Math.round(y);
            let y2 = Math.round(y + thumbnailHeight);
            let roundedVScale = (y2 - y1) / portholeHeight;

            let x1, x2;
            if (rtl) {
                x1 = contentBox.x1 + slideOffset * thumbnail.slidePosition;
                x2 = x1 + thumbnailWidth;
            } else {
                x1 = contentBox.x2 - thumbnailWidth + slideOffset * thumbnail.slidePosition;
                x2 = x1 + thumbnailWidth;
            }

            if (thumbnail.metaWorkspace == indicatorWorkspace)
                indicatorY = y1;

            // Allocating a scaled actor is funny - x1/y1 correspond to the origin
            // of the actor, but x2/y2 are increased by the *unscaled* size.
            childBox.x1 = x1;
            childBox.x2 = x1 + portholeWidth;
            childBox.y1 = y1;
            childBox.y2 = y1 + portholeHeight;

            thumbnail.actor.set_scale(roundedHScale, roundedVScale);
            thumbnail.actor.allocate(childBox, flags);

            // We round the collapsing portion so that we don't get thumbnails resizing
            // during an animation due to differences in rounded, but leave the uncollapsed
            // portion unrounded so that non-animating we end up with the right total
            y += thumbnailHeight - Math.round(thumbnailHeight * thumbnail.collapseFraction);
        }

        if (rtl) {
            childBox.x1 = contentBox.x1;
            childBox.x2 = contentBox.x1 + thumbnailWidth;
        } else {
            childBox.x1 = contentBox.x2 - thumbnailWidth;
            childBox.x2 = contentBox.x2;
        }
        childBox.y1 = indicatorY;
        childBox.y2 = childBox.y1 + thumbnailHeight;
        this._indicator.allocate(childBox, flags);
    },

    /** Callback when the current workspace is switched.
     * This updates the current-workspace-indicator {@link Workspace#_indicator}'s
     * position to animate sliding it over to the new current workspace.
     */
    _activeWorkspaceChanged: function(wm, from, to, direction) {
        let thumbnail;
        let activeWorkspace = global.screen.get_active_workspace();
        for (let i = 0; i < this._thumbnails.length; i++) {
            if (this._thumbnails[i].metaWorkspace == activeWorkspace) {
                thumbnail = this._thumbnails[i];
                break;
            }
        }

        this._animatingIndicator = true;
        this.indicatorY = this._indicator.allocation.y1;
        Tweener.addTween(this,
                         { indicatorY: thumbnail.actor.allocation.y1,
                           time: WorkspacesView.WORKSPACE_SWITCH_TIME,
                           transition: 'easeOutQuad',
                           onComplete: function() {
                               this._animatingIndicator = false;
                               this._queueUpdateStates();
                           },
                           onCompleteScope: this
                         });
    }
};
