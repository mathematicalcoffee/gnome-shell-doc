// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Classes handling the window previews you see in the 'Windows' tab of the
 * Overview.
 *
 * A {@link Workspace} is in charge of showing the window thumbnails for
 * each workspace (in the Overview).
 *
 * Each window thumbnail is a {@link WindowClone}, and each has a close button
 * and window caption which is a {@link WindowOverlay}.
 *
 * ![[WorkspacesDisplay]{@link WorkspacesView.WorkspacesDisplay} (red) contains both a [ThumbnailsBox]{@link WorkspaceThumbnail.ThumbnailsBox} (right hand sidebar) and a {@link Workspace} for each workspace, containing {@link WindowClone}s (yellow) and a {@link WindowOveray} (green}](pics/workspace_Display-red_Clone-yellow_Overlay-green.png)
 *
 */

const Clutter = imports.gi.Clutter;
const GConf = imports.gi.GConf;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Pango = imports.gi.Pango;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Signals = imports.signals;

const DND = imports.ui.dnd;
const Lightbox = imports.ui.lightbox;
const Main = imports.ui.main;
const Overview = imports.ui.overview;
const Panel = imports.ui.panel;
const Tweener = imports.ui.tweener;

/** @+
 * @const
 * @default */
// UNUSED
const FOCUS_ANIMATION_TIME = 0.15;
/** @+
 * @type {number} */
/** maximum size for a {@link WindowClone}'s drag actor. */
const WINDOW_DND_SIZE = 256;

/** When scrolling over a {@link WindowClone} it zooms bigger or smaller by
 * {@link SCROLL_SCALE_AMOUNT} percent until we reach our original size.
 * @see WindowClone#_onScroll
 * @see WindowClone#_zoomUpdate
 */
const SCROLL_SCALE_AMOUNT = 100 / 5;

/** Time (seconds) it takes for the lightbox over the rest of the Overview
 * to appear when we start zooming into a {@link WindowClone}.
 * @see WindowClone#_zoomSart
 */
const LIGHTBOX_FADE_TIME = 0.1;
/** Time (seconds) it takes for the close button on the top-left of a
 * {@link WindowClone} to fade when the user stops hovering over it (and
 * is not hovering over another WindowClone).
 * @see WindowOverlay#fadeIn */
const CLOSE_BUTTON_FADE_TIME = 0.1;

/** Opacity of the drag actor for a {@link WindowClone}. */
const DRAGGING_WINDOW_OPACITY = 100;
/** @- */

/** GConf key to get the metacity window button layout (minimize/maximize/close/menu keys)
 * from.
 * @type {string} */
const BUTTON_LAYOUT_KEY = '/desktop/gnome/shell/windows/button_layout';

/**
 * This defines the layout scheme used for {@link WindowClone}s in a
 * {@link Workspace} for one to 5 windows.
 *
 * i.e. whereabouts we position the window thumbnails in the workspace.
 *
 * For larger
 * counts we fall back to an algorithm. We need more schemes here
 * unless we have a really good algorithm.
 *
 * The key is the number of windows we have, `numWindows`.
 * The value is an array of `numWindows` numeric tuples, each tuple being
 * a {@link Slot}.
 *
 * That is, each tuple is `[xCenter, yCenter, scale]` where the scales an
 * centres are relative to the width/height of the parent {@link Workspace}.
 *
 * That is, `POSITIONS[numWindows][i]` defines where to position window
 * `i` relative to the parent {@link Workspace}.
 * @see Slot
 * @type {Object.<number, [Slot]{@link Workspace.Slot}>}
 * @see Workspace#_computeWindowSlot
 */
const POSITIONS = {
        1: [[0.5, 0.5, 0.95]],
        2: [[0.25, 0.5, 0.48], [0.75, 0.5, 0.48]],
        3: [[0.25, 0.25, 0.48],  [0.75, 0.25, 0.48],  [0.5, 0.75, 0.48]],
        4: [[0.25, 0.25, 0.47],   [0.75, 0.25, 0.47], [0.75, 0.75, 0.47], [0.25, 0.75, 0.47]],
        5: [[0.165, 0.25, 0.32], [0.495, 0.25, 0.32], [0.825, 0.25, 0.32], [0.25, 0.75, 0.32], [0.75, 0.75, 0.32]]
};

/** Used in {@link Workspace#_orderWindowsByMotionAndStartup},
 * 5! = 120 which is probably the highest we can go.
 *
 * If a {@link Workspace} less than this many windows,
 * {@link Workspace#_orderWindowsByMotionAndStartup} uses the
 * {@link Workspace#_orderWindowsPermutation} algorithm to arrange its windows,
 * ensuring the total distance moved by all windows to arrange themselves thus
 * is minimized.
 *
 * If the {@link Workspace} has *more* than this many windows, that algorithm
 * is too expensive, so we use the cheaper {@link Workspace#_orderWindowsGreedy}
 * method. This may result in more total window movement but is cheaper.
 *
 * @type {number} */
const POSITIONING_PERMUTATIONS_MAX = 5;
/** @- */
/** A tuple defining the position and scale of a {@link WindowClone} within
 * its {@link Workspace}.
 *
 * The elements are `[xCenter, yCenter, scale]` where the x center is a fraction
 * relative to the width of the {@link Workspace} the clone is in, the y
 * center is a fraction relative to its height, and the scale is used to determine
 * the rectangle that the {@link WindowClone} is allowed to occupy (fraction
 * of the parent {@link Workspace}'s size).
 *
 * @see POSITIONS
 * @typedef Slot
 * @type {[number, number, number]}
 */

/** Utility function, interpolates a fraction `step` between `start` an `end`.
 * @param {number} start - the start number
 * @param {number} end - the end number
 * @param {number} step - between 0 and 1, the fraction of the way between
 * `start` and `end` we wish to interpolate.
 * @returns {number} `start + (end - start) * step`.
 */
function _interpolate(start, end, step) {
    return start + (end - start) * step;
}

/** Utility function, clamps `value` to be between `min` and `max`.
 * @param {number} min - the minimum value to clamp to.
 * @param {number} max - the maximum value to clamp to.
 * @param {number} value - the value to clamp.
 * @returns {number} `min` if `value < min`, `max` if `value > max`,
 * and `value` if it is otherwise in between the two.
 */
function _clamp(value, min, max) {
    return Math.max(min, Math.min(max, value));
}

/** Creates a new ScaledPoint.
 *
 * I think this is a bit misleading because the x/y coordinates are entirely
 * separate to the x/y scale - you use {@link #interpPosition} to interpolate
 * between the x/y coordinates of two different points, and
 * {@link #interpScale} to interpolate between the x/y scales of two
 * different points, but the position calculation doesn't make use of the scale and
 * the scale calculation doesn't make use of the position, so in effect the
 * x/y position and x/y scales are entirely independent of each other.
 * @param {number} x - x coordinate of the point (unscaled)
 * @param {number} y - y coordinate of the point (unscaled)
 * @param {number} scaleX - a x scale (is never multiplied by the x coordinate...)
 * @param {number} scaleY - a y scale (is never multiplied by the y coordinate...)
 * @classdesc
 * This is a convenience class, only used for zooming calculates in
 * {@link WindowClone}.
 *
 * The name is a bit misleading (I think), because you give it an unscaled
 * `x` and `y` coordinate, and a scale (that *does not apply to the `x` and
 * `y` coordinates*).
 *
 * There are convenience functions for interpolating positions or
 * scales between two different {@link ScaledPoint}s, but there is **never**
 * any interaction between the scales and the points, they are quite separate!
 *
 * @see WindowClone#_zoomStart
 * @class
 */
function ScaledPoint(x, y, scaleX, scaleY) {
    [this.x, this.y, this.scaleX, this.scaleY] = arguments;
    /** @+
     * @type {number}
     * @memberof ScaledPoint
     */
    /** The x coordinate.
     * @name x */
    /** The y coordinate.
     * @name y */
    /** The x scale.
     * @name scaleX */
    /** The y scale.
     * @name scaleY */
    /** @- */
}

ScaledPoint.prototype = {
    /** Gets the position of the point.
     * @returns {[number, number]} `[{@link #x}, {@link #y}]`.
     */
    getPosition : function() {
        return [this.x, this.y];
    },

    /** Gets the stored x and y scales.
     * @returns {[number, number]} `[{@link #scaleX}, {@link #scaleY}]`.
     */
    getScale : function() {
        return [this.scaleX, this.scaleY];
    },

    /** Sets the position of the point
     * @inheritparams ScaledPoint */
    setPosition : function(x, y) {
        [this.x, this.y] = arguments;
    },

    /** Sets the x/y scales (these are *never* applied to the x/y positions, so think
     * of the scales as entirely separate to/independent of the positions).
     * @inheritparams ScaledPoint */
    setScale : function(scaleX, scaleY) {
        [this.scaleX, this.scaleY] = arguments;
    },

    /** Interpolates between this point's position and another's.
     * Doesn't take the scales into account (they are independent to all position
     * calculations).
     * @param {Workspace.ScaledPoint} other - the other scaled point to
     * interpolate.
     * @inheritparams _interpolate
     * @returns {[number, number]} the interpolated x, y coordinates.
     */
    interpPosition : function(other, step) {
        return [_interpolate(this.x, other.x, step),
                _interpolate(this.y, other.y, step)];
    },

    /** Interpolates between this point's scale and another's.
     * Doesn't take the x/y positions into account (they are independent to
     * all scale calculations).
     * @param {Workspace.ScaledPoint} other - the other scaled point whose scale
     * to interpolate with ours.
     * @inheritparams _interpolate
     * @returns {[number, number]} the interpolated x, y scales.
     */
    interpScale : function(other, step) {
        return [_interpolate(this.scaleX, other.scaleX, step),
                _interpolate(this.scaleY, other.scaleY, step)];
    }
};

/** creates a new WindowClone.
 *
 * We store `realWindow` and its metacity window into {@link #realWindow}
 * and {@link #metaWindow} and set the metacity window's delegate
 * to point back to `this`.
 *
 * Then we create a non-reactive Clutter.Clone which clones the `realWindow`'s
 * texture, i.e. it is a thumbnail of the window ({@link #_windowClone}).
 * There is a bit of messing
 * around because a Meta.WindowActor (`realWindow`) is actually a bit larger
 * than its corresponding Meta.Window (`metaWindow`) because it has some artificial
 * clear padding adding around the outside in order to make it easier to
 * resize; see {@link WindowClon#_getInvisibleBorderPadding}.
 *
 * Then we create the top-level actor {@link #actor} for the window clone being a
 * reactive Clutter.Group (the Clutter.Clone {@link #_windowClone} is
 * not reactive and has this issue of invisible border padding; the
 * actor it is embedded in takes care of this).
 * We embed the window clone {@link #_windowClone} into the actor,
 * taking care to position the clone and size the actor so as not show the
 * invisible border around the Meta.WindowActor.
 *
 * We then connect up to a few of `realWindow`'s signals to make sure
 * our clone updates with the window.
 *
 * Finally we make {@link #actor} draggable (see
 * {@link DND.makeDraggable}).
 *
 * @param {Meta.WindowActor} realWindow - the Meta.WindowActor this clone is for.
 * @classdesc
 * ![The {@link WindowClone} for the terminal is outlined in yellow.](pics/workspace_Display-red_Clone-yellow_Overlay-green.png)
 *
 * This is the thumbnail of a window that you see in the windows tab of the
 * Overview.
 *
 * You can vertical-scroll on the window to zoom in/out, click on it to activate
 * it, drag it to move it between workspaces. One per window actor.
 *
 * A {@link Workspace} contains many {@link WindowClone}s, one for each actor
 * on that workspace.
 * @class
 */
function WindowClone(realWindow) {
    this._init(realWindow);
}

WindowClone.prototype = {
    _init : function(realWindow) {
        /** The Meta.WindowActor this clone is for.
         * @type {Meta.WindowActor} */
        this.realWindow = realWindow;
        /** The Meta.Window for the window actor this clone is for
         * (`[this.realWindow]{@link #realWindow}.meta_window`).
         * @type {Meta.Window} */
        this.metaWindow = realWindow.meta_window;
        this.metaWindow._delegate = this;

        let [borderX, borderY] = this._getInvisibleBorderPadding();
        /** The Clutter.Clone cloning {@link Meta.WindowActor}'s texture, i.e.
         * a thumbnail of the window (we remove the invisible border padding
         * that all Meta.WindowActors have).
         * @type {Clutter.Clone} */
        this._windowClone = new Clutter.Clone({ source: realWindow.get_texture(),
                                                x: -borderX,
                                                y: -borderY });
        // We expect this.actor to be used for all interaction rather than
        // this._windowClone; as the former is reactive and the latter
        // is not, this just works for most cases. However, for DND all
        // actors are picked, so DND operations would operate on the clone.
        // To avoid this, we hide it from pick.
        Shell.util_set_hidden_from_pick(this._windowClone, true);

        this.origX = realWindow.x + borderX;
        this.origY = realWindow.y + borderY;

        let outerRect = realWindow.meta_window.get_outer_rect();

        // The MetaShapedTexture that we clone has a size that includes
        // the invisible border; this is inconvenient; rather than trying
        // to compensate all over the place we insert a ClutterGroup into
        // the hierarchy that is sized to only the visible portion.
        this.actor = new Clutter.Group({ reactive: true,
                                         x: this.origX,
                                         y: this.origY,
                                         width: outerRect.width,
                                         height: outerRect.height });

        this.actor.add_actor(this._windowClone);

        this.actor._delegate = this;

        this._stackAbove = null;

        this._sizeChangedId = this.realWindow.connect('size-changed',
            Lang.bind(this, this._onRealWindowSizeChanged));
        this._realWindowDestroyId = this.realWindow.connect('destroy',
            Lang.bind(this, this._disconnectRealWindowSignals));

        let clickAction = new Clutter.ClickAction();
        clickAction.connect('clicked', Lang.bind(this, this._onClicked));
        clickAction.connect('long-press', Lang.bind(this, this._onLongPress));

        this.actor.add_action(clickAction);

        this.actor.connect('scroll-event',
                           Lang.bind(this, this._onScroll));

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));
        this.actor.connect('leave-event',
                           Lang.bind(this, this._onLeave));

        /** The draggable for this actor.
         * We use parameters `restoreOnSuccess=true`, `manualMode=true`,
         * `dragActorMaxSize={@link WINDOW_DND_SIZE}`,
         * `dragActorOpacity={@link DRAGGING_WINDOW_OPACITY}`.
         * @type {DND._Draggable} */
        this._draggable = DND.makeDraggable(this.actor,
                                            { restoreOnSuccess: true,
                                              manualMode: true,
                                              dragActorMaxSize: WINDOW_DND_SIZE,
                                              dragActorOpacity: DRAGGING_WINDOW_OPACITY });
        this._draggable.connect('drag-begin', Lang.bind(this, this._onDragBegin));
        this._draggable.connect('drag-cancelled', Lang.bind(this, this._onDragCancelled));
        this._draggable.connect('drag-end', Lang.bind(this, this._onDragEnd));
        /** Whether we are currently being dragged.
         * @type {boolean} */
        this.inDrag = false;

        this._windowIsZooming = false;
        this._zooming = false;
        this._selected = false;
    },

    /** Makes this actor be stacked above the other actor `actor`,
     * i.e. calls `clutter_actor_raise(self, actor)` to raise this actor to
     * be above `actor`. Use `actor=null` to lower us to the bottom.
     *
     * Used in {@link Workspace#syncStacking}.
     * @see Workspace#syncStacking
     * @param {?Clutter.Actor} actor - other actor to appear above. If `null`,
     * this actor is lowered to the bottom.
     */
    setStackAbove: function (actor) {
        this._stackAbove = actor;
        if (this.inDrag || this._zooming)
            // We'll fix up the stack after the drag/zooming
            return;
        if (this._stackAbove == null)
            this.actor.lower_bottom();
        else
            this.actor.raise(this._stackAbove);
    },

    /** Destroys {@link #actor} */
    destroy: function () {
        this.actor.destroy();
    },

    /** Cancels a zoom (used from {@link Workspace#zoomFromOverview} when we
     * leave the overview - need to end the zoom first if it's not the window
     * we'll be bringing to focus upon exiting the overview).
     *
     * Comment in code:
     * If the user clicked on the zoomed window, or we are
     * returning there anyways, then we can zoom right to the
     * window, but if we are going to some other window, then
     * we need to cancel the zoom before animating, or it
     * will look funny.
     * @see Workspace#zoomFromOverview
     * @see #_zoomEnd
     */
    zoomFromOverview: function() {
        if (this._zooming) {
            // If the user clicked on the zoomed window, or we are
            // returning there anyways, then we can zoom right to the
            // window, but if we are going to some other window, then
            // we need to cancel the zoom before animating, or it
            // will look funny.

            if (!this._selected &&
                this.metaWindow != global.display.focus_window)
                this._zoomEnd();
        }
    },

    /** Disconnects from signals we were listening to on
     * {@link #realWindow} (in particular, the 'size-changed'
     * and 'destroy' signals). */
    _disconnectRealWindowSignals: function() {
        if (this._sizeChangedId > 0)
            this.realWindow.disconnect(this._sizeChangedId);
        this._sizeChangedId = 0;

        if (this._realWindowDestroyId > 0)
            this.realWindow.disconnect(this._realWindowDestroyId);
        this._realWindowDestroyId = 0;
    },

    /** Works out the width and height of the invisible border padding around
     * this window (the difference between the window actor and the window's sizes).
     * 
     * This is because metacity window actors are slightly larger than their
     * metacity windows, adding extra clear padding around the sides of the
     * window in order to make resizing easier.
     *
     * We need this to make sure that {@link #_windowClone} (which
     * is a clone of the window *actor*) sits nicely in {@link #actor}
     * (which is the same size as the *window*, smaller than that of the actor).
     *
     * See the note in the [constructor documentation]{@link WindowClone}.
     *
     * @returns {number[]} `[xpadding, ypadding]`.
     */
    _getInvisibleBorderPadding: function() {
        // We need to adjust the position of the actor because of the
        // consequences of invisible borders -- in reality, the texture
        // has an extra set of "padding" around it that we need to trim
        // down.

        // The outer rect paradoxically is the smaller rectangle,
        // containing the positions of the visible frame. The input
        // rect contains everything, including the invisible border
        // padding.
        let outerRect = this.metaWindow.get_outer_rect();
        let inputRect = this.metaWindow.get_input_rect();
        let [borderX, borderY] = [outerRect.x - inputRect.x,
                                  outerRect.y - inputRect.y];

        return [borderX, borderY];
    },

    /** Callback for the 'size-changed' signal of {@link #realWindow}
     * (when the underlying window's size changes).
     *
     * This emits a 'size-changed' signal and updates the stored invsible border
     * padding values to make sure that {@link #_windowClone} is
     * sitting nicely in {@link #actor}.
     * @see #_getInvisibleBorderPadding
     * @fires .size-changed
     */
    _onRealWindowSizeChanged: function() {
        let [borderX, borderY] = this._getInvisibleBorderPadding();
        let outerRect = this.metaWindow.get_outer_rect();
        this.actor.set_size(outerRect.width, outerRect.height);
        this._windowClone.set_position(-borderX, -borderY);
        this.emit('size-changed');
    },

    /** Callback when the underlying window for this clone is destroyed.
     *
     * We disconnect all our signals from it, unzoom if we are currently zoomed,
     * emit 'drag-end' if we were being dragged, and disconnect any listeners
     * to our signals.
     * @fires drag-end
     */
    _onDestroy: function() {
        this._disconnectRealWindowSignals();

        this.metaWindow._delegate = null;
        this.actor._delegate = null;
        if (this._zoomLightbox)
            this._zoomLightbox.destroy();

        if (this.inDrag) {
            this.emit('drag-end');
            this.inDrag = false;
        }

        this.disconnectAll();
    },

    /** Callback when the user's cursor leaves the thumbnail.
     *
     * If we are currently zoomed in and the user's cursor leaves the thumbnail,
     * we stop the zoom.
     */
    _onLeave: function (actor, event) {
        if (this._zoomStep)
            this._zoomEnd();
    },

    /** Callback when the user scrolls with the mouse while over the thumbnail.
     *
     * If they scrolled up, we zoom in the thumbnail. If they scrolled down,
     * we zoom out of the thumbnail.
     *
     * You can only scroll up until the window clone reaches the same size
     * as the underlying window, and you can only scroll down until the window
     * clone is the same as its starting size in the overview.
     *
     * We call {@link #_zoomUpdate} to update the picture to reflect
     * the new scale.
     * @see #_zoomUpdate
     * @see #_zoomStart
     * @see #_zoomEnd
     */
    _onScroll : function (actor, event) {
        let direction = event.get_scroll_direction();
        if (direction == Clutter.ScrollDirection.UP) {
            if (this._zoomStep == undefined)
                this._zoomStart();
            if (this._zoomStep < 100) {
                this._zoomStep += SCROLL_SCALE_AMOUNT;
                this._zoomUpdate();
            }
        } else if (direction == Clutter.ScrollDirection.DOWN) {
            if (this._zoomStep > 0) {
                this._zoomStep -= SCROLL_SCALE_AMOUNT;
                this._zoomStep = Math.max(0, this._zoomStep);
                this._zoomUpdate();
            }
            if (this._zoomStep <= 0.0)
                this._zoomEnd();
        }

    },

    /** Updates the thumbnail in response to a change in zooming.
     * The biggest we can zoom is up to the actor's original size and we make
     * sure that we never take up more than one monitor's space. */
    _zoomUpdate : function () {
        [this.actor.x, this.actor.y] = this._zoomGlobalOrig.interpPosition(this._zoomTarget, this._zoomStep / 100);
        [this.actor.scale_x, this.actor.scale_y] = this._zoomGlobalOrig.interpScale(this._zoomTarget, this._zoomStep / 100);

        let [width, height] = this.actor.get_transformed_size();

        let monitorIndex = this.metaWindow.get_monitor();
        let monitor = Main.layoutManager.monitors[monitorIndex];
        let availArea = new Meta.Rectangle({ x: monitor.x,
                                             y: monitor.y,
                                             width: monitor.width,
                                             height: monitor.height });
        if (monitorIndex == Main.layoutManager.primaryIndex) {
            availArea.y += Main.panel.actor.height;
            availArea.height -= Main.panel.actor.height;
        }

        this.actor.x = _clamp(this.actor.x, availArea.x, availArea.x + availArea.width - width);
        this.actor.y = _clamp(this.actor.y, availArea.y, availArea.y + availArea.height - height);
    },

    /** Called to start a zoom (for example from {@link #_onScroll})
     * when the user scrolls up on a previously-unzoomed window clone).
     * 
     * Emits a zoom-start signal and drops a {@link LightBox.LightBox} over
     * the rest of the screen over time {@link LIGHTBOX_FADE_TIME}.
     *
     * We create and store some {@link ScaledPoint}s to help with the zoom calculations
     * and call {@link #_zoomUpdate} when we're done to do the new
     * positioning.
     * @fires .zoom-start
     */
    _zoomStart : function () {
        this._zooming = true;
        this.emit('zoom-start');

        if (!this._zoomLightbox)
            this._zoomLightbox = new Lightbox.Lightbox(Main.uiGroup,
                                                       { fadeTime: LIGHTBOX_FADE_TIME });
        this._zoomLightbox.show();

        this._zoomLocalOrig  = new ScaledPoint(this.actor.x, this.actor.y, this.actor.scale_x, this.actor.scale_y);
        this._zoomGlobalOrig = new ScaledPoint();
        let parent = this._origParent = this.actor.get_parent();
        let [width, height] = this.actor.get_transformed_size();
        this._zoomGlobalOrig.setPosition.apply(this._zoomGlobalOrig, this.actor.get_transformed_position());
        this._zoomGlobalOrig.setScale(width / this.actor.width, height / this.actor.height);

        this.actor.reparent(Main.uiGroup);
        this._zoomLightbox.highlight(this.actor);

        [this.actor.x, this.actor.y]             = this._zoomGlobalOrig.getPosition();
        [this.actor.scale_x, this.actor.scale_y] = this._zoomGlobalOrig.getScale();

        this.actor.raise_top();

        this._zoomTarget = new ScaledPoint(0, 0, 1.0, 1.0);
        this._zoomTarget.setPosition(this.actor.x - (this.actor.width - width) / 2, this.actor.y - (this.actor.height - height) / 2);
        this._zoomStep = 0;

        this._zoomUpdate();
    },

    /** This is used to end a zoom, either when the user has finished zooming
     * and returned the clone back to its original size, or the zoom is cancelled
     * (the overview was left or they moved their mouse out of the window clone).
     *
     * We hide the lightbox that was in place while we were zooming and reset
     * the various zooming variables.
     *
     * Emits a 'zoom-end' signal.
     *
     * @fires WindowCone.zoom-end
     */
    _zoomEnd : function () {
        this._zooming = false;
        this.emit('zoom-end');

        this.actor.reparent(this._origParent);
        if (this._stackAbove == null)
            this.actor.lower_bottom();
        // If the workspace has been destroyed while we were reparented to
        // the stage, _stackAbove will be unparented and we can't raise our
        // actor above it - as we are bound to be destroyed anyway in that
        // case, we can skip that step
        else if (this._stackAbove.get_parent())
            this.actor.raise(this._stackAbove);

        [this.actor.x, this.actor.y]             = this._zoomLocalOrig.getPosition();
        [this.actor.scale_x, this.actor.scale_y] = this._zoomLocalOrig.getScale();

        this._zoomLightbox.hide();

        this._zoomLocalPosition  = undefined;
        this._zoomLocalScale     = undefined;
        this._zoomGlobalPosition = undefined;
        this._zoomGlobalScale    = undefined;
        this._zoomTargetPosition = undefined;
        this._zoomStep           = undefined;
    },

    /** Callback when {@link #actor} is clicked.
     * This sets a local '_selected' property to true and emits the
     * 'selected' signal with the current time but does nothing else.
     *
     * The {@link Workspace} listens to this and activates the window.
     * @fires .selected
     */
    _onClicked: function(action, actor) {
        this._selected = true;
        this.emit('selected', global.get_current_time());
    },

    /** Callback when the user long-presses {@link #actor}.
     * This is used to detect the start of a drag and call
     * [`this._draggable.startDrag`]{@link DND._Draggable#startDrag},
     * starting the drag.
     */
    _onLongPress: function(action, actor, state) {
        // Take advantage of the Clutter policy to consider
        // a long-press canceled when the pointer movement
        // exceeds dnd-drag-threshold to manually start the drag
        if (state == Clutter.LongPressState.CANCEL) {
            // A click cancels a long-press before any click handler is
            // run - make sure to not start a drag in that case
            Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this,
                function() {
                    if (this._selected)
                        return;
                    let [x, y] = action.get_coords();
                    this._draggable.startDrag(x, y, global.get_current_time());
                }));
        }
        return true;
    },

    /** Callback when {@link #_draggable} emits 'drag-begin', i.e.
     * this window clone has started to be dragged.
     *
     * This cancels any zooming and re-emits a 'drag-begin' signal from the clone
     * so that other classes can listen.
     *
     * Also sets {#inDrag} to true.
     * @fires .drag-begin
     */
    _onDragBegin : function (draggable, time) {
        if (this._zooming)
            this._zoomEnd();

        [this.dragOrigX, this.dragOrigY] = this.actor.get_position();
        this.dragOrigScale = this.actor.scale_x;
        this.inDrag = true;
        this.emit('drag-begin');
    },

    /** Returns the {@link Workspace.Workspace} that owns this window clone
     * (makes the call via [`Main.overview.workspaces`]{@link Overview.Overview#workspaces}).
     * @see WorkspacesView.WorkspacesView#getWorkspacesByIndex
     * @returns {Workspace.Workspace} the workspace that this window clone
     * is owned by.
     */
    _getWorkspaceActor : function() {
        let index = this.metaWindow.get_workspace().index();
        return Main.overview.workspaces.getWorkspaceByIndex(index);
    },

    /** Implements a the handleDragOver function which sets this object up
     * as a potential drag target. This just outsources to {@link Workspace#handleDragOver}
     * (for the workspace that this window clone belongs to).
     *
     * (Basically dragging  something over a window clone is the same as dragging
     * it on over its {@link Workspace}).
     * @todo mark as part of the Draggable interface.
     * @inheritparams Workspace#handleDragOver
     * @see Workspace#handleDragOver
     */
    handleDragOver : function(source, actor, x, y, time) {
        let workspace = this._getWorkspaceActor();
        return workspace.handleDragOver(source, actor, x, y, time);
    },

    /** Implements a the accept function which sets this object up
     * to accept drops. This just outsources to {@link Workspace#acceptDrop}
     * (for the workspace that this window clone belongs to).
     *
     * (Basically dropping something on a window clone is the same as dropping
     * it on its {@link Workspace}).
     * @todo mark as part of the Draggable interface.
     * @inheritparams Workspace#acceptDrop
     * @see Workspace#acceptDrop
     */
    acceptDrop : function(source, actor, x, y, time) {
        let workspace = this._getWorkspaceActor();
        workspace.acceptDrop(source, actor, x, y, time);
    },

    /** Callback for the 'drag-cancelled' signal of {@link #_draggable}.
     * Just re-emits the signal (from the clone rather than the draggable).
     * @fires .drag-cancelled
     */
    _onDragCancelled : function (draggable, time) {
        this.emit('drag-cancelled');
    },

    /** Callback for the 'drag-end' signal of {@link #_draggable}.
     *
     * Sets {@link #inDrag} to false and makes sure our stacking is
     * in sync with the actual window stacking order.
     * Re-emits the signal (from the clone rather than the draggable).
     * @inheritparams DND._Draggable.drag-end
     * @fires .drag-end
     */
    _onDragEnd : function (draggable, time, snapback) {
        this.inDrag = false;

        // We may not have a parent if DnD completed successfully, in
        // which case our clone will shortly be destroyed and replaced
        // with a new one on the target workspace.
        if (this.actor.get_parent() != null) {
            if (this._stackAbove == null)
                this.actor.lower_bottom();
            else
                this.actor.raise(this._stackAbove);
        }


        this.emit('drag-end');
    }
};
Signals.addSignalMethods(WindowClone.prototype);
// Window Clone signals
/** Emitted when the size of the clone's underlying window is changed (and hence
 * our size is changed to reflect that).
 * @name size-changed
 * @event
 * @memberof WindowClone
 */
/** Emitted when the user starts zooming a window clone.
 * @see WindowClone#_zoomStart
 * @name zoom-start
 * @event
 * @memberof WindowClone
 */
/** Emitted when the user ends zooming a window clone (it returns to its
 * original size, or the zoom is cancelled).
 * @see WindowClone#_zoomEnd
 * @event
 * @name zoom-end
 * @memberof WindowClone
 */
/** Emitted when the user clicks on a window clone.
 * The {@link Workspace} listens to this and activates the window.
 * @name selected
 * @param {Workspace.WindowClone} clone - the window clone that emitted the
 * signal
 * @param {number} time - the time of the selection.
 * @memberof WindowClone
 * @event
 * @see Workspace#_onCloneSelected
 */
/** Emitted when the user starts dragging the window clone.
 * @name drag-begin
 * @event
 * @memberof WindowClone
 */
/** Emitted when the user stops dragging the window clone.
 * @see WindowClone#_onDragEnd
 * @name drag-end
 * @event
 * @memberof WindowClone
 */
/** Emitted when the user cancels dragging the window clone.
 * @name drag-cancelled
 * @event
 * @memberof WindowClone
 */


/** Creates a new WindowOverlay
 *
 * We create a St.Label for the window caption and add it to `parentActor`,
 * connecting up to `windowClone.metaWindow`'s 'notify::title' signal to
 * make sure the caption is always up to date. This is stored in
 * {@link #title}.
 *
 * We then create a St.Button for the close button and add it to `parentActor`,
 * connecting up its click event to {@link #_closeWindow}.
 *
 * @param {Workspace.WindowClone} windowClone - Corresponding window clone
 * @param {Clutter.Actor} parentActor - The actor which will be the parent of
 * all overlay items such as app icon and window caption (i.e. we add the close
 * button and caption to this). In our case it's {@link Workspace#_windowOverlaysGroup}.
 * @classdesc
 *
 * A Window Overlay consists of stuff added to a {@link WindowClone} in the overlay.
 *
 * That is, the close button on the window clone and the caption for the window clone.
 *
 * Each WindowClone gets its own WindowOverlay.
 *
 * The spacing between the bottom of a window clone and its caption is
 * defined by the '-shell-caption-spacing' CSS property of {@link #title}'s
 * style class.
 *
 * The amount of overlap of the close button over the corner of the window
 * clone is defined by the '-shell-close-overlap' CSS property of
 * {@link #closeButton}'s style class.
 *
 * @todo pic
 * @class
 */
function WindowOverlay(windowClone, parentActor) {
    this._init(windowClone, parentActor);
}

WindowOverlay.prototype = {
    _init : function(windowClone, parentActor) {
        let metaWindow = windowClone.metaWindow;

        /** The window clone for this overlay.
         * @type {Workspace.WindowClone} */
        this._windowClone = windowClone;
        /** The parent meta window actor to add the button and title to.
         * @see Workspace#_windowOverlaysGroup
         * @type {Clutter.Actor}
         */
        this._parentActor = parentActor;
        this._hidden = false;

        let title = new St.Label({ style_class: 'window-caption',
                                   text: metaWindow.title });
        title.clutter_text.ellipsize = Pango.EllipsizeMode.END;
        title._spacing = 0;

        this._updateCaptionId = metaWindow.connect('notify::title',
            Lang.bind(this, function(w) {
                this.title.text = w.title;
            }));

        let button = new St.Button({ style_class: 'window-close' });
        button._overlap = 0;

        this._idleToggleCloseId = 0;
        button.connect('clicked', Lang.bind(this, this._closeWindow));

        windowClone.actor.connect('destroy', Lang.bind(this, this._onDestroy));
        windowClone.actor.connect('enter-event',
                                  Lang.bind(this, this._onEnter));
        windowClone.actor.connect('leave-event',
                                  Lang.bind(this, this._onLeave));

        this._windowAddedId = 0;
        windowClone.connect('zoom-start', Lang.bind(this, this.hide));
        windowClone.connect('zoom-end', Lang.bind(this, this.show));

        button.hide();

        /** The caption displaying the window's title. Style class
         * 'window-caption', ellipsized if too long.
         * @type {St.Label} */
        this.title = title;
        /** The close button over the window clone. Style class 'window-close'.
         * @type {St.Button} */
        this.closeButton = button;

        parentActor.add_actor(this.title);
        parentActor.add_actor(this.closeButton);
        title.connect('style-changed',
                      Lang.bind(this, this._onStyleChanged));
        button.connect('style-changed',
                       Lang.bind(this, this._onStyleChanged));
        // force a style change if we are already on a stage - otherwise
        // the signal will be emitted normally when we are added
        if (parentActor.get_stage())
            this._onStyleChanged();
    },

    /** Hides the overlay (close button and caption) */
    hide: function() {
        this._hidden = true;
        this.closeButton.hide();
        this.title.hide();
    },

    /** Shows the overlay (caption, and the close button if the pointer is over
     * the window clone for this overlay) without animation.
     * @see #fadeIn */
    show: function() {
        this._hidden = false;
        if (this._windowClone.actor.has_pointer)
            this.closeButton.show();
        this.title.show();
    },

    /** Shows the close button if the pointer is over the window clone for this
     * overlay, and fades in the caption over time {@link CLOSE_BUTTON_FADE_TIME}.
     */
    fadeIn: function() {
        this.show();
        this.title.opacity = 0;
        this._parentActor.raise_top();
        Tweener.addTween(this.title,
                        { opacity: 255,
                          time: CLOSE_BUTTON_FADE_TIME,
                          transition: 'easeOutQuad' });
    },

    /** The amount by which the close button sticks outside of its window clone
     * width-wise (i.e. its width minus the overlap).
     * @returns {number} the pixels by which the close button sticks outside
     * of its window clone.
     * @todo diagram
     */
    chromeWidth: function () {
        return this.closeButton.width - this.closeButton._overlap;
    },

    
    /** The amount by which the close button sticks outside of its window clone
     * height-wise, top and bottom.
     *
     * @returns {number[]} the pixels by which the close button sticks outside
     * of its window clone (its height minus its vertical overlap), and the
     * pixels by which the caption sticks below its window clone (being
     * its height plus the spacing between the bottom of the clone and it).
     * @todo diagram
     */
    chromeHeights: function () {
        return [this.closeButton.height - this.closeButton._overlap,
               this.title.height + this.title._spacing];
    },

    /** Updates the position of the close button and title so that they are
     * positioned:
     *
     * * the close button should be one top-right corner of the window clone,
     *   unless gconf key {@link BUTTON_LAYOUT_KEY} has it in the top-left in
     *   which case it's put there.
     * * the title should be placed below the window clone, capped at the width
     * of the clone and centered horizontally.
     *
     * These parameters are not the values retrieved with
     * get_transformed_position() and get_transformed_size(),
     * as the WindowClone might be moving. (so what are they??)
     *
     * @see Workspace#_showWindowOverlay
     * @param {number} cloneX - x position of windowClone
     * @param {number} cloneY - y position of windowClone
     * @param {number} cloneWidth - width of windowClone
     * @param {number} cloneHeight
     */
    updatePositions: function(cloneX, cloneY, cloneWidth, cloneHeight) {
        let button = this.closeButton;
        let title = this.title;

        let gconf = GConf.Client.get_default();
        let layout = gconf.get_string(BUTTON_LAYOUT_KEY);
        let rtl = St.Widget.get_default_direction() == St.TextDirection.RTL;

        let split = layout.split(":");
        let side;
        if (split[0].indexOf("close") > -1)
            side = rtl ? St.Side.RIGHT : St.Side.LEFT;
        else
            side = rtl ? St.Side.LEFT : St.Side.RIGHT;

        let buttonX;
        let buttonY = cloneY - (button.height - button._overlap);
        if (side == St.Side.LEFT)
            buttonX = cloneX - (button.width - button._overlap);
        else
            buttonX = cloneX + (cloneWidth - button._overlap);

        button.set_position(Math.floor(buttonX), Math.floor(buttonY));

        if (!title.fullWidth)
            title.fullWidth = title.width;
        title.width = Math.min(title.fullWidth, cloneWidth);

        let titleX = cloneX + (cloneWidth - title.width) / 2;
        let titleY = cloneY + cloneHeight + title._spacing;
        title.set_position(Math.floor(titleX), Math.floor(titleY));
    },

    /** Callback when the user clicks on the close button.
     *
     * This closes the source window, but listens on the same metacity workspace
     * for a window being added that is transient for this one, calling
     * {@link WindowClone#_onWindowAdded} when this is detected.
     *
     * I think the point of this is if you (say) close a window from the overview
     * and it pops up a dialog ("Are you SURE you want to quit?"), we listen
     * for this dialog and if it is found, we switch to it.
     */
    _closeWindow: function(actor) {
        let metaWindow = this._windowClone.metaWindow;
        this._workspace = metaWindow.get_workspace();

        this._windowAddedId = this._workspace.connect('window-added',
                                                      Lang.bind(this,
                                                                this._onWindowAdded));

        metaWindow.delete(global.get_current_time());
    },

    /** Callback when the user tried to close a window with the close button
     * and then a new transient window was spawned for this window.
     *
     * I think the point of this is if you (say) close a window from the overview
     * and it pops up a dialog ("Are you SURE you want to quit?"), we listen
     * for this dialog and if it is found, we switch to it.
     *
     * Upon detecting this we emit ['selected']{@link WindowClone.event:selected} from
     * the window clone which causes the window to be activated, allowing the
     * user to deal with the dialog.
     * @fires .event:selected
     */
    _onWindowAdded: function(workspace, win) {
        let metaWindow = this._windowClone.metaWindow;

        if (win.get_transient_for() == metaWindow) {
            workspace.disconnect(this._windowAddedId);
            this._windowAddedId = 0;

            // use an idle handler to avoid mapping problems -
            // see comment in Workspace._windowAdded
            Mainloop.idle_add(Lang.bind(this,
                                        function() {
                                            this._windowClone.emit('selected');
                                            return false;
                                        }));
        }
    },

    /** Callback when our source {@link WindowClone} is destroyed.
     *
     * This connects any signals/timeouts we currently have outstanding
     * and destroys the caption and close button.
     */
    _onDestroy: function() {
        if (this._windowAddedId > 0) {
            this._workspace.disconnect(this._windowAddedId);
            this._windowAddedId = 0;
        }
        if (this._idleToggleCloseId > 0) {
            Mainloop.source_remove(this._idleToggleCloseId);
            this._idleToggleCloseId = 0;
        }
        this._windowClone.metaWindow.disconnect(this._updateCaptionId);
        this.title.destroy();
        this.closeButton.destroy();
    },

    /** Callback when the user's pointer enters our window clone.
     * 
     * If the overlay is currently showing, we show the close button and
     * emit 'show-close-button' (the {@link Workspace} listens to this in order
     * to hide any close buttons that are still visible on other window clones
     * in the workspace).
     * @see Workspace#_onShowOverlayClose
     * @fires show-close-button
     */
    _onEnter: function() {
        // We might get enter events on the clone while the overlay is
        // hidden, e.g. during animations, we ignore these events,
        // as the close button will be shown as needed when the overlays
        // are shown again
        if (this._hidden)
            return;
        this._parentActor.raise_top();
        this.closeButton.show();
        this.emit('show-close-button');
    },

    /** Callback when the user's pointer leaves our window clone. This causes
     * the close button to fade after 750ms (unless the pointer enters a different
     * WindowClone which will cause that clone's button to be shown which will
     * emit a show-close-button signal which will cause the parent {@link Workspace}
     * to hide this button immediately).
     * @see #_idleToggleCloseButton */
    _onLeave: function() {
        if (this._idleToggleCloseId == 0)
            this._idleToggleCloseId = Mainloop.timeout_add(750, Lang.bind(this, this._idleToggleCloseButton));
    },

    /** Callback after 750ms has elapsed sinc the user's pointer left our
     * window clone. If the user is no longer hovering over this window clone,
     * hide the close button.
     * @see #_onLeave
     */
    _idleToggleCloseButton: function() {
        this._idleToggleCloseId = 0;
        if (!this._windowClone.actor.has_pointer &&
            !this.closeButton.has_pointer)
            this.closeButton.hide();

        return false;
    },

    /** Hides the close button straight away, cancelling any timeouts waiting
     * to close the button through inactivity.
     *
     * This happens when the user enters a window clone causing its close button
     * to show, leaves it, and then enters another window clone before 750ms
     * has passed (after which the close button of the first clone will faded out).
     * Entering the second window clone and causing its close button to show
     * emits a 'show-close-button' which is caught by the parent {@link Workspace}
     * to force all other close buttons to disappear immediately rather than
     * waiting for the timeout.
     * @see Workspace#_onShowOverlayClose
     */
    hideCloseButton: function() {
        if (this._idleToggleCloseId > 0) {
            Mainloop.source_remove(this._idleToggleCloseId);
            this._idleToggleCloseId = 0;
        }
        this.closeButton.hide();
    },

    /** Callback when the styles of {@link #title} or
     * {@link #closeButton} change. This reloads
     * the '-shell-caption-spacing' property of the former and
     * '-shell-close-overlap' property of the latter and causes a reposition.
     *
     * '-shell-caption-spacing' is the distance between the bottom of the window
     * clone and the top of the caption. '-shell-close-overlap' is the amount
     * of vertical and horizontal overlap (pixels) of the close button over
     * the corner of its window clone.
     */
    _onStyleChanged: function() {
        let titleNode = this.title.get_theme_node();
        this.title._spacing = titleNode.get_length('-shell-caption-spacing');

        let closeNode = this.closeButton.get_theme_node();
        this.closeButton._overlap = closeNode.get_length('-shell-close-overlap');

        this._parentActor.queue_relayout();
    }
};
Signals.addSignalMethods(WindowOverlay.prototype);
// WindowOverlay events
/** Emitted when the user's pointer enters the window overlay's source
 * window clone, causing the close button to be shown.
 * @name show-close-button
 * @event
 * @memberof WindowOverlay
 */

/** Flags used for window positioning.
 * @const
 * @enum
 * @see Workspace#positionWindows
 */
const WindowPositionFlags = {
    /** Means that this is the initial positioning of the windows. */
    INITIAL: 1 << 0,
    /** Indicates that we need to animate changing position. */
    ANIMATE: 1 << 1
};

/**
 * Creates a new Workspace
 *
 * Note that a {@link Workspace} is meant to create {@link WindowClone}s and
 * {@link WindowOverlay}s for each window on a particular workspace and monitor,
 * positioning them nicely.
 *
 * It is possible for the `metaWorkspace` argument to be `null`, meaning that
 * we simply want to show all windows on this monitor regardless of workspace.
 * This is used (for example) when the user has "workspace only on primary monitor"
 * set, where switching workspaces switches on the primary monitor only.
 * In this case, windows on the secondary monitor are not really in any workspace.
 *
 * First we store the underlying workspace and monitor that this workspace
 * is for in {@link #metaWorkspace} and {@link #monitorIndex}.
 *
 * Then we create the top-level actor for the {@link Workspace}
 * {@link #actor}, being a Clutter.Group which will hold the
 * {@link WindowClone}.actor for each of the windows on this monitor/workspace,
 * as well as a single group {@link #_windowOverlaysGroup} which
 * will hold all the {@link WindowOverlay}s for each window clone.
 *
 * The top-level actor and workspace overlays group are all given size 0 (their
 * contents have non-zero size so will still be shown properly).
 *
 * We create a Clutter.Rectangle which *does* get allocate the full size
 * available to it, {@link #_dropRect}. No actors are added to it.
 * The reasoning behind this is that since the container actors have size 0,
 * this will receive *all* drag/drop events, unless the drag/drop is over
 * one of the {@link WindowClone}s itself. So, this is a convenience actor
 * used to capture/implement all the drag/drop stuff rather than replicating
 * the same code for both the overlay group and the top-level actor. (I think).
 *
 * We then go through each window on the current workspace and monitor,
 * and call {@link #_addWindowClone} to create and add a window
 * clone for each, connecting to various of the clone's signals.
 *
 * Finally we connect to various window signals (enter/leaving monitor,
 * added/removed) in order to update our list of {@link WindowClone}s.
 *
 * @param {?Meta.Workspace} metaWorkspace - the underlying `Meta.Workspace` for
 * this {@link Workspace}, or `null` if we are going to manage all windows on
 * this monitor regardless of workspace.
 * @param {index} monitorIndex - the monitor on this workspace that the {@link
 * Workspace} is for.
 * @classdesc
 * @todo pic
 * A Workspace represents a collection of {@link WindowClone}s in the overview
 * (i.e. the window thumbnails). The {@link WorkspacesView.WorkspacesView} is
 * what manages multiple {@link Workspace}s.
 *
 * Each one just looks after the window clones in its own workspace *and* monitor
 * (specified at creation). 
 *
 * It handles how to lay out its windows (for example in GNOME 3.4 when you
 * have 5 overlays it positions three in the top row and two in the bottom row).
 * For small numbers of windows (one to five) the layouts are defined in
 * {@link POSITIONS}.
 *
 * The window layout code is fairly complex.
 * Useful terminology - a "slot"
 * is the rectangle in {@link Workspace} that a given {@link WindowClone}
 * is allowed to occupy. A slot's position/size is determined by its
 * {@link Slot} (center and scale as fractions relative to the {@link Workspace}
 * size). The window clone doesn't have to adjust its aspect ratio to fill the slot;
 * it just preserves its original aspect ratio but otherwise takes up as much
 * space in the slot as it can.
 *
 * Usually we try to arrange
 * windows such that the *total* squared distance that each window has to move
 * to reach its slot is minimized, with some tweaks if there are too many
 * windows for this code to be viable. See {@link #_orderWindowsByMotionAndStartup}
 * and {@link #positionWindows} (and the functions linked therein) for
 * further details.
 *
 * Note - this is *not* the workspace previews you see in the workspaces sidebar;
 * they are {@link WorkspaceThumbnail.WorkspaceThumbnail}s.
 * @class
 */
function Workspace(metaWorkspace, monitorIndex) {
    this._init(metaWorkspace, monitorIndex);
}

Workspace.prototype = {
    _init : function(metaWorkspace, monitorIndex) {
        // When dragging a window, we use this slot for reserve space.
        this._reservedSlot = null;
        /** The underlying workspace for this {@link Workspace}
         * (we only display windows on this workspace and the monitor
         * {@link #monitorIndex}).
         *
         * Can be null, meaning we are displaying all windows on this monitor
         * regardless of workspace (for example if it's the secondary monitor
         * and we have set the "workspaces only on primary monitor" setting to
         * true).
         * @type {?Meta.Workspace} */
        this.metaWorkspace = metaWorkspace;
        this._x = 0;
        this._y = 0;
        this._width = 0;
        this._height = 0;

        /** The monitor that this workspace is for (we only display windows
         * on this monitor and {@link Workspace.metaWorkspace}).
         * @type {number} */
        this.monitorIndex = monitorIndex;
        this._monitor = Main.layoutManager.monitors[this.monitorIndex];
        /** A Clutter.Group that will hold the {@link WindowOverlay}s 
         * (close button/window caption) for
         * each {@link WindowClone} this workspace has. 
         * @type {Clutter.Group} */
        this._windowOverlaysGroup = new Clutter.Group();
        // Without this the drop area will be overlapped.
        this._windowOverlaysGroup.set_size(0, 0);

        /** The top-level actor for this workspace. It's just a Clutter.Group
         * used to hold everything (it itself has size 0...)
         * @type {Clutter.Group} */
        this.actor = new Clutter.Group();
        this.actor.set_size(0, 0);

        /** I think this is what is used as the drag/drop target for the
         * {@link Workspace}. Its size is set with {@link #setGeometry}.
         * It actually has non-zero size (unlike {@link #actor} and
         * {@link #_windowOverlaysGroup} which both have 0 size,
         * although their *contents* have positive size).
         * @type {Clutter.Rectangle} */
        this._dropRect = new Clutter.Rectangle({ opacity: 0 });
        this._dropRect._delegate = this;

        this.actor.add_actor(this._dropRect);
        this.actor.add_actor(this._windowOverlaysGroup);

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        let windows = global.get_window_actors().filter(this._isMyWindow, this);

        // Create clones for windows that should be
        // visible in the Overview
        /** This holds the {@link WindowClone}s for each of the windows on
         * this workspace/monitor.
         * @see #_addWindowClone
         * @type {Workspace.WindowClone[]} */
        this._windows = [];
        /** This holds the {@link WindowOverlay}s for each of the
         * {@link WindowClone}s in {@link #_windows}.
         * @see #_addWindowClone
         * @type {Workspace.WindowOverlay[]} */
        this._windowOverlays = [];
        for (let i = 0; i < windows.length; i++) {
            if (this._isOverviewWindow(windows[i])) {
                this._addWindowClone(windows[i]);
            }
        }

        // Track window changes
        if (this.metaWorkspace) {
            this._windowAddedId = this.metaWorkspace.connect('window-added',
                                                             Lang.bind(this, this._windowAdded));
            this._windowRemovedId = this.metaWorkspace.connect('window-removed',
                                                               Lang.bind(this, this._windowRemoved));
        }
        this._windowEnteredMonitorId = global.screen.connect('window-entered-monitor',
                                                           Lang.bind(this, this._windowEnteredMonitor));
        this._windowLeftMonitorId = global.screen.connect('window-left-monitor',
                                                           Lang.bind(this, this._windowLeftMonitor));
        this._repositionWindowsId = 0;

        /** Are we currently in the process of leaving the overview?
         * @readonly
         * @type {boolean} */
        this.leavingOverview = false;
    },

    /** Sets the size of the {@link Workspace} and repositions all the windows
     * inside.
     *
     * In particular, it sets the size of {@link #_dropRect} to the
     * requested size. (Recall that {@link #actor} and
     * {@link #_windowOverlaysGroup} have size 0 so that they do not
     * receive any events; the {@link #_dropRect} gets them all).
     * @param {number} x - x coordinate for the {@link Workspace}
     * @param {number} y - y coordinate for the {@link Workspace}
     * @param {number} width - width of the {@link Workspace}
     * @param {number} height - height of the {@link Workspace}
     * @see #positionWindows
     */
    setGeometry: function(x, y, width, height) {
        this._x = x;
        this._y = y;
        this._width = width;
        this._height = height;

        // This is sometimes called during allocation, so we do this later
        Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this,
            function () {
                this._dropRect.set_position(x, y);
                this._dropRect.set_size(width, height);
                this.positionWindows(WindowPositionFlags.ANIMATE);
                return false;
            }));

    },

    /** Looks up the index of a meta window in our internal list
     * {@link #_windows}.
     * @param {Meta.Window} metaWindow - a metacity window to look up
     * @returns {number} the index of the clone for that window in {@link #_windows},
     * or -1 if not found.
     */
    _lookupIndex: function (metaWindow) {
        for (let i = 0; i < this._windows.length; i++) {
            if (this._windows[i].metaWindow == metaWindow) {
                return i;
            }
        }
        return -1;
    },

    /** Returns whether this Workspace is currently managing this metacity window
     * (i.e. whether we have a {@link WindowClone} for it).
     * @inheritparams #_lookupIndex
     * @returns {boolean} whether we contain `metaWindow`.
     * @see #_lookupIndex
     */
    containsMetaWindow: function (metaWindow) {
        return this._lookupIndex(metaWindow) >= 0;
    },

    /** Whether this {@link Workspace} is empty (actually whether we have no window
     * clones, as not all windows on a metacity workspace are given clones,
     * such as the desktop window if Nautilus is managing the desktop).
     * @returns {boolean} whether this {@link Workspace} has no window clones.
     */
    isEmpty: function() {
        return this._windows.length == 0;
    },

    // Only use this for n <= 20 say
    /** Wow - an implementation of factorial...(isn't this in javascript Math
     * somewhere?)
     *
     *     n! := n * (n - 1) * (n - 2) * .... * 3 * 2 * 1
     *
     * Calculates it with a good ol' fashioned loop (Comment says
     * "Only use this for n <= 20 say").
     *
     * @param {number} n - number to take factorial of.
     * @returns {number} `n!`.
     */
    _factorial: function(n) {
        let result = 1;
        for (let i = 2; i <= n; i++)
            result *= i;
        return result;
    },

    /**
     * Given an integer between 0 and `list.length!` (i.e. factorial of the
     * list's length), re-order the array in-place
     * into a permutation denoted by the index.
     *
     * I'm not sure how exactly `permutationIndex` maps to a permutation.
     * @param {number} permutationIndex - An integer from `[0, list.length!)`
     * (exclamation mark == factorial).
     * @param {Array.<any>} list - Array of objects to permute; will be modified in place
     * @todo explain how the integer `permutationIndex` relates to the permutation.
     * @see #_forEachPermutations
     */
    _permutation: function(permutationIndex, list) {
        for (let j = 2; j <= list.length; j++) {
            let firstIndex = (permutationIndex % j);
            let secondIndex = j - 1;
            // Swap
            let tmp = list[firstIndex];
            list[firstIndex] = list[secondIndex];
            list[secondIndex] = tmp;
            permutationIndex = Math.floor(permutationIndex / j);
        }
    },

    /**
     * Call `func` with each permutation of `list` as an argument.
     * Does not return anything.
     *
     * @param {Array.<any>} list - Array of objects to apply `func` to all
     * permuations of.
     * @param {function(Array.<any>)} func - Function which takes a single array
     * argument and will be applied to each permutation of `list`, all
     * `factorial(list.length)` of them.
     * @see #_permutation
     */
    _forEachPermutations: function(list, func) {
        let nCombinations = this._factorial(list.length);
        for (let i = 0; i < nCombinations; i++) {
            let listCopy = list.concat();
            this._permutation(i, listCopy);
            func(listCopy);
        }
    },

    /**
     * Returns a number corresponding to how much perceived motion
     * would be involved in moving the window to the given slot.
     * Currently this is the square of the distance between the
     * centers.
     *
     * Used in {@link #_orderWindowsPermutations} to determine how
     * to rearrange windows to minimize the amount of window movement (when
     * the windows have to rearrange themselves, example when a new one is
     * created).
     * 
     * @param {Clutter.Actor} actor - A {@link WindowClone}'s actor.
     * @param {Workspace.Slot} slot - A {@link Slot} to move the window
     * clone to.
     * @returns {number} the squared distance (pixels) between the centre of
     * `actor` and the coordinate specified by `slot`.
     */
    _computeWindowMotion: function (actor, slot) {
        let [xCenter, yCenter, fraction] = slot;
        let xDelta, yDelta, distanceSquared;
        let actorWidth, actorHeight;

        let x = actor.x;
        let y = actor.y;
        let scale = actor.scale_x;

        if (actor._delegate.inDrag) {
            x = actor._delegate.dragOrigX;
            y = actor._delegate.dragOrigY;
            scale = actor._delegate.dragOrigScale;
        }

        actorWidth = actor.width * scale;
        actorHeight = actor.height * scale;
        xDelta = x + actorWidth / 2.0 - xCenter * this._width - this._x;
        yDelta = y + actorHeight / 2.0 - yCenter * this._height - this._y;
        distanceSquared = xDelta * xDelta + yDelta * yDelta;

        return distanceSquared;
    },

    /**
     * Iterate over all permutations of the windows, and determine the
     * permutation which has the least total motion.
     *
     * Used in to determine how
     * to rearrange windows to minimize the amount of window movement (when
     * the windows have to rearrange themselves, example when a new one is
     * created) in order to fit them into arrangement `slots`.
     *
     * Called from {@link #_orderWindowsByMotionAndStartup} and ultimately
     * from {@link #positionWindows}.
     *
     * Contrast to {@link #_orderWindowsGreedy}, whereby each slot
     * (in order) just takes the window that is closest to it (excluding windows
     * previously taken). This is quicker but doesn't necessarily minimize the
     * overall amount of window movement that needs to be done.
     * @see #_orderWindowsGreedy
     * @see #positionWindows
     *
     * @param {Workspace.WindowClone[]} clones - array of window clones we want
     * to find the best permutation of.
     * @param {Workspace.Slot[]} slots - array of same length as `clones`
     * where `slots[i]` is the {@link Slot} of `clones[i]`.
     *
     * @returns {Workspace.WindowClone[]} the permutation of window clones
     * that minimizes the total distance the windows have to move to arrange
     * themselves into the position `slots` (the clone in `output[i]` should be
     * put into position `slots[i]`).
     */
    _orderWindowsPermutations: function (clones, slots) {
        let minimumMotionPermutation = null;
        let minimumMotion = -1;
        let permIndex = 0;
        this._forEachPermutations(clones, Lang.bind(this, function (permutation) {
            let motion = 0;
            for (let i = 0; i < permutation.length; i++) {
                let cloneActor = permutation[i].actor;
                let slot = slots[i];

                let delta = this._computeWindowMotion(cloneActor, slot);

                motion += delta;

                // Bail out early if we're already larger than the
                // previous best
                if (minimumMotionPermutation != null &&
                    motion > minimumMotion)
                    continue;
            }

            if (minimumMotionPermutation == null || motion < minimumMotion) {
                minimumMotionPermutation = permutation;
                minimumMotion = motion;
            }
            permIndex++;
        }));
        return minimumMotionPermutation;
    },

    /**
     * Iterate over available slots in order, placing into each one the window
     * we find with the smallest motion to that slot.
     *
     * Note that this is not necessarily the permutation the minimizes *total*
     * movement of windows into `slots`; see {@link #_orderWindowsPermutations}
     * for that. This is cheaper though.
     *
     * "Greedy" because each slot grabs whichever of the remaining windows is
     * best for it, without thinking about whether this is best for the *overall*
     * rearrangement of windows.
     *
     * @inheritparams #_orderWindowsPermutation
     *
     * @returns {Workspace.WindowClone[]} the permutation of window clones
     * such that `output[0]` is the window clone closest to `slots[0]`,
     * `output[1]` is the window clone closest to `slots[1]` (not including
     * the clone already in `output[0]`), and so on.
     * @see #_orderWindowsPermutations
     * @see #positionWindows
     */
    _orderWindowsGreedy: function(clones, slots) {
        let result = [];
        let slotIndex = 0;
        // Copy since we mutate below
        let clonesCopy = clones.concat();
        for (let i = 0; i < slots.length; i++) {
            let slot = slots[i];
            let minimumMotionIndex = -1;
            let minimumMotion = -1;
            for (let j = 0; j < clonesCopy.length; j++) {
                let cloneActor = clonesCopy[j].actor;
                let delta = this._computeWindowMotion(cloneActor, slot);
                if (minimumMotionIndex == -1 || delta < minimumMotion) {
                    minimumMotionIndex = j;
                    minimumMotion = delta;
                }
            }
            result.push(clonesCopy[minimumMotionIndex]);
            clonesCopy.splice(minimumMotionIndex, 1);
        }
        return result;
    },

    /**
     * Returns a copy of `clones`, ordered in such a way that they require least motion
     * to move to the final screen coordinates of `slots`.  Ties are broken in a stable
     * fashion by the order in which the windows were created.
     *
     * If there are at most {@link POSITIONING_PERMUTATIONS_MAX} windows
     * in `clones`, we use {@link #_orderWindowsPermutations} in order
     * to find the permutation of `windows` such the total distance each
     * window has to move in order to have `windows_permutation[i]` move to `slots[i]`
     * is minimized.
     *
     * Otherwise, this algorithm is deemed too expensive and we instead use
     * {@link #_orderWindowsGreedy} to find the permutation of
     * `windows` such that `windows_permutation[i]` is the window closest
     * to `slots[i]`, when we iterate through `slots` in order and never revisit
     * windows already assigned to previous slots (this is quicker but doesn't
     * necessarily minimize the total distance moved by all the windows).
     *
     * Called from {@link #positionWindows}
     *
     * @inheritparams #_orderWindowsPermutation
     * @returns {Workspace.WindowClone[]} a permutation of `clones` such
     * that `clones_permutation[i]` should be placed at position `slots[i]`.
     *
     * @see #positionWindows
     * @see POSITIONING_PERMUTATIONS_MAX
     * @see #_orderWindowsGreedy
     * @see #_orderWindowsPermutations
     */
    _orderWindowsByMotionAndStartup: function(clones, slots) {
        clones.sort(function(w1, w2) {
            return w2.metaWindow.get_stable_sequence() - w1.metaWindow.get_stable_sequence();
        });
        if (clones.length <= POSITIONING_PERMUTATIONS_MAX)
            return this._orderWindowsPermutations(clones, slots);
        else
            return this._orderWindowsGreedy(clones, slots);
    },

    /** Converts a {@link Slot} (whose elements are in fractions of the
     * {@link Workspace} dimensions) to absolute sizes/dimensions.
     *
     * @param {Workspace.Slot} slot - A layout slot definition
     * @returns {number[]} the screen-relative `[x, y, width, height]`
     * of a given window layout slot.
     */
    _getSlotGeometry: function(slot) {
        let [xCenter, yCenter, fraction] = slot;

        let width = this._width * fraction;
        let height = this._height * fraction;

        let x = this._x + xCenter * this._width - width / 2 ;
        let y = this._y + yCenter * this._height - height / 2;

        return [x, y, width, height];
    },

    /**
     * Given a window and slot to fit it in, compute its
     * screen-relative [x, y, scale] where scale applies
     * to both X and Y directions, such that the window's {@link WindowOverlay}
     * and {@link WindowClone} would fit inside.
     *
     * Used in {@link #positionWindows}.
     *
     * @param {Meta.Window} metaWindow - A metacity window to calculate dimensions
     * for (usually the 'metaWindow' property of a {@link Window.Clone}).
     * @param {Workspace.Slot} slot - A layout slot
     * @returns {number[]} `[x, y, scale]` the screen-relative x and y position
     * and a scale to apply such that `metaWindow`'s {@link WindowClone} and
     * {@link WindowOverlay} (the caption and close button stick out) would
     * fit into `slot`. It's the maximum scale that will let all of these things
     * fit.
     */
    _computeWindowLayout: function(metaWindow, slot) {
        let [x, y, width, height] = this._getSlotGeometry(slot);

        let rect = metaWindow.get_outer_rect();
        let buttonOuterHeight, captionHeight;
        let buttonOuterWidth = 0;

        if (this._windowOverlays[0]) {
            [buttonOuterHeight, captionHeight] = this._windowOverlays[0].chromeHeights();
            buttonOuterWidth = this._windowOverlays[0].chromeWidth();
        } else
            [buttonOuterHeight, captionHeight] = [0, 0];

        let scale = Math.min((width - buttonOuterWidth) / rect.width,
                             (height - buttonOuterHeight - captionHeight) / rect.height,
                             1.0);

        x = Math.floor(x + (width - scale * rect.width) / 2);

        // We want to center the window in case we have just one
        if (metaWindow.get_workspace().n_windows == 1)
            y = Math.floor(y + (height - scale * rect.height) / 2);
        else
            y = Math.floor(y + height - rect.height * scale - captionHeight);

        return [x, y, scale];
    },

    /** A reserved slot is a blank space in the {@link Workspace} - i.e.
     * a place where a {@link WindowClone} would go except there isn't one there
     * yet; it's reserved.
     *
     * This function adds or removes a reserved slot for the window clone `clone`.
     * (removes if `clone` is `null`, adds if we don't have `clone` in our
     * {@link Workspace} yet).
     *
     * Example of when this would be used - dragging a {@link WindowClone}
     * from the {@link Workspace} for one screen to the {@link Workspace} for
     * another screen. While we are still dragging the actor and hovering over
     * the second workspace, it makes an empty space of where the window would
     * be placed if the user dropped it. This is the "reserved slot".
     *
     * If the slot is added or removed we make another call to
     * {@link #positionWindows} to animate in/out the reserved slot
     * and rearrange all the other windows.
     *
     * {@link #positionWindows} is careful to leave space for the
     * reserved slot if we have set one.
     * @param {?Workspace.WindowClone} clone - the window clone to reserve
     * a slot for, or `null` to remove a reserved slot.
     * @see #positionWindows
     */
    setReservedSlot: function(clone) {
        if (this._reservedSlot == clone)
            return;

        if (clone && this.containsMetaWindow(clone.metaWindow)) {
            this._reservedSlot = null;
            this.positionWindows(WindowPositionFlags.ANIMATE);
            return;
        }
        if (clone)
            this._reservedSlot = clone;
        else
            this._reservedSlot = null;
        this.positionWindows(WindowPositionFlags.ANIMATE);
    },

    /** This function positions all the {@link WindowClone}s owned by this
     * {@link Workspace} so that the fill the screen nicely.
     *
     * This gives that nice "zoomy" effect when you open/close the overview
     * an the windows tab (i.e. {@link Workspace}(s) for the current workspace)
     * animate in.
     *
     * First we see if we have to add an empty slot for the reserved slot
     * (see {@link #setReservedSlot}).
     *
     * Then we calculate which window should go into which slot, trying to
     * minimize the amount of moving around each window has to do, using
     * {@link #_computeAllWindowSlots} and
     * {@link #_orderWindowsByMotionAndStartup}.
     *
     * Then for every {@link WindowClone} we have in {@link #_windows}, we hide
     * its {@link WindowOverlay}.
     *
     * We calculate the position and scale each clone should have using
     * {@link #_computeWindowLayout}.
     *
     * We place the window's {@link WindowClone} at this position and scale it
     * to that scale, animating if {@link WindowPositionFlags.ANIMATE} was
     * passed in to `flags` and call {@link #_showWindowOverlay}
     * to display the window overlays once we're done.
     *
     * Additionally if {@link WindowPositionFlags.INITIAL} was passed in with
     * the {@link WindowPositionFlags.ANIMATE} flag, we
     * expand the window in from a dot (grow it out to its proper size) and fade
     * it in before we move/scale it.
     *
     * The animation time is {@link Overview.ANIMATION_TIME} so that things
     * fade in at the same rate as the Overview does, meaning both animations
     * will be done as the overview is done showing.
     *
     * If we are not animating we just set the position/scale of each window
     * clone directly and show the clone's overlay.
     *
     * @param {Workspace.WindowPositionFlags} flags - `INITIAL` indicates that
     * this is the initial positioning of windows. `ANIMATE` indicates that
     * we need to animate the positioning. The flags can bitwise and/or'd
     * together if need be.
     */
    positionWindows : function(flags) {
        if (this._repositionWindowsId > 0) {
            Mainloop.source_remove(this._repositionWindowsId);
            this._repositionWindowsId = 0;
        }

        let clones = this._windows.slice();
        if (this._reservedSlot)
            clones.push(this._reservedSlot);

        let initialPositioning = flags & WindowPositionFlags.INITIAL;
        let animate = flags & WindowPositionFlags.ANIMATE;

        // Start the animations
        let slots = this._computeAllWindowSlots(clones.length);
        clones = this._orderWindowsByMotionAndStartup(clones, slots);

        let currentWorkspace = global.screen.get_active_workspace();
        let isOnCurrentWorkspace = this.metaWorkspace == null || this.metaWorkspace == currentWorkspace;

        for (let i = 0; i < clones.length; i++) {
            let slot = slots[i];
            let clone = clones[i];
            let metaWindow = clone.metaWindow;
            let mainIndex = this._lookupIndex(metaWindow);
            let overlay = this._windowOverlays[mainIndex];

            // Positioning a window currently being dragged must be avoided;
            // we'll just leave a blank spot in the layout for it.
            if (clone.inDrag)
                continue;

            let [x, y, scale] = this._computeWindowLayout(metaWindow, slot);

            if (overlay)
                overlay.hide();
            if (animate && isOnCurrentWorkspace) {
                if (!metaWindow.showing_on_its_workspace()) {
                    /* Hidden windows should fade in and grow
                     * therefore we need to resize them now so they
                     * can be scaled up later */
                     if (initialPositioning) {
                         clone.actor.opacity = 0;
                         clone.actor.scale_x = 0;
                         clone.actor.scale_y = 0;
                         clone.actor.x = x;
                         clone.actor.y = y;
                     }

                     // Make the window slightly transparent to indicate it's hidden
                     Tweener.addTween(clone.actor,
                                      { opacity: 255,
                                        time: Overview.ANIMATION_TIME,
                                        transition: 'easeInQuad'
                                      });
                }

                Tweener.addTween(clone.actor,
                                 { x: x,
                                   y: y,
                                   scale_x: scale,
                                   scale_y: scale,
                                   time: Overview.ANIMATION_TIME,
                                   transition: 'easeOutQuad',
                                   onComplete: Lang.bind(this, function() {
                                      this._showWindowOverlay(clone, overlay, true);
                                   })
                                 });
            } else {
                clone.actor.set_position(x, y);
                clone.actor.set_scale(scale, scale);
                this._showWindowOverlay(clone, overlay, isOnCurrentWorkspace);
            }
        }
    },

    /** Synchronises the {@link WindowClone}s so that their stacking order
     * represents the stacking order of their Meta.Windows.
     * 
     * (TODO - why does this matter? the slots are such that the window clones
     * don't overlap anyway?)
     *
     * @param {Object.<number, number>} stackIndices - object mapping each
     * meta window's `get_stable_sequence()` number to its index in the stacking
     * order, 0 being the lowest and `n-1` being the foremost window.
     * @see WorkspacesView.WorkspacesDisplay#_onRestacked
     * @see WorkspacesView.WorkspacesView#syncStacking
     * @see WindowClone#setStackAbove
     */
    syncStacking: function(stackIndices) {
        let clones = this._windows.slice();
        clones.sort(function (a, b) { return stackIndices[a.metaWindow.get_stable_sequence()] - stackIndices[b.metaWindow.get_stable_sequence()]; });

        for (let i = 0; i < clones.length; i++) {
            let clone = clones[i];
            let metaWindow = clone.metaWindow;
            if (i == 0) {
                clone.setStackAbove(this._dropRect);
            } else {
                let previousClone = clones[i - 1];
                clone.setStackAbove(previousClone.actor);
            }
        }
    },

    /** Shows the {@link WindowOverlay} for its {@link WindowClone}.
     *
     * This uses the clone's position to update the overlay position
     * with {@link WindowOverlay#updatePositions} and then fades in the overlay
     * (if `fade` is true) or simply shows it otherwise.
     * @param {Workspace.WindowClone} clone - window clone
     * @param {Workspace.WindowOverlay} overlay - overlay to show (should
     * be the one corresponding to `clone`...)
     * @param {boolean} fade - whether to fade in the actors or just show them.
     * @see WindowOverlay#fadeIn
     * @see WindowOverlay#show
     * @see WindowOverlay#updatePositions
     */
    _showWindowOverlay: function(clone, overlay, fade) {
        if (clone.inDrag)
            return;

        // This is a little messy and complicated because when we
        // start the fade-in we may not have done the final positioning
        // of the workspaces. (Tweener doesn't necessarily finish
        // all animations before calling onComplete callbacks.)
        // So we need to manually compute where the window will
        // be after the workspace animation finishes.
        let [cloneX, cloneY] = clone.actor.get_position();
        let [cloneWidth, cloneHeight] = clone.actor.get_size();
        cloneWidth = clone.actor.scale_x * cloneWidth;
        cloneHeight = clone.actor.scale_y * cloneHeight;

        if (overlay) {
            overlay.updatePositions(cloneX, cloneY, cloneWidth, cloneHeight);
            if (fade)
                overlay.fadeIn();
            else
                overlay.show();
        }
    },

    /** Shows the {@link WindowOverlay} for every {@link WindowClone} in
     * {@link #_windows}, fading it in if this {@link Workspace}
     * has no {@link #metaWorkspace} or if this {@link Workspace} is
     * the current workspace (if neither of these are true, we simply show
     * the overlay rather than animating).
     *
     * TODO - if {@link #metaWorkspace} is not the current workspace
     * what does it matter whether we animate in or not, won't we *not* be
     * seeing the {@link Workspace} then anyway?
     *
     * @see #_showWindowOverlay
     */
    _showAllOverlays: function() {
        let currentWorkspace = global.screen.get_active_workspace();
        for (let i = 0; i < this._windows.length; i++) {
            let clone = this._windows[i];
            let overlay = this._windowOverlays[i];
            this._showWindowOverlay(clone, overlay,
                                    this.metaWorkspace == null || this.metaWorkspace == currentWorkspace);
        }
    },

    /** Potentially repositions windows. This is a callback for a 
     * `Mainloop.timeout_add`.
     *
     * It is used to delay repositioning of windows until the pointer has
     * remained still for at least 750ms (or has moved out of this {@link Workspace}).
     *
     * This is used in
     * {@link #_doRemoveWindow} to not reposition all the window clones
     * if the user is currently interacting with the {@link Workspace} (for
     * example they are currently zooming in on a {@link WindowClone} or are
     * otherwise moving their mouse around). Instead, we wait until the user
     * has not moved their pointer for at least 750ms before doing the repositioning
     * to prevent things that the user is trying to interact with from moving
     * around annoyingly.
     *
     * @returns {boolean} `true` if the pointer has moved since our last check
     * but we are still within the {@link Workspace} (hence re-adding this
     * timeout back to Mainloop), or `false` if the pointer has not moved
     * since the last time this function was called (in which case windows
     * are repositioned).
     * @see #_doRemoveWindow
     */
    _delayedWindowRepositioning: function() {
        if (this._windowIsZooming)
            return true;

        let [x, y, mask] = global.get_pointer();

        let pointerHasMoved = (this._cursorX != x && this._cursorY != y);
        let inWorkspace = (this._x < x && x < this._x + this._width &&
                           this._y < y && y < this._y + this._height);

        if (pointerHasMoved && inWorkspace) {
            // store current cursor position
            this._cursorX = x;
            this._cursorY = y;
            return true;
        }

        this.positionWindows(WindowPositionFlags.ANIMATE);
        return false;
    },

    /** Public function, shows all window overlays (unless we are currently
     * leaving the overview).
     * @see #_showAllOverlays */
    showWindowsOverlays: function() {
        if (this.leavingOverview)
            return;

        this._windowOverlaysGroup.show();
        this._showAllOverlays();
    },

    /** Public function, hides all window overlays. */
    hideWindowsOverlays: function() {
        this._windowOverlaysGroup.hide();
    },

    /** Potentially removes one of our window clones from us.
     * Called (for example) when a 'window-removed' signal is caught from
     * {@link #metaWorkspace}.
     *
     * If we don't currently manage have a clone for this window or the window
     * isn't on our workspace/screen, we don't do anything.
     *
     * Otherwise (we have a {@link WindowClone} for the window and it is
     * our window), the window clone and overlay are removed from
     * {@link #_windows} and {@link #_windowOverlays}.
     *
     * If the window is just being moved to another {@link Workspace} as opposed to
     * actually being destroyed, we set its `_overviewHint` accordingly (this
     * stores the current x and y coordinates and scale of the window
     * clone (relative to the stage) so that when it is added to the
     * new {@link Workspace} it is given the slot/scale closest to its
     * old one if possible ??).
     *
     * We then destroy our {@link WindowClone} for the window and queue a
     * window repositioning once the user has stopped interacting with the
     * {@link Workspace} (we don't want to make all the windows move around
     * if the user is currently trying to pick one or zoome into it! So instead
     * we wait until the pointer has stayed still for at least 750ms before
     * moving all the windows around, see {@link #_delayedWindowRepositioning}).
     *
     * @param {Meta.Window} metaWin - the metacity window
     */
    _doRemoveWindow : function(metaWin) {
        let win = metaWin.get_compositor_private();

        // find the position of the window in our list
        let index = this._lookupIndex (metaWin);

        if (index == -1)
            return;

        // Check if window still should be here
        if (win && this._isMyWindow(win))
            return;

        let clone = this._windows[index];

        this._windows.splice(index, 1);
        this._windowOverlays.splice(index, 1);

        // If metaWin.get_compositor_private() returned non-NULL, that
        // means the window still exists (and is just being moved to
        // another workspace or something), so set its overviewHint
        // accordingly. (If it returned NULL, then the window is being
        // destroyed; we'd like to animate this, but it's too late at
        // this point.)
        if (win) {
            let [stageX, stageY] = clone.actor.get_transformed_position();
            let [stageWidth, stageHeight] = clone.actor.get_transformed_size();
            win._overviewHint = {
                x: stageX,
                y: stageY,
                scale: stageWidth / clone.actor.width
            };
        }
        clone.destroy();


        // We need to reposition the windows; to avoid shuffling windows
        // around while the user is interacting with the workspace, we delay
        // the positioning until the pointer remains still for at least 750 ms
        // or is moved outside the workspace

        // remove old handler
        if (this._repositionWindowsId > 0) {
            Mainloop.source_remove(this._repositionWindowsId);
            this._repositionWindowsId = 0;
        }

        // setup new handler
        let [x, y, mask] = global.get_pointer();
        this._cursorX = x;
        this._cursorY = y;

        this._repositionWindowsId = Mainloop.timeout_add(750,
            Lang.bind(this, this._delayedWindowRepositioning));
    },

    /** Potentially adds a new {@link WindowClone} for a new window to us.
     *
     * Called (for example) when a 'window-added' signal is caught from
     * {@link #metaWorkspace}.
     *
     * If we already have a clone for this window or the window
     * isn't on our workspace/screen, we don't do anything.
     *
     * Otherwise (we don't have a {@link WindowClone} for the window and it is
     * a window we should be managing), we call {@link #_addWindowClone}
     * to add a clone for this window.
     *
     * If the window has a `_overviewHint` property (being an object with its
     * current x and y stage coordinates and the current scale of the actor
     * relevant to the global stage), we make sure that our window clone has
     * the same x, y and scale (I think that this is so that when you drag
     * a {@link WindowClone} from one {@link Workspace} to another,
     * it appears on the other {@link Workspace} as close to that original
     * scale and size as it can).
     *
     * We then immediately call {@link #positionWindows} (why don't
     * we do a delayed repositioning like on {@link #_doRemoveWindows}?)
     *
     * @param {Meta.Window} metaWin - the metacity window
     */
    _doAddWindow : function(metaWin) {
        if (this.leavingOverview)
            return;

        let win = metaWin.get_compositor_private();

        if (!win) {
            // Newly-created windows are added to a workspace before
            // the compositor finds out about them...
            Mainloop.idle_add(Lang.bind(this,
                                        function () {
                                            if (this.actor &&
                                                metaWin.get_compositor_private() &&
                                                metaWin.get_workspace() == this.metaWorkspace)
                                                this._doAddWindow(metaWin);
                                            return false;
                                        }));
            return;
        }

        // We might have the window in our list already if it was on all workspaces and
        // now was moved to this workspace
        if (this._lookupIndex (metaWin) != -1)
            return;

        if (!this._isMyWindow(win) || !this._isOverviewWindow(win))
            return;

        let clone = this._addWindowClone(win);

        if (win._overviewHint) {
            let x = win._overviewHint.x - this.actor.x;
            let y = win._overviewHint.y - this.actor.y;
            let scale = win._overviewHint.scale;
            delete win._overviewHint;

            clone.actor.set_position (x, y);
            clone.actor.set_scale (scale, scale);
        } else {
            // Position new windows at the top corner of the workspace rather
            // than where they were placed for real to avoid the window
            // being clipped to the workspaceView. Its not really more
            // natural for the window to suddenly appear in the overview
            // on some seemingly random location anyway.
            clone.actor.set_position (this._x, this._y);
        }

        this.positionWindows(WindowPositionFlags.ANIMATE);
    },

    /** Callback for the 'window-added' signal of {@link #metaWorkspace}.
     *
     * Calls {@link #_doAddWindow}
     * @see #_doAddWindow
     */
    _windowAdded : function(metaWorkspace, metaWin) {
        this._doAddWindow(metaWin);
    },

    /** Callback for the 'window-removed' signal of {@link #metaWorkspace}.
     *
     * Calls {@link #_doRemoveWindow}
     * @see #_doRemoveWindow
     */
    _windowRemoved : function(metaWorkspace, metaWin) {
        this._doRemoveWindow(metaWin);
    },

    /** Callback for the 'window-entered-monitor' signal of
     * the screen at index {@link #monitorIndex}.
     *
     * Calls {@link #_doAddWindow}
     * @see #_doAddWindow
     */
    _windowEnteredMonitor : function(metaScreen, monitorIndex, metaWin) {
        if (monitorIndex == this.monitorIndex) {
            this._doAddWindow(metaWin);
        }
    },

    /** Callback for the 'window-left-monitor' signal of
     * the screen at index {@link #monitorIndex}.
     *
     * Calls {@link #_doRemoveWindow}
     * @see #_doRemoveWindow
     */
    _windowLeftMonitor : function(metaScreen, monitorIndex, metaWin) {
        if (monitorIndex == this.monitorIndex) {
            this._doRemoveWindow(metaWin);
        }
    },

    /** Check for maximized windows on this {@link Workspace}.
     * @returns {boolean} whether any of the windows managed by this
     * workspace is maximized both horizontally and vertically. */
    hasMaximizedWindows: function() {
        for (let i = 0; i < this._windows.length; i++) {
            let metaWindow = this._windows[i].metaWindow;
            if (metaWindow.showing_on_its_workspace() &&
                metaWindow.maximized_horizontally &&
                metaWindow.maximized_vertically)
                return true;
        }
        return false;
    },

    /** Animates the full-screen to Overview transition (when we show the
     * Overview).
     *
     * We call {@link #positionWindows} with the
     * [INITIAL flag]{@link WindowPositionFlags.INITIAL}, and also with
     * the [ANIMATE flag]{@link WindowPositionFlags.ANIMATE} if the overview
     * is currently being animated. */
    zoomToOverview : function() {
        // Position and scale the windows.
        if (Main.overview.animationInProgress)
            this.positionWindows(WindowPositionFlags.ANIMATE | WindowPositionFlags.INITIAL);
        else
            this.positionWindows(WindowPositionFlags.INITIAL);
    },

    /** Animates the return from Overview mode (when we hide the
     * Overview).
     *
     * We set {@link #leavingOverview} to `true` and hide all the
     * window overlays with {@link #hideWindowOverlays}.
     *
     * We connect to the 'hidden' signal of the overview to call
     * {@link #_doneLeavingOverview} when the overview finishes hiding
     * itself.
     *
     * We also animate scaling and moving each window clone back up to the
     * original window's size/position.
     *
     * The animation happens over time {@link Overview.ANIMATION_TIME} so
     * that we are in sync with the overview fading.
     */
    zoomFromOverview : function() {
        let currentWorkspace = global.screen.get_active_workspace();

        this.leavingOverview = true;

        this.hideWindowsOverlays();

        if (this._repositionWindowsId > 0) {
            Mainloop.source_remove(this._repositionWindowsId);
            this._repositionWindowsId = 0;
        }
        this._overviewHiddenId = Main.overview.connect('hidden', Lang.bind(this,
                                                                           this._doneLeavingOverview));

        if (this.metaWorkspace != null && this.metaWorkspace != currentWorkspace)
            return;

        // Position and scale the windows.
        for (let i = 0; i < this._windows.length; i++) {
            let clone = this._windows[i];

            clone.zoomFromOverview();

            if (clone.metaWindow.showing_on_its_workspace()) {
                Tweener.addTween(clone.actor,
                                 { x: clone.origX,
                                   y: clone.origY,
                                   scale_x: 1.0,
                                   scale_y: 1.0,
                                   time: Overview.ANIMATION_TIME,
                                   opacity: 255,
                                   transition: 'easeOutQuad'
                                 });
            } else {
                // The window is hidden, make it shrink and fade it out
                Tweener.addTween(clone.actor,
                                 { scale_x: 0,
                                   scale_y: 0,
                                   opacity: 0,
                                   time: Overview.ANIMATION_TIME,
                                   transition: 'easeOutQuad'
                                 });
            }
        }

    },

    /** Destroys the Workspace. This destroys {@link #actor} which
     * triggers {@link #_onDestroy}. */
    destroy : function() {
        this.actor.destroy();
    },

    /** Callback when {@link #actor} is destroyed, for example from
     * {@link #destroy}.
     *
     * This disconnects all our outstanding signals/timeouts, and destroys
     * each {@link WindowClone}.
     *
     * It also removes any tweens in progress.
     * @see WindowClone#destroy
     */
    _onDestroy: function(actor) {
        if (this._overviewHiddenId) {
            Main.overview.disconnect(this._overviewHiddenId);
            this._overviewHiddenId = 0;
        }
        Tweener.removeTweens(actor);

        if (this.metaWorkspace) {
            this.metaWorkspace.disconnect(this._windowAddedId);
            this.metaWorkspace.disconnect(this._windowRemovedId);
        }
        global.screen.disconnect(this._windowEnteredMonitorId);
        global.screen.disconnect(this._windowLeftMonitorId);

        if (this._repositionWindowsId > 0)
            Mainloop.source_remove(this._repositionWindowsId);

        // Usually, the windows will be destroyed automatically with
        // their parent (this.actor), but we might have a zoomed window
        // which has been reparented to the stage - _windows[0] holds
        // the desktop window, which is never reparented
        for (let w = 0; w < this._windows.length; w++)
            this._windows[w].destroy();
        this._windows = [];
    },

    /** Callback on the overview's 'hidden' signal.
     * Sets [`this.leavingOverview`]{@link #leavinOverview} flag to false.
     */
    _doneLeavingOverview : function() {
        this.leavingOverview = false;
    },

    /** Tests if `win` belongs to this workspaces and monitor.
     * @param {Meta.WindowActor} win - window actor to test.
     * @returns {boolean} true if `win` passes the test.
     */
    _isMyWindow : function (win) {
        return (this.metaWorkspace == null || Main.isWindowActorDisplayedOnWorkspace(win, this.metaWorkspace.index())) &&
            (!win.get_meta_window() || win.get_meta_window().get_monitor() == this.monitorIndex);
    },

    /** Tests if `win` should be shown in the Overview by calling Shell.Tracker's
     * `is_window_interesting` method on the window. For example, modal dialogs
     * are not interesting and are hence not shown.
     * @inheritparams #_isMyWindow
     */
    _isOverviewWindow : function (win) {
        let tracker = Shell.WindowTracker.get_default();
        return tracker.is_window_interesting(win.get_meta_window());
    },

    /** Create a clone of a (non-desktop) window and add it to the window list
     *
     * This creates a {@link WindowClone} for `win` and a {@link WindowOverlay}
     * for the clone with parent actor {@link #_windowOverlaysGroup}.
     *
     * The clone is added to {@link #_windows} and its actor
     * to {@link #actor}, and the overlay is added
     * to {@link #_windowOverlays}.
     *
     * We also connect to a bunch of the clone and overlay's signals:
     *
     * * clone's ['selected' signal]{@link WindowClone.selected} - call
     * {@link #_onCloneSelected}
     * * clone's ['drag-begin' signal]{@link WindowClone.drag-begin} - call
     * [Main.overview.beginWindowDrag]{@link Overview.Overview#beginWindowDrag}
     * and hide the clone's WindowOverlay.
     * * clone's ['drag-cancelled' signal]{@link WindowClone.drag-cancelled} - call
     * [Main.overview.cancelledWindowDrag]{@link Overview.Overview#cancelledWindowDrag}.
     * * clone's ['drag-end' signal]{@link WindowClone.drag-end} - call
     * [Main.overview.endWindowDrag]{@link Overview.Overview#endWindowDrag}
     * and show the clone's WindowOverlay.
     * * clone's ['zoom-start']{@link WindowClone.zoom-start} 
     * and ['zoom-end']{@link WindowClone.zoom-end} signals - make
     * a note that a window is being zoomed (we avoid repositioning due to
     * a window being removed if the user is currently zooming)
     * * clone's ['size-changed']{@link WindowClone.size-changed} signal -
     * reposition the windows in response (with no animation).
     * * overlay's ['show-close-button'{@link WindowOverlay.show-close-button} signal -
     * conect up to {@link #_onShowOverlayClose}.
     *
     * @param {Meta.WindowActor} win - window actor to create a clone for.
     * @returns {Workspace.WindowClone} the created clone
     */
    _addWindowClone : function(win) {
        let clone = new WindowClone(win);
        let overlay = new WindowOverlay(clone, this._windowOverlaysGroup);

        clone.connect('selected',
                      Lang.bind(this, this._onCloneSelected));
        clone.connect('drag-begin',
                      Lang.bind(this, function(clone) {
                          Main.overview.beginWindowDrag();
                          overlay.hide();
                      }));
        clone.connect('drag-cancelled',
                      Lang.bind(this, function(clone) {
                          Main.overview.cancelledWindowDrag();
                      }));
        clone.connect('drag-end',
                      Lang.bind(this, function(clone) {
                          Main.overview.endWindowDrag();
                          overlay.show();
                      }));
        clone.connect('zoom-start',
                      Lang.bind(this, function() {
                          this._windowIsZooming = true;
                      }));
        clone.connect('zoom-end',
                      Lang.bind(this, function() {
                          this._windowIsZooming = false;
                      }));
        clone.connect('size-changed',
                      Lang.bind(this, function() {
                          this.positionWindows(0);
                      }));

        this.actor.add_actor(clone.actor);

        overlay.connect('show-close-button', Lang.bind(this, this._onShowOverlayClose));

        this._windows.push(clone);
        this._windowOverlays.push(overlay);

        return clone;
    },

    /** Callback when any window overlay's
     * ['show-close-button']{@link WindowOverlay.show-close-button} signal is 
     * emitted.
     *
     * We immediately hide the close buttons of all other window overlays, bypassing
     * their hide button timeouts.
     * @param {Workspace.WindowOverlay} windowOverlay - overlay that emitted
     * the signal.
     */
    _onShowOverlayClose: function (windowOverlay) {
        for (let i = 0; i < this._windowOverlays.length; i++) {
            let overlay = this._windowOverlays[i];
            if (overlay == windowOverlay)
                continue;
            overlay.hideCloseButton();
        }
    },

    /** Compute a {@link Slot} (i.e. size and scale) for a window clone given
     * the total number of windows.
     *
     * If `numberOfWindows` has a pre-calculated layout in {@link POSITIONS},
     * this is used to get the relevant slot.
     *
     * Otherwise we arrange the windows in a grid pattern with
     * `ceil(sqrt(numberOfWindows))` columns and rows.
     *
     * @param {number} windowIndex - index of the window (used to position
     * it in the grid or look up its {@link Slot} in `POSITIONS[numberOfWindows]`)
     * @param {number} numberOfWindows - total number of windows we have to
     * fit in the layout
     * @returns {Workspace.Slot}
     * [`POSITIONS[numberOfWindows][windowIndex]`]{@link POSITIONS} if this
     * exists, or the {@link Slot} corresponding to a cell in the grid otherwise.
     */
    _computeWindowSlot : function(windowIndex, numberOfWindows) {
        if (numberOfWindows in POSITIONS)
            return POSITIONS[numberOfWindows][windowIndex];

        // If we don't have a predefined scheme for this window count,
        // arrange the windows in a grid pattern.
        let gridWidth = Math.ceil(Math.sqrt(numberOfWindows));
        let gridHeight = Math.ceil(numberOfWindows / gridWidth);

        let fraction = 0.95 * (1. / gridWidth);

        let xCenter = (.5 / gridWidth) + ((windowIndex) % gridWidth) / gridWidth;
        let yCenter = (.5 / gridHeight) + Math.floor((windowIndex / gridWidth)) / gridHeight;

        return [xCenter, yCenter, fraction];
    },

    /** Computes a set of {@link Slot}s for each window.
     * @inheritparams #_computeWindowSlot
     * @returns {Workspace.Slot[]} an array of slots, one per window. Each
     * slot is guaranteed not to overlap any of the others.
     */
    _computeAllWindowSlots: function(totalWindows) {
        let slots = [];
        for (let i = 0; i < totalWindows; i++) {
            slots.push(this._computeWindowSlot(i, totalWindows));
        }
        return slots;
    },

    /** Callback when a {@link WindowClone} emits a 'selected' signal.
     *
     * This activates the window for that clone the workspace matching ours.
     *
     * (This will cause the overview to hide triggering our hide animation).
     * @see Main.activateWindow
     * @see WindowClone.selected
     * @inheritparams WindowClone.selected
     */
    _onCloneSelected : function (clone, time) {
        let wsIndex = undefined;
        if (this.metaWorkspace)
            wsIndex = this.metaWorkspace.index();
        Main.activateWindow(clone.metaWindow, time, wsIndex);
    },

    // Draggable target interface
    /** Implements the 'handleDragOver' function (for {@link #_dropRect})
     * meaning the {@link Workspace} knows how to accept drag/drops.
     * @returns {DND.DragMotionResult} `MOVE_DROP` if the window is not ours
     * and it is a window clone; `COPY_DROP` if the item has a `shellWorkspaceLaunch`
     * property (for example {@link SearchDisplay.SearchResult}s and
     * {@link AppDisplay.AppWellIcon}s do), indicating that the application represented
     * by the draggable will be launched if dropped on this {@link Workspace}.
     * @todo link this to Draggable and inherit params
     */
    handleDragOver : function(source, actor, x, y, time) {
        if (source.realWindow && !this._isMyWindow(source.realWindow))
            return DND.DragMotionResult.MOVE_DROP;
        if (source.shellWorkspaceLaunch)
            return DND.DragMotionResult.COPY_DROP;

        return DND.DragMotionResult.CONTINUE;
    },

    /** Implements the 'acceptDrop' function (for {@link #_dropRect})
     * indicating that this {@link Workspace} can handle drops from drag and drop.
     *
     * If the item being dropped is a {@link Window.Clone} but we already have it
     * in our {@link Workspace}, we do nothing (return `false`).
     *
     * If it is a {@link Window.Clone} and is *not* managed by our {@link Workspace},
     * we move its window from its old monitor/workspace to ours, which will
     * trigger a 'window-added' event which will add the window clone.
     *
     * If instead the draggable has a `shellWorkspaceLaunch` function
     * (e.g. {@link SearchDisplay.SearchResult}s and
     * {@link AppDisplay.AppWellIcon}s do), we call this function with
     * {@link #metaWorkspace}'s index (or -1 if we have no workspace)
     * and the timestamp of `time`. This typically launches the search
     * result (or the application if it is an AppWellIcon) on that workspace.
     *
     * @see AppDisplay.AppWellIcon#shellWorkspaceLaunch
     * @see SearchDisplay.SearchResult#shellWorkspaceLaunch
     * 
     * @todo link this to Draggable and inherit params
     */
    acceptDrop : function(source, actor, x, y, time) {
        if (source.realWindow) {
            let win = source.realWindow;
            if (this._isMyWindow(win))
                return false;

            // Set a hint on the Mutter.Window so its initial position
            // in the new workspace will be correct
            win._overviewHint = {
                x: actor.x,
                y: actor.y,
                scale: actor.scale_x
            };

            let metaWindow = win.get_meta_window();

            // We need to move the window before changing the workspace, because
            // the move itself could cause a workspace change if the window enters
            // the primary monitor
            if (metaWindow.get_monitor() != this.monitorIndex)
                metaWindow.move_to_monitor(this.monitorIndex);

            let index = this.metaWorkspace ? this.metaWorkspace.index() : global.screen.get_active_workspace_index();
            metaWindow.change_workspace_by_index(index,
                                                 false, // don't create workspace
                                                 time);
            return true;
        } else if (source.shellWorkspaceLaunch) {
            source.shellWorkspaceLaunch({ workspace: this.metaWorkspace ? this.metaWorkspace.index() : -1,
                                          timestamp: time });
            return true;
        }

        return false;
    }
};

Signals.addSignalMethods(Workspace.prototype);
