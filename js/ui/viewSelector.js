// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * The main part of the Overview - defining a notebook/tabs model.
 * For example the 'Applications' and 'Windows' sections of the Overview are
 * 'tabs' within the notebook.
 */

const Clutter = imports.gi.Clutter;
const Gtk = imports.gi.Gtk;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Signals = imports.signals;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Main = imports.ui.main;
const Search = imports.ui.search;
const SearchDisplay = imports.ui.searchDisplay;
const ShellEntry = imports.ui.shellEntry;
const Tweener = imports.ui.tweener;

/** creates a new BaseTab.
 *
 * This stores `titleActor` in {@link #title}.
 *
 * We create a `St.Bin` with style class 'view-tab-page' and child
 * `pageActor`, storing this in {@link #page}.
 *
 * Then the tab is added to the [Ctrl Alt Tab Manager]{@link CtrlAltTab.CtrlAltTabManager}
 * with its [`addGroup`]{@link CtrlAltTab.CtrlAltTabManager#addGroup}. method.
 *
 * The `root` argument (i.e. what gets given focus when the item is selected from
 * the ctrl alt tab popup) is either `titleActor` *if* `titleActor.can_focus` is
 * true (basically for `St.Entry`s, as with the search tab).
 *
 * Otherwise {@link #page} is added as `root` into the ctrl alt tab manager and
 * `titleActor` is used as its proxy with {@link #_a11yFocus} as the
 * focus callback (activates the tab, emitting the 'activated' signal, and
 * gives {@link #page} keyboard focus).
 * @see CtrlAltTab.CtrlAltTabManager#addGroup
 * @see CtrlAltTab.CtrlAltTabItem
 * @param {Clutter.Actor} titleActor - the actor that will represent the tab
 * (always visible even if you are not on this tab, click on it to switch to
 * that tab). Example - a St.Button.
 * @param {Clutter.Actor} pageActor - the actor that represents the tab *contents*,
 * for example a {@link AppDisplay.AllAppDisplay} for the applications tab or
 * a {@link SearchDisplay.SearchResults} for the search tab.
 * @param {string} name - the name of the tab. Used only in the
 * [Ctrl Alt Tab manager]{@link CtrlAltTab.CtrlAltTabManager}
 * @param {string} a11yIcon - name of the icon to use for the item in the
 * [Ctrl Alt Tab manager]{@link CtrlAltTab.CtrlAltTabManager}.
 * @classdesc
 * Base class for a tab (a page in the overview, e.g. 'Windows' and
 * 'Applications'). Requires an actor defining its title (being the thing you
 * click on to switch to that tab) and an actor defining the tab contents.
 *
 * ![ViewSelector in red; the "Applications" tab is showing](pics/ViewSelector.png)
 *
 * A tab has a title actor (the "Applications" button for the applications tab
 * above) and a page actor (a {@link AppDisplay.AllAppDisplay}, being the entire
 * content of the tab with the applications grid and categories).
 *
 * All the Overview tabs are {@link BaseTab}s, even the search tab (where
 * the title actor is the search box and the page actor is the
 * {@link SearchDisplay.SearchResults}).
 *
 * Any {@link BaseTab} is automatically added to the
 * [Ctrl Alt Tab Manager]{@link CtrlAltTab.CtrlAltTabManager}. If the
 * title actor can be focused it is used as the `root` argument (for example
 * the search tab has title being a St.Entry so choosing the search tab in
 * the ctrl alt tab manager will give focus to the entry). Otherwise, the
 * page actor is used instead. The icon in the ctrl alt tab manager is provided
 * in the constructor.
 * @class
 */
function BaseTab(titleActor, pageActor, name, a11yIcon) {
    this._init(titleActor, pageActor, name, a11yIcon);
}

BaseTab.prototype = {
    _init: function(titleActor, pageActor, name, a11yIcon) {
        /** The actor that is the tab's title (e.g. St.Button for the applications
         * and windows tabs in the Overview, a St.Entry for the search tab (the
         * search box)).
         * @type {Clutter.Actor} */
        this.title = titleActor;
        /** A St.Bin holding the `pageActor`. Style class 'view-tab-page'.
         * @type {St.Bin} */
        this.page = new St.Bin({ child: pageActor,
                                 x_align: St.Align.START,
                                 y_align: St.Align.START,
                                 x_fill: true,
                                 y_fill: true,
                                 style_class: 'view-tab-page' });

        if (this.title.can_focus) {
            Main.ctrlAltTabManager.addGroup(this.title, name, a11yIcon);
        } else {
            Main.ctrlAltTabManager.addGroup(this.page, name, a11yIcon,
                                            { proxy: this.title,
                                              focusCallback: Lang.bind(this, this._a11yFocus) });
        }

        /** Whether the tab is visible. Read-only in the sense that setting this
         * property does nothing.
         * @type {boolean} */
        this.visible = false;
    },

    /** Shows this tab.
     * If `animate` is true, the page will fade in (opacity 0 to 255) over
     * 0.1 seconds as opposed to simply appearing.
     * @param {boolean} animate - whether to animate
     */
    show: function(animate) {
        this.visible = true;
        this.page.show();

        if (!animate)
            return;

        this.page.opacity = 0;
        Tweener.addTween(this.page,
                         { opacity: 255,
                           time: 0.1,
                           transition: 'easeOutQuad' });
    },

    /** Hides this tab.
     * It is faded out (opacity 255 to 0) over 0.1 seconds (so whereas in the
     * {@link #show} method you get a choice to animate or not, in this
     * method animation occurs and you can't choose to not animate.
     */
    hide: function() {
        this.visible = false;
        Tweener.addTween(this.page,
                         { opacity: 0,
                           time: 0.1,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this,
                               function() {
                                   this.page.hide();
                               })
                         });
    },

    /** Callback when this tab is navigated to using the
     * [ctrl alt tab manager]{@link CtrlAltTab.CtrlAltTabManager}, giving it
     * keyboard focus. (Only if {@link #page} is in the manager, not
     * {@link #title}).
     *
     * This calls {@link #_activate}, firing the 'activated'
     * signal, and passes keyboard focus to
     * {@link #page}.
     * @see #_activate
     */
    _a11yFocus: function() {
        this._activate();
        this.page.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
    },

    /** Emits the 'activated' signal. Called when the user selects this tab
     * from the ctrl alt tab popup and focus is given to the tab (as opposed to
     * the tab title). Also called when the user clicks on the tab title, if this
     * is a {@link ViewTab} (the {@link BaseTab} doesn't have the click-activate
     * code).
     * .
     *
     * The {@link ViewSelector} catches this signal an ensures that this tab is
     * switched to.
     * @fires .activated */
    _activate: function() {
        this.emit('activated');
    }
};
Signals.addSignalMethods(BaseTab.prototype);
/** Emitted when the user selects this tab from the
 * [ctrl alt tab popup]{@link CtrlAltTab.CtrlAltTabPopup} or.
 *
 * The {@link ViewSelector} listens to this in order to switch to this tab so
 * that is visible for the user to keyboard-navigate through.
 * @name activated
 * @event
 * @memberof BaseTab
 */

/** creates a new ViewTab
 * This creates a `St.Button` with style class 'view-tab-title' and puts the
 * text `label` in it.
 *
 * It also connects the button's 'clicked' signal to inherited function
 * {@link #_activate}, emitting the 'activated' signal.
 *
 * Then we call the [parent constructor]{@link BaseTab} with the St.Button
 * as the title actor, the input `pageActor`, `label`, and `a11yIcon`.
 *
 * @param {string} id - an ID for the tab (feed it in to {@link ViewSelector#switchTab}
 * in order to switch to this tab).
 * @param {string} label - the text that will appear on the tab's title.
 * @inheritparams BaseTab
 * @classdesc
 * Convenience class - extends {@link BaseTab} where the title actor is just a
 * `St.Button` with the specified text in it.
 *
 * The 'windows' and 'applications' tabs are examples of this.
 * @class
 * @extends BaseTab
 */
function ViewTab(id, label, pageActor, a11yIcon) {
    this._init(id, label, pageActor, a11yIcon);
}

ViewTab.prototype = {
    __proto__: BaseTab.prototype,

    _init: function(id, label, pageActor, a11yIcon) {
        /** ID for the tab. Used in {@link ViewSelector#switchTab} to switch
         * to this tab.
         * @type {string} */
        this.id = id;

        let titleActor = new St.Button({ label: label,
                                         style_class: 'view-tab-title' });
        titleActor.connect('clicked', Lang.bind(this, this._activate));

        BaseTab.prototype._init.call(this, titleActor, pageActor, label, a11yIcon);
    }
};


/** creates a new SearchTab
 *
 * This creates a new {@link Search.SearchSystem} and {@link Search.OpenSearchSystem}
 * to power the searches ({@link #_searchSystem},
 * {@link #_openSearchSystem}).
 *
 * Then we create a `St.Entry` with hint text "Type to search..."
 * ({@link #_entry}). This will serve as the tab's title. We also
 * add a context menu to it with {@link ShellEntry.addContextMenu}.
 *
 * We also create two icons, an inactive one to show when there's no text in
 * the entry ('edit-find', ![edit-find](pics/icons/edit-find-symbolic.svg),
 * {@link #_inactiveIcon})
 * and an active one when there is text in the entry
 * ('edit-clear', ![edit-clear](pics/icons/edit-clear-symbolic.svg),
 * {@link #_activeIcon}).
 *
 * Then we create a {@link SearchDisplay.SearchResults} to display all the
 * results in ({@link #_searchResults}).
 *
 * Then we call the [parent (Base Tab) constructor]{@link BaseTab} with
 * {@link #_entry} as the title actor and
 * `this._searchResults.actor` as the page actor.
 *
 * Finally, we connect the entry box up to events to change its icon,
 * and also such that pressing Enter in the entry performs a search and activates
 * the first on found.
 * @classdesc
 * ![Search Tab, title actor is an entry box](pics/SearchTab.png)
 *
 * Extends {@link BaseTab} to create the search 'tab' in the overview.
 *
 * The title actor is a St.Entry (the search box in the overview), and the page actor
 * is a {@link SearchDisplay.SearchResults} (recall that this displays a
 * {@link SearchDisplay.GridSearchResults} per provider).
 *
 * Use [`addSearchProvider`]{@link #addSearchProvider} and
 * [`removeSearchProvider`]{@link #removeSearchProvider} to add/remove
 * providers to the underlying search system and search results (however if you
 * want to add/remove search providers to the search tab in the overview, you
 * should use [`Main.overview.addSearchProvider`]{@link Overview.Overview#addSearchProvider}
 * and [`Main.overview.removeSearchProvider`]{@link Overview.Overview#removeSearchProvider},
 * which pass the commands down to the search system in the search tab).
 * @class
 * @extends BaseTab
 */
function SearchTab() {
    this._init();
}

SearchTab.prototype = {
    __proto__: BaseTab.prototype,

    _init: function() {
        /** Whether the search entry box has text in it (i.e. search tab title
         * is active).
         * @type {boolean} */
        this.active = false;
        this._searchPending = false;
        this._searchTimeoutId = 0;

        /** search system instance.
         * @see #addSearchProvider
         * @type {Search.SearchSystem} */
        this._searchSystem = new Search.SearchSystem();
        /** search system instance.
         * @type {Search.OpenSearchSystem} */
        this._openSearchSystem = new Search.OpenSearchSystem();

        /** The tab title being the search box in the overview.
         * ![title of the search tab, a search box](pics/SearchEntry.png)
         * @type {St.Entry} */
        this._entry = new St.Entry({ name: 'searchEntry',
                                     /* Translators: this is the text displayed
                                        in the search entry when no search is
                                        active; it should not exceed ~30
                                        characters. */
                                     hint_text: _("Type to search..."),
                                     track_hover: true,
                                     can_focus: true });
        ShellEntry.addContextMenu(this._entry);
        /** Convenience alias for `this._entry.clutter_text`.
         * The Clutter.Text backing the St.Entry.
         * @type {Clutter.Text} */
        this._text = this._entry.clutter_text;
        this._text.connect('key-press-event', Lang.bind(this, this._onKeyPress));

        /** The icon shown in the entry when there is no text in it.
         * ![edit-find](pics/icons/edit-find-symbolic.svg)
         * @see #_activeIcon
         * @type {St.Icon} */
        this._inactiveIcon = new St.Icon({ style_class: 'search-entry-icon',
                                           icon_name: 'edit-find',
                                           icon_type: St.IconType.SYMBOLIC });
        /** The icon shown in the entry when there is text in it.
         * ![edit-clear](pics/icons/edit-clear-symbolic.svg)
         * @see #_inactiveIcon
         * @type {St.Icon} */
        this._activeIcon = new St.Icon({ style_class: 'search-entry-icon',
                                         icon_name: 'edit-clear',
                                         icon_type: St.IconType.SYMBOLIC });
        this._entry.set_secondary_icon(this._inactiveIcon);

        this._iconClickedId = 0;

        /** The Search Results to display all the results from the search systems.
         * Used as the page actor.
         * @type {SearchDisplay.SearchResults} */
        this._searchResults = new SearchDisplay.SearchResults(this._searchSystem, this._openSearchSystem);
        BaseTab.prototype._init.call(this,
                                     this._entry,
                                     this._searchResults.actor,
                                     _("Search"),
                                     'edit-find');

        this._text.connect('text-changed', Lang.bind(this, this._onTextChanged));
        this._text.connect('key-press-event', Lang.bind(this, function (o, e) {
            // We can't connect to 'activate' here because search providers
            // might want to do something with the modifiers in activateSelected.
            let symbol = e.get_key_symbol();
            if (symbol == Clutter.Return || symbol == Clutter.KP_Enter) {
                if (this._searchTimeoutId > 0) {
                    Mainloop.source_remove(this._searchTimeoutId);
                    this._doSearch();
                }
                this._searchResults.activateSelected();
                return true;
            }
            return false;
        }));

        this._entry.connect('notify::mapped', Lang.bind(this, this._onMapped));

        global.stage.connect('notify::key-focus', Lang.bind(this, this._updateCursorVisibility));

        this._capturedEventId = 0;
    },

    /** Extend the parent function, called when this tab is hidden.
     * We first call the parent function and then call {@link #_reset}
     * to reset the key focus from the entry and clear the text (if any).
     * @override
     */
    hide: function() {
        BaseTab.prototype.hide.call(this);

        // Leave the entry focused when it doesn't have any text;
        // when replacing a selected search term, Clutter emits
        // two 'text-changed' signals, one for deleting the previous
        // text and one for the new one - the second one is handled
        // incorrectly when we remove focus
        // (https://bugzilla.gnome.org/show_bug.cgi?id=636341) */
        if (this._text.text != '')
            this._reset();
    },

    /** Resets the search box by clearing its text and removing key focus from
     * it. */
    _reset: function () {
        this._text.text = '';

        global.stage.set_key_focus(null);

        this._text.set_cursor_visible(true);
        this._text.set_selection(0, 0);
    },

    /** Updates whether the cursor is visible in {@link #_text}.
     * The cursor is set to visible if the stage's key focus is currently
     * in the text. */
    _updateCursorVisibility: function() {
        let focus = global.stage.get_key_focus();
        this._text.set_cursor_visible(focus == this._text);
    },

    /** Callback when {@link #_entry} is mapped.
     * If it is mapped (i.e. Overview has just been opened), we capture all events
     * and divert them to {@link #_onCapturedEvent}. All that does
     * is listen for button presses outside of the search box to possibly
     * cancel the search if the user didn't type anything in.
     *
     * @see #_onCapturedEvent
     */
    _onMapped: function() {
        if (this._entry.mapped) {
            // Enable 'find-as-you-type'
            this._capturedEventId = global.stage.connect('captured-event',
                                 Lang.bind(this, this._onCapturedEvent));
            this._text.set_cursor_visible(true);
            this._text.set_selection(0, 0);
        } else {
            // Disable 'find-as-you-type'
            if (this._capturedEventId > 0)
                global.stage.disconnect(this._capturedEventId);
            this._capturedEventId = 0;
        }
    },

    /** Adds a search provider to {@link #_searchSystem} and
     * creates a result actor for it in {@link #_searchResults}.
     * @see ViewSelector#addSearchProvider
     * @see SearchDisplay.SearchResults#createProviderMeta
     * @see Search.SearchSystem#registerProvider
     * @param {Search.SearchProvider} provider - a search provider.
     */
    addSearchProvider: function(provider) {
        this._searchSystem.registerProvider(provider);
        this._searchResults.createProviderMeta(provider);
    },

    /** Removes a search provider from {@link #_searchSystem} and
     * destroys its result actor for in {@link #_searchResults}.
     * @see ViewSelector#removeSearchProvider
     * @see SearchDisplay.SearchResults#destroyProviderMeta
     * @see Search.SearchSystem#unregisterProvider
     * @param {Search.SearchProvider} provider - a search provider.
     */
    removeSearchProvider: function(provider) {
        this._searchSystem.unregisterProvider(provider);
        this._searchResults.destroyProviderMeta(provider);
    },

    /** Possibly starts a search from a captured event.
     * Sets the key focus to {@link #_text} and fires the event at it.
     * Depending on what event it is, it may trigger {@link #_onKeyPress},
     * starting a search.
     *
     * Passed through by {@link ViewSelector#_onStageKeyPress}.
     * @param {Clutter.Event} event - a captured event.
     * @see ViewSelector#_onStageKeyPress
     */
    startSearch: function(event) {
        global.stage.set_key_focus(this._text);
        this._text.event(event, false);
    },

    // the entry does not show the hint
    /** Whether the user has given focus to the entry box. Different to
     * {@link #active} which requires that text be entered, whilst
     * this just requires that the hint text "Type to search..." is not displaying.
     * @returns {boolean} whether the entry box is active.
     */
    _isActivated: function() {
        return this._text.text == this._entry.get_text();
    },

    /** Callback for the 'text-changed' signal of {@link #_text}, caught
     * when the user changes the text of the search entry.
     *
     * If this is a new search, we call
     * [this._searchResults.startingSearch()]{@link SearchDisplay.SearchResults#startingSearch}
     * to display the "Searching..." text in the search results.
     *
     * Then if there is text in the entry, we change the icon to {@link #_activeIcon}
     * and make sure that clicking it clears/resets the search box.
     *
     * Otherwise if there is no text in the entry we emit a 'search-cancelled'
     * signal (because the user must have deleted all the text from the entry)
     * and reset the entry's icon to {@link #_inactiveIcon} and cancel
     * any search in progress.
     *
     * If there is a search to do we queue {@link #_doSearch} after
     * 150ms.
     * @fires .search-cancelled
     */
    _onTextChanged: function (se, prop) {
        let searchPreviouslyActive = this.active;
        this.active = this._entry.get_text() != '';
        this._searchPending = this.active && !searchPreviouslyActive;
        if (this._searchPending) {
            this._searchResults.startingSearch();
        }
        if (this.active) {
            this._entry.set_secondary_icon(this._activeIcon);

            if (this._iconClickedId == 0) {
                this._iconClickedId = this._entry.connect('secondary-icon-clicked',
                    Lang.bind(this, function() {
                        this._reset();
                    }));
            }
            this._activate();
        } else {
            if (this._iconClickedId > 0)
                this._entry.disconnect(this._iconClickedId);
            this._iconClickedId = 0;

            this._entry.set_secondary_icon(this._inactiveIcon);
            this.emit('search-cancelled');
        }
        if (!this.active) {
            if (this._searchTimeoutId > 0) {
                Mainloop.source_remove(this._searchTimeoutId);
                this._searchTimeoutId = 0;
            }
            return;
        }
        if (this._searchTimeoutId > 0)
            return;
        this._searchTimeoutId = Mainloop.timeout_add(150, Lang.bind(this, this._doSearch));
    },

    /** Callback for {@link #_text}'s 'key-press-event' signal, when
     * the user presses keys while focus is on the entry.
     *
     * If they pressed Escape we call {@link #_reset} to reset the
     * search entry box (this may trigger a 'text-changed' signal to trigger
     * {@link #_onTextChanged}, emitting the
     * ['search-cancelled']{@link .event:search-cancelled} signal
     * from this).
     *
     * If they pressed the up or down key and we have results, we call
     * [`this._searchResults.selectUp`]{@link SearchDisplay.SearchResults#selectUp}
     * to navigate through the results.
     */
    _onKeyPress: function(entry, event) {
        let symbol = event.get_key_symbol();
        if (symbol == Clutter.Up) {
            if (!this.active)
                return true;
            this._searchResults.selectUp(false);

            return true;
        } else if (symbol == Clutter.Down) {
            if (!this.active)
                return true;

            this._searchResults.selectDown(false);
            return true;
        } else if (symbol == Clutter.Escape) {
            if (this._isActivated()) {
                this._reset();
                return true;
            }
        }

        return false;
    },

    /** Callback for a global captured event while the search entry box is showing.
     *
     * If the event is a button press and the user clicked outside the entry box
     * after activating the entry but not typing any search terms in, we cancel
     * the search.
     */
    _onCapturedEvent: function(actor, event) {
        if (event.type() == Clutter.EventType.BUTTON_PRESS) {
            let source = event.get_source();
            if (source != this._text && this._text.text == '' &&
                !Main.layoutManager.keyboardBox.contains(source)) {
                // the user clicked outside after activating the entry, but
                // with no search term entered and no keyboard button pressed
                // - cancel the search
                this._reset();
            }
        }

        return false;
    },

    /** Starts a search.
     * This gets the text from {@link #_text}, trims it, and passes it
     * through to the [`doSearch`]{@link SearchDisplay.SearchResults#doSearch}
     * method of {@link #_searchResults}. (The SearchResults class then
     * handles displaying results as they are found).
     * @see SearchDisplay.SearchResults#doSearch
     */
    _doSearch: function () {
        this._searchTimeoutId = 0;
        let text = this._text.get_text().replace(/^\s+/g, '').replace(/\s+$/g, '');
        this._searchResults.doSearch(text);

        return false;
    }
};
/** Emitted whenever a search from the search box is cancelled.
 * @name search-cancelled
 * @event
 * @memberof SearchTab
 */

/** creates a new ViewSelector
 *
 * This creates the UI elements:
 *
 * * the top-level actor {@link #actor}, a vertical box layout.
 * * the tab bar {@link #_tabBar}, a Shell.GenericContainer, added
 * to {@link #actor}.
 * * a box to hold "normal" tab labels (application/windows) {@link #_tabBox},
 * placed into the tab bar.
 * * a Bin to hold the search tab {@link #_searchArea}, also placed
 * into the tab bar.
 * * a Shell.Stack to hold all the page contents (each page gets allocated the
 * full area available to it, we just hide the inactive ones)
 * {@link #_pageActor}, added to {@link #actor}.
 *
 * We then create a {@link SearchTab} and add it with {@link #_addTab}.
 * We connect to its ['search-cancelled']{@link SearchTab.search-cancelled} signal
 * in order to switch tabs back to the previous one upon a search being cancelled.
 *
 * We also connect to the Overview's ['item-drag-begin']{@link Overview.Overview.item-drag-begin}
 * signal in order to switch back to the workspaces display/windows tab when an
 * item is dragged ({@link #_switchDefaultTab}).
 *
 * We also connect to the Overview's ['showing']{@link Overview.Overview.event:showing}
 * and ['hiding']{@link Overview.Overview.event:hiding} signals in order to make
 * sure the windows/workspaces display tab is shown as the default one.
 * 
 * Upon the overview showing, we also intercept global key press events, sending
 * them to {@link #_onStageKeyPress} which usually relays the events
 * on to the search box.
 *
 * Finally, we expose some public height and vertical position constraints
 * {@link #constrainY} and {@link #constrainHeight} which
 * may be used to constrain another actor's height or vertical position to
 * the current tab's content (for example the dash is constrained to be the same
 * vertical position and height as this so that it remains centered vertically).
 *
 * @classdesc
 * ![ViewSelector in red, displaying the Applications tab](pics/ViewSelector.png)
 *
 * This is the notebook-like class being the 'content area' bit of the overview
 * (the overview minus the workspace display and dash).
 *
 * You add pages ('tabs') to it with `addViewTab`, and switch between tabs with
 * {@link #switchTab}. The view selector is stored in
 * [`Main.overview._viewSelector`]{@link Overview.Overview._viewSelector}.
 * @class
 */
function ViewSelector() {
    this._init();
}

ViewSelector.prototype = {
    _init : function() {
        /** Top-level actor for the view selector. A vertical box layout (will
         * contain the tab bar and then a the page contents area)
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ name: 'viewSelector',
                                        vertical: true });

        /** The tab bar, containing all the tabs' title actors.
         *
         * The tab bar is located at the top of the view selector and
         * holds both "normal" tab labels and the search entry. The former
         * is left aligned, the latter right aligned - unless the text
         * direction is RTL, in which case the order is reversed.
         * @type {Shell.GenericContainer} */
        this._tabBar = new Shell.GenericContainer();
        this._tabBar.connect('get-preferred-width',
                             Lang.bind(this, this._getPreferredTabBarWidth));
        this._tabBar.connect('get-preferred-height',
                             Lang.bind(this, this._getPreferredTabBarHeight));
        this._tabBar.connect('allocate',
                             Lang.bind(this, this._allocateTabBar));
        this.actor.add(this._tabBar);

        /** Box to hold "normal" tab labels (applications, windows)
         * @type {St.BoxLayout} */
        this._tabBox = new St.BoxLayout({ name: 'viewSelectorTabBar' });
        this._tabBar.add_actor(this._tabBox);

        /** The searchArea just holds the entry (search tab title actor, being
         * the search box).
         * @type {St.Bin} */
        this._searchArea = new St.Bin({ name: 'searchArea' });
        this._tabBar.add_actor(this._searchArea);

        /** The page area holds the tab pages. Every page is given the
         * area's full allocation, so that the pages would appear on top
         * of each other if the inactive ones weren't hidden.
         * @type {Shell.Stack}
         */
        this._pageArea = new Shell.Stack();
        this.actor.add(this._pageArea, { x_fill: true,
                                         y_fill: true,
                                         expand: true });

        this._tabs = [];
        this._activeTab = null;

        /** The search tab.
         * @type {ViewSelector.SearchTab} */
        this._searchTab = new SearchTab();
        this._searchArea.set_child(this._searchTab.title);
        this._addTab(this._searchTab);

        this._searchTab.connect('search-cancelled', Lang.bind(this,
            function() {
                this._switchTab(this._activeTab);
            }));

        Main.overview.connect('item-drag-begin',
                              Lang.bind(this, this._switchDefaultTab));

        this._stageKeyPressId = 0;
        Main.overview.connect('showing', Lang.bind(this,
            function () {
                this._switchDefaultTab();
                this._stageKeyPressId = global.stage.connect('key-press-event',
                                                             Lang.bind(this, this._onStageKeyPress));
            }));
        Main.overview.connect('hiding', Lang.bind(this,
            function () {
                this._switchDefaultTab();
                if (this._stageKeyPressId != 0) {
                    global.stage.disconnect(this._stageKeyPressId);
                    this._stageKeyPressId = 0;
                }
            }));

        /** Public constraints which may be used to tie actors' height or
         * vertical position to the current tab's content; as the content's
         * height and position depend on the view selector's style properties
         * (e.g. font size, padding, spacing, ...) it would be extremely hard
         * and ugly to get these from the outside. While it would be possible
         * to use position and height properties directly, outside code would
         * need to ensure that the content is properly allocated before
         * accessing the properties.
         * @see #constrainHeight
         * @type {Clutter.BindConstraint}
         */
        this.constrainY = new Clutter.BindConstraint({ source: this._pageArea,
                                                       coordinate: Clutter.BindCoordinate.Y });
        /** Public constraints which may be used to tie actors' height or
         * vertical position to the current tab's content; as the content's
         * height and position depend on the view selector's style properties
         * (e.g. font size, padding, spacing, ...) it would be extremely hard
         * and ugly to get these from the outside. While it would be possible
         * to use position and height properties directly, outside code would
         * need to ensure that the content is properly allocated before
         * accessing the properties.
         * @see #constrainY
         * @type {Clutter.BindConstraint}
         */
        this.constrainHeight = new Clutter.BindConstraint({ source: this._pageArea,
                                                            coordinate: Clutter.BindCoordinate.HEIGHT });
    },

    /** Internal function. Adds a tab to the view selector.
     * This adds its page to the {@link #_pageArea} and connects
     * to its ['activated']{@link BaseTab.activated} signal in order to switch
     * to that tab upon it being activated.
     * @param {ViewSelector.BaseTab} tab - a tab.
     */
    _addTab: function(tab) {
        tab.page.hide();
        this._pageArea.add_actor(tab.page);
        tab.connect('activated', Lang.bind(this, function(tab) {
            this._switchTab(tab);
        }));
    },

    /** Adds a "normal" tab ({@link ViewTab}) to the view selector, i.e. where the
     * tab title is a St.Button not anything exotic like the search tab's
     * St.Entry.
     *
     * This creates a new {@link ViewTab} from the input arguments and then
     * calls {@link #_addTab} internally to add the tab.
     * @inheritparams ViewTab
     */
    addViewTab: function(id, title, pageActor, a11yIcon) {
        let viewTab = new ViewTab(id, title, pageActor, a11yIcon);
        this._tabs.push(viewTab);
        this._tabBox.add(viewTab.title);
        this._addTab(viewTab);
    },

    /** Switches tabs. This adds the 'selected' pseudo style class to the
     * tab's title actor.
     *
     * We only set the `animate` parameter of {@link BaseTab#show} to `true`
     * (i.e. fade in the tab as oppposed to abruptly switching) if we are
     * switching between tabs (as opposed to setting the initially selected one).
     * @param {ViewSelector.BaseTab} tab - a tab.
     */
    _switchTab: function(tab) {
        let firstSwitch = this._activeTab == null;

        if (this._activeTab && this._activeTab.visible) {
            if (this._activeTab == tab)
                return;
            this._activeTab.title.remove_style_pseudo_class('selected');
            this._activeTab.hide();
        }

        if (tab != this._searchTab) {
            tab.title.add_style_pseudo_class('selected');
            this._activeTab = tab;
            if (this._searchTab.visible) {
                this._searchTab.hide();
            }
        }

        // Only fade when switching between tabs,
        // not when setting the initially selected one.
        if (!tab.visible)
            tab.show(!firstSwitch);

        // Pull a Meg Ryan:
        if (!firstSwitch && Main.overview.workspaces) {
            if (tab != this._tabs[0]) {
                Tweener.addTween(Main.overview.workspaces.actor,
                                 { opacity: 0,
                                   time: 0.1,
                                   transition: 'easeOutQuad',
                                   onComplete: Lang.bind(this,
                                       function() {
                                           Main.overview.workspaces.actor.hide();
                                           Main.overview.workspaces.actor.opacity = 255;
                                       })
                                });
            } else {
                Main.overview.workspaces.actor.opacity = 0;
                Main.overview.workspaces.actor.show();
                Tweener.addTween(Main.overview.workspaces.actor,
                                 { opacity: 255,
                                   time: 0.1,
                                   transition: 'easeOutQuad' });
            }
        }
    },

    /** Switch to a tab based on its ID (the same ID that was given to
     * {@link ViewTab} or {@link #addViewTab}).
     * @param {string} id - ID of the tab to switch to (the same ID that was
     * given to {@link ViewTab} or {@link #addViewTab}).
     */
    switchTab: function(id) {
        for (let i = 0; i < this._tabs.length; i++)
            if (this._tabs[i].id == id) {
                this._switchTab(this._tabs[i]);
                break;
            }
    },

    /** Switches to the default tab (being the first tab added that isn't
     * the search tab, so for the Overview it's the windows tab (with the
     * workspaces display)) */
    _switchDefaultTab: function() {
        if (this._tabs.length > 0)
            this._switchTab(this._tabs[0]);
    },

    /** Switches to the next tab (does not wrap around). */
    _nextTab: function() {
        if (this._tabs.length == 0 ||
            this._tabs[this._tabs.length - 1] == this._activeTab)
            return;

        for (let i = 0; i < this._tabs.length; i++)
            if (this._tabs[i] == this._activeTab) {
                this._switchTab(this._tabs[i + 1]);
                return;
            }
    },

    /** Switches to the previous tab (does not wrap around). */
    _prevTab: function() {
        if (this._tabs.length == 0 || this._tabs[0] == this._activeTab)
            return;

        for (let i = 0; i < this._tabs.length; i++)
            if (this._tabs[i] == this._activeTab) {
                this._switchTab(this._tabs[i - 1]);
                return;
            }
    },

    /** Callback for the 'get-preferred-width' signal of the tab bar
     * {@link #_tabBar}.
     *
     * This returns the sum of the widths of each of the tab titles.
     */
    _getPreferredTabBarWidth: function(box, forHeight, alloc) {
        let children = box.get_children();
        for (let i = 0; i < children.length; i++) {
            let [childMin, childNat] = children[i].get_preferred_width(forHeight);
            alloc.min_size += childMin;
            alloc.natural_size += childNat;
        }
    },

    /** Callback for the 'get-preferred-height' signal of the tab bar
     * {@link #_tabBar}.
     *
     * This returns the maximum of the {natural, minimum} heights for each of
     * the tab titles.
     */
    _getPreferredTabBarHeight: function(box, forWidth, alloc) {
        let children = box.get_children();
        for (let i = 0; i < children.length; i++) {
            let [childMin, childNatural] = children[i].get_preferred_height(forWidth);
            if (childMin > alloc.min_size)
                alloc.min_size = childMin;
            if (childNatural > alloc.natural_size)
                alloc.natural_size = childNatural;
        }
    },

    /** Callback for the 'allocate' signal of the tab bar
     * {@link #_tabBar}.
     *
     * This allocates {@link #_tabbox} its preferred width
     * and positions it to the left of the tab bar and puts the search entry
     * on the right side (opposite if text direction is RTL).
     *
     * This also updates {@link #_constrainY}.
     */
    _allocateTabBar: function(container, box, flags) {
        let allocWidth = box.x2 - box.x1;
        let allocHeight = box.y2 - box.y1;

        let [searchMinWidth, searchNatWidth] = this._searchArea.get_preferred_width(-1);
        let [barMinWidth, barNatWidth] = this._tabBox.get_preferred_width(-1);
        let childBox = new Clutter.ActorBox();
        childBox.y1 = 0;
        childBox.y2 = allocHeight;
        if (this.actor.get_direction() == St.TextDirection.RTL) {
            childBox.x1 = allocWidth - barNatWidth;
            childBox.x2 = allocWidth;
        } else {
            childBox.x1 = 0;
            childBox.x2 = barNatWidth;
        }
        this._tabBox.allocate(childBox, flags);

        if (this.actor.get_direction() == St.TextDirection.RTL) {
            childBox.x1 = 0;
            childBox.x2 = searchNatWidth;
        } else {
            childBox.x1 = allocWidth - searchNatWidth;
            childBox.x2 = allocWidth;
        }
        this._searchArea.allocate(childBox, flags);

        Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this,
            function() {
                this.constrainY.offset = this.actor.y;
            }));
    },

    /** Callback when the overview is showing and a key press from the global
     * stage is intercepted.
     *
     * If it's the Escape button, the overview is hidden. If it's Ctrl + Page Up
     * or Ctrl + Page Down, we navigate to the previous or next tabs respectively.
     * 
     * Otherwise if the symbol has a unicode character (i.e. it's not something
     * like num lock), we call {@link SearchTab#startSearch} with the event to
     * send the text to the search box.
     */
    _onStageKeyPress: function(actor, event) {
        let modifiers = Shell.get_event_state(event);
        let symbol = event.get_key_symbol();

        if (symbol == Clutter.Escape) {
            Main.overview.hide();
            return true;
        } else if (modifiers & Clutter.ModifierType.CONTROL_MASK) {
            if (symbol == Clutter.Page_Up) {
                if (!this._searchTab.active)
                    this._prevTab();
                return true;
            } else if (symbol == Clutter.Page_Down) {
                if (!this._searchTab.active)
                    this._nextTab();
                return true;
            }
        } else if (Clutter.keysym_to_unicode(symbol)) {
            this._searchTab.startSearch(event);
        }
        return false;
    },

    /** Adds a search provider to the search tab, calling
     * [`this._searchTab.addSearchProvider`]{@link SearchTab#addSearchProvider}.
     * @see Overview.Overview#addSearchProvider
     * @see SearchTab#addSearchProvider
     * @param {Search.SearchProvider} provider - a search provider.
     */
    addSearchProvider: function(provider) {
        this._searchTab.addSearchProvider(provider);
    },

    /** Removes a search provider from the search tab, calling
     * [`this._searchTab.removeSearchProvider`]{@link SearchTab#removeSearchProvider}.
     * @see Overview.Overview#removeSearchProvider
     * @see SearchTab#removeSearchProvider
     * @param {Search.SearchProvider} provider - a search provider.
     */
    removeSearchProvider: function(provider) {
        this._searchTab.removeSearchProvider(provider);
    }
};
Signals.addSignalMethods(ViewSelector.prototype);
