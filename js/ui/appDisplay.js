// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file defines a {@link Search.SearchProvider} for applications
 * ({@link AppSearchProvider}) as well as one
 * for settings ({@link SettingsSearchProvider}).
 *
 * It also defines classes for the applications tab in the overview and for
 * displaying apps with their icons and an app-specific menu (
 * {@link AllAppDisplay}, {@link ViewByCategories}, {@link AlphabeticalView},
 * {@link AppWellIcon}).
 *
 * @todo pic of appDisplay and of the app/setting search providers
 */

const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const GMenu = imports.gi.GMenu;
const Shell = imports.gi.Shell;
const Lang = imports.lang;
const Signals = imports.signals;
const Meta = imports.gi.Meta;
const St = imports.gi.St;
const Mainloop = imports.mainloop;

const AppFavorites = imports.ui.appFavorites;
const DND = imports.ui.dnd;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const Overview = imports.ui.overview;
const PopupMenu = imports.ui.popupMenu;
const Search = imports.ui.search;
const Tweener = imports.ui.tweener;
const Workspace = imports.ui.workspace;
const Params = imports.misc.params;

/** @+
 * @const
 * @default
 * @type {number} */
/** Unused. */
const MAX_APPLICATION_WORK_MILLIS = 75;
/** Time (ms) for a 'long-click' on an {@link AppWellIcon} - if you click and hold
 * down on the icon, after this many milliseconds the popup menu will show. */
const MENU_POPUP_TIMEOUT = 600;
/** Time (milliseconds) over which the {@link AlphabeticalView} will scroll
 * if you navigate to an icon that is currently scrolled out of view.
 * @see AlphabeticalView#_ensureIconVisible */
const SCROLL_TIME = 0.1;
/** @- */

/** creates a new AlphabeticalView.
 *
 * This stores a copy of the Shell.AppSystem instance to use to look up
 * apps.
 *
 * UI-wise we create a `St.BoxLayout` for the top-level actor {@link #actor}
 * and inside it we put a {@link IconGrid.IconGrid} ({@link #_grid})
 * to display all the applications.
 *
 * @classdesc
 * ![AlphabeticalView in the Applications tab of the overview](pics/AlphabeticalView.png)
 *
 * The AlphabeticalView is an alphabetical list of applications, each represented
 * by a {@link AppWellIcon}. It's really just a {@link IconGrid.IconGrid}.
 *
 * It has the additional ability to only display a subset of all the applications
 * (see {@link #setVisibleApps}). This can be used to say
 * only show apps from a particular category.
 *
 * The {@link AlphabeticalView} is embedded into a {@link ViewByCategories}
 * (which provides a list of application categories and displays matching
 * applications for that category in the AlphabeticalView), which itself
 * is embedded into an {@link AllAppDisplay}, which is used as the Applications
 * tab in the overview.
 *
 * @class
 */
function AlphabeticalView() {
    this._init();
}

AlphabeticalView.prototype = {
    _init: function() {
        /** A {@link IconGrid.IconGrid} to display all the app icons. No
         * row/column limit.
         * @type {IconGrid.IconGrid} */
        this._grid = new IconGrid.IconGrid({ xAlign: St.Align.START });
        /** Store the app system instance locally to list apps etc.
         * @type {Shell.AppSystem} */
        this._appSystem = Shell.AppSystem.get_default();

        this._pendingAppLaterId = 0;
        /** Hash of app ID (desktop file name) to the app's
         * icon ({@link AppWellIcon#actor}).
         * @type {Object.<string, St.Button>} */
        this._appIcons = {}; // desktop file id

        let box = new St.BoxLayout({ vertical: true });
        box.add(this._grid.actor, { y_align: St.Align.START, expand: true });

        /** Top-level actor is a `St.ScrollView` containing the grid of icons
         * in order to allow scrolling. Style class 'vfade'.
         * @type {St.ScrollView} */
        this.actor = new St.ScrollView({ x_fill: true,
                                         y_fill: false,
                                         y_align: St.Align.START,
                                         style_class: 'vfade' });
        this.actor.add_actor(box);
        this.actor.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
        this.actor.connect('notify::mapped', Lang.bind(this,
            function() {
                if (!this.actor.mapped)
                    return;

                let adjustment = this.actor.vscroll.adjustment;
                let direction = Overview.SwipeScrollDirection.VERTICAL;
                Main.overview.setScrollAdjustment(adjustment, direction);

                // Reset scroll on mapping
                adjustment.value = 0;
            }));
    },

    /** Removes all the icons from the grid. */
    _removeAll: function() {
        this._grid.removeAll();
        this._appIcons = {};
    },

    /** Internal function - adds an application to the grid.
     *
     * This creates a new {@link AppWellIcon} for the app, adds it to
     * {@link #_grid} and also to the hash
     * {@link #_appIcons}.
     * 
     * @param {Shell.App} app - application to add to the grid.
     */
    _addApp: function(app) {
        var id = app.get_id();
        let appIcon = new AppWellIcon(app);

        this._grid.addItem(appIcon.actor);
        appIcon.actor.connect('key-focus-in', Lang.bind(this, this._ensureIconVisible));

        this._appIcons[id] = appIcon;
    },

    /** Ensures that an icon is visible by scrolling the scroll view
     * {@link #actor} to fully show the icon.
     *
     * This is the callback for the 'key-focus-in' event of each icon in the
     * grid, i.e. when the user navigates to the icon using the keyboard.
     *
     * You would notice it when you navigate (with the keyboard) to the icon of an application
     * that is out of view of the scroll view - the scroll view will scroll
     * so that the icon is visible.
     *
     * @param {St.Button} icon - the {@link AppWellIcon#actor} that emitted
     * the signal.
     */
    _ensureIconVisible: function(icon) {
        let adjustment = this.actor.vscroll.adjustment;
        let [value, lower, upper, stepIncrement, pageIncrement, pageSize] = adjustment.get_values();

        let offset = 0;
        let vfade = this.actor.get_effect("vfade");
        if (vfade)
            offset = vfade.fade_offset;

        // If this gets called as part of a right-click, the actor
        // will be needs_allocation, and so "icon.y" would return 0
        let box = icon.get_allocation_box();

        if (box.y1 < value + offset)
            value = Math.max(0, box.y1 - offset);
        else if (box.y2 > value + pageSize - offset)
            value = Math.min(upper, box.y2 + offset - pageSize);
        else
            return;

        Tweener.addTween(adjustment,
                         { value: value,
                           time: SCROLL_TIME,
                           transition: 'easeOutQuad' });
    },

    /** Sets which app icons should be visible.
     *
     * This is used (for example) by {@link ViewByCategories} to display just
     * those apps that are in a particular category.
     * @param {?Shell.App[]} apps - the applications to be visible. Use `null`
     * to make every app visible.
     */
    setVisibleApps: function(apps) {
        if (apps == null) { // null implies "all"
            for (var id in this._appIcons) {
                var icon = this._appIcons[id];
                icon.actor.visible = true;
            }
        } else {
            // Set everything to not-visible, then set to visible what we should see
            for (var id in this._appIcons) {
                var icon = this._appIcons[id];
                icon.actor.visible = false;
            }
            for (var i = 0; i < apps.length; i++) {
                var app = apps[i];
                var id = app.get_id();
                var icon = this._appIcons[id];
                icon.actor.visible = true;
            }
        }
    },

    /** Sets the list of applications that this AlphabeticalView can display.
     * What it displays will always be a subset of this (so you would add every
     * app in the app system here, and then use {@link #setVisibleApps}
     * to display your desired subset of application icons).
     * @param {Shell.App[]} apps - array of applications to add to the AlphabeticalView.
     * @see #_addApp
     */
    setAppList: function(apps) {
        this._removeAll();
        for (var i = 0; i < apps.length; i++) {
            var app = apps[i];
            this._addApp(app);
         }
    }
};

/** creates a new ViewByCategories
 *
 * The constructor stores Shell.AppSystem instance to use to look up applications.
 *
 * It also creates a {@link AlphabeticalView} ({@link #AlphabeticalView})
 * to display the apps.
 *
 * UI-wise, we create a St.BoxLayout ({@link #actor}) that
 * contains a St.ScrollView that contains a St.BoxLayout
 * {@link #_categoryBox}. It is this box layout that
 * the application headings get put into.
 *
 * We also ensure that the first time the actor is mapped, the 'All'
 * category gets selected.
 *
 * @classdesc
 * ![ViewByCategories is a {@link AlphabeticalView} and a list of categories](pics/ViewByCategories.png)
 *
 * The ViewByCategores is an AlphabeticalView (for display apps) along with
 * a sidebar containing a list of application categories (All, Accessories, ...).
 *
 * Clicking on a category in the sidebar filters what apps are shown in the
 * AlphabeticalView.
 *
 * This class is embedded in a {@link AllAppDisplay} and used as the
 * "Applications" tab of the overview.
 *
 * To get a list of applications for each directory, the [`GMenu library`](http://www.roojs.org/seed/gir-1.2-gtk-2.0/seed/GMenu.html)
 * is used.
 * @class
 */
function ViewByCategories() {
    this._init();
}

ViewByCategories.prototype = {
    _init: function() {
        /** Store the app system instance locally to list apps etc.
         * @type {Shell.AppSystem} */
        this._appSystem = Shell.AppSystem.get_default();
        /** Top-level actor. A `St.BoxLayout` with style class 'all-app'.
         * It contains a `St.ScrollView` that is what the list of categories
         * is added to.
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ style_class: 'all-app' });
        this.actor._delegate = this;

        /** This displays the icons for the selected category.
         * @type {AlphabeticalView} */
        this._view = new AlphabeticalView();

        /** The current category of apps we are viewing.
         * Categories can be -1 (the All view) or 0...`n`-1, where `n`
         * is the number of sections.
         * -2 is a flag to indicate that nothing is selected
         * (used only before the actor is mapped the first time)
         * @type {number} */
        this._currentCategory = -2;
        /* List of information for each category - the subset of apps corresponding
         * to it, category name, and actor in the {@link #_categoryBox}.
         * @type {[CategoryMeta]{@link CategoryMeta}[]} */
        this._categories = [];
        this._apps = null;

        /** This is what each of the category titles gets added to.
         * It gets nested into a St.ScrollView before being added to
         * {@link #actor} so that it is scrollable.
         * @type {St.Box} */
        this._categoryBox = new St.BoxLayout({ vertical: true, reactive: true });
        this._categoryScroll = new St.ScrollView({ x_fill: false,
                                                   y_fill: false,
                                                   style_class: 'vfade' });
        this._categoryScroll.add_actor(this._categoryBox);
        this.actor.add(this._view.actor, { expand: true, x_fill: true, y_fill: true });
        this.actor.add(this._categoryScroll, { expand: false, y_fill: false, y_align: St.Align.START });

        // Always select the "All" filter when switching to the app view
        this.actor.connect('notify::mapped', Lang.bind(this,
            function() {
                if (this.actor.mapped && this._allCategoryButton)
                    this._selectCategory(-1);
            }));

        // We need a dummy actor to catch the keyboard focus if the
        // user Ctrl-Alt-Tabs here before the deferred work creates
        // our real contents
        this._focusDummy = new St.Bin({ can_focus: true });
        this.actor.add(this._focusDummy);
    },

    /** Selects a category to view the apps for.
     * Use -1 to see all apps, otherwise 0...n-1, where `n` is
     * [`this._categories.length`]{@link #_categories}.
     *
     * This adds the 'selected' style class to the appropriate button in the
     * sidebar and calls {@link AlphabeticalView#setVisibleApps} with
     * either `null` to show all apps (`num=-1`), or the category's apps
     * (see {@link CategoryMeta}.apps).
     *
     * @param {number} num - the number of the category to view apps for.
     */
    _selectCategory: function(num) {
        if (this._currentCategory == num) // nothing to do
            return;

        this._currentCategory = num;

        if (num != -1) {
            var category = this._categories[num];
            this._allCategoryButton.remove_style_pseudo_class('selected');
            this._view.setVisibleApps(category.apps);
        } else {
            this._allCategoryButton.add_style_pseudo_class('selected');
            this._view.setVisibleApps(null);
        }

        for (var i = 0; i < this._categories.length; i++) {
            if (i == num)
                this._categories[i].button.add_style_pseudo_class('selected');
            else
                this._categories[i].button.remove_style_pseudo_class('selected');
        }
    },

    // Recursively load a GMenuTreeDirectory; we could put this in ShellAppSystem too
    /** This creates a list of all apps under a GMenuTreeDirectory, recursing
     * down into directories.
     *
     * We look through all children of `dir` and if it is an entry and exists
     * in {@link #_appSystem}, we add it to `appList`.
     * If the child is a directory instead of an entry/node, we recurse further
     * into it.
     *
     * Used to create an array of all applications for a particular category.
     * @param {GMenu.TreeDirectory} dir - directory of the GMenu we are recursing into
     * @param {Shell.App} appList - array of apps found (is modified by
     * the function).
     */
    _loadCategory: function(dir, appList) {
        var iter = dir.iter();
        var nextType;
        while ((nextType = iter.next()) != GMenu.TreeItemType.INVALID) {
            if (nextType == GMenu.TreeItemType.ENTRY) {
                var entry = iter.get_entry();
                var app = this._appSystem.lookup_app_by_tree_entry(entry);
                if (!entry.get_app_info().get_nodisplay())
                    appList.push(app);
            } else if (nextType == GMenu.TreeItemType.DIRECTORY) {
                if (!dir.get_is_nodisplay())
                    this._loadCategory(iter.get_directory(), appList);
            }
        }
    },

    /** Adds a category to the ViewByCategories.
     *
     * This creates a `St.Button` with style class 'app-filter' for the category
     * and puts it into the {@link #_categoryBox}, connecting
     * up its 'clicked' signal to {@link #_selectCategory}.
     *
     * Then {@link #_loadCategory} is called to get a list
     * of apps for that category, and a {@link CategoryMeta} is created for
     * the category and added to {@link #_categories}.
     *
     * @param {string} name - name of the category ('Internet', 'Accessories', ...)
     * @param {number} index - index of the category
     * ({@link #_categories.length} given that we push onto the end of it?)
     * @param {?GMenu.TreeDirectory} dir - the entry of the GMenu tree for
     * 'applications.menu' that is that category, used to find all apps in
     * that category. If `null`, we assume that it is the "All" category.
     * @param {Shell.App[]} allApps - array of every app (if the category
     * is "All" then this will be used instead of {@link #_loadCategory}).
     */
    _addCategory: function(name, index, dir, allApps) {
        let button = new St.Button({ label: GLib.markup_escape_text (name, -1),
                                     style_class: 'app-filter',
                                     x_align: St.Align.START,
                                     can_focus: true });
        button.connect('clicked', Lang.bind(this, function() {
            this._selectCategory(index);
        }));

        var apps;
        if (dir == null) {
            apps = allApps;
            this._allCategoryButton = button;
        } else {
            apps = [];
            this._loadCategory(dir, apps);
            this._categories.push({ apps: apps,
                                    name: name,
                                    button: button });
        }

        this._categoryBox.add(button, { expand: true, x_fill: true, y_fill: false });
    },

    /** Removes all the category buttons and metadata. */
    _removeAll: function() {
        this._categories = [];
        this._categoryBox.destroy_children();
    },

    /** Refreshes the list of apps in the {@link AlphabeticalView} and
     * refreshes the list of categories and apps for each category.
     *
     * First we remove all existing apps/categories ({@link #_removeAll}).
     *
     * Then we add the "All" category.
     *
     * Then we get the application tree from {@link #_appSystem}
     * and traverse its top level which is the list of categories.
     * For each category found, we add that category with
     * {@link #_addCategory}.
     *
     * Finally we ensure that {@link #_view} has the updated
     * list of apps ({@link AlphabeticalView#setAppList}), and select the
     * 'All' category. */
    refresh: function() {
        this._removeAll();

        var allApps = Shell.AppSystem.get_default().get_all();
        allApps.sort(function(a, b) {
            return a.compare_by_name(b);
        });

        /* Translators: Filter to display all applications */
        this._addCategory(_("All"), -1, null, allApps);

        var tree = this._appSystem.get_tree();
        var root = tree.get_root_directory();

        var iter = root.iter();
        var nextType;
        var i = 0;
        while ((nextType = iter.next()) != GMenu.TreeItemType.INVALID) {
            if (nextType == GMenu.TreeItemType.DIRECTORY) {
                var dir = iter.get_directory();
                if (dir.get_is_nodisplay())
                    continue;
                this._addCategory(dir.get_name(), i, dir);
                i++;
            }
        }

        this._view.setAppList(allApps);
        this._selectCategory(-1);

        if (this._focusDummy) {
            let focused = this._focusDummy.has_key_focus();
            this._focusDummy.destroy();
            this._focusDummy = null;
            if (focused)
                this.actor.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
        }
    }
};
/** Object describing a category in the {@link ViewByCategories}
 * @typedef CategoryMeta
 * @type {Object}
 * @property {Shell.App[]} apps - list of apps that fall under this category.
 * @property {string} name - name of the category ('Internet', 'Accessories', ...)
 * @property {St.Button} button - the St.Button for this category
 * in {@link ViewByCategories#_categoryBox}.
 */

/** creates a new AllAppDisplay.
 *
 * We store the instance of the AppSystem locally.
 *
 * Create a {@link ViewByCategories}, and a top-level actor
 * {@link #actor} (a St.Bin) to put it in.
 *
 * Then, we use {@link Main.intialiseDeferredWork} and also connect to
 * the app system's 'installed-changed' signal with {@link Main.queueDeferredWork}
 * in order to update the ViewByCategories when the list of installed applications
 * changes, but only when the computer is idle (unless the alphabetical view
 * is visible, in which case we update straight away).
 *
 * @classdesc 
 * This class is just a thin wrapper around {@link ViewByCategories}, but
 * we additionally listen to the 'installed-changed' signal from the app
 * system (when the installed programs on the system change) and update
 * the ViewByCategories when this happens.
 *
 * However the update is done using {@link Main.queueDeferredWork} so that
 * if the app view is not visible we will wait for an idle cycle to do the
 * refresh rather than using up the user's CPU.
 *
 * One of these is used in the Overview as the Applications tab.
 * @see Main.queueDeferredWork
 * @see ViewByCategories#refresh
 * @see #_redisplay
 * @class
 */
function AllAppDisplay() {
    this._init();
}

AllAppDisplay.prototype = {
    _init: function() {
        this._appSystem = Shell.AppSystem.get_default();
        this._appSystem.connect('installed-changed', Lang.bind(this, function() {
            Main.queueDeferredWork(this._workId);
        }));

        this._appView = new ViewByCategories();
        this.actor = new St.Bin({ child: this._appView.actor, x_fill: true, y_fill: true });

        this._workId = Main.initializeDeferredWork(this.actor, Lang.bind(this, this._redisplay));
    },

    /** callback for the 'installed-changed' signal of the app system.
     * Called when the list of installed applications on the system changes.
     *
     * This is used in conjunction with {@link Main.queueDeferredWork} in order
     * to refresh the app view ({@link ViewByCategories#refresh}), but we only
     * refresh straight away if the app view is visible. If it is not visible
     * we wait until the next idle cycle before refreshing (to save some CPU).
     * @see Main.queueDeferredWork
     * @see ViewByCategories#refresh
     */
    _redisplay: function() {
        this._appView.refresh();
    }
};

/** creates a new AppSearchProvider
 *
 * This calls the [parent constructor]{@link Search.SearchProvider} with `title`
 * parameter `_("APPLICATIONS")` and stores the default instance of the
 * `Shell.AppSystem` in {@link #_appSys} to do all the searching.
 * @classdesc
 * This is an implementation of {@link Search.SearchProvider} for searching
 * for applications in the Overview.
 * It wraps around a {@link Shell.AppSystem} to do all the searching.
 *
 * However whereas {@link Search.SearchProvider}s are meant to use strings
 * as result IDs, this uses the application's `Shell.App` instance instead...
 * @extends Search.SearchProvider
 * @class
 */
function AppSearchProvider() {
    this._init();
}

AppSearchProvider.prototype = {
    __proto__: Search.SearchProvider.prototype,

    _init: function() {
        Search.SearchProvider.prototype._init.call(this, _("APPLICATIONS"));
        /** The default app system instance to do all the searching.
         * @type {Shell.AppSystem} */
        this._appSys = Shell.AppSystem.get_default();
    },

    /** Implements the parent method to return metadata representing an
     * application. The return object's 'id' property is the app instance
     * (! not a string), the 'name' property is `app.get_name()`, and the
     * `createIcon` function outsources to `app.create_icon_texture`.
     * @param {Shell.App} app - application to create metadata for.
     * @override
     */
    getResultMeta: function(app) {
        return { 'id': app,
                 'name': app.get_name(),
                 'createIcon': function(size) {
                                   return app.create_icon_texture(size);
                               }
               };
    },

    /** Implements the parent method to do an initial search.
     * Uses the app system's `initial_search` method.
     * @returns {Shell.App[]} array of matching applications.
     * @override
     */
    getInitialResultSet: function(terms) {
        return this._appSys.initial_search(terms);
    },

    /** Implements the parent method to do a subsearch within a set of results.
     * Uses the app system's `subsearch` method.
     * @param {Shell.App[]} previousResults - results to search within.
     * @returns {Shell.App[]} array of matching applications.
     * @override
     */
    getSubsearchResultSet: function(previousResults, terms) {
        return this._appSys.subsearch(previousResults, terms);
    },

    /** Implements the parent method to activate an application.
     *
     * The extra `params` parameter tells us which workspace to launch the
     * workspace on and is only used by {@link SearchDisplay.SearchResult#shellWorkspaceLaunch}.
     *
     * This also sees if the user had pressed CTRL down at the time of the
     * function call and if so, launches a new window for the application
     * as opposed to switching to the current window for that application
     * (or if the application is not running, it launches it).
     * @inheritparams SearchDisplay.SearchResult#shellWorkspaceLaunch
     * @param {Shell.App} app - application to launch.
     * @override
     */
    activateResult: function(app, params) {
        params = Params.parse(params, { workspace: -1,
                                        timestamp: 0 });

        let event = Clutter.get_current_event();
        let modifiers = event ? Shell.get_event_state(event) : 0;
        let openNewWindow = modifiers & Clutter.ModifierType.CONTROL_MASK;

        if (openNewWindow)
            app.open_new_window(params.workspace);
        else
            app.activate_full(params.workspace, params.timestamp);
    },

    /** Implements the parent method to activate an application search result
     * if it is dragged over a {@link WorkspaceThumbnail.WorkspaceThumbnail}
     * (in the sidebar of the overview) or a {@link Workspace.Workspace}.
     *
     * This creates a new window on the provided workspace.
     *
     * This is different to if the user simply clicked a result (`activateResult`)
     * in that that will switch to the most recent window of that application if
     * it is running and only open a new window if CTRL was held down, whereas
     * this will *always* open a new window.
     * @inheritparams Search.SearchProvider#dragActivateResult
     * @override
     */
    dragActivateResult: function(id, params) {
        params = Params.parse(params, { workspace: -1,
                                        timestamp: 0 });

        let app = this._appSys.lookup_app(id);
        app.open_new_window(workspace);
    },

    /** Implements the parent method to provide a custom result actor (other
     * than the default {@link IconGrid.BaseIcon} created by
     * {@link SearchDisplay.SearchResult}).
     *
     * This creates an {@link AppWellIcon} for the result, being (essentially)
     * a {@link IconGrid.BaseIcon} along with a right-click menu.
     * @override
     */
    createResultActor: function (resultMeta, terms) {
        let app = resultMeta['id'];
        let icon = new AppWellIcon(app);
        return icon.actor;
    }
};

/** creates a new SettingsSearchProvider
 *
 * This calls the [parent constructor]{@link Search.SearchProvider} with `title`
 * paremetar `_("APPLICATIONS")` and stores the default instance of the
 * `Shell.AppSystem` in {@link #_appSys} to do all the searching.
 * @classdesc
 * This is an implementation of {@link Search.SearchProvider} for searching
 * for settings (stuff that would appear in the gnome control center in the
 * Overview, like 'User Accounts' or 'Date and Time').
 *
 * It wraps around a {@link Shell.AppSystem} to do all the searching.
 *
 * However whereas {@link Search.SearchProvider}s are meant to use strings
 * as result IDs, this uses the application's `Shell.App` instance instead...
 *
 * It's basically the same as the {@link AppSearchProvider} except that instead
 * of using `search` it uses `search_settings`.
 * @extends Search.SearchProvider
 * @class
 */
function SettingsSearchProvider() {
    this._init();
}

SettingsSearchProvider.prototype = {
    __proto__: Search.SearchProvider.prototype,

    _init: function() {
        Search.SearchProvider.prototype._init.call(this, _("SETTINGS"));
        /** The default app system instance to do all the searching.
         * @type {Shell.AppSystem} */
        this._appSys = Shell.AppSystem.get_default();
        this._gnomecc = this._appSys.lookup_app('gnome-control-center.desktop');
    },

    /** Implements the parent method to return metadata representing a
     * settings widget. The return object's 'id' property is the preferences
     * widget instance
     * (! not a string), the 'name' property is `pref.get_name()`, and the
     * `createIcon` function outsources to `pref.create_icon_texture`.
     * @param {Shell.App} pref - preference to create the metadata for.
     * @override
     */
    getResultMeta: function(pref) {
        return { 'id': pref,
                 'name': pref.get_name(),
                 'createIcon': function(size) {
                                   return pref.create_icon_texture(size);
                               }
               };
    },

    /** Implements the parent method to do an initial search.
     * Uses the app system's `search_settings` method.
     * @inheritparams AppSearchProvider#getInitialResultSet
     * @override
     */
    getInitialResultSet: function(terms) {
        return this._appSys.search_settings(terms);
    },

    /** Implements the parent method and is meant to do a subsearch in `previousResults`
     * for `terms`. However it doesn't do a subsearch, but instead uses
     * `appSystem.search_settings` which is the same as an initial search
     * (there aren't many preferences in the gnome control center so it's
     * not a big deal).
     * 
     * @inheritparams AppSearchProvider#getInitialResultSet
     * @override
     */
    getSubsearchResultSet: function(previousResults, terms) {
        return this._appSys.search_settings(terms);
    },

    /** Implements the parent method to activate a preferences dialog.
     *
     * The extra `params` parameter tells us which workspace to launch the
     * workspace on and is only used by {@link SearchDisplay.SearchResult#shellWorkspaceLaunch}.
     *
     * This calls `activate_full` on the provided preference, meaning that if
     * it is already running the window will be brought to focus, and otherwise
     * the application will be launched.
     *
     * @inheritparams SearchDisplay.SearchResult#shellWorkspaceLaunch
     * @param {Shell.App} pref - preference configurator to launch.
     * @override
     */
    activateResult: function(pref, params) {
        params = Params.parse(params, { workspace: -1,
                                        timestamp: 0 });

        pref.activate_full(params.workspace, params.timestamp);
    },

    /** Implements the parent method to activate an application search result
     * if it is dragged over a {@link WorkspaceThumbnail.WorkspaceThumbnail}
     * (in the sidebar of the overview) or a {@link Workspace.Workspace}.
     *
     * This does exactly the same as {@link #activateResult}
     * (so there really is no need to implement it as if this doesn't exist
     * that will be used anyway...).
     * @inheritparams Search.SearchProvider#dragActivateResult
     * @override
     */
    dragActivateResult: function(pref, params) {
        this.activateResult(pref, params);
    },

    /** Implements the parent method to provide a custom result actor (other
     * than the default {@link IconGrid.BaseIcon} created by
     * {@link SearchDisplay.SearchResult}).
     *
     * This creates an {@link AppWellIcon} for the result, being (essentially)
     * a {@link IconGrid.BaseIcon} along with a right-click menu.
     * @override
     */
    createResultActor: function (resultMeta, terms) {
        let app = resultMeta['id'];
        let icon = new AppWellIcon(app);
        return icon.actor;
    }
};

/** creates a new AppIcon
 *
 * This calls the [parent constructor]{@link IconGrid.BaseIcon} with
 * 'label' parameter being the application's name (`app.get_name`) and the
 * input `params`.
 *
 * @param {Shell.App} app - application
 * @param {Object} [params] - parameters for the BaseIcon (passed through to
 * its constructor), see {@link IconGrid.BaseIcon}.
 * @see IconGrid.BaseIcon
 * @classdesc
 * ![AppWellIcon is an AppIcon and an AppIconMenu](pics/AppWellIcon.png)
 *
 * This is an implementation of {@link IconGrid.BaseIcon} for an application,
 * implementing the `createIcon` function (creating the icon for that application).
 *
 * @extends IconGrid.BaseIcon
 * @class
 */
function AppIcon(app, params) {
    this._init(app, params);
}

AppIcon.prototype = {
    __proto__:  IconGrid.BaseIcon.prototype,

    _init : function(app, params) {
        /** Application the icon is for.
         * @type {Shell.App} */
        this.app = app;

        let label = this.app.get_name();

        IconGrid.BaseIcon.prototype._init.call(this,
                                               label,
                                               params);
    },

    /** Implements the parent method to provide an icon, being
     * `app.create_icon_texture`.
     * @param {number} iconSize - size of the icon.
     * @override
     */
    createIcon: function(iconSize) {
        return this.app.create_icon_texture(iconSize);
    }
};

/** creates a new AppWellIcon.
 *
 * This creates an {@link AppIcon} for the application ({@link #icon}),
 * and stores it in the top-level actor/St.Button {@link #actor}.
 *
 * It connects to the 'button-press-event' event of {@link #actor}
 * to listen for right clicks (to open the menu), the 'clicked' event to listen
 * for left and middle click (activate and activate on new workspace).
 * (**Note** - why is there a need to listen to both events? Either event
 * can handle all three clicks?).
 *
 * It also listens to the 'popup-menu' event of {@link #actor},
 * triggered when the user presses the 'menu' button on their keyboard while
 * focus is on the actor, in order to show the popup menu.
 *
 * Finally we make the {@link #actor} draggable
 * ({@link DND.makeDraggable}), connecting its 'drag-begin',
 * 'drag-cancelled' and 'drag-end' events to {@link Overview.Overview#beginItemDrag},
 * {@link Overview.Overview#cancelledItemDrag}, and
 * {@link Overview.Overview#endItemDrag}.
 *
 * We also listen to changes of state of the `app` (i.e. stopped, starting,
 * running) in order to change the style of the icon according to the application's
 * state (see the picture below, where Chromium is running and the Configuration
 * Editor is not).
 *
 * ![AppWellIcons - on the left for a currently-open application](pics/AppWellIconState.png)
 *
 * @param {Shell.App} app - application
 * @param {Object} [iconParams] - parameters for the AppIcon, see {@link AppIcon}.
 * @param {function(Clutter.Event)} [onActivateOverride] - if provided, this
 * will override the callback for when the icon is clicked (current
 * default is `app.activate()`; see {@link #_onActivate}).
 *
 * As far as I can tell, nothing makes use of this.
 * @classdesc
 * ![AppWellIcon - an AppIcon and an AppIconMenu](pics/AppWellIcon.png)
 *
 * The AppWellIcon is the top-level icon class that appears for applications
 * in the Overview, both in the applications tab ({@link AllAppDisplay}) and
 * as a search result for {@link AppSearchProvider#createResultActor}.
 *
 * It consists of a {@link AppIcon} along with a popup menu and drag events.
 *
 * Left-clicking the icon launches the application; right-clicking opens the
 * popup menu; middle-clicking opens the application on a new workspace.
 * @class
 */
function AppWellIcon(app, iconParams, onActivateOverride) {
    this._init(app, iconParams, onActivateOverride);
}

AppWellIcon.prototype = {
    _init : function(app, iconParams, onActivateOverride) {
        /**  Application the icon is for.
         * @type {Shell.App} */
        this.app = app;
        /** Top-level actor - a button that responds to left and middle click
         * with style class 'app-well-app'. The Appicon lives in it.
         * @type {St.Button} */
        this.actor = new St.Button({ style_class: 'app-well-app',
                                     reactive: true,
                                     button_mask: St.ButtonMask.ONE | St.ButtonMask.TWO,
                                     can_focus: true,
                                     x_fill: true,
                                     y_fill: true });
        this.actor._delegate = this;

        /** The AppIcon that is the main visual element (it is embedded into
         * the top-level actor {@link #actor}).
         * @type {AppDisplay.AppIcon} */
        this.icon = new AppIcon(app, iconParams);
        this.actor.set_child(this.icon.actor);

        this.actor.label_actor = this.icon.label;

        /** A function callback to override the default "app.activate()" when
         * the icon is clicked. As far as I can tell nothing uses this.
         * @function
         */
        this._onActivateOverride = onActivateOverride;
        this.actor.connect('button-press-event', Lang.bind(this, this._onButtonPress));
        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
        this.actor.connect('popup-menu', Lang.bind(this, this._onKeyboardPopupMenu));

        /** Right-click menu for the icon. It is only created on-demand, so
         * it is `null` until the user asks for it.
         * @type {?AppDisplay.AppIconMenu} */
        this._menu = null;
        this._menuManager = new PopupMenu.PopupMenuManager(this);

        /** The draggable for the actor.
         * @type {DND._Draggable} */
        this._draggable = DND.makeDraggable(this.actor);
        this._draggable.connect('drag-begin', Lang.bind(this,
            function () {
                this._removeMenuTimeout();
                Main.overview.beginItemDrag(this);
            }));
        this._draggable.connect('drag-cancelled', Lang.bind(this,
            function () {
                Main.overview.cancelledItemDrag(this);
            }));
        this._draggable.connect('drag-end', Lang.bind(this,
            function () {
               Main.overview.endItemDrag(this);
            }));

        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

        this._menuTimeoutId = 0;
        this._stateChangedId = this.app.connect('notify::state',
                                                Lang.bind(this,
                                                          this._onStateChanged));
        this._onStateChanged();
    },

    /** Callback when {@link #actor} is destroyed.
     * We stop listening to {@#app}'s state. */
    _onDestroy: function() {
        if (this._stateChangedId > 0)
            this.app.disconnect(this._stateChangedId);
        this._stateChangedId = 0;
        this._removeMenuTimeout();
    },

    /** This removes the timeout that will open the menu after a long-click of
     * duration {@link MENU_POPUP_TIMEOUT}.
     * @see #_onButtonPress */
    _removeMenuTimeout: function() {
        if (this._menuTimeoutId > 0) {
            Mainloop.source_remove(this._menuTimeoutId);
            this._menuTimeoutId = 0;
        }
    },

    /** Callback when an application's state changed (e.g. running/stopped/starting).
     * If the application is stopped, pseudo style class 'running' is removed.
     * If it is running or starting, pseudo style class 'running' is added.
     *
     * ![AppWellIcon, on the left with pseudo style class 'running'](pics/AppWellIconState.png)
     */
    _onStateChanged: function() {
        if (this.app.state != Shell.AppState.STOPPED)
            this.actor.add_style_class_name('running');
        else
            this.actor.remove_style_class_name('running');
    },

    /** Callback for the button press event of {@link #actor}.
     *
     * Note that a button press is different to a 'click' ({@link #_onClicked})
     * in that a click consists of a button press and a button release in short
     * succession, whereas this is just a button press.
     *
     * If it was a right-button-press (i.e. start of a right click), the popup
     * menu for the icon is shown.
     *
     * If it was a left-button-press *and* the 'clicked' signal did not fire
     * (it was a left button press only with no button release hence did not
     * count as a click, i.e. a "long left click"/"long press" of sorts with
     * the mouse), the popup menu is shown after a delay of {@link MENU_POPUP_TIMEOUT}.
     * @see #popupMenu
     * @see #_onClicked
     */
    _onButtonPress: function(actor, event) {
        let button = event.get_button();
        if (button == 1) {
            this._removeMenuTimeout();
            this._menuTimeoutId = Mainloop.timeout_add(MENU_POPUP_TIMEOUT,
                Lang.bind(this, function() {
                    this.popupMenu();
                }));
        } else if (button == 3) {
            this.popupMenu();
            return true;
        }
        return false;
    },

    /** Callback when {@link #actor} emits a 'clicked' signal.
     *
     * Note that this is different to a button-press ({@link #_onButtonPress})
     * in that a click consists of a button press shortly followed by a button
     * release, whereas a button press event does not have the associated release.
     * (I think the event logic is such that on a button press, if a button release
     * happens shortly after the "clicked" event only is fired, and otherwise the
     * "button-press" event is fired instead).
     *
     * If the click was a left click, {@link #_onActivate} is called
     * (switching to that application's window/launching the application, usually).
     * (This function will emit the 'launching' signal).
     *
     * If the click was a *middle* click, a new window for the application is
     * opened on an empty workspace, being the last workspace (guaranteed to be
     * empty if you have dynamic workspaces) and the 'launching' signal is
     * emitted.
     * @fires .launching
     */
    _onClicked: function(actor, button) {
        this._removeMenuTimeout();

        if (button == 1) {
            this._onActivate(Clutter.get_current_event());
        } else if (button == 2) {
            // Last workspace is always empty
            let launchWorkspace = global.screen.get_workspace_by_index(global.screen.n_workspaces - 1);
            launchWorkspace.activate(global.get_current_time());
            this.emit('launching');
            this.app.open_new_window(-1);
            Main.overview.hide();
        }
        return false;
    },

    /** Callback for the 'popup-menu' signal of {@link #actor}, i.e.
     * the user has pressed the menu key on their keyboard
     * (![](pics/icons/menu_key.gif)).
     * This displays the popup menu and gives it keyboard focus.
     * @see #popupMenu */
    _onKeyboardPopupMenu: function() {
        this.popupMenu();
        this._menu.actor.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
    },

    /** Gets the ID of the app (desktop file name).
     * @returns {string} the ID of {@link #app} being `app.get_id()`;
     * this is usually the desktop file name (e.g. 'evolution.desktop'). */
    getId: function() {
        return this.app.get_id();
    },

    /** Pops up the app icon's {@link AppIconMenu}, creating it if it doesn't
     * exist and storing in {@link #_menu}.
     *
     * The menu contains "New Window" and "Add to/Remove from Favorites" entries,
     * along with other application-specific entries (see {@link AppIconMenu}).
     */
    popupMenu: function() {
        this._removeMenuTimeout();
        this.actor.fake_release();

        if (!this._menu) {
            this._menu = new AppIconMenu(this);
            this._menu.connect('activate-window', Lang.bind(this, function (menu, window) {
                this.activateWindow(window);
            }));
            this._menu.connect('open-state-changed', Lang.bind(this, function (menu, isPoppedUp) {
                if (!isPoppedUp)
                    this._onMenuPoppedDown();
            }));
            Main.overview.connect('hiding', Lang.bind(this, function () { this._menu.close(); }));

            this._menuManager.addMenu(this._menu);
        }

        this.actor.set_hover(true);
        this.actor.show_tooltip();
        this._menu.popup();

        return false;
    },

    /** Activates the specified window. This is connected to the
     * 'activate-window' signal of {@link AppIconMenu}.
     * @see Main.activateWindow
     * @see AppIconMenu.activate-window
     * @inheritparams AppIconMenu.activate-window
     */
    activateWindow: function(metaWindow) {
        if (metaWindow) {
            Main.activateWindow(metaWindow);
        } else {
            Main.overview.hide();
        }
    },

    /** Callback when the popup menu {@link #_menu} is toggled on
     * or off - just calls
     * [`this.actor.sync_hover()`](http://developer.gnome.org/st/stable/StWidget.html#st-widget-sync-hover)
     * which ensures that the icon's hover style is updated depending on where
     * the pointer is */
    _onMenuPoppedDown: function() {
        this.actor.sync_hover();
    },

    /** Callback when the user left-clicks on the icon.
     * Called from {@link #_onClicked}.
     *
     * If {@link #_onActivateOverride} is not null, this is called
     * with the input event (it is expected to somehow launch the app).
     *
     * If it is not set, if the user had CTRL pressed and the app is running,
     * new window for the application is opened. Otherwise, `app.activate()`
     * is called (switches to the most recent window of the application if
     * it is running, or launches it on the current workspace if not).
     * @param {Clutter.Event} event - the click event.
     */
    _onActivate: function (event) {
        this.emit('launching');
        let modifiers = Shell.get_event_state(event);

        if (this._onActivateOverride) {
            this._onActivateOverride(event);
        } else {
            if (modifiers & Clutter.ModifierType.CONTROL_MASK
                && this.app.state == Shell.AppState.RUNNING) {
                this.app.open_new_window(-1);
            } else {
                this.app.activate();
            }
        }
        Main.overview.hide();
    },

    /** Callback if this app icon is dragged over a workspace thumbnail (in
     * the sidebar of the overview) or over the workspacesDisplay (windows
     * tab of the overview) - opens a new window of the application on that
     * workspace.
     * @see SearchDisplay.SearchResult#shellWorkspaceLaunch
     * @inheritparams SearchDisplay.SearchResult#shellWorkspaceLaunch
     */
    shellWorkspaceLaunch : function(params) {
        params = Params.parse(params, { workspace: -1,
                                        timestamp: 0 });

        this.app.open_new_window(params.workspace);
    },

    /** Implementation of {@link DND.Draggable#getDragActor} - the drag
     * actor (what the user "drags"/follows the mouse during a drag) is the
     * icon for the application, created with size {@link Overview.Overview#dashIconSize}.
     * @see DND.Draggable#getDragActor
     * @inheritparams DND.Draggable#getDragActor
     * @todo mark as implements draggable API */
    getDragActor: function() {
        return this.app.create_icon_texture(Main.overview.dashIconSize);
    },

    // Returns the original actor that should align with the actor
    // we show as the item is being dragged.
    /** Implementation of {@link DND.Draggable#getDragActorSource} - this is
     * what appears in the original space when we start dragging (?).
     * @returns {Clutter.Actor} the original actor that should align with the
     * actor we show as the item is being dragged.
     * @see DND.Draggable#getDragActorSource
     * @todo mark as implements draggable API */
    getDragActorSource: function() {
        return this.icon.icon;
    }
};
Signals.addSignalMethods(AppWellIcon.prototype);
/** Emitted when the app well icon launches the application it represents,
 * usually when the user left-clicked the app icon.
 * @name launching
 * @event
 * @memberof AppWellIcon
 */

/** creates a new AppIconMenu.
 *
 * This calls the [parent constructor]{@link PopupMenu.PopupMenu}, creating
 * the popup menu on `source.actor` with the box pointer to on the left
 * side of the popup menu (right side if the text direction is right-to-left) and
 * the box pointer in the middle of the popup menu.
 *
 * We also set {@link PopupMenu.PopupMenu#blockSourceEvents} to `true` in order
 * to keep the AppWellIcon 'hovered' state while the menu is open, regardless
 * of whether we actually are hovering over the icon while we are choosing items
 * from the menu.
 *
 * The menu has style class 'app-well-menu'.
 * @param {AppDisplay.AppWellIcon} source - the AppWellIcon that the popup menu
 * is tied to.
 * @classdesc
 * This is the right-click menu for an {@link AppWellIcon}.
 *
 * It provides a "New Window" and "Add to/Remove from Favorites" item, as
 * well as application-specific items.
 *
 * ![AppWellIcon is an AppIcon and an AppIconMenu](pics/AppWellIcon.png)
 * @extends PopupMenu.PopupMenu
 * @class
 */
function AppIconMenu(source) {
    this._init(source);
}

AppIconMenu.prototype = {
    __proto__: PopupMenu.PopupMenu.prototype,
    _init: function(source) {
        let side = St.Side.LEFT;
        if (St.Widget.get_default_direction() == St.TextDirection.RTL)
            side = St.Side.RIGHT;

        PopupMenu.PopupMenu.prototype._init.call(this, source.actor, 0.5, side);

        // We want to keep the item hovered while the menu is up
        this.blockSourceEvents = true;

        /** The {@link AppWellIcon} this menu is attached to.
         * @type {AppDisplay.AppWellIcon} */
        this._source = source;

        this.connect('activate', Lang.bind(this, this._onActivate));

        this.actor.add_style_class_name('app-well-menu');

        // Chain our visibility and lifecycle to that of the source
        source.actor.connect('notify::mapped', Lang.bind(this, function () {
            if (!source.actor.mapped)
                this.close();
        }));
        source.actor.connect('destroy', Lang.bind(this, function () { this.actor.destroy(); }));

        Main.uiGroup.add_actor(this.actor);
    },

    /** Updates the popup menu according to the app's current state.
     *
     * If the application is running and has windows, an item is added for
     * each of the windows.
     *
     * If the app can have multiple windows/instances, a "New Window" item is added.
     *
     * Also, a "Add to Favorites"/"Remove from Favorites" item is added depending
     * on whether the app is in the favourites.
     */
    _redisplay: function() {
        this.removeAll();

        let windows = this._source.app.get_windows();

        // Display the app windows menu items and the separator between windows
        // of the current desktop and other windows.
        let activeWorkspace = global.screen.get_active_workspace();
        let separatorShown = windows.length > 0 && windows[0].get_workspace() != activeWorkspace;

        for (let i = 0; i < windows.length; i++) {
            if (!separatorShown && windows[i].get_workspace() != activeWorkspace) {
                this._appendSeparator();
                separatorShown = true;
            }
            let item = this._appendMenuItem(windows[i].title);
            item._window = windows[i];
        }

        if (!this._source.app.is_window_backed()) {
            if (windows.length > 0)
                this._appendSeparator();

            let isFavorite = AppFavorites.getAppFavorites().isFavorite(this._source.app.get_id());

            this._newWindowMenuItem = this._appendMenuItem(_("New Window"));
            this._appendSeparator();

            this._toggleFavoriteMenuItem = this._appendMenuItem(isFavorite ? _("Remove from Favorites")
                                                                : _("Add to Favorites"));
        }
    },

    /** Convenience function - adds a separator to our menu. */
    _appendSeparator: function () {
        let separator = new PopupMenu.PopupSeparatorMenuItem();
        this.addMenuItem(separator);
    },

    /** Convenience function - creates a {@link PopupMenu.PopupMenuItem} for
     * the provided text and adds it to our menu, returning the created
     * item.
     * @param {string} labelText - text to put on the menu item
     * @returns {PopupMenu.PopupMenuItem} a popup menu item with the `labelText`
     * on it.
     */
    _appendMenuItem: function(labelText) {
        // FIXME: app-well-menu-item style
        let item = new PopupMenu.PopupMenuItem(labelText);
        this.addMenuItem(item);
        return item;
    },

    /** Opens the menu, but first calls {@link #_redisplay} in
     * order to update the items.
     * @param {?} activatingButton - unused.
     */
    popup: function(activatingButton) {
        this._redisplay();
        this.open();
    },

    /** Callback for the 'activate' signal of the menu, called when any of the
     * menu items in the menu is activated (clicked).
     *
     * If the item was an item for a window of the application, we emit
     * 'activate-window' with that window (the {@link AppWellIcon} listens to
     * this to activate the requested window).
     *
     * If the item was the 'new window' item, we create a new window on the
     * default workspace and emit 'activate-window' with
     * a `null` window (the {@link AppWellIcon} listens to this and hides the
     * overview).
     *
     * If the item was the add/remove favourites item, then the item is added or
     * removed from the favourites.
     *
     * Finally, the menu is closed.
     * @see AppFavorites.AppFavorites
     * @see PopupMenu.PopupMenu#activate
     * @param {AppDisplay.AppIconMenu} actor - the menu instance that emitted the signal (`this`).
     * @param {PopupMenu.PopupBaseMenuItem} child - the popup item that was clicked.
     * @fires .activate-window
     */
    _onActivate: function (actor, child) {
        if (child._window) {
            let metaWindow = child._window;
            this.emit('activate-window', metaWindow);
        } else if (child == this._newWindowMenuItem) {
            this._source.app.open_new_window(-1);
            this.emit('activate-window', null);
        } else if (child == this._toggleFavoriteMenuItem) {
            let favs = AppFavorites.getAppFavorites();
            let isFavorite = favs.isFavorite(this._source.app.get_id());
            if (isFavorite)
                favs.removeFavorite(this._source.app.get_id());
            else
                favs.addFavorite(this._source.app.get_id());
        }
        this.close();
    }
};
Signals.addSignalMethods(AppIconMenu.prototype);
/** Emitted by the {@link AppIconMenu} whenever the user selects to open an
 * existing or new window for that application.
 * @event
 * @name activate-window
 * @memberof AppiconMenu
 * @param {AppDisplay.AppIconMenu} o - the menu instance that emitted the signal
 * @param {?Meta.Window} metaWindow - the window to be activated, or `null`
 * if it has already been activated.
 */
