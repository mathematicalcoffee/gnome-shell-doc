// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file handles the popup menu when you mount an external media offering
 * you options to view photos, browse, play music, etc on the media.
 */

const Lang = imports.lang;
const DBus = imports.dbus;
const Gio = imports.gi.Gio;
const St = imports.gi.St;

const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const ShellMountOperation = imports.ui.shellMountOperation;

// GSettings keys
/** @+
 * @const
 * @default
 * @type {string} */
/** Gsettings schema for media handling */
const SETTINGS_SCHEMA = 'org.gnome.desktop.media-handling';
/** Gsettings key under {@link SETTINGS_SCHEMA} for whether we should *never*
 * autorun. */
const SETTING_DISABLE_AUTORUN = 'autorun-never';
/** Gsettings key under {@link SETTINGS_SCHEMA} containing the list of
 * x-content types for which the user have chosen to start an application in
 * the preference capplet. */
const SETTING_START_APP = 'autorun-x-content-start-app';
/** Gsettings key under {@link SETTINGS_SCHEMA} containing the list of
 * x-content types for which the user have chosen "Do Nothing" in the preference
 * capplet. No prompt will be shown nor will any matching application be started
 * on insertion of media matching these types. */
const SETTING_IGNORE = 'autorun-x-content-ignore';
/** Gsettings key under {@link SETTINGS_SCHEMA} containing the list of x-content
 * types for which the user have chosen "Open Folder" in the preferences capplet.
 *
 * A folder window will be opened on insertion of media matching these types. */
const SETTING_OPEN_FOLDER = 'autorun-x-content-open-folder';
/** @- */

/** The autorun setting for a particular MIME type. This is determined by
 * seeing if the mime type is in various gsettings keys under
 * 'org.gnome.desktop.media-handling' ({@link SETTINGS_SCHEMA}).
 * @see AutorunTransientDispatcher#_getAutorunSettingForType
 * @see AutorunTransientDispatcher#addMount
 * @const
 * @enum */
const AutorunSetting = {
    /** This content type's application should be run immediately.
     * (in key 'autorun-x-content-start-app', {@link SETTING_START_APP}) */
    RUN: 0,
    /** This content type should have no action taken on it.
     * (in key 'autorun-x-content-ignore', {@link SETTING_IGNORE}) */
    IGNORE: 1,
    /** Media with this content type should have the default file browser
     * window opened
     * (in key 'autorun-x-content-open-folder', {@link SETTING_OPEN_FOLDER}) */
    FILES: 2,
    /** Media with this content type should have a {@link AutorunTransientSource}
     * and {@link AutorunTransientNotification} created for them, i.e. we should
     * ask the user whether they want to run the application. */
    ASK: 3
};

// misc utils
/** Whether a mount should have autorun ignored on it.
 *
 * Autorun is ignored for a mount if:
 *
 * + the mount's root is under a hidden directory (starting with '.'); AND
 * + the mount doesn't have a volume, or the volume doesn't allow autorun, or the
 * volume shouldn't automount
 *
 * Upon the user plugging in a drive that should have autorun ignored, nothing
 * happens (no notification gets shown to the user with autorun options).
 * @inheritparams startAppForMount
 * @returns {boolean} whether `mount`'s autorun should be ignored.
 */
function ignoreAutorunForMount(mount) {
    let root = mount.get_root();
    let volume = mount.get_volume();

    if ((root.is_native() && !isMountRootHidden(root)) ||
        (volume && volume.allowAutorun && volume.should_automount()))
        return false;

    return true;
}

/** Returns whether `root`'s path is under a hidden directory (i.e. directory
 * starting with '.').
 * @param {Gio.File} root - the Gio.File whose path to check
 * @returns {boolean} whether any directory in `root`'s path is hidden (starts
 * with a dot '.').
 */
function isMountRootHidden(root) {
    let path = root.get_path();

    // skip any mounts in hidden directory hierarchies
    return (path.indexOf('/.') != -1);
}

/** Starts an application with the URI of a mount (for example starting
 * Nautilus with the URI of a USB).
 *
 * It is up to the application to do something with the URI. For example,
 * Nautilus will open at that directory.
 * @param {Shell.App} app - application
 * @param {Gio.Mount} mount - mount
 */
function startAppForMount(app, mount) {
    let files = [];
    let root = mount.get_root();
    let retval = false;

    files.push(root);

    try {
        retval = app.launch(files, 
                            global.create_app_launch_context())
    } catch (e) {
        log('Unable to launch the application ' + app.get_name()
            + ': ' + e.toString());
    }

    return retval;
}

/******************************************/

/** Definition of the DBus interface
 * 'org.gnome.Shell.HotplugSniffer'.
 *
 * Has method "SniffURI".
 *
 * Wrapped by {@link HotplugSniffer}, see that.
 *
 * This DBus service is provided by the gnome-shell C code (see the
 * [code in the repository](http://git.gnome.org/browse/gnome-shell/tree/src/hotplug-sniffer?h=gnome-3-2)),
 * and we use it from the javascript size.
 * @see [this file](http://cgit.freedesktop.org/xdg/shared-mime-info/tree/freedesktop.org.xml.in)
 * for a list of different mime types.
 * @const
 * @see HotplugSniffer
 */
const HotplugSnifferIface = {
    name: 'org.gnome.Shell.HotplugSniffer',
    /** Retrieves mime types that the `URI` supports.
     * @param {string} URI - uri to sniff
     * @returns {string[]} array of content types the URI supports.
     * @see [this file](http://cgit.freedesktop.org/xdg/shared-mime-info/tree/freedesktop.org.xml.in)
     * for a list of different mime types.
     * @function
     * @memberof HotplugSniffer */
    methods: [{ name: 'SniffURI',
                inSignature: 's',
                outSignature: 'as' }]
};

/** Creates a new HotplugSniffer
 * @classdesc
 *
 * The {@link HotplugSniffer} is a DBus proxy for object
 * '/org/gnome/Shell/HotplugSniffer' on bus
 * 'org.gnome.Shell.HotplugSniffer'.
 *
 * Has method SniffURI(Remote).
 * @see HotplugSnifferIface
 * @see [this file](http://cgit.freedesktop.org/xdg/shared-mime-info/tree/freedesktop.org.xml.in)
 * for a list of different mime types.
 * @class
 */
const HotplugSniffer = function() {
    this._init();
};

HotplugSniffer.prototype = {
    _init: function() {
        DBus.session.proxifyObject(this,
                                   'org.gnome.Shell.HotplugSniffer',
                                   '/org/gnome/Shell/HotplugSniffer');
    },
};
DBus.proxifyPrototype(HotplugSniffer.prototype, HotplugSnifferIface);

/** Creates a new ContentTypeDiscoverer
 *
 * Nothing interesting here, we just store `callback` in {@link #_callback}.
 *
 * Use {@link #guessContentTypes} to actually do anything.
 *
 * @param {function(Gio.Mount, Shell.App[], string[]} callback - function
 * taking in a mount, an array of applications (the default for each mime
 * type the mount supports), and an array of mime types that the mount supports.
 * When the supported mime types of the mount are discovered ({@link #guessContentTypes})
 * this is called.
 * @classdesc
 * This is a convenience class used to get the content types of a mount
 * and generate a list of default applications for each of these types.
 *
 * Upon the content type being guessed, a callback (provided in the constructor)
 * is called.
 * @see [this file](http://cgit.freedesktop.org/xdg/shared-mime-info/tree/freedesktop.org.xml.in)
 * for a list of different mime types.
 * @class
 * @see #_callback
 */
function ContentTypeDiscoverer(callback) {
    this._init(callback);
}

ContentTypeDiscoverer.prototype = {
    _init: function(callback) {
        /** A function taking in a Gio.Mount, array of {@link Shell.App},
         * and array of strings being the content types the mount supports.
         *
         * The applications are the default application for each content
         * type in `contentTypes`.
         *
         * Will be called upon the completion of {@link #guessContentTypes}.
         *
         * This is assigned in the constructor.
         * @function
         * @param {Gio.Mount} mount - a mount
         * @param {Shell.App[]} apps - array of applications, the default one per
         * MIME type supported by `mount`
         * @param {string[]} contentTypes - array of MIME types supported by
         * the mount (e.g. 'x-content/software').
         * @see [this file](http://cgit.freedesktop.org/xdg/shared-mime-info/tree/freedesktop.org.xml.in)
         * for a list of different mime types.
         */
        this._callback = callback;
    },

    /** Guesses what content types a mount supports.
     *
     * As this is asynchronous, {@link #_onContentTypeGuessed} is called
     * once the content types are obtained.
     * @see [`g_mount_guess_content_type`](http://developer.gnome.org/gio/stable/GMount.html#g-mount-guess-content-type)
     * @inheritparams startAppForMount
     */
    guessContentTypes: function(mount) {
        // guess mount's content types using GIO
        mount.guess_content_type(false, null,
                                 Lang.bind(this,
                                           this._onContentTypeGuessed));
    },

    /** Called upon completion of {@link #guessContentTypes}, once the
     * content types for a mount have been determined.
     *
     * If content types were successfully determined, we apply the callback
     * passed in the constructor (see {@link #_emitCallback}).
     *
     * Otherwise we use a {@link HotplugSniffer} to guess the content types and
     * call {@link #_emitCallback} when we get them.
     * @inheritparams startAppForMount
     * @param {?} res - result? use with `g_mount_guess_content_type_finish`.
     * @see #_emitCallback
     */
    _onContentTypeGuessed: function(mount, res) {
        let contentTypes = [];

        try {
            contentTypes = mount.guess_content_type_finish(res);
        } catch (e) {
            log('Unable to guess content types on added mount ' + mount.get_name()
                + ': ' + e.toString());
        }

        if (contentTypes.length) {
            this._emitCallback(mount, contentTypes);
        } else {
            let root = mount.get_root();

            let hotplugSniffer = new HotplugSniffer();
            hotplugSniffer.SniffURIRemote
                (root.get_uri(), DBus.CALL_FLAG_START,
                 Lang.bind(this, function(contentTypes) {
                     this._emitCallback(mount, contentTypes);
                 }));
        }
    },

    /** Called once content types for the mount have been determined.
     *
     * This generates a list of the default application for each content
     * type in `contentTypes` (if there are none, then it gets the default
     * application for content type 'inode/directory', i.e. the default
     * file browser).
     *
     * It thens call {@link #_callback} on the list of applications and
     * content types (this is the `callback` argument fed in to the
     * constructor).
     * @inheritparams startAppForMount
     * @param {string[]} contentTypes - content types for `mount` (e.g.
     * 'x-content/software').
     * @see [this file](http://cgit.freedesktop.org/xdg/shared-mime-info/tree/freedesktop.org.xml.in)
     * for a list of different mime types.
     */
    _emitCallback: function(mount, contentTypes) {
        if (!contentTypes)
            contentTypes = [];

        // we're not interested in win32 software content types here
        contentTypes = contentTypes.filter(function(type) {
            return (type != 'x-content/win32-software');
        });

        let apps = [];
        contentTypes.forEach(function(type) {
            let app = Gio.app_info_get_default_for_type(type, false);

            if (app)
                apps.push(app);
        });

        if (apps.length == 0)
            apps.push(Gio.app_info_get_default_for_type('inode/directory', false));

        this._callback(mount, apps, contentTypes);
    }
}

/** creates a new AutorunManager
 *
 * We first create get the default {@link Gio.VolumeMonitor} and store it in
 * {@link #_volumeMonitor}. This is used to listen for mounts being added
 * or removed.
 *
 * Then we create a {@link AutorunTransientDispatcher} instance and store
 * it in {@link #_transDispatcher}.
 *
 * We create a source for our resident notifications in the message tray
 * with {@link #_createResidentSource}.
 *
 * Finally for each mount in the volume monitor, we use a
 * {@link ContentTypeDiscoverer} to guess the supported mime types of the
 * mount as well as the default application for each of these mime types,
 * and upon completion call {@link AutorunResidentSource#addMount} to
 * create a notification for the mount on the message tray.
 *
 * @classdesc
 * This is the autorun manager for gnome shell. You only need one instance,
 * and that is stored in {@link Main.autorunManager}.
 *
 * Whenever a mount is added (e.g. plug in a USB drive), the autorun manager:
 *
 * 1. creates a popup notification ({@link AutorunTransientNotification})
 * informing the user showing buttons for the default application for each
 * detected MIME type for the mount, along with an "Eject" button
 * (see {@link AutorunTransientDispatcher#addMount});
 * 2. updates the resident notification (that lives in the message tray) with
 * a list of the current mounts that are plugged in (see
 * {@link AutorunResidentNotification}, {@link AutorunResidentSource#addMount}).
 *
 * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
 *
 * ![an {@link AutorunResidentNotification}](pics/autorunResidentNotification.png)
 *
 * @class
 */
function AutorunManager() {
    this._init();
}

AutorunManager.prototype = {
    _init: function() {
        /** A volume monitor to know when mounts are added or removed.
         * @type {Gio.VolumeMonitor} */
        this._volumeMonitor = Gio.VolumeMonitor.get();

        this._volumeMonitor.connect('mount-added',
                                    Lang.bind(this,
                                              this._onMountAdded));
        this._volumeMonitor.connect('mount-removed',
                                    Lang.bind(this,
                                              this._onMountRemoved));

        /** This handles the creation of popup notifications
         * ({@link AutorunTransientNotification}) for each mount showing
         * options to open the mount with particular applications.
         * @see AutorunTransientNotification
         * @type {AutorunTransientDispatcher} */
        this._transDispatcher = new AutorunTransientDispatcher();
        this._createResidentSource();

        let mounts = this._volumeMonitor.get_mounts();

        mounts.forEach(Lang.bind(this, function (mount) {
            let discoverer = new ContentTypeDiscoverer(Lang.bind (this, 
                function (mount, apps) {
                    this._residentSource.addMount(mount, apps);
                }));

            discoverer.guessContentTypes(mount);
        }));
    },

    /** Ensures that we have a {@link AutorunResidentSource} in the message tray
     * to show the list of currently-plugged-in devices with their eject buttons.
     * */
    _createResidentSource: function() {
        /** The Autorun manager has one Source in the message tray whose
         * (resident) notification shows a list of mounts currently plugged
         * in with eject buttons for each.
         *
         * This is the Source.
         * @type {AutorunResidentSource} */
        this._residentSource = new AutorunResidentSource();
        this._residentSource.connect('destroy',
                                     Lang.bind(this,
                                               this._createResidentSource));
    },

    /** Callback when a mount is plugged in ({@link #_volumeMonitor}'s
     * 'mount-added' signal).
     *
     * This uses a {@link ContentTypeDiscover} to guess the content types
     * for the newly-added mount `mount`, and upon getting them we:
     *
     * 1. Create a {@link AutorunTransientNotification} for the mount
     * (see {@link AutorunTransientDispatcher#addMount});
     * 2. Update the notification from our {@link #_residentSource} in the
     * message tray to show the new mount (see
     * {@link AutorunResidentSource#addMount}).
     *
     * @param {Gio.VolumeMonitor} monitor - monitor that emitted the signal
     * ({@link #_monitor})
     * @param {Gio.Mount} mount - the mount
     * @see AutorunTransientDispatcher#addMount
     * @see AutorunResidentSource#addMount
     * @see ContentTypeDiscoverer#guessContentTypes
     */
    _onMountAdded: function(monitor, mount) {
        // don't do anything if our session is not the currently
        // active one
        if (!Main.automountManager.ckListener.sessionActive)
            return;

        let discoverer = new ContentTypeDiscoverer(Lang.bind (this,
            function (mount, apps, contentTypes) {
                this._transDispatcher.addMount(mount, apps, contentTypes);
                this._residentSource.addMount(mount, apps);
            }));

        discoverer.guessContentTypes(mount);
    },

    /** Callback when a mount is removed/ejected ({@link #_volumeMonitor}'s
     * 'mount-removed' signal).
     *
     * We:
     *
     * 1. remove the transient popup notification for the mount (if it still exists);
     * 2. remove the mount from our resident source's (in the message tray)
     * notification
     *
     * @see AutorunTransientDispatcher#removeMount
     * @see AutorunResidentSource#removeMount
     * @inheritparams #_onMountAdded
     */
    _onMountRemoved: function(monitor, mount) {
        this._transDispatcher.removeMount(mount);
        this._residentSource.removeMount(mount);
    },

    /** Ejects a mount.
     *
     * First, we create a {@link ShellMountOperation.ShellMountOperation} for
     * the mount; this will monitor any user interaction requests from the
     * mount (like "Device is busy, can't remove") and present them to the
     * user in various dialogs/notifications.
     *
     * Then, we stop, eject or unmount the drive, calling {@link #_onStop},
     * {@link #_onEject} or {@link #_onUnmount} upon completion (I don't know
     * what the difference is really).
     *
     * The stop/eject/unmount is done with the previously-created
     * ShellMountOperation to allow user interaction if need be.
     *
     * @inheritparams #_onMountAdded
     */
    ejectMount: function(mount) {
        let mountOp = new ShellMountOperation.ShellMountOperation(mount);

        // first, see if we have a drive
        let drive = mount.get_drive();
        let volume = mount.get_volume();

        if (drive &&
            drive.get_start_stop_type() == Gio.DriveStartStopType.SHUTDOWN &&
            drive.can_stop()) {
            drive.stop(0, mountOp.mountOp, null,
                       Lang.bind(this, this._onStop));
        } else {
            if (mount.can_eject()) {
                mount.eject_with_operation(0, mountOp.mountOp, null,
                                           Lang.bind(this, this._onEject));
            } else if (volume && volume.can_eject()) {
                volume.eject_with_operation(0, mountOp.mountOp, null,
                                            Lang.bind(this, this._onEject));
            } else if (drive && drive.can_eject()) {
                drive.eject_with_operation(0, mountOp.mountOp, null,
                                           Lang.bind(this, this._onEject));
            } else if (mount.can_unmount()) {
                mount.unmount_with_operation(0, mountOp.mountOp, null,
                                             Lang.bind(this, this._onUnmount));
            }
        }
    },

    /** Callback when a mount is unmounted. Finishes the unmount with
     * [`g_mount_unmount_with_operation_finish`](http://developer.gnome.org/gio/stable/GMount.html#g-mount-unmount-with-operation-finish).
     */
    _onUnmount: function(mount, res) {
        try {
            mount.unmount_with_operation_finish(res);
        } catch (e) {
            // FIXME: we need to ignore G_IO_ERROR_FAILED_HANDLED errors here
            // but we can't access the error code from JS.
            // See https://bugzilla.gnome.org/show_bug.cgi?id=591480
            log('Unable to eject the mount ' + mount.get_name() 
                + ': ' + e.toString());
        }
    },

    /** Callback when a mount is ejected. Finishes the eject with
     * [`g_mount_eject_with_operation_finish`](http://developer.gnome.org/gio/stable/GMount.html#g-mount-eject-with-operation-finish).
     */
    _onEject: function(source, res) {
        try {
            source.eject_with_operation_finish(res);
        } catch (e) {
            // FIXME: we need to ignore G_IO_ERROR_FAILED_HANDLED errors here
            // but we can't access the error code from JS.
            // See https://bugzilla.gnome.org/show_bug.cgi?id=591480
            log('Unable to eject the drive ' + source.get_name() 
                + ': ' + e.toString());
        }
    },

    /** Callback when a mount is stopped. Finishes the stop with
     * [`g_mount_stop_with_operation_finish`](http://developer.gnome.org/gio/stable/GMount.html#g-mount-stop-with-operation-finish).
     */
    _onStop: function(drive, res) {
        try {
            drive.stop_finish(res);
        } catch (e) {
            // FIXME: we need to ignore G_IO_ERROR_FAILED_HANDLED errors here
            // but we can't access the error code from JS.
            // See https://bugzilla.gnome.org/show_bug.cgi?id=591480
            log('Unable to stop the drive ' + drive.get_name() 
                + ': ' + e.toString());
        }
    },
}

/** creates a new AutorunResidentSource
 *
 * We call the [parent constructor]{@link MessageTray.Source} with
 * "Removable Devices" as the title.
 *
 * Then we create a our single {@link AutorunResidentNotification} and store
 * it in {@link #_notification}.
 *
 * Finally we set the source's summary item's icon (in the message tray).
 *
 * @classdesc
 * The {@link AutorunManager} keeps a source in the message tray from which
 *  to show a notification with a list of currently-plugged0in mounts.
 *
 * ![an {@link AutorunResidentNotification}](pics/autorunResidentNotification.png)
 *
 * This is that source. It is called "resident" because its notification
 * ({@link AutorunResidentNotification}) is resident.
 *
 * Note that the source only has *one* notification, which shows all the mounts.
 * 
 * @class
 * @extends MessageTray.Source
 * @see AutorunResidentNotification
 */
function AutorunResidentSource() {
    this._init();
}

AutorunResidentSource.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function() {
        MessageTray.Source.prototype._init.call(this, _("Removable Devices"));

        /** Array where each element is an object storing the {@link Gio.Mount}
         * and the list of {@link Shell.App}s it can be launched with.
         * @type {Array.<{mount: Gio.Mount, apps: Shell.App[]}>} */
        this._mounts = [];

        /** The notification for this source (shows a list of mounts with
         * an eject button for each).
         * @type {AutorunResidentNotification} */
        this._notification = new AutorunResidentNotification(this);
        this._setSummaryIcon(this.createNotificationIcon());
    },

    /** Adds a mount to our notification.
     *
     * It does this by adding the mount and its applications to {@link #_mounts},
     * and then calling {@link #_redisplay}.
     * @see #_redisplay
     * @inheritparams ContentTypeDiscoverer#_callback
     */
    addMount: function(mount, apps) {
        if (ignoreAutorunForMount(mount))
            return;

        let filtered = this._mounts.filter(function (element) {
            return (element.mount == mount);
        });

        if (filtered.length != 0)
            return;

        let element = { mount: mount, apps: apps };
        this._mounts.push(element);
        this._redisplay();
    },

    /** Removes a mount from our notification.
     *
     * Just removes the mount from {@link #_mounts} and calls
     * {@link #_redisplay} to actually update the notification.
     * @inheritparams ContentTypeDiscoverer#_callback
     */
    removeMount: function(mount) {
        this._mounts =
            this._mounts.filter(function (element) {
                return (element.mount != mount);
            });

        this._redisplay();
    },

    /** Updates our notification {@link #_notification} so that the list of
     * mounts on it is up to date.
     *
     * If there are no longer any mounts we destroy the notification and
     * this source, removing it from the message tray.
     *
     * Otherwise we call {@link AutorunResidentNotification#updateForMounts}
     * to update the notification. */
    _redisplay: function() {
        if (this._mounts.length == 0) {
            this._notification.destroy();
            this.destroy();

            return;
        }

        this._notification.updateForMounts(this._mounts);

        // add ourselves as a source, and push the notification
        if (!Main.messageTray.contains(this)) {
            Main.messageTray.add(this);
            this.pushNotification(this._notification);
        }
    },

    /** Implements the parent function, providing an icon for this source.
     * The icon is the 'media-removable' icon in colour
     * ![media-removable](pics/icons/media-removable.png).
     * @override */
    createNotificationIcon: function() {
        return new St.Icon ({ icon_name: 'media-removable',
                              icon_type: St.IconType.FULLCOLOR,
                              icon_size: this.ICON_SIZE });
    }
}

/** creates a new AutorunResidentNotification
 *
 * We call the [parent constructor]{@link MessageTray.Notification} with `source`
 * as the source, `source.title` ("Removable Devices") as the notification
 * title, and specifying that this notification has custom content.
 *
 * We then set the notification to be resident.
 *
 * Finally we create a vertical {@link St.BoxLayout} into which we will place
 * the icon/name/eject button for each mount, {@link #_layout}.
 *
 * @param {AutorunResidentSource} source - source the notification belongs to.
 * @classdesc
 * ![an {@link AutorunResidentNotification}](pics/autorunResidentNotification.png)
 *
 * This is the notification that the {@link AutorunManager} puts into the
 * message tray. It shows an item for each device currently plugged in
 * (icon, name and eject button).
 *
 * Use {@link #updateForMounts} to update the list of mounts shown on the
 * notification.
 *
 * The notification is resident.
 * @see AutorunResidentSource
 * @class
 * @extends MessageTray.Notification
 */
function AutorunResidentNotification(source) {
    this._init(source);
}

AutorunResidentNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source) {
        MessageTray.Notification.prototype._init.call(this, source,
                                                      source.title, null,
                                                      { customContent: true });

        // set the notification as resident
        this.setResident(true);

        /** A vertical box layout. We put the icon/name/eject button for
         * each mount into this. Style class 'hotplug-resident-box'.
         * @see #_itemForMount
         * @type {St.BoxLayout} */
        this._layout = new St.BoxLayout ({ style_class: 'hotplug-resident-box',
                                           vertical: true });

        this.addActor(this._layout,
                      { x_expand: true,
                        x_fill: true });
    },

    /** Updates the notification to show items for each of the mounts
     * in `mounts`.
     *
     * We fill {@link #_layout} with the icon/name/eject button for each
     * mount using {@link #_itemForMount}.
     * @param {Array.<{mount: Gio.Mount, apps: Shell.App[]} mounts - mounts
     *  to show in the notification (see {@link AutorunResidentSource#_mounts}).
     * @see #_itemForMount
     */
    updateForMounts: function(mounts) {
        // remove all the layout content
        this._layout.destroy_children();

        for (let idx = 0; idx < mounts.length; idx++) {
            let element = mounts[idx];

            let actor = this._itemForMount(element.mount, element.apps);
            this._layout.add(actor, { x_fill: true,
                                      expand: true });
        }
    },

    /** Creates the UI element for a particular mount in the notification.
     *
     * ![an {@link AutorunResidentNotification}](pics/autorunResidentNotification.png)
     *
     * This consists of:
     *
     * * a clickable {@link St.Button} (style class 'hotplug-resident-mount')
     *   containing:
     *   + a {@link St.Icon} for the mount - style class 'hotplug-resident-mount-icon';
     *   + a {@link St.Label} with the mount's name, style class
     *     'hotplug-resident-mount-label';
     * * an eject button to the far right: the 'media-eject' icon in a button
     *   with style 'hotplug-resedent-eject-button'.
     *
     * Clicking on the button containing the mount icon/name launches the
     * mount with the first app in its list: see {@link startAppForMount}.
     *
     * Clicking on the eject button calls {@link AutorunManager#ejectMount}
     * to eject the mount.
     *
     * @see AutorunManager#ejectMount
     * @see startAppForMount
     * @inheritparams AutorunManager#_callback
     */
    _itemForMount: function(mount, apps) {
        let item = new St.BoxLayout();

        // prepare the mount button content
        let mountLayout = new St.BoxLayout();

        let mountIcon = new St.Icon({ gicon: mount.get_icon(),
                                      style_class: 'hotplug-resident-mount-icon' });
        mountLayout.add_actor(mountIcon);

        let labelBin = new St.Bin({ y_align: St.Align.MIDDLE });
        let mountLabel =
            new St.Label({ text: mount.get_name(),
                           style_class: 'hotplug-resident-mount-label',
                           track_hover: true,
                           reactive: true });
        labelBin.add_actor(mountLabel);
        mountLayout.add_actor(labelBin);

        let mountButton = new St.Button({ child: mountLayout,
                                          x_align: St.Align.START,
                                          x_fill: true,
                                          style_class: 'hotplug-resident-mount',
                                          button_mask: St.ButtonMask.ONE });
        item.add(mountButton, { x_align: St.Align.START,
                                expand: true });

        let ejectIcon = 
            new St.Icon({ icon_name: 'media-eject',
                          style_class: 'hotplug-resident-eject-icon' });

        let ejectButton =
            new St.Button({ style_class: 'hotplug-resident-eject-button',
                            button_mask: St.ButtonMask.ONE,
                            child: ejectIcon });
        item.add(ejectButton, { x_align: St.Align.END });

        // now connect signals
        mountButton.connect('clicked', Lang.bind(this, function(actor, event) {
            startAppForMount(apps[0], mount);
        }));

        ejectButton.connect('clicked', Lang.bind(this, function() {
            Main.autorunManager.ejectMount(mount);
        }));

        return item;
    },
}

/** creates a new AutorunTransientDispatcher
 * 
 * We initialise the gsettings {@link SETTINGS_SCHEMA} in {@link #_settings} and
 * do not much else (use {@link #addMount} to get things happening).
 * @classdesc
 * The AutorunTransientDispatcher handles the creation of
 * {@link AutorunTransientNotification}s for mounts.
 *
 * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
 *
 * More specifically, one adds a mount to it using {@link #addMount} and an
 * action is taken based off the first content type for that mount according
 * to settings in the media-handling gsettings schema {@link SETTINGS_SCHEMA}:
 *
 * * if the content type is in key
 *   'org.gnome.desktop.media-handling.autorun-x-content-start-app', we start
 *   the application with the mount straight away;
 * * if the content type is in 'autorun-x-content-open-folder', we open
 *   the default file browser at this mount's URI straight away;
 * * if the content type is in 'autorun-x-content-ignore', we do nothing;
 * * otherwise, we create a {@link AutorunTransientSource} and
 *   {@link AutorunTransientNotification} for the mount giving the user the
 *   choice to launch an application.
 *
 * @class
 */
function AutorunTransientDispatcher() {
    this._init();
}

AutorunTransientDispatcher.prototype = {
    _init: function() {
        /** The list of sources we manage (one per mount).
         * @type {AutorunTransientSource[]} */
        this._sources = [];
        /** GSettings for media handling ({@link SETTINGS_SCHEMA}).
         * @type {Gio.Settings} */
        this._settings = new Gio.Settings({ schema: SETTINGS_SCHEMA });
    },

    /** Gets the autorun setting for a MIME type.
     * @param {string} contentType - content type (e.g. 'x-content/software')
     * @returns {AutorunSetting} - the autorun setting for this type. Either RUN
     * (launch the application with the mount immediately), IGNORE (don't do
     * anything), OPEN_FOLDER (open the file browser with the mount immediately),
     * or ASK (ask the user whether to launch the application).
     */
    _getAutorunSettingForType: function(contentType) {
        let runApp = this._settings.get_strv(SETTING_START_APP);
        if (runApp.indexOf(contentType) != -1)
            return AutorunSetting.RUN;

        let ignore = this._settings.get_strv(SETTING_IGNORE);
        if (ignore.indexOf(contentType) != -1)
            return AutorunSetting.IGNORE;

        let openFiles = this._settings.get_strv(SETTING_OPEN_FOLDER);
        if (openFiles.indexOf(contentType) != -1)
            return AutorunSetting.FILES;

        return AutorunSetting.ASK;
    },

    /** Looks up the {@link AutorunTransientSource} for `mount`.
     * @inheritparams AutorunManager#_callback
     * @returns {?AutorunTransientSource} the source corresponding to `mount`,
     * or `null` if it doesn't exist.
     */
    _getSourceForMount: function(mount) {
        let filtered =
            this._sources.filter(function (source) {
                return (source.mount == mount);
            });

        // we always make sure not to add two sources for the same
        // mount in addMount(), so it's safe to assume filtered.length
        // is always either 1 or 0.
        if (filtered.length == 1)
            return filtered[0];

        return null;
    },

    /** Creates a {@link AutorunTransientSource} for `mount` (which creates
     * a {@link AutorunTransientNotification} and notifies it to the user).
     *
     * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
     *
     * The source is added to {@link #_sources}.
     * @inheritparams AutorunManager#_callback
     */
    _addSource: function(mount, apps) {
        // if we already have a source showing for this 
        // mount, return
        if (this._getSourceForMount(mount))
            return;
     
        // add a new source
        this._sources.push(new AutorunTransientSource(mount, apps));
    },

    /** Call this to add a mount to the dispatcher.
     *
     * If autorun is disabled globally (in the gsettings), we do nothing.
     *
     * If the mount doesn't want to be autorun (see {@link ignoreAutorunForMount}),
     * we do nothing.
     *
     * Otherwise, we work out what sort of autorun the first content type supports
     * ({@link #_getAutorunSettingForType}).
     *
     * If it's {@link AutorunSetting.IGNORE}, we do nothing.
     *
     * If it's {@link AutorunSetting.RUN} or {@link AutorunSetting.FILES}, we
     * launch the default application for the first type automatically using
     * {@link startAppForMount}.
     *
     * Otherwise (the setting is ASK, or we failed to launch the default app),
     * we create a popup notification ({@link AutorunTransientSource},
     * {@link AutorunTransientNotification}) asking the user what action
     * they want to take.
     *
     * @see #_addSource
     * @see #_getAutorunSettingForType
     * @inheritparams AutorunManager#_callback
     */
    addMount: function(mount, apps, contentTypes) {
        // if autorun is disabled globally, return
        if (this._settings.get_boolean(SETTING_DISABLE_AUTORUN))
            return;

        // if the mount doesn't want to be autorun, return
        if (ignoreAutorunForMount(mount))
            return;

        let setting = this._getAutorunSettingForType(contentTypes[0]);

        // check at the settings for the first content type
        // to see whether we should ask
        if (setting == AutorunSetting.IGNORE)
            return; // return right away

        let success = false;
        let app = null;

        if (setting == AutorunSetting.RUN) {
            app = Gio.app_info_get_default_for_type(contentTypes[0], false);
        } else if (setting == AutorunSetting.FILES) {
            app = Gio.app_info_get_default_for_type('inode/directory', false);
        }

        if (app)
            success = startAppForMount(app, mount);

        // we fallback here also in case the settings did not specify 'ask',
        // but we failed launching the default app or the default file manager
        if (!success)
            this._addSource(mount, apps);
    },

    /** Removes a mount from the dispatcher.
     *
     * This destroys the mount's transient source/notification if they exist.
     * @inheritparams AutorunManager#_callback
     */
    removeMount: function(mount) {
        let source = this._getSourceForMount(mount);
        
        // if we aren't tracking this mount, don't do anything
        if (!source)
            return;

        // destroy the notification source
        source.destroy();
    }
}

/** creates a new AutorunTransientSource
 *
 * We call the [parent constructor]{@link MessageTray.Source} with `mount`'s
 * name as the title.
 *
 * We then create a {@link AutorunTransientNotification}, add ourselves to
 * the message tray ({@link MessageTray.MessageTray#add}), and notify the
 * notification ({@link MessageTray.Source#notify}).
 * @inheritparams AutorunManager#_callback
 * @classdesc
 * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
 *
 * This is made when the user plugs in a device (like a USB stick) and the
 * device does *not* autorun (if it autoruns there is no need for a notification).
 *
 * Oddly enough, an AutorunTransientSource is not a transient source (see
 * {@link #setTransient}).
 *
 * Rather, it is the source that corresponds to a {@link AutorunTransientNotification}
 * which is a transient notification.
 *
 * Hence, this source's icon won't actually be seen in the message tray.
 *
 * However it exists because all notifications (even transient ones) require
 * a source to be notified from.
 *
 * Also, it is convenient in that creating an {@link AutorunTransientSource}
 * *automatically* creates the corresponding {@link AutorunTransientNotification}
 * and notifies it.
 *
 * The icon for the source is the icon of the mount.
 *
 * Created from {@link AutorunTransientDispatcher#_addSource}.
 * @class
 * @extends MessageTray.Source
 */
function AutorunTransientSource(mount, apps) {
    this._init(mount, apps);
}

AutorunTransientSource.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function(mount, apps) {
        MessageTray.Source.prototype._init.call(this, mount.get_name());

        /** The underlying mount for the notification.
         * @type {Gio.Mount} */
        this.mount = mount;
        /** Array of applications that this mount can be opened with (for
         * example a USB stick can be opened with Nautilus, an mp3 player
         * can be opened with Nautilus/your music player).
         * @type {Shell.App[]} */
        this.apps = apps;

        this._notification = new AutorunTransientNotification(this);
        this._setSummaryIcon(this.createNotificationIcon());

        // add ourselves as a source, and popup the notification
        Main.messageTray.add(this);
        this.notify(this._notification);
    },

    /** Implements the parent function to create the icon for the source.
     *
     * This is the icon for the mount {@link #mount}.
     * @override */
    createNotificationIcon: function() {
        return new St.Icon({ gicon: this.mount.get_icon(),
                             icon_size: this.ICON_SIZE });
    }
}

/** creates a new AutorunTransientNotification
 *
 * First the [parent constructor]{@link MessageTray.Notification} is called
 * with `source` as the source (in reality an {@link AutorunTransientSource}),
 * `source.title` as the notification title, and custom content.
 *
 * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
 *
 * Then we construct the notification:
 *
 * 1. create a BoxLayout to hold our custom content ({@link #_box}) and
 * {@link #addActor} it to ourselves;
 * 2. for each app in [`source.apps`]{@link AutorunTransientSource#apps} we
 * create a "Open with [appname]" button and add it to the notification;
 * create a button for that action ({@link #_buttonForApp}) and add it to the
 * notification;
 * 3. add an 'Eject' button ({@link #_buttonForEject}).
 *
 * We set the notification to be transient and of [CRITICAL]{@link MessageTray.Urgency.CRITICAL}
 * urgency, so that it pops up fully expanded.
 * @classdesc
 * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
 *
 * An AutorunTransientNotification is the notification that pops up from the
 * bottom of the screen when you plug in a device that does *not* autorun
 * (if the device autoruns there is no need for the notification).
 *
 * The notification has custom content, consisting of a button per application
 * that is relevant to the mount (for example a camera's SD card might have
 * relevant applications Nautilus (file browser) and Shotwell (image viewer)).
 * Each button has text "Open with [application name]" and clicking on it
 * will attempt to open the mount with the application ({@link startAppForMount}).
 *
 * Since the notification is urgent it will not go away until you click it
 * (quite annoying!). It is also transient, so once dismissed it's gone forever
 * and cannot be brought back by clicking on its {@link AutorunTransientSource}.
 * @class
 * @extends MessageTray.Notification
 * @todo do you need inheritparams with extends? this has a `source` notification
 */
function AutorunTransientNotification(source) {
    this._init(source);
}

AutorunTransientNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source) {
        MessageTray.Notification.prototype._init.call(this, source,
                                                      source.title, null,
                                                      { customContent: true });

        /** The St.BoxLayout we put all our buttons into. Vertical, style
         * class 'hotplug-transient-box'.
         * @type {St.BoxLayout} */
        this._box = new St.BoxLayout({ style_class: 'hotplug-transient-box',
                                       vertical: true });
        this.addActor(this._box);

        this._mount = source.mount;

        source.apps.forEach(Lang.bind(this, function (app) {
            let actor = this._buttonForApp(app);

            if (actor)
                this._box.add(actor, { x_fill: true,
                                       x_align: St.Align.START });
        }));

        this._box.add(this._buttonForEject(), { x_fill: true,
                                                x_align: St.Align.START });

        // set the notification to transient and urgent, so that it
        // expands out
        this.setTransient(true);
        this.setUrgency(MessageTray.Urgency.CRITICAL);
    },

    /** Creates a button for an application (that can do something with this
     * mount. For example, a camera's SD card might have Nautilus and Shotwell
     * (image viewer) as applications).
     *
     * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
     *
     * The button says "Open with [application name]", and clicking it
     * calls {@link startAppForMount} with the mount and application and
     * destroys the notification.
     *
     * @param {Shell.App} app - application
     * @returns {St.Button} the button to add to the notification
     */
    _buttonForApp: function(app) {
        let box = new St.BoxLayout();
        let icon = new St.Icon({ gicon: app.get_icon(),
                                 style_class: 'hotplug-notification-item-icon' });
        box.add(icon);

        let label = new St.Bin({ y_align: St.Align.MIDDLE,
                                 child: new St.Label
                                 ({ text: _("Open with %s").format(app.get_display_name()) })
                               });
        box.add(label);

        let button = new St.Button({ child: box,
                                     x_fill: true,
                                     x_align: St.Align.START,
                                     button_mask: St.ButtonMask.ONE,
                                     style_class: 'hotplug-notification-item' });

        button.connect('clicked', Lang.bind(this, function() {
            startAppForMount(app, this._mount);
            this.destroy();
        }));

        return button;
    },

    /** Create an 'eject' button for the notification.
     *
     * ![an {@link AutorunTransientNotification}](pics/autorunTransientNotification.png)
     *
     * It has an icon 'media-eject' and text "Eject" and is connected up
     * such that clicking on it will call {@link AutorunManager#ejectMount} on
     * the mount to eject it.
     * @returns {St.Button} the Eject button to add to the notification
     * @todo media-eject
     * @see AutorunManager#ejectMount */
    _buttonForEject: function() {
        let box = new St.BoxLayout();
        let icon = new St.Icon({ icon_name: 'media-eject',
                                 style_class: 'hotplug-notification-item-icon' });
        box.add(icon);

        let label = new St.Bin({ y_align: St.Align.MIDDLE,
                                 child: new St.Label
                                 ({ text: _("Eject") })
                               });
        box.add(label);

        let button = new St.Button({ child: box,
                                     x_fill: true,
                                     x_align: St.Align.START,
                                     button_mask: St.ButtonMask.ONE,
                                     style_class: 'hotplug-notification-item' });

        button.connect('clicked', Lang.bind(this, function() {
            Main.autorunManager.ejectMount(this._mount);
        }));

        return button;
    }
}

