// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the popup menus and items that can go in them (for example the popup
 * menu from the user menu).
 *
 * ![A Popup Menu displaying various items](pics/PopupMenu.collapsed.png)
 */

const Cairo = imports.cairo;
const Clutter = imports.gi.Clutter;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const BoxPointer = imports.ui.boxpointer;
const Main = imports.ui.main;
const Params = imports.misc.params;
const Tweener = imports.ui.tweener;

/** When a scroll event is received on a {@link PopupSliderMenuItem},
 * this is the percentage that gets scrolled.
 * @const
 * @default
 * @type {number} */
const SLIDER_SCROLL_STEP = 0.05; /* Slider scrolling step in % */

/** Ensures `actor` (a St object) and all its descendants have read their
 * style information.
 *
 * @param {St.Widget} actor - the actor to ensure has its style (and children).
 * @see [`st_widget_ensure_style`](http://developer.gnome.org/st/stable/StWidget.html#st-widget-ensure-style) */
function _ensureStyle(actor) {
    if (actor.get_children) {
        let children = actor.get_children();
        for (let i = 0; i < children.length; i++)
            _ensureStyle(children[i]);
    }

    if (actor instanceof St.Widget)
        actor.ensure_style();
}

/** creates a new PopupBaseMenuItem
 *
 * ![A {@link PopupMenuItem} is a {@link PopupBaseMenuItem} with a label](pics/PopupMenuItem.png)
 *
 * The top-level actor {@link #actor} is a Shell.GenericContainer. It is
 * empty. Subclasses should add content to this using {@link #addActor}.
 *
 * We set a number of variables saying whether the item is:
 *
 * We then determine whether the item is *activatable*.
 * If `params.reactive` or `params.activate` were explicitly
 * set to `false` in the constructor, then the item cannot be activated
 * at all (even if it is technically reactive) - that is, the {@link #activate}
 * method and signal will not be called/emitted by this item ever. Stored
 * in {@link #_activateable}, not reversible.
 *
 * Note that this is different to whether the item is *currently* active. This
 * is a property that can be set with {@link #setActive} and is really just a
 * visual effect.
 *
 * If the item is activatable (`params.reactive` and `params.activate` were
 * both `true` or not provided), we connect up {@link #actor}'s button release
 * and key press events (which will eventually call {@link #activate}, sending
 * a signal out to any listeners).
 *
 * If the item is hoverable (`params.hover` was `true` or not provided),
 * we listen to the 'notify::hover' event in order to set the item as
 * active ({@link #setActive}) whilst it is being hovered over - this just
 * adds a pseudo style class 'active' to the item for a visual effect.
 *
 * If the item is reactive (`params.reactive`), it responds to keyboard
 * navigation so we connect up the item's keyboard focus event in order to
 * style the item with {@link #setActive} in the same way as the hover effect.
 *
 * @param {Object} [params] - parameters for the item.
 * @param {boolean} [params.reactive=true] - whether the item should be
 * reactive ({@link #actor} receives events that you can connect to directly).
 * @param {boolean} [params.activate=true] - whether the item as a whole
 * should be interactive/activatable (e.g. slideable for a
 * {@link PopupSliderMenuItem}, or clickable for a {@link PopupMenuItem}).
 * This will only occur if BOTH `params.reactive` and `params.activate` are
 * `true`.
 * @param {boolean} [params.hover=true] - whether the item will have a different
 * style when hovered over (taken from the ':active' pseudo style class
 * of {@link #actor})
 * @param {boolean} [params.sensitive=true] - whether the item is initially
 * sensitive (enabled). An insensitive item looks "greyed out" (see
 * {@link #setSensitive}).
 * @param {?string} [params.style_class=null] - any additional
 * style classes to apply to {@link #actor} besides the default 'popup-menu-item'.
 *
 * @classdesc
 * This is the base class for all items that will appear in a popup menu.
 *
 * It is an "empty" popup menu item - no UI element. The top-level actor
 * {@link #actor} is an empty container that subclasses should populate
 * with UI elements. For example, the {@link PopupMenuItem} is just a
 * {@link PopupBaseMenuItem} with a St.Label (text) added to {@link #actor}.
 * Use {@link #addActor} to add UI elements to the item.
 *
 * ![A {@link PopupMenuItem} demonstrating various aspects of {@link PopupBaseMenuItem}; normal, setShowDot, setSensitive, setActive](pics/PopupBaseMenuItem.styles.png)
 *
 * The {@link PopupBaseMenuItem} class handles:
 *
 * * styling elements as they are hovered over (as specified by the ':active"
 * pseudo style class);
 * * function to explicitly set a popup menu item active ({@link #setActive});
 * * function to explicitly set an item as *sensitive* (i.e. enabled
 *   or "greyed out"), {@link #setSensitive};
 * * function to show or hide a circular dot to the left of the item, perhaps
 *   to indicate that it is selected {@link #setShowDot}.
 *
 * When the item is clicked it emits a ['activate']{@link .event:activate}
 * signal. If you wish to do something else when the item is clicked
 * (for example the {@link PopupSwitchMenuItem} has to toggle the switch
 * on and off), override {@link #activate}.
 *
 * @class
 */
function PopupBaseMenuItem(params) {
    this._init(params);
}

PopupBaseMenuItem.prototype = {
    _init: function (params) {
        params = Params.parse (params, { reactive: true,
                                         activate: true,
                                         hover: true,
                                         sensitive: true,
                                         style_class: null
                                       });
        /** The top-level actor. It's just an empty container.
         * Add content to the popup menu item using {@link #addActor}.
         *
         * Style class 'popup-menu-item'.
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer({ style_class: 'popup-menu-item',
                                                  reactive: params.reactive,
                                                  track_hover: params.reactive,
                                                  can_focus: params.reactive });
        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));
        this.actor.connect('style-changed', Lang.bind(this, this._onStyleChanged));
        this.actor._delegate = this;

        /** List of children in {@link #actor} along with their parameters
         * (column span, expand, alignment).
         * @type {Array.<{span: number, expand: boolean, align: St.Align, actor: Clutter.Actor}>} */
        this._children = [];
        /** If {@link #setShowDot} is `true` a dot is drawn to the left
         * of the item. This is that dot (it is only created on demand
         * so is `null` until called for the first time).
         * @see #setShowDot
         * @type {?St.DrawingArea} */
        this._dot = null;
        this._columnWidths = null;
        /** The spacing in between each actor that is {@link #addActor}ed
         * to the item. Read from the 'spacing' style property of
         * {@link #actor}'s CSS style class.
         * @type {number} */
        this._spacing = 0;
        /** Whether the item is 'active'. This means that it is either
         * being hovered over (if `params.hover` was not `false` in the
         * constructor), or has had {@linK #setActive} called.
         * @see #setActive
         * @type {boolean} */
        this.active = false;
        /** Whether the item can even be activated - will be false if
         * either `params.reactive` or `params.activate` were specified
         * as `false` in the constructor.
         * @type {boolean} */
        this._activatable = params.reactive && params.activate;
        /** Whether the item is sensitive (can be interacted with, or is
         * 'greyed out').
         * @see #setSensitive
         * @type {boolean} */
        this.sensitive = this._activatable && params.sensitive;

        this.setSensitive(this.sensitive);

        if (params.style_class)
            this.actor.add_style_class_name(params.style_class);

        if (this._activatable) {
            this.actor.connect('button-release-event', Lang.bind(this, this._onButtonReleaseEvent));
            this.actor.connect('key-press-event', Lang.bind(this, this._onKeyPressEvent));
        }
        if (params.reactive && params.hover)
            this.actor.connect('notify::hover', Lang.bind(this, this._onHoverChanged));
        if (params.reactive) {
            this.actor.connect('key-focus-in', Lang.bind(this, this._onKeyFocusIn));
            this.actor.connect('key-focus-out', Lang.bind(this, this._onKeyFocusOut));
        }
    },

    /** Callback when the item's ({@link #actor}'s) style class changes.
     *
     * We look for the 'spacing' attribute in {@link #actor}'s style class
     * and store it (this is the spacing between elements that are
     * {@link #addActor}'ed into the item).
     * @see #addActor
     * @param {Shell.GenericContainer} actor - actor emitting the signal
     * ({@link #actor})
     */
    _onStyleChanged: function (actor) {
        this._spacing = Math.round(actor.get_theme_node().get_length('spacing'));
    },

    /** Called when the item is clicked on. Calls {@link #activate} to indicate
     * the item has been activated.
     *
     * Note that if the item isn't activatable ({@link #_activatable}, depends
     * on the constructor parameters) this will never be called.
     * @inheritparams #_onStyleChanged
     * @param {Clutter.Event} event - the event information
     * @returns {boolean} whether the event was handled.
     */
    _onButtonReleaseEvent: function (actor, event) {
        this.activate(event);
        return true;
    },

    /** Called whenever the item has a key press on it.
     *
     * If it's space or return, the event is handled and the item is activated
     * ({@link #activate}). Otherwise, we pass the even through.
     *
     * Note that if the item isn't activatable ({@link #_activatable}, depends
     * on the constructor parameters) this will never be called.
     * @inheritparams #_onButtonReleaseEvent
     */
    _onKeyPressEvent: function (actor, event) {
        let symbol = event.get_key_symbol();

        if (symbol == Clutter.KEY_space || symbol == Clutter.KEY_Return) {
            this.activate(event);
            return true;
        }
        return false;
    },

    /** Callback when the item is navigated to by keyboard.
     * Calls {@link #setActive} to give the item a different visual look.
     *
     * Not called if {@link #actor} is not reactive (as specified by the
     * `params.reactive` argument in the constructor).
     * @inheritparams #_onStyleChanged
     */
    _onKeyFocusIn: function (actor) {
        this.setActive(true);
    },

    /** Callback when the item is navigated from by keyboard.
     * Calls {@link #setActive} to remove the 'highlighted' look from the item.
     *
     * Not called if {@link #actor} is not reactive (as specified by the
     * `params.reactive` argument in the constructor).
     * @inheritparams #_onStyleChanged
     */
    _onKeyFocusOut: function (actor) {
        this.setActive(false);
    },

    /** Callback when the item is hovered over.
     * Calls {@link #setActive} to add or remove the 'highlighted' look on the
     * item.
     *
     * Not called if {@link #actor} does not track hovers (as specified by the
     * `params.hover` argument in the constructor).
     * @inheritparams #_onStyleChanged
     */
    _onHoverChanged: function (actor) {
        this.setActive(actor.hover);
    },

    /** This is called whenever the user "activates" this item - clicks on
     * it, or presses Enter or Space while it has keyboard focus.
     *
     * This also emits the 'activate' signal. If you are using a popup
     * menu item, you should connect to its 'activate' signal to perform
     * the action for that item.
     *
     * If you are subclassing the popup base menu item, you may wish override
     * this function to do additional tasks, for example toggling the switch
     * for a {@link PopupSwitchMenuItem}.
     * @param {Clutter.Event} event - the event information
     * @fires .event:activate
     */
    activate: function (event) {
        this.emit('activate', event);
    },

    /** Sets the item to an "active" visual state - e.g. when it is hovered
     * over.
     *
     * ![An active {@link PopupMenuItem} down the bottom](pics/PopupBaseMenuItem.styles.png)
     *
     * This adds (or removes) the 'active' pseudo style class from {@link #actor}.
     * An active item additionally grabs key focus.
     *
     * We emit a signal 'active-changed' with the new state.
     * @param {boolean} active - whether the item is to be active or not.
     * @fires .active-changed
     */
    setActive: function (active) {
        let activeChanged = active != this.active;

        if (activeChanged) {
            this.active = active;
            if (active) {
                this.actor.add_style_pseudo_class('active');
                this.actor.grab_key_focus();
            } else
                this.actor.remove_style_pseudo_class('active');
            this.emit('active-changed', active);
        }
    },

    /** Sets the item to be sensitive or not.
     *
     * ![An insensitive {@link PopupMenuItem} second from the bottom](pics/PopupBaseMenuItem.styles.png)
     *
     * An insensitive popup menu item:
     *
     * * cannot be hovered over or focused;
     * * is not reactive - can't be clicked, etc
     * * has the 'insensitive' pseudo style class applied to it.
     *
     * We update {@link #sensitive} to match and emit 'sensitive-changed' if
     * the value changed.
     * @param {boolean} sensitive - whether the item should be sensitive or not
     * @fires .sensitive-changed
     */
    setSensitive: function(sensitive) {
        if (!this._activatable)
            return;
        if (this.sensitive == sensitive)
            return;

        this.sensitive = sensitive;
        this.actor.reactive = sensitive;
        this.actor.can_focus = sensitive;

        if (sensitive)
            this.actor.remove_style_pseudo_class('insensitive');
        else
            this.actor.add_style_pseudo_class('insensitive');
        this.emit('sensitive-changed', sensitive);
    },

    /** Destroys the item. This destroys {@link #actor} and emits the
     * 'destroy' signal.
     * @fires .event:destroy */
    destroy: function() {
        this.actor.destroy();
        this.emit('destroy');
    },

    /** Adds an actor to the menu item {@link #actor}.
     *
     * @param {Clutter.Actor} child - actor (or St widget)
     * @param {Object} [params] - parameters to add the child with.
     * @param {number} [span=1] - column span of the item. -1 means "all the
     * remaining width".
     * @param {boolean} [expand=false] - whether to expand the child to fill
     * its column.
     * @param {St.Align} [align=St.Align.START] - alignment of the child
     * (START, MIDDLE, END) within the column.
     */
    addActor: function(child, params) {
        params = Params.parse(params, { span: 1,
                                        expand: false,
                                        align: St.Align.START });
        params.actor = child;
        this._children.push(params);
        this.actor.connect('destroy', Lang.bind(this, function () { this._removeChild(child); }));
        this.actor.add_actor(child);
    },

    /** Callback when a child of {@link #actor} is destroyed.
     *
     * This removes the child from our internal list {@link #_children}.
     * @inheritparams #addActor
     */
    _removeChild: function(child) {
        for (let i = 0; i < this._children.length; i++) {
            if (this._children[i].actor == child) {
                this._children.splice(i, 1);
                return;
            }
        }
    },

    /** Removes a child added to the popup menu item with {@link #addActor}.
     * @inheritparams #addActor
     */
    removeActor: function(child) {
        this.actor.remove_actor(child);
        this._removeChild(child);
    },

    /** @inheritdoc LoginDialog.SessionListItem#setShowDot */
    setShowDot: function(show) {
        if (show) {
            if (this._dot)
                return;

            this._dot = new St.DrawingArea({ style_class: 'popup-menu-item-dot' });
            this._dot.connect('repaint', Lang.bind(this, this._onRepaintDot));
            this.actor.add_actor(this._dot);
        } else {
            if (!this._dot)
                return;

            this._dot.destroy();
            this._dot = null;
        }
    },

    /** @inheritdoc LoginDialog.SessionListItem#_repaintDot */
    _onRepaintDot: function(area) {
        let cr = area.get_context();
        let [width, height] = area.get_surface_size();
        let color = area.get_theme_node().get_foreground_color();

        cr.setSourceRGBA (
            color.red / 255,
            color.green / 255,
            color.blue / 255,
            color.alpha / 255);
        cr.arc(width / 2, height / 2, width / 3, 0, 2 * Math.PI);
        cr.fill();
    },

    /** This returns column widths in the item. The order is the order
     * they were added with {@link #addActor}.
     *
     * Note - the comment in the code says it gets column widths including
     * the dot to the left, but the code doesn't do that.
     *
     * @returns {number[]} - the width of each column, being the
     * natural preferred width of the child in the column. If a child spans
     * multiple columns the first of these will have the width of the child
     * and the rest will have width 0.
     */
    getColumnWidths: function() {
        let widths = [];
        for (let i = 0, col = 0; i < this._children.length; i++) {
            let child = this._children[i];
            let [min, natural] = child.actor.get_preferred_width(-1);
            widths[col++] = natural;
            if (child.span > 1) {
                for (let j = 1; j < child.span; j++)
                    widths[col++] = 0;
            }
        }
        return widths;
    },

    /** Sets the widths of each column in the item.
     * @see #_allocate
     * @param {number[]} widths - widths of each column. Be careful, if
     * children span multiple columns weird things could happen!
     */
    setColumnWidths: function(widths) {
        this._columnWidths = widths;
    },

    /** Callback for {@link #actor}'s 'get-preferred-width' signal.
     *
     * Recall that each item {@link #addActor}d to the item gets
     * its own column.
     *
     * If column widths have been specified explicitly with
     * {@link #setColumnWidths}, the sum of these are returned, allowing for
     * {@link #_spacing} pixels of space between each column.
     *
     * Otherwise we return the sum of each child's natural preferred width,
     * with {@link #_spacing} pixels of space between each child.
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        let width = 0;
        if (this._columnWidths) {
            for (let i = 0; i < this._columnWidths.length; i++) {
                if (i > 0)
                    width += this._spacing;
                width += this._columnWidths[i];
            }
        } else {
            for (let i = 0; i < this._children.length; i++) {
                let child = this._children[i];
                if (i > 0)
                    width += this._spacing;
                let [min, natural] = child.actor.get_preferred_width(-1);
                width += natural;
            }
        }
        alloc.min_size = alloc.natural_size = width;
    },

    /** Callback for {@link #actor}'s 'get-preferred-width' signal.
     *
     * For each actor (child) that was {@link #addActor}ed to the item, we:
     *
     * 1. work out how much width it wants (being careful with children
     * with span -1, i.e. taking up the remainder of the space), and
     * 2. work out its preferred natural height for that width.
     *
     * We take the maximum of these heights over all the children and set
     * this as the preferred height for {@link #actor}.
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        let height = 0, x = 0, minWidth, childWidth;
        for (let i = 0; i < this._children.length; i++) {
            let child = this._children[i];
            if (this._columnWidths) {
                if (child.span == -1) {
                    childWidth = 0;
                    for (let j = i; j < this._columnWidths.length; j++)
                        childWidth += this._columnWidths[j]
                } else
                    childWidth = this._columnWidths[i];
            } else {
                if (child.span == -1)
                    childWidth = forWidth - x;
                else
                    [minWidth, childWidth] = child.actor.get_preferred_width(-1);
            }
            x += childWidth;

            let [min, natural] = child.actor.get_preferred_height(childWidth);
            if (natural > height)
                height = natural;
        }
        alloc.min_size = alloc.natural_size = height;
    },

    /** Lays out all the visual elements of the item. (Callback for
     * {@link #actor}'s 'allocate' signal).
     *
     * If the dot to the left is currently showing
     * ([`setShowDot(true)`]{@link #setShowDot} has been called),
     * we allocate space for the dot.
     *
     * The dot is placed in the padding for the item, with the width of the
     * dot being half the padding and it being centred within the padding
     * (so 1/4 of the padding either side).
     *
     * Then we position each child that was {@link #addActor}ed:
     *
     * 1. Calculate the available width for the column the child is in
     * (if the child has `span: -1` the column width is the remainder of the
     * space available, otherwise it's the child's natural width, OR the
     * sum of the widths specified for the columns it occupies if
     * {@link #setColumnWidths} was explicity set).
     * 2. Position the child in its column. If it was added with `expand: true`
     * it is allocated the entire column's width. Otherwise the child is
     * placed within the column according to its `align` parameter (START,
     * MIDDLE, END).
     * 3. Make sure that the spacing between each column is honoured ({@link #_spacing}).
     *
     * If the text direction is right-to-left, we do this in reverse (dot is
     * placed right-most and the children are placed right to left).
     * @todo diagram?
     */
    _allocate: function(actor, box, flags) {
        let height = box.y2 - box.y1;
        let direction = this.actor.get_direction();

        if (this._dot) {
            // The dot is placed outside box
            // one quarter of padding from the border of the container
            // (so 3/4 from the inner border)
            // (padding is box.x1)
            let dotBox = new Clutter.ActorBox();
            let dotWidth = Math.round(box.x1 / 2);

            if (direction == St.TextDirection.LTR) {
                dotBox.x1 = Math.round(box.x1 / 4);
                dotBox.x2 = dotBox.x1 + dotWidth;
            } else {
                dotBox.x2 = box.x2 + 3 * Math.round(box.x1 / 4);
                dotBox.x1 = dotBox.x2 - dotWidth;
            }
            dotBox.y1 = Math.round(box.y1 + (height - dotWidth) / 2);
            dotBox.y2 = dotBox.y1 + dotWidth;
            this._dot.allocate(dotBox, flags);
        }

        let x;
        if (direction == St.TextDirection.LTR)
            x = box.x1;
        else
            x = box.x2;
        // if direction is ltr, x is the right edge of the last added
        // actor, and it's constantly increasing, whereas if rtl, x is
        // the left edge and it decreases
        for (let i = 0, col = 0; i < this._children.length; i++) {
            let child = this._children[i];
            let childBox = new Clutter.ActorBox();

            let [minWidth, naturalWidth] = child.actor.get_preferred_width(-1);
            let availWidth, extraWidth;
            if (this._columnWidths) {
                if (child.span == -1) {
                    if (direction == St.TextDirection.LTR)
                        availWidth = box.x2 - x;
                    else
                        availWidth = x - box.x1;
                } else {
                    availWidth = 0;
                    for (let j = 0; j < child.span; j++)
                        availWidth += this._columnWidths[col++];
                }
                extraWidth = availWidth - naturalWidth;
            } else {
                if (child.span == -1) {
                    if (direction == St.TextDirection.LTR)
                        availWidth = box.x2 - x;
                    else
                        availWidth = x - box.x1;
                } else {
                    availWidth = naturalWidth;
                }
                extraWidth = 0;
            }

            if (direction == St.TextDirection.LTR) {
                if (child.expand) {
                    childBox.x1 = x;
                    childBox.x2 = x + availWidth;
                } else if (child.align === St.Align.MIDDLE) {
                    childBox.x1 = x + Math.round(extraWidth / 2);
                    childBox.x2 = childBox.x1 + naturalWidth;
                } else if (child.align === St.Align.END) {
                    childBox.x2 = x + availWidth;
                    childBox.x1 = childBox.x2 - naturalWidth;
                } else {
                    childBox.x1 = x;
                    childBox.x2 = x + naturalWidth;
                }
            } else {
                if (child.expand) {
                    childBox.x1 = x - availWidth;
                    childBox.x2 = x;
                } else if (child.align === St.Align.MIDDLE) {
                    childBox.x1 = x - Math.round(extraWidth / 2);
                    childBox.x2 = childBox.x1 + naturalWidth;
                } else if (child.align === St.Align.END) {
                    // align to the left
                    childBox.x1 = x - availWidth;
                    childBox.x2 = childBox.x1 + naturalWidth;
                } else {
                    // align to the right
                    childBox.x2 = x;
                    childBox.x1 = x - naturalWidth;
                }
            }

            let [minHeight, naturalHeight] = child.actor.get_preferred_height(childBox.x2 - childBox.x1);
            childBox.y1 = Math.round(box.y1 + (height - naturalHeight) / 2);
            childBox.y2 = childBox.y1 + naturalHeight;

            child.actor.allocate(childBox, flags);

            if (direction == St.TextDirection.LTR)
                x += availWidth + this._spacing;
            else
                x -= availWidth + this._spacing;
        }
    }
};
Signals.addSignalMethods(PopupBaseMenuItem.prototype);
/** Emitted whenever the item is activated - clicked, or enter/space is pressed
 * while it has keyboard focus.
 * @name activate
 * @event
 * @param {PopupBaseMenuItem} item - the item that emitted the signal
 * @param {Clutter.Event} event - the event information
 * @memberof PopupBaseMenuItem
 */
/** Emitted when an item's 'active' state changes.
 * @see #setActive
 * @name active-changed
 * @event
 * @param {PopupBaseMenuItem} item - the item that emitted the signal
 * @param {boolean} active - whether the item is active or not
 * @memberof PopupBaseMenuItem
 */
/** Emitted when an item's 'sensitive' state changes.
 * @see #setSensitive
 * @name sensitive-changed
 * @event
 * @param {PopupBaseMenuItem} item - the item that emitted the signal
 * @param {boolean} sensitive - whether the item is sensitive or not
 * @memberof PopupBaseMenuItem
 */
/** Emitted when an item is destroyed.
 * @see #destroy
 * @name destroy
 * @event
 * @memberof PopupBaseMenuItem
 */

/** creates a new PopupMenuItem
 *
 * ![A {@link PopupMenuItem}.](pics/PopupMenuItem.png)
 *
 * This is just a {@link PopupBaseMenuItem} with a label for showing the text.
 *
 * We call the [parent constructor]{@link PopupBaseMenuItem} with `params`.
 *
 * Then we create a {@link St.Label} to display `text` ({@link #label})
 * and call {@link #addActor} to add it.
 *
 * @param {string} text - text to display on the popup menu item.
 * @inheritparams PopupBaseMenuItem
 * @classdesc
 * ![A {@link PopupMenuItem}.](pics/PopupMenuItem.png)
 *
 * A PopupMenuItem is just an item in a popup menu that displays text.
 *
 * That is, it's a {@link PopupBaseMenuItem} with a label.
 *
 * Connect to its ['activate']{@link .event:activate} signal to know when it
 * gets clicked and do something.
 * @example <caption>Creating a new PopupMenuItem that notifies 'Hello' on clicking.</caption>
 * const PopupMenu = imports.ui.popupMenu;
 * // create the item with text "Say hello".
 * let item = new PopupMenu.PopupMenuItem("Say hello");
 * // connect up a callback to notify "Hello" to the user.
 * item.connect('activate', function () {
 *     Main.notify("Hello!");
 * });
 * @class
 * @extends PopupBaseMenuItem
 */
function PopupMenuItem() {
    this._init.apply(this, arguments);
}

PopupMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function (text, params) {
        PopupBaseMenuItem.prototype._init.call(this, params);

        /** The label for the popup menu item (shows the text).
         * @type {St.Label} */
        this.label = new St.Label({ text: text });
        this.addActor(this.label);
    }
};

/** creates a new PopupSeparatorMenuItem
 *
 * ![A {@link PopupSeparatorMenuItem}](pics/PopupSeparatorMenuItem.png)
 *
 * ![A {@link PopupSeparatorMenuItem} in between some other items](pics/PopupSeparatorMenuItem.surrounded.png)
 *
 * This is a non-reactive {@link PopupBaseMenuItem} with a {@link St.DrawingArea}
 * in it on which we draw the horizontal seperator.
 *
 * We call the [parent constructor]{@link PopupBaseMenuItem}, making the item
 * non-reactive.
 *
 * Then we create a canvas {@link #_drawingArea} on which to draw the separator
 * and {@link #addActor} it to ourselves, with parameters `span: -1` (its column
 * takes up as much space as available) and `expand: true` (the drawing area
 * takes up the entire column width).
 *
 * We connect its 'repaint' signal to {@link #_onRepaint}.
 *
 * @classdesc
 * This is a {@link PopupBaseMenuItem} that is a horizontal line (separator).
 *
 * ![A {@link PopupSeparatorMenuItem}](pics/PopupSeparatorMenuItem.png)
 *
 * ![A {@link PopupSeparatorMenuItem} in between some other items](pics/PopupSeparatorMenuItem.surrounded.png)
 *
 * It is drawn with a {@link St.DrawingArea} and cairo directives.
 *
 * @class
 * @extends PopupBaseMenuItem
 * @example <caption>Creating a PopupSeparatorMenuItem</caption>
 * const PopupMenu = imports.ui.popupMenu;
 * let item = new PopupMenu.PopupSeparatorMenuItem();
 */
function PopupSeparatorMenuItem() {
    this._init();
}

PopupSeparatorMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function () {
        PopupBaseMenuItem.prototype._init.call(this, { reactive: false });

        /** What we draw the separator on.
         * @see #_onRepaint
         * @type {St.DrawingArea} */
        this._drawingArea = new St.DrawingArea({ style_class: 'popup-separator-menu-item' });
        this.addActor(this._drawingArea, { span: -1, expand: true });
        this._drawingArea.connect('repaint', Lang.bind(this, this._onRepaint));
    },

    /** Called on {@link #_drawingArea}'s 'repaint' signal to draw the horizontal
     * separator.
     *
     * ![A {@link PopupSeparatorMenuItem}](pics/PopupSeparatorMenuItem.png)
     *
     * The separator is a horizontal line specified by the following parameters
     * of its CSS style class ('popup-separator-menu-item'):
     *
     * * '-margin-horizontal': the gap between the edge of the item and the
     *   end of the line on each side.
     * * '-gradient-height': the thickness of the line (2px by default);
     * * '-gradient-start': the colour of the line at the edges, which blends to:
     * * '-gradient-end': the colour of the line in the centre.
     *
     * A line is drawn with thickness '-gradient-height' and width
     * `area.width - 2 * '-margin-horizontal'`. The colour of the line
     * starts off as '-gradient-start' and blends to colour '-gradient-end'
     * at the centre of the line, before blending back to '-gradient-start'
     * at the end of the line.
     *
     * @param {St.DrawingArea} area - {@link #_drawingArea}.
     */
    _onRepaint: function(area) {
        let cr = area.get_context();
        let themeNode = area.get_theme_node();
        let [width, height] = area.get_surface_size();
        let margin = themeNode.get_length('-margin-horizontal');
        let gradientHeight = themeNode.get_length('-gradient-height');
        let startColor = themeNode.get_color('-gradient-start');
        let endColor = themeNode.get_color('-gradient-end');

        let gradientWidth = (width - margin * 2);
        let gradientOffset = (height - gradientHeight) / 2;
        let pattern = new Cairo.LinearGradient(margin, gradientOffset, width - margin, gradientOffset + gradientHeight);
        pattern.addColorStopRGBA(0, startColor.red / 255, startColor.green / 255, startColor.blue / 255, startColor.alpha / 255);
        pattern.addColorStopRGBA(0.5, endColor.red / 255, endColor.green / 255, endColor.blue / 255, endColor.alpha / 255);
        pattern.addColorStopRGBA(1, startColor.red / 255, startColor.green / 255, startColor.blue / 255, startColor.alpha / 255);
        cr.setSource(pattern);
        cr.rectangle(margin, gradientOffset, gradientWidth, gradientHeight);
        cr.fill();
    }
};

/** The current state of a {@link PopupAlternatingMenuItem} - whether it
 * is showing its default text or alternate text.
 * @enum
 * @const
 */
const PopupAlternatingMenuItemState = {
    /** Showing the default text. */
    DEFAULT: 0,
    /** Showing the alternate text. */
    ALTERNATIVE: 1
}

/** creates a new PopupAlternatingMenuItem
 *
 * This displays `text` normally, but when ALT is pressed down `alternateText`
 * is shown.
 *
 * We create a {@link St.Label} {@link #label} to display the text and
 * {@link #addActor} it to ourselves.
 *
 * We create a state variable {@link #state} that tells us whether we should
 * be displaying `text` or `alternateText`.
 *
 * This class listens to any event captured on the global stage, checking if it's
 * a key press and if it was ALT and then showing the alternate text.
 *
 * To save a bit of work, we *only* listen for these events when the item
 * is actually visible.
 *
 * So, we connect to its 'notify::mapped' signal in the constructor. The
 * callback {@link #_onMapped} handles listening for an ALT keypress/release.
 *
 * @inheritparams {@link PopupAlternatingMenuItem}
 * @param {string} text - the default text to show on the item.
 * @param {string} alternateText - the text to show when ALT is held down.
 * @classdesc
 * A PopupAlternatingMenuItem is a {@link PopupMenuItem} with two "modes":
 * the default mode will show one message and the alternate mode will show
 * another message.
 *
 * One triggers the alternate mode by holding down the ALT key.
 *
 * An example of this is the "Suspend"/"Power off" item in the user menu -
 * it shows "Suspend" by default and holding down ALT will make it show
 * "Power off".
 *
 * To update the default and alternative text, use {@link #updateText}.
 * @class
 * @extends PopupBaseMenuItem
 * @example <caption>Creating a PopupAlternatingMenuItem displaying "Hello" normally and "Goodbye" when ALT is held down.</caption>
 * const PopupMenu = imports.ui.popupMenu;
 * // create the item with text "Hello" and alternate text "Goodbye"
 * let item = new PopupMenu.PopupAlternatingMenuItem("Hello", "Goodbye");
 *
 * // connect up a callback to notify "Hello" to the user if "Hello" is being
 * // shown, and "Goodbye" if the alternate text is shown.
 * item.connect('activate', function () {
 *     if (item.state === PopupMenu.PopupAlternatingMenuItemState.DEFAULT) {
 *         // default text ("Hello") was showing when the item was clicked
 *         Main.notify("Hello!");
 *     } else {
 *         // alternate text ("Goodbye") was showing when the item was clicked
 *         Main.notify("Goodbye!");
 *     }
 * });
 */
function PopupAlternatingMenuItem() {
    this._init.apply(this, arguments);
}

PopupAlternatingMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function(text, alternateText, params) {
        PopupBaseMenuItem.prototype._init.call(this, params);
        this.actor.add_style_class_name('popup-alternating-menu-item');

        /** The default text shown on the item.
         * @type {string} */
        this._text = text;
        /** The alternate text shown on the item when ALT is pressed.
         * @type {string} */
        this._alternateText = alternateText;
        /** The label that shows the item's text.
         * @type {St.Label} */
        this.label = new St.Label({ text: text });
        /** The current state of the item (i.e. whether we're showing the
         * normal text or the alternate text).
         * @type {PopupAlternatingMenuItemState} */
        this.state = PopupAlternatingMenuItemState.DEFAULT;
        this.addActor(this.label);

        this.actor.connect('notify::mapped', Lang.bind(this, this._onMapped));
    },

    /** Called whenever the item is mapped or unmapped on the screen, i.e.
     * whenever it becomes visible or not.
     *
     * We only listen to the ALT keypress events while we are actually
     * visible.
     *
     * If we are visible (mapped), we listen to the global stage's
     * 'captured-evenet' signal (to listen for ALT key presses/releases).
     *
     * Otherwise, we disconnect this signal. */
    _onMapped: function() {
        if (this.actor.mapped) {
            this._capturedEventId = global.stage.connect('captured-event',
                                                         Lang.bind(this, this._onCapturedEvent));
            this._updateStateFromModifiers();
        } else {
            if (this._capturedEventId != 0) {
                global.stage.disconnect(this._capturedEventId);
                this._capturedEventId = 0;
            }
        }
    },

    /** Internal function - sets our state (whether we are in the DEFAULT
     * or ALTERNATIVE state) and updates the text accordingly.
     * @param {PopupAlternatingMenuItemState} state - state of the item. DEFAULT
     * means we are showing the default text, ALTERNATIVE means we are showing
     * @see #_updateLabel
     * the alternative text (when ALT is held down).
     */
    _setState: function(state) {
        if (this.state != state) {
            if (state == PopupAlternatingMenuItemState.ALTERNATIVE && !this._canAlternate())
                return;

            this.state = state;
            this._updateLabel();
        }
    },

    /** Updates our state (DEFAULT or ALTERNATIVE) depending on whether
     * the ALT key is currently being held down.
     *
     * We query the pointer's position `global.get_pointer()` and look at
     * its modifiers for ALT. If ALT is pressed we are in the alternate state.
     * @see #_setState */
    _updateStateFromModifiers: function() {
        let [x, y, mods] = global.get_pointer();
        let state;

        if ((mods & Clutter.ModifierType.MOD1_MASK) == 0) {
            state = PopupAlternatingMenuItemState.DEFAULT;
        } else {
            state = PopupAlternatingMenuItemState.ALTERNATIVE;
        }

        this._setState(state);
    },

    /** Callback when any event is captured from the global stage whilst
     * the item is mapped.
     *
     * If the ALT key is being held down we set our state to ALTERNATIVE
     * (showing the alternate text), and otherwise we set our state
     * to DEFAULT.
     * @see #_updateStateFromModifiers
     */
    _onCapturedEvent: function(actor, event) {
        if (event.type() != Clutter.EventType.KEY_PRESS &&
            event.type() != Clutter.EventType.KEY_RELEASE)
            return false;

        let key = event.get_key_symbol();

        if (key == Clutter.KEY_Alt_L || key == Clutter.KEY_Alt_R)
            this._updateStateFromModifiers();

        return false;
    },

    /** Updates the text shown on our label according to our state.
     *
     * If we are in ALTERNATIVE state we show the alternate text
     * ({@link #_alternateText}) and add 'alternate' pseudo style class
     * to the item. Otherwise we show {@link #_text} and remove the
     * 'alternate' pseudo style class.
     */
    _updateLabel: function() {
        if (this.state == PopupAlternatingMenuItemState.ALTERNATIVE) {
            this.actor.add_style_pseudo_class('alternate');
            this.label.set_text(this._alternateText);
        } else {
            this.actor.remove_style_pseudo_class('alternate');
            this.label.set_text(this._text);
        }
    },

    /** Whether we can alternate at the moment.
     *
     * We can alternate if an alternate text was specified in the constructor
     * or with #updateText. */
    _canAlternate: function() {
        if (this.state == PopupAlternatingMenuItemState.DEFAULT && !this._alternateText)
            return false;
        return true;
    },

    /** Updates the default and alternative text for the item.
     * @inheritparams PopupAlternatingMenuItem
     */
    updateText: function(text, alternateText) {
        this._text = text;
        this._alternateText = alternateText;

        if (!this._canAlternate())
            this._setState(PopupAlternatingMenuItemState.DEFAULT);

        this._updateLabel();
    }
};

/** creates a new PopupSliderMenuItem
 *
 * ![{@link PopupSliderMenuItem}](pics/PopupSliderMenuItem.png)
 *
 * We call the [parent constructor]{@link PopupBaseMenuItem} with
 * parameter `activate:false`. This means that the item **cannot* be
 * 'activated' by clicking on it or pressing enter while it has keyboard
 * focus - these don't make sense for a slider anyway. It also means
 * that this class will **never emit the ['activate']{@link .event:activate}
 * signal**.
 *
 * We create the drawing area on which we will draw the slider, {@link #_slider},
 * and {@link #addActor} it to ourselves taking up all available space. The
 * slider's 'repaint' signal is connected to {@link #_sliderRepaint}.
 *
 * We also connect to various signals on {@link #actor} to detect key presses
 * (pressing right and left arrow keys will move the slider), button presses
 * (to start a drag) and scroll events (which move the slider).
 *
 * @param {number} value - the initial value of the slider (between 0 and 1).
 * @classdesc
 * This is a {@link PopupBaseMenuItem} that displays a horizontal slider
 * widget.
 *
 * ![{@link PopupSliderMenuItem}](pics/PopupSliderMenuItem.png)
 *
 * The range of the widget is always from 0 to 1 (if you need other values
 * you should convert appropriately).
 *
 * The slider is drawn with cairo graphics ({@link #_sliderRepaint}) and the
 * slider may move via mouse dragging, scrolling over the slider, and
 * using the left and right arrow keys if the item has keyboard focus.
 *
 * The slider has event ['value-changed']{@link .event:value-changed} that
 * fires whenever the slider value changes (including while it is being
 * dragged), and a ['drag-end']{@link .event:drag-end} signal that fires
 * when the user has *finished* dragging.
 *
 * It might be useful to pair the slider menu item with say a text menu item
 * that displays the current value of the slider. Typically this label
 * would be updated on ['value-changed']{@link .event:value-changed} whereas
 * the response to the drag might only happen on the ['drag-end']{@link .event:drag-end}
 * event.
 *
 * The {@link PopupSliderMenuItem} class **never** emits the
 * ['activate']{@link .event:activate} signal.
 *
 * @TODO link to the slider-with-labels subclass (tutorial?).
 * @class
 * @example <caption>Creating a PopupSliderMenuItem</caption>
 * // TODO: provide screenshots of the examples.
 * const PopupMenu = imports.ui.popupMenu;
 * // create a slider with initial value 0.5 (between 0 and 1)
 * let item = new PopupMenu.PopupSliderMenuItem(0.5);
 * // listen when the value is changed (the user is sliding it):
 * item.connect('value-changed', function (item, value) {
 *     // the current value of the slider (between 0 and 1) is `value`.
 *     log(value);
 * });
 * // listen when the user has *finished* dragging the slider:
 * item.connect('drag-end', function () {
 *     // the current value of the slider:
 *     log(item.value);
 * });
 * @extends PopupBaseMenuItem
 */
function PopupSliderMenuItem() {
    this._init.apply(this, arguments);
}

PopupSliderMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function(value) {
        PopupBaseMenuItem.prototype._init.call(this, { activate: false });

        this.actor.connect('key-press-event', Lang.bind(this, this._onKeyPressEvent));

        if (isNaN(value))
            // Avoid spreading NaNs around
            throw TypeError('The slider value must be a number');
        /** The current value of the slider, between 0 and 1.
         * Use {@link #setValue} and {@link #value}, not {@link #_value}.
         * @see #setValue
         * @see #value
         * @type {number} */
        this._value = Math.max(Math.min(value, 1), 0);

        /** The canvas on which we will draw the slider widget.
         *
         * Style class 'popup-slider-menu-item'.
         * @type {St.DrawingArea} */
        this._slider = new St.DrawingArea({ style_class: 'popup-slider-menu-item', reactive: true });
        this.addActor(this._slider, { span: -1, expand: true });
        this._slider.connect('repaint', Lang.bind(this, this._sliderRepaint));
        this.actor.connect('button-press-event', Lang.bind(this, this._startDragging));
        this.actor.connect('scroll-event', Lang.bind(this, this._onScrollEvent));

        this._releaseId = this._motionId = 0;
        /** Whether the slider is currently being dragged.
         * @type {boolean} */
        this._dragging = false;
    },

    /** Sets the value of the slider, updating the picture to match.
     * @param {number} value - new value, between 0 and 1.
     * @see #_sliderRepaint
     */
    setValue: function(value) {
        if (isNaN(value))
            throw TypeError('The slider value must be a number');

        this._value = Math.max(Math.min(value, 1), 0);
        this._slider.queue_repaint();
    },

    /** Paints the slider.
     *
     * ![{@link PopupSliderMenuItem}](pics/PopupSliderMenuItem.png)
     *
     * Various properties of {@link #_slider}'s style class ('popup-slider-menu-item')
     * are used to do the drawing.
     *
     * The circular "handle" that gets dragged is a circle with radius
     * '-slider-handle-radius', drawn with the same colour as other popup
     * menu items.
     *
     * The slider itself is split into two parts, the "active" part and
     * "inactive" part. The "active" part is blue, from the left end of the
     * handle. The "inactive" part is the remainder.
     *
     * The slider is a horizontal line with height '-slider-height', and width
     * as wide as the space available (such that the handle would still fit if
     * it were on end side of the slider).
     *
     * The active part has background colour '-slider-active-background-color'
     * (the blue in the picture above) and border colour
     * '-slider-active-border-color'.
     *
     * The inactive part has background colour '-slider-background-color'
     * (dark grey in the picture above) and border colour '-slider-border-color'
     *
     * The width of the border for bot hparts is '-slider-border-width'.
     *
     * @param {St.DrawingArea} area - {@link #_slider}.
     */
    _sliderRepaint: function(area) {
        let cr = area.get_context();
        let themeNode = area.get_theme_node();
        let [width, height] = area.get_surface_size();

        let handleRadius = themeNode.get_length('-slider-handle-radius');

        let sliderWidth = width - 2 * handleRadius;
        let sliderHeight = themeNode.get_length('-slider-height');

        let sliderBorderWidth = themeNode.get_length('-slider-border-width');

        let sliderBorderColor = themeNode.get_color('-slider-border-color');
        let sliderColor = themeNode.get_color('-slider-background-color');

        let sliderActiveBorderColor = themeNode.get_color('-slider-active-border-color');
        let sliderActiveColor = themeNode.get_color('-slider-active-background-color');

        cr.setSourceRGBA (
            sliderActiveColor.red / 255,
            sliderActiveColor.green / 255,
            sliderActiveColor.blue / 255,
            sliderActiveColor.alpha / 255);
        cr.rectangle(handleRadius, (height - sliderHeight) / 2, sliderWidth * this._value, sliderHeight);
        cr.fillPreserve();
        cr.setSourceRGBA (
            sliderActiveBorderColor.red / 255,
            sliderActiveBorderColor.green / 255,
            sliderActiveBorderColor.blue / 255,
            sliderActiveBorderColor.alpha / 255);
        cr.setLineWidth(sliderBorderWidth);
        cr.stroke();

        cr.setSourceRGBA (
            sliderColor.red / 255,
            sliderColor.green / 255,
            sliderColor.blue / 255,
            sliderColor.alpha / 255);
        cr.rectangle(handleRadius + sliderWidth * this._value, (height - sliderHeight) / 2, sliderWidth * (1 - this._value), sliderHeight);
        cr.fillPreserve();
        cr.setSourceRGBA (
            sliderBorderColor.red / 255,
            sliderBorderColor.green / 255,
            sliderBorderColor.blue / 255,
            sliderBorderColor.alpha / 255);
        cr.setLineWidth(sliderBorderWidth);
        cr.stroke();

        let handleY = height / 2;
        let handleX = handleRadius + (width - 2 * handleRadius) * this._value;

        let color = themeNode.get_foreground_color();
        cr.setSourceRGBA (
            color.red / 255,
            color.green / 255,
            color.blue / 255,
            color.alpha / 255);
        cr.arc(handleX, handleY, handleRadius, 0, 2 * Math.PI);
        cr.fill();
    },

    /** Callback for a button-press event on the slider.
     *
     * We use this to move the handle of the slider to where the pointer is.
     *
     * We additionally listen to the button release event ({@link #_endDragging})
     * to end the drag, and to the cursor moving ({@link #_motionEvent}) to
     * adjust the slider as the user drags.
     * @see #_moveHandle
     * @see #_endDragging
     */
    _startDragging: function(actor, event) {
        if (this._dragging) // don't allow two drags at the same time
            return;

        this._dragging = true;

        // FIXME: we should only grab the specific device that originated
        // the event, but for some weird reason events are still delivered
        // outside the slider if using clutter_grab_pointer_for_device
        Clutter.grab_pointer(this._slider);
        this._releaseId = this._slider.connect('button-release-event', Lang.bind(this, this._endDragging));
        this._motionId = this._slider.connect('motion-event', Lang.bind(this, this._motionEvent));
        let absX, absY;
        [absX, absY] = event.get_coords();
        this._moveHandle(absX, absY);
    },

    /** Callback when the 'button-release' event is received after 'button-press'
     * was received (i.e. the user releases the mouse after they started
     * dragging in {@link #_startDragging}).
     *
     * We ungrab the pointer from the slider and emit 'drag-end'.
     *
     * @fires .event:drag-end
     * @see #_startDragging
     */
    _endDragging: function() {
        if (this._dragging) {
            this._slider.disconnect(this._releaseId);
            this._slider.disconnect(this._motionId);

            Clutter.ungrab_pointer();
            this._dragging = false;

            this.emit('drag-end');
        }
        return true;
    },

    /** Callback when the user scrolls the mouse over the slider.
     *
     * The slider's value is updated up or down in accordance with the
     * scroll. The scroll step is {@link SLIDER_SCROLL_STEP}.
     *
     * As this changes the value of the slider, 'value-changed' is emitted.
     * @fires .event:value-changed
     */
    _onScrollEvent: function (actor, event) {
        let direction = event.get_scroll_direction();

        if (direction == Clutter.ScrollDirection.DOWN) {
            this._value = Math.max(0, this._value - SLIDER_SCROLL_STEP);
        }
        else if (direction == Clutter.ScrollDirection.UP) {
            this._value = Math.min(1, this._value + SLIDER_SCROLL_STEP);
        }

        this._slider.queue_repaint();
        this.emit('value-changed', this._value);
    },

    /** Callback when the user moves the mouse while dragging the handle of
     * the slider. Updates the slider's value to reflect the position of the
     * curors
     * @see #_moveHandle
     */
    _motionEvent: function(actor, event) {
        let absX, absY;
        [absX, absY] = event.get_coords();
        this._moveHandle(absX, absY);
        return true;
    },

    /** Recalculates the value of the slider from the mouse position,
     * and calls `this._slider.queue_repaint()` to repaint the slider to match.
     * @param {number} absX - the X coordinate to move the handle to,
     * **relative to the global stage**.
     * @param {number} absY - UNUSED.
     * @fires .value-changed
     */
    _moveHandle: function(absX, absY) {
        let relX, relY, sliderX, sliderY;
        [sliderX, sliderY] = this._slider.get_transformed_position();
        relX = absX - sliderX;
        relY = absY - sliderY;

        let width = this._slider.width;
        let handleRadius = this._slider.get_theme_node().get_length('-slider-handle-radius');

        let newvalue;
        if (relX < handleRadius)
            newvalue = 0;
        else if (relX > width - handleRadius)
            newvalue = 1;
        else
            newvalue = (relX - handleRadius) / (width - 2 * handleRadius);
        this._value = newvalue;
        this._slider.queue_repaint();
        this.emit('value-changed', this._value);
    },

    /** Gets the current value of the slider (0 to 1).
     * @todo document getters/setters as functions or as properties?
     * @readonly
     * @number */
    get value() {
        return this._value;
    },

    /** Callback when the slider receives a key press.
     *
     * If the key pressed was the left or right arrow, the value of
     * the slider is adjusted (0.1 increment in either direction).
     *
     * This fires a 'value-changed' and 'drag-end' event.
     *
     * @fires .value-changed
     * @fires .drag-end
     */
    _onKeyPressEvent: function (actor, event) {
        let key = event.get_key_symbol();
        if (key == Clutter.KEY_Right || key == Clutter.KEY_Left) {
            let delta = key == Clutter.KEY_Right ? 0.1 : -0.1;
            this._value = Math.max(0, Math.min(this._value + delta, 1));
            this._slider.queue_repaint();
            this.emit('value-changed', this._value);
            this.emit('drag-end');
            return true;
        }
        return false;
    }
};
/** Emitted when the user stops dragging the slider. Typically application code
 * will listen to this signal to perform some action in response (use
 * `item.value` to retrieve the final value of the slider).
 *
 * Note - this will **not** fire if the value of the slider is changed by
 * scrolling the mouse. Only if it's changed through dragging or the arrow keys.
 * @see {@link .event:value-changed} which is fired every time the slider's
 * value changes, including during a drag.
 * @name drag-end
 * @param {PopupSliderMenuItem} item - the item that emitted the signal.
 * @event
 * @memberof PopupSliderMenuItem */
/** Emitted whenever the slider's value changes, be it by keyboard, dragging,
 * or scrolling.
 * @see {@link .event:drag-end} which is fired upon the end of a drag (by
 * dragging or keyboard but not scrolling).
 * @name value-changed
 * @param {PopupSliderMenuItem} item - the item that emitted the signal.
 * @param {number} value - the new value (between 0 and 1)
 * @event
 * @memberof PopupSliderMenuItem */

/** creates a new Switch
 *
 * ![A {@link Switch}, on (above); off (below)](pics/Switch.png)
 *
 * The top-level actor {@link #actor} is a St.Bin with style classes 'toggle-switch'
 * and ('toggle-switch-us' showing ON/OFF or 'toggle-switch-intl' showing |/◯).
 *
 * These style classes load the switch widget which is just an image for each
 * state.
 *
 * @param {boolean} state - state of the switch (on or off).
 * @classdesc
 * ![A {@link Switch}, on (above); off (below)](pics/Switch.png)
 *
 * A Switch is an on/off toggle. Used in the {@link PopupSwitchMenuItem}.
 *
 * The text shown on the switch is either "ON"/"OFF" (English version) or
 * "◯"/"|" (international version).
 *
 * Toggle the switch with {@link #toggle} or explicitly set the state with
 * {@link #setToggleState}.
 *
 * It emits no signals when it is toggled; the {@link PopupSwitchMenuItem}
 * does.
 * @class
 */
function Switch() {
    this._init.apply(this, arguments);
}

Switch.prototype = {
    _init: function(state) {
        /** Top-level actor for the switch. The style class loads the image
         * that is the visual part. Style classes 'toggle-switch' and
         * ('toggle-switch-us' (switch shows ON/OFF) OR 'toggle-switch-intl'
         * (switch shows |/◯)).
         *
         * @type {St.Bin} */
        this.actor = new St.Bin({ style_class: 'toggle-switch' });
        // Translators: this MUST be either "toggle-switch-us"
        // (for toggle switches containing the English words
        // "ON" and "OFF") or "toggle-switch-intl" (for toggle
        // switches containing "◯" and "|"). Other values will
        // simply result in invisible toggle switches.
        this.actor.add_style_class_name(_("toggle-switch-us"));
        this.setToggleState(state);
    },

    /** Sets the state of the switch, on or off.
     *
     * This adds or removes the 'checked' pseudo style class to {@link #actor}.
     * @inheritparams Switch
     */
    setToggleState: function(state) {
        if (state)
            this.actor.add_style_pseudo_class('checked');
        else
            this.actor.remove_style_pseudo_class('checked');
        this.state = state;
    },

    /** Toggles the switch to the opposite state of the current.
     * @see #setToggleState */
    toggle: function() {
        this.setToggleState(!this.state);
    }
};

/** creates a new PopupSwitchMenuItem
 *
 * We create:
 *
 * * a label to display the text, {@link #label};
 * * a {@link Switch} widget, {@link #_switch};
 * * a label to display the status text, {@link #_statusLabel};
 * * a St.Bin that will show *either* the switch *or* the label, {@link #_statusBin}.
 *
 * We {@link #addActor} the label and the status bin to the item and set
 * the switch to show by default (rather than the status label).
 *
 * @param {string} text - text to display
 * @param {boolean} active - whether the switch is on or off initially.
 * @inheritparams PopupBaseMenuItem
 * @classdesc
 * ![A {@link PopupSwitchMenuItem}](pics/PopupSwitchMenuItem.png)
 *
 * This is a {@link PopupBaseMenuItem} that displays text and an on/off
 * toggle (a {@link Switch}).
 *
 * Alternatively the item can display a "status text" instead of the switch.
 * Calling {@link #setStatus} with non-null text will do this, while calling
 * it with a `null` argument will revert to showing the switch.
 *
 * In "status text" mode the item cannot be toggled or interacted with.
 * @TODO screenshot displaying a status
 *
 * @class
 *
 * @example <caption>Creating a PopupSwitchMenuItem</caption>
 * const PopupMenu = imports.ui.popupMenu;
 * // create the item, initialised to OFF.
 * let item = new PopupMenu.PopupMenuItem("I'm awake", false);
 * // know when the user toggled the switch.
 * item.connect('toggled', function (item, state) {
 *     if (state) { // switch was toggled on.
 *         Main.notify("Good morning sleepy-head!");
 *     } else {
 *         Main.notify("Good night, zzzZZZZ");
 *     }
 * });
 *
 * @extends PopupBaseMenuItem
 */
function PopupSwitchMenuItem() {
    this._init.apply(this, arguments);
}

PopupSwitchMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function(text, active, params) {
        PopupBaseMenuItem.prototype._init.call(this, params);

        /** Label displaying the text for the item.
         * @type {St.Label} */
        this.label = new St.Label({ text: text });
        /** The on/off switch widget for the item.
         * @type {Switch} */
        this._switch = new Switch(active);

        this.addActor(this.label);

        /** Holds either the on/off switch or the status text. Right-aligned.
         * @type {St.Bin} */
        this._statusBin = new St.Bin({ x_align: St.Align.END });
        this.addActor(this._statusBin,
                      { expand: true, span: -1, align: St.Align.END });

        /** The label showing the status of the item (if we are displaying
         * the status instead of the on/off switch).
         * @see #setStatus
         * @type {St.Label} */
        this._statusLabel = new St.Label({ text: '',
                                           style_class: 'popup-inactive-menu-item'
                                         });
        this._statusBin.child = this._switch.actor;
    },

    /** Sets the item to show some text **instead** of the switch widget.
     *
     * If `text` is not `null`, the switch widget is replaced by a label showing
     * the text ({@link #_statusLabel}), and the item is made non-reactive.
     *
     * If `text` is `null` we revert to showing the switch widget {@link #_switch},
     * and the item is made reactive again.
     *
     * Note - if the item is in "status text" mode (not showing the switch),
     * it cannot be toggled or interacted with by the user.
     * @TODO screenshot displaying a status
     * 
     * @param {?string} text - the status text, or `null` to show the switch
     * instead of the text.
     */
    setStatus: function(text) {
        if (text != null) {
            this._statusLabel.text = text;
            this._statusBin.child = this._statusLabel;
            this.actor.reactive = false;
            this.actor.can_focus = false;
        } else {
            this._statusBin.child = this._switch.actor;
            this.actor.reactive = true;
            this.actor.can_focus = true;
        }
    },

    /** Called when the item is activated (clicked on/the user presses Enter
     * or space on it).
     *
     * This calls the [parent method]{@link PopupBaseMenuItem#activate},
     * emitting the 'activate' signal, and also toggles the switch (if
     * it is showing). {@link #toggle} is used to toggle the switch, which
     * will emit a ['toggled']{@link .event:toggled} signal.
     * @see #toggle
     * @override
     */
    activate: function(event) {
        if (this._switch.actor.mapped) {
            this.toggle();
        }

        PopupBaseMenuItem.prototype.activate.call(this, event);
    },

    /** Toggles the switch.
     * Emits the 'toggled' signal.
     * @fires .event:toggled */
    toggle: function() {
        this._switch.toggle();
        this.emit('toggled', this._switch.state);
    },

    /** Returns the current state of the switch (on or off).
     * @type {boolean} */
    get state() {
        return this._switch.state;
    },

    /** Sets the current state of the switch (on or off). Does *not* cause
     * the ['toggled']{@link .event:toggled} event to be fired.
     * @param {boolean} state - the state to set the switch to.
     */
    setToggleState: function(state) {
        this._switch.setToggleState(state);
    }
};
/** Emitted when the switch of a {@link PopupSwitchMenuItem} is toggled.
 * @param {PopupSwitchMenuItem} item - the item that emitted the signal
 * @param {boolean} state - whether the item is toggled or not.
 * @name toggled
 * @event
 * @memberof PopupSwitchMenuItem
 */

/** creates a new PopupImageMenuItem
 *
 * ![A {@link PopupImageMenuItem}](pics/PopupImageMenuItem.png)
 *
 * We call the [parent constructor]{@link PopupBaseMenuItem} with `params`.
 *
 * We create a label to display the text, as well as a {@link St.Icon}
 * {@link #_icon} to show the icon. It will be symbolic.
 *
 * We load the icon from `iconName` with {@link #setIcon}.
 * @param {string} text
 * @param {string} iconName
 * @inheritparams PopupBaseMenuItem
 * @classdesc
 * ![A {@link PopupImageMenuItem}](pics/PopupImageMenuItem.png)
 *
 * This is a {@link PopupBaseMenuItem} that displays text and a
 * right-aligned icon. The icon can only be specified by name (rather than
 * by file path) and is symbolic, not full colour.
 *
 * It functions the same as a {@link PopupMenuItem}.
 *
 * @class
 *
 * @example <caption>Creating a PopupImageMenuItem</caption>
 * const PopupMenu = imports.ui.popupMenu;
 * // create the item with text "Contacts" and icon avatar-default.
 * let item = new PopupMenu.PopupImageMenuItem("Contacts", 'avatar-default');
 * // you can connect to its 'activate' signal to know when it is clicked
 * item.connect('activate', function () {
 *     // do something.
 * });
 * @extends PopupBaseMenuItem
 */
function PopupImageMenuItem() {
    this._init.apply(this, arguments);
}

PopupImageMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function (text, iconName, params) {
        PopupBaseMenuItem.prototype._init.call(this, params);

        /** The text for the item.
         * @type {St.Label} */
        this.label = new St.Label({ text: text });
        this.addActor(this.label);
        /** The icon for the item. Symbolic by default. Style class
         * 'popup-menu-icon'.
         * @type {St.Icon} */
        this._icon = new St.Icon({ style_class: 'popup-menu-icon' });
        this.addActor(this._icon, { align: St.Align.END });

        this.setIcon(iconName);
    },

    /** Sets the icon of the item by the icon's name.
     * @param {string} name - name of the icon.
     */
    setIcon: function(name) {
        this._icon.icon_name = name;
    }
};

/** creates a new PopupMenuBase.
 *
 * The main actor is {@link #box} into which all the popup menu items
 * are added.
 *
 * We:
 *
 * * store `sourceActor` in {@link #sourceActor}.
 * * create a vertical St.BoxLayout (with style `styleClass` if provided) in
 *   which all our popup menu items will be placed, {@link #box}.
 * * initialize various configuration and state variables - {@link #length}
 *   (number of top-level children, counting submenus as one child),
 *   {@link #isOpen} (whether the menu is currently open), {@link #blockSourceEvents}
 *   and {@link #passEvents}.
 *
 * @param {Clutter.Actor} sourceActor - the actor that the popup menu
 * popups up from.
 * @param {string} [styleClass] - the style class for the menu (if provided).
 * @classdesc
 * This is the base class for a popup menu.
 *
 * This is an abstract class that needs to be subclassed. The subclasses that
 * already exist are:
 *
 * * {@link PopupMenu},
 * * {@link PopupSubMenu},
 * * {@link PopupMenuSection}, and
 * * {@link PopupComboMenu}.
 *
 * Subclasses **must** at least implement methods 'close' and 'open' to open or
 * close the menu, and assign {@link #actor} to something.
 * Subclasses should note that the popup menu *contents* are in {@link #box},
 * so {@link #actor} might be a {@link BoxPointer.BoxPointer} containing
 * {@link #box} (in the case of the {@link PopupMenu}), or it could be
 * just a scrollview containing {@link #box} to have a scrollable menu
 * with no pointer (in the case of {@link PopupSubMenu}).
 *
 * Some handy methods:
 *
 * * {@link #addAction} - convenience function that creates a {@link PopupMenuItem}
 * (a simple text item) and executes the specified callback upon clicking.
 * * {@link #addSettingsAction} - convenience function that creates a
 * {@link PopupMenuItem} (simple text item) that opens the specified settings widget.
 * * {@link #addMenuItem} - add a popup menu item or popup menu section to the menu.
 * * {@link #toggle} - toggle the menu open and closed.
 * @todo this is abstract.
 * @class
 */
function PopupMenuBase() {
    throw new TypeError('Trying to instantiate abstract class PopupMenuBase');
}

PopupMenuBase.prototype = {
    _init: function(sourceActor, styleClass) {
        /** The top-level actor for the popup menu. **Subclasses of
         * {@linkPopupMenuBase} MUST assign this.**
         * @name actor
         * @memberof PopupMenuBase
         * @type {Clutter.Actor} */

        /** The actor from which the popup menu comes from.
         * @type {Clutter.Actor} */
        this.sourceActor = sourceActor;

        if (styleClass !== undefined) {
            /** Vertical BoxLayout into which all the popup menu items will be
             * added.
             * @type {St.BoxLayout} */
            this.box = new St.BoxLayout({ style_class: styleClass,
                                          vertical: true });
        } else {
            this.box = new St.BoxLayout({ vertical: true });
        }
        this.box.connect_after('queue-relayout', Lang.bind(this, this._menuQueueRelayout));

        /** The number of direct children of this menu that were added using
         * {@link #addMenuItem}.
         * This means that if the menu has a {@link PopupMenuItem} and then a
         * {@link PopupMenuSection} with 2 items and then a {@link PopupSubMenuMenuItem}
         * with 3 items in it, `this.length` is still just 3.
         * @type {number} */
        this.length = 0;

        /** Whether the popup menu is currently open.
         * @type {boolean} */
        this.isOpen = false;

        /** If set, we don't send events (including crossing events) to the source actor
         * for the menu which causes its prelight state to freeze.
         *
         * The above is used in {@link AppDisplay.AppIconMenu} to keep the icon
         * in the 'hovered' state while its menu is open regardless of whether
         * the pointer is actually over the icon or not.
         * @type {boolean} */
        this.blockSourceEvents = false;

        /** Can be set while a menu is up to let all events through without special
         * menu handling. Useful for scrollbars in menus, and probably not otherwise.
         * @type {boolean} */
        this.passEvents = false;

        this._activeMenuItem = null;
        /** An array containing our child menus, if any.
         *
         * A child menu is **not** a sub-menu added to us with {@link #addMenuItem};
         * rather, it is a secondary popup menu that spawns from this one such
         * that if the child menu is open, this menu should be open too.
         *
         * For example, the menu of a {@link PopupComboBoxMenuItem} an entirely
         * separate popup menu to the one the item is in, and must be open
         * simultaneously to the parent item. It is a child menu of the item's
         * menu.
         * @see #addChildMenu
         * @see #removeChildMenu
         * @type {PopupMenuBase[]} */
        this._childMenus = [];
    },

    /** Convenience function that creates a {@link PopupMenuItem} displaying
     * `text` and adds it to the menu. Upon clicking the item, `callback`
     * will be executed.
     * @param {string} title - text to display on the popup menu item.
     * @param {function(Clutter.Event)} callback - executed upon the item
     * being activated. It is passed the event that activated the item (to
     * query for modifiers, for example).
     * @returns {PopupMenuItem} the created popup menu item.
     * @see {@link #addMenuItem} which simply adds a popup menu item (it is up
     * to the user to connect the callback, if any).
     * @see {@link #addSettingsAction} same as this but launches a settings
     * dialog upon activating.
     * @see PopupMenuItem.event:activate
     * @example <caption>Using the 'addAction' function</caption>
     * // This will create an entry saying "Say Hello" and
     * // clicking on it will notify the user.
     * myPopupMenu.addAction("Say Hello", function () {
     *     Main.notify("hello!");
     * });
     */
    addAction: function(title, callback) {
        let menuItem = new PopupMenuItem(title);
        this.addMenuItem(menuItem);
        menuItem.connect('activate', Lang.bind(this, function (menuItem, event) {
            callback(event);
        }));

        return menuItem;
    },

    /** Convenience function that creates a {@link PopupMenuItem} displaying
     * `text` and adds it to the menu. Upon clicking the item, the settings widget
     * specified by `desktopFile` is launched.
     *
     * If we are not in a user session, `null` is returned.
     *
     * @inheritdoc #addAction
     * @param {string} desktopFile - name of the .desktop file for the settings
     * widget, e.g. 'gnome-network-panel.desktop' or 'gnome-datetime-panel.desktop'.
     * @example <caption>Using PopupMenuBase.addSettingsAction</caption>
     * this.addSettingsAction(_("Network Settings"), 'gnome-network-panel.desktop');
     * @see {@link #addMenuItem} which simply adds a popup menu item (it is up
     * to the user to connect the callback, if any).
     * @see {@link #addAction} which adds a popup menu item and executes a
     * generic callback upon activation.
     * @see PopupMenuItem.event:activate
     */
    addSettingsAction: function(title, desktopFile) {
        // Don't allow user settings to get edited unless we're in a user session
        if (global.session_type != Shell.SessionType.USER)
            return null;

        let menuItem = this.addAction(title, function() {
                           let app = Shell.AppSystem.get_default().lookup_setting(desktopFile);

                           if (!app) {
                               log('Settings panel for desktop file ' + desktopFile + ' could not be loaded!');
                               return;
                           }

                           Main.overview.hide();
                           app.activate();
                       });
        return menuItem;
    },

    /** Returns whether `menu` is a child of this menu.
     *
     * In this context "child" means that `menu` is a popup menu that is intended
     * to be open *while* this menu is open.
     *
     * For example, when a {@link PopupComboBoxMenuItem}'s dropdown menu is open, the
     * menu that the item belongs to should also remain open. Hence the
     * dropdown menu is a child of the item's menu.
     * @inheritparams #addChildMenu
     * @returns {boolean} whether `menu` is a child menu of this one.
     */
    isChildMenu: function(menu) {
        return this._childMenus.indexOf(menu) != -1;
    },

    /** Adds `menu` as a child menu of this one.
     *
     * In this context "child" means that `menu` is a popup menu that is intended
     * to be open *while* this menu is open.
     *
     * Adding a menu as a child menu of this one mainly ensures when `menu`
     * is opened from this (its parent) menu, the parent menu remains open while
     * `menu` is open.
     *
     * For example, when a {@link PopupComboBoxMenuItem}'s dropdown menu is open, the
     * menu that the item belongs to should also remain open. Hence the
     * dropdown menu is a child of the item's menu.
     *
     * Emits a signal to notify listeners.
     * @fires .child-menu-added
     * @param {PopupMenuBase} menu - a popup menu.
     * @see {@link PopupComboBoxMenuItem#activate} which uses {@link #addMenu}
     * to add the combo box's menu as a child of the item's menu.
     * @see #removeChildMenu
     */
    addChildMenu: function(menu) {
        if (this.isChildMenu(menu))
            return;

        this._childMenus.push(menu);
        this.emit('child-menu-added', menu);
    },

    /** Removes `menu` as a child menu of this one.
     *
     * In this context "child" means that `menu` is a popup menu that is intended
     * to be open *while* this menu is open.
     *
     * Adding `menu` as a child menu of this one mainly ensures when `menu`
     * is opened from this (its parent) menu, the parent menu remains open while
     * `menu` is open.
     *
     * For example, when a {@link PopupComboBoxMenuItem}'s dropdown menu is open, the
     * menu that the item belongs to should also remain open. Hence the
     * dropdown menu is a child of the item's menu.
     *
     * As far as I can tell, {@link #removeChildMenu} is not used anywhere (GNOME 3.2).
     * @inheritparams #addChildMenu
     * @see {@link PopupComboBoxMenuItem#activate} which uses {@link #addMenu}
     * to add the combo box's menu as a child of the item's menu.
     * @see #addMenu
     * @fires .child-menu-removed
     */
    removeChildMenu: function(menu) {
        let index = this._childMenus.indexOf(menu);

        if (index == -1)
            return;

        this._childMenus.splice(index, 1);
        this.emit('child-menu-removed', menu);
    },

    /** Connects to signals on `menu` that are necessary for
     * operating the submenu, and stores the ids on `object`.
     *
     * In particular:
     *
     * * when any of `menu`'s items are activated, we ourselves emit
     * ['activate']{@link .event:activate} (indicating that one of our
     * items has been activated) and close this (`menu`'s parent menu).
     * * when any of `menu`'s items emits an
     * ['active-changed']{@link PopupMenuBase.event:active-changed} signal,
     * we ensure that only one of our items is active at any one time (so we
     * deactivate any other active item). We then emit
     * ['active-changed']{@link .event:active-changed} from ourselves with the
     * new active item fror the menu (being an item in `menu`).
     *
     * @param {PopupMenuSection|PopupSubMenuMenuItem} object - a submenu item,
     * or a menu section
     * @param {PopupMenuSection|PopupSubMenuMenuItem.menu} menu - the menu of a
     * submenu item, or the menu section.
     * @see PopupMenuBase.event:activate
     * @see PopupMenuBase.event:active-changed
     * @see {@link #_connectItemSignals} which does something similar for non-menu
     * popup items.
     * @todo we do not really fire these events...the callbacks do?
     * @fires .event:activate
     * @fires .event:active-changed
     */
    _connectSubMenuSignals: function(object, menu) {
        object._subMenuActivateId = menu.connect('activate', Lang.bind(this, function() {
            this.emit('activate');
            this.close(true);
        }));
        object._subMenuActiveChangeId = menu.connect('active-changed', Lang.bind(this, function(submenu, submenuItem) {
            if (this._activeMenuItem && this._activeMenuItem != submenuItem)
                this._activeMenuItem.setActive(false);
            this._activeMenuItem = submenuItem;
            this.emit('active-changed', submenuItem);
        }));
    },

    /** Called upon addition of `menuItem` to the menu - we listen to a number
     * of signals of `menuItem` and re-emit them from us (the menu):
     * 
     * * [menuItem's 'active-changed' signal]{@link PopupBaseMenuItem.event:active-changed} -
     * this is emitted when {@link PopupBaseMenuItem#setActive} is called. 
     * We enforce the rule that there may be *only one* active item in a
     * menu at a time (??). We emit ['active-changed']{@link .event:active-changed}
     * from *ourselves* (the menu) with the newly-activated item (or if the
     * item was deactivated, with `null`).
     * * [menuItem's 'sensitive-changed' signal]{@link PopupBaseMenuItem.event:sensitive-changed} -
     * this is emitted when [`menuItem.setSensitive`]{@link PopupBaseMenuItem#setSensitive}
     * is called. If we (the menu) currently have key focus and `menuItem`
     * has become sensitive, it is given key focus. Otherwise if `menuItem` has
     * become insensitive and keyboard focus was on it, it is transferred to
     * the menu.
     * * [menuItem's 'activate' signal]{@link PopupBaseMenuItem.event:activate} -
     * emitted when `menuItem` is clicked/activated. We emit
     * ['activate']{@link .event:activate} with `menuItem` and close ourselves
     * (the menu).
     * * [menuItem's 'destroy' signal]{@link PopupBaseMenuItem.event:destroy} -
     * if `menuItem` is destroyed, we disconnect all of the above signals
     * (the item disappear visually from the menu automatically).
     * @param {PopupBaseMenuItem} menuItem - item to connect.
     * @todo doesn't actually fire, the callback does.
     * @fires .event:active-changed
     * @fires .event:activate
     * @see {@link #_connectItemSignals} which does something similar for menu-like
     * popup items.
     * @see PopupMenuBase.event:activate
     * @see PopupMenuBase.event:active-changed
     * @see PopupMenuBase.event:sensitive-changed
     */
    _connectItemSignals: function(menuItem) {
        menuItem._activeChangeId = menuItem.connect('active-changed', Lang.bind(this, function (menuItem, active) {
            if (active && this._activeMenuItem != menuItem) {
                if (this._activeMenuItem)
                    this._activeMenuItem.setActive(false);
                this._activeMenuItem = menuItem;
                this.emit('active-changed', menuItem);
            } else if (!active && this._activeMenuItem == menuItem) {
                this._activeMenuItem = null;
                this.emit('active-changed', null);
            }
        }));
        menuItem._sensitiveChangeId = menuItem.connect('sensitive-changed', Lang.bind(this, function(menuItem, sensitive) {
            if (!sensitive && this._activeMenuItem == menuItem) {
                if (!this.actor.navigate_focus(menuItem.actor,
                                               Gtk.DirectionType.TAB_FORWARD,
                                               true))
                    this.actor.grab_key_focus();
            } else if (sensitive && this._activeMenuItem == null) {
                if (global.stage.get_key_focus() == this.actor)
                    menuItem.actor.grab_key_focus();
            }
        }));
        menuItem._activateId = menuItem.connect('activate', Lang.bind(this, function (menuItem, event) {
            this.emit('activate', menuItem);
            this.close(true);
        }));
        menuItem.connect('destroy', Lang.bind(this, function(emitter) {
            menuItem.disconnect(menuItem._activateId);
            menuItem.disconnect(menuItem._activeChangeId);
            menuItem.disconnect(menuItem._sensitiveChangeId);
            if (menuItem.menu) {
                menuItem.menu.disconnect(menuItem._subMenuActivateId);
                menuItem.menu.disconnect(menuItem._subMenuActiveChangeId);
                this.disconnect(menuItem._closingId);
            }
            if (menuItem == this._activeMenuItem)
                this._activeMenuItem = null;
        }));
    },

    /** Updates the visibility of `menuItem`, a popup menu separator.
     *
     * If the (visible) item directly before or after the separator is also a
     * separator, or if the separator is the first or last visible item
     * in the menu, it is hidden. Otherwise, it's shown.
     *
     * This is just so that if multiple separators are adjacent to each other,
     * only one is shown. Also, if a separator is at either end of the
     * menu (without an item on both sides) it is hidden. It's all for aesthetics.
     *
     * @param {PopupMenuSeparatorItem} menuItem - separator item.
     */
    _updateSeparatorVisibility: function(menuItem) {
        let children = this.box.get_children();

        let index = children.indexOf(menuItem.actor);

        if (index < 0)
            return;

        let childBeforeIndex = index - 1;

        while (childBeforeIndex >= 0 && !children[childBeforeIndex].visible)
            childBeforeIndex--;

        if (childBeforeIndex < 0
            || children[childBeforeIndex]._delegate instanceof PopupSeparatorMenuItem) {
            menuItem.actor.hide();
            return;
        }

        let childAfterIndex = index + 1;

        while (childAfterIndex < children.length && !children[childAfterIndex].visible)
            childAfterIndex++;

        if (childAfterIndex >= children.length
            || children[childAfterIndex]._delegate instanceof PopupSeparatorMenuItem) {
            menuItem.actor.hide();
            return;
        }

        menuItem.actor.show();
    },

    /** Adds a {@link PopupBaseMenuItem} (or subclass) to this menu.
     *
     * First we add `menuItem.actor` to {@link #box} (our vertical box layout)
     * at the specified `position`.
     *
     * Then we listen to a number of signals of `menuItem` depending on
     * what it is. In general we re-transmit `menuItem`'s 'active-changed'
     * and 'activate' signals from the menu, whether `menuItem` is a
     * menu item or itself a menu.
     *
     * If it's a {@link PopupMenuSection} (a group of items that should
     * otherwise act as top-level children of this menu), we call
     * {@link #_connectSubmenuSignals} to connect up the signals.
     * We listen the section's 'destroy' signal to ensure we disconnect
     * any signals we were listening to and update {@link #length} accordingly.
     *
     * If it's a {@link PopupSubMenuMenuItem} (where it appears as a single
     * item in this menu and clicking on it will expand its submenu), we
     * call *both* {@link #_connectSubmenuSignals} and {@link #_connectItemSignals}
     * to connect to various signals of both the item and its menu.
     * The item's menu (`menuItem.menu.actor`) is also added to {@link #box}
     * below the item (since the submenu appears as part of this menu rather than
     * as an additional popup).
     *
     * Otherwise (`menuItem` isn't menu-like), we call
     * {@link #_connectItemSignals} on it. If it's a {@link PopupSeparatorMenuItem}
     * we additionally ensure that the separator's visibility is updated
     * whenever the menu is opened (see {@link #_updateSeparatorVisibility};
     * when two separators are added adjacent to each other, we only show one).
     *
     * Finally, for all items, we increase {@link #length} by one.
     *
     * @param {PopupBaseMenuItem|PopupMenuSection} menuItem - item to add (could be a
     * {@link PopupMenuItem}, {@link PopupSwitchMenuItem}, etc...)
     * @param {number} [position] - position at which to add the item
     * (0 is the top). If not provided, will be added to the end. Note that
     * a {@link PopupSubMenuMenuItem} and its menu are all counted as one item,
     * and a {@link PopupMenuSection} is also just counted as one item, regardless
     * of the number of items it itself has.
     *
     * @see #_connectItemSignals
     * @see #_connectSubMenuSignals
     * @see #_updateSeparatorVisibility
     */
    addMenuItem: function(menuItem, position) {
        let before_item = null;
        if (position == undefined) {
            this.box.add(menuItem.actor);
        } else {
            let items = this._getMenuItems();
            if (position < items.length) {
                before_item = items[position].actor;
                this.box.insert_before(menuItem.actor, before_item);
            } else
                this.box.add(menuItem.actor);
        }
        if (menuItem instanceof PopupMenuSection) {
            this._connectSubMenuSignals(menuItem, menuItem);
            menuItem.connect('destroy', Lang.bind(this, function() {
                menuItem.disconnect(menuItem._subMenuActivateId);
                menuItem.disconnect(menuItem._subMenuActiveChangeId);

                this.length--;
            }));
        } else if (menuItem instanceof PopupSubMenuMenuItem) {
            if (before_item == null)
                this.box.add(menuItem.menu.actor);
            else
                this.box.insert_before(menuItem.menu.actor, before_item);
            this._connectSubMenuSignals(menuItem, menuItem.menu);
            this._connectItemSignals(menuItem);
            menuItem._closingId = this.connect('open-state-changed', function(self, open) {
                if (!open)
                    menuItem.menu.close(false);
            });
        } else if (menuItem instanceof PopupSeparatorMenuItem) {
            this._connectItemSignals(menuItem);

            // updateSeparatorVisibility needs to get called any time the
            // separator's adjacent siblings change visibility or position.
            // open-state-changed isn't exactly that, but doing it in more
            // precise ways would require a lot more bookkeeping.
            this.connect('open-state-changed', Lang.bind(this, function() { this._updateSeparatorVisibility(menuItem); }));
        } else if (menuItem instanceof PopupBaseMenuItem)
            this._connectItemSignals(menuItem);
        else
            throw TypeError("Invalid argument to PopupMenuBase.addMenuItem()");

        this.length++;
    },

    /** Gets the widths of the columns of the visible popup menu
     * items/sections/submenus in the popup menu.
     *
     * Recall that each popup menu item can be thought of as separated into
     * columns (e.g. a {@link PopupSwitchMenuItem} has the first column
     * displaying text and the second column showing the switch widget).
     *
     * We calculate the column widths over the entire menu, where the width
     * of column `i` is the maximum width of column `i` over all children in
     * the menu.
     *
     * @see PopupBaseMenuItem#getColumnWidths
     * @todo I don't think I've got this right. I don't quite understand what it does.
     */
    getColumnWidths: function() {
        let columnWidths = [];
        let items = this.box.get_children();
        for (let i = 0; i < items.length; i++) {
            if (!items[i].visible)
                continue;
            if (items[i]._delegate instanceof PopupBaseMenuItem || items[i]._delegate instanceof PopupMenuBase) {
                let itemColumnWidths = items[i]._delegate.getColumnWidths();
                for (let j = 0; j < itemColumnWidths.length; j++) {
                    if (j >= columnWidths.length || itemColumnWidths[j] > columnWidths[j])
                        columnWidths[j] = itemColumnWidths[j];
                }
            }
        }
        return columnWidths;
    },

    /** Imposes uniform column widths on all popup menu items in this menu.
     *
     * Recall that each popup menu item can be thought of as separated into
     * columns (e.g. a {@link PopupSwitchMenuItem} has the first column
     * displaying text and the second column showing the switch widget).
     *
     * Now each item's column widths do not have to be the same as any other
     * item in the menu.
     *
     * Calling this function forces each item to have the same column widths
     * as every other item in the menu. I think it'll only make a visual difference
     * if components of the child item were set to be expanded (see
     * {@link PopupBaseMenuItem#addActor}).
     *
     * I think the effect of calling this function is just to ensure that
     * components of the popup menu items line up nicely (for example right-aligned
     * column all align to the same point).
     *
     * @param {number[]} widths - widths to set on each child item.
     * @see PopupBaseMenuItem#setColumnWidths
     * @todo I don't think I've got this right. I don't quite understand what it does.
     */
    setColumnWidths: function(widths) {
        let items = this.box.get_children();
        for (let i = 0; i < items.length; i++) {
            if (items[i]._delegate instanceof PopupBaseMenuItem || items[i]._delegate instanceof PopupMenuBase)
                items[i]._delegate.setColumnWidths(widths);
        }
    },

    /** Called after {@link #box}'s 'queue-relayout' signal.
     *
     * Because of the above column-width funniness ({@link #setColumnWidths}
     * and {@link #getColumnWidths}), we need to do a
     * queue-relayout on every item whenever the menu itself changes
     * size, to force clutter to drop its cached size requests. (The
     * menuitems will in turn call queue_relayout on their parent, the
     * menu, but that call will be a no-op since the menu already
     * has a relayout queued, so we won't get stuck in a loop.
     */
    _menuQueueRelayout: function() {
        this.box.get_children().map(function (actor) { actor.queue_relayout(); });
    },

    /** Adds an actor to the popup menu where it isn't necessarily a
     * {@link PopupBaseMenuItem} or {@link PopupMenuSection}.
     *
     * This simply adds `actor` to {@link #box}.
     *
     * If you want to add a {@link PopupMenuSection} or {@link PopupBaseMenuItem},
     * use {@link #addMenuItem} instead.
     *
     * @see {@link #addMenuItem} which adds a popup menu item and listens to
     * its signals to integrate it into the menu better.
     * @param {Clutter.Actor} actor - actor to add.
     */
    addActor: function(actor) {
        this.box.add(actor);
    },

    /** Retrieves the direct children of {@link #box} (i.e. added with
     * {@link #addMenuItem}).
     * @returns {Array.<PopupBaseMenuItem|PopupMenuSection>} array of items
     * added to the menu with {@link #addMenuItem}.
     */
    _getMenuItems: function() {
        return this.box.get_children().map(function (actor) {
            return actor._delegate;
        }).filter(function(item) {
            return item instanceof PopupBaseMenuItem || item instanceof PopupMenuSection;
        });
    },

    /** The first menu item/menu section in the menu, or `null` if we have no
     * children (that are popup menu items or popup menu sections).
     * @type {?PopupBaseMenuItem|PopupMenuSection} */
    get firstMenuItem() {
        let items = this._getMenuItems();
        if (items.length)
            return items[0];
        else
            return null;
    },

    /** The number of items in the menu that were added with {@link #addMenuItem}.
     * Note that a {@link PopupMenuSection} and {@link PopupSubMenuMenuItem} still
     * only count as one item no matter the number of child items in their menus.
     * @type {number} */
    get numMenuItems() {
        return this._getMenuItems().length;
    },

    /** Removes all the items from the menu. Items added with {@link #addActor}
     * that are not {@link PopupBaseMenuItem}s or {@link PopupMenuSection}s are
     * not removed. */
    removeAll: function() {
        let children = this._getMenuItems();
        for (let i = 0; i < children.length; i++) {
            let item = children[i];
            item.destroy();
        }
    },

    /** Toggles the menu between open and closed.
     *
     * Calls {@link #close} and {@link #open} with parameter `true`, meaning
     * the open/close animation should be animated.
     *
     * The subclasses of {@link PopupMenuBase} should implement the 'open'
     * and 'close' methods for this to work.
     */
    toggle: function() {
        if (this.isOpen)
            this.close(true);
        else
            this.open(true);
    },

    /** Destroys this menu.
     * All child items are destroyed ({@link #removeAll}) as well as our
     * menu actor {@link #actor}.
     * @fires .event:destroy */
    destroy: function() {
        this.removeAll();
        this.actor.destroy();

        this.emit('destroy');
    }

    /** Closes the menu, possibly animating it.
     * **MUST BE IMPLEMENTED BY A SUBCLASS**.
     * @name close
     * @function
     * @param {boolean} animate - whether to animate closing the menu.
     * @memberof PopupMenuBase */
    /** Opens the menu, possibly animating it.
     * **MUST BE IMPLEMENTED BY A SUBCLASS**.
     * @name open
     * @function
     * @param {boolean} animate - whether to animate opening the menu.
     * @memberof PopupMenuBase */
};
Signals.addSignalMethods(PopupMenuBase.prototype);
/** Emitted when a menu is added as a child of this one.
 * @param {PopupMenuBase} menu - menu emitting the signal (that the child menu was added to).
 * @param {PopupMenuBase} childMenu - the child menu that was added.
 * @name child-menu-added
 * @event
 * @see #addChildMenu
 * @memberof PopupMenuBase */
/** Emitted when a menu is removed as a child of this one.
 * @param {PopupMenuBase} menu - menu emitting the signal (that the child menu was removed from).
 * @param {PopupMenuBase} childMenu - the child menu that was removed.
 * @name child-menu-removed
 * @see #removeChildMenu
 * @event
 * @memberof PopupMenuBase */
/** Emitted when any child of this menu changes its active state (has
 * {@link PopupBaseMenuItem#setActive} called on it).
 * @param {PopupMenuBase} menu - menu emitting the signal.
 * @param {?PopupBaseMenuItem} item - the item that was made active, or `null`
 * if it was deactivated (hence the menu has no active item).
 * @name active-changed
 * @see PopupBaseMenuItem.event:active-changed
 * @event
 * @memberof PopupMenuBase */
/** Emitted when any child of this menu is activated (clicked on or the user
 * presses Enter/Space while keyboard focus is on the item).
 * @param {PopupMenuBase} menu - menu emitting the signal.
 * @param {?PopupBaseMenuItem} item - the item that was activated.
 * @name activate
 * @see PopupBaseMenuItem.event:activate
 * @event
 * @memberof PopupMenuBase */
/** Emitted when this menu is destroyed.
 * @param {PopupMenuBase} menu - menu emitting the signal.
 * @name destroy
 * @see #destroy
 * @event
 * @memberof PopupMenuBase */

/** creates a new PopupMenu
 *
 * ![A Popup Menu displaying various items](pics/PopupMenu.collapsed.png)
 *
 * We call the [parent constructor]{@link PopupMenuBase} with `sourceActor`
 * and style class 'popup-menu-content'.
 *
 * ![The popup menu is placed in a {@link BoxPointer.BoxPointer}. `arrowAlignment` is red, `sourceAlignment` is yellow.](pics/BoxPointerDiagram.png)
 *
 * We then create a {@link BoxPointer.BoxPointer} in which the popup menu contents
 * will be placed. The pointer is placed on `arrowSide` of the
 * popup menu. This is stored in {@link #_boxPointer}.
 *
 * Our top-level actor {@link #actor} is set to the pointer's actor
 * `this._boxPointer.actor`. It gets style class 'popup-menu-boxpointer' and
 * 'popup-menu' and is set to be reactive.
 *
 * We then create a Shell.GenericContainer {@link #_boxWrapper} into which we place
 * the popup menu contents {@link #box}. We place {@link #_boxWrapper}
 * into the box pointer to finish constructing the menu.
 *
 * Finally we add the menu's actor to the global focus manager (to help out
 * with keyboard navigation and other focus issues).
 *
 * @inheritparams PopupMenuBase
 * @param {number} arrowAlignment - fraction of the popup menu's side along which
 * to place the arrow.
 * @param {St.Side} arrowSide - the side of the popup menu that the pointer should
 * come from. For example `St.Side.TOP` means the popup menu is positioned
 * below `sourceActor` and the pointer is on the top side pointing up to
 * `sourceActor`.
 * pointer is drawn; in the picture above this is `St.Side.TOP` (since the
 * pointer is on the top side of the menu).
 * @classdesc
 * This is an implementation of {@link PopupMenuBase} used for almost all
 * popup menus in gnome-shell.
 *
 * ![A Popup Menu displaying various items](pics/PopupMenu.collapsed.png)
 *
 * It is a top-level popup menu. That is, it includes the 'pointer' between
 * the menu and the source actor and draws the border around the entire
 * {@link PopupMenuBase}.
 *
 * @class
 * @extends PopupMenuBase
 * @example <caption>How to create a standalone Popup Menu</caption>
 * // note - it's recommended you use a PanelMenu.Button which comes with its
 * // own menu rather than creating your own. But this is how you do it:
 * const PopupMenu = imports.ui.popupMenu;
 *
 * // 1. create the menu coming from sourceActor (presumed already defined)
 * //    we want the arrow to be on the top side of the menu in the middle.
 * let menu = new PopupMenu.PopupMenu(sourceActor, 0.5, St.Side.TOP);
 * // 2. add to the stage
 * Main.uiGroup.add_actor(menu.actor);
 * // 3. add it to a PopupMenuManager -- if it's in the top panel use that
 * //    one (Main.panel._menus), otherwise create your own. Recall that when a
 * //    menu is opened the menu manager's owner is pushed modally.
 * let manager = new PopupMenu.PopupMenuManager(sourceActor);
 * manager.addMenu(menu);
 * // 4a. hide the menu.
 * menu.actor.hide();
 * // 4b. make sure you connect up some code somewhere that will launch the
 * //     menu with menu.open(true); (example clicking on a button).
 *
 */
function PopupMenu() {
    this._init.apply(this, arguments);
}

PopupMenu.prototype = {
    __proto__: PopupMenuBase.prototype,

    _init: function(sourceActor, arrowAlignment, arrowSide) {
        PopupMenuBase.prototype._init.call (this, sourceActor, 'popup-menu-content');

        /** Fraction of the popup menu's width along which we place the
         * popup menu's pointer. Number between 0 and 1.
         *
         * ![`arrowAlignment` is red.](pics/BoxPointerDiagram.png)
         * @type {number} */
        this._arrowAlignment = arrowAlignment;
        /** The side of the popup menu that the pointer will come from.
         * @type {St.Side} */
        this._arrowSide = arrowSide;

        /** The box pointer for the menu (the arrow that points to `sourceActor`
         * and surrounds the popup menu).
         * @type {BoxPointer.BoxPointer}
         */
        this._boxPointer = new BoxPointer.BoxPointer(arrowSide,
                                                     { x_fill: true,
                                                       y_fill: true,
                                                       x_align: St.Align.START });
        /** Top-level actor for the popup menu. This is the actor of
         * {@link #_boxPointer}, the container for the popup menu contents
         * (drawing the pointer and border of the popup menu).
         * @type {Clutter.Actor} */
        this.actor = this._boxPointer.actor;
        this.actor._delegate = this;
        this.actor.style_class = 'popup-menu-boxpointer';
        this.actor.connect('key-press-event', Lang.bind(this, this._onKeyPressEvent));

        /** We place the popup menu's contents {@link #box} into this wrapper
         * and place the wrapper into the box pointer.
         * @type {Shell.GenericContainer} */
        this._boxWrapper = new Shell.GenericContainer();
        this._boxWrapper.connect('get-preferred-width', Lang.bind(this, this._boxGetPreferredWidth));
        this._boxWrapper.connect('get-preferred-height', Lang.bind(this, this._boxGetPreferredHeight));
        this._boxWrapper.connect('allocate', Lang.bind(this, this._boxAllocate));
        this._boxPointer.bin.set_child(this._boxWrapper);
        this._boxWrapper.add_actor(this.box);
        this.actor.add_style_class_name('popup-menu');

        global.focus_manager.add_group(this.actor);
        this.actor.reactive = true;
    },

    /** Callback for {@link #_boxWrapper}'s 'get-preferred-width' signal.
     *
     * We get the column widths for all our items ({@link #getColumnWidths}),
     * and explicitly set it with {@link #setColumnWidths} so that every actor
     * gets its natural preferred width.
     *
     * We assign the resulting width of {@link #box} as the requested width (now
     * that all the child items have their requested width).
     */
    _boxGetPreferredWidth: function (actor, forHeight, alloc) {
        let columnWidths = this.getColumnWidths();
        this.setColumnWidths(columnWidths);

        // Now they will request the right sizes
        [alloc.min_size, alloc.natural_size] = this.box.get_preferred_width(forHeight);
    },

    /** Callback for {@link #_boxWrapper}'s 'get-preferred-height' signal.
     * We give {@link #box}'s preferred height.
     */
    _boxGetPreferredHeight: function (actor, forWidth, alloc) {
        [alloc.min_size, alloc.natural_size] = this.box.get_preferred_height(forWidth);
    },

    /** Callback for {@link #_boxWrapper}'s 'allocate' signal.
     * We simply allocate {@link #box} the entire space provided (it's the
     * only child of {@link #_boxWrapper}).
     */
    _boxAllocate: function (actor, box, flags) {
        this.box.allocate(box, flags);
    },

    /** Callback when a key press is receieved while the menu has keyboard
     * focus. If the user pressed 'Escape' we close the menu and return `true`
     * to indicate that we've handled the event. Otherwise we return `false`.
     */
    _onKeyPressEvent: function(actor, event) {
        if (event.get_key_symbol() == Clutter.Escape) {
            this.close(true);
            return true;
        }

        return false;
    },

    /** Sets the position of the popup menu pointer relative to the popup menu
     * (in pixels).
     *
     * ![`arrowOrigin` is red.](pics/BoxPointerDiagram.png)
     *
     * @see BoxPointer.BoxPointer#setArrowOrigin
     * @param {number} origin - number of pixels along the popup menu to position
     * the pointer.
     */
    setArrowOrigin: function(origin) {
        this._boxPointer.setArrowOrigin(origin);
    },

    /** Sets where relative to the *source* actor the point of the menu is
     * positioned at.
     *
     * ![`alignment` is yellow.](pics/BoxPointerDiagram.png)
     *
     * @param {number} alignment - How far along the source actor we should
     * position the tip of the pointer. Fraction of the source actor's width/height,
     * between 0 and 1.
     */
    setSourceAlignment: function(alignment) {
        this._boxPointer.setSourceAlignment(alignment);
    },

    /** Implements the parent function to open the popup menu, animating if
     * `animate` is true.
     *
     * We call {@link BoxPointer.BoxPointer#show} to do the animation,
     * ensure our actor is top-most, and emit 'open-state-changed' to let
     * listeners know we've opened.
     * @fires .open-state-changed
     * @override
     */
    open: function(animate) {
        if (this.isOpen)
            return;

        this.isOpen = true;

        this._boxPointer.setPosition(this.sourceActor, this._arrowAlignment);
        this._boxPointer.show(animate);

        this.actor.raise_top();

        this.emit('open-state-changed', true);
    },

    /** Implements the parent function to close the popup menu, animating if
     * `animate` is true.
     *
     * We call {@link BoxPointer.BoxPointer#hide} to do the animation,
     * and emit 'open-state-changed' to let listeners know we've closed.
     * @fires .open-state-changed
     * @override
     */
    close: function(animate) {
        if (!this.isOpen)
            return;

        if (this._activeMenuItem)
            this._activeMenuItem.setActive(false);

        this._boxPointer.hide(animate);

        this.isOpen = false;
        this.emit('open-state-changed', false);
    }
};
/** Emitted whenever a popup menu opens or closes.
 * @name open-state-changed
 * @param {PopupMenuBase} menu - the child menu that was added.
 * @param {boolean} open - whether the menu is open or closed.
 * @memberof PopupMenuBase
 */

/** creates a new PopupSubMenu.
 *
 * This is essentially a {@link PopupMenuBase} that has vertically-scrollable menu
 * contents, and is designed to be nested into another menu.
 *
 * We call the [parent constructor]{@link PopupMenuBase} with `sourceActor`
 * (and no additional style classes). Note that in this case, `sourceActor`
 * **should be a {@link PopupBaseMenuItem}**.
 *
 * We store `sourceArrow` as {@link #_arrow} and set it to rotate about
 * its centre (we will rotate it clockwise 90 degrees when the submenu is opened).
 *
 * We create our top-level actor {@link #actor}, a {@link St.ScrollView} with
 * style class 'popup-sub-menu'. It is set to scroll vertically.
 * We want submenus to be scrollable vertically because a common use of
 * submenus is to provide a "More..." expander with long content.
 *
 * However we set the scrollview's policy to NEVER and *manually* switch it
 * to AUTOMATIC when it is needed (and then back to NEVER when it isn't).
 * This is for a better visual look, because the scrollbar always requests
 * horizontal space even when it isn't showing (in AUTOMATIC mode), and this
 * makes the popup menu look weird since scrollbars aren't needed most of the time.
 *
 * We add the popup menu contents {@link #box} to our scrollbox, and then
 * hide ourselves.
 *
 * Note that the popup sub menu doesn't use a {@link BoxPointer.BoxPointer}
 * like the {@link PopupMenu} does because it is designed to be nested *within*
 * a {@link PopupMenu} and hence doesn't need its own pointer.
 *
 * @inheritparams PopupMenuBase
 * @param {Clutter.Actor} sourceArrow - an actor that will be rotated clockwise
 * upon the menu being opened. For example, the {@link PopupSubMenuMenuItem}
 * provides a St.Label with a right-pointing arrow ▻ that will be rotated to
 * a downwards-pointing arrow when the submenu is opened.
 * @classdesc
 * The PopupSubMenu is a helper class used in the {@link PopupSubMenuMenuItem}.
 * It is designed to be a menu that lives within a parent menu.
 *
 * ![A {@link PopupSubMenuMenuItem}. Its {@link PopupSubMenu} is currently displaying](pics/PopupSubMenuMenuItem.expanded.png)
 *
 * It is the 'menu' part of the {@link PopupSubMenuMenuItem}.
 *
 * It also manages animating an 'arrow' actor to rotate when the menu is
 * opened or closed - for example the {@link PopupSubMenuMenuItem} displays
 * a right-pointing arrow ▻ while the menu is closed, which rotates to a
 * downwards-pointing arrow when the menu is opened.
 *
 * This is essentially a {@link PopupMenuBase} that has vertically-scrollable menu
 * contents, and is designed to be nested into another menu.
 *
 * @class
 * @extends PopupMenuBase
 */
function PopupSubMenu() {
    this._init.apply(this, arguments);
}

PopupSubMenu.prototype = {
    __proto__: PopupMenuBase.prototype,

    _init: function(sourceActor, sourceArrow) {
        PopupMenuBase.prototype._init.call(this, sourceActor);

        /** The "arrow" actor for this submenu. It is just some sort of
         * actor that will be rotated 90 degrees clockwise when the
         * menu is opened.
         *
         * For example, you could provide a label showing a right-pointing
         * arrow ▻ that rotates to a downwards-pointing arrow when the submenu
         * is open.
         * @type {Clutter.Actor} */
        this._arrow = sourceArrow;
        this._arrow.rotation_center_z_gravity = Clutter.Gravity.CENTER;

        /** The top-level actor for the popup submenu is a scroll box that
         * is meant to scroll vertically. We place the popup menu
         * contents {@link #box} into this.
         *
         * Since a function of a submenu might be to provide a "More.." expander
         * with long content, we make it scrollable - the scrollbar will only take
         * effect if a CSS max-height is set on the top menu.
         * @type {St.ScrollView}
         */
        this.actor = new St.ScrollView({ style_class: 'popup-sub-menu',
                                         hscrollbar_policy: Gtk.PolicyType.NEVER,
                                         vscrollbar_policy: Gtk.PolicyType.NEVER });

        // StScrollbar plays dirty tricks with events, calling
        // clutter_set_motion_events_enabled (FALSE) during the scroll; this
        // confuses our event tracking, so we just turn it off during the
        // scroll.
        let vscroll = this.actor.get_vscroll_bar();
        vscroll.connect('scroll-start',
                        Lang.bind(this, function() {
                                      let topMenu = this._getTopMenu();
                                      if (topMenu)
                                          topMenu.passEvents = true;
                                  }));
        vscroll.connect('scroll-stop',
                        Lang.bind(this, function() {
                                      let topMenu = this._getTopMenu();
                                      if (topMenu)
                                          topMenu.passEvents = false;
                                  }));

        this.actor.add_actor(this.box);
        this.actor._delegate = this;
        this.actor.clip_to_allocation = true;
        this.actor.connect('key-press-event', Lang.bind(this, this._onKeyPressEvent));
        this.actor.hide();
    },

    /** Gets the top-level {@link PopupMenu} that this is a submenu of.
     *
     * We walk up {@link #actor}'s parent chain until we find a {@link PopupMenu}
     * and return that.
     * @returns {?PopupMenu} the {@link PopupMenu} ancestor of this actor, or
     * `null` if none.
     */
    _getTopMenu: function() {
        let actor = this.actor.get_parent();
        while (actor) {
            if (actor._delegate && actor._delegate instanceof PopupMenu)
                return actor._delegate;

            actor = actor.get_parent();
        }

        return null;
    },

    /** Asks whether this submenu needs its vertical scrollbar to be visible.
     *
     * We get the natural height of our parent menu and the maximum height of
     * our parent menu (according to its CSS style).
     *
     * If the natural height is greater than the maximum height and the
     * maximum height was actually set, we return `true` as we need a
     * vertical scrollbar.
     * @returns {boolean} whether the vertical scrollbar should be visible. */
    _needsScrollbar: function() {
        let topMenu = this._getTopMenu();
        let [topMinHeight, topNaturalHeight] = topMenu.actor.get_preferred_height(-1);
        let topThemeNode = topMenu.actor.get_theme_node();

        let topMaxHeight = topThemeNode.get_max_height();
        return topMaxHeight >= 0 && topNaturalHeight >= topMaxHeight;
    },

    /** Implements the parent function to opens the menu.
     *
     * First we work out whether we need to show the scrollbar. If we do,
     * we set our scrollbar policy to AUTOMATIC. Otherwise, we set it to NEVER.
     *
     * The reason we don't just leave it on AUTOMATIC all the time is for a
     * better visual look.
     * St.ScrollView always requests horizontal space for the vertical scrollbar
     * in AUTOMATIC mode even if it's not showing. This looks bad when we
     * *don't* need it (which is most of the time), so we just turn off the
     * scrollbar manually.
     *
     * A consequence of this is that if items are dynamically added or removed
     * while the submenu is open, the scrollbar policy won't be updated again
     * until you close the menu and re-open it.
     *
     * If the user set `animate` to true but we need to add a scrollbar (that
     * wasn't there before), we skip animating (how do you animate in a scrollbar
     * while you're simultaneously animating in other things?).
     *
     * If we are going to animate the menu opening, we animate:
     * 
     * * the submenu growing vertically to its natural height, and
     * * the arrow {@link #_arrow} rotating 90 degrees clockwise to indicate
     *   that the submenu is open(ing).
     *
     * Upon completion of this (whether we animated or not), we emit
     * 'open-state-changed'.
     *
     * @override
     * @fires .event:open-state-changed
     */
    open: function(animate) {
        if (this.isOpen)
            return;

        this.isOpen = true;

        this.actor.show();

        let needsScrollbar = this._needsScrollbar();

        // St.ScrollView always requests space horizontally for a possible vertical
        // scrollbar if in AUTOMATIC mode. Doing better would require implementation
        // of width-for-height in St.BoxLayout and St.ScrollView. This looks bad
        // when we *don't* need it, so turn off the scrollbar when that's true.
        // Dynamic changes in whether we need it aren't handled properly.
        this.actor.vscrollbar_policy =
            needsScrollbar ? Gtk.PolicyType.AUTOMATIC : Gtk.PolicyType.NEVER;

        // It looks funny if we animate with a scrollbar (at what point is
        // the scrollbar added?) so just skip that case
        if (animate && needsScrollbar)
            animate = false;

        if (animate) {
            let [minHeight, naturalHeight] = this.actor.get_preferred_height(-1);
            this.actor.height = 0;
            this.actor._arrow_rotation = this._arrow.rotation_angle_z;
            Tweener.addTween(this.actor,
                             { _arrow_rotation: 90,
                               height: naturalHeight,
                               time: 0.25,
                               onUpdateScope: this,
                               onUpdate: function() {
                                   this._arrow.rotation_angle_z = this.actor._arrow_rotation;
                               },
                               onCompleteScope: this,
                               onComplete: function() {
                                   this.actor.set_height(-1);
                                   this.emit('open-state-changed', true);
                               }
                             });
        } else {
            this._arrow.rotation_angle_z = 90;
            this.emit('open-state-changed', true);
        }
    },

    /** Closes the submenu, possibly animating it.
     *
     * If animation is requested we:
     *
     * * rotate {@link #_arrow}'s back to normal (it was rotated 90 degrees
     * clockwise in {@link #open});
     * * animate the menu {@link #actor} shrinking down to 0 height (to
     * give the appearance of it sliding closed).
     *
     * If animation wasn't requested we just set {@link #_arrow}'s rotation
     * back to normal and hide {@link #actor}.
     *
     * Finally we emit 'open-state-changed'.
     * @fires .event:open-state-changed
     * @override
     */
    close: function(animate) {
        if (!this.isOpen)
            return;

        this.isOpen = false;

        if (this._activeMenuItem)
            this._activeMenuItem.setActive(false);

        if (animate && this._needsScrollbar())
            animate = false;

        if (animate) {
            this.actor._arrow_rotation = this._arrow.rotation_angle_z;
            Tweener.addTween(this.actor,
                             { _arrow_rotation: 0,
                               height: 0,
                               time: 0.25,
                               onCompleteScope: this,
                               onComplete: function() {
                                   this.actor.hide();
                                   this.actor.set_height(-1);

                                   this.emit('open-state-changed', false);
                               },
                               onUpdateScope: this,
                               onUpdate: function() {
                                   this._arrow.rotation_angle_z = this.actor._arrow_rotation;
                               }
                             });
            } else {
                this._arrow.rotation_angle_z = 0;
                this.actor.hide();

                this.isOpen = false;
                this.emit('open-state-changed', false);
            }
    },

    /** Callback if the user presses a key while keyboard focus is on the
     * submenu.
     *
     * If they pressed the left arrow we put focus back to the parent menu and
     * close the submenu.
     */
    _onKeyPressEvent: function(actor, event) {
        // Move focus back to parent menu if the user types Left.

        if (this.isOpen && event.get_key_symbol() == Clutter.KEY_Left) {
            this.close(true);
            this.sourceActor._delegate.setActive(true);
            return true;
        }

        return false;
    }
};

/** creates a new PopupMenuSection
 *
 * We call the [parent constructor]{@link PopupMenuBase} (with no arguments).
 *
 * We assign {@link #actor} to be {@link #box}, and set ourselves to be open.
 *
 * @classdesc
 * This is a section of a {@link PopupMenu} which is handled like a submenu
 * (you can add and remove items, you can destroy it, you
 * can add it to another menu), but is completely transparent
 * to the user. That is, its children look as if they were children of
 * the parent menu.
 *
 * It's just a handy way to group sets of popup menu items within a parent menu.
 *
 * The popup menu section doesn't open or close, it is permanently open.
 * @class
 * @extends PopupMenuBase
 * @example <caption>Adding a PopupMenuSection to an existing popup menu.</caption>
 * // suppose myPopupMenu is the parent menu.
 * let section = new PopupMenuSection();
 * // now add various items to the popup menu 
 * section.addMenuItem(new PopupMenuItem("asdf"));
 * section.addMenuItem(new PopupSeparatorMenuItem());
 * section.addMenuItem(new PopupMenuItem("fdsa"));
 * // add thte section to the parent meny myPopupMenu
 * myPopupMenu.addMenuItem(section);
 */
function PopupMenuSection() {
    this._init.apply(this, arguments);
}

PopupMenuSection.prototype = {
    __proto__: PopupMenuBase.prototype,

    _init: function() {
        PopupMenuBase.prototype._init.call(this);

        /** The top-level actor for the PopupMenuSection is {@link #box},
         * i.e. the contents of the popup menu.
         * @type {St.BoxLayout} */
        this.actor = this.box;
        this.actor._delegate = this;
        this.isOpen = true;
    },

    // deliberately ignore any attempt to open() or close()
    /** Implement the parent function. A {@link PopupMenuSection} is permanently
     * open so this function does nothing.
     * @override
     */
    open: function(animate) { },
    /** Implement the parent function. A {@link PopupMenuSection} is permanently
     * open so this function does nothing.
     * @override
     */
    close: function() { },
}

/** creates a new PopupSubMenuMenuItem.
 *
 * This is a {@link PopupBaseMenuItem} that shows `text` on it, along with
 * a right-pointing triangle to the right ▻.
 *
 * Upon clicking the item, a submenu expands and the triangle rotates to point
 * downwards.
 *
 * This is achieved by having the SubMenuMenuItem inherit from {@link PopupBaseMenuItem}
 * but have a member `this.menu` which is a {@link PopupSubMenu}.
 *
 * First we call the [parent constructor]{@link PopupBaseMenuItem} to make our
 * popup menu item and add style class 'popu-submenu-menu-item' to ourselves.
 *
 * We make a label to display `text` and {@link #addActor} it ({@link #label)}.
 * Then we make another label {@link #_triangle} to display the right-pointing
 * arrow ▻. This arrow will be rotated 90 degrees clockwise when the menu opens,
 * and back when it closes. We {@link #addActor} this to ourselves too,
 * aligned right.
 *
 * Finally we create our submenu, a {@link PopupSubMenu}, and store it in
 * {@link #menu}.
 *
 * @param {string} text - text to display on the item.
 * @see PopupSubMenu
 * @classdesc
 * The PopupSubMenuMenuItem is a submenu item in a popup menu.
 *
 * ![A {@link PopupSubMenuMenuItem}, collapsed](pics/PopupSubMenuMenuItem.png)
 *
 * ![A {@link PopupSubMenuMenuItem}, expanded](pics/PopupSubMenuMenuItem.expanded.png)
 * 
 * It displays text on the item and clicking on it will expand or collapse its submenu.
 *
 * The item itself inherits {@link PopupBaseMenuItem}, and it has a member
 * `this.menu`, a {@link PopupSubMenu}.
 *
 * @class
 * @example <caption>Creating and populating PopupSubMenuMenuItem</caption>
 * // create the submenu item
 * let item = new PopupSubMenuMenuItem("my submenu");
 * // add some items to the submenu with item.menu:
 * item.menu.addMenuItem(new PopupMenuItem("first item"));
 * item.menu.addMenuItem(new PopupMenuItem("second item item")); // and so on.
 * @extends PopupBaseMenuItem
 */
function PopupSubMenuMenuItem() {
    this._init.apply(this, arguments);
}

PopupSubMenuMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function(text) {
        PopupBaseMenuItem.prototype._init.call(this);

        this.actor.add_style_class_name('popup-submenu-menu-item');

        /** Displays the text of the popup submenu item.
         * @type {St.Label} */
        this.label = new St.Label({ text: text });
        this.addActor(this.label);
        /** Displays a right-pointing triangle ▻ at the end of the item that
         * indicates the submenu is closed. Upon opening the submenu it will
         * rotate to point downwards.
         * @type {St.Label} */
        this._triangle = new St.Label({ text: '\u25B8' });
        this.addActor(this._triangle, { align: St.Align.END });

        /** The submenu for this item.
         * @type {PopupSubMenu} */
        this.menu = new PopupSubMenu(this.actor, this._triangle);
        this.menu.connect('open-state-changed', Lang.bind(this, this._subMenuOpenStateChanged));
    },

    /** Callback when the submenu is opened or closed. While the submenu
     * is open, we add pseudo style class 'open' to {@link #actor} (this
     * highlights the submenu item in a different colour to indicate that it
     * is open).
     * @inheritdoc PopupSubMenu.event:open-state-changed
     * @listens PopupSubMenu.event:open-state-changed
     * @todo '@listens' or '@handles'?
     */
    _subMenuOpenStateChanged: function(menu, open) {
        if (open)
            this.actor.add_style_pseudo_class('open');
        else
            this.actor.remove_style_pseudo_class('open');
    },

    /** Extends the parent method to destroy this item. In addition to calling
     * the parent method, we destroy our submenu {@link #menu}.
     * @override */
    destroy: function() {
        this.menu.destroy();
        PopupBaseMenuItem.prototype.destroy.call(this);
    },

    /** Overrides the parent function - this is what happens when a key is
     * pressed while the submenu item has focus.
     *
     * If the user pressed the right arrow, we open the submenu and transfer
     * keyboard navigation to the submenu.
     *
     * If the user pressed the left arrow and the submenu is open, we close the
     * submenu.
     *
     * Otherwise, we call the [parent method]{@link PopupBaseMenuItem#_onKeyPressEvent}
     * (listens for pressing Enter or Space which activates the item).
     * @override
     */
    _onKeyPressEvent: function(actor, event) {
        let symbol = event.get_key_symbol();

        if (symbol == Clutter.KEY_Right) {
            this.menu.open(true);
            this.menu.actor.navigate_focus(null, Gtk.DirectionType.DOWN, false);
            return true;
        } else if (symbol == Clutter.KEY_Left && this.menu.isOpen) {
            this.menu.close();
            return true;
        }

        return PopupBaseMenuItem.prototype._onKeyPressEvent.call(this, actor, event);
    },

    /** Overrides the parent method - this is what happens when the item
     * is activated, by clicking on it or pressing Enter or Space while keyboard
     * focus is on it.
     *
     * When we are activated, we open our submenu {@link #menu} and do nothing
     * else.
     *
     * In particular, we **do not** emit the
     * ['activate']{@link PopupBaseMenuItem.event:activate} signal (which the
     * default implementation does).
     * @override
     */
    activate: function(event) {
        this.menu.open(true);
    },

    /** Overrides the parent function - this is what happens when the item
     * is clicked on.
     *
     * While the parent method calls {@link #activate}, we **do not**. Instead,
     * we toggle the submenu {@link #menu}.
     * @override
     */
    _onButtonReleaseEvent: function(actor) {
        this.menu.toggle();
    }
};

/** creates a new PopupComboMenu.
 *
 * We call the [parent constructor]{@link PopupMenuBase} with `sourceActor`
 * and style class 'popup-combo-menu'.
 *
 * For the popup combo menu, `sourceActor` **should be the actor of the parent
 * {@link PopupComboBoxMenuItem}**.
 *
 * We set our top-level actor {@link #actor} to {@link #box} (the menu
 * contents).
 * @inheritparams PopupMenuBase
 * @classdesc
 *
 * The {@link PopupComboBoxMenuItem} is a combo box for the popup menu (it
 * has a dropdown menu).
 *
 * This is the 'menu' part of that item.
 *
 * ![{@link PopupComboBoxMenuItem}](pics/PopupComboBoxMenuItem.png)
 * ![{@link PopupComboBoxMenuItem} with its menu (a {@link PopupComboMenu}) open](pics/PopupComboBoxMenuItem.expanded.png)
 *
 * Since the menu part of a combo box inherits {@link PopupMenuBase}, this
 * means that *any* popup menu item can be placed into a combo box, not just
 * text.
 *
 * The combo box menu has different styling to the other menus. It isn't in
 * a scrollable box like the {@link PopupSubMenu} and doesn't need a pointer
 * like the {@link PopupMenu}. The style is defined by its CSS style class
 * 'popup-combo-menu' (it has rounded corners, a different-coloured border, ...).
 * @class
 * @extends PopupMenuBase
 */
function PopupComboMenu() {
    this._init.apply(this, arguments);
}

PopupComboMenu.prototype = {
    __proto__: PopupMenuBase.prototype,

    _init: function(sourceActor) {
        PopupMenuBase.prototype._init.call(this,
                                           sourceActor, 'popup-combo-menu');
        /** The top-level actor for the PopupComboMenu is {@link #box},
         * i.e. the contents of the popup menu.
         * @type {St.BoxLayout} */
        this.actor = this.box;
        this.actor._delegate = this;
        this.actor.connect('key-press-event', Lang.bind(this, this._onKeyPressEvent));
        this.actor.connect('key-focus-in', Lang.bind(this, this._onKeyFocusIn));
        /** The index of the selected item of the combo box.
         * @type {number} */
        this._activeItemPos = -1;
        global.focus_manager.add_group(this.actor);
    },

    /** Callback when the user presses a key while focus is on the combo menu.
     * If they pressed 'escape', we close the menu.
     */
    _onKeyPressEvent: function(actor, event) {
        if (event.get_key_symbol() == Clutter.Escape) {
            this.close(true);
            return true;
        }

        return false;
    },

    /** Callback when keyboard focus is transferred to the combo menu.
     *
     * We ensure that the active item (item that was selected in the combo box)
     * is given keyboard focus.
     */
    _onKeyFocusIn: function(actor) {
        let items = this._getMenuItems();
        let activeItem = items[this._activeItemPos];
        activeItem.actor.grab_key_focus();
    },

    /** Implements the parent function to open the combo box menu.
     * This is always animated.
     *
     * To open the menu we fade it in (opacity 0 to 255) over
     * {@link BoxPointer.POPUP_ANIMATION_TIME} seconds.
     *
     * First though, we carefully position the menu so that:
     *
     * * the active item (that was previously selected and visible in the combo box) appears
     * in line with the combo box (so that it does not appear to "jump" as
     * the menu opens);
     * * the menu's width matches that of its combo box item.
     *
     * Then we emit 'open-state-changed' to indicate that we're open.
     * @fires .open-state-changed
     * @override */
    open: function() {
        if (this.isOpen)
            return;

        this.isOpen = true;

        let [sourceX, sourceY] = this.sourceActor.get_transformed_position();
        let items = this._getMenuItems();
        let activeItem = items[this._activeItemPos];

        this.actor.set_position(sourceX, sourceY - activeItem.actor.y);
        this.actor.width = Math.max(this.actor.width, this.sourceActor.width);
        this.actor.raise_top();

        this.actor.opacity = 0;
        this.actor.show();

        Tweener.addTween(this.actor,
                         { opacity: 255,
                           transition: 'linear',
                           time: BoxPointer.POPUP_ANIMATION_TIME });

        this.emit('open-state-changed', true);
    },

    /** Implements the parent function to close the combo box menu. This is
     * always animated.
     *
     * To close the menu we fade it out (opacity 255 to 0) over
     * {@link BoxPointer.POPUP_ANIMATION_TIME} seconds.
     *
     * Then we emit 'open-state-changed' to indicate that we're closed.
     * @fires .open-state-changed
     * @override */
    close: function() {
        if (!this.isOpen)
            return;

        this.isOpen = false;
        Tweener.addTween(this.actor,
                         { opacity: 0,
                           transition: 'linear',
                           time: BoxPointer.POPUP_ANIMATION_TIME,
                           onComplete: Lang.bind(this,
                               function() {
                                   this.actor.hide();
                               })
                         });

        this.emit('open-state-changed', false);
    },

    /** Sets the active (selected) item of the combo box menu.
     *
     * There is no corresponding getter for this! However the combo box
     * menu item's ['active-item-changed']{@link PopupComboBoxMenuItemm.event:active-item-changed}
     * signal will indicate the index of the item made active.
     * @param {number} position - the index of the item in the menu to set
     * active.
     * @see PopupComboBoxMenuItem.event:active-item-changed
     */
    setActiveItem: function(position) {
        this._activeItemPos = position;
    },

    /** Sets one of the combo menu items to be visible or not. You cannot
     * make the active item invisible.
     * @param {number} position - index of the item in the menu.
     * @param {boolean} visible - whether the item should be visible or not.
     * @see #getItemVisible
     */
    setItemVisible: function(position, visible) {
        if (!visible && position == this._activeItemPos) {
            log('Trying to hide the active menu item.');
            return;
        }

        this._getMenuItems()[position].actor.visible = visible;
    },

    /** Returns whether the item at index `position` within the combo menu is
     * visible.
     * @inheritparams #setItemVisible
     * @returns {boolean} whether the item at index `position` is visible.
     * @see #setItemVisible
     */
    getItemVisible: function(position) {
        return this._getMenuItems()[position].actor.visible;
    }
};

/** creates a new PopupComboBoxMenuItem.
 *
 * ![{@link PopupComboBoxMenuItem}](pics/PopupComboBoxMenuItem.png)
 *
 * First we call the [parent constructor]{@link PopupBaseMenuItem} with `params`.
 *
 * Then we create the widget that will display the currently-selected item
 * (called the "active item"). This is a {@link Shell.Stack} stored as
 * {@link #_itemBox}. It will display the active item.
 *
 * We create a label displaying the drop-down arrow '⌄' and align it to the right
 * of the item (it isn't stored).
 *
 * We create the [menu for the combo box, using {@link #actor} as its source
 * actor, and store it in {@link #_menu}. The menu is a {@link PopupComboMenu}.
 *
 * @inheritparams PopupBaseMenuItem
 * @classdesc
 *
 * ![{@link PopupComboBoxMenuItem}](pics/PopupComboBoxMenuItem.png)
 * ![{@link PopupComboBoxMenuItem} with its menu (a {@link PopupComboMenu}) open](pics/PopupComboBoxMenuItem.expanded.png)
 *
 * This is a popup menu item containing a combo box widget - clicking on it
 * will display a menu from which other items can be selected.
 *
 * The item itself inherits from {@link PopupBaseMenuItem} and the menu
 * is achieved with a {@link PopupComboMenu}.
 *
 * Note that *any* popup menu item can be added to the combo box, not just
 * text. Add an item to the combo with {@link #addMenuItem}. Connect to the
 * ['active-item-changed']{@link .event:active-item-changed} signal to
 * know when the user selects an item from the combo box.
 *
 * The combo box menu item does **not** emit 'activate'; instead it emits
 * ['active-item-changed']{@link .active-item-changed} when the user selects
 * an item from the combo menu.
 *
 * @class
 * @extends PopupBaseMenuItem
 */
function PopupComboBoxMenuItem() {
    this._init.apply(this, arguments);
}

PopupComboBoxMenuItem.prototype = {
    __proto__: PopupBaseMenuItem.prototype,

    _init: function (params) {
        PopupBaseMenuItem.prototype._init.call(this, params);

        /** The container in which we display the active item for the combo box.
         * @type {Shell.Stack} */
        this._itemBox = new Shell.Stack();
        this.addActor(this._itemBox);

        let expander = new St.Label({ text: '\u2304' });
        this.addActor(expander, { align: St.Align.END,
                                  span: -1 });

        /** The menu for the combo box.
         * @type {PopupComboMenu} */
        this._menu = new PopupComboMenu(this.actor);
        Main.uiGroup.add_actor(this._menu.actor);
        this._menu.actor.hide();

        if (params.style_class)
            this._menu.actor.add_style_class_name(params.style_class);

        this.actor.connect('scroll-event', Lang.bind(this, this._onScrollEvent));

        /** The index of the selected item of the combo box.
         * @type {number} */
        this._activeItemPos = -1;
        /** Array containing clones of all the items in the combo box.
         * @type {St.BoxLayout[]} */
        this._items = [];
    },

    /** Gets the top-level {@link PopupMenu} or {@link PopupComboMenu} that this
     * item belongs to (woah, combo box menu items within combo box menu items?!).
     *
     * We walk up {@link #actor}'s parent chain until we find a {@link PopupMenu}
     * or {@link PopupComboMenu} and return that.
     * @returns {?PopupMenu} the {@link PopupMenu} ancestor of this actor, or
     * `null` if none.
     */
    _getTopMenu: function() {
        let actor = this.actor.get_parent();
        while (actor) {
            if (actor._delegate &&
                (actor._delegate instanceof PopupMenu ||
                 actor._delegate instanceof PopupComboMenu))
                return actor._delegate;

            actor = actor.get_parent();
        }

        return null;
    },

    /** Callback when the users scrolls over this item.
     *
     * This scrolls the combo box's active item (selection) up or down to
     * the next visible item (if any).
     *
     * We call {@link #setActiveItem} to set this new item as active and emit
     * 'active-item-changed'.
     * @fires .active-item-changed
     * @see #setActiveItem
     */
    _onScrollEvent: function(actor, event) {
        if (this._activeItemPos == -1)
            return;

        let position = this._activeItemPos;
        let direction = event.get_scroll_direction();
        if (direction == Clutter.ScrollDirection.DOWN) {
            while (position < this._items.length - 1) {
                position++;
                if (this._menu.getItemVisible(position))
                    break;
            }
        } else if (direction == Clutter.ScrollDirection.UP) {
            while (position > 0) {
                position--;
                if (this._menu.getItemVisible(position))
                    break;
            }
        }

        if (position == this._activeItemPos)
            return;

        this.setActiveItem(position);
        this.emit('active-item-changed', position);
    },

    /** Overrides the parent method. This is called whenever the combo box
     * item is clicked.
     *
     * Unlike the parent method, we do *not* emit the 'activate' signal.
     *
     * Instead, we add our combo box menu {@link #_menu} as a child menu of
     * our parent menu ({@link #_getTopMenu}) and toggle it open.
     *
     * The reason we have to add it as a child menu is so that the parent
     * menu remains open while the combo box menu is open.
     * @see PopupMenuBase#addChildMenu
     * @override
     */
    activate: function(event) {
        let topMenu = this._getTopMenu();
        if (!topMenu)
            return;

        topMenu.addChildMenu(this._menu);
        this._menu.toggle();
    },

    /** Adds an item to the combo box at the specified position.
     *
     * NOTE that the only way to retrieve the currently-active item is by its
     * index within the menu ({@link #_activeItemPos} or with
     * [signal 'active-item-changed']{@link .active-item-changed}). Hence it may
     * be worth-while to remember which item is in which position (as opposed
     * to not providing `position` to add the item to the end) so that you
     * can identify the active item given its index.
     *
     * We add `menuItem` to our combo menu {@link #_menu}.
     *
     * Then we create a *clone* of `menuItem` and add *that* to {@link #_itemBox}.
     *
     * To understand why we do this, first suppose `menuItem` is the active item
     * (currently selected). Then it has to be visible in the main popup menu, so it
     * should be a child of this item. However, it should *also* be part of
     * the combo menu.
     *
     * Since an actor cannot be a child of two things at once, we create a clone
     * of it and add that clone to our popup menu item in the popup menu, while we
     * add the original item to the combo box menu.
     *
     * To prevent us from having to make a clone every time the selected
     * item changes, we instead make the clone once upon adding, and store it in
     * {@link #_items}.
     *
     * Then when the active item is changed, we just get the relevant clone from
     * {@link #_items} and place it in {@link #_itemBox} so it is visible
     * from the popup menu in which this item lives.
     *
     * @param {PopupBaseMenuItem} menuItem - the item to add to the combo box.
     * @param {number} [position] - position to add it. If not provided, it
     * will be appended to the end.
     */
    addMenuItem: function(menuItem, position) {
        if (position === undefined)
            position = this._menu.numMenuItems;

        this._menu.addMenuItem(menuItem, position);
        _ensureStyle(this._menu.actor);

        let item = new St.BoxLayout({ style_class: 'popup-combobox-item' });

        let children = menuItem.actor.get_children();
        for (let i = 0; i < children.length; i++) {
            let clone = new Clutter.Clone({ source: children[i] });
            item.add(clone, { y_fill: false });
        }

        let oldItem = this._items[position];
        if (oldItem)
            this._itemBox.remove_actor(oldItem);

        this._items[position] = item;
        this._itemBox.add_actor(item);

        menuItem.connect('activate',
                         Lang.bind(this, this._itemActivated, position));
    },

    /** This sets the active (selected) item of the combo box. The item at
     * index `position` is set active.
     *
     * This does **not** emit the 'active-item-changed' signal; that is triggered
     * when the user manually selects an item from the menu
     * (see {@link #_itemActivated}).
     *
     * There is no corresponding getter for this! However the combo box
     * menu item's ['active-item-changed']{@link PopupComboBoxMenuItemm.event:active-item-changed}
     * signal will indicate the index of the item made active.
     * @inheritparams PopupComboMenu#setActiveItem
     */
    setActiveItem: function(position) {
        let item = this._items[position];
        if (!item)
            return;
        if (this._activeItemPos == position)
            return;
        this._menu.setActiveItem(position);
        this._activeItemPos = position;
        for (let i = 0; i < this._items.length; i++)
            this._items[i].visible = (i == this._activeItemPos);
    },

    /** Sets whether the item in the combo box at index `position` is visible
     * or not.
     * @inheritparams PopupComboMenu#setItemVisible
     */
    setItemVisible: function(position, visible) {
        this._menu.setItemVisible(position, visible);
    },

    /** Callback when any item added to the combo box is selected.
     *
     * This sets that item as active using {@link #setActiveItem}, and
     * additionally emits 'active-item-changed' with the position of the
     * newly-activated item for listeners to respond to.
     * @param {PopupBaseMenuItem} menuItem - the item that was activated.
     * @param {Clutter.Event} event - information about the event.
     * @param {number} position - index `menuItem`.
     * @fires .active-item-changed
     */
    _itemActivated: function(menuItem, event, position) {
        this.setActiveItem(position);
        this.emit('active-item-changed', position);
    }
};
/** Emitted whenever the user changes the selection of the active item in a
 * combo box menu item.
 * @name active-item-changed
 * @param {PopupComboBoxMenuItem} item - the combo box item.
 * @param {number} position - the index of the item in the combo box menu that
 * was activated.
 * @event
 * @memberof PopupComboBoxMenuItem
 */

/** Creates a new PopupMenuManager
 *
 * We store `owner` in {@link #_owner} and initialise are variables
 * ({@link #_activeMenu}, {@link #_menus}, {@link #_menuStack}).
 * @param {?} owner - class with property `owner.actor`. `owner.actor` is the
 * actor that contains all the source actors for the managed menus. For example,
 * the top panel is the owner for the popup menu manager for all the status icons.
 * @classdesc
 * This is a basic implementation of a menu manager.
 *
 * Call {@link #addMenu} to add menus to it.
 *
 * It provides features like:
 *
 * * keyboard navigation between multiple menus added to the same manager.
 * * changing between added menus by opening a menu and then hovering over
 *   the source actor for another menu.
 *
 * It also handles essential features a popup menu needs like:
 *
 * * handling grabs (and clicking outside an open menu should close it),
 * * handling menu stacks (e.g. for comboboxes, which are menus within menus).
 *
 * I *think* that if you make a PopupMenu you should really ensure it gets
 * added to a manager somehow to make sure it works properly, but I'm not
 * entirely sure (if you make a {@link PanelMenu.Button} and add
 * it to the top panel this gets done for you).
 *
 *     mathematicalcoffee: Jasper, so if I make a PopupMenu I should really make sure it gets added to a manager to "work" properly?
 *     Jasper: mathematicalcoffee, most likely, yes
 *     Jasper: message tray right-click menus aren't added to a manager, btw.
 *     Jasper: But that's an exception.
 *
 * @class
 */
function PopupMenuManager(owner) {
    this._init(owner);
}

PopupMenuManager.prototype = {
    _init: function(owner) {
        /** The owner of all the popup menu source actors. It should have property
         * `owner.actor` to which the `menu.sourceActor`s belong. For example,
         * the top panel is the owner for the popup menu manager for all the
         * status icons.
         *
         * This is pushed modally when a menu in this group is opened, and popped
         * upon completion.
         * @type {?} */
        this._owner = owner;
        /** Whether we are currently in a grab.
         * @type {boolean} */
        this.grabbed = false;

        this._eventCaptureId = 0;
        this._enterEventId = 0;
        this._leaveEventId = 0;
        this._keyFocusNotifyId = 0;
        /** If one of the menus we manage is currently open, this is it.
         * @type {?PopupMenuBase} */
        this._activeMenu = null;
        /** This is the list of menus we manage. Each element is a
         * {@link MenuData} which stores the menu as well as connection IDs
         * for various signals we are listening to (see {@link #addMenu}).
         * @type {MenuData[]} */
        this._menus = [];
        /** This is the menu stack for the current grab: if we open the
         * child menu of a menu the chain of ancestor menus (that are currently
         * open) is stored here. This is used for example with
         * {@link PopupComboBoxMenuItem}s, because their menus are added as
         * child menus of the parent menu.
         * @type {PopupMenuBase[]} */
        this._menuStack = [];
        this._preGrabInputMode = null;
        this._grabbedFromKeynav = false;
    },

    /** Adds `menu` to the manager.
     *
     * We create a {@link MenuData} for the menu, storing signal connection
     * IDs for a number of signals:
     *
     * * `menu`'s ['open-state-changed']{@link PopupMenuBase.open-state-changed}
     * signal (when it opens or closes): connect to {@link #_onMenuOpenState}.
     * * `menu`'s ['child-menu-added']{@link PopupMenuBase.child-menu-added} signal
     * (when it has a child menu added to it): connects to {@link #_onChildMenuAdded}.
     * * `menu`'s ['child-menu-removed']{@link PopupMenuBase.child-menu-removed}
     * signal (when it has a child menu removed from it): connects to
     * {@link #_onChildMenuRemoved}.
     * * `menu`'s ['destroy']{@link PopupMenuBase.destroy} signal (when it is
     * destroyed): connect to {@link #_onMenuDestroy}.
     * * [`menu.sourceActor`]{@link PopupMenuBase#sourceActor}'s 'enter-event'
     * signal (when the mouse enters the source actor): call {@link #_onMenuSourceEnter}.
     * * [`menu.sourceActor`]{@link PopupMenuBase#sourceActor}'s 'key-focus-in'
     * signal (when keyboard focus is transferred to the source actor): call
     * {@link #_onMenuSourceEnter}.
     *
     * We store the object in {@link #_menus}.
     *
     * @param {PopupMenuBase} menu - menu.
     * @param {number} [position] - index of the menu. If not provided,
     * we add it to the end. As far as I can tell think the position of a menu
     * in {@link #_menus} has no significance.
     * @see #_onMenuOpenState
     * @see #_onChildMenuAdded
     * @see #_onChildMenuRemoved
     * @see #_onMenuDestroy
     * @see #_onMenuSourceEnter
     */
    addMenu: function(menu, position) {
        let menudata = {
            menu:              menu,
            openStateChangeId: menu.connect('open-state-changed', Lang.bind(this, this._onMenuOpenState)),
            childMenuAddedId:  menu.connect('child-menu-added', Lang.bind(this, this._onChildMenuAdded)),
            childMenuRemovedId: menu.connect('child-menu-removed', Lang.bind(this, this._onChildMenuRemoved)),
            destroyId:         menu.connect('destroy', Lang.bind(this, this._onMenuDestroy)),
            enterId:           0,
            focusInId:         0
        };

        let source = menu.sourceActor;
        if (source) {
            menudata.enterId = source.connect('enter-event', Lang.bind(this, function() { this._onMenuSourceEnter(menu); }));
            menudata.focusInId = source.connect('key-focus-in', Lang.bind(this, function() { this._onMenuSourceEnter(menu); }));
        }

        if (position == undefined)
            this._menus.push(menudata);
        else
            this._menus.splice(position, 0, menudata);
    },

    /** Removes `menu` from the manager.
     *
     * We close the menu if it's currently the active one.
     *
     * Then we look up its {@link MenuData} from {@link #_menus} and disconnect
     * all the signals that are stored there.
     *
     * Finally we remove the menu data from {@link #_menus}.
     * @inheritparams #addMenu
     */
    removeMenu: function(menu) {
        if (menu == this._activeMenu)
            this._closeMenu();

        let position = this._findMenu(menu);
        if (position == -1) // not a menu we manage
            return;

        let menudata = this._menus[position];
        menu.disconnect(menudata.openStateChangeId);
        menu.disconnect(menudata.childMenuAddedId);
        menu.disconnect(menudata.childMenuRemovedId);
        menu.disconnect(menudata.destroyId);

        if (menudata.enterId)
            menu.sourceActor.disconnect(menudata.enterId);
        if (menudata.focusInId)
            menu.sourceActor.disconnect(menudata.focusInId);

        this._menus.splice(position, 1);
    },

    /** Starts a grab. Called from (for example) {@link #_onMenuOpenState}
     * when one of our menus is opened.
     *
     * We push [`this._owner.actor`]{@link #_owner} modally (@link Main.pushModal).
     *
     * Then we listen to a number of events on the global stage while the
     * grab is active:
     *
     * * 'captured-event', 'enter-event', 'leave-event': {@link #_onEventCapture}
     * * 'notify::key-focus': {@link #_onKeyFocusChanged}.
     *
     * @see #_grab
     */
    _grab: function() {
        Main.pushModal(this._owner.actor);

        this._eventCaptureId = global.stage.connect('captured-event', Lang.bind(this, this._onEventCapture));
        // captured-event doesn't see enter/leave events
        this._enterEventId = global.stage.connect('enter-event', Lang.bind(this, this._onEventCapture));
        this._leaveEventId = global.stage.connect('leave-event', Lang.bind(this, this._onEventCapture));
        this._keyFocusNotifyId = global.stage.connect('notify::key-focus', Lang.bind(this, this._onKeyFocusChanged));

        this.grabbed = true;
    },

    /** Ends a grab started with {@link #_grab}.
     *
     * We disconnect the signals on the global stage that we were listening to,
     * set {@link #grabbed} to false, and pop [`this._owner.actor`]{@link #_owner}
     * from the modal stack ({@link Main.popModal}).
     * @see #_grab
     */
    _ungrab: function() {
        global.stage.disconnect(this._eventCaptureId);
        this._eventCaptureId = 0;
        global.stage.disconnect(this._enterEventId);
        this._enterEventId = 0;
        global.stage.disconnect(this._leaveEventId);
        this._leaveEventId = 0;
        global.stage.disconnect(this._keyFocusNotifyId);
        this._keyFocusNotifyId = 0;

        this.grabbed = false;
        Main.popModal(this._owner.actor);
    },

    /** Callback when a child menu (or any descendant menu of the menus we
     * manage) is opened.
     *
     * Essentially if a menu is opened we start a grab, meaning that if the
     * cursor moves over the source actor of any other menu we are managing
     * while this menu is open, we switch to displaying that menu instead.
     *
     * Instead, if a menu that we were managing is closed, we drop the grab
     * and restore keyboard focus to whatever used to have focus.
     *
     * More details:
     *
     * If the menu was opened and is a child menu of our currently-active
     * menu {@link #_activeMenu}, we grab key focus to it.
     * We also add it to our stack {@link #_menuStack} (which contains the
     * ancestors of `menu` that are still open). We reset {@link #_activeMenu}
     * to `menu`.
     *
     * Otherwise (the menu was closed or is not a child menu of the current
     * active menu), we use {@link #_menuStack} to see if there's a parent menu
     * of `menu` that is open. If so, we grab key focus to it, set {@link #_activeMenu}
     * to it, and pop it from the stack.
     *
     * If the menu was opened, we grab input ({@link #_grab}) and grab key
     * focus to `menu` if it (or its ancestor) didn't have it already.
     *
     * If the menu was closed, we ungrab focus from the menus and restore
     * it to whatever it was before we grabbed it.
     * @listens PopupMenuBase.event:open-state-changed
     * @inheritdoc PopupMenuBase.event:open-state-changed
     * @see #_grab
     * @see #_ungrab
     */
    _onMenuOpenState: function(menu, open) {
        if (open) {
            if (this._activeMenu && this._activeMenu.isChildMenu(menu)) {
                this._menuStack.push(this._activeMenu);
                menu.actor.grab_key_focus();
            }
            this._activeMenu = menu;
        } else {
            if (this._menuStack.length > 0) {
                this._activeMenu = this._menuStack.pop();
                if (menu.sourceActor)
                    menu.sourceActor.grab_key_focus();
                this._didPop = true;
            }
        }

        // Check what the focus was before calling pushModal/popModal
        let focus = global.stage.key_focus;
        let hadFocus = focus && this._activeMenuContains(focus);

        if (open) {
            if (!this.grabbed) {
                this._preGrabInputMode = global.stage_input_mode;
                this._grabbedFromKeynav = hadFocus;
                this._grab();
            }

            if (hadFocus)
                focus.grab_key_focus();
            else
                menu.actor.grab_key_focus();
        } else if (menu == this._activeMenu) {
            if (this.grabbed)
                this._ungrab();
            this._activeMenu = null;

            if (this._grabbedFromKeynav) {
                if (this._preGrabInputMode == Shell.StageInputMode.FOCUSED)
                    global.stage_input_mode = Shell.StageInputMode.FOCUSED;
                if (hadFocus && menu.sourceActor)
                    menu.sourceActor.grab_key_focus();
                else if (focus)
                    focus.grab_key_focus();
            }
        }
    },

    /** Callback when any of the menus we are managing has a child menu added
     * to it.
     *
     * We add that menu to ourselves too ({@link #addMenu}).
     * @listens PopupMenuBase.event:child-menu-added
     * @inheritdoc PopupMenuBase.event:child-menu-added
     */
    _onChildMenuAdded: function(menu, childMenu) {
        this.addMenu(childMenu);
    },

    /** Callback when any of the menus we are managing had a child menu removed
     * from it.
     *
     * We remove that menu from ourselves too ({@link #removeMenu}).
     * @listens PopupMenuBase.event:child-menu-removed
     * @inheritdoc PopupMenuBase.event:child-menu-removed
     */
    _onChildMenuRemoved: function(menu, childMenu) {
        this.removeMenu(childMenu);
    },

    /** Changes the currently-open menu without dropping our grab.
     *
     * Open one of the status button menus in the top panel. Then, without closing it,
     * move your cursor over a different status icon in the top panel. The
     * first menu will close and the menu of the icon you're currently hovering
     * over will open.
     *
     * That switchover happens with {@link #_changeMenu}. We have to be careful
     * not to drop our popup menu grab.
     *
     * If there is no menu currently open (i.e. {@link #_activeMenu} is `null`),
     * we simply open `newMenu` with animation.
     *
     * Otherwise:
     *
     * We go through our menu stack {@link #_menuStack} and close all the menus
     * as we go (no animations).
     *
     * Finally we close whatever menu was previously open, and open `newMenu`,
     * without animation.
     *
     * @param {PopupMenuBase} newMenu - the new menu to open.
     */
    _changeMenu: function(newMenu) {
        if (this._activeMenu) {
            // _onOpenMenuState will drop the grab if it sees
            // this._activeMenu being closed; so clear _activeMenu
            // before closing it to keep that from happening
            let oldMenu = this._activeMenu;
            this._activeMenu = null;
            for (let i = this._menuStack.length - 1; i >= 0; i--)
                this._menuStack[i].close(false);
            oldMenu.close(false);
            newMenu.open(false);
        } else
            newMenu.open(true);
    },

    /** Called whenever [`menu.sourceActor`]{@link PopupMenuBase#sourceActor}
     * is entered (with the mouse or with keyboard focus) for any of the menus
     * we are managing.
     *
     * If we are currently in a grab (one of our menus is open) and the user
     * has moved their mouse over the source actor for a different menu than
     * the one that is open, we switch to displaying that menu instead
     * ({@link #_changeMenu}).
     *
     * @inheritparams #addMenu
     * time for this)
     */
    _onMenuSourceEnter: function(menu) {
        if (!this.grabbed || menu == this._activeMenu)
            return false;

        if (this._activeMenu && this._activeMenu.isChildMenu(menu))
            return false;

        if (this._menuStack.indexOf(menu) != -1)
            return false;

        if (this._menuStack.length > 0 && this._menuStack[0].isChildMenu(menu))
            return false;

        this._changeMenu(menu);
        return false;
    },

    /** Callback when keyboard focus changes while we are in a grab.
     *
     * If focus was changed to some item within our active menu, or to
     * an item that has a menu that we manage, or if the current menu
     * stack {@link #_menuStack} has menus in it (we are in a child menu),
     * we do nothing (various signals on those menus will handle the
     * transfer of focus and changing of the open menu if necessary).
     *
     * Otherwise, we close the active menu ({@link #_closeMenu}).
     */
    _onKeyFocusChanged: function() {
        if (!this.grabbed || !this._activeMenu)
            return;

        let focus = global.stage.key_focus;
        if (focus) {
            if (this._activeMenuContains(focus))
                return;
            if (this._menuStack.length > 0)
                return;
            if (focus._delegate && focus._delegate.menu &&
                this._findMenu(focus._delegate.menu) != -1)
                return;
        }

        this._closeMenu();
    },

    /** Callback when one of the menus we are managing is destroyed.
     * We remove the menu.
     * @listens PopupMenuBase.event:destroy
     * @inheritdoc PopupMenuBase.event:destroy
     */
    _onMenuDestroy: function(menu) {
        this.removeMenu(menu);
    },

    /** Asks whether the currently-open menu ({@link #_activeMenu}) or its
     * source actor contains `actor` as a direct child.
     * @param {Clutter.Actor} actor - actor to query
     * @returns {boolean} whether the currently-open menu or its source actor
     * contain `actor` as a direct child.
     */
    _activeMenuContains: function(actor) {
        return this._activeMenu != null
                && (this._activeMenu.actor.contains(actor) ||
                    (this._activeMenu.sourceActor && this._activeMenu.sourceActor.contains(actor)));
    },

    /** Returns whether the specified event was for either an actor in the
     * currently-active menu {@link #_activeMenu} or its source actor.
     * @param {Clutter.Event} event - an event.
     * @returns {boolean} whether `event.get_source()` is in the currently-active
     * menu or its actor.
     */
    _eventIsOnActiveMenu: function(event) {
        return this._activeMenuContains(event.get_source());
    },

    /** Returns whether `event` should be blocked or not.
     *
     * An event should be blocked if:
     *
     * * it did not occur on any of our menus or their source actors, OR
     * * it occured on one of our menus or its source actor, but that menu
     * is either active or has {@link PopupMenuBase#blockSourceEvents} set to
     * `true`.
     *
     * @inheritparams #_eventIsOnActiveMenu
     * @returns {boolean} whether the event should be blocked (it is blocked
     * if it didn't occur on any of our menus or their source actors, or it
     * occured on a menu but that menu wasn't active and has been set to
     * ignore such events).
     * @see PopupMenuBase#blockSourceEvents
     */
    _shouldBlockEvent: function(event) {
        let src = event.get_source();

        if (this._activeMenu != null && this._activeMenu.actor.contains(src))
            return false;

        for (let i = 0; i < this._menus.length; i++) {
            let menu = this._menus[i].menu;
            if (menu.sourceActor && !menu.blockSourceEvents && menu.sourceActor.contains(src)) {
                return false;
            }
        }

        return true;
    },

    /** Finds the index of the {@link MenuData} for a particular menu
     * in {@link #_menus}.
     * @param {PopupMenuBase} item - the menu to look up.
     * @returns {number} the index `i` for the {@link MenuData} for `item` in
     * {@link #_menus}, i.e. `i` such that `this._menus[i].menu` is `item`. -1
     * if not found.
     */
    _findMenu: function(item) {
        for (let i = 0; i < this._menus.length; i++) {
            let menudata = this._menus[i];
            if (item == menudata.menu)
                return i;
        }
        return -1;
    },

    /** Callback for captured events (as well as enter- and leave- events) captured
     * on the global stage **whilst a grab is in progress**.
     *
     * If the user clicked outside the currently-open menu, we close the
     * active menu, ending the grab ({@link #_closeMenu}) and handling the event.
     *
     * Otherwise we block this event if it didn't occur on any of our menus
     * (or it occured on one of our menus but that has 
     * {@link PopupMenuBase#blockSourceEvents} set to `true`, see
     * {@link #_shouldBlockEvent}). This is to prevent any other Shell elements
     * from reacting to events while we have our grab.
     */
    _onEventCapture: function(actor, event) {
        if (!this.grabbed)
            return false;

        if (this._owner.menuEventFilter &&
            this._owner.menuEventFilter(event))
            return true;

        if (this._activeMenu != null && this._activeMenu.passEvents)
            return false;

        if (this._didPop) {
            this._didPop = false;
            return true;
        }

        let activeMenuContains = this._eventIsOnActiveMenu(event);
        let eventType = event.type();

        if (eventType == Clutter.EventType.BUTTON_RELEASE) {
            if (activeMenuContains) {
                return false;
            } else {
                this._closeMenu();
                return true;
            }
        } else if (eventType == Clutter.EventType.BUTTON_PRESS && !activeMenuContains) {
            this._closeMenu();
            return true;
        } else if (!this._shouldBlockEvent(event)) {
            return false;
        }

        return true;
    },

    /** Closes the active menu {@link #_activeMenu}. */
    _closeMenu: function() {
        if (this._activeMenu != null)
            this._activeMenu.close(true);
    }
};
/** Information about a menu, used by the {@link PopupMenuManager}.
 * @typedef MenuData
 * @type {Object}
 * @property {PopupMenuBase} menu - the menu.
 *
 * @property {number} openStateChangedId - signal ID for menu's 
 * ['open-state-changed']{@link PopupMenuBase.open-state-changed} signal
 * (when it opens or closes).
 * Connects to {@link #_onMenuOpenState}.
 *
 * @property {number} childMenuAddedId - signal ID for menu's
 * ['child-menu-added']{@link PopupMenuBase.child-menu-added} signal
 * (when it has a child menu added to it).
 * Connects to {@link #_onChildMenuAdded}.
 *
 * @property {number} childMenuRemovedId - signal ID for menu's
 * ['child-menu-removed']{@link PopupMenuBase.child-menu-removed} signal
 * (when it has a child menu removed from it).
 * Connects to {@link #_onChildMenuRemoved}.
 *
 * @property {number} destroyId - signal ID for menu's
 * ['destroy']{@link PopupMenuBase.destroy} signal (when it is destroyed).
 * Connects to {@link #_onMenuDestroy}.
 *
 * @property {number} enterId - signal ID for menu's source actor's 'enter-event'
 * signal (when the mouse enters the source actor). Calls {@link #_onMenuSourceEnter}.
 *
 * @property {number} focusInId - signal ID for menu's source actor's 'key-focus-in'
 * signal (when key focus is transferred to the menu's source actor).
 * Calls {@link #_onMenuSourceEnter}.
 */
