// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
// TODO: this namespace should be Status.Keyboard perhaps.
/**
 * @fileOverview
 * Defines the keyboard layout indicator (letting you switch between layouts,
 * only appears if you have chosen multiple).
 * 
 * ![Keyboard layout status indicator](pics/status.keyboard.png)
 *
 * Each menu item in the menu shows the long keyboard layout name (e.g.
 * "English (US)" along with some sort of keyboard layout *indicator*; this
 * can be either the layout's short name ("en"), *or* a flag for the country.
 *
 * You will only get a flag *if* you have gnome keyboard's 'showFlags' setting
 * set to TRUE. In GNOME 3.2 and 3.4, this can be adjusted with dconf setting
 * 'org.gnome.libgnomekbd.indicator.show-flags'. 
 *
 * If you're lucky supposedly Ubuntu has the flag images in
 * `/usr/share/pixmaps/flags` and then the flags will show. 
 * Otherwise you have to find pictures of them and put them in
 * `$HOME/.icons/flags` named with the country's 
 * [two-letter ISO 3166-1 combination](http://www.iso.org/iso/support/country_codes/iso_3166_code_lists/iso-3166-1_decoding_table.htm). 
 *
 * I think some packages may provide flags, but you can also get them from
 * [gnome-look.org](http://gnome-look.org/content/search.php?search&text=language%20flags).
 */

const Clutter = imports.gi.Clutter;
const GdkPixbuf = imports.gi.GdkPixbuf;
const Gkbd = imports.gi.Gkbd;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const Util = imports.misc.util;

/** creates a new LayoutMenuItem
 * @param {Gkbd.Configuration} config - configuration for Gkeyboard. (see
 * [Gkbd.Configuration](http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gkbd.Configuration.html))
 * @param {number} id - index of the keyboard layout.
 * @param {St.Icon|St.Label} indicator - the indicator for this keyboard layout.
 * It's either a `St.Label` with the keyboard layout's short name (like "en")
 * or a `St.Icon` with the flag for that country's flag if the 'showFlags'
 * setting is true. See the note in {@link namespace:Keyboard} on how to set
 * this.
 * @param {string} long_name - full name of the keyboard (e.g. "English (US)").
 * @classdesc
 * ![two LayoutMenuItems, with showFlags `false` (I don't have flag icons)](pics/status.keyboard.LayoutMenuItem.png)
 *
 * This is a {@link PopupMenu.PopupBaseMenuItem} containing both a label
 * with the full keyboard layout name (e.g. "English (US)"), along with
 * an indicator for the layout - either text ("en") or a flag. See the note
 * in {@link namespace:Keyboard} on how to set this.
 * @extends PopupMenu.PopupBaseMenuItem
 * @class
 */
function LayoutMenuItem() {
    this._init.apply(this, arguments);
}

LayoutMenuItem.prototype = {
    __proto__: PopupMenu.PopupBaseMenuItem.prototype,

    _init: function(config, id, indicator, long_name) {
        PopupMenu.PopupBaseMenuItem.prototype._init.call(this);

        this._config = config;
        this._id = id;
        this.label = new St.Label({ text: long_name });
        this.indicator = indicator;
        this.addActor(this.label);
        this.addActor(this.indicator);
    },

    /** overrides parent function - when the user clicks on one of these items.
     * Selects this keyboard layout.
     * @override
     */
    activate: function(event) {
        PopupMenu.PopupBaseMenuItem.prototype.activate.call(this);
        this._config.lock_group(this._id);
    }
};

/** creates a new XKBIndicator
 *
 * This creates what will be displayed for the indicator on the panel menu,
 * either the current keyboard layout's code ('en') or a country flag.
 *
 * It also populates its menu with items to show the keyboard layout and
 * launch the gnome control centre keyboard settings.
 * @classdesc
 * ![Keyboard layout status indicator, XKBIndicator](pics/status.keyboard.png)
 *
 * The {@link PanelMenu.Button} defining the keyboard indicator. 
 * Note it is not a `SystemStatusButton` since its 'icon' is not a stock one,
 * but either text (current keyboard layout code like 'en') or icon (flag for
 * the country's keyboard layout). See note in {@link namespace:Keyboard} for
 * how to determine which gets shown.
 *
 * The menu lets you switch between keyboard layouts, though the status button
 * only shows if you set more than one layout in the GNOME control centre -&gt;
 * 'Keyboard' -&gt; 'Layout Settings').
 *
 * Internally a [Gkbd.Configuration](http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gkbd.Configuration.html)
 * is used to get/set all the keyboard settings.
 * @class
 * @extends PanelMenu.Button
 */
function XKBIndicator() {
    this._init.call(this);
}

XKBIndicator.prototype = {
    __proto__: PanelMenu.Button.prototype,

    _init: function() {
        PanelMenu.Button.prototype._init.call(this, St.Align.START);

        this._container = new Shell.GenericContainer();
        this._container.connect('get-preferred-width', Lang.bind(this, this._containerGetPreferredWidth));
        this._container.connect('get-preferred-height', Lang.bind(this, this._containerGetPreferredHeight));
        this._container.connect('allocate', Lang.bind(this, this._containerAllocate));
        this.actor.add_actor(this._container);
        this.actor.add_style_class_name('panel-status-button');

        this._iconActor = new St.Icon({ icon_name: 'keyboard', icon_type: St.IconType.SYMBOLIC, style_class: 'system-status-icon' });
        this._container.add_actor(this._iconActor);
        this._labelActors = [ ];
        this._layoutItems = [ ];

        this._showFlags = false;
        /** The keyboard settings instance this class wraps around.
         * @type {Gkbd.Configuration} */
        this._config = Gkbd.Configuration.get();
        this._config.connect('changed', Lang.bind(this, this._syncConfig));
        this._config.connect('group-changed', Lang.bind(this, this._syncGroup));
        this._config.start_listen();

        this._syncConfig();

        if (global.session_type == Shell.SessionType.USER) {
            this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
            this.menu.addAction(_("Show Keyboard Layout"), Lang.bind(this, function() {
                Main.overview.hide();
                Util.spawn(['gkbd-keyboard-display', '-g', String(this._config.get_current_group() + 1)]);
            }));
        }
        this.menu.addSettingsAction(_("Region and Language Settings"), 'gnome-region-panel.desktop');
    },

    /** Adjusts keyboard layout short names so that they're all unique.
     * For example if you have keyboard layouts "English (US)" and
     * "English (classic Dvorak)", these both have name 'en'. This function
     * adjusts them to be 'en1' and 'en2'.
     * @param {string[]} names - input language keyboard layout short names.
     * @returns {string[]} array of input `names` made unique by appending
     * numbers to duplicate ones.
     */
    _adjustGroupNames: function(names) {
        // Disambiguate duplicate names with a subscript
        // This is O(N^2) to avoid sorting names
        // but N <= 4 so who cares?

        for (let i = 0; i < names.length; i++) {
            let name = names[i];
            let cnt = 0;
            for (let j = i + 1; j < names.length; j++) {
                if (names[j] == name) {
                    cnt++;
                    // U+2081 SUBSCRIPT ONE
                    names[j] = name + String.fromCharCode(0x2081 + cnt);
                }
            }
            if (cnt != 0)
                names[i] = name + '\u2081';
        }

        return names;
    },

    /** Makes sure the menu items are in sync with the keyboard configuration.
     *
     * Firstly, checks whether it should paint an icon (flag) or text ('en')
     * in the button on the panel.
     *
     * Next, creates a {@link LayoutMenuItem} for each keyboard format.
     *
     * Finally, calls {@link #_syncGroup} to ensure that we remain
     * in sync with the currently-selected format.
     */
    _syncConfig: function() {
        this._showFlags = this._config.if_flags_shown();
        if (this._showFlags) {
            this._container.set_skip_paint(this._iconActor, false);
        } else {
            this._container.set_skip_paint(this._iconActor, true);
        }

        let groups = this._config.get_group_names();
        if (groups.length > 1) {
            this.actor.show();
        } else {
            this.menu.close();
            this.actor.hide();
        }

        for (let i = 0; i < this._layoutItems.length; i++)
            this._layoutItems[i].destroy();

        for (let i = 0; i < this._labelActors.length; i++)
            this._labelActors[i].destroy();

        let short_names = this._adjustGroupNames(this._config.get_short_group_names());

        this._selectedLayout = null;
        this._layoutItems = [ ];
        this._selectedLabel = null;
        this._labelActors = [ ];
        for (let i = 0; i < groups.length; i++) {
            let icon_name = this._config.get_group_name(i);
            let actor;
            if (this._showFlags)
                actor = new St.Icon({ icon_name: icon_name, icon_type: St.IconType.SYMBOLIC, style_class: 'popup-menu-icon' });
            else
                actor = new St.Label({ text: short_names[i] });
            let item = new LayoutMenuItem(this._config, i, actor, groups[i]);
            item._short_group_name = short_names[i];
            item._icon_name = icon_name;
            this._layoutItems.push(item);
            this.menu.addMenuItem(item, i);

            let shortLabel = new St.Label({ text: short_names[i] });
            this._labelActors.push(shortLabel);
            this._container.add_actor(shortLabel);
            this._container.set_skip_paint(shortLabel, true);
        }

        this._syncGroup();
    },

    /** Ensures that the current keyboard layout appears so in the popup menu:
     *
     * * shows a dot next to the {@link LayoutMenuItem} of the current keyboard
     * layout
     * * updates the button in the panel to reflect the current flag/layout code.
     */
    _syncGroup: function() {
        let selected = this._config.get_current_group();

        if (this._selectedLayout) {
            this._selectedLayout.setShowDot(false);
            this._selectedLayout = null;
        }

        if (this._selectedLabel) {
            this._container.set_skip_paint(this._selectedLabel, true);
            this._selectedLabel = null;
        }

        let item = this._layoutItems[selected];
        item.setShowDot(true);

        this._iconActor.icon_name = item._icon_name;
        this._selectedLabel = this._labelActors[selected];
        this._container.set_skip_paint(this._selectedLabel, this._showFlags);

        this._selectedLayout = item;
    },

    /** callback for 'get-preferred-width' of the visual element in the button
     * in the top panel (i.e. the label or flag).
     *
     * Returns the maximum width of all the label/flags (whichever relevant) for
     * the layouts listed in the menu.
     */
    _containerGetPreferredWidth: function(container, for_height, alloc) {
        // Here, and in _containerGetPreferredHeight, we need to query
        // for the height of all children, but we ignore the results
        // for those we don't actually display.
        let max_min_width = 0, max_natural_width = 0;
        if (this._showFlags)
            [max_min_width, max_natural_width] = this._iconActor.get_preferred_width(for_height);

        for (let i = 0; i < this._labelActors.length; i++) {
            let [min_width, natural_width] = this._labelActors[i].get_preferred_width(for_height);
            if (!this._showFlags) {
                max_min_width = Math.max(max_min_width, min_width);
                max_natural_width = Math.max(max_natural_width, natural_width);
            }
        }

        alloc.min_size = max_min_width;
        alloc.natural_size = max_natural_width;
    },

    /** callback for 'get-preferred-height' of the visual element in the button
     * in the top panel (i.e. the label or flag).
     *
     * Returns the maximum height of all the label/flags (whichever relevant) for
     * the layouts listed in the menu.
     */
    _containerGetPreferredHeight: function(container, for_width, alloc) {
        let max_min_height = 0, max_natural_height = 0;
        if (this._showFlags)
            [max_min_height, max_natural_height] = this._iconActor.get_preferred_height(for_width);
        
        for (let i = 0; i < this._labelActors.length; i++) {
            let [min_height, natural_height] = this._labelActors[i].get_preferred_height(for_width);
            if (!this._showFlags) {
                max_min_height = Math.max(max_min_height, min_height);
                max_natural_height = Math.max(max_natural_height, natural_height);
            }
        }

        alloc.min_size = max_min_height;
        alloc.natural_size = max_natural_height;
    },

    /** callback for 'allocate' of the visual element in the button
     * in the top panel (i.e. the label or flag).
     *
     * Simply ensures that the label/flag (whichever relevant) of the current
     * keyboard layout gets allocated to the button in the top panel, and
     * the rest are hidden.
     */
    _containerAllocate: function(container, box, flags) {
        // translate box to (0, 0)
        box.x2 -= box.x1;
        box.x1 = 0;
        box.y2 -= box.y1;
        box.y1 = 0;

        this._iconActor.allocate_align_fill(box, 0.5, 0, false, false, flags);
        for (let i = 0; i < this._labelActors.length; i++)
            this._labelActors[i].allocate_align_fill(box, 0.5, 0, false, false, flags);
    }
};
