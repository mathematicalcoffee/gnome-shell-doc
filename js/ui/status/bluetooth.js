// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the bluetooth indicator, along with implementing a message tray
 * source and a few notifications for displaying bluetooth requests.
 *
 * ![Bluetooth.Indicator](pics/status.bluetooth.png)
 */

const Clutter = imports.gi.Clutter;
const Gdk = imports.gi.Gdk;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const GnomeBluetoothApplet = imports.gi.GnomeBluetoothApplet;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const St = imports.gi.St;
const Shell = imports.gi.Shell;

const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

/** Status of the bluetooth connection.
 * @enum
 * @const
 */
const ConnectionState = {
    /** We are disconnected from the device. */
    DISCONNECTED: 0,
    /** We are connected to the device. */
    CONNECTED: 1,
    /** We are disconnecting from the device. */
    DISCONNECTING: 2,
    /** We are connecting to the device. */
    CONNECTING: 3
}

/** creates a new (bluetooth status) Indicator.
 *
 * The initial icon is 'bluetooth-disabled'.
 *
 * This kills the 'bluetooth-applet' process and creates a new
 * GnomeBluetoothApplet.Applet to replace it ({@link #_applet}).
 *
 * It also populates its menu with a "Bluetooth" on/off toggle (also ensuring this
 * is kept up-to-date with the bluetooth status), and similarly for a
 * "Visibility" toggle.
 *
 * The "Send Files to Device", "Set up a New Device", and "Bluetooth Settings"
 * items are also added (spawning commands `bluetooth-sendto`, `bluetooth-wizard`
 * and the 'bluetooth-properties.desktop' widget respectively).
 *
 * We also listen to signals from {@link #_applet} in order to
 * create popup menu items for each device, and listen to authorization, pin
 * or confirmation requests (which are relayed through as {@link AuthNotification},
 * {@link PinNotification} and {@link ConfirmNotification} notifications from
 * a {@link Source}).
 *
 * @classdesc
 * ![Bluetooth.Indicator](pics/status.bluetooth.png)
 *
 * This class defines the bluetooth status indicator.
 * 
 * It wraps around the [Gnome Bluetooth Applet](http://library.gnome.org/users/gnome-bluetooth/stable/gnome-bluetooth-applet.html.en) and relays through relevant signals (like pairing requests) to
 * display notifications for them, as well as maintaining a list of bluetooth
 * devices that have been detected.
 *
 * @class
 * @extends PanelMenu.SystemStatusButton
 */
function Indicator() {
    this._init.apply(this, arguments);
}

Indicator.prototype = {
    __proto__: PanelMenu.SystemStatusButton.prototype,

    _init: function() {
        PanelMenu.SystemStatusButton.prototype._init.call(this, 'bluetooth-disabled', null);

        GLib.spawn_command_line_sync ('pkill -f "^bluetooth-applet$"');
        /** This indicator wraps around a
         * [Gnome Bluetooth Applet](http://library.gnome.org/users/gnome-bluetooth/stable/gnome-bluetooth-applet.html.en) and basically listens to all its signals.
         * @type {GnomeBluetoothApplet.Applet} */
        this._applet = new GnomeBluetoothApplet.Applet();

        this._killswitch = new PopupMenu.PopupSwitchMenuItem(_("Bluetooth"), false);
        this._applet.connect('notify::killswitch-state', Lang.bind(this, this._updateKillswitch));
        this._killswitch.connect('toggled', Lang.bind(this, function() {
            let current_state = this._applet.killswitch_state;
            if (current_state != GnomeBluetoothApplet.KillswitchState.HARD_BLOCKED &&
                current_state != GnomeBluetoothApplet.KillswitchState.NO_ADAPTER) {
                this._applet.killswitch_state = this._killswitch.state ?
                    GnomeBluetoothApplet.KillswitchState.UNBLOCKED:
                    GnomeBluetoothApplet.KillswitchState.SOFT_BLOCKED;
            } else
                this._killswitch.setToggleState(false);
        }));

        this._discoverable = new PopupMenu.PopupSwitchMenuItem(_("Visibility"), this._applet.discoverable);
        this._applet.connect('notify::discoverable', Lang.bind(this, function() {
            this._discoverable.setToggleState(this._applet.discoverable);
        }));
        this._discoverable.connect('toggled', Lang.bind(this, function() {
            this._applet.discoverable = this._discoverable.state;
        }));

        this._updateKillswitch();
        this.menu.addMenuItem(this._killswitch);
        this.menu.addMenuItem(this._discoverable);
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this._fullMenuItems = [new PopupMenu.PopupSeparatorMenuItem(),
                               new PopupMenu.PopupMenuItem(_("Send Files to Device...")),
                               new PopupMenu.PopupMenuItem(_("Set up a New Device...")),
                               new PopupMenu.PopupSeparatorMenuItem()];
        this._hasDevices = false;

        this._fullMenuItems[1].connect('activate', function() {
            GLib.spawn_command_line_async('bluetooth-sendto');
        });
        this._fullMenuItems[2].connect('activate', function() {
            GLib.spawn_command_line_async('bluetooth-wizard');
        });

        for (let i = 0; i < this._fullMenuItems.length; i++) {
            let item = this._fullMenuItems[i];
            this.menu.addMenuItem(item);
        }

        this._deviceItemPosition = 3;
        /** List of bluetooth devices
         * TODO
         * @type {array} */
        this._deviceItems = [];
        this._applet.connect('devices-changed', Lang.bind(this, this._updateDevices));
        this._updateDevices();

        this._applet.connect('notify::show-full-menu', Lang.bind(this, this._updateFullMenu));
        this._updateFullMenu();

        this.menu.addSettingsAction(_("Bluetooth Settings"), 'bluetooth-properties.desktop');

        this._applet.connect('pincode-request', Lang.bind(this, this._pinRequest));
        this._applet.connect('confirm-request', Lang.bind(this, this._confirmRequest));
        this._applet.connect('auth-request', Lang.bind(this, this._authRequest));
        this._applet.connect('cancel-request', Lang.bind(this, this._cancelRequest));
    },

    /** Updates the Bluetooth on/off switch so that it reflects the bluetooth
     * state, and sets the panel menu's icon to 'bluetooth-active' or 'bluetooth-disabled'
     * accordingly.
     * @see {@link #setIcon} */
    _updateKillswitch: function() {
        let current_state = this._applet.killswitch_state;
        let on = current_state == GnomeBluetoothApplet.KillswitchState.UNBLOCKED;
        let has_adapter = current_state != GnomeBluetoothApplet.KillswitchState.NO_ADAPTER;
        let can_toggle = current_state != GnomeBluetoothApplet.KillswitchState.NO_ADAPTER &&
                         current_state != GnomeBluetoothApplet.KillswitchState.HARD_BLOCKED;

        this._killswitch.setToggleState(on);
        if (can_toggle)
            this._killswitch.setStatus(null);
        else
            /* TRANSLATORS: this means that bluetooth was disabled by hardware rfkill */
            this._killswitch.setStatus(_("hardware disabled"));

        if (has_adapter)
            this.actor.show();
        else
            this.actor.hide();

        if (on) {
            this._discoverable.actor.show();
            this.setIcon('bluetooth-active');
        } else {
            this._discoverable.actor.hide();
            this.setIcon('bluetooth-disabled');
        }
    },

    /** Updates the list of devices, making sure each has a popup menu item
     * in the menu.
     * @see #_updateDeviceItem
     * @see #_createDeviceItem
     */
    _updateDevices: function() {
        let devices = this._applet.get_devices();

        let newlist = [ ];
        for (let i = 0; i < this._deviceItems.length; i++) {
            let item = this._deviceItems[i];
            let destroy = true;
            for (let j = 0; j < devices.length; j++) {
                if (item._device.device_path == devices[j].device_path) {
                    this._updateDeviceItem(item, devices[j]);
                    destroy = false;
                    break;
                }
            }
            if (destroy)
                item.destroy();
            else
                newlist.push(item);
        }

        this._deviceItems = newlist;
        this._hasDevices = newlist.length > 0;
        for (let i = 0; i < devices.length; i++) {
            let d = devices[i];
            if (d._item)
                continue;
            let item = this._createDeviceItem(d);
            if (item) {
                this.menu.addMenuItem(item, this._deviceItemPosition + this._deviceItems.length);
                this._deviceItems.push(item);
                this._hasDevices = true;
            }
        }
    },

    /** Updates the popup submenu item for a particular device.
     *
     * This makes sure that the device name label is up to date, refreshes
     * the submenu of options available for the device, and updates the
     * device's on/off toggle to reflect the current connection state (if it
     * can connect over bluetooth).
     * @param {PopupMenu.PopupSubMenuMenuItem} item - item to update (it's the
     * submenu item that's the heading for that device in the popup menu).
     * @param {?} device - Underlying device for those popup menu items.
     * @see #_buildDeviceSubMenu
     */
    _updateDeviceItem: function(item, device) {
        if (!device.can_connect && device.capabilities == GnomeBluetoothApplet.Capabilities.NONE) {
            item.destroy();
            return;
        }

        let prevDevice = item._device;
        let prevCapabilities = prevDevice.capabilities;
        let prevCanConnect = prevDevice.can_connect;

        // adopt the new device object
        item._device = device;
        device._item = item;

        // update properties
        item.label.text = device.alias;

        if (prevCapabilities != device.capabilities ||
            prevCanConnect != device.can_connect) {
            // need to rebuild the submenu
            item.menu.removeAll();
            this._buildDeviceSubMenu(item, device);
        }

        // update connected property
        if (device.can_connect)
            item._connectedMenuitem.setToggleState(device.connected);
    },

    /** Creates a submenu for a particular device.
     * @inheritparams #_updateDeviceItem
     * @returns {PopupMenu.PopupSubMenuMenuItem} - a submenu with the device's
     * alias, and menu items being options for interacting with the device
     * (see {@link #_buildDeviceSubMenu}).
     * @see #_buildDeviceSubMenu
     */
    _createDeviceItem: function(device) {
        if (!device.can_connect && device.capabilities == GnomeBluetoothApplet.Capabilities.NONE)
            return null;
        let item = new PopupMenu.PopupSubMenuMenuItem(device.alias);

        // adopt the device object, and add a back link
        item._device = device;
        device._item = item;

        this._buildDeviceSubMenu(item, device);

        return item;
    },

    /** Populates a device's submenu with items being options for that device.
     *
     * It creates an on/off toggle to connect/disconnect from the device (will
     * only be shown if the device can be connected/disconnected to).
     *
     * Also adds a 'Send Files...' and 'Browse Files...' item if the device
     * supports this.
     *
     * Finally, if the device is a bluetooth keyboard or mouse or speakers,
     * an item is added leading to the appropriate settinsg
     * ("{Keyboard, Mouse Sound} Settings").
     * @inheritparams #_updateDeviceItem
     */
    _buildDeviceSubMenu: function(item, device) {
        if (device.can_connect) {
            item._connected = device.connected;
            item._connectedMenuitem = new PopupMenu.PopupSwitchMenuItem(_("Connection"), device.connected);
            item._connectedMenuitem.connect('toggled', Lang.bind(this, function() {
                if (item._connected > ConnectionState.CONNECTED) {
                    // operation already in progress, revert
                    // (should not happen anyway)
                    menuitem.setToggleState(menuitem.state);
                }
                if (item._connected) {
                    item._connected = ConnectionState.DISCONNECTING;
                    menuitem.setStatus(_("disconnecting..."));
                    this._applet.disconnect_device(item._device.device_path, function(applet, success) {
                        if (success) { // apply
                            item._connected = ConnectionState.DISCONNECTED;
                            menuitem.setToggleState(false);
                        } else { // revert
                            item._connected = ConnectionState.CONNECTED;
                            menuitem.setToggleState(true);
                        }
                        menuitem.setStatus(null);
                    });
                } else {
                    item._connected = ConnectionState.CONNECTING;
                    menuitem.setStatus(_("connecting..."));
                    this._applet.connect_device(item._device.device_path, function(applet, success) {
                        if (success) { // apply
                            item._connected = ConnectionState.CONNECTED;
                            menuitem.setToggleState(true);
                        } else { // revert
                            item._connected = ConnectionState.DISCONNECTED;
                            menuitem.setToggleState(false);
                        }
                        menuitem.setStatus(null);
                    });
                }
            }));

            item.menu.addMenuItem(item._connectedMenuitem);
        }

        if (device.capabilities & GnomeBluetoothApplet.Capabilities.OBEX_PUSH) {
            item.menu.addAction(_("Send Files..."), Lang.bind(this, function() {
                this._applet.send_to_address(device.bdaddr, device.alias);
            }));
        }
        if (device.capabilities & GnomeBluetoothApplet.Capabilities.OBEX_FILE_TRANSFER) {
            item.menu.addAction(_("Browse Files..."), Lang.bind(this, function(event) {
                this._applet.browse_address(device.bdaddr, event.get_time(),
                    Lang.bind(this, function(applet, result) {
                        try {
                            applet.browse_address_finish(result);
                        } catch (e) {
                            this._ensureSource();
                            this._source.notify(new MessageTray.Notification(this._source,
                                 _("Bluetooth"),
                                 _("Error browsing device"),
                                 { body: _("The requested device cannot be browsed, error is '%s'").format(e) }));
                        }
                    }));
            }));
        }

        switch (device.type) {
        case GnomeBluetoothApplet.Type.KEYBOARD:
            item.menu.addSettingsAction(_("Keyboard Settings"), 'gnome-keyboard-panel.desktop');
            break;
        case GnomeBluetoothApplet.Type.MOUSE:
            item.menu.addSettingsAction(_("Mouse Settings"), 'gnome-mouse-panel.desktop');
            break;
        case GnomeBluetoothApplet.Type.HEADSET:
        case GnomeBluetoothApplet.Type.HEADPHONES:
        case GnomeBluetoothApplet.Type.OTHER_AUDIO:
            item.menu.addSettingsAction(_("Sound Settings"), 'gnome-sound-panel.desktop');
            break;
        default:
            break;
        }
    },

    /** Callback ensuring that {@link #_applet}'s 
     * ['show_full_menu' property](http://git.gnome.org/browse/gnome-bluetooth/tree/applet/bluetooth-applet.c#n1027)
     * is respected.
     *
     * This setting controls whether the submenu items for each device and the
     * "Send Files to Device"/"Set up a New Device" items are shown
     * in the menu.
     *
     * (If this setting is false, just the bluetooth and visibility toggles
     * will be shown).
     */
    _updateFullMenu: function() {
        if (this._applet.show_full_menu) {
            this._showAll(this._fullMenuItems);
            if (this._hasDevices)
                this._showAll(this._deviceItems);
        } else {
            this._hideAll(this._fullMenuItems);
            this._hideAll(this._deviceItems);
        }
    },

    /** Shows all popup menu items in `items`.
     * @param {PopupMenu.PopupBaseMenuItem[]} items - array of popup menu items.
     */
    _showAll: function(items) {
        for (let i = 0; i < items.length; i++)
            items[i].actor.show();
    },

    /** Hides all popup menu items in `items`.
     * @inheritparams #_showAll
     */
    _hideAll: function(items) {
        for (let i = 0; i < items.length; i++)
            items[i].actor.hide();
    },

    /** Destroys all popup menu items in `items`.
     * @inheritparams #_showAll
     */
    _destroyAll: function(items) {
        for (let i = 0; i < items.length; i++)
            items[i].destroy();
    },

    /** Ensures that we have a {@link Source} in the MessageTray (from which
     * to show bluetooth notifications).
     *
     * If we already have one, does nothing. If we don't, we create one
     * and store it in `this._source` and add it to the message tray.
     * @see Source
     * @see MessageTray.MessageTray#add
     */
    _ensureSource: function() {
        if (!this._source) {
            this._source = new Source();
            Main.messageTray.add(this._source);
        }
    },

    /** Callback for the 'auth-request' signal of {@link #_applet},
     * when an authorization request is received.
     * This creates a new {@link AuthNotification} for the request and
     * notifies it from our {@link Source}.
     * @inheritparams AuthNotification
     */
    _authRequest: function(applet, device_path, name, long_name, uuid) {
        this._ensureSource();
        this._source.notify(new AuthNotification(this._source, this._applet, device_path, name, long_name, uuid));
    },

    /** Callback for the 'confirm-request' signal of {@link #_applet},
     * when a confirm request is received.
     * This creates a new {@link ConfirmNotification} for the request and
     * notifies it from our {@link Source}.
     * @inheritparams ConfirmNotification
     */
    _confirmRequest: function(applet, device_path, name, long_name, pin) {
        this._ensureSource();
        this._source.notify(new ConfirmNotification(this._source, this._applet, device_path, name, long_name, pin));
    },

    /** Callback for the 'pin-request' signal of {@link #_applet},
     * when a pin request is received.
     * This creates a new {@link PinNotification} for the request and
     * notifies it from our {@link Source}.
     * @inheritparams PinNotification
     */
    _pinRequest: function(applet, device_path, name, long_name, numeric) {
        this._ensureSource();
        this._source.notify(new PinNotification(this._source, this._applet, device_path, name, long_name, numeric));
    },

    /** Callback for the 'cancel-request' signal of {@link #_applet},
     * called when a request is cancelled.
     * Calls {@link Source#destroy} to remove any notifications and the source
     * from the message tray.
     */
    _cancelRequest: function() {
        this._source.destroy();
    }
}

/** creates a new Source for bluetooth-related notifications.
 *
 * Calls the [constructofr]{@link MessageTray.Source} with title "Bluetooth"
 * (translated), and calls {@link #_setSummaryIcon} with
 * {@link #createNotificationIcon} (the icon is the 'bluetooth-active'
 * symbolic icon).
 * @classdesc
 * This implements {@link MessageTray.Source} for bluetooth-related notifications.
 *
 * In particular, it implements {@link MessageTray.#createNotificationIcon} to set the
 * source icon to the 'bluetooth-active' symbolic icon.
 *
 * It also *overrides* {@link MessageTray.Source#notify} to add an extra signal listening
 * to the notification's 'destroy' signal such that when the last notification
 * for the Source is destroyed, {@link #destroy} is called (TODO -
 * isn't that what {@link #_lastNotificationRemoved} is for?)
 *
 * The [Bluetooth Indicator]{@link Indicator} uses one of these to emit
 * {@link AuthNotification}, {@link PinNotification} and
 * {@link ConfirmNotification} (as well as standard
 * {@link MessageTray.Notification}s).
 *
 * @class
 * @extends MessageTray.Source
 * @see AuthNotification
 * @see PinNotification
 * @see ConfirmNotification
 */
function Source() {
    this._init.apply(this, arguments);
}

Source.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function() {
        MessageTray.Source.prototype._init.call(this, _("Bluetooth"));

        this._setSummaryIcon(this.createNotificationIcon());
    },

    /** Overrides the parent function to additionally connect to the 'destroy'
     * signal of `notification` such that when the last notification is destroyed,
     * {@link #destroy} is called (TODO: isn't that what
     * {@link #_lastNotificationRemoved} is for?)
     *
     * After that it calls the parent function.
     * @override
     */
    notify: function(notification) {
        this._private_destroyId = notification.connect('destroy', Lang.bind(this, function(notification) {
            if (this.notification == notification) {
                // the destroyed notification is the last for this source
                this.notification.disconnect(this._private_destroyId);
                this.destroy();
            }
        }));

        MessageTray.Source.prototype.notify.call(this, notification);
    },

    /** Implements the parent function to set the icon for this source to
     * the 'bluetooth-active' icon (symbolic).
     * @override */
    createNotificationIcon: function() {
        return new St.Icon({ icon_name: 'bluetooth-active',
                             icon_type: St.IconType.SYMBOLIC,
                             icon_size: this.ICON_SIZE });
    }
}

/** creates a new AuthNotification.
 *
 * This calls the constructor with `source`, title "Bluetooth",
 * banner "Authorization request from `name`", and custom content.
 * 
 * The notification is resident.
 *
 * The body of the notification is "Device `long_name` wants to access
 * service `uuid`" ({@link #addBody}).
 *
 * Buttons "Always grant access", "Grant this time only", and "Reject" are
 * added ({@link #addButton}).
 *
 * The 'action-invoked' signal of the notification (i.e. when one of the buttons
 * is clicked) is connected to, communicating the response to the `applet` and
 * destroying the notification.
 *
 * @param {Bluetooth.Source} source - the {@link Source} to open the
 * notification from.
 * @param {GnomeBluetoothApplet.Applet} applet - the applet emitting the signal
 * ({@link Indicator#_applet}).
 * @param {string} device_path - DBus path to the device.
 * @param {string} name - name of the device
 * @param {string} long_name - long name of the device
 * @param {string} uuid - UUID of the device
 * @classdesc
 * ![AuthNotification](pics/bluetooth.AuthNotification.png)
 *
 * This is a notification that appears upon an authorization request.
 * @class
 * @see Indicator#_onAuthRequest
 * @extends MessageTray.Notification
 */
function AuthNotification() {
    this._init.apply(this, arguments);
}

AuthNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, applet, device_path, name, long_name, uuid) {
        MessageTray.Notification.prototype._init.call(this,
                                                      source,
                                                      _("Bluetooth"),
                                                      _("Authorization request from %s").format(name),
                                                      { customContent: true });
        this.setResident(true);

        this._applet = applet;
        this._devicePath = device_path;
        this.addBody(_("Device %s wants access to the service '%s'").format(long_name, uuid));

        this.addButton('always-grant', _("Always grant access"));
        this.addButton('grant', _("Grant this time only"));
        this.addButton('reject', _("Reject"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            switch (action) {
            case 'always-grant':
                this._applet.agent_reply_auth(this._devicePath, true, true);
                break;
            case 'grant':
                this._applet.agent_reply_auth(this._devicePath, true, false);
                break;
            case 'reject':
            default:
                this._applet.agent_reply_auth(this._devicePath, false, false);
            }
            this.destroy();
        }));
    }
}

/** creates a new ConfirmNotification.
 *
 * This calls the constructor with `source`, title "Bluetooth",
 * banner "Pairing confirmation from `name`", and custom content.
 * 
 * The notification is resident.
 *
 * The body of the notification is "Device `long_name` wants to pair with this
 * computer" and "Please confirm whether the PIN '`pin`' matches the one on this
 * device" ({@link #addBody}).
 *
 * Buttons "Matches" and "Does not match" are added
 * ({@link #addButton}).
 *
 * The 'action-invoked' signal of the notification (i.e. when one of the buttons
 * is clicked) is connected to, communicating the response to the `applet` and
 * destroying the notification.
 *
 * @param {Bluetooth.Source} source - the {@link Source} to open the
 * notification from.
 * @param {GnomeBluetoothApplet.Applet} applet - the applet emitting the signal
 * ({@link Indicator#_applet}).
 * @param {string} device_path - DBus path to the device.
 * @param {string} name - name of the device
 * @param {string} long_name - long name of the device
 * @param {string} pin - pin to compare against.
 * @classdesc
 * ![ConfirmNotification](pics/bluetooth.ConfirmNotification.png)
 *
 * This is a notification that appears upon an authorization request.
 * @class
 * @see Indicator#_onConfirmRequest
 * @extends MessageTray.Notification
 */
function ConfirmNotification() {
    this._init.apply(this, arguments);
}

ConfirmNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, applet, device_path, name, long_name, pin) {
        MessageTray.Notification.prototype._init.call(this,
                                                      source,
                                                      _("Bluetooth"),
                                                      _("Pairing confirmation for %s").format(name),
                                                      { customContent: true });
        this.setResident(true);

        this._applet = applet;
        this._devicePath = device_path;
        this.addBody(_("Device %s wants to pair with this computer").format(long_name));
        this.addBody(_("Please confirm whether the PIN '%s' matches the one on the device.").format(pin));

        this.addButton('matches', _("Matches"));
        this.addButton('does-not-match', _("Does not match"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            if (action == 'matches')
                this._applet.agent_reply_confirm(this._devicePath, true);
            else
                this._applet.agent_reply_confirm(this._devicePath, false);
            this.destroy();
        }));
    }
}

/** creates a new PinNotification.
 *
 * This calls the constructor with `source`, title "Bluetooth",
 * banner "Pairing confirmation from `name`", and custom content.
 * 
 * The notification is resident.
 *
 * The body of the notification is "Device `long_name` wants to pair with this
 * computer" and "Please enter the PIN mentioned on the device"
 * ({@link #addBody}).
 *
 * A St.Entry is added to the notification for the user to enter a PIN
 * ({@link #addActor}).
 *
 * Buttons "OK" and "Cancel" are added ({@link #addButton}).
 *
 * Upon pressing enter in the entry box or clicking the "OK" button, the pin is
 * communicated to `applet`. Upon pressing escape in the entry box or clicking
 * the "Cancel" button, the request is cancelled.
 * Either way, the notification is then destroyed.
 *
 * @param {Bluetooth.Source} source - the {@link Source} to open the
 * notification from.
 * @param {GnomeBluetoothApplet.Applet} applet - the applet emitting the signal
 * ({@link Indicator#_applet}).
 * @param {string} device_path - DBus path to the device.
 * @param {string} name - name of the device
 * @param {string} long_name - long name of the device
 * @param {boolean} numeric - whether the pin is numeric.
 * @classdesc
 * ![ConfirmNotification](pics/bluetooth.ConfirmNotification.png)
 *
 * This is a notification that appears upon an authorization request.
 * @class
 * @see Indicator#_onConfirmRequest
 * @extends MessageTray.Notification
 */
function PinNotification() {
    this._init.apply(this, arguments);
}

PinNotification.prototype = {
    __proto__: MessageTray.Notification.prototype,

    _init: function(source, applet, device_path, name, long_name, numeric) {
        MessageTray.Notification.prototype._init.call(this,
                                                      source,
                                                      _("Bluetooth"),
                                                      _("Pairing request for %s").format(name),
                                                      { customContent: true });
        this.setResident(true);

        this._applet = applet;
        this._devicePath = device_path;
        this._numeric = numeric;
        this.addBody(_("Device %s wants to pair with this computer").format(long_name));
        this.addBody(_("Please enter the PIN mentioned on the device."));

        this._entry = new St.Entry();
        this._entry.connect('key-release-event', Lang.bind(this, function(entry, event) {
            let key = event.get_key_symbol();
            if (key == Clutter.KEY_Return) {
                this.emit('action-invoked', 'ok');
                return true;
            } else if (key == Clutter.KEY_Escape) {
                this.emit('action-invoked', 'cancel');
                return true;
            }
            return false;
        }));
        this.addActor(this._entry);

        this.addButton('ok', _("OK"));
        this.addButton('cancel', _("Cancel"));

        this.connect('action-invoked', Lang.bind(this, function(self, action) {
            if (action == 'ok') {
                if (this._numeric) {
                    let num = parseInt(this._entry.text);
                    if (isNaN(num)) {
                        // user reply was empty, or was invalid
                        // cancel the operation
                        num = -1;
                    }
                    this._applet.agent_reply_passkey(this._devicePath, num);
                } else
                    this._applet.agent_reply_pincode(this._devicePath, this._entry.text);
            } else {
                if (this._numeric)
                    this._applet.agent_reply_passkey(this._devicePath, -1);
                else
                    this._applet.agent_reply_pincode(this._devicePath, null);
            }
            this.destroy();
        }));
    },

    // NOTE: do not document this, first it is never called and second if it
    // were called it would result in an error as MessageTray.Notification has
    // *no* 'grabFocus' method.
    //
    // I suspect it's a stub from an older version of GNOME shell that forgot
    // to be removed.
    // https://bugzilla.gnome.org/show_bug.cgi?id=687081
    grabFocus: function(lockTray) {
        MessageTray.Notification.prototype.grabFocus.call(this, lockTray);
        global.stage.set_key_focus(this._entry);
    }
}
