// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the acceibility (a11y) indicator.
 * ![Accessibility status indicator](pics/status.accessibility.png)
 */

const DBus = imports.dbus;
const GConf = imports.gi.GConf;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Util = imports.misc.util;

/** @+
 * @const
 * @default */
/** @+
 * @type {string} */
/** gsettings schema for a11y settings for the keyboard */
const A11Y_SCHEMA = 'org.gnome.desktop.a11y.keyboard';
/** gsettings key under {@link A11Y_SCHEMA} for enabling sticky keys */
const KEY_STICKY_KEYS_ENABLED = 'stickykeys-enable';
/** gsettings key under {@link A11Y_SCHEMA} for enabling bounce keys */
const KEY_BOUNCE_KEYS_ENABLED = 'bouncekeys-enable';
/** gsettings key under {@link A11Y_SCHEMA} for slow keys */
const KEY_SLOW_KEYS_ENABLED   = 'slowkeys-enable';
/** gsettings key under {@link A11Y_SCHEMA} for mouse keys */
const KEY_MOUSE_KEYS_ENABLED  = 'mousekeys-enable';

/** gsettings schema for a11y settings for enabling/disabling the screen
 * magnifier/keyboard/reader */
const APPLICATIONS_SCHEMA = 'org.gnome.desktop.a11y.applications';
/** @- */

// haven't documented these yet
const DPI_LOW_REASONABLE_VALUE  = 50;
const DPI_HIGH_REASONABLE_VALUE = 500;

/** factor by which to increase text scaling
 * (see {@link KEY_TEXT_SCALING_FACTOR})
 * @type {number} */
const DPI_FACTOR_LARGE   = 1.25;
const DPI_FACTOR_LARGER  = 1.5;
const DPI_FACTOR_LARGEST = 2.0;

/** @+
 * @type {string} */
/** GConf schema for metacity settings (on GNOME 3.4+ these have all been moved
 * to gsettings key 'org.gnome.shell.wm.preferences' */
const KEY_META_DIR       = '/apps/metacity/general';
/** GConf key for the 'visual bell' setting under {@link KEY_META_DIR}. */
const KEY_VISUAL_BELL = KEY_META_DIR + '/visual_bell';

/** Gsettings schema for interface themes etc */
const DESKTOP_INTERFACE_SCHEMA = 'org.gnome.desktop.interface';
/* Gsettings key under {@link DESKTOP_INTERFACE_SCHEMA} that controls
 * the GTK theme. */
const KEY_GTK_THEME      = 'gtk-theme';
/** Gsettings key under {@link DESKTOP_INTERFACE_SCHEMA} that controls
 * the icon theme. */
const KEY_ICON_THEME     = 'icon-theme';
/** Gsettings key under {@link DESKTOP_INTERFACE_SCHEMA} that controls
 * the text scaling factor.
 * @see DPI_FACTOR_LARGE */
const KEY_TEXT_SCALING_FACTOR = 'text-scaling-factor';

/** Gsettings key under {@link DESKTOP_INTERFACE_SCHEMA} specifying the high
 * contrast theme name. */
const HIGH_CONTRAST_THEME = 'HighContrast';
/** @- */
/** @- */

/** creates a new ATIndicator (accessibility indicator).
 * The icon name in the constructor is 'preferences-desktop-accessibility'.
 *
 * The constructor creates a whole bunch of
 * {@link PopupMenu.PopupSwitchMenuItem}s, each tied to a dconf/gconf
 * setting - updating their toggles to reflect the settings values, and also
 * making sure that when the toggles are used, the settings values are changed.
 *
 * @see #_buildItem
 * @see #_buildItemGConf
 *
 * @classdesc
 * ![Accessibility status indicator](pics/status.accessibility.png)
 * 
 * This is the accessibility indicator in the status area.
 * It mainly wraps around a whole bunch of gconf/dconf settings controlling
 * accessibility (see all the constants in this module).
 *
 * It contains a number of popup menu items controlling each of the settings.
 * @class
 * @extends PanelMenu.SystemStatusButton
 */
function ATIndicator() {
    this._init.apply(this, arguments);
}

ATIndicator.prototype = {
    __proto__: PanelMenu.SystemStatusButton.prototype,

    _init: function() {
        PanelMenu.SystemStatusButton.prototype._init.call(this, 'preferences-desktop-accessibility', null);

        let client = GConf.Client.get_default();
        client.add_dir(KEY_META_DIR, GConf.ClientPreloadType.PRELOAD_ONELEVEL, null);
        client.notify_add(KEY_META_DIR, Lang.bind(this, this._keyChanged), null, null);

        let highContrast = this._buildHCItem();
        this.menu.addMenuItem(highContrast);

        let magnifier = this._buildItem(_("Zoom"), APPLICATIONS_SCHEMA,
                                                   'screen-magnifier-enabled');
        this.menu.addMenuItem(magnifier);

        let textZoom = this._buildFontItem();
        this.menu.addMenuItem(textZoom);

//        let screenReader = this._buildItem(_("Screen Reader"), APPLICATIONS_SCHEMA,
//                                                               'screen-reader-enabled');
//        this.menu.addMenuItem(screenReader);

        let screenKeyboard = this._buildItem(_("Screen Keyboard"), APPLICATIONS_SCHEMA,
                                                                   'screen-keyboard-enabled');
        this.menu.addMenuItem(screenKeyboard);

        let visualBell = this._buildItemGConf(_("Visual Alerts"), client, KEY_VISUAL_BELL);
        this.menu.addMenuItem(visualBell);

        let stickyKeys = this._buildItem(_("Sticky Keys"), A11Y_SCHEMA, KEY_STICKY_KEYS_ENABLED);
        this.menu.addMenuItem(stickyKeys);

        let slowKeys = this._buildItem(_("Slow Keys"), A11Y_SCHEMA, KEY_SLOW_KEYS_ENABLED);
        this.menu.addMenuItem(slowKeys);

        let bounceKeys = this._buildItem(_("Bounce Keys"), A11Y_SCHEMA, KEY_BOUNCE_KEYS_ENABLED);
        this.menu.addMenuItem(bounceKeys);

        let mouseKeys = this._buildItem(_("Mouse Keys"), A11Y_SCHEMA, KEY_MOUSE_KEYS_ENABLED);
        this.menu.addMenuItem(mouseKeys);

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
        this.menu.addSettingsAction(_("Universal Access Settings"), 'gnome-universal-access-panel.desktop');
    },

    /** Convenience function that makes a {@link PopupSwitchMenuItem} with
     * a label, initial value, and callback for when it's toggled.
     *
     * @param {string} string - label to display on the menu item.
     * @param {boolean} initial_value - initial value of the toggle.
     * @param {boolean} writable - whether the user can change the value
     * (if not, the `reactive` property of the menu item is set to `false`).
     * @param {function(boolean)} on_set - callback for when the item is
     * toggled. The input argument is whether the item has been toggled on
     * or off.
     * @returns {PopupMenu.PopupSwitchMenuItem} a popup menu item with a toggle
     * reflecting the input parameters.
     */
    _buildItemExtended: function(string, initial_value, writable, on_set) {
        let widget = new PopupMenu.PopupSwitchMenuItem(string, initial_value);
        if (!writable)
            widget.actor.reactive = false;
        else
            widget.connect('toggled', function(item) {
                on_set(item.state);
            });
        return widget;
    },

    /** Convenience function that makes a {@link PopupMenu.PopupSwitchMenuItem}
     * that is tied to a particular gconf key and ensures that the toggle is
     * always in sync with the value of the gconf key.
     *
     * Listens to signal {@link .gconf-changed} in order to keep
     * the toggle in sync with changes in the gconf key.
     * @inheritparams #_buildItemExtended
     * @param {Gconf.Client} client - GConf client to interact with.
     * @param {string} key - gconf key (e.g. {@link KEY_VISUAL_BELL}).
     */
    _buildItemGConf: function(string, client, key) {
        function on_get() {
            return client.get_bool(key);
        }
        let widget = this._buildItemExtended(string,
            client.get_bool(key),
            client.key_is_writable(key),
            function(enabled) {
                client.set_bool(key, enabled);
            });
        this.connect('gconf-changed', function() {
            widget.setToggleState(client.get_bool(key));
        });
        return widget;
    },

    /** Convenience function that makes a {@link PopupMenu.PopupSwitchMenuItem}
     * that is tied to a particular dconf (gsettings) key and ensures that the
     * toggle is always in sync with the value of the gsettings key.
     * @inheritparams #_buildItemExtended
     * @param {string} schema - the schema to use (example {@link A11Y_SCHEMA}).
     * @param {string} key - gsettings key in the schema to keep in sync with.
     */
    _buildItem: function(string, schema, key) {
        let settings = new Gio.Settings({ schema: schema });
        let widget = this._buildItemExtended(string,
            settings.get_boolean(key),
            settings.is_writable(key),
            function(enabled) {
                return settings.set_boolean(key, enabled);
            });
        settings.connect('changed::'+key, function() {
            widget.setToggleState(settings.get_boolean(key));
        });
        return widget;
    },

    /** Builds the high contrast {@link PopupMenu.PopupSwitchMenuItem}.
     * When this is toggled on, both the GTK and icon themes are changed to
     * {@link HIGH_CONTRAST_THEME}. When toggling off, the themes are switched
     * back to what they used to be.
     * @inheritparams #_buildItemExtended
     */
    _buildHCItem: function() {
        let settings = new Gio.Settings({ schema: DESKTOP_INTERFACE_SCHEMA });
        let gtkTheme = settings.get_string(KEY_GTK_THEME);
        let iconTheme = settings.get_string(KEY_ICON_THEME);
        let hasHC = (gtkTheme == HIGH_CONTRAST_THEME);
        let highContrast = this._buildItemExtended(
            _("High Contrast"),
            hasHC,
            settings.is_writable(KEY_GTK_THEME) && settings.is_writable(KEY_ICON_THEME),
            function (enabled) {
                if (enabled) {
                    settings.set_string(KEY_GTK_THEME, HIGH_CONTRAST_THEME);
                    settings.set_string(KEY_ICON_THEME, HIGH_CONTRAST_THEME);
                } else if(!hasHC) {
                    settings.set_string(KEY_GTK_THEME, gtkTheme);
                    settings.set_string(KEY_ICON_THEME, iconTheme);
                } else {
                    settings.reset(KEY_GTK_THEME);
                    settings.reset(KEY_ICON_THEME);
                }
            });
        settings.connect('changed::' + KEY_GTK_THEME, function() {
            let value = settings.get_string(KEY_GTK_THEME);
            if (value == HIGH_CONTRAST_THEME) {
                highContrast.setToggleState(true);
            } else {
                highContrast.setToggleState(false);
                gtkTheme = value;
            }
        });
        settings.connect('changed::' + KEY_ICON_THEME, function() {
            let value = settings.get_string(KEY_ICON_THEME);
            if (value != HIGH_CONTRAST_THEME)
                iconTheme = value;
        });
        return highContrast;
    },

    /** Builds the font toggle item (to toggled between large and normal
     * font size).
     *
     * If enabled, this sets the text scaling factor to {@link DPI_FACTOR_LARGE}.
     * When disabled, this sets the text scaling factor back to whatever it
     * used to be.
     * @inheritparams #_buildItemExtended
     */
    _buildFontItem: function() {
        let settings = new Gio.Settings({ schema: DESKTOP_INTERFACE_SCHEMA });

        let factor = settings.get_double(KEY_TEXT_SCALING_FACTOR);
        let initial_setting = (factor > 1.0);
        let widget = this._buildItemExtended(_("Large Text"),
            initial_setting,
            settings.is_writable(KEY_TEXT_SCALING_FACTOR),
            function (enabled) {
                if (enabled)
                    settings.set_double(KEY_TEXT_SCALING_FACTOR,
                                        DPI_FACTOR_LARGE);
                else
                    settings.reset(KEY_TEXT_SCALING_FACTOR);
            });
        settings.connect('changed::' + KEY_TEXT_SCALING_FACTOR, function() {
            let factor = settings.get_double(KEY_TEXT_SCALING_FACTOR);
            let active = (factor > 1.0);
            widget.setToggleState(active);
        });
        return widget;
    },

    /** callback whenever a key in GConf schema {@link KEY_META_DIR} changes.
     * This just emits a 'gconf-changed' signal (used by
     * {@link #_buildItemGConf}).
     * @fires .gconf-changed
     */
    _keyChanged: function() {
        this.emit('gconf-changed');
    }
};
Signals.addSignalMethods(ATIndicator.prototype);
/** Emitted whenever a key under gconf schema {@link KEY_META_DIR} changes.
 * Used internally by the {@link ATIndicator} to keep toggle items in sync
 * with their settings, not much use to external users.
 * @event
 * @name .gconf-changed
 * @memberof ATIndicator
 * @param {Accessibility.ATIndicator} obj - the indicator that fired the signal
 */
