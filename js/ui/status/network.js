// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file defines classes for the network status indicator.
 *
 * Basically this defines the network manager applet for the
 * [NetworkManager](http://projects.gnome.org/NetworkManager/) service
 * (?), providing a graphical way for the user to interact with it.
 *
 * ![{@link NMApplet} (network status indicator)](pics/status.network.png)
 *
 * Although the indicator looks simple, there are many classes in this file
 * to get it all working.
 * They include:
 *
 * * Graphical classes for the UI:
 *  + the overall status indicator, {@link NMApplet}
 *  + Custom popup menu section headers ("Wireless", "Wired") with on/off toggles if appropriate
 *  or a status message ("Disconnected"): {@link NMWiredSectionTitleMenuItem},
 *  {@link NMWirelessSectionTitleMenuItem};
 *  + a Source for the message tray for network-related notifications
 *  ({@link NMMessageTraySource}).
 * * Local javascript classes and wrappers to interact with NetworkManager,
 *   as well as managing popup menu items:
 *  + {@link NMDevice}
 *  + {@link NMDeviceBluetooth}
 *  + {@link NMDeviceModem}
 *  + {@link NMDeviceVPN}
 *  + {@link NMDeviceWired}
 *  + {@link NMDeviceWireless}
 *
 * The NMDevice* classes all manage a popup menu section that consists of:
 *
 * * the "title" item for the device bearing the device name and an on/off
 * toggle or status text (example: the "Atheros AR8132 Fast Ethernet" item
 * or the "HTC Android Phone" item in the above picture);
 * * a section containing a popup menu item for each availabe connection
 * for this device (for example, the item for each available network in
 * the picture above, the device being my wireless card).
 *
 * There are various factors that affect what gets shown in the network manager;
 * for example, if a particular "section" (wired/wireless/vpn/mobile broadband)
 * has no network interfaces it is hidden; if it has more than one (as my
 * "Wired" section does in the picture above) each NMDevice*'s popup menu
 * items are shown in full; if it has exactly one (like my "Wireless" section
 * above), the device heading is hidden and combined into the section title
 * heading (see how the "Wireless" section title has the on/off toggle and
 * there's no item with my wireless card's name on it and an on/off toggle).
 *
 * The way we talk to NetworkManager is using their GObject introspection libraries,
 * which wrap the NetworkManager DBus interface (i.e open
 * it up to Javascript via GObject introspection):
 *  + [NMClient](http://developer.gnome.org/libnm-glib/unstable/index.html), known
 *  in the code as `NMClient = imports.gi.NMClient`;
 *  + [NM-Utils](http://developer.gnome.org/libnm-util/unstable8/index.html),
 *  known in the code as `NetworkManager = imoprts.gi.NetworkManager`.
 *
 * Before jumping into reading the documentation for this file it may be helpful
 * to first get an idea of how all the NetworkManager classes work together.
 *
 * NMClient's [Object Overview page](http://developer.gnome.org/libnm-glib/unstable/ref-overview.html)
 * is well worth reading in this regard. I will reproduce it here:
 *
 * ![How NMClient classes work together](pics/libnm-glib.png)
 *
 * `NMClient` is the base object that provides access to network device objects,
 * active connection objects, an general network state connection.
 * There is one instance that gets shared around all the objects in this file;
 * it is stored as {@link NMApplet#_client}.
 *
 * `NMDevice` represents a known network interface that may be used to connect
 * to a network, for example Ethernet, Wifi, Modem, Bluetooth...
 * We have local Javascript classes for these: {@link NMDevice},
 * {@link NMDeviceBluetooth}, {@link NMDeviceModem}, {@link NMDeviceVPN},
 * {@link NMDeviceWired}, {@link NMDeviceWireless}.
 * Get the devices from the client using `nm_client_get_devices()`.
 *
 * `NMActiveConnection` represents an active connection to a *specific* network.
 * It has an associated `NMRemoteConnection` from which it gets its
 * settings, which are all managed by a single `NMRemoteSettings`.
 * This settings instance is stored in the {@link NMApplet} as
 * {@link NMApplet#_settings} and one retrieves all connections it has using
 * `nm_settings_list_connections`.
 *
 * To look up the `NMRemoteConnection` for
 * a particular `NMActiveConnection`, get its (DBus) object path with
 * `nm_active_connection_get_connection` and then call
 * `nm_remote_settings_get_connection_by_path` to get the remote connection.
 *
 * To get the interfaces (devices) used by a connection, use
 * `nm_active_connection_get_devices`.
 */
const ByteArray = imports.byteArray;
const DBus = imports.dbus;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const NetworkManager = imports.gi.NetworkManager;
const NMClient = imports.gi.NMClient;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const MessageTray = imports.ui.messageTray;
const ModemManager = imports.misc.modemManager;
const Util = imports.misc.util;

/** @+
 * @enum
 * @const
 */

/** Categories for various devices. Used in the various classes
 * as keys into objects.
 * @type {string} */
const NMConnectionCategory = {
    INVALID: 'invalid',
    WIRED: 'wired',
    WIRELESS: 'wireless',
    WWAN: 'wwan',
    VPN: 'vpn'
};

/** The encryption/security type of an access point.
 * @type {number} */
const NMAccessPointSecurity = {
    UNKNOWN: 0,
    NONE: 1,
    WEP: 2,
    WPA_PSK: 3,
    WPA2_PSK: 4,
    WPA_ENT: 5,
    WPA2_ENT: 6
};
/** @- */

// small optimization, to avoid using [] all the time
const NM80211Mode = NetworkManager['80211Mode']; //  ADHOC:1 INFRA:2
const NM80211ApFlags = NetworkManager['80211ApFlags'];
const NM80211ApSecurityFlags = NetworkManager['80211ApSecurityFlags'];

/** Number of wireless networks that should be visible
 * (the remaining are placed into a "More..." section)
 * @type {number}
 * @default
 * @const */
const NUM_VISIBLE_NETWORKS = 5;

/** 
 * **[UNUSED]** Converts a MAC address to an array of numbers by converting each section
 * from hexadecimal to decimal.
 *
 * @param {string} string - mac address as a string, including the ':'
 * @returns {number[]} array where each section of the MAC address (separated
 * by ':') has been converted to its decimal number equivalent.
 * @example
 * macToArray('ab:cd:ef:01:23:45')
 * // returns [171,205,239,1,35,69]
 */
function macToArray(string) {
    return string.split(':').map(function(el) {
        return parseInt(el, 16);
    });
}

/**
 * **[UNUSED]** Compares two MAC addresses.
 * @param {number[]|string[]} one - array with 6 elements representing the
 * MAC address (e.g. `['ab','cd','ef','01','23','45]`, or output from
 * {@link macToArray}).
 * @param {number[]|string[]} two - same as `one`, to compare against
 * @returns {boolean} whether the addresses are identical.
 */
function macCompare(one, two) {
    for (let i = 0; i < 6; i++) {
        if (one[i] != two[i])
            return false;
    }
    return true;
}

/** Compares two SSIDs.
 *
 * Note - it first compares their lengths, returning `false` if they are not equal,
 * and then it loops through each element (byte?) of the SSIDs, quitting as soon
 * as it finds some that don't match.
 *
 * @param {SSIDType} one - first SSID, e.g. from
 * [`access_point_get_ssid`](http://developer.gnome.org/libnm-glib/unstable/NMAccessPoint.html#nm-access-point-get-ssid)
 * @param {SSIDType} two - second SSID, as above.
 * @returns {boolean} whether the SSIDs are identical.
 */
function ssidCompare(one, two) {
    if (!one || !two)
        return false;
    if (one.length != two.length)
        return false;
    for (let i = 0; i < one.length; i++) {
        if (one[i] != two[i])
            return false;
    }
    return true;
}

/** Converts a signal strength to an icon name.
 *
 * From 0 to 4 (inclusive) is 'none'; 5 to 29 'weak'; 30 to 54 'ok';
 * 55 to 79 'good'; 80 to 100 'excellent'.
 *
 * @param {number} value - signal string, 0 to 100 (percent).
 * @returns {string} icon name for that signal strength.
 */
function signalToIcon(value) {
    if (value > 80)
        return 'excellent';
    if (value > 55)
        return 'good';
    if (value > 30)
        return 'ok';
    if (value > 5)
        return 'weak';
    return 'none';
}

/** Sorts an array of access points by strength, strongest connection
 * strength to weakest.
 * @param {NMClient.AccessPoint[]} accessPoints - array of access points to sort (each element should
 * have a property `strength` which is used to sort).
 * @returns {NMClient.AccessPoint[]} array of sorted access points, strongest connection
 * strength to weakest.
 */
function sortAccessPoints(accessPoints) {
    return accessPoints.sort(function (one, two) {
        return two.strength - one.strength;
    });
}

/** Converts an SSID to a string.
 * This uses [`NetworkManager.utils_ssid_to_utf8`](http://developer.gnome.org/libnm-util/unstable/libnm-util-nm-utils.html#nm-utils-ssid-to-utf8),
 * see for further details.
 * @param {SSIDType} ssid - SSID to convert
 * @returns {string} the result of `NetworkManager.utils_ssid_to_utf8`, or
 * "<unknown>" if that failed.
 */
function ssidToLabel(ssid) {
    let label = NetworkManager.utils_ssid_to_utf8(ssid);
    if (!label)
        label = _("<unknown>");
    return label;
}

/** creates a new NMNetworkMenuItem (the popup menu item for each available
 * wireless network).
 *
 * ![{@link NMNetworkMenuItem} outlined in red](pics/NMNetworkMenuItem.png)
 *
 * We call the [parent constructor]{@link PopupMenu.PopupBaseMenuItem} with
 * parameters `params`.
 *
 * We then sort `accessPoints` strongest to weakest ({@link sortAccessPoints})
 * and store the strongest one in {@link #bestAP}.
 *
 * If `title` was not given, we set it to the strongest access point's SSID
 * as a string ({@link ssidToLabel}).
 *
 * We then construct the item itself. It consists of:
 * 
 * * text (a St.Label) with `title`, left aligned, taking up most of the space;
 * * a St.BoxLayout at the right/end to hold:
 *  + the signal strength icon for the
 * network (see {@link #_getIcon}; the icon is stored in
 * {@link #_signalIcon}), and
 *  + the padlock icon indicating that the network is secured, stored as
 *  {@link #_secureIcon} (if relevant to the network).
 *
 * Finally we populate {@link #_accessPoints} where each element is an object
 * with properties 'ap' being an element of `accessPoints`, and 'updateId'
 * being the signal ID for its `notify::strength` signal (connected up to
 * {@link #_updated}).
 * @param {NMClient.AccessPoint[]} accessPoints - array of access points for this network
 * @param {?string} title - the label to show on the item. If not provided,
 * we will use the SSID (converted to a string) of the strongest access point
 * for the network.
 * @inheritparams PopupMenu.PopupBaseMenuItem
 * @classdesc
 * ![{@link NMNetworkMenuItem} outlined in red](pics/NMNetworkMenuItem.png)
 * This is the popup menu item that appears for each available wireless network
 * in the network menu. Used in {@link NMDeviceWireless#_createAPItem}.
 *
 * It consists of text with the network's name (or SSID converted to a string),
 * along with a signal strength icon and possibly a padlock icon showing that the
 * network is encrypted.
 *
 * Since each wireless network can have multiple access points, this class monitors
 * the list of access points to make sure the signal strength icon shows the
 * strength of the strongest access point, stored in {@link NMNetworkMenu#bestAP}.
 * @class
 * @extends PopupMenu.PopupBaseMenuItem
 */
function NMNetworkMenuItem() {
    this._init.apply(this, arguments);
}

NMNetworkMenuItem.prototype = {
    __proto__: PopupMenu.PopupBaseMenuItem.prototype,

    _init: function(accessPoints, title, params) {
        PopupMenu.PopupBaseMenuItem.prototype._init.call(this, params);

        accessPoints = sortAccessPoints(accessPoints);
        /** The strongest access point for this network.
         * @type {NMClient.AccessPoint}
         */
        this.bestAP = accessPoints[0];

        if (!title) {
            let ssid = this.bestAP.get_ssid();
            title = ssidToLabel(ssid);
        }

        /** The label for the popup menu item, showing the network's name.
         * @type {St.Label} */
        this._label = new St.Label({ text: title });
        this.addActor(this._label);
        this._icons = new St.BoxLayout({ style_class: 'nm-menu-item-icons' });
        this.addActor(this._icons, { align: St.Align.END });

        /** The icon for the popup menu item that shows the signal strength
         * (if relevant). See {@link #_getIcon}.
         * @type {St.Icon} */
        this._signalIcon = new St.Icon({ icon_name: this._getIcon(),
                                         style_class: 'popup-menu-icon' });
        this._icons.add_actor(this._signalIcon);

        /** The icon for the popup menu item that shows the network security
         * (if relevant). It's the 'network-wireless-encrypted' icon.
         * @type {St.Icon} */
        this._secureIcon = new St.Icon({ style_class: 'popup-menu-icon' });
        if (this.bestAP._secType != NMAccessPointSecurity.UNKNOWN &&
            this.bestAP._secType != NMAccessPointSecurity.NONE)
            this._secureIcon.icon_name = 'network-wireless-encrypted';
        this._icons.add_actor(this._secureIcon);

        /** The access points for this network. An array storing both the
         * access point and the signal ID for its 'notify::strength' signal
         * (which is connected to {@link #_updated}).
         * @type {Array.<{ap: NMClient.AccessPoint, updateId: number}>} */
        this._accessPoints = [ ];
        for (let i = 0; i < accessPoints.length; i++) {
            let ap = accessPoints[i];
            // need a wrapper object here, because the access points can be shared
            // between many NMNetworkMenuItems
            let apObj = {
                ap: ap,
                updateId: ap.connect('notify::strength', Lang.bind(this, this._updated))
            };
            this._accessPoints.push(apObj);
        }
    },

    /** Callback when the strength of any of the access points for this network
     * changes.
     *
     * This resets {@link #bestAP} to the strongest access point and updates
     * the signal strength icon.
     * @see #_getIcon
     * @param {NMClient.AccessPoint} ap - the access point whose strength changed.
     */
    _updated: function(ap) {
        if (ap.strength > this.bestAP.strength)
            this.bestAP = ap;

        this._signalIcon.icon_name = this._getIcon();
    },

    /** Retrieves the relevant icon name for the network's signal strenth.
     *
     * If it's an ad hoc network, the 'network-workgroup' icon is returned
     * ![network-workgroup](pics/icons/network-workgroup-symbolic.svg).
     * Otherwise, 'network-wireless-signal-[signalstrength]' is used:
     * ![network-wireless-signal-excellent](pics/icons/network-wireless-signal-excellent-symbolic.svg)
     * ![network-wireless-signal-good](pics/icons/network-wireless-signal-good-symbolic.svg)
     * ![network-wireless-signal-ok](pics/icons/network-wireless-signal-ok-symbolic.svg)
     * ![network-wireless-signal-weak](pics/icons/network-wireless-signal-weak-symbolic.svg)
     * ![network-wireless-signal-none](pics/icons/network-wireless-signal-none-symbolic.svg).
     *
     * This icon is stored in {@link #_signalIcon}.
     * @see signalToIcon
     */
    _getIcon: function() {
        if (this.bestAP.mode == NM80211Mode.ADHOC)
            return 'network-workgroup';
        else
            return 'network-wireless-signal-' + signalToIcon(this.bestAP.strength);
    },

    /** Updates {@link #_accessPoints}, regenerating it from `accessPoints`.
     *
     * Element `i` of {@link #_accessPoints} is an object:
     *
     *     {
     *       ap: `accessPoints[i]`,
     *       updateId: ap.connect('notify::strength', Lang.bind(this, this._updated))
     *     }
     *
     * @inheritparams NMNetworkMenuItem
     * @see #_updated
     */
    updateAccessPoints: function(accessPoints) {
        for (let i = 0; i < this._accessPoints.length; i++) {
            let apObj = this._accessPoints[i];
            apObj.ap.disconnect(apObj.updateId);
            apObj.updateId = 0;
        }

        accessPoints = sortAccessPoints(accessPoints);
        this.bestAP = accessPoints[0];
        this._accessPoints = [ ];
        for (let i = 0; i < accessPoints; i++) {
            let ap = accessPoints[i];
            let apObj = {
                ap: ap,
                updateId: ap.connect('notify::strength', Lang.bind(this, this._updated))
            };
            this._accessPoints.push(apObj);
        }
    },

    /** Destroys this network menu item.
     *
     * It disconnects all the 'notify::strength' signals we were listenin to
     * in {@link #_accessPoints} and then calls the parent function.
     * @override */
    destroy: function() {
        for (let i = 0; i < this._accessPoints.length; i++) {
            let apObj = this._accessPoints[i];
            apObj.ap.disconnect(apObj.updateId);
            apObj.updateId = 0;
        }

        PopupMenu.PopupBaseMenuItem.prototype.destroy.call(this);
    }
};

/** creates a new NMWiredSectionTitleMenuItem
 *
 * We call the [parent constructor]{@link PopupMenu.PopupSwitchMenuItem}
 * with text `label`, the toggle initialised to off, and configuration values
 * described in `params`.
 *
 * That's all!
 * @inheritparams PopupMenu.PopupSwitchMenuItem
 * @param {string} label - text to display on the title (e.g. "Wired" or
 * "VPN Connections").
 * @inheritparams NMWiredSectionTitleMenuItem
 * @classdesc
 * This is the menu item used as the section title for wired connections.
 *
 * By "wired connections", it is used for the "Wired" and "VPN Connections"
 * section of the network status menu.
 *
 * Note that this is the section *title* (e.g. "Wired", "VPN Connections"),
 * not the item for each individual network.
 *
 * ![{@link NMWiredSectionTitleMenuItem} says "Wired". Above: no device set for it. Below: it has set to show its (single) device's status text/toggle](pics/status.network.section.png)
 *
 * Consider the picture above.
 *
 * If there are more than one devices (network interfaces) for this category
 * (e.g. 'wired'.'vpn'),
 * then the item contains just the section title ("Wired") with no switch
 * widget and no status text; instead under the "Wired" section of the popup
 * menu there will be one popup menu section for each wired device
 * ({@link NMWiredDevice}).
 *
 * If however there is only one device for this category (for example most
 * people can only have one wired connection), one can use
 * {@link #updateForDevice}
 * with the relevant device (e.g. {@link NMWiredDevice}), in which case the
 * status item for the device {@link NMDevice#statusItem} is hidden, and the
 * on/off toggle and status text that would have been on the status item
 * is shown on the section title item instead.
 *
 * (One example of how you could have more than one device for a wired interface
 * if you do USB tethering from your mobile phone to your computer, and your
 * computer also has a LAN port so that there are two devices in your "Wired"
 * section).
 * @class
 * @extends PopupMenu.PopupSwitchMenuItem
 */
function NMWiredSectionTitleMenuItem() {
    this._init.apply(this, arguments);
}

NMWiredSectionTitleMenuItem.prototype = {
    __proto__: PopupMenu.PopupSwitchMenuItem.prototype,

    _init: function(label, params) {
        params = params || { };
        params.style_class = 'popup-subtitle-menu-item';
        PopupMenu.PopupSwitchMenuItem.prototype._init.call(this, label, false, params);
    },

    /** Updates this title item to show the status text and toggle item for
     * a particular device, or to not show any at all (i.e. act just as a section
     * title).
     *
     * If `device` is `null`, this item reverts to being essentially a
     * PopupMenuItem (e.g. text "Wired" and no on/off switch or status text).
     * You would use this if there are more than one device for this connection
     * category.
     *
     * If called with a non-null device, this section title acts as
     * that device's status item instead ({@link NMDevice#statusItem}), showing
     * the on/off toggle and status text for that particular device.
     *
     * You should **only** call this with a non-null device if there is
     * only one device for this category (e.g. you only have one wireless
     * card).
     *
     * ![{@link #updateForDevice}](pics/status.network.section.png)
     *
     * In the picture above, the top "Wired" item is not for any particular
     * device, while the bottom "Wired" item has had `updateForDevice` called
     * on it for the Atheros Ethernet card and hence is displaying its status
     * text/toggle.
     * @param {?Network.NMDevice} device - a subclass of {@link NMDevice}
     * that we want this section title to show the toggle/status text  for,
     * or `null` if we don't want to show any status text.
     */
    updateForDevice: function(device) {
        if (device) {
            /** The underlying device for this interface (e.g. a
             * {@link NMWiredDevice}).
             * @type {Network.NMDevice} */
            this._device = device;
            this.setStatus(device.getStatusLabel());
            this.setToggleState(device.connected);
        } else
            this.setStatus('');
    },

    /** This overrides {@link PopupMenu.PopupSwitchMenuItem#activate}, called
     * when the user toggles the on/off switch of the section title.
     *
     * Note that this is only valid when the title is also acting as the device
     * header for a {@link NMDevice} (i.e.
     * {@link #updateForDevice} has been called with
     * non-null device), and this will *only* happen if this
     * network category (e.g. 'wired' or 'vpn') only has one device.
     *
     * First the parent `activate` function is called (to update the toggle).
     *
     * If the switch was toggled on, [`this._device.activate`]{@link NMDevice#activate}
     * is called.
     * Otherwise, [`this._device.deactivate`]{@link NMDevice#deactivate} is
     * called.
     *
     * @see NMDevice#activate
     * @see NMDevice#deactivate
     * @override
     */
    activate: function(event) {
        PopupMenu.PopupSwitchMenuItem.prototype.activate.call(this, event);

        if (!this._device) {
            log('Section title activated when there is more than one device, should be non reactive');
            return;
        }

        let newState = this._switch.state;

        // Immediately reset the switch to false, it will be updated appropriately
        // by state-changed signals in devices (but fixes the VPN not being in sync
        // if the ActiveConnection object is never seen by libnm-glib)
        this._switch.setToggleState(false);

        if (newState)
            this._device.activate();
        else
            this._device.deactivate();
    }
};

/** creates a new NMWirelessSectionTitleMenuItem
 * We add style class `popup-subtitle-menu-item` and call the
 * [parent constructor]{@link PopupMenu.PopupSwitchMenuItem}.
 *
 * We store the NMClient instance (from {@link NMApplet#_client}) as
 * {@link #_client}.
 *
 * We then connect to `client`'s `notify::[property]-enabled` and
 * `notify::[property]-hardware-enabled` signals to know when the user
 * enables or disables the device (e.g. enable/disable wifi) via software
 * (e.g. Fn + F8) or hardware (physical switch on the laptop) respectively.
 *
 * @param {Network.NMConnectionCategory} property - type of the
 * section ('wwan' or 'wireless').
 * @param {string} title - title for the title item ("Wireless", "Mobile Broadband").
 * @classdesc
 * This is the menu item used as the section title for wireless connections.
 *
 * By "wireless connections", it is used for the "Wireless" and
 * "Mobile Broadband" ('wwan') sections of the network status menu.
 *
 * Note that this acts as the *section title* (e.g. "Wireless",
 * "Mobile Broadband"),
 * *not* the item for each individual network.
 *
 * If there are more than one devices (network interfaces) for this category
 * (e.g. you have more than one wireless card),
 * then the item contains just the section title ("Wireless") with no switch
 * widget and no status text; it is essentially reduced to a
 * {@link PopupMenu.PopupMenuItem}.
 *
 * Then under this section title in the {@link NMApplet}'s popup menu,
 * there will be one popup menu item with the name of each wireless
 * card ({@link NMWirelessDevice}) you have (well it shows an on/off toggle
 * or a status text depending on the state of the device), and each of these has its own
 * popup menu subsection listing the available wireless networks for each
 * card.
 *
 * If however there is only one device for this category (for example most
 * people only have one wireless card), {@link NMApplet} uses
 * {@link #updateForDevice} with the relevant
 * {@link NMDevice}, in which case the
 * status item for the device {@link NMDevice#statusItem} is *combined* with
 * this section title. That is, instead of having a section title "Wireless"
 * with a toggle menu item "[name of your wireless card]" and under that a
 * submenu of available networks, the "Wireless" section title will show
 * the toggle and the "[name of your wireless card]" item will be hidden.
 *
 * Use {@link #updateForDevice} to switch between
 * having this section title show the status for a particular device, and
 * having it show no status at all.
 *
 * ![{@link NMWiredSectionTitleMenuItem} says "Wired". Above: no device set for it. Below: it has set to show its (single) device's status text/toggle](pics/status.network.section.png)
 *
 * The above picture demonstrates this (multiple devices: don't show status text.
 * one device: show the device's status in the section title), albeit for
 * a wired section title not a wireless section title..
 * @inheritparams NMApplet
 * @inheritparams PopupMenu.PopupSwitchMenuItem
 * @class
 * @see NMApplet#_makeToggleItem
 * @extends PopupMenu.PopupSwitchMenuItem
 */
function NMWirelessSectionTitleMenuItem() {
    this._init.apply(this, arguments);
}

NMWirelessSectionTitleMenuItem.prototype = {
    __proto__: PopupMenu.PopupSwitchMenuItem.prototype,

    _init: function(client, property, title, params) {
        params = params || { };
        params.style_class = 'popup-subtitle-menu-item';
        PopupMenu.PopupSwitchMenuItem.prototype._init.call(this, title, false, params);

        this._client = client;
        this._property = property + '_enabled';
        this._propertyHardware = property + '_hardware_enabled';
        this._setEnabledFunc = property + '_set_enabled';

        this._client.connect('notify::' + property + '-enabled', Lang.bind(this, this._propertyChanged));
        this._client.connect('notify::' + property + '-hardware-enabled', Lang.bind(this, this._propertyChanged));

        this._propertyChanged();
    },

    /** Updates this title item to show the status text and toggle item for
     * a particular device, or to not show any at all (i.e. act just as a section
     * title).
     *
     * If `device` is `null`, this item reverts to being essentially a
     * PopupMenuItem (e.g. text "Wireless" and no on/off switch or status text).
     * You would use this if there are more than one device for this connection
     * category.
     *
     * If called with a non-null device, this section title acts as
     * that device's status item instead ({@link NMDevice#statusItem}), showing
     * the on/off toggle and status text for that particular device.
     *
     * You should **only** call this with a non-null device if there is
     * only one device for this category (e.g. you only have one wireless
     * card).
     *
     * ![{@link NMWiredSectionTitleMenuItem#updateForDevice}](pics/status.network.section.png)
     *
     * In the picture above, the top "Wired" item is not for any particular
     * device, while the bottom "Wired" item has had `updateForDevice` called
     * on it for the Atheros Ethernet card and hence is displaying its status
     * text/toggle. (I couldn't get a "Wireless" example because I only
     * have one wireless card and don't know how to add another wireless
     * interface to my laptop).
     * @inheritparams NMWiredSectionTitleMenuItem#updateForDevice
     */
    updateForDevice: function(device) {
        // we show the switch
        // - if there not just one device
        // - if the switch is off
        // - if the device is activated or disconnected
        if (device && this._softwareEnabled && this._hardwareEnabled) {
            let text = device.getStatusLabel();
            this.setStatus(text);
        } else
            this.setStatus(null);
    },

    /** Overrides the parent function - this is what happens when the on/off
     * switch is toggled for the section title.
     *
     * Note that this will *only* occur if there is just one device for this
     * section (e.g. just one wireless card for the 'wireless' section), and
     * hence the wireless device's status item {@link NMDeviceWireless#statusItem}
     * has been combined with this section title.
     *
     * First we call {@link PopupMenu.PopupSwitchMenuItem#activate}.
     *
     * Then we activate or deactivate the connection using
     * `this._client.[property]_set_enabled`, where `property` was fed in
     * to the constructor (e.g. 'wireless' or 'wwan').
     * @override
     */
    activate: function(event) {
        PopupMenu.PopupSwitchMenuItem.prototype.activate.call(this, event);

        this._client[this._setEnabledFunc](this._switch.state);
    },

    /** callback when the device is enabled or disabled, by software
     * (e.g. keyboard shortcut) or hardware (physical switch on the laptop).
     *
     * This updates a couple of internal variables and ensures the toggle
     * switch only ON if we are both software and hardware enabled.
     *
     * If we are *not* hardware enabled, the switch is replaced by status
     * text "disabled".
     *
     * In either case, we emit signal 'enabled-changed' to notify any
     * listeners.
     * @fires .enabled-changed
     */
    _propertyChanged: function() {
        this._softwareEnabled = this._client[this._property];
        this._hardwareEnabled = this._client[this._propertyHardware];

        let enabled = this._softwareEnabled && this._hardwareEnabled;
        this.setToggleState(enabled);
        if (!this._hardwareEnabled)
            /* Translators: this indicates that wireless or wwan is disabled by hardware killswitch */
            this.setStatus(_("disabled"));

        this.emit('enabled-changed', enabled);
    }
};
/** Emitted whenever wireless (wwan or wifi) is software- or hardware-
 * enabled or disabled.
 * @param {Network.NMWirelessSectionTitleMenuItem} item - item emitting
 * the signal.
 * @param {boolean} enabled - whether wireless is enabled (it is enabled
 * if it is *both* hardware and software enabled).
 * @name enabled-changed
 * @event
 * @memberof NMWirelessSectionTitleMenuItem
 */

/** creates a new NMDevice
 * A NMDevice is a javascript wrapper for the
 * {@link NMClient.Device}.
 *
 * The input `device` is the NMClient.Device we will wrap around. It is
 * stored in {@link #device}.
 *
 * We then filter `connections` to only get those relevant to `device`
 * (see {@link #connectionValid}). It may help to think of `connections`
 *  as the list of available wireless networks for the wireless device.
 *
 * If a connection is valid for this device, we create a {@link ConnectionData}
 * for it and add it to {@link #_connections}. The {@link ConnectionData}
 * stores the underlying NMClient.RemoteConnection, as well as its UUID,
 * name, and time of last successful connection.
 *
 * We sort {@link #_connections} from most recent successful connection to
 * last.
 *
 * Then we create the UI side of `device`, i.e. its popup menu items for the
 * network status item menu:
 *
 * First we make a {@link PopupMenu.PopupSwitchMenuItem} for the device
 * with the toggle state set to whether the connection is active or not.
 * This is stored in {@link #statusItem}.
 *
 * The label for the item is based off the device's vendor and product
 * name (e.g. "Atheros AR8132 Fast Ethernet").
 * It will display either a toggle to let the user
 * activate/deactivate this device, or a text with the current status
 * of the device (e.g. "connecting...").
 *
 * **Note**: if there is only one device for a particular
 * modality (for example you only have one wired interface, or only one
 * wireless device), {@link #statusItem} is hidden by the {@link NMApplet}.
 * This is the usual case.
 *
 * ![Example where there is more than one device for a particular section (above), and only one device for a particular section (below).](pics/status.network.section.png)
 *
 * The above picture is an example of where there is more than one device
 * for a particular section (above) and only one device for that section (below).
 * Note how in the latter case the status text for my (single) wired interface
 * is now shown in the "Wired" section title instead of in its own
 * "Atheros AR8132 Fast Ethernet" item like in the top case.
 * 
 * We then create a {@link PopupMenu.PopupMenuSection}, stored in
 * {@link #section}, that holds a popup menu item for each available connection
 * (e.g. the {@link NMDeviceWireless} creates a {@link NMNetworkMenuItem}
 * for each available wireless network).
 * @see #_createSection
 * @param {NMClient.Client} client - the NMClient.Client instance for the device ({@link NMApplet#_client}).
 * @param {NMClient.Device} device -
 * the device that this is a wrapper for. As long as it inherits NMClient.Device
 * it is valid; see [NMClient.Device's object hierarchy](http://developer.gnome.org/libnm-glib/unstable/NMDevice.html#NMDevice.object-hierarchy)
 * for examples of devices that this could be (and we have JS
 * wrappers for most of these).
 * @property {NMClient.RemoteConnection[]} connections - array of remote connections,
 * {@link NMApplet#_connections}. We will filter these to only store the
 * ones relevant to this device.
 * @classdesc
 * A NMDevice represents a
 * {@link NMClient.Device}.
 *
 * A network device is like a network interface (e.g. eth0) - one per wireless
 * card and so on.
 *
 * A {@link NMDevice} is also responsible for creating and managing all the
 * popup menu items for that device in the network status icon's popup
 * menu.
 *
 * This consists of:
 *
 * * a "title" PopupSwitchMenuItem for the device, showing the device name
 * (like "Broadcom NetXtreme BCM5751 Gigabit Ethernet Express"). It will
 * either show the on/off toggle, or the current status of the item
 * (like "connecting...", or "cable unplugged"). Toggling the switch causes
 * the device to activate. This is stored as {@link #statusItem}.
 * * a PopupMenuSection displaying all the available connections for this
 * device, for example all available wireless networks for a {@link NMDeviceWired}.
 * This is stored in {@link #section}. Clicking on one of these items
 * will attempt to connect to that network.
 *
 * The {@link NMDevice} is the base class; it must be further subclassed to
 * be of use. See:
 *
 * * {@link NMDeviceWired}
 * * {@link NMDeviceModem}
 * * {@link NMDeviceBluetooth}
 * * {@link NMDeviceVPN}
 * * {@link NMDeviceWireless}
 * * {@link NMDeviceWired}
 *
 * The various underlying devices all subclass NMClient.Device; see
 * [NMClient.Device's object hierarchy](http://developer.gnome.org/libnm-glib/unstable/NMDevice.html#NMDevice.object-hierarchy).
 *
 * A device can either have a list of available connections that one may choose
 * from, like wireless, OR it supports automatic connections, like wired
 * networks where you can't choose which network to connect to, you just connect
 * to the network whose cable you just plugged in.
 *
 * If the device supports automatic connections, then the subclass
 * **must**:
 
 * * set {@link #_autoConnectionName} to a string, used in the popup menu
 * item for this device such that clicking on it will perform the automatic
 * connection;
 * * define {@link #_createAutomaticConnection} which should return a connection
 * that the NMClient can activate and connect to. Clicking on the popup menu
 * item for the auto-connect option will call this function to connect.
 *
* Otherwise, the list of available connections is stored in {@link #_connections};
* you should add connections to this list with {@link #addConnection} and
* remove them with {@link #removeConnection}.
 * @TODO mark as virtual/abstract?
 * @class
 * @see NMClient.Device
 */
function NMDevice() {
    throw new TypeError('Instantanting abstract class NMDevice');
}

NMDevice.prototype = {
    _init: function(client, device, connections) {
        /** The underlying NMClient.Device that this wraps around.
         * @type {NMClient.Device}
         */
        this.device = device;
        if (device) {
            this.device._delegate = this;
            this._stateChangedId = this.device.connect('state-changed', Lang.bind(this, this._deviceStateChanged));
        } else
            this._stateChangedId = 0;

        /** The NM client
         * @type {NMClient.Client}
         */
        // protected
        this._client = client;
        /** Array of connections and info (name, uuid, timestamp of last
         * successful connection) relevant to
         * this device.
         * @type {[Network.ConnectionData][]}
         */
        this._connections = [ ];
        for (let i = 0; i < connections.length; i++) {
            if (!connections[i]._uuid)
                continue;
            if (!this.connectionValid(connections[i]))
                continue;
            // record the connection
            let obj = {
                connection: connections[i],
                name: connections[i]._name,
                uuid: connections[i]._uuid,
                timestamp: connections[i]._timestamp,
            };
            this._connections.push(obj);
        }
        this._connections.sort(this._connectionSortFunction);

        /** The active connection for this device, if any (i.e. the network
         * we are currently connected to).
         * @type {?NMClient.ActiveConnection}
         */
        this._activeConnection = null;
        /** If this device is currently connected to a network, this is
         * the PopupMenuItem show in in the menu for this connection
         * (it typically has {@link PopupMenu.PopupMenuItem#setShowDot}
         * set to `true` to indicate that the connection is active).
         * @type {PopupMenu.PopupMenuItem}
         */
        this._activeConnectionItem = null;
        /** If {@link #_autoConnectionName} is not `null` (i.e this
         * device has auto-connecting), the popup menu item for the
         * auto-connect option appears here.
         * @type {PopupMenu.PopupMenuItem}
         */
        this._autoConnectionItem = null;
        /** If there are more than {@link NUM_VISIBLE_NETWORKS} networks in the
         * popup menu section for this device, we add a "More..." submenu
         * and add subsequent networks to the submenu.
         *
         * The submenu is stored here.
         * @type {PopupMenu.PopupMenuSection} */
        this._overflowItem = null;

        if (this.device) {
            /** The on/off toggle popup menu item to be displayed
             * in the network indicator's popup menu for this device.
             *
             * Toggling it on/off calls {@link #activate}/{@link #deactivate}
             * and emits ['enabled-changed']{@link .enabled-changed}.
             * @type {PopupMenu.PopupSwitchMenuItem} */
            this.statusItem = new PopupMenu.PopupSwitchMenuItem(this._getDescription(), this.connected, { style_class: 'popup-subtitle-menu-item' });
            this._statusChanged = this.statusItem.connect('toggled', Lang.bind(this, function(item, state) {
                if (state)
                    this.activate();
                else
                    this.deactivate();
                this.emit('enabled-changed');
            }));

            this._updateStatusItem();
        }
        /** The PopupMenuSection for this device (???)
         * @type {PopupMenu.PopupMenuSection}
         */
        this.section = new PopupMenu.PopupMenuSection();

        this._createSection();

        // other properties not assigned here but they should be part of this
        // class.

        // this._autoConnectionName
        /** If this is not `null`, then this device supports automatic
         * connections (for example, {@link NMDeviceWired} does) rather
         * than having a list of possible connections to choose from
         * in {@link #_connections}.
         *
         * This string is used to make a popup menu item with this as the label,
         * and clicking on it will connect using the automatic connection.
         *
         * If this is set, you **must** implement
         * {@link #_createAutomaticConnection} which should return a
         * connection for the device to connect to.
         * @member
         * @name _autoConnectionName
         * @type {?string}
         * @default null
         */
        // this.category
        /** The category for this device - WIRED, WIRELESS, WWAN, VPN.
         * @type {Network.NMConnectionCategory}
         */
    },

    /** Destroys this device.
     *
     * Disconnects any signals we were listening to, clears our
     * popup menu section ({@link #_clearSection}), and destroys our
     * toggle item {@link #statusItem} if any.
     */
    destroy: function() {
        if (this.device)
            this.device._delegate = null;

        if (this._stateChangedId) {
            // Need to go through GObject.Object.prototype because
            // nm_device_disconnect conflicts with g_signal_disconnect
            GObject.Object.prototype.disconnect.call(this.device, this._stateChangedId);
            this._stateChangedId = 0;
        }
        if (this._carrierChangedId) {
            // see above for why this is needed
            GObject.Object.prototype.disconnect.call(this.device, this._carrierChangedId);
            this._carrierChangedId = 0;
        }
        if (this._firmwareChangedId) {
            GObject.Object.prototype.disconnect.call(this.device, this._firmwareChangedId);
            this._firmwareChangedId = 0;
        }

        this._clearSection();
        if (this.statusItem)
            this.statusItem.destroy();
        this.section.destroy();
    },

    /** Deactiviates the device.
     *
     * Calls[`nm_device_disconnect`](http://developer.gnome.org/libnm-glib/unstable/NMDevice.html#nm-device-disconnect)
     * on {@link #device} (which disconnects the device if currently connected,
     * and prevents it from automatically connecting to networks until the
     * next manual network connection request).
     */
    deactivate: function() {
        this.device.disconnect(null);
    },

    /** Activates the device.
     *
     * We pick the most recently used connection from {@link #_connections}
     * and connect to that one.
     *
     * If no connections are set and the device supports automatic
     * connections, we create one ({@link #_createAutomaticConnection}) and
     * connect to that.
     *
     * Calls [`nm_client_activate_connection`](http://developer.gnome.org/libnm-glib/unstable/NMClient.html#nm-client-activate-connection)
     * on {@link #_client} with the appropriate connection.
     */
    activate: function() {
        if (this._activeConnection)
            // nothing to do
            return;

        // pick the most recently used connection and connect to that
        // or if no connections ever set, create an automatic one
        if (this._connections.length > 0) {
            this._client.activate_connection(this._connections[0].connection, this.device, null, null);
        } else if (this._autoConnectionName) {
            let connection = this._createAutomaticConnection();
            if (connection)
                this._client.add_and_activate_connection(connection, this.device, null, null);
        }
    },

    /** returns whether this device is currently connected.
     * @returns {boolean} whether the device is connected.
     */
    get connected() {
        return this.device.state == NetworkManager.DeviceState.ACTIVATED;
    },

    /** Sets the active connection for this device.
     *
     * It's stored in {@link #_activeConnection}.
     *
     * We then regenerate our popup menu section displaying the list of
     * connections using {@link #_createSection},
     * ensuring that the active connection is on top of the menu.
     *
     * If this is set, then {@link #activate} does nothing as the connection
     * is already activated.
     * @param {?NMClient.ActiveConnection} activeConnection - the active connection.
     */
    setActiveConnection: function(activeConnection) {
        if (activeConnection == this._activeConnection)
            // nothing to do
            return;

        // remove any UI
        if (this._activeConnectionItem) {
            this._activeConnectionItem.destroy();
            this._activeConnectionItem = null;
        }

        this._activeConnection = activeConnection;

        this._clearSection();
        this._createSection();
    },

    /** Checks `connection`, either:
     *
     * * removing it if it exists for this device (is in {@link #_connections})
     * but is no longer valid for the device ({@link #removeConnection});
     * * adding it if it we don't know about it yet and it *is* valid for
     * this device ({@link #addConnection});
     * * otherwise, do nothing.
     * @property {NMClient.RemoteConnection} connection - a NMClient.RemoteConnection
     */
    checkConnection: function(connection) {
        let exists = this._findConnection(connection._uuid) != -1;
        let valid = this.connectionValid(connection);
        if (exists && !valid)
            this.removeConnection(connection);
        else if (!exists && valid)
            this.addConnection(connection);
    },

    /** Adds a connection to our list of stored connections.
     *
     * We create a {@link ConnectionData} for `connection` and store it
     * in {@link #_connections}, re-sorting so that it is in order
     * of most recently connected to least recently connected.
     *
     * We then regenerate our popup menu section displaying the list of
     * connections using {@link #_createSection},
     * so that this connection has its popup menu item added.
     * @inheritparams #checkConnection
     */
    addConnection: function(connection) {
        // record the connection
        let obj = {
            connection: connection,
            name: connection._name,
            uuid: connection._uuid,
            timestamp: connection._timestamp,
        };
        this._connections.push(obj);
        this._connections.sort(this._connectionSortFunction);

        this._clearSection();
        this._createSection();
    },

    /** Removes a connection from our list of stored connections.
     *
     * We remove it from {@link #_connections}.
     *
     * We then regenerate our popup menu section displaying the list of
     * connections using {@link #_createSection},
     * so that this connection has its popup menu item removed.
     *
     * @inheritparams #checkConnection
     */
    removeConnection: function(connection) {
        if (!connection._uuid) {
            log('Cannot remove a connection without an UUID');
            return;
        }
        let pos = this._findConnection(connection._uuid);
        if (pos == -1) {
            // this connection was never added, nothing to do here
            return;
        }

        let obj = this._connections[pos];
        if (obj.item)
            obj.item.destroy();
        this._connections.splice(pos, 1);

        if (this._connections.length <= 1) {
            // We need to show the automatic connection again
            // (or in the case of NMDeviceWired, we want to hide
            // the only explicit connection)
            this._clearSection();
            this._createSection();
        }
    },

    /** Returns whether a particular connection is valid for this device,
     * that is, whether the connection may be activated with this
     * device.
     *
     * For example, if {@link #device} is a WiFi device that supports only
     * WEP encryption, the connection will only be valid if it is a WiFi
     * connection which describes a WEP or open network, and will not be valid
     * if it is a WPA network, or if it is an Ethernet, Bluetooth, WWAN, etc
     * connection that is incompatible with the device.
     *
     * @inheritparams #checkConnection
     * @returns {boolean} whether a particular connection is valid for this device
     */
    connectionValid: function(connection) {
        return this.device.connection_valid(connection);
    },

    /** Sorts two connections. This is used in `Array.sort` to sort
     * {@link #_connections} from most recently to least recently connected.
     *
     * @param {Network.ConnectionData} one - the first connection to compare
     * @param {Network.ConnectionData} two - the second connection to compare
     * @returns {number} `two.timestamp - one.timestamp`
     */
    _connectionSortFunction: function(one, two) {
        if (one.timestamp == two.timestamp)
            return GLib.utf8_collate(one.name, two.name);

        return two.timestamp - one.timestamp;
    },

    /** Sets whether this device is enabled.
     *
     * I'm not entire sure how "enabled" differs from "active connection", but
     * I think a device is enabled if it *can* be used, as opposed to whether
     * it is actually being used. For example, a wireless device would only
     * be enabled if it is both software- and hardware- enabled.
     *
     * This function does nothing by default.
     *
     * For example, the {@link NMDeviceWireless} will hide both the on/off
     * status item and the popup menu section if wireless is not enabled
     * (because if wireless is hardware or software disabled then we won't
     * be able to connect to any networks, so don't even show the device).
     * @param {boolean} enabled
     */
    setEnabled: function(enabled) {
        // do nothing by default, we want to keep the conneciton list visible
        // in the majority of cases (wired, wwan, vpn)
    },

    /** Gets a string describing the current status of this device.
     *
     * This is typically displayed on the right hand side of the popup menu
     * item for the device ({@link #statusItem}) in place of the toggle (the
     * toggle is usually shown if the device is activated so that it may be
     * disconnected, or if the device is disconnected).
     *
     *
     * For the {@link NMDevice} version, we return (always translated, in
     * the case of strings):
     *
     * * `null`, if the device is disconnected or activated. This is because
     * `getStatusLabel` is usually called as the argument of
     * {@link PopupMenu.PopupSwitchMenuItem#setStatus}, and returning `null`
     * will show the toggle of the item (whereas returning a string replaces
     * the toggle with the status text). The toggle is shown when the device
     * is disconnected so that the user can easily re-activate the device, and
     * it is shown when the device is activated so that the user can easily
     * disconnect.
     * * "unmanaged" if the device is physically present but not
     * under NetworkManager's control;
     * * "disconnecting..." if the device is currently deactivating.
     * * "connecting..." if the device is in the various stages of activating
     * itself and connecting;
     * * "authentication required" if it needs authentication (e.g. password);
     * * "firmware missing" if the device is unavailable due to missing firmware
     * or kernel module;
     * * "cable unplugged" if it's a wired network device that is physically
     * disconnected;
     * * "unavailable" if the device cannot be activated (for example it is
     * disabled by `rfkill` or has no coverage);
     * * "connection failed" if the connection failed (for example the password
     * wasn't right);
     * * 'invalid' otherwise (this should never be returned!)
     *
     * @returns {?string} a string describing the current status of the device,
     * for example "disconnecting...", "connecting...",
     * "authentication required", or `null` if the device is disconnected
     * or activated. See the [function description]{@link #getStatusLabel}
     * for further details.
     */
    getStatusLabel: function() {
        switch(this.device.state) {
        case NetworkManager.DeviceState.DISCONNECTED:
        case NetworkManager.DeviceState.ACTIVATED:
            return null;
        case NetworkManager.DeviceState.UNMANAGED:
            /* Translators: this is for network devices that are physically present but are not
               under NetworkManager's control (and thus cannot be used in the menu) */
            return _("unmanaged");
        case NetworkManager.DeviceState.DEACTIVATING:
            return _("disconnecting...");
        case NetworkManager.DeviceState.PREPARE:
        case NetworkManager.DeviceState.CONFIG:
        case NetworkManager.DeviceState.IP_CONFIG:
        case NetworkManager.DeviceState.IP_CHECK:
        case NetworkManager.DeviceState.SECONDARIES:
            return _("connecting...");
        case NetworkManager.DeviceState.NEED_AUTH:
            /* Translators: this is for network connections that require some kind of key or password */
            return _("authentication required");
        case NetworkManager.DeviceState.UNAVAILABLE:
            // This state is actually a compound of various states (generically unavailable,
            // firmware missing, carrier not available), that are exposed by different properties
            // (whose state may or may not updated when we receive state-changed).
            if (!this._firmwareMissingId)
                this._firmwareMissingId = this.device.connect('notify::firmware-missing', Lang.bind(this, this._substateChanged));
            if (this.device.firmware_missing) {
                /* Translators: this is for devices that require some kind of firmware or kernel
                   module, which is missing */
                return _("firmware missing");
            }
            if (this.device.capabilities & NetworkManager.DeviceCapabilities.CARRIER_DETECT) {
                if (!this._carrierChangedId)
                    this._carrierChangedId = this.device.connect('notify::carrier', Lang.bind(this, this._substateChanged));
                if (!this.carrier) {
                    /* Translators: this is for wired network devices that are physically disconnected */
                    return _("cable unplugged");
                }
            }
            /* Translators: this is for a network device that cannot be activated (for example it
               is disabled by rfkill, or it has no coverage */
            return _("unavailable");
        case NetworkManager.DeviceState.FAILED:
            return _("connection failed");
        default:
            log('Device state invalid, is %d'.format(this.device.state));
            return 'invalid';
        }
    },

    /** Creates an automatic connection.
     * 
     * Called when {@link #activate} is called to activate the device but
     * there are no connections to connect to.
     * @see NMDeviceWired#_createAutomaticConnection
     * @returns {NetworkManager.Connection} a connection to be fed into
     * [`nm_client_add_and_activate_connection`](http://developer.gnome.org/libnm-glib/unstable/NMClient.html#nm-client-add-and-activate-connection)
     */
    // protected
    _createAutomaticConnection: function() {
        throw new TypeError('Invoking pure virtual function NMDevice.createAutomaticConnection');
    },

    /** Locates the index of the connection identified by `uuid` in
     * {@link #_connections}.
     * @param {string} uuid - UUID of the connection to search for.
     * @returns {index} index of the connection specified by UUID, or -1
     * if it is not our connection/not found.
     */
    _findConnection: function(uuid) {
        for (let i = 0; i < this._connections.length; i++) {
            let obj = this._connections[i];
            if (obj.uuid == uuid)
                return i;
        }
        return -1;
    },

    /** Clears our popup menu section {@link #section}.
     *
     * Recall that the section contains a popup menu item for each available
     * connection/network for this device. */
    _clearSection: function() {
        // Clear everything
        this.section.removeAll();
        this._autoConnectionItem = null;
        this._activeConnectionItem = null;
        this._overflowItem = null;
        for (let i = 0; i < this._connections.length; i++) {
            this._connections[i].item = null;
        }
    },

    /** Returns whether we should show the list of available connections.
     *
     * We always show it unless the device is unavailable or unmanaged.
     * @returns {boolean} `false` if the device is unavailable or unmanaged;
     * `true` otherwise.
     */
    _shouldShowConnectionList: function() {
        return (this.device.state >= NetworkManager.DeviceState.DISCONNECTED);
    },

    /** Populates our popup menu section {@link #section} with a set of
     * {@link PopupMenu.PopupMenuItem}s for each connection available to this
     * device.
     *
     * ![{@link NMDeviceWireless#section}](pics/NMDeviceWireless.section.png)
     *
     * The above picture shows an example of {@link NMDeviceWireless#section}.
     *
     * If the device is unavailable or unmanaged we return immediately, leaving
     * the section empty ({#link #_shouldShowConnectionList}).
     *
     * Otherwise if we have a currently-active connection, we create an
     * item for it with {@link #_createActiveConnectionItem} and add it.
     * This is typically a non-reactive text popup menu item with the active
     * connection's name on it and a dot to the left of it to show it is
     * active. This is the top item in the picture above.
     *
     * ![{@link NMDeviceWireless}](pics/NMDeviceWireless.png)
     *
     * Then we add a popup menu item for each other connection in the list
     * ({@link #_createConnectionItem}).
     * However if there are more than {@link NUM_VISIBLE_NETWORKS} already in
     * the list, we add a "More..." submenu and add subsequent items to this
     * submenu. The "More..." submenu item is stored as {@link #_overflowItem}.
     * The picture above shows an example of the "More..." item.
     *
     * Otherwise if there are no connections in {@link #_connections} but
     * the device supports automatic connections (for example a wired device
     * will automatically connects to whatever network it is plugged into),
     * an item is added for that and stored in {@link #_autoConnectionItem}.
     * The test for whether a device supports automatic connections is that
     * {@link #_autoConnectionName} is populated (in which case
     * {@link #_createAutomaticConnection} *must* be implemented, as clicking
     * on the item will call this function in order to make the connection).
     */
    _createSection: function() {
        if (!this._shouldShowConnectionList())
            return;

        if (this._activeConnection) {
            this._createActiveConnectionItem();
            this.section.addMenuItem(this._activeConnectionItem);
        }
        if (this._connections.length > 0) {
            let activeOffset = this._activeConnectionItem ? 1 : 0;

            for(let j = 0; j < this._connections.length; ++j) {
                let obj = this._connections[j];
                if (this._activeConnection &&
                    obj.connection == this._activeConnection._connection)
                    continue;
                obj.item = this._createConnectionItem(obj);

                if (j + activeOffset >= NUM_VISIBLE_NETWORKS) {
                    if (!this._overflowItem) {
                        this._overflowItem = new PopupMenu.PopupSubMenuMenuItem(_("More..."));
                        this.section.addMenuItem(this._overflowItem);
                    }
                    this._overflowItem.menu.addMenuItem(obj.item);
                } else
                    this.section.addMenuItem(obj.item);
            }
        } else if (this._autoConnectionName) {
            this._autoConnectionItem = new PopupMenu.PopupMenuItem(this._autoConnectionName);
            this._autoConnectionItem.connect('activate', Lang.bind(this, function() {
                let connection = this._createAutomaticConnection();
                if (connection)
                    this._client.add_and_activate_connection(connection, this.device, null, null);
            }));
            this.section.addMenuItem(this._autoConnectionItem);
        }
    },

    /** Creates a popup menu item for an available connection for this
     * device (for example available networks for a wireless device) such
     * that clicking on the item will activate that connection with this
     * device.
     *
     * For example the {@link NMDeviceWireless} shows a {@link NMNetworkMenuItem}
     * for each available wireless network and clicking on it will cause
     * us to connect to that network.
     *
     * This default implementation creates a {@link PopupMenu.PopupMenuItem}
     * with the human-readable name of the connection.
     * @param {Network.ConnectionData} obj - object describing the connection
     * we are to make the item for.
     * @returns {PopupMenu.PopupMenuItem} a popup menu item with `obj.name`
     * as the text (the connection's human-readable name). It is already
     * connected up such that clicking on it will activate the connection.
     * This uses [`nm_client_activate_connection`](http://developer.gnome.org/libnm-glib/unstable/NMClient.html#nm-client-activate-connection).
     */
    _createConnectionItem: function(obj) {
        let connection = obj.connection;
        let item = new PopupMenu.PopupMenuItem(obj.name);

        item.connect('activate', Lang.bind(this, function() {
            this._client.activate_connection(connection, this.device, null, null);
        }));
        return item;
    },

    /** Creates a popup menu item for the current active connection for this
     * device, if there is one ({@link #_activeConnection}).
     *
     * By default it's a non-reactive text popup menu item showing
     * the connection's name, and with the dot on the left-hand side showing.
     *
     * ![Example of an active connection item](pics/status.network.activeConnectionItem.png)
     *
     * The picture above shows an example of an active connection item for
     * the {@link NMDeviceWireless} (which is a bit more than a simple text popup
     * menu item, but illustrates the purpose - note the dot on the left
     * hand side).
     */
    _createActiveConnectionItem: function() {
        let title;
        let active = this._activeConnection._connection;
        if (active) {
            title = active._name;
        } else {
            /* TRANSLATORS: this is the indication that a connection for another logged in user is active,
               and we cannot access its settings (including the name) */
            title = _("Connected (private)");
        }
        this._activeConnectionItem = new PopupMenu.PopupMenuItem(title, { reactive: false });
        this._activeConnectionItem.setShowDot(true);
    },

    /** Callback when {@link #device} emits a ['state-changed'](http://developer.gnome.org/libnm-glib/unstable/NMDevice.html#NMDevice-state-changed) signal.
     *
     * This happens when the device's state (connecting, activated, deactivated,
     * ...) changes.
     *
     * We update the status item for the device {@link #statusItem} (the item
     * that shows the device name) using {@link #_updateStatusItem}.
     * This ensures that the on/off toggle shows if the device is activated
     * or disconnected, and that a status text shows otherwise.
     *
     * We also regenerate our list of available connections {@link #section} -
     * we don't show the section at all if the device is now UNAVAILABLE or
     * UNMANAGED, and the ordering of networks in our list may change (we
     * show the active connection first, if any, and then order by most
     * recently connected). See {@link #_createSection}.
     *
     * If we've changed from ACTIVATED to anything else (i.e. have for any
     * reason disconnected), we emit {@link .event:network-lost}.
     *
     * If we've changed to a FAILED state, we emit {@link .event:activation-failed}
     * with the reason of failure.
     *
     * @param {NMClient.Device} device - the device whose state changed
     * ({@link #device}).
     * @param {NetworkManager.DeviceState} newstate - the new state of the
     * device (ACTIVATED, FAILED, ...)
     * @param {NetworkManager.DeviceState} oldstate - the previous state
     * of the device.
     * @param {NetworkManager.DeviceStateReason} reason - the reason for the
     * change (e.g. MODEM_BUSY, GSM_SIM_PIN_REQURED, SUPPLICANT_DISCONNECT,
     * ...)
     * @fires .network-lost
     * @fires .activation-failed
     * @fires .state-changed
     * @see {@link http://developer.gnome.org/libnm-glib/unstable/NMDevice.html#NMDevice-state-changed|documentation for NMClient.Device's 'state-changed' signal}
     */
    _deviceStateChanged: function(device, newstate, oldstate, reason) {
        if (newstate == oldstate) {
            log('device emitted state-changed without actually changing state');
            return;
        }

        if (oldstate == NetworkManager.DeviceState.ACTIVATED) {
            this.emit('network-lost');
        }

        if (newstate == NetworkManager.DeviceState.FAILED) {
            this.emit('activation-failed', reason);
        }

        this._updateStatusItem();

        this._clearSection();
        this._createSection();
        this.emit('state-changed');
    },

    /** Updates {@link #statusItem} to reflect the current state of the device.
     *
     * We call {@link PopupMenu.PopupSwitchMenuItem#setStatus} with
     * {@link #getStatusLabel}. This will show the on/off toggle if the
     * device is disconnected or activated, and otherwise shows a status
     * text instead of the toggle (e.g. "disconnecting...").
     */
    _updateStatusItem: function() {
        if (this._carrierChangedId) {
            // see above for why this is needed
            GObject.Object.prototype.disconnect.call(this.device, this._carrierChangedId);
            this._carrierChangedId = 0;
        }
        if (this._firmwareChangedId) {
            GObject.Object.prototype.disconnect.call(this.device, this._firmwareChangedId);
            this._firmwareChangedId = 0;
        }

        this.statusItem.setStatus(this.getStatusLabel());
        this.statusItem.setToggleState(this.connected);
    },

    /** Callback if the firmware or carrier of the device changes.
     *
     * This emits the 'state-changed' signal and updates the status of
     * our status item {@link #statusItem} with {@link #getStatusLabel}.
     *
     * These both come under state "UNAVAILABLE" but {@link #getStatusLabel}
     * distinguishes multiple substates under this:
     *
     * * if the state is UNAVAILABLE because the firmware for the device
     * is missing, we get status "firmware missing";
     * * if the state is UNAVAILABLE because there is no carrier, we
     * use status "cable unplugged";
     * * if the state is UNAVAILABLE for neither of these reasons, we use
     * status "unavailable".
     *
     * @fires .state-changed
     */
    _substateChanged: function() {
        this.statusItem.setStatus(this.getStatusLabel());

        this.emit('state-changed');
    },

    /** Gets the human-readable name for this device.
     *
     * We get the device's product and vendor name, try to shorten them
     * a bit with {@link Util.fixupPCIDescription}, and join them together.
     *
     * For example TODO TODO
     */
    _getDescription: function() {
        let dev_product = this.device.get_product();
        let dev_vendor = this.device.get_vendor();
        if (!dev_product || !dev_vendor)
	    return '';

        let product = Util.fixupPCIDescription(dev_product);
        let vendor = Util.fixupPCIDescription(dev_vendor);
        let out = '';

        // Another quick hack; if all of the fixed up vendor string
        // is found in product, ignore the vendor.
        if (product.indexOf(vendor) == -1)
            out += vendor + ' ';
        out += product;

        return out;
    }
};
Signals.addSignalMethods(NMDevice.prototype);
/** Emitted whenever the user enables/disables the device (by the toggle
 * switch next to the device name).
 * @event
 * @name enabled-changed
 * @param {Network.NMDevice} device - device that emitted the signal.
 * @memberof NMDevice
 */
/** Emitted when the device's state changes. Use `device.device.state`
 * to query the new state (ACTIVATED, DISCONNECTED, ... - a NetworkManager.DeviceState)
 * @event
 * @param {Network.NMDevice} device - device that emitted the signal.
 * @name state-changed
 * @memberof NMDevice
 */
/** Emitted when connecting to a network with the device fails. Emitted
 * with the reason for failure.
 * @event
 * @name activation-failed
 * @param {Network.NMDevice} device - device that emitted the signal.
 * @param {NetworkManager.DeviceStateReason} reason - the reason for the
 * change (e.g. MODEM_BUSY, GSM_SIM_PIN_REQURED, SUPPLICANT_DISCONNECT,
 * ...)
 * @memberof NMDevice
 */
/** Emitted when the device's state changes from ACTIVATED to anything else.
 * @event
 * @param {Network.NMDevice} device - device that emitted the signal.
 * @name network-lost
 * @memberof NMDevice
 */

/** creates a new NMDeviceWired.
 *
 * We set {@link #_autoConnectionName} to "Auto Ethernet" and call
 * the [parent constructor]{@link NMDevice}.
 *
 * We also set {@link #category} to NMConnectionCategory.WIRED.
 * @inheritparams NMDevice
 * @classdesc
 * This is a subclass of {@link NMDevice} for wired connections.
 *
 * ![Two {@link NMDeviceWired} under the "Wired" section heading](pics/NMDeviceWired.png)
 *
 * The above picture shows *two* {@link #statusItem}s, one for
 * "HTC Android Phone" and another for "Atheros AR8132 Fast Ethernet" (the
 * "Wired" section title up top is a {@link NMWiredSectionTitleMenuItem}).
 *
 * The {@link NMDeviceWired} has automatic connection capability, so if there
 * are no available connections for the device, its popup menu will show
 * an item "Auto Ethernet" which will perform the automatic connection.
 *
 * As such we set {@link #_autoConnectionName} to "Auto Ethernet" and
 * implement {@link #_createAutomaticConnection} to return a new
 * NetworkManager.Connection for the wired connection.
 *
 * Also, we override {@link #_createSection} so that if we only have one
 * wired connection available to us (normal or automatic), as is common for
 * a wired device, then the submenu of available connections {@link #section}
 * is hidden.
 * @class
 * @extends NMDevice
 * @see {@link NMClient.DeviceEthernet}
 */
function NMDeviceWired() {
    this._init.apply(this, arguments);
}

NMDeviceWired.prototype = {
    __proto__: NMDevice.prototype,

    _init: function(client, device, connections) {
        this._autoConnectionName = _("Auto Ethernet");
        this.category = NMConnectionCategory.WIRED;

        NMDevice.prototype._init.call(this, client, device, connections);
    },

    /** Extends the parent function creating the PopupMenu.PopupMenuSection
     * showing the list of available connections for this device.
     *
     * If there is only one connection available to us (normal or automatic)
     * as is common with a wired interface (you can only connect to the
     * network your computer is physically plugged into), we hide the section
     * completely.
     * @override */
    _createSection: function() {
        NMDevice.prototype._createSection.call(this);

        // if we have only one connection (normal or automatic)
        // we hide the connection list, and use the switch to control
        // the device
        // we can do it here because addConnection and removeConnection
        // both call _createSection at some point
        if (this._connections.length <= 1)
            this.section.actor.hide();
        else
            this.section.actor.show();
    },

    /** Implements the parent function to return a NetworkManager.Connection
     * corresponding to an automatic wireless connection.
     *
     * We create a new {@link NetworkManager.Connection},
     * add a new (default) {@link NetworkManager.SettingWired} to it,
     * and then add a {@link NetworkManager.SettingConnection}
     * to it, setting its UUID to a new randomly-generated one, its
     * ID to "Auto Ethernet" ({@link #_autoConnectionName}), and setting
     * its `autoconnect` property to `true`.
     * @override */
    _createAutomaticConnection: function() {
        let connection = new NetworkManager.Connection();
        connection._uuid = NetworkManager.utils_uuid_generate();
        connection.add_setting(new NetworkManager.SettingWired());
        connection.add_setting(new NetworkManager.SettingConnection({
            uuid: connection._uuid,
            id: this._autoConnectionName,
            type: NetworkManager.SETTING_WIRED_SETTING_NAME,
            autoconnect: true
        }));
        return connection;
    }
};

/** creates a new NMDeviceModem
 *
 * This is a NMDevice for a modem, be it for a wired or mobile broadband
 * connection.
 *
 * We set {@link #category} to either `NMConnectionCategory.WWAN` (mobile
 * broadband modem) or `NMConnectionCategory.WIRED` (wired modem).
 *
 * This device supports auto-connect; {@link #_autoConnectionName} is
 * either set to "Auto dial-up" or "Auto broadband" depending on whether
 * the device's category is wired or wwan.
 *
 * If the device is for mobile broadband (WWAN), we set
 * {@link #mobileDevice} to either a {@link ModemManager.ModemGsm}
 * or {@link ModemManager.ModemCdma} and use this to query signal
 * strength and so on.
 *
 * If it's for mobile broadband, we also connect to
 * {@link ModemManager.ModemGsm.notify::operator-name} or
 * {@link ModemManager.ModemCdma.notify::operator-name} to ensure that our
 * popup menu item shows the correct operator name (or is hidden if there
 * is no operator). Additionally,
 * {@link ModemManager.ModemGsm.notify::signal-quality} or
 * {@link ModemManager.ModemCdma.notify::signal-quality} is used to
 * make sure the popup menu item for that operator displays the appropriate
 * signal strength icon.
 *
 * Finally, we call the [parent constructor]{@link NMDevice}.
 *
 * We also set {@link #category} to NMConnectionCategory.WIRED.
 * @inheritparams NMDevice
 * @classdesc
 * This is a NMDevice for a modem, be it for a wired or mobile broadband.
 *
 * If it's for mobile broadband, we make use of the {@link ModemManager.ModemGsm}
 * or {@link ModemManager.ModemCdma} class to get signal strength and
 * operator name from (see {@link #mobileDevice}). Additionally, an item
 * is added at the top of the menu subsection for the device {@link #section}
 * (that lists the available networks). This item shows the current operator
 * name for the mobile device as well as a signal strength icon, which
 * is kept in sync.
 * @TODO picture
 * @class
 * @extends NMDevice
 * @see {@link http://developer.gnome.org/libnm-glib/unstable/NMDeviceModem.html|NMDeviceModem}
 */
function NMDeviceModem() {
    this._init.apply(this, arguments);
}

NMDeviceModem.prototype = {
    __proto__: NMDevice.prototype,

    _init: function(client, device, connections) {
        let is_wwan = false;

        this._enabled = true;
        /** If this modem is for mobile broadband (WWAN), the underlying
         * modem object is stored here.
         * @type {ModemManager.ModemGsm|ModemManager.ModemCdma}
         */
        this.mobileDevice = null;
        this._connectionType = 'ppp';

        this._capabilities = device.current_capabilities;
        if (this._capabilities & NetworkManager.DeviceModemCapabilities.GSM_UMTS) {
            is_wwan = true;
            this.mobileDevice = new ModemManager.ModemGsm(device.udi);
            this._connectionType = NetworkManager.SETTING_GSM_SETTING_NAME;
        } else if (this._capabilities & NetworkManager.DeviceModemCapabilities.CDMA_EVDO) {
            is_wwan = true;
            this.mobileDevice = new ModemManager.ModemCdma(device.udi);
            this._connectionType = NetworkManager.SETTING_CDMA_SETTING_NAME;
        } else if (this._capabilities & NetworkManager.DeviceModemCapabilities.LTE) {
            is_wwan = true;
            // FIXME: support signal quality
        }

        if (is_wwan) {
            this.category = NMConnectionCategory.WWAN;
            this._autoConnectionName = _("Auto broadband");
        } else {
            this.category = NMConnectionCategory.WIRED;
            this._autoConnectionName = _("Auto dial-up");
        }

        if (this.mobileDevice) {
            this._operatorNameId = this.mobileDevice.connect('notify::operator-name', Lang.bind(this, function() {
                if (this._operatorItem) {
                    let name = this.mobileDevice.operator_name;
                    if (name) {
                        this._operatorItem.label.text = name;
                        this._operatorItem.actor.show();
                    } else
                        this._operatorItem.actor.hide();
                }
            }));
            this._signalQualityId = this.mobileDevice.connect('notify::signal-quality', Lang.bind(this, function() {
                if (this._operatorItem) {
                    this._operatorItem.setIcon(this._getSignalIcon());
                }
            }));
        }

        NMDevice.prototype._init.call(this, client, device, connections);
    },

    /** Extends the parent function, sets whether the device is enabled or not.
     *
     * If this is a wired modem device, we do nothing (except call the
     * parent function, which also does nothing).
     *
     * If this is a mobile broadband modem device, we set our status item
     * {@link #statusItem} to show an on/off toggle if the network is enabled
     * and to show the device's status (e.g. "unavailable", "connecting...").
     * @override
     * @see #getStatusLabel
     */
    setEnabled: function(enabled) {
        this._enabled = enabled;
        if (this.category == NMConnectionCategory.WWAN) {
            if (enabled) {
                // prevent "network unavailable" statuses
                this.statusItem.setStatus(null);
            } else
                this.statusItem.setStatus(this.getStatusLabel());
        }

        NMDevice.prototype.setEnabled.call(this, enabled);
    },

    /** returns whether this device is currently connected.
     * @returns {boolean} whether the device is connected.
     */
    get connected() {
        return this._enabled && this.device.state == NetworkManager.DeviceState.ACTIVATED;
    },

    /** Extends the parent method to destroy this device.
     *
     * We disconnect from the operator-name and signal-strength signals
     * we were listening to on {@link #mobileDevice} and then call the
     * parent destroy function.
     * @override */
    destroy: function() {
        if (this._operatorNameId) {
            this.mobileDevice.disconnect(this._operatorNameId);
            this._operatorNameId = 0;
        }
        if (this._signalQualityId) {
            this.mobileDevice.disconnect(this._signalQualityId);
            this._signalQualityId = 0;
        }

        NMDevice.prototype.destroy.call(this);
    },

    /** Get the name of the icon corresponding to the current signal strength,
     * being 'network-cellular-signal-[strength]', where `[strength]` is calculated
     * with {@link signalToIcon} (e.g. 'weak', 'strong'):
     * ![network-cellular-signal-excellent](pics/icons/network-cellular-signal-excellent-symbolic.svg)
     * ![network-cellular-signal-good](pics/icons/network-cellular-signal-good-symbolic.svg)
     * ![network-cellular-signal-ok](pics/icons/network-cellular-signal-ok-symbolic.svg)
     * ![network-cellular-signal-weak](pics/icons/network-cellular-signal-weak-symbolic.svg)
     * ![network-cellular-signal-none](pics/icons/network-cellular-signal-none-symbolic.svg).
     * @returns {string} the name of the icon corresponding to the current
     * signal strength.
     */
    _getSignalIcon: function() {
        return 'network-cellular-signal-' + signalToIcon(this.mobileDevice.signal_quality);
    },

    /** Extends the parent function, creating the popup submenu of available
     * networks.
     *
     * If this device is for a mobile broadband modem, we create a
     * {@link PopupMenu.PopupImageMenuItem} which displays the current operator
     * name along with an icon indicating the signal strength of the connection.
     *
     * This is stored in {@link #_operatorItem} and placesd at the top of the
     * submenu, before we call the parent `_createSection` function to generate
     * the rest of the submenu.
     * @override */
    _createSection: function() {
        if (!this._shouldShowConnectionList())
            return;

        if (this.mobileDevice) {
            // If operator_name is null, just pass the empty string, as the item is hidden anyway
            /** If this device is for a mobile broadband modem, this item
             * is a {@link PopupMenu.PopupImageMenuItem} (text + icon to the right)
             * displaying the operator name as the text (e.g. "TELSTRA"),
             * and the current signal strength as the icon.
             *
             * It's only shown if the operator name is not null.
             * @type {PopupMenu.PopupImageMenuItem}
             */
            this._operatorItem = new PopupMenu.PopupImageMenuItem(this.mobileDevice.operator_name || '',
                                                                  this._getSignalIcon(),
                                                                  { reactive: false });
            if (!this.mobileDevice.operator_name)
                this._operatorItem.actor.hide();
            this.section.addMenuItem(this._operatorItem);
        }

        NMDevice.prototype._createSection.call(this);
    },

    /** Extends the parent function which clears the popup menu section
     * showing the list of available networks.
     *
     * We make sure to remove the operator item {@link #_operatorItem} too
     * (only relevant for mobile broadband modems).
     */
    _clearSection: function() {
        this._operatorItem = null;

        NMDevice.prototype._clearSection.call(this);
    },

    /** Implements the parent function to provide an automatic connection for
     * this modem.
     *
     * We return `null` and just spawn the gnome control  centre network
     * panel for the user to handle the connection there.
     */
    _createAutomaticConnection: function() {
        // Mobile wizard is too complex for the shell UI and
        // is handled by the network panel
        Util.spawn(['gnome-control-center', 'network',
                    'connect-3g', this.device.get_path()]);
        return null;
    }
};

/** creates a new NMDeviceBluetooth
 *
 * This is a {@link NMDevice} for a bluetooth device such as a bluetooth dongle.
 *
 * We set our category {@link #category} to
 * `NMConnectionCategory.WWAN`, and set
 * our auto connection name to {@link #_makeConnectionName} (being 
 * "Auto [name of device]" or "Auto bluetooth") as this device
 * supports automatic connection.
 *
 * Then we call the [parent constructor]{@link NMDevice}.
 * @inheritparams NMDevice
 * @classdesc
 * This is a {@link NMDevice} for a bluetooth device such as a bluetooth dongle.
 *
 * It allows auto-connect.
 *
 * @class
 * @extends NMDevice
 * @see {@link http://developer.gnome.org/libnm-glib/unstable/NMDeviceBt.html|NMDeviceBt}
 */
function NMDeviceBluetooth() {
    this._init.apply(this, arguments);
}

NMDeviceBluetooth.prototype = {
    __proto__: NMDevice.prototype,

    _init: function(client, device, connections) {
        this._autoConnectionName = this._makeConnectionName(device);
        device.connect('notify::name', Lang.bind(this, this._updateAutoConnectionName));

        this.category = NMConnectionCategory.WWAN;

        NMDevice.prototype._init.call(this, client, device, connections);
    },

    /** Implements the parent function to create an automatic bluetooth
     * connection.
     *
     * We create a new {@link NetworkManager.Connection},
     * add a new (default) {@link NetworkManager.SettingBluetooth} to it,
     * and then add a {@link NetworkManager.SettingConnection}
     * to it, setting its UUID to a new randomly-generated one, its
     * ID to {@link #_makeConnectionName}, and setting
     * its `autoconnect` property to `false`.
     * @override */
    _createAutomaticConnection: function() {
        let connection = new NetworkManager.Connection;
        connection._uuid = NetworkManager.utils_uuid_generate();
        connection.add_setting(new NetworkManager.SettingBluetooth);
        connection.add_setting(new NetworkManager.SettingConnection({
            uuid: connection._uuid,
            id: this._autoConnectionName,
            type: NetworkManager.SETTING_BLUETOOTH_SETTING_NAME,
            autoconnect: false
        }));
        return connection;
    },

    /** Generates an automatic connection name for this device.
     *
     * Returns "Auto [device.name]" where 'device.name'
     * is the name of the bluetooth device, or "Auto bluetooth" if it doesn't
     * have one.
     * @param {NMClient.NMDeviceBt} device - device to create an autoconnect
     * name for.
     * @returns {string} "Auto [device.name]" if the device has a name,
     * or "Auto bluetooth" otherwise.
     * @see {@link http://developer.gnome.org/libnm-glib/unstable/NMDeviceBt.html#NMDeviceBt--name|NMDeviceBt.name}
     */
    _makeConnectionName: function(device) {
        let name = device.name;
        if (name)
            return _("Auto %s").format(name);
        else
            return _("Auto bluetooth");
    },

    /** Callback whenever the device's name changes.
     * This updates {@link #_autoConnectionName} accordingly and regenerates
     * the popup menu item(s) to match. */
    _updateAutoConnectionName: function() {
        this._autoConnectionName = this._makeConnectionName(this.device);

        this._clearSection();
        this._createSection();
    }
};


// Not a real device, but I save a lot code this way
/** creates a new NMDeviceVPN
 *
 * We disable auto connections (set {@link #_autoConnectionName} to `null`),
 * and set {@link #category} to NMConnectionCategory.VPN.
 *
 * Then we call the [parent constructor]{@link NMDevice} with `client`,
 * a `null` device, and no connections.
 *
 * @inheritparams NMDevice
 * @classdesc
 * This is a dummy {@link NMDevice} implementation for VPNs.
 *
 * It is dummy because it doesn't have a corresponding NetworkManager
 * device. However, a comment in the code says that making a dummy device
 * for VPN "saves a lot of code".
 *
 * A number of functions from {@link NMDevice} need to be overridden as
 * they rely on accessing {@link #device} which is `null` for the VPN
 * device.
 *
 * A VPN device has no auto-connect capability.
 *
 * @class
 * @extends NMDevice
 */
function NMDeviceVPN() {
    this._init.apply(this, arguments);
}

NMDeviceVPN.prototype = {
    __proto__: NMDevice.prototype,

    _init: function(client) {
        // Disable autoconnections
        this._autoConnectionName = null;
        this.category = NMConnectionCategory.VPN;

        NMDevice.prototype._init.call(this, client, null, [ ]);
    },

    /** Override the parent function. A connection is valid for this device
     * if its type is VPN (use
     *
     *      connection.get_setting_by_name(
     *          NetworkManager.SETTING_CONNECTION_SETTING_NAME
     *      ).type
     *
     * to compare the type against `NetworkManager.SETTING_VPN_SETTING_NAME`).
     * @override
     */
    connectionValid: function(connection) {
        return connection._type == NetworkManager.SETTING_VPN_SETTING_NAME;
    },

    /** Whether this VPN device has any connections.
     * @type {boolean} */
    get empty() {
        return this._connections.length == 0;
    },

    /** Overrides the parent property as to whether this device is
     * connected. It is connected if {@link #_activeConnection} is
     * not null, ie. we have an active connection set via
     * {@link #setActiveConnection}.
     * @override */
    get connected() {
        return !!this._activeConnection;
    },

    /** Extends the parent function to set the active connection.
     *
     * We call {@link NMDevice#setActiveConnection} and then additionally
     * emit 'active-connection-changed'.
     * @fires .active-connection-changed
     * @override
     */
    setActiveConnection: function(activeConnection) {
        NMDevice.prototype.setActiveConnection.call(this, activeConnection);

        this.emit('active-connection-changed');
    },

    /** Overrides the parent function - for the VPN device we
     * should *always* show the list of available connections.
     * @override */
    _shouldShowConnectionList: function() {
        return true;
    },

    /** Override the parent function to deactivate the VPN device.
     * 
     * We call `nm_client_deactivate_connection` with {@link #_activeConnection}.
     * @override */
    deactivate: function() {
        if (this._activeConnection)
            this._client.deactivate_connection(this._activeConnection);
    },

    /** Overrides the parent function which gets a status text corresponding
     * to the device. We *always* return `null` so that {@link #statusItem}
     * only every shows an on/off toggle.
     * @override */
    getStatusLabel: function() {
        return null;
    }
};
/** Emitted whenever the active connection of a VPN device changes.
 * Usually from a call to {@link NMDeviceVPN#setActiveConnection}.
 * @event
 * @name active-connection-changed
 * @memberof NMDeviceVPN
 */

/** creates a new NMDeviceWireless
 *
 * We set {@link #category} to `NMConnectionCategory.WIRELESS`.
 *
 * The NMDeviceWireless maintains a list of available networks that this device
 * connect to, {@link #_networks}.
 *
 * Each item in {@link #_networks} is an object describing a network
 * ({@link #WirelessNetworkObject}), usually referred to throughout the
 * code as `apObj`. Each network is identified by
 * its SSID, mode, and security. Each network may have multiple access points,
 * and each access point may enable multiple connections. These are stored
 * in `apObj.connections` and `apObj.accessPoints`. Typically a network
 * only has one connection.
 *
 * We ensure {@link #_networks} is always sorted
 * (see {@link #_networkSortFunction}): networks with connections before
 * networks with none, networks with security before those without, and
 * then alphabetically.
 *
 * We connect to a number of signals so that we always know when an
 * access point is added or removed, and which access point is active.
 *
 * Finally, we call the [parent constructor]{@link NMDevice}.
 *
 * @inheritparams NMDevice
 * @classdesc
 * This is a subclass of {@link NMDevice} for wireless devices.
 *
 * ![{@link NMDeviceWireless}](pics/NMDeviceWireless.png)
 *
 * The above picture is an example of a {@link NMDeviceWireless} (the
 * status item {@link #statusItem} has been hidden since
 * the "Wireless" section only has one device (I only have one wireless card),
 * and {@link #section} is the list of available networks).
 *
 * It is more complex than {@link NMDevice} because in addition to managing
 * connections ({@link NMDevice#_connections}), it must also manage
 * "networks", stored in {@link #_networks}.
 *
 * A "network" is identified by the combination of its SSID, mode
 * (ADHOC or INFRA), and security. Each network may have multiple access points,
 * and each access point may enable one or more of {@link #_connections} to
 * be activated with that access point. Hence a particular network
 * not only has multiple access points, but may also have multiple
 * connections it can activate (usually it will only have one connection
 * though).
 *
 * A network may also have access points, but no connections. (If a network
 * has no access points it is not included).
 *
 * This makes the UI a bit different too.
 * Instead of using a simple PopupMenuItem for each connection as
 * {@link #_createConnectionItem} does, we instead show item(s) for each
 * *network*. The underlying items are always {@link NMNetworkMenuItem}s
 * instead of popup menu items. These display the network name as well as an
 * icon for signal strength (of the strongest access point for that
 * connection), and potentially a padlock icon if the network is secure.
 *
 * Hence we must override {@link #_createSection} to create our popup menu
 * section so that instead of having a popup menu item per connection, we have
 * one (possibly more) per network. Functions {@link #_createNetworkItem}
 * and {@link #_createAPItem} assist in this.
 *
 * If a network has exactly one or no connections, it gets a single
 * {@link NMNetworkMenuItem} in the popup menu for that network. Clicking
 * on the item will attempt to connect to that network (creating an automatic
 * connection for it if it has none).
 *
 * However if a network has more than one connection (because its access
 * points may connect to more than one connection), it will get its own
 * sub menu: the title of the submenu is the network's SSID, and within the
 * submenu is one {@link NMNetworkMenuItem} *per connection* that this
 * network has.
 *
 * @class
 * @extends NMDevice
 */
function NMDeviceWireless() {
    this._init.apply(this, arguments);
}

NMDeviceWireless.prototype = {
    __proto__: NMDevice.prototype,

    _init: function(client, device, connections) {
        this.category = NMConnectionCategory.WIRELESS;

        this._overflowItem = null;
        /** Array of available networks for this wireless device.
         *
         * Each element is an object describing the network: its access points,
         * SSID, security type, corresponding {@link NMNetworkMenuItem}
         * for the network icon's popup menu, and array of
         * {@link NMClient.RemoteConnection}s
         * corresponding to the network.
         *
         * An access point belongs to a particular "network" if its SSID,
         * mode, and security matches the SSID of the current access points.
         * (see {@link #_networkCompare}).
         * @type {Network.WirelessNetworkObject[]} */
        this._networks = [ ];

        // breaking the layers with this, but cannot call
        // this.connectionValid until I have a device
        this.device = device;

        let validConnections = connections.filter(Lang.bind(this, function(connection) {
            return this.connectionValid(connection);
        }));
        let accessPoints = device.get_access_points() || [ ];
        for (let i = 0; i < accessPoints.length; i++) {
            // Access points are grouped by network
            let ap = accessPoints[i];

            if (ap.get_ssid() == null) {
                // hidden access point cannot be added, we need to know
                // the SSID and security details to connect
                // nevertheless, the access point can acquire a SSID when
                // NetworkManager connects to it (via nmcli or the control-center)
                ap._notifySsidId = ap.connect('notify::ssid', Lang.bind(this, this._notifySsidCb));
                continue;
            }

            let pos = this._findNetwork(ap);
            let obj;
            if (pos != -1) {
                obj = this._networks[pos];
                obj.accessPoints.push(ap);
            } else {
                obj = { ssid: ap.get_ssid(),
                        mode: ap.mode,
                        security: this._getApSecurityType(ap),
                        connections: [ ],
                        item: null,
                        accessPoints: [ ap ]
                      };
                obj.ssidText = ssidToLabel(obj.ssid);
                this._networks.push(obj);
            }

            // Check if some connection is valid for this AP
            for (let j = 0; j < validConnections.length; j++) {
                let connection = validConnections[j];
                if (ap.connection_valid(connection) &&
                    obj.connections.indexOf(connection) == -1) {
                    obj.connections.push(connection);
                }
            }
        }

        if (this.device.active_access_point) {
            let networkPos = this._findNetwork(this.device.active_access_point);

            if (networkPos == -1) // the connected access point is invisible
                this._activeNetwork = null;
            else
                /** The currently-active network (if any, `null` if none).
                 * It is a member of {@link #_networks}.
                 * @type {?Network.WirelessNetworkObject} */
                this._activeNetwork = this._networks[networkPos];
        } else {
            this._activeNetwork = null;
        }
        this._networks.sort(this._networkSortFunction);

        this._apChangedId = device.connect('notify::active-access-point', Lang.bind(this, this._activeApChanged));
        this._apAddedId = device.connect('access-point-added', Lang.bind(this, this._accessPointAdded));
        this._apRemovedId = device.connect('access-point-removed', Lang.bind(this, this._accessPointRemoved));

        NMDevice.prototype._init.call(this, client, device, validConnections);
    },

    /** Destroys this NMDeviceWireless. This simply calls the parent `destroy`
     * and additionally disconnects various signals we were listening to on
     * the device (like addition/removal of access points)
     * @override */
    destroy: function() {
        if (this._apChangedId) {
            // see above for this HACK
            GObject.Object.prototype.disconnect.call(this.device, this._apChangedId);
            this._apChangedId = 0;
        }

        if (this._apAddedId) {
            GObject.Object.prototype.disconnect.call(this.device, this._apAddedId);
            this._apAddedId = 0;
        }

        if (this._apRemovedId) {
            GObject.Object.prototype.disconnect.call(this.device, this._apRemovedId);
            this._apRemovedId = 0;
        }

        NMDevice.prototype.destroy.call(this);
    },

    /** Overrides the parent function to set whether the device is enabled
     * or not. A wireless device is considered "enabled" if it is *both*
     * software- and hardware-enabled, regardless of whether it is
     * connected to anything.
     *
     * If `enabled`, the status item (on/off toggle/connection status) and
     * popup menu section showing the list of available networks
     * {@link #section} are shown.
     *
     * Otherwise, they are all hidden (if my wireless card is hardware disabled
     * then there's no point showing its list of available networks because
     * I can't use it until I hardware-enable it).
     * @override
     */
    setEnabled: function(enabled) {
        if (enabled) {
            this.statusItem.actor.show();
            this.section.actor.show();
        } else {
            this.statusItem.actor.hide();
            this.section.actor.hide();
        }
    },

    /** Overrides the parent function to activate the wireless device.
     *
     * If we already have an active connection as set by
     * {@link #setActiveConnection} we return (there is nothing to be done).
     *
     * Otherwise, amongst all the visible networks in {@link #_networks},
     * we pick the most recently used connection and connect using the
     * access point with the strongest signal strength.
     *
     * If there is no most recently used connection we use
     * {@link #_createAutomaticConnection} to connect to the first network
     * in {@link #_networks} (the comments say this is just a corner case
     * anyway and unlikely to happen).
     * @override
     */
    activate: function() {
        if (this._activeConnection)
            // nothing to do
            return;

        // among all visible networks, pick the last recently used connection
        let best = null;
        let bestApObj = null;
        let bestTime = 0;
        for (let i = 0; i < this._networks.length; i++) {
            let apObj = this._networks[i];
            for (let j = 0; j < apObj.connections.length; j++) {
                let connection = apObj.connections[j];
                if (connection._timestamp > bestTime) {
                    best = connection;
                    bestTime = connection._timestamp;
                    bestApObj = apObj;
                }
            }
        }

        if (best) {
            for (let i = 0; i < bestApObj.accessPoints.length; i++) {
                let ap = bestApObj.accessPoints[i];
                if (ap.connection_valid(best)) {
                    this._client.activate_connection(best, this.device, ap.dbus_path, null);
                    break;
                }
            }
            return;
        }

        // XXX: what else to do?
        // for now, just pick a random network
        // (this function is called in a corner case anyway, that is, only when
        // the user toggles the switch and has more than one wireless device)
        if (this._networks.length > 0) {
            let connection = this._createAutomaticConnection(this._networks[0]);
            let accessPoints = sortAccessPoints(this._networks[0].accessPoints);
            this._client.add_and_activate_connection(connection, this.device, accessPoints[0].dbus_path, null);
        }
    },

    /** Callback when an access point's SSID changes (for example, hidden
     * access points have `null` SSID).
     *
     * If the access point has a non-null SSID we add it with
     * {@link #_accessPointAdded}, which adds the access point to its matching
     * {@link WirelessNetworkObject} in {@link #_networks} (or creates a new
     * one for it if it doesn't belong to any existing ones).
     * @param {NMClient.AccessPoint[]} accessPoint - an access point.
     */
    _notifySsidCb: function(accessPoint) {
        if (accessPoint.get_ssid() != null) {
            accessPoint.disconnect(accessPoint._notifySsidId);
            accessPoint._notifySsidId = 0;
            this._accessPointAdded(this.device, accessPoint);
        }
    },

    /** Callback when the active access point (the access point we are connected
     * to) for this device changes.
     *
     * This finds the network from {@link #_network} to which the access
     * point corresponds, and resets {@link #_activeNetwork} accordingly.
     */
    _activeApChanged: function() {
        this._activeNetwork = null;

        let activeAp = this.device.active_access_point;

        if (activeAp) {
            let res = this._findExistingNetwork(activeAp);

            if (res != null)
                this._activeNetwork = this._networks[res.network];
        }

        // we don't refresh the view here, setActiveConnection will
    },

    /** Convenience function to gets the security type of an access point.
     *
     * It does this by looking at the `flags`, `wpa_flags` and `rsn_flags`
     * properties of the access point.
     * @inheritparams #_notifySsidCb
     * @returns {Network.NMAccessPointSecurity} the security for this access
     * point (e.g. WPA2_ENT, WPA2_PSK, ...)
     * @see documentation for the
     * {@link http://developer.gnome.org/libnm-glib/unstable/NMAccessPoint.html#NMAccessPoint--flags|flags},
     * {@link http://developer.gnome.org/libnm-glib/unstable/NMAccessPoint.html#NMAccessPoint--wpa-flags|`wpa_flags`}
     * and
     * {@link http://developer.gnome.org/libnm-glib/unstable/NMAccessPoint.html#NMAccessPoint--rsn-flags|`rsn_flags`}
     * properties of a NMClient.AccessPoint
     */
    _getApSecurityType: function(accessPoint) {
        if (accessPoint._secType)
            return accessPoint._secType;

        let flags = accessPoint.flags;
        let wpa_flags = accessPoint.wpa_flags;
        let rsn_flags = accessPoint.rsn_flags;
        let type;
        if (rsn_flags != NM80211ApSecurityFlags.NONE) {
            /* RSN check first so that WPA+WPA2 APs are treated as RSN/WPA2 */
            if (rsn_flags & NM80211ApSecurityFlags.KEY_MGMT_802_1X)
	        type = NMAccessPointSecurity.WPA2_ENT;
	    else if (rsn_flags & NM80211ApSecurityFlags.KEY_MGMT_PSK)
	        type = NMAccessPointSecurity.WPA2_PSK;
        } else if (wpa_flags != NM80211ApSecurityFlags.NONE) {
            if (wpa_flags & NM80211ApSecurityFlags.KEY_MGMT_802_1X)
                type = NMAccessPointSecurity.WPA_ENT;
            else if (wpa_flags & NM80211ApSecurityFlags.KEY_MGMT_PSK)
	        type = NMAccessPointSecurity.WPA_PSK;
        } else {
            if (flags & NM80211ApFlags.PRIVACY)
                type = NMAccessPointSecurity.WEP;
            else
                type = NMAccessPointSecurity.NONE;
        }

        // cache the found value to avoid checking flags all the time
        accessPoint._secType = type;
        return type;
    },

    /** Sorts two networks such that:
     *
     * * networks with connections come before networks without connections.
     * * In case of a tie,
     *   secure networks come before ones without security.
     * * In case of a tie, sort alphabetically by SSID.
     *
     * Used as the sort function in `Array.sort` on {@link #_networks}.
     * @param {Network.WirelessNetworkObject} one - first network to compare
     * @param {Network.WirelessNetworkObject} two - second network to compare
     * @returns {number} -1 if `one` comes before `two`, 1 if `two`
     * comes before `one`, 0 if they tie.
     */
    _networkSortFunction: function(one, two) {
        let oneHasConnection = one.connections.length != 0;
        let twoHasConnection = two.connections.length != 0;

        // place known connections first
        // (-1 = good order, 1 = wrong order)
        if (oneHasConnection && !twoHasConnection)
            return -1;
        else if (!oneHasConnection && twoHasConnection)
            return 1;

        let oneHasSecurity = one.security != NMAccessPointSecurity.NONE;
        let twoHasSecurity = two.security != NMAccessPointSecurity.NONE;

        // place secure connections first
        // (we treat WEP/WPA/WPA2 the same as there is no way to
        // take them apart from the UI)
        if (oneHasSecurity && !twoHasSecurity)
            return -1;
        else if (!oneHasSecurity && twoHasSecurity)
            return 1;

        // sort alphabetically
        return GLib.utf8_collate(one.ssidText, two.ssidText);
    },

    /** Tests an access point to see if it belongs to a network.
     *
     * An access point belongs to a {@link WirelessNetworkObject} if:
     *
     * * the SSIDs match ({@link ssidCompare}); AND
     * * the modes match (NetworkManager.80211Mode.{INFRA, ADHOC}); AND
     * * the security types match ({@link NMAccessPointSecurity}).
     * @param {Network.WirelessNetworkObject} network - network to test the
     * access point against
     * @inheritparams #_notifySsidCb
     * @returns {true} if `accessPoint` belongs to network `network`.
     */
    _networkCompare: function(network, accessPoint) {
        if (!ssidCompare(network.ssid, accessPoint.get_ssid()))
            return false;
        if (network.mode != accessPoint.mode)
            return false;
        if (network.security != this._getApSecurityType(accessPoint))
            return false;

        return true;
    },

    /** Finds the {@link WirelessNetworkObject} that `accessPoint` is a
     * member of in {@link #_networks}, as well as the index of `accessPoint`
     * in the matching `network.accessPoints`.
     *
     * Note - this doesn't ask whether an access point *could* be an access
     * point for a particular network; see {@link #_findNetwork} for that.
     * It only asks whether the access point has been *stored* as an access
     * point for a particular network in {@link #_networks}, i.e. it asks
     * whether we know about this access point yet.
     * @inheritparams #_notifySsidCb
     * @returns {?{network: number, ap: number}} - an object with propery
     * `network` being the index of the network with this access point within
     * {@link #_networks}, and property `ap` being the index of
     * `accessPoint` that {@link WirelessNetworkObject}'s `accessPoints` array.
     *
     * If the access point does not belong to any network in {@link #_networks},
     * we return `null`.
     */
    _findExistingNetwork: function(accessPoint) {
        for (let i = 0; i < this._networks.length; i++) {
            let apObj = this._networks[i];
            for (let j = 0; j < apObj.accessPoints.length; j++) {
                if (apObj.accessPoints[j] == accessPoint)
                    return { network: i, ap: j };
            }
        }

        return null;
    },

    /** Finds the index of the network in {@link #_networks} to which
     * `accessPoint` corresponds, whether or not we have actually
     * *added* it to `this._networks[i].accessPoints` yet.
     *
     * That is, this function asks whether the access point belongs to
     * any of our networks.
     *
     * Contrast with {@link #_findExistingNetwork}, which will only find
     * a network if it is *already stored* in `this._networks[i].accessPoints`.
     *
     * We use {@link #_networkCompare} to see if an access point matches one
     * of our networks by ensuring the SSID, mode, and encryption type of
     * the access point and network match.
     *
     * @inheritparams #_notifySsidCb
     * @returns {number} the index `i` of the network in {@link #_networks} such
     * that this access point's SSID/security/mode match that of
     * `this._networks[i]`, or -1 if the SSID is `null` or the access point
     * didn't match any of our networks.
     * @see #_networkCompare
     */
    _findNetwork: function(accessPoint) {
        if (accessPoint.get_ssid() == null)
            return -1;

        for (let i = 0; i < this._networks.length; i++) {
            if (this._networkCompare(this._networks[i], accessPoint))
                return i;
        }
        return -1;
    },

    /** Callback when an access point is added to {@link #device}.
     *
     * If it is not visible yet (i.e. no SSID) we wait until it gets an SSID
     * before adding it (using {@link #_notifySsidCb} and its 'notify::ssid'
     * signal) and return.
     *
     * Otherwise, we see if the access point belongs to any of our
     * networks ({@link #_findNetwork}).
     *
     * If it does but it is already in `this._networks[i].accessPoints`, we
     * do nothing (we have already added it).
     * Otherwise, we add it to the end of `this._networks[i].accessPoints`.
     *
     * If `this._networks[i].item` exists (i.e. we have a {@link NMNetworkMenuItem}
     * for this network) we call {@link NMNetworkMenuItem#updateAccessPoints}
     * which will resort the access points by strength, strongest to weakest.
     *
     * On the other hand, if `accessPoint` does *not* belong to any of the
     * networks in {@link #_networks}, we create a new
     * {@link WirelessNetworkObject} for it and add it in to {@link #_networks},
     * ensuring that it remains sorted (see {@link #_networkSortFunction}:
     * networks with connections before those with none; secured networks before
     * unsecured, and then alphabetical order by SSID).
     *
     * In either case, we check whether the addition of `accessPoint` to
     * `this._networks[i].accessPoints` enables any new connections for the
     * network that we didn't have before (in `this._networks[i].connections`),
     * and add them if so.
     *
     * If we had to create a new {@link WirelessNetworkObject} or the addition
     * of `accessPoint` enabled us to connect to new wireless networks with
     * this device, then we have to update the popup menu items to reflect this:
     *
     * * Destroy `this._networks[i].item` if it exists;
     * * (Re)create the {@link NMNetworkMenuItem} for this network to put
     * into the popup menu section of available connections {@link #section}.
     * See {@link #_createNetworkItem}.
     *
     * @param {NMClient.NMDeviceWifi} device - the device ({@link #device})
     * @inheritparams #_notifySsidCb
     * @see #_createNetworkItem
     */
    _accessPointAdded: function(device, accessPoint) {
        if (accessPoint.get_ssid() == null) {
            // This access point is not visible yet
            // Wait for it to get a ssid
            accessPoint._notifySsidId = accessPoint.connect('notify::ssid', Lang.bind(this, this._notifySsidCb));
            return;
        }

        let pos = this._findNetwork(accessPoint);
        let apObj;
        let needsupdate = false;

        if (pos != -1) {
            apObj = this._networks[pos];
            if (apObj.accessPoints.indexOf(accessPoint) != -1) {
                log('Access point was already seen, not adding again');
                return;
            }

            apObj.accessPoints.push(accessPoint);
            if (apObj.item)
                apObj.item.updateAccessPoints(apObj.accessPoints);
        } else {
            apObj = { ssid: accessPoint.get_ssid(),
                      mode: accessPoint.mode,
                      security: this._getApSecurityType(accessPoint),
                      connections: [ ],
                      item: null,
                      accessPoints: [ accessPoint ]
                    };
            apObj.ssidText = ssidToLabel(apObj.ssid);
            needsupdate = true;
        }

        // check if this enables new connections for this group
        for (let i = 0; i < this._connections.length; i++) {
            let connection = this._connections[i].connection;
            if (accessPoint.connection_valid(connection) &&
                apObj.connections.indexOf(connection) == -1) {
                apObj.connections.push(connection);

                // this potentially changes the order
                needsupdate = true;
            }
        }

        if (needsupdate) {
            if (apObj.item)
                apObj.item.destroy();

            if (pos != -1)
                this._networks.splice(pos, 1);

            if (this._networks.length == 0) {
                // only network in the list
                this._networks.push(apObj);
                this._clearSection();
                this._createSection();
                return;
            }

            // skip networks that should appear earlier
            let menuPos = 0;
            for (pos = 0;
                 pos < this._networks.length &&
                 this._networkSortFunction(this._networks[pos], apObj) < 0; ++pos) {
                if (this._networks[pos] != this._activeNetwork)
                    menuPos++;
            }

            // (re-)add the network
            this._networks.splice(pos, 0, apObj);

            if (this._shouldShowConnectionList()) {
                menuPos += (this._activeConnectionItem ? 1 : 0);
                this._createNetworkItem(apObj, menuPos);
            }
        }
    },

    /** Callback when an access point is removed from {@link #device}.
     *
     * We use {@link #_findExistingNetwork} to find the index of the
     * corresponding {@link WirelessNetworkObject} in
     * [`this._networks`]{@link #_networks} (call it `apObj`),
     * and the index of `accessPoint` within `apObj.accessPoints`.
     *
     * We then remove the access point from `apObj.accessPoints`.
     *
     * If `apObj` now has no more access points:
     *
     * * if `apObj` represents the currently active network, we set
     * {@link #_activeNetwork} back to `null`;
     * * if `apObj.item` exists (i.e. a {@link NMNetworkMenuItem} for this
     * network in the popup menu section), we remove it;
     * * if removing this item frees up space in the popup menu section, we move
     * the top item from the "More..." submenu up into the main menu section;
     * * if there are now no longer any items in the "More..." submenu,
     * we remove it from our popup menu section;
     * * finally, remove `apObj` from {@link #_networks}.
     *
     * If `apObj` *does* have remaining access points, we just call
     * [apObj.item.updateAccessPoints]{@link NMNetworkMenuItem#updateAccessPoints}
     * to resort the access points by strength (strong to weak).
     * @inheritparams #_accessPointRemoved
     */
    _accessPointRemoved: function(device, accessPoint) {
        let res = this._findExistingNetwork(accessPoint);

        if (res == null) {
            log('Removing an access point that was never added');
            return;
        }

        let apObj = this._networks[res.network];
        apObj.accessPoints.splice(res.ap, 1);

        if (apObj.accessPoints.length == 0) {
            if (this._activeNetwork == apObj)
                this._activeNetwork = null;

            if (apObj.item)
                apObj.item.destroy();

            if (this._overflowItem) {
                if (!apObj.isMore) {
                    // we removed an item in the main menu, and we have a more submenu
                    // we need to extract the first item in more and move it to the submenu

                    let item = this._overflowItem.menu.firstMenuItem;
                    if (item && item._apObj) {
                        item.destroy();
                        // clear the cycle, and allow the construction of the new item
                        item._apObj.item = null;

                        this._createNetworkItem(item._apObj, NUM_VISIBLE_NETWORKS-1);
                    } else {
                        log('The more... menu was existing and empty! This should not happen');
                    }
                }

                // This can happen if the removed connection is from the overflow
                // menu, or if we just moved the last connection out from the menu
                if (this._overflowItem.menu.numMenuItems == 0) {
                    this._overflowItem.destroy();
                    this._overflowItem = null;
                }
            }
            this._networks.splice(res.network, 1);

        } else if (apObj.item)
            apObj.item.updateAccessPoints(apObj.accessPoints);
    },

    /** Creates a popup menu item for a particular network to be displayed
     * in the popup menu section for this device as an available network.
     *
     * First we create a {@link NMNetworkMenuItem}, giving it `accessPointObj`'s
     * access points for its `accessPoints` argument, and either
     * `connection`'s ID (e.g. "T-Mobile 3G") or the SSID of the strongest
     * access point as the title.
     *
     * We ensure that clicking on the item will cause the connection to
     * be activated with the strongest access point.
     * @param {Network.ConnectionData} connection - the underlying connection for the network,
     * a member of {@link #_connections}.
     * @param {Network.WirelessNetworkObject} accessPointObj - object describing
     * the network to which this connection belongs (it has a list of access
     * points for the network ordered by strength, strongeset to weakest).
     * It is a member of {@link #_networks}.
     * @param {boolean} useConnectionName - whether to use the connection's
     * name (e.g. "T-Mobile 3G") as the text on the {@link NMNetworkMenuItem}.
     * Otherwise the SSID of the strongest access point for this connection
     * is used.
     * @see ConnectionData
     * @see NMNetworkMenuItem
     */
    _createAPItem: function(connection, accessPointObj, useConnectionName) {
        let item = new NMNetworkMenuItem(accessPointObj.accessPoints, useConnectionName ? connection._name : undefined);
        item._connection = connection;
        item.connect('activate', Lang.bind(this, function() {
            let accessPoints = sortAccessPoints(accessPointObj.accessPoints);
            for (let i = 0; i < accessPoints.length; i++) {
                if (accessPoints[i].connection_valid(connection)) {
                    this._client.activate_connection(connection, this.device, accessPoints[i].dbus_path, null);
                    break;
                }
            }
        }));
        return item;
    },

    /** Extends the parent function clearing the popup menu section showing
     * the list of available connections/networks.
     *
     * We additionally ensure that all of the {@link NMNetworkMenuItem}s we created
     * in {@link #_createAPItem}/{@link #_createNetworkItem} are removed.
     */
    _clearSection: function() {
        NMDevice.prototype._clearSection.call(this);

        for (let i = 0; i < this._networks.length; i++)
            this._networks[i].item = null;
        this._overflowItem = null;
    },

    /** Overrides the parent function to remove a connection from this device.
     *
     * We locate the connection and remove it from {@link #_connections}.
     *
     * We then go through each network in {@link #_networks[i]} (call it `apObj`).
     * If `apObj.connections` contains `connection` we remove it.
     *
     * We then ensure `apObj.item` reflects the number of connections that
     * `apObj` has - for example if it used to have multiple connections but
     * now only has one, we convert it back from a
     * {@link PopupMenu.PopupSubMenuMenuItem} to a {@link NMNetworkMenuItem}.
     * (Note that we don't remove `apObj` if it now has *no* connections left;
     * we only remove it if it has no access points left).
     *
     * Finally we regenerate our popup menu section showing all the items
     * for each network for this device ({@link #section}), ensuring
     * that we resort the networks (networks with connections before
     * networks without connections, then networks with security before
     * unsecured ones, then sort alphabetically).
     *
     * @override
     */
    removeConnection: function(connection) {
        if (!connection._uuid)
            return;
        let pos = this._findConnection(connection._uuid);
        if (pos == -1) {
            // removing connection that was never added
            return;
        }

        let obj = this._connections[pos];
        this._connections.splice(pos, 1);

        let anyauto = false, forceupdate = false;
        for (let i = 0; i < this._networks.length; i++) {
            let apObj = this._networks[i];
            let connections = apObj.connections;
            for (let k = 0; k < connections.length; k++) {
                if (connections[k]._uuid == connection._uuid) {
                    // remove the connection from the access point group
                    connections.splice(k);
                    anyauto = connections.length == 0;

                    if (anyauto) {
                        // this potentially changes the sorting order
                        forceupdate = true;
                        break;
                    }
                    if (apObj.item) {
                        if (apObj.item instanceof PopupMenu.PopupSubMenuMenuItem) {
                            let items = apObj.item.menu.getMenuItems();
                            if (items.length == 2) {
                                // we need to update the connection list to convert this to a normal item
                                forceupdate = true;
                            } else {
                                for (let j = 0; j < items.length; j++) {
                                    if (items[j]._connection._uuid == connection._uuid) {
                                        items[j].destroy();
                                        break;
                                    }
                                }
                            }
                        } else {
                            apObj.item.destroy();
                            apObj.item = null;
                        }
                    }
                    break;
                }
            }
        }

        if (forceupdate || anyauto) {
            this._networks.sort(this._networkSortFunction);
            this._clearSection();
            this._createSection();
        }
    },

    /** Overrides the parent function to add a connection to this device.
     *
     * First we create a {@link ConnectionData} for `connection` (recording
     * its ID, UUID, and `connection`) and add this to {@link #_connections}.
     *
     * Then we look through each element of {@link #_networks} to see if
     * `connection` is valid for any of `this._networks[i].accessPoints`.
     * If so, we add `connection` to `this._networks[i].connections`.
     *
     * Then we resort {@link #_networks} (recall that networks with connections
     * got before those without, see {@link #_networkSortFunction}), and regenerate
     * our popup menu section that displays items for each available network.
     * @override
     */
    addConnection: function(connection) {
        // record the connection
        let obj = {
            connection: connection,
            name: connection._name,
            uuid: connection._uuid,
        };
        this._connections.push(obj);

        // find an appropriate access point
        let forceupdate = false;
        for (let i = 0; i < this._networks.length; i++) {
            let apObj = this._networks[i];

            // Check if connection is valid for any of these access points
            for (let k = 0; k < apObj.accessPoints.length; k++) {
                let ap = apObj.accessPoints[k];
                if (ap.connection_valid(connection)) {
                    apObj.connections.push(connection);
                    // this potentially changes the sorting order
                    forceupdate = true;
                    break;
                }
            }
        }

        if (forceupdate) {
            this._networks.sort(this._networkSortFunction);
            this._clearSection();
            this._createSection();
        }
    },

    /** Overrides the parent function to create a popup menu item for the
     * currently-active connection (example shown below, ignore the blue
     * border).
     *
     * ![Example of the active connection item for {@link NMDeviceWireless}](pics/status.network.activeConnectionItem)
     *
     * If we have an active network {@link #_activeNetwork}, we create
     * a non-reactive {@link NMNetworkMenuItem} for it with its access
     * points.
     * This will show a signal strength icon for the strongest access point
     * and a padlock icon if the network is secured.
     *
     * Otherwise if we have an active connection {@link #_activeConnection}
     * but no active network,
     * we create a non-reactive {@link PopupMenu.PopupImageMenuItem}.
     * This consists of text (the connection's ID) and
     * the 'network-wireless-connected' icon 
     * ![network-wireless-connected](pics/icons/network-wireless-connected.svg)
     * as the icon.
     *
     * Otherwise if we have no active connection and no active network,
     * we create a {@link PopupMenu.PopupImageMenuItem} as above but
     * show "Connected (private)" instead of the connection's name (as we
     * can't access that for some reason).
     *
     * In all cases, we store this item into {@link #_activeConnectionItem}.
     * Then we call {@link PopupMenu.PopupBaseMenuItem#setShowDot}
     * on it to show a dot to the left of the item, indicating that that
     * network is the one we are connected to.
     * @override */
    _createActiveConnectionItem: function() {
        let icon, title;
        if (this._activeConnection._connection) {
            let connection = this._activeConnection._connection;
            if (this._activeNetwork)
                this._activeConnectionItem = new NMNetworkMenuItem(this._activeNetwork.accessPoints, undefined,
                                                                   { reactive: false });
            else
                this._activeConnectionItem = new PopupMenu.PopupImageMenuItem(connection._name,
                                                                              'network-wireless-connected',
                                                                              { reactive: false });
        } else {
            // We cannot read the connection (due to ACL, or API incompatibility), but we still show signal if we have it
            let menuItem;
            if (this._activeNetwork)
                this._activeConnectionItem = new NMNetworkMenuItem(this._activeNetwork.accessPoints, undefined,
                                                                   { reactive: false });
            else
                this._activeConnectionItem = new PopupMenu.PopupImageMenuItem(_("Connected (private)"),
                                                                              'network-wireless-connected',
                                                                              { reactive: false });
        }
        this._activeConnectionItem.setShowDot(true);
    },

    /** Implements the parent function to create an automatic wireless
     * connection.
     *
     * We create a new {@link NetworkManager.Connection},
     * add a new (default) {@link NetworkManager.SettingWireless} to it,
     * and then add a {@link NetworkManager.SettingConnection}
     * to it. setting its UUID to a new randomly-generated one, its
     *
     * We set the SettingConnection's id to "Auto [`apObj.ssid` as a string]",
     * or "Auto wireless" if `apObj` doesn't have a SSID, `uuid` to a newly
     * generated UUID, and `autoconnect` to `true`.
     * @param {Network.WirelessNetworkObject} apObj - the network to create
     * an automatic connection for.
     * @override
     */
    _createAutomaticConnection: function(apObj) {
        let name;
        let ssid = NetworkManager.utils_ssid_to_utf8(apObj.ssid);
        if (ssid) {
            /* TRANSLATORS: this the automatic wireless connection name (including the network name) */
            name = _("Auto %s").format(ssid);
        } else
            name = _("Auto wireless");

        let connection = new NetworkManager.Connection();
        connection.add_setting(new NetworkManager.SettingWireless());
        connection.add_setting(new NetworkManager.SettingConnection({
            id: name,
            autoconnect: true, // NetworkManager will know to ignore this if appropriate
            uuid: NetworkManager.utils_uuid_generate(),
            type: NetworkManager.SETTING_WIRELESS_SETTING_NAME
        }));
        return connection;
    },

    /** This creates popup menu items for a particular network, represented
     * by `apObj`:
     *
     * * if `apObj` only has one connection (in `apObj.connections`), we
     * create a {@link NMNetworkMenuItem} for it, such that clicking
     * the item from the popup menu will connect to that network using
     * that one connection and the network's strongest access point
     * (see {@link #_createAPItem});
     * * if `apObj` has multiple connections available to it, we create a
     * [popup submenu]{@link PopupMenu.PopupSubMenuMenuItem} with title being
     * `apObj`'s SSID, and for each connection in `apObj.connections` we
     * create a *separate* {@link NMNetworkMenuItem} with
     * {@link #_createAPItem} and add it to the submenu.
     * * if `apObj` has *no* connections we create a {@link NMNetworkMenuItem}
     * for it such that clicking on the item will either launch
     * gnome-control-center's network applet to further configure the
     * connection (if the access point security is 802.1x), or will
     * attempt to create an automatic connection for `apObj` via
     * {@link #_createAutomaticConnection} and attempt to connect using
     * that (since `apObj` has no ready-made connections we can use).
     *
     * We then add the resulting item(s) into the popup menu at position
     * `position`. If `position` is at least {@link NUM_VISIBLE_NETWORKS},
     * we create a "More..." submenu and add the item(s) there instead.
     *
     * @param {Network.WirelessNetworkObject} apObj
     * @param {number} position - the position if this item within the
     * popup menu.
     * @TODO want a PIC with multiple connections!
     */
    _createNetworkItem: function(apObj, position) {
        if(!apObj.accessPoints || apObj.accessPoints.length == 0) {
            // this should not happen, but I have no idea why it happens
            return;
        }

        if(apObj.connections.length > 0) {
            if (apObj.connections.length == 1) {
                apObj.item = this._createAPItem(apObj.connections[0], apObj, false);
            } else {
                let title = apObj.ssidText;
                apObj.item = new PopupMenu.PopupSubMenuMenuItem(title);
                for (let i = 0; i < apObj.connections.length; i++)
                    apObj.item.menu.addMenuItem(this._createAPItem(apObj.connections[i], apObj, true));
            }
        } else {
            apObj.item = new NMNetworkMenuItem(apObj.accessPoints);
            apObj.item.connect('activate', Lang.bind(this, function() {
                let accessPoints = sortAccessPoints(apObj.accessPoints);
                if (   (accessPoints[0]._secType == NMAccessPointSecurity.WPA2_ENT)
                    || (accessPoints[0]._secType == NMAccessPointSecurity.WPA_ENT)) {
                    // 802.1x-enabled APs require further configuration, so they're
                    // handled in gnome-control-center
                    Util.spawn(['gnome-control-center', 'network', 'connect-8021x-wifi',
                                this.device.get_path(), accessPoints[0].dbus_path]);
                } else {
                    let connection = this._createAutomaticConnection(apObj);
                    this._client.add_and_activate_connection(connection, this.device, accessPoints[0].dbus_path, null)
                }
            }));
        }
        apObj.item._apObj = apObj;

        if (position < NUM_VISIBLE_NETWORKS) {
            apObj.isMore = false;
            this.section.addMenuItem(apObj.item, position);
        } else {
            if (!this._overflowItem) {
                this._overflowItem = new PopupMenu.PopupSubMenuMenuItem(_("More..."));
                this.section.addMenuItem(this._overflowItem);
            }
            this._overflowItem.menu.addMenuItem(apObj.item, position - NUM_VISIBLE_NETWORKS);
            apObj.isMore = true;
        }
    },

    /** Overrides the parent function to populate {@link #section} (popup menu
     * section) with items for each of our available networks.
     *
     * If we have an active connection {@link #_activeConnection} we create
     * an item for it ({@link #_createActiveConnectionItem}) and place it at
     * the top of the menu.
     *
     * Then for each network in {@link #_networks}, we make its
     * {@link NMNetworkMenuItem}(s) with {@link #_createNetworkItem} (if
     * a network has multiple connections it gets a submenu with a
     * {@link NMNetworkMenuItem} per connection; otherwise it just gets
     * a single {@link NMNetworkMenuItem}). Remember that if there are more
     * than {@link NUM_VISIBLE_NETWORKS} item in our menu already we put the 
     * rest into a "More..." submenu.
     * @override
     */
    _createSection: function() {
        if (!this._shouldShowConnectionList())
            return;

        if(this._activeConnection) {
            this._createActiveConnectionItem();
            this.section.addMenuItem(this._activeConnectionItem);
        }

        let activeOffset = this._activeConnectionItem ? 1 : 0;

        for(let j = 0; j < this._networks.length; j++) {
            let apObj = this._networks[j];
            if (apObj == this._activeNetwork)
                continue;

            this._createNetworkItem(apObj, j + activeOffset);
        }
    }
};

/** creates a new NMApplet
 * 
 * First we call the [parent constructor]{@link PanelMenu.SystemStatusButton}
 * with icon 'network-error' ![network-error](pics/icons/network-error-symbolic.svg).
 *
 * Then we create a new {@link NMClient.Client}
 * to get information from/interact with, stored in {@link NMNetworkMenuItem#_client}.
 *
 * This is the same `client` argument that gets fed to all the {@link NMDevice}
 * classes/subclasses.
 *
 * ![{@link NMApplet} menu showing two {@link NMDeviceWired} for the "Wired" section and one {@link NMDeviceWireless} for the "Wireless" section](pics/status.network.png)
 *
 * Then we construct our popup menu.
 *
 * The popup menu consists of one section per device category, where the
 * categories are "wired", "wireless", "wwan" (mobile broadband), and "vpn" -
 * also stored in {@link NMConnectionCategory}.
 *
 * Each category/section can have multiple devices (network interfaces) - for example,
 * you might have two wireless cards.
 *
 * So, we show a title item for each section ("Wired" or "Wireless"),
 * along with the popup menu for each {@link NMDevice}. This consists of
 * a title item for the device {@link NMDevice#statusItem}, showing the device's
 * name (e.g. "Atheros AR8132 Fast Ethernet"), along with a list of
 * available connections for that device (e.g. the list of available wireless
 * networks for the wireless card). Note that for wired interfaces this
 * list isn't shown unless there is more than one to save some clutter,
 * as typically a wired interface can only have one connection anyway
 * (that provided by the network it is plugged into).
 *
 * To store all this information, we have an object {@link #_devices}.
 *
 * Each key in {@link #_devices} is a {@link NMConnectionCategory} - 
 * 'wired', 'wwan' (mobile network), 'wireless', and 'vpn'.
 *
 * Each member of {@link #_devices} describes all the UI elements
 * for that section in the network menu:
 *
 * * `section`: a {@link PopupMenu.PopupMenuSection} where we'll put all the
 * popup menu items for this section;
 * * `devices`: an *array* of {@link NMDevice} subclasses for this section
 * (for example in the picture above I have two wired interfaces; the normal
 * one from my laptop's LAN port, and one from USB tethering to my phone).
 * * `item`: the title menu item for the section (the "Wired" or "Wireless"
 * items in the picture above). This is a {@link NMWiredSectionTitleMenuItem}
 * or {@link NMWirelessSectionTitleMenuItem}.
 *
 * The object described above is a {@link NMAppletSection}.
 *
 * In the constructor we initialise four {@link NMAppletSection}s (one
 * each for 'wired', 'wireless', 'wwan' and 'vpn') and store then into
 * {@link #_devices}.
 *
 * The wired and VPN sections have a {@link NMWiredSectionTitleMenuItem} as
 * the section title, whereas the wireless and mobile broadband sections
 * have a {@link NMWirelessSectionTitleMenuItem} (created in
 * {@link #_makeToggleItem}).
 *
 * The section titles and popup menu sections for each category are added
 * to our popup menu.
 *
 * The VPN device is slightly different in that it has just one
 * device, a {@link NMDeviceVPN}. This is a dummy device to simplify code
 * because the {@link NMDeviceVPN} has no underlying NetworkManager device.
 * We create a new {@link NMDeviceVPN}, store it as the only device for
 * this category, and call {@link NMWiredSectionTitleMenuItem#updateForDevice}
 * to set this device as the only device for that section, hence hiding
 * the device heading and deferring its on/off toggle and status text to
 * be shown in the section heading instead.
 *
 * We then create a number of hash tables for convenience {@link #_dtypes}
 * and {@link #_ctypes}, to map NetworkManager devices to {@link NMDevice}
 * subclasses and to map NetworkManager connections to particular
 * connection categories so they are added to the appropriate section in the
 * popup menu.
 *
 * Then we create a NMClient.RemoteSettings instance, {@link #_settings},
 * to retrieve all connections from (which we will then assign to the
 * appropriate sections).
 *
 * Upon all the connections being read from {@link #_settings}, we call
 * {@link #_readConnections}, {@link #_readDevices} and
 * {@link #_syncNMState}.
 * These read all the connections and devices from the network manager client and
 * assign them to their appropriate sections/categories (wired, wireless, ...).
 * @classdesc
 *
 * This is the network manager status icon and associated popup menu.
 *
 * ![{@link NMApplet} menu showing two {@link NMDeviceWired} for the "Wired" section and one {@link NMDeviceWireless} for the "Wireless" section](pics/status.network.png)
 *
 * Its popup menu consists of four sections, "Wireless", "Wired",
 * "VPN" and "Mobile Broadband" (also known as 'wwan').
 *
 * Each section has a popup menu item that acts as the "title" for
 * that section. This is either a {@link NMWiredSectionTitleMenuItem}
 * or a {@link NMWirelessSectionTitleMenuItem}. Essentially it is a popup
 * menu item with the section title written on it ("Wired", "VPN Connections", ...).
 *
 * Under each section title item, we display the popup menu for
 * each device (network interface) for that section.
 *
 * The popup menu for each device is managed by the appropriate subclass
 * of {@link NMDevice}. For example if you have two wireless cards you will have one
 * {@link NMDeviceWireless} for each card.
 *
 * Each Device's popup menu consists of:
 *
 * * a device "title" PopupSwitchMenuItem showing the device's name (e.g. "Atheros AR8132
 * Fast Ethernet"), which either has an on/off toggle to enable/disable it,
 * or a status text (like "cable unplugged", "disconnecting...") - this is
 * {@link NMDevice#statusItem}.
 * * under the device title, a popup menu section showing all the available
 * connections for this device. For example a wireless device will show a
 * popup menu item for each network that is available to connect to - this
 * is {@link NMDevice#section}.
 *
 * Note that for wired devices ({@link NMDeviceWired}) the list of available
 * connections is hidden unless there are at least two (because typically
 * each wired interface will have just one available connection - that of
 * the cable that is plugged in).
 *
 * Now, if a particular section on the NMApplet menu ("Wired", "Wireless", ...)
 * has *only one* {@link NMDevice} for it, we *hide* the device heading
 * {@link NMDevice#statusItem} and instead combine it with the *section*
 * heading. That is, the on/off toggle and connection status will be shown
 * on the section heading ("Wired") instead of the device heading
 * ("Atheros AR8132 Fast Ethernet"), which is hidden. This is to avoid
 * the redundant "Wired" as well as "Device Name" headings when there is
 * only one device for the wired section.
 *
 * In the picture above, there are two wired interfaces (one is my laptop's
 * ethernet port and the other is from USB tethering to my phone), so
 * each {@link NMDeviceWired#statusItem} is shown under the "Wired"
 * section heading.
 *
 * However there's only *one* wireless interface (I only have one wireless
 * card) so the heading for that device, {@link NMDeviceWireless#statusItem},
 * has been hidden and combined into the wireless section heading ("Wireless").
 *
 * Finally, if there are *no* devices for a particular interface, that
 * section is hidden entirely from the menu (for example, I have no capability
 * to do mobile broadband).
 *
 * @class
 * @extends PanelMenu.SystemStatusButton
 */
function NMApplet() {
    this._init.apply(this, arguments);
}
NMApplet.prototype = {
    __proto__: PanelMenu.SystemStatusButton.prototype,

    _init: function() {
        PanelMenu.SystemStatusButton.prototype._init.call(this, 'network-error');

        /** Underlying instance of a network manager client that we
         * use for information and connecting.
         * @type {NMClient.Client}
         */
        this._client = NMClient.Client.new();

        this._statusSection = new PopupMenu.PopupMenuSection();
        this._statusItem = new PopupMenu.PopupMenuItem('', { style_class: 'popup-inactive-menu-item', reactive: false });
        this._statusSection.addMenuItem(this._statusItem);
        this._statusSection.addAction(_("Enable networking"), Lang.bind(this, function() {
            this._client.networking_enabled = true;
        }));
        this._statusSection.actor.hide();
        this.menu.addMenuItem(this._statusSection);
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        /** We populate this object with information about each section on
         * our menu: wired, wireless, wwan, and VPN connection.
         *
         * Each element is an object with properties:
         *
         * * 'section': as a PopupMenuSection where we'll display the popup
         * menu items for each device in this category (wired, wireless, wwan,
         * vpn);
         * * 'devices': an array of {@link NMDevice}s that belong in this
         * section;
         * * 'item': the section title popup menu item
         * ({@link NMWiredSectionTitleMenuItem},
         * {@link NMWirelessSectionTitleMenuItem}).
         * @type {Object.<string, [NMAppletSection]{NetworkManager.NMAppletSection}
         */
        this._devices = { };

        this._devices.wired = {
            section: new PopupMenu.PopupMenuSection(),
            devices: [ ],
            item: new NMWiredSectionTitleMenuItem(_("Wired"))
        };

        this._devices.wired.section.addMenuItem(this._devices.wired.item);
        this._devices.wired.section.actor.hide();
        this.menu.addMenuItem(this._devices.wired.section);
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this._devices.wireless = {
            section: new PopupMenu.PopupMenuSection(),
            devices: [ ],
            item: this._makeToggleItem('wireless', _("Wireless"))
        };
        this._devices.wireless.section.addMenuItem(this._devices.wireless.item);
        this._devices.wireless.section.actor.hide();
        this.menu.addMenuItem(this._devices.wireless.section);
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this._devices.wwan = {
            section: new PopupMenu.PopupMenuSection(),
            devices: [ ],
            item: this._makeToggleItem('wwan', _("Mobile broadband"))
        };
        this._devices.wwan.section.addMenuItem(this._devices.wwan.item);
        this._devices.wwan.section.actor.hide();
        this.menu.addMenuItem(this._devices.wwan.section);
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this._devices.vpn = {
            section: new PopupMenu.PopupMenuSection(),
            device: new NMDeviceVPN(this._client),
            item: new NMWiredSectionTitleMenuItem(_("VPN Connections"))
        };
        this._devices.vpn.device.connect('active-connection-changed', Lang.bind(this, function() {
            this._devices.vpn.item.updateForDevice(this._devices.vpn.device);
        }));
        this._devices.vpn.item.updateForDevice(this._devices.vpn.device);
        this._devices.vpn.section.addMenuItem(this._devices.vpn.item);
        this._devices.vpn.section.addMenuItem(this._devices.vpn.device.section);
        this._devices.vpn.section.actor.hide();
        this.menu.addMenuItem(this._devices.vpn.section);
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
        this.menu.addSettingsAction(_("Network Settings"), 'gnome-network-panel.desktop');

        /** Array of NMActiveConnections for the client.
         * @type {NMClient.ActiveConnection[]} */
        this._activeConnections = [ ];
        /** Array of {@link NMClient.RemoteConnection}s for the client.
         * @type {NMClient.RemoteConnection[]} */
        this._connections = [ ];

         /** The "main" active connection. This is the connection that the
          * icon in the top panel will show the status of.
          *
          * It is usually the connection for the network we are currently
          * connected to (typically you are only connected to one network
          * at a time!)
          *
          * If `null`, it means we aren't connected to *any* networks at all.
          * @see #_syncActiveConnections
         * @type {?NMClient.ActiveConnection} */
        this._mainConnection = null;
        this._activeAccessPointUpdateId = 0;
        this._activeAccessPoint = null;
        this._mobileUpdateId = 0;
        this._mobileUpdateDevice = null;

        /** Device types hash map.
         *
         * This maps `NetworkManager.DeviceType`s to the appropriate subclass
         * of {@link NMDevice}:
         *
         * * `NetworkManager.DeviceType.ETHERNET` is a {@link NMDeviceWired};
         * * `NetworkManager.DeviceType.WIFI` is a {@link NMDeviceWireless};
         * * `NetworkManager.DeviceType.MODEm` is a {@link NMDeviceModem};
         * * `NetworkManager.DeviceType.BT` is a {@link NMDeviceBluetooth};
         * @type {Object.<NetworkManager.DeviceType, {@link Network.NMDevice}>}
         */
        this._dtypes = { };
        this._dtypes[NetworkManager.DeviceType.ETHERNET] = NMDeviceWired;
        this._dtypes[NetworkManager.DeviceType.WIFI] = NMDeviceWireless;
        this._dtypes[NetworkManager.DeviceType.MODEM] = NMDeviceModem;
        this._dtypes[NetworkManager.DeviceType.BT] = NMDeviceBluetooth;
        // TODO: WiMax support

        /** Connection types hash map.
         *
         * This maps a connection to a {@link NMConnectionCategory} so that
         * connections can be assigned to the appropriate section in
         * {@link #_devices}.
         * @type {Object.<string, {@link Network.NMConnectionCategory}>}
         */
        this._ctypes = { };
        this._ctypes[NetworkManager.SETTING_WIRELESS_SETTING_NAME] = NMConnectionCategory.WIRELESS;
        this._ctypes[NetworkManager.SETTING_WIRED_SETTING_NAME] = NMConnectionCategory.WIRED;
        this._ctypes[NetworkManager.SETTING_PPPOE_SETTING_NAME] = NMConnectionCategory.WIRED;
        this._ctypes[NetworkManager.SETTING_PPP_SETTING_NAME] = NMConnectionCategory.WIRED;
        this._ctypes[NetworkManager.SETTING_BLUETOOTH_SETTING_NAME] = NMConnectionCategory.WWAN;
        this._ctypes[NetworkManager.SETTING_CDMA_SETTING_NAME] = NMConnectionCategory.WWAN;
        this._ctypes[NetworkManager.SETTING_GSM_SETTING_NAME] = NMConnectionCategory.WWAN;
        this._ctypes[NetworkManager.SETTING_VPN_SETTING_NAME] = NMConnectionCategory.VPN;

        /** Remote settings. Used to get connections from.
         * @type {NMClient.RemoteSettings}
         */
        this._settings = NMClient.RemoteSettings.new(null);
        this._connectionsReadId = this._settings.connect('connections-read', Lang.bind(this, function() {
            this._readConnections();
            this._readDevices();
            this._syncNMState();

            // Connect to signals late so that early signals don't find in inconsistent state
            // and connect only once (this signal handler can be called again if NetworkManager goes up and down)
            if (!this._inited) {
                this._inited = true;
                this._client.connect('notify::manager-running', Lang.bind(this, this._syncNMState));
                this._client.connect('notify::networking-enabled', Lang.bind(this, this._syncNMState));
                this._client.connect('notify::state', Lang.bind(this, this._syncNMState));
                this._client.connect('notify::active-connections', Lang.bind(this, this._updateIcon));
                this._client.connect('device-added', Lang.bind(this, this._deviceAdded));
                this._client.connect('device-removed', Lang.bind(this, this._deviceRemoved));
                this._settings.connect('new-connection', Lang.bind(this, this._newConnection));
            }
        }));
    },

    /** The {@link NMApplet} uses the one same message tray Source for all
     * its notifications (a {@link NMMessageTraySource}).
     *
     * It's only created on-demand, so calling this function ensures that
     * the source is created and added to the message tray, if it hasn't
     * been already.
     * 
     * It's stored in {@link #_source}.
     * @see NMMessageTraySource
     */
    _ensureSource: function() {
        if (!this._source) {
            this._source = new NMMessageTraySource();
            this._source.connect('destroy', Lang.bind(this, function() {
                /** The Source we use for all our notifications (only
                 * created on demand, so `null` if we haven't had to notify
                 * anything yet).
                 * @type {?Network.NMMessageTraySource} */
                this._source = null;
            }));
            Main.messageTray.add(this._source);
        }
    },

    /** Makes a section title popup menu item for a wireless category
     * (i.e. wireless and mobile broadband).
     * 
     * This is a {@link NMWirelessSectionTitleMenuItem}.
     *
     * It also connects to {@link NMWirelessSectionTitleMenuItem.enabled-changed}
     * ensuring that each device for the section is enabled/disabled when that section
     * is hardware or software enabled or disabled.
     *
     * @param {Network.NMConnectionCategory} type - the category of
     * connection ('wwan' or 'wireless')
     * @param {string} title - the label to show on the title ("Wireless",
     * "Mobile Broadband")
     * @returns {Network.NMWirelessSectionTitleMenuItem}
     */
    _makeToggleItem: function(type, title) {
        let item = new NMWirelessSectionTitleMenuItem(this._client, type, title);
        item.connect('enabled-changed', Lang.bind(this, function(item, enabled) {
            let devices = this._devices[type].devices;
            devices.forEach(function(dev) {
                dev.setEnabled(enabled);
            });
            this._syncSectionTitle(type);
        }));
        return item;
    },

    /** Synchronises the section title popup menu item for a particular
     * category/section in the popup menu.
     *
     * If the section (e.g. "Wired", "Wireless") has:
     *
     * * no devices: the entire section is hidden.
     * * one device: we hide the device's popup menu item
     * {@link NMDevice#statusItem} and call
     * {@link NMWiredSectionTitleMenuItem#updateForDevice} or
     * {@link NMWirelessSectionTitleMenuItem#updateForDevice} with the one
     * device which puts the status text/toggle from the (now-hidden)
     * device status item into the section title item instead.
     * * more than one device: ensure that the section title doesn't show
     * any status text (ie it just shows "Wired"), and ensure that each
     * device's status item (status text and toggle) is showing.
     * @param {Network.NMConnectionCategory} category - category that the
     * title is for ('wireless', 'wired', 'vpn', 'wwan').
     * @see NMWiredSectionTitleMenuItem#updateForDevice
     * @see NMWirelessSectionTitleMenuItem#updateForDevice
     * @see NMdevice#statusItem
     */
    _syncSectionTitle: function(category) {
        let devices = this._devices[category].devices;
        let item = this._devices[category].item;
        let section = this._devices[category].section;
        if (devices.length == 0)
            section.actor.hide();
        else {
            section.actor.show();
            if (devices.length == 1) {
                let dev = devices[0];
                dev.statusItem.actor.hide();
                item.updateForDevice(dev);
            } else {
                devices.forEach(function(dev) {
                    dev.statusItem.actor.show();
                });
                // remove status text from the section title item
                item.updateForDevice(null);
            }
        }
    },

    /** Gets all the devices from {@link #_client} and adds them
     * with {@link #_deviceAdded}.
     * @see #_deviceAdded */
    _readDevices: function() {
        let devices = this._client.get_devices() || [ ];
        for (let i = 0; i < devices.length; ++i) {
            this._deviceAdded(this._client, devices[i]);
        }
    },

    /** Sends a notification from a particular device and notifies
     * it from our source {@link #_source}, replacing any previous notification
     * from that device.
     * @param {NMDevice} device - device to notify about (we store
     * the notification in `device._notification`: can only have one
     * notification per device at a time).
     * @param {string} iconName - name of the icon to use for the
     * notification.
     * @param {string} title - title for the notification
     * @param {string} text - what to show in the body of the notification
     * @param {MessageTray.Urgency} urgency - urgency of the notification.
     */
    _notifyForDevice: function(device, iconName, title, text, urgency) {
        if (device._notification)
            device._notification.destroy();

        /* must call after destroying previous notification,
           or this._source will be cleared */
        this._ensureSource();

        let icon = new St.Icon({ icon_name: iconName,
                                 icon_type: St.IconType.SYMBOLIC,
                                 icon_size: this._source.ICON_SIZE
                               });
        device._notification = new MessageTray.Notification(this._source, title, text,
                                                            { icon: icon });
        device._notification.setUrgency(urgency);
        device._notification.setTransient(true);
        device._notification.connect('destroy', function() {
            device._notification = null;
        });
        this._source.notify(device._notification);
    },

    /** Callback when a device is added to the client {@link #_client}.
     *
     * This creates a new {@link NMDevice} for `device` (use
     * {@link #_dtypes} to look up which {@link NMDevice} subclass
     * corresponds to `device`'s type), called the "wrapper" for the device.
     *
     * We connect to wrapper's ['activation-failed']{@link NMDevice.activation-failed}
     * signal and upon receiving it, we send a notification to the user
     * with title "Connection failed", body "Activation of network connection
     * failed", and HIGH urgency.
     *
     * We also connect to the wrapper's ['state-changed']{@link NMDevice.state-changed}
     * signal, updating the section popup menu items ({@link #_syncSectionTitle})
     * when this happens. (If a device's status changes to UNAVAILABLE it might
     * be removed, meaning we need to update whether or not the section title
     * shows the status of a particular device).
     *
     * We add the wrapper's popup menu items {@link NMDevice#section}
     * and {@link NMDevice#statusItem} to the appropriate section
     * (i.e. `this._devices[wrapper.category].section`) and add the wrapper
     * to the list of devices for that section (`this._devices[wrapper.category].devices`).
     *
     * Finally we call {@link #_syncSectionTitle} on that section to ensure
     * that the popup menu for that section is shown if there is at least
     * one device for it, and update whether the section title shows
     * the status text of a device (if there is only one device) or not
     * (multiple devices).
     * @see NMAppletSection
     * @param {NMClient.Client} client - the NMClient.Client instance that
     * emitted the signal ({@link #_client}).
     * @param {NMClient.Device} device - the device
     */
    _deviceAdded: function(client, device) {
        if (device._delegate) {
            // already seen, not adding again
            return;
        }
        let wrapperClass = this._dtypes[device.get_device_type()];
        if (wrapperClass) {
            let wrapper = new wrapperClass(this._client, device, this._connections);

            wrapper._activationFailedId = wrapper.connect('activation-failed', Lang.bind(this, function(device, reason) {
                // XXX: nm-applet has no special text depending on reason
                // but I'm not sure of this generic message
                this._notifyForDevice(device, 'network-error',
                                      _("Connection failed"),
                                      _("Activation of network connection failed"),
                                     MessageTray.Urgency.HIGH);
            }));
            wrapper._deviceStateChangedId = wrapper.connect('state-changed', Lang.bind(this, function(dev) {
                this._syncSectionTitle(dev.category);
            }));
            // Note - NMDevice's do not have a 'destroy' signal!
            wrapper._destroyId = wrapper.connect('destroy', function(wrapper) {
                wrapper.disconnect(wrapper._activationFailedId);
                wrapper.disconnect(wrapper._deviceStateChangedId);
                wrapper.disconnect(wrapper._destroyId);
            });
            let section = this._devices[wrapper.category].section;
            let devices = this._devices[wrapper.category].devices;

            section.addMenuItem(wrapper.section, 1);
            section.addMenuItem(wrapper.statusItem, 1);
            devices.push(wrapper);

            this._syncSectionTitle(wrapper.category);
        } else
            log('Invalid network device type, is ' + device.get_device_type());
    },

    /** Callback when a device is removed from the client {@link #_client}.
     *
     * We remove the device from the appropriate section of {@link #_devices}.
     *
     * Then we call {@link #_syncSectionTitle} to update the popup menu to
     * reflect the new number of devices for that section.
     * @see #_syncSectionTitle
     * @inheritparams #_deviceAdded
     */
    _deviceRemoved: function(client, device) {
        if (!device._delegate) {
            log('Removing a network device that was not added');
            return;
        }

        let wrapper = device._delegate;
        wrapper.destroy();

        let devices = this._devices[wrapper.category].devices;
        let pos = devices.indexOf(wrapper);
        devices.splice(pos, 1);

        this._syncSectionTitle(wrapper.category)
    },

    /**
     * We retrieve the active connections from {@link #_client} and
     * compare it against {@link #_activeConnections} to create
     * an array of connections that have been closed and new ones
     * that have been added.
     *
     * For each closed connection, we look up its {@link NMDevice} and
     * call {@link NMDevice#setActiveConnection} with `null` to remove the
     * connection as an active one. We also disconnect any signals we
     * were listening to on it.
     *
     * Then we set {@link #_activeConnections} to the new active
     * connections.
     *
     * For each of these active connections:
     *
     * * we connect to a number of signals of the active connection (for example
     * when it changes from activated to not activated).
     * * we find the NMClient.RemoteConnection corresponding to the active
     * connection and store it.
     * * we work out which section ('wwan', 'wireless', 'wired', 'vpn')
     * the active connection belongs to
     * * we see if any of the active connection's devices (from
     * `nm_active_connection_get_devices`) already have a {@link NMDevice}
     * made for them, and if so call {@link NMDevice#setActiveConnection}
     * with this connection.
     * * if there are any outstanding "account failure" notifications on this
     * device (corresponding to the active connection), we remove them.
     *
     * We then set our main connection (the one that we will show the icon
     * for in the top panel) {@link #_mainConnection} to the first of these
     * that is non-null:
     *
     * + a NMActiveConnection that is in the process of activating, OR
     * + a NMActiveConnection that is the default IPv4 connection, OR
     * + a NMActiveConnection that is the default IPv6 connection, OR
     * + the first NMActiveConnection in the list.
     */
    _syncActiveConnections: function() {
        let closedConnections = [ ];
        let newActiveConnections = this._client.get_active_connections() || [ ];
        for (let i = 0; i < this._activeConnections.length; i++) {
            let a = this._activeConnections[i];
            if (newActiveConnections.indexOf(a) == -1) // connection is removed
                closedConnections.push(a);
        }

        for (let i = 0; i < closedConnections.length; i++) {
            let active = closedConnections[i];
            if (active._primaryDevice) {
                active._primaryDevice.setActiveConnection(null);
                active._primaryDevice = null;
            }
            if (active._inited) {
                active.disconnect(active._notifyStateId);
                active.disconnect(active._notifyDefaultId);
                active.disconnect(active._notifyDefault6Id);
                active._inited = false;
            }
        }

        this._activeConnections = newActiveConnections;
        this._mainConnection = null;
        let activating = null;
        let default_ip4 = null;
        let default_ip6 = null;
        for (let i = 0; i < this._activeConnections.length; i++) {
            let a = this._activeConnections[i];

            if (!a._inited) {
                a._notifyDefaultId = a.connect('notify::default', Lang.bind(this, this._updateIcon));
                a._notifyDefault6Id = a.connect('notify::default6', Lang.bind(this, this._updateIcon));
                a._notifyStateId = a.connect('notify::state', Lang.bind(this, this._notifyActivated));

                a._inited = true;
            }

            if (!a._connection) {
                a._connection = this._settings.get_connection_by_path(a.connection);

                if (a._connection) {
                    a._type = a._connection._type;
                    a._section = this._ctypes[a._type];
                } else {
                    a._connection = null;
                    a._type = null;
                    a._section = null;
                    log('Cannot find connection for active (or connection cannot be read)');
                }
            }

            if (a['default'])
                default_ip4 = a;
            if (a.default6)
                default_ip6 = a;

            if (a.state == NetworkManager.ActiveConnectionState.ACTIVATING)
                activating = a;

            if (!a._primaryDevice) {
                if (a._type != NetworkManager.SETTING_VPN_SETTING_NAME) {
                    // find a good device to be considered primary
                    a._primaryDevice = null;
                    let devices = a.get_devices();
                    for (let j = 0; j < devices.length; j++) {
                        let d = devices[j];
                        if (d._delegate) {
                            a._primaryDevice = d._delegate;
                            break;
                        }
                    }
                } else
                    a._primaryDevice = this._devices.vpn.device

                if (a._primaryDevice)
                    a._primaryDevice.setActiveConnection(a);

                if (a.state == NetworkManager.ActiveConnectionState.ACTIVATED
                    && a._primaryDevice && a._primaryDevice._notification) {
                    a._primaryDevice._notification.destroy();
                    a._primaryDevice._notification = null;
                }
            }
        }

        this._mainConnection = activating || default_ip4 || default_ip6 || this._activeConnections[0] || null;
    },

    /** Callback when an activate connection's 'state' property changes (ACTIVATED/
     * ACTIVATING/DEACTIVATING).
     *
     * We call {@link #_updateIcon} which updates the icon shown for the
     * NMApplet in the top panel to reflect that of the active connection
     * (for example a wireless icon will show if we're using wireless).
     *
     * If the connection has become activated we remove any pending
     * 'activation failed' notifications
     * for the connection's NMDevice.
     * 
     * @see NetworkManager.ActiveConnectionState
     */
    _notifyActivated: function(activeConnection) {
        if (activeConnection.state == NetworkManager.ActiveConnectionState.ACTIVATED
            && activeConnection._primaryDevice && activeConnection._primaryDevice._notification) {
            activeConnection._primaryDevice._notification.destroy();
            activeConnection._primaryDevice._notification = null;
        }

        this._updateIcon();
    },

    /** Reads all the {@link NMClient.RemoteConnection}s from {@link #_settings},
     * calls {@link #_updateConnection} on each (which will add it to
     * each appropriate {@link NMDevice}) and adding it to
     * {@link #_connections}.
     *
     * @see [`nm_remote_settings_list_connections`](http://developer.gnome.org/libnm-glib/unstable/NMRemoteSettings.html#nm-remote-settings-list-connections)
     */
    _readConnections: function() {
        let connections = this._settings.list_connections();
        for (let i = 0; i < connections.length; i++) {
            let connection = connections[i];
            if (connection._uuid) {
                // connection was already seen (for example because NetworkManager was restarted)
                continue;
            }
            connection._removedId = connection.connect('removed', Lang.bind(this, this._connectionRemoved));
            connection._updatedId = connection.connect('updated', Lang.bind(this, this._updateConnection));

            this._updateConnection(connection);
            this._connections.push(connection);
        }
    },

    /** Callback when {@link #_settings} gets a new connection.
     *
     * We add the connection to {@link #_connections}, call
     * {@link #_updateConnection} to add it to any relevant {@link NMDevice}s,
     * and call {@link #_updateIcon} to update the icon shown in the top
     * panel in case this connection changes anything.
     */
    _newConnection: function(settings, connection) {
        if (connection._uuid) {
            // connection was already seen
            return;
        }

        connection._removedId = connection.connect('removed', Lang.bind(this, this._connectionRemoved));
        connection._updatedId = connection.connect('updated', Lang.bind(this, this._updateConnection));

        this._updateConnection(connection);
        this._connections.push(connection);

        this._updateIcon();
    },

    /** Callback when a NMClient.RemoteConnection is removed.
     *
     * This removes it from {@link #_connections} and calls
     * {@link NMDevice#removeConnection} for each device in that connection's
     * section to remove the connection from each device it was added to.
     *
     * @inheritparams #_updateConnection
     * @see NMDevice#removeConnection
     */
    _connectionRemoved: function(connection) {
        let pos = this._connections.indexOf(connection);
        if (pos != -1)
            this._connections.splice(connection);

        let section = connection._section;

        if (section == NMConnectionCategory.VPN) {
            this._devices.vpn.device.removeConnection(connection);
            if (this._devices.vpn.device.empty)
                this._devices.vpn.section.actor.hide();
        } else if (section != NMConnectionCategory.INVALID) {
            let devices = this._devices[section].devices;
            for (let i = 0; i < devices.length; i++)
                devices[i].removeConnection(connection);
        }

        connection._uuid = null;
        connection.disconnect(connection._removedId);
        connection.disconnect(connection._updatedId);
    },

    /** Callback when a connection is updated (or call it upon receiving
     * a new connection).
     *
     * This caches some metadata about the connection for convenience which
     * the various {@link NMDevice} subclasses use to create
     * {@link ConnectionData} for each connection.
     *
     * We store (or update):
     *
     * * the section the connection belongs to, using hash map
     * {@link #_ctypes} (e.g. 'wired', 'wireless', ...),
     * * the connection's ID (human readable name),
     * * the connection's UUID,
     * * the timestamp of the most recent activation of this connection
     *
     * For each device in the same section as the connection (e.g. all
     * wired devices), we call {@link NMDevice#checkConnection} which will
     * add the connection to the device if relevant to the device.
     * @inheritparams NMDevice#checkConnection
     */
    _updateConnection: function(connection) {
        let connectionSettings = connection.get_setting_by_name(NetworkManager.SETTING_CONNECTION_SETTING_NAME);
        connection._type = connectionSettings.type;

        connection._section = this._ctypes[connection._type] || NMConnectionCategory.INVALID;
        connection._name = connectionSettings.id;
        connection._uuid = connectionSettings.uuid;
        connection._timestamp = connectionSettings.timestamp;

        let section = connection._section;

        if (connection._section == NMConnectionCategory.INVALID)
            return;
        if (section == NMConnectionCategory.VPN) {
            this._devices.vpn.device.checkConnection(connection);
            this._devices.vpn.section.actor.show();
        } else {
            let devices = this._devices[section].devices;
            for (let i = 0; i < devices.length; i++) {
                devices[i].checkConnection(connection);
            }
        }
    },

    /** Hides all the sections/devices from the network applet's popup
     * menu.
     *
     * This means all the "Wired", "Wireless", "Mobile broadband" and "VPN"
     * section titles along with any of their devices (basically anything
     * in {@link #_devices}), and leaves just the "Network Settings"
     * item (and possibly a "Networking is disabled" item, see
     * {@link #_syncNMState}).
     *
     * Undo this with {@link #_showNormal}.
     * @see #_syncNMState
     */
    _hideDevices: function() {
        this._devicesHidden = true;

        for (let category in this._devices)
            this._devices[category].section.actor.hide();
    },

    /** Restores the network applet's popup menu to its usual state,
     * ie undoes {@link #_hideDevices}.
     *
     * This hides the "Networking is disabled..." item if it's present,
     * and syncs all the sections to ensure the correct menu items are
     * showing with {@link #_syncSectionTitle} (which re-shows them if
     * they were hidden with {@link #_hideDevices}).
     * @see #_syncNMState
     */
    _showNormal: function() {
        if (!this._devicesHidden) // nothing to do
            return;
        this._devicesHidden = false;

        this._statusSection.actor.hide();

        this._syncSectionTitle('wired');
        this._syncSectionTitle('wireless');
        this._syncSectionTitle('wwan');

        if (!this._devices.vpn.device.empty)
            this._devices.vpn.section.actor.show();
    },

    /** Querie's NetworkManager's state and updates the icon in the top
     * panel and popup menu to reflect that.
     *
     * If network manager is not running, we hide the system status button
     * completely.
     *
     * If it's running but networking is not enabled, we set the
     * icon to 'network-offline'
     * ![network-offline](pics/icons/network-offline-symbolic.svg), add
     * an item to the menu "Networking is disabled", and hide all the other
     * sections in the menu ({@link #_hideDevices}.
     *
     * If it's running and enabled we call {@link #_showNormal} which
     * ensures the right popup menu items are showing. We also call
     * {@link #_updateIcon} to make sure the icon in the top panel reflects
     * our current status.
     */
    _syncNMState: function() {
        if (!this._client.manager_running) {
            log('NetworkManager is not running, hiding...');
            this.menu.close();
            this.actor.hide();
            return;
        } else
            this.actor.show();

        if (!this._client.networking_enabled) {
            this.setIcon('network-offline');
            this._hideDevices();
            this._statusItem.label.text = _("Networking is disabled");
            this._statusSection.actor.show();
            return;
        }

        this._showNormal();
        this._updateIcon();
    },

    /** This updates the icon shown for the NMApplet in the top panel to
     * be in sync with our current connection.
     *
     * We call {@link #_syncActiveConnections} to work out our primary
     * connection {@link #_mainConnection}.
     * We will show the icon corresponding to this connection.
     *
     * If {@link #_mainConnection} is `null` it means we are not currently
     * connected to any network. We set our icon to 'network-offline'
     * ![network-offline](pics/icons/network-offline-symbolic.svg).
     *
     * Otherwise if it is in the process of activating, we show the
     * appropriate icon:
     * ![network-cellular-acquiring](pics/icons/network-cellular-acquiring-symbolic.svg)
     * ![network-wireless-acquiring](pics/icons/network-wireless-acquiring-symbolic.svg)
     * ![network-wired-acquiring](pics/icons/network-wired-acquiring-symbolic.svg)
     * ![network-vpn-acquiring](pics/icons/network-vpn-acquiring-symbolic.svg).
     *
     * Otherwise, the connection is activated.
     *
     * If the connection is wireless, we
     * show the icon corresponding to the strongest access point's signal
     * strength:
     * ![network-wireless-signal-excellent](pics/icons/network-wireless-signal-excellent-symbolic.svg)
     * ![network-wireless-signal-good](pics/icons/network-wireless-signal-good-symbolic.svg)
     * ![network-wireless-signal-ok](pics/icons/network-wireless-signal-ok-symbolic.svg)
     * ![network-wireless-signal-weak](pics/icons/network-wireless-signal-weak-symbolic.svg)
     * ![network-wireless-signal-none](pics/icons/network-wireless-signal-none-symbolic.svg),
     * or ![network-wireless-connected](pics/icons/network-wireless-connected-symbolic.svg)
     * if there was any issue working this out.
     *
     * If it's a wired connection, we use
     * ![network-wired](pics/icons/network-wired-symbolic.svg)
     *
     * If it's mobile broadband, we use the icon corresponding to the
     * cellular signal strength:
     * ![network-cellular-signal-excellent](pics/icons/network-cellular-signal-excellent-symbolic.svg)
     * ![network-cellular-signal-good](pics/icons/network-cellular-signal-good-symbolic.svg)
     * ![network-cellular-signal-ok](pics/icons/network-cellular-signal-ok-symbolic.svg)
     * ![network-cellular-signal-weak](pics/icons/network-cellular-signal-weak-symbolic.svg)
     * ![network-cellular-signal-none](pics/icons/network-cellular-signal-none-symbolic.svg),
     * or ![network-cellular-connected](pics/icons/network-cellular-connected-symbolic.svg)
     * if there was any issue working this out.
     *
     * If it's a VPN, we use
     * ![network-vpn](pics/icons/network-vpn-symbolic.svg)
     *
     * Otherwise (some other connection we couldn't work out), we default
     * to the wired icon
     * ![network-wired](pics/icons/network-wired-symbolic.svg)
     * @see #setIcon
     */
    _updateIcon: function() {
        this._syncActiveConnections();
        let mc = this._mainConnection;
        let hasApIcon = false;
        let hasMobileIcon = false;

        if (!mc) {
            this.setIcon('network-offline');
        } else if (mc.state == NetworkManager.ActiveConnectionState.ACTIVATING) {
            switch (mc._section) {
            case NMConnectionCategory.WWAN:
                this.setIcon('network-cellular-acquiring');
                break;
            case NMConnectionCategory.WIRELESS:
                this.setIcon('network-wireless-acquiring');
                break;
            case NMConnectionCategory.WIRED:
                this.setIcon('network-wired-acquiring');
                break;
            case NMConnectionCategory.VPN:
                this.setIcon('network-vpn-acquiring');
                break;
            default:
                // fallback to a generic connected icon
                // (it could be a private connection of some other user)
                this.setIcon('network-wired-acquiring');
            }
        } else {
            let dev;
            switch (mc._section) {
            case NMConnectionCategory.WIRELESS:
                dev = mc._primaryDevice;
                if (dev) {
                    let ap = dev.device.active_access_point;
                    let mode = dev.device.mode;
                    if (!ap) {
                        if (mode != NM80211Mode.ADHOC) {
                            log('An active wireless connection, in infrastructure mode, involves no access point?');
                            break;
                        }
                        this.setIcon('network-wireless-connected');
                    } else {
                        if (this._accessPointUpdateId && this._activeAccessPoint != ap) {
                            this._activeAccessPoint.disconnect(this._accessPointUpdateId);
                            this._activeAccessPoint = ap;
                            this._activeAccessPointUpdateId = ap.connect('notify::strength', Lang.bind(function() {
                                this.setIcon('network-wireless-signal-' + signalToIcon(ap.strength));
                            }));
                        }
                        this.setIcon('network-wireless-signal-' + signalToIcon(ap.strength));
                        hasApIcon = true;
                    }
                    break;
                } else {
                    log('Active connection with no primary device?');
                    break;
                }
            case NMConnectionCategory.WIRED:
                this.setIcon('network-wired');
                break;
            case NMConnectionCategory.WWAN:
                dev = mc._primaryDevice;
                if (!dev) {
                    log('Active connection with no primary device?');
                    break;
                }
                if (!dev.mobileDevice) {
                    // this can happen for bluetooth in PAN mode
                    this.setIcon('network-cellular-connected');
                    break;
                }

                if (this._mobileUpdateId && this._mobileUpdateDevice != dev) {
                    this._mobileUpdateDevice.disconnect(this._mobileUpdateId);
                    this._mobileUpdateDevice = dev.mobileDevice;
                    this._mobileUpdateId = dev.mobileDevice.connect('notify::signal-quality', Lang.bind(this, function() {
                        this.setIcon('network-cellular-signal-' + signalToIcon(dev.mobileDevice.signal_quality));
                    }));
                }
                this.setIcon('network-cellular-signal-' + signalToIcon(dev.mobileDevice.signal_quality));
                hasMobileIcon = true;
                break;
            case NMConnectionCategory.VPN:
                this.setIcon('network-vpn');
                break;
            default:
                // fallback to a generic connected icon
                // (it could be a private connection of some other user)
                this.setIcon('network-wired');
                break;
            }
        }

        // cleanup stale signal connections

        if (!hasApIcon && this._activeAccessPointUpdateId) {
            this._activeAccessPoint.disconnect(this._activeAccessPointUpdateId);
            this._activeAccessPoint = null;
            this._activeAccessPointUpdateId = 0;
        }
        if (!hasMobileIcon && this._mobileUpdateId) {
            this._mobileUpdateDevice.disconnect(this._mobileUpdateId);
            this._mobileUpdateDevice = null;
            this._mobileUpdateId = 0;
        }
    }
};

/** creates a new NMMessageTraySource
 *
 * We call the [parent constructor]{@link MessageTray.Source} with title
 * "Network Manager" and then create an icon 
 * 'network-transmit-receive' (symbolic) and call {@link NMMessageTraySource#_setSummaryIcon}
 * with it.
 *
 * That's all this class does.
 * @classdesc
 * This is a source used for network manager notifications.
 *
 * ![{@link NMMessageTraySource}](pics/NMMessageTraySource)
 *
 * This is a good example of the most basic implementation of a {@link MessageTray.Source}.
 * All it does is set its title to "Network Manager" and its icon to
 * 'network-transmit-receive' (symbolic).
 * @class
 * @extends MessageTray.Source
 */
function NMMessageTraySource() {
    this._init();
}

NMMessageTraySource.prototype = {
    __proto__: MessageTray.Source.prototype,

    _init: function() {
        MessageTray.Source.prototype._init.call(this, _("Network Manager"));

        let icon = new St.Icon({ icon_name: 'network-transmit-receive',
                                 icon_type: St.IconType.SYMBOLIC,
                                 icon_size: this.ICON_SIZE
                               });
        this._setSummaryIcon(icon);
    }
};

//// TYPEDEFS ////
/** Local Javascript representation of a wireless network and its access points.
 * (when connecting to a wireless network you'd usually connect to the strongest
 * access point, hence the reason for this object).
 *
 * Used in {@link NMDeviceWireless}: {@link NMDeviceWireless#_networks} is an
 * array of these.
 * @see NMDeviceWireless
 * @typedef WirelessNetworkObject
 * @type {Object}
 * @property {SSIDType} ssid - SSID of the access point (the same for all access
 * points in this network)
 * @property {string} ssidText - the SSID of the access point converted to
 * text (see {@link ssidToLabel}).
 * @property {NetworkManager.80211Mode} mode - mode of the access point (the same
 * for all access points in this network). Either ADHOC or INFRA.
 * @property {Network.NMAccessPointSecurity} security - the security for
 * this access point (WEP, WPA-PSK, ...), the same for all access points in
 * this network.
 * @property {NMClient.RemoteConnection[]} connections - array of remote connections that can be activated with any of
 * the access points in `accessPoints` (subset of {@link NMApplet#_connections}).
 * Sorted by last time of connection, most recent to least recent.
 * @property {NMClient.AccessPoint[]} accessPoints - array of access points for this wireless network.
 * The {@link NMNetworkMenuItem} will ensure these are sorted by signal
 * strength, strongest to weakest.
 * @property {boolean} isMore - available networks for a particular wireless
 * device are all shown in a popup menu section in the network icon's menu.
 * If more than {@link NUM_VISIBLE_NETWORKS} are available, subsequent ones are
 * placed in a "More..." submenu. If `isMore` is `true`, this item is in the
 * "More..." submenu.
 * @property {Network.NMNetworkMenuItem} item - the menu item for this network.
 */

/** Local Javascript object representing a NMClient.RemoteConnection from
 * a NMClient.RemoteSettings. Each {@link NMDevice} has an array
 * {@link NMDeviceModem#_connections} where each element is one of these (where the
 * connection is relevant to the device).
 * @typedef ConnectionData
 * @type {object}
 * @property {NMClient.RemoteConnection} connection - the underlying connection this wrapps around.
 * @property {string} name - A human readable unique idenfier for the connection,
 * like "Work WiFi" or "T-Mobile 3G".
 * @property {string} uuid - A universally unique idenfier for the connection,
 * for example generated with libuuid. 
 * Should be assigned when the connection is created, and never changed as long
 * as the connection still applies to the same network. 
 *
 * The UUID is in the format '2815492f-7e56-435e-b2e9-246bd7cdc664' (ie,
 * contains only hexadecimal characters and '-').
 * @property {string} timestamp - The time, in seconds since the Unix Epoch,
 * that the connection was last _successfully_ fully activated.
 * @property {PopupMenu.PopupBaseMenuItem} item - the popup menu item for this connection.
 * Will be displayed in this device's menu section. For example, for the
 * {@link NMDeviceWireless} this is the item with the network name with signal
 * strength and and encryption icons ({@link NMNetworkMenuItem}).
 * @see {@link http://developer.gnome.org/libnm-util/unstable/NMSettingConnection.html#NMSettingConnection.properties|NMSettingConnection}: ConnectionData's `name` property
 * `id`, our `uuid` property is their `uuid`, and our `timestamp` property
 * is their `timestamp` property.
 */

/** Object holding information about a section on the {@link NMApplet}'s popup
 * menu, where each section focuses on a category of devices (wired,
 * wireless, wwan (mobile broadband), or vpn).
 *
 * ![Example of the Wired section](pics/NMDeviceWired.png)
 *
 * The above picture is an example of the "Wired" section of the network
 * icon's menu - showing menu items for two wired {@link NMDeviceWired} (my
 * in-built ethernet port and USB tethering from my mobile phone).
 * @typedef NMAppletSection
 * @type {object}
 * @property {PopupMenu.PopupMenuSection} section - the popup menu section
 * where we'll put all the popup menu items for this section into.
 * @property {Network.NMDevice[]} devices - array of devices in this section,
 * for example a {@link NMDeviceWired}. It is possible to have more than
 * one device for a particular section (e.g. you might have two wireless cards)
 * @property {PopupMenu.PopupBaseMenuItem} item - the popup menu item that acts
 * as the heading for this section (e.g. the "Wireless" heading). The popup
 * menu for each device in `devices` will appear under this heading.
 *
 * The wired
 * and VPN connections categories use a {@link NMWiredSectionTitleMenuItem},
 * the others (wifi/mobile broadband) use a {@link NMWirelessSectionTitleMenuItem}.
 *
 * These are in fact {@link PopupMenu.PopupSwitchMenuItem}s much like
 * {@link NMDevice#statusItem} so that if there is only one device for this
 * section, we will combine the device's status item into the section title item
 * (it has an on/off toggle to enable/disable the connection and can also
 * show the device's status, like "cable unplugged").
 * @see NMWiredSectionTitleMenuItem
 * @see NMWirelessSectionTitleMenuItem
 */

/** The type for an SSID as from `nm_access_point_get_ssid`.
 *
 * In essence it's an array of numbers.
 *
 * Note that (in GNOME 3.2 anyway) if I try to use the SSID directly,
 * I get a segmentation fault. However if I access an element of the SSID
 * via `ssid[i]`, or convert the SSID to string vial `ssid.toString()` I don't
 * get any segfaults.
 *
 * I think the underlying type is GLib.ByteArray - perhaps it's not mapped
 * properly to a javascript object.
 * @typedef SSIDType
 * @type {number[]}
 */
