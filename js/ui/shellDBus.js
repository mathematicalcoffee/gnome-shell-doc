// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines a GNOME shell DBus service (interface name `org.gnome.shell`, path
 * `/org/gnome/Shell`) - for installing/enabling/disabling/uninstalling
 * extensions and requesting information from them, taking screenshots,
 * and so on, as well as the interface for connecting to it.
 */

const DBus = imports.dbus;
const Lang = imports.lang;

const Config = imports.misc.config;
const ExtensionSystem = imports.ui.extensionSystem;
const Main = imports.ui.main;

const GnomeShellIface = {
    name: 'org.gnome.Shell',
    methods: [{ name: 'Eval',
                inSignature: 's',
                outSignature: 'bs'
              },
              { name: 'ListExtensions',
                inSignature: '',
                outSignature: 'a{sa{sv}}'
              },
              { name: 'GetExtensionInfo',
                inSignature: 's',
                outSignature: 'a{sv}'
              },
              { name: 'GetExtensionErrors',
                inSignature: 's',
                outSignature: 'as'
              },
              { name: 'ScreenshotArea',
                inSignature: 'iiiis',
                outSignature: 'b'
              },
              { name: 'ScreenshotWindow',
                inSignature: 'bs',
                outSignature: 'b'
              },
              { name: 'Screenshot',
                inSignature: 's',
                outSignature: 'b'
              },
              { name: 'EnableExtension',
                inSignature: 's',
                outSignature: ''
              },
              { name: 'DisableExtension',
                inSignature: 's',
                outSignature: ''
              },
              { name: 'InstallRemoteExtension',
                inSignature: 'ss',
                outSignature: ''
              },
              { name: 'UninstallExtension',
                inSignature: 's',
                outSignature: 'b'
              }
             ],
    signals: [{ name: 'ExtensionStatusChanged',
                inSignature: 'sis' }],
    properties: [{ name: 'OverviewActive',
                   signature: 'b',
                   access: 'readwrite' },
                 { name: 'ApiVersion',
                   signature: 'i',
                   access: 'read' },
                 { name: 'ShellVersion',
                   signature: 's',
                   access: 'read' }]
};

/** creates a new GnomeShell (service).
 *
 * This exports the instance as DBus object '/org/gnome/Shell', making it
 * available over DBus.
 *
 * (Only one instance need be made for the session, and this is stored in
 * {@link Main.shellDBusService}).
 * @classdesc
 * This provides the gnome shell DBus service. It provides a number of methods
 * for gnome-shell over DBus. The instance is exported to path '/org/gnome/Shell',
 * bus 'org.gnome.Shell'.
 *
 * You just have to create one instance to start the service (stored in
 * {@link Main.shellDBusService}.
 *
 * Executing one of the methods from another proxy for the object will execute
 * the methods defined here.
 * @class
 */
function GnomeShell() {
    this._init();
}

GnomeShell.prototype = {
    _init: function() {
        DBus.session.exportObject('/org/gnome/Shell', this);
        ExtensionSystem.connect('extension-state-changed',
                                Lang.bind(this, this._extensionStateChanged));
    },

    /**
     * This function executes arbitrary code in the main
     * loop, and returns a boolean success and
     * JSON representation of the object as a string.
     *
     * If evaluation completes without throwing an exception,
     * then the return value will be [true, JSON.stringify(result)].
     * If evaluation fails, then the return value will be
     * [false, JSON.stringify(exception)];
     *
     * @param {string} code - A string containing JavaScript code
     * @returns {Array.<boolean, string>} the boolean indicates whether the
     * code ran successfully (without error), and the string is the
     * `JSON.stringify`'d version of the result of running `code`.
     */
    Eval: function(code) {
        let returnValue;
        let success;
        try {
            returnValue = JSON.stringify(eval(code));
            // A hack; DBus doesn't have null/undefined
            if (returnValue == undefined)
                returnValue = '';
            success = true;
        } catch (e) {
            returnValue = JSON.stringify(e);
            success = false;
        }
        return [success, returnValue];
    },

    /**
     * Takes a screenshot of the passed in area and saves it
     * in `filename` as a png image. It returns a boolean
     * indicating whether the operation was successful or not asynchronously.
     *
     * This is an asynchronous method, so you have to supply a callback
     * specifying what to do with the result (boolean).
     *
     * @param {number} x - The X coordinate of the area
     * @param {number} y - The Y coordinate of the area
     * @param {number} width - The width of the area
     * @param {number} height - The height of the area
     * @param {string} filename - The filename for the screenshot
     * @param {function(boolean)} callback - when the screenshot is
     * complete, this callback is used on the result of the screenshot
     * operation (being a boolean indicating whether th escreenshot succeeded
     * or not).
     *
     */
    ScreenshotAreaAsync : function (x, y, width, height, filename, callback) {
        global.screenshot_area (x, y, width, height, filename, function (obj, result) { callback(result); });
    },

    /**
     * Takes a screenshot of the focused window (optionally omitting the frame)
     * and saves it in `filename` as a png image. It returns a boolean
     * indicating whether the operation was successful or not.
     *
     * ScreenshotWindow:
     * @param {boolean} include_frame - Whether to include the window frame or
     * not in the screenshot.
     * @param {string} filename - The filename for the screenshot
     * @returns {boolean} whether the operation was successful or not.
     */
    ScreenshotWindow : function (include_frame, filename) {
        return global.screenshot_window (include_frame, filename);
    },

    /**
     * Takes a screenshot of the whole screen and saves it
     * in @filename as png image, it returns a boolean
     * indicating whether the operation was successful or not.
     *
     * This method is *asynchronous*, so the returned value must be dealt
     * with by the `callback` parameter.
     *
     * @param {string} filename - The filename for the screenshot
     * @param {function (boolean)} - when the screenshot completes
     * `callback` is called with the success of the operation.
     */
    ScreenshotAsync : function (filename, callback) {
        global.screenshot(filename, function (obj, result) { callback(result); });
    },

    /** Returns a list of the currently-installed extensions on the system
     * along with their metadata.
     * @returns {Object.<string, ExtensionSystem.ExtensionMeta>} hash of
     * extension UUID to extension metadata.
     * @see ExtensionSystem.extensionMeta
     */
    ListExtensions: function() {
        return ExtensionSystem.extensionMeta;
    },

    /** Returns the metadata for the specified extension.
     * @param {string} uuid - UUID of the extension to get information for.
     * @returns {ExtensionSytem.ExtensionMeta} The metadata for that extension.
     * @see ExtensionSystem.extensionMeta
     */
    GetExtensionInfo: function(uuid) {
        return ExtensionSystem.extensionMeta[uuid] || {};
    },

    /** Gets the array of extension errors for a particular extension.
     * @param {string} uuid - the UUID to get the errors for.
     * @returns {string[]} array of error messages for that extension.
     */
    GetExtensionErrors: function(uuid) {
        return ExtensionSystem.errors[uuid] || [];
    },

    /** Enables an extension by UUID.
     *
     * This works by updating gsettings key
     * ['org.gnome.shell.enabled-extensions']{@link ExtensionSystem.ENABLED_EXTENSIONS_KEY}
     * with the UUID, which in turn triggers {@link ExtensionSystem.onEnabledExtensionsChanged}
     * which enables the extension.
     * @param {string} uuid - UUID of the extension to enable.
     * @see ExtensionSystem.enableExtension
     */
    EnableExtension: function(uuid) {
        let enabledExtensions = global.settings.get_strv(ExtensionSystem.ENABLED_EXTENSIONS_KEY);
        if (enabledExtensions.indexOf(uuid) == -1)
            enabledExtensions.push(uuid);
        global.settings.set_strv(ExtensionSystem.ENABLED_EXTENSIONS_KEY, enabledExtensions);
    },

    /** Disables an extension by UUID.
     *
     * This works by updating gsettings key
     * ['org.gnome.shell.enabled-extensions']{@link ExtensionSystem.ENABLED_EXTENSIONS_KEY}
     * removing the UUID, which in turn triggers {@link ExtensionSystem.onEnabledExtensionsChanged}
     * which disabled the extension.
     * @param {string} uuid - UUID of extension to disable.
     * @see ExtensionSystem.disableExtension
     */
    DisableExtension: function(uuid) {
        let enabledExtensions = global.settings.get_strv(ExtensionSystem.ENABLED_EXTENSIONS_KEY);
        while (enabledExtensions.indexOf(uuid) != -1)
            enabledExtensions.splice(enabledExtensions.indexOf(uuid), 1);
        global.settings.set_strv(ExtensionSystem.ENABLED_EXTENSIONS_KEY, enabledExtensions);
    },

    /** Installs an extension from [e.g.o](https://extensions.gnome.org).
     * @param {string} uuid - UUID of extension
     * @inheritparams ExtensionSystem.installExtensionFromUUID
     * @see ExtensionSystem.installExtensionFromUUID
     */
    InstallRemoteExtension: function(uuid, version_tag) {
        ExtensionSystem.installExtensionFromUUID(uuid, version_tag);
    },

    /** Uninstalls an extension.
     * @inheritparams ExtensionSystem.uninstallExtensionFromUUID
     * @see ExtensionSystem.uninstallExtensionFromUUID
     */
    UninstallExtension: function(uuid) {
        return ExtensionSystem.uninstallExtensionFromUUID(uuid);
    },

    /** Gets whether the Overview is currently open.
     * @returns {boolean} whether the overview is open ({@link Overview.Overview#visible}).
     */
    get OverviewActive() {
        return Main.overview.visible;
    },

    /** Sets whether the Overview is currently open. Calls either
     * {@link Overview.Overview#show} or {@link Overview.Overview#hide}.
     * @param {boolean} visible - whether to show or hide the overview.
     */
    set OverviewActive(visible) {
        if (visible)
            Main.overview.show();
        else
            Main.overview.hide();
    },

    /** The API version of the extension system (only used when communicating
     * with [e.g.o](https://extensions.gnome.org).
     * @type {string} */
    ApiVersion: ExtensionSystem.API_VERSION,

    /** The current gnome-shell version (e.g. '3.2.2.1').
     * @type {string}
     * @see Config.PACKAGE_VERSION */
    ShellVersion: Config.PACKAGE_VERSION,

    /** callback when any extension's state changes.
     * This connects to {@link ExtensionSystem.extension-state-changed} and
     * re-emits the signal as DBus signal 'ExtensionStatusChanged' with
     * the extension's UUID, new state, an any error message.
     * @param {?} _ - object that emitted the signal.
     * @param {ExtensionSystem.ExtensionMeta} newState - the metadata with the
     * updated extension information.
     */
    _extensionStateChanged: function(_, newState) {
        DBus.session.emit_signal('/org/gnome/Shell',
                                 'org.gnome.Shell',
                                 'ExtensionStatusChanged', 'sis',
                                 [newState.uuid, newState.state, newState.error]);
    }
};

DBus.conformExport(GnomeShell.prototype, GnomeShellIface);

