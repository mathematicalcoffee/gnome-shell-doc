// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines the top panel.
 */

const Cairo = imports.cairo;
const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Pango = imports.gi.Pango;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Signals = imports.signals;

const Config = imports.misc.config;
const CtrlAltTab = imports.ui.ctrlAltTab;
const Layout = imports.ui.layout;
const Overview = imports.ui.overview;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const DateMenu = imports.ui.dateMenu;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

/** @+
 * @const
 * @default */
/** @+
 * @type {number} */

/** the default size of icons in the top panel. */
const PANEL_ICON_SIZE = 24;

/** The time you have to hover a dragged item over the Activities Button before
 * the overview will launch (milliseconds) */
const BUTTON_DND_ACTIVATION_TIMEOUT = 250;

/** The time (milliseconds) between frames of the animation of the
 * {@link AnimatedIcon} */
const ANIMATED_ICON_UPDATE_TIMEOUT = 100;
/** The time (seconds) over which the {@link AnimatedIcon} (spinner in the
 * title bar) fades in and out. */
const SPINNER_ANIMATION_TIME = 0.2;
/** @- */

/** The order of the standard status area icons (a11y, keyboard, volume, ...)
 * in the top panel. Left to right.
 *
 * Default: accessibility icon, keyboard indicator, volume indicator,
 * bluetooth indicator, network indicator, battery indicator,
 * user menu.
 *
 * Note that not all of these are necessarily visible (e.g. if you don't have
 * bluetooth you won't have a bluetooth indicator).
 * @type {string[]}
 */
const STANDARD_STATUS_AREA_ORDER = ['a11y', 'keyboard', 'volume', 'bluetooth', 'network', 'battery', 'userMenu'];
/** Hash map mapping status icon name to the constructor for that status icon:
 *
 * * 'a11y': {@link Accessibility.ATIndicator}
 * * 'battery': {@link Power.Indicator}
 * * 'bluetooth': {@link Bluetooth.Indicator}
 * * 'keyboard': {@link Keyboard.XKBIndicator}
 * * 'network': {@link Network.NMApplet}
 * * 'userMenu': {@link UserMenu.UserMenuButton}
 * * 'volume': {@link Volume.Indicator}
 *
 * The 'bluetooth' and 'network' properties are only there if they are supported
 * by your system.
 *
 * @type {Object.<string, PanelMenu.Button>}
 */
const STANDARD_STATUS_AREA_SHELL_IMPLEMENTATION = {
    'a11y': imports.ui.status.accessibility.ATIndicator,
    'volume': imports.ui.status.volume.Indicator,
    'battery': imports.ui.status.power.Indicator,
    'keyboard': imports.ui.status.keyboard.XKBIndicator,
    'userMenu': imports.ui.userMenu.UserMenuButton
};

if (Config.HAVE_BLUETOOTH)
    STANDARD_STATUS_AREA_SHELL_IMPLEMENTATION['bluetooth'] = imports.ui.status.bluetooth.Indicator;

try {
    STANDARD_STATUS_AREA_SHELL_IMPLEMENTATION['network'] = imports.ui.status.network.NMApplet;
} catch(e) {
    log('NMApplet is not supported. It is possible that your NetworkManager version is too old');
}

/** The order of the standard status area icons in the top panel, in GDM
 * (the login screen). left to right.
 *
 * Default: accessibility icon, keyboard indicator, volume indicator,
 * battery indicator, power menu.
 *
 * Difference between this and {@link STANDARD_STATUS_AREA_ORDER} is that the
 * bluetooth and network indicators and the user menu are not shown in the login screen,
 * and the power menu is only shown in the login screen.
 * @type {string[]} */
const GDM_STATUS_AREA_ORDER = ['a11y', 'display', 'keyboard', 'volume', 'battery', 'powerMenu'];
/** Hash map mapping status icon name to the constructor for that status icon
 * for the GDM (login) screen:
 *
 * * 'a11y': {@link Accessibility.ATIndicator}
 * * 'battery': {@link Power.Indicator}
 * * 'keyboard': {@link Keyboard.XKBIndicator}
 * * 'powerMenu': {@link PowerMenu.PowerMenuButton}
 * * 'volume': {@link Volume.Indicator}
 *
 * @type {Object.<string, PanelMenu.Button>}
 */
const GDM_STATUS_AREA_SHELL_IMPLEMENTATION = {
    'a11y': imports.ui.status.accessibility.ATIndicator,
    'volume': imports.ui.status.volume.Indicator,
    'battery': imports.ui.status.power.Indicator,
    'keyboard': imports.ui.status.keyboard.XKBIndicator,
    'powerMenu': imports.gdm.powerMenu.PowerMenuButton
};
/** @- */

// To make sure the panel corners blend nicely with the panel,
// we draw background and borders the same way, e.g. drawing
// them as filled shapes from the outside inwards instead of
// using cairo stroke(). So in order to give the border the
// appearance of being drawn on top of the background, we need
// to blend border and background color together.
// For that purpose we use the following helper methods, taken
// from st-theme-node-drawing.c
//
/** Helper method, normalizes a colour (0 to 255) to the range 0 to 1.
 * @param {number} x - pixel intensity (0 to 255)
 * @returns {number} x / 255. */
function _norm(x) {
    return Math.round(x / 255);
}

/** Returns the Clutter.Color that represents `dstColor` drawn over `srcColor`
 * (with the source colour's transparency:
 *
 *     result = srcColor * srcColor.alpha + (1-srcColor.alpha) * dstColor
 *
 * ?)
 *
 * Helper function for drawing the {@link PanelCorner}s.
 * @param {Clutter.Color} srcColor - the base/background colour
 * @param {Clutter.Color} dstColor - the colour do draw "over" the source colour
 * @returns {Clutter.Color} essentially
 * `srcColor * srcColor.alpha + (1-srcColor.alpha) * dstColor`?
 */
function _over(srcColor, dstColor) {
    let src = _premultiply(srcColor);
    let dst = _premultiply(dstColor);
    let result = new Clutter.Color();

    result.alpha = src.alpha + _norm((255 - src.alpha) * dst.alpha);
    result.red = src.red + _norm((255 - src.alpha) * dst.red);
    result.green = src.green + _norm((255 - src.alpha) * dst.green);
    result.blue = src.blue + _norm((255 - src.alpha) * dst.blue);

    return _unpremultiply(result);
}

/** Premultiplies a Clutter.Color's intensities by the alpha value, returning
 * the result in a new colour.
 *
 * Helper function for drawing the {@link PanelCorner}s.
 * @param {Clutter.Color} color - a colour
 * @returns {Clutter.Color} a new colour such that the red, green and blue
 * values are multiplied by `color`'s alpha level (where the alpha level is
 * considered as being from 0 to 1) and with the original alpha level as the
 * output colour's alpha.
 */
function _premultiply(color) {
    return new Clutter.Color({ red: _norm(color.red * color.alpha),
                               green: _norm(color.green * color.alpha),
                               blue: _norm(color.blue * color.alpha),
                               alpha: color.alpha });
};

/** Undoes a {@link _premultiply}, un-multiplying `color`'s alpha level
 * from its red, green and blue channels.
 *
 * Helper function for drawing the {@link PanelCorner}s.
 * @inheritparams _premultiply
 * @returns {Clutter.Color} a clutter colour with the red/green/blue
 * channels with the alpha level divided out.
 * @see _premultiply */
function _unpremultiply(color) {
    if (color.alpha == 0)
        return new Clutter.Color();

    let red = Math.min((color.red * 255 + 127) / color.alpha, 255);
    let green = Math.min((color.green * 255 + 127) / color.alpha, 255);
    let blue = Math.min((color.blue * 255 + 127) / color.alpha, 255);
    return new Clutter.Color({ red: red, green: green,
                               blue: blue, alpha: color.alpha });
};


/** creates a new AnimatedIcon
 * We create the top-level actor {@link #actor}, a St.Bin.
 *
 * Inside this we place a Clutter.Group {@link #_animations} where child `n` is
 * the `n`th frame of the image identified by `name` - the default
 * {St.TextureCache} is used to load the image into frames.
 *
 * The icon is connected up such that the animation only runs whilst it is
 * actually visible, so one can start and stop the animation simply by
 * setting {@link #actor} visible or not.
 *
 * @param {string} name - the file name of the image containing the animation
 * (e.g. '/usr/share/gnome-shell/theme/process-working.svg')
 * @param {number} size - the size of each frame of the animation (assumed
 * square!)
 * @classdesc
 * Base class for an animated icon (represented by an image with the tiled
 * animation).
 *
 * ![Example of the image for an {@link AnimatedIcon}](pics/process-working.png)
 *
 * ![Spinner animation, an {@link AnimatedIcon}](pics/process-working.gif)
 *
 * It will do the animation automatically
 * (frame delay {@link ANIMATED_ICON_UPDATE_TIMEOUT}).
 *
 * In GNONE-shell this is used for the spinner in the top panel when an
 * application is loading.
 *
 * @class
 */
function AnimatedIcon(name, size) {
    this._init(name, size);
}

AnimatedIcon.prototype = {
    _init: function(name, size) {
        /** The top-level actor for the animated icon is a St.Bin into which
         * the animation is nested ({@link #_animations}).
         * @type {St.Bin} */
        this.actor = new St.Bin({ visible: false });
        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));
        this.actor.connect('notify::visible', Lang.bind(this, function() {
            if (this.actor.visible) {
                this._timeoutId = Mainloop.timeout_add(ANIMATED_ICON_UPDATE_TIMEOUT, Lang.bind(this, this._update));
            } else {
                if (this._timeoutId)
                    Mainloop.source_remove(this._timeoutId);
                this._timeoutId = 0;
            }
        }));

        this._timeoutId = 0;
        /** The frame of the animation we are up to.
         * @type {number} */
        this._i = 0;
        /** The set of frames that make up the animation - child `i` is frame `i`.
         * @see [`st_texture_cache_load_sliced_image`](http://developer.gnome.org/st/stable/StTextureCache.html#st-texture-cache-load-sliced-image)
         * @type {Clutter.Group} */
        this._animations = St.TextureCache.get_default().load_sliced_image (global.datadir + '/theme/' + name, size, size);
        this.actor.set_child(this._animations);
    },

    /** Advances the animation by one frame (every
     * {@link ANIMATED_ICN_UPDATE_TIMEOUT} milliseconds).
     *
     * Increments {@link #_i}, retrieves that child of {@link #_animations},
     * and shows it.
     *
     * We loop back to the start of the animation upon completion of the
     * cycle.
     */
    _update: function() {
        this._animations.hide_all();
        this._animations.show();
        if (this._i && this._i < this._animations.get_n_children())
            this._animations.get_nth_child(this._i++).show();
        else {
            this._i = 1;
            if (this._animations.get_n_children())
                this._animations.get_nth_child(0).show();
        }
        return true;
    },

    /** Callback when {@link #actor} is destroyed - simply removes the
     * timeout that is performing the animation. */
    _onDestroy: function() {
        if (this._timeoutId)
            Mainloop.source_remove(this._timeoutId);
    }
};

/** creates a new TextShadower
 *
 * We create our top-level actor {@link #actor}, a {@link Shell.GenericContainer}.
 *
 * In it we place the main label (white text), {@link #_label}.
 *
 * Then we add four extra labels, all with style class 'label-shadow'. These
 * are black and half-transparent and will be offset by one pixel in each
 * direction from the main label to give a "text shadow" appearance.
 *
 * ![{@link TextShadower}](pics/Panel.TextShadower.png)
 *
 * The above picture is a blown-up version of the text shadower showing the
 * offset "shadow" labels.
 *
 * @classdesc
 * The {@link TextShadower} is used in the {@link AppMenuButton} to show the
 * title of the application of the current window.
 *
 * It consists of five text labels, all showing the same text:
 *
 * * the "main" label, white text.
 * * four "shadow" labels, black half-transparent text.
 *
 * The "shadow" labels are offset one pixel in each direction from the main
 * label and are positioned behind the main label, to give the appearance
 * of a shadow.
 *
 * ![{@link TextShadower}](pics/Panel.TextShadower.png)
 *
 * The above picture is a blown-up version of the text shadower showing the
 * offset "shadow" labels.
 *
 * (I find that since the offset is only one pixel in each direction, the
 * TextShadower is indistinguishable from a normal label with now shadow!
 * Why not just use the CSS `text-shadow` property?)
 * @class
 */
function TextShadower() {
    this._init();
}

TextShadower.prototype = {
    _init: function() {
        /** The top-level actor is a Shell.GenericContainer (to allow for
         * precise positioning of all the text labels within it).
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer();
        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));

        /** The "actual" label (not one of the shadow labels).
         * @type {St.Label} */
        this._label = new St.Label();
        this.actor.add_actor(this._label);
        for (let i = 0; i < 4; i++) {
            let actor = new St.Label({ style_class: 'label-shadow' });
            actor.clutter_text.ellipsize = Pango.EllipsizeMode.END;
            this.actor.add_actor(actor);
        }
        this._label.raise_top();
    },

    /** Sets the text of the text shadower (sets it on the main label
     * as well as all shadow labels).
     * @param {string} text - label to show
     */
    setText: function(text) {
        let children = this.actor.get_children();
        for (let i = 0; i < children.length; i++)
            children[i].set_text(text);
    },

    /** Callback for the 'get-preferred-width' signal of {@link #actor}.
     *
     * Allocates the main label ({@link #_label})'s width plus 2 pixels (one
     * either side for the two side "shadow" labels).
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        let [minWidth, natWidth] = this._label.get_preferred_width(forHeight);
        alloc.min_size = minWidth + 2;
        alloc.natural_size = natWidth + 2;
    },

    /** Callback for the 'get-preferred-height' signal of {@link #actor}.
     *
     * Allocates the main label ({@link #_label})'s height plus 2 pixels (one
     * either side for the top and bottom "shadow" labels).
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        let [minHeight, natHeight] = this._label.get_preferred_height(forWidth);
        alloc.min_size = minHeight + 2;
        alloc.natural_size = natHeight + 2;
    },

    /** Callback for the 'allocate' signal of {@link #actor}.
     *
     * The "main" label is given offset (1, 1) from the top-left corner.
     *
     * The top "shadow label" is set 1 pixel above the main one; the bottom
     * shadow label is set 1 pixel below the main one; the right shadow label
     * is set 1 pixel to the right; and the left shadow label is 1 pixel to
     * the left.
     *
     * ![{@link TextShadower}](pics/Panel.TextShadower.png)
     */
    _allocate: function(actor, box, flags) {
        let children = this.actor.get_children();

        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;

        let [minChildWidth, minChildHeight, natChildWidth, natChildHeight] =
            this._label.get_preferred_size();

        let childWidth = Math.min(natChildWidth, availWidth - 2);
        let childHeight = Math.min(natChildHeight, availHeight - 2);

        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            let childBox = new Clutter.ActorBox();
            // The order of the labels here is arbitrary, except
            // we know the "real" label is at the end because Clutter.Group
            // sorts by Z order
            switch (i) {
                case 0: // top
                    childBox.x1 = 1;
                    childBox.y1 = 0;
                    break;
                case 1: // right
                    childBox.x1 = 2;
                    childBox.y1 = 1;
                    break;
                case 2: // bottom
                    childBox.x1 = 1;
                    childBox.y1 = 2;
                    break;
                case 3: // left
                    childBox.x1 = 0;
                    childBox.y1 = 1;
                    break;
                case 4: // center
                    childBox.x1 = 1;
                    childBox.y1 = 1;
                    break;
            }
            childBox.x2 = childBox.x1 + childWidth;
            childBox.y2 = childBox.y1 + childHeight;
            child.allocate(childBox, flags);
        }
    }
};

/** creates a new AppMenuButton
 *
 * We construct the graphical part of the app menu button:
 *
 * ![{@link AppMenuButton}; {@link #_iconBox} in red, {@link #_label} in blue, {@link #_container} in green.](pics/AppMenuButton.png)
 *
 * * {@link #_iconBox}: holds the icon of the app being displayed, outlined
 * red in the picture above;
 * * {@link #_label}: holds the {@link TextShadower} showing the title of
 * the application, outlined blue in the picture above.
 * * an 'spinner' animation that shows when an application is starting up,
 * {@link #_spinner} (yellow).
 * * These are all placed into a Shell.GenericConainer {@link #_container} to
 * allow for precise positioning (outlined green in the picture above).
 * * {@link #_container} is placed into a {@link St.Bin}
 * with style name 'appMenu' (not stored as a member in the class).
 *
 * We then construct the app menu button's popup menu, which for now just
 * consists of a "Quit" button ({@link #_quitMenu}).
 *
 * Finally we call {@link #_sync} to update our visibility state, title, etc.
 *
 * @classdesc
 * ![{@link AppMenuButton}](pics/Panel.appMenu.png)
 *
 * This class manages the "application menu" component.  It tracks the
 * currently focused application.  However, when an app is launched,
 * this menu also handles startup notification for it.  So when we
 * have an active startup notification, we switch modes to display that.
 *
 * It consists of the icon and name of the currently focused application,
 * an animated 'spinner' icon while an application is launching, and a
 * dropdown menu with an option to quit the application.
 *
 * The button does not show if no windows are focused.
 * @class
 * @extends PanelMenu.Button
 */
function AppMenuButton() {
    this._init();
}

AppMenuButton.prototype = {
    __proto__: PanelMenu.Button.prototype,

    _init: function() {
        PanelMenu.Button.prototype._init.call(this, 0.0);
        /** Array of apps that are in the process of starting up.
         * @type {Shell.App[]} */
        this._startingApps = [];

        /** The currently-focused application. If `null`, there isn't one.
         * @type {?Shell.App} */
        this._targetApp = null;

        let bin = new St.Bin({ name: 'appMenu' });
        this.actor.add_actor(bin);

        this.actor.reactive = false;
        this._targetIsCurrent = false;

        /** This holds the icon ({@link #_iconBox}) and application name
         * ({@link #_label}) inside it.
         *
         * @see #_getContentPreferredHeight
         * @see #_getContentPreferredWidth
         * @see #_contentAllocate
         * @type {Shell.GenericContainer} */
        this._container = new Shell.GenericContainer();
        bin.set_child(this._container);
        this._container.connect('get-preferred-width', Lang.bind(this, this._getContentPreferredWidth));
        this._container.connect('get-preferred-height', Lang.bind(this, this._getContentPreferredHeight));
        this._container.connect('allocate', Lang.bind(this, this._contentAllocate));

        /** Holds the application icon. Style name 'appMenuIcon'.
         *
         * When its style changes {@link #_onIconBoxStyleChanged} is called,
         * and upon its allocation (size/position) changing the clipping of
         * the icon is updated.
         *
         * Added to {@link #_container}.
         *
         * @see #_onIconBoxStyleChanged
         * @see #_updateIconBoxClip
         * @type {Shell.Slicer} */
        this._iconBox = new Shell.Slicer({ name: 'appMenuIcon' });
        this._iconBox.connect('style-changed',
                              Lang.bind(this, this._onIconBoxStyleChanged));
        this._iconBox.connect('notify::allocation',
                              Lang.bind(this, this._updateIconBoxClip));
        this._container.add_actor(this._iconBox);
        /** The label(s) showing the title of the application.
         *
         * Added to {@link #_container}.
         * @type {TextShadower} */
        this._label = new TextShadower();
        this._container.add_actor(this._label.actor);

        this._iconBottomClip = 0;

        /** The quit popup menu item in the app menu.
         * @see #_onQuit
         * @type {PopupMenu.PopupMenuItem} */
        this._quitMenu = new PopupMenu.PopupMenuItem('');
        this.menu.addMenuItem(this._quitMenu);
        this._quitMenu.connect('activate', Lang.bind(this, this._onQuit));

        this._visible = !Main.overview.visible;
        if (!this._visible)
            this.actor.hide();
        Main.overview.connect('hiding', Lang.bind(this, function () {
            this.show();
        }));
        Main.overview.connect('showing', Lang.bind(this, function () {
            this.hide();
        }));

        this._stop = true;

        /** The 'spinner' animation
         * ![spinner animation, an {@link AnimatedIcon}](pics/process-working.gif)
         * @type AnimatedIcon
         */
        this._spinner = new AnimatedIcon('process-working.svg',
                                         PANEL_ICON_SIZE);
        this._container.add_actor(this._spinner.actor);
        this._spinner.actor.lower_bottom();

        let tracker = Shell.WindowTracker.get_default();
        let appSys = Shell.AppSystem.get_default();
        tracker.connect('notify::focus-app', Lang.bind(this, this._sync));
        appSys.connect('app-state-changed', Lang.bind(this, this._onAppStateChanged));

        global.window_manager.connect('switch-workspace', Lang.bind(this, this._sync));

        this._sync();
    },

    /** Shows the app menu button by fading it in and makes it reactive. */
    show: function() {
        if (this._visible)
            return;

        this._visible = true;
        this.actor.show();
        this.actor.reactive = true;

        if (!this._targetIsCurrent)
            return;

        Tweener.removeTweens(this.actor);
        Tweener.addTween(this.actor,
                         { opacity: 255,
                           time: Overview.ANIMATION_TIME,
                           transition: 'easeOutQuad' });
    },

    /** Hides the app menu button by fading it out. */
    hide: function() {
        if (!this._visible)
            return;

        this._visible = false;
        this.actor.reactive = false;
        if (!this._targetIsCurrent) {
            this.actor.hide();
            return;
        }

        Tweener.removeTweens(this.actor);
        Tweener.addTween(this.actor,
                         { opacity: 0,
                           time: Overview.ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: function() {
                               this.actor.hide();
                           },
                           onCompleteScope: this });
    },

    /** Callback for {@link #_iconBox}'s 'style-changed' signal (the icon for
     * the currently-focused application).
     *
     * This refreshes the value of 'app-icon-bottom-clip' from the style class
     * and adjusts the icon box's clip to reflect this ({@link #_updateIconBoxClip}).
     */
    _onIconBoxStyleChanged: function() {
        let node = this._iconBox.get_theme_node();
        this._iconBottomClip = node.get_length('app-icon-bottom-clip');
        this._updateIconBoxClip();
    },

    /** Updates the clipping on the application icon in the app menu,
     * {@link #_iconBox}.
     *
     * Called whenever the icon box's size or position changes, or its style
     * class (the 'app-icon-bottom-clip' property).
     *
     * The clip is applied to the *bottom* of the icon box (it doesn't extend
     * all the we down to the bottom of the top panel). The default clip
     * is 1px. */
    _updateIconBoxClip: function() {
        let allocation = this._iconBox.allocation;
        if (this._iconBottomClip > 0)
            this._iconBox.set_clip(0, 0,
                                   allocation.x2 - allocation.x1,
                                   allocation.y2 - allocation.y1 - this._iconBottomClip);
        else
            this._iconBox.remove_clip();
    },

    /** Stops the spinner animation ({@link #_spinner},
     * ![spinner animation {@link #_spinner}](pics/process-working.gif)) by
     * fading it out over time {@link SPINNER_ANIMATION_TIME}.
     */
    stopAnimation: function() {
        if (this._stop)
            return;

        this._stop = true;
        Tweener.addTween(this._spinner.actor,
                         { opacity: 0,
                           time: SPINNER_ANIMATION_TIME,
                           transition: "easeOutQuad",
                           onCompleteScope: this,
                           onComplete: function() {
                               this._spinner.actor.opacity = 255;
                               this._spinner.actor.hide();
                           }
                         });
    },

    /** Starts the spinner animation ({@link #_spinner},
     * ![spinner animation {@link #_spinner}](pics/process-working.gif)) by
     * showing it (which triggers the animation starting, see {@link AnimatedIcon}).
     *
     * No fading in done; it is just shown immediately. */
    startAnimation: function() {
        this._stop = false;
        this._spinner.actor.show();
    },

    /** Callback for {@link #_container}'s (holds the app icon, app title and
     * spinner animation) 'get-preferred-width' signal.
     *
     * Allocates width such that the app name/text shadower {@link #_label}
     * is positioned half-way along the app icon {@link #_iconBox}.
     *
     * ![{@link AppMenuButton}; {@link #_label} (blue) is positioned half-way along {@link #_iconBx} (red)](pics/AppMenuButton.png)
     *
     * No width is allocated to the spinner icon {@link #_spinner} (???)
     */
    _getContentPreferredWidth: function(actor, forHeight, alloc) {
        let [minSize, naturalSize] = this._iconBox.get_preferred_width(forHeight);
        alloc.min_size = minSize;
        alloc.natural_size = naturalSize;
        [minSize, naturalSize] = this._label.actor.get_preferred_width(forHeight);
        alloc.min_size = alloc.min_size + Math.max(0, minSize - Math.floor(alloc.min_size / 2));
        alloc.natural_size = alloc.natural_size + Math.max(0, naturalSize - Math.floor(alloc.natural_size / 2));
    },

    /** Callback for {@link #_container}'s (holds the app icon, app title and
     * spinner animation) 'get-preferred-height' signal.
     *
     * Allocates the height of the app icon {@link #_iconBox} or the app
     * title label {@link #_label}, whichever is greater.
     */
    _getContentPreferredHeight: function(actor, forWidth, alloc) {
        let [minSize, naturalSize] = this._iconBox.get_preferred_height(forWidth);
        alloc.min_size = minSize;
        alloc.natural_size = naturalSize;
        [minSize, naturalSize] = this._label.actor.get_preferred_height(forWidth);
        if (minSize > alloc.min_size)
            alloc.min_size = minSize;
        if (naturalSize > alloc.natural_size)
            alloc.natural_size = naturalSize;
    },

    /** Callback for {@link #_container}'s (holds the app icon, app title and
     * spinner animation) 'allocate' signal.
     *
     * ![{@link AppMenuButton}; {@link #_label} (blue) is positioned half-way along {@link #_iconBx} (red)](pics/AppMenuButton.png)
     *
     * The positioning is mirrored if the text direction is right to left
     * rather than left to right. We position:
     *
     * * the application icon {@link #_iconBox} left-most, giving it its requested
     *   width and height (up to the available space).
     * * the application title label {@link #_label} is centred vertically,
     *   and placed such that it starts mid-way along the width of the icon
     *   {@link #_iconBox};
     * * the spinner {@link #_spinner} is positioned to the right of the
     *   application title label.
     */
    _contentAllocate: function(actor, box, flags) {
        let allocWidth = box.x2 - box.x1;
        let allocHeight = box.y2 - box.y1;
        let childBox = new Clutter.ActorBox();

        let [minWidth, minHeight, naturalWidth, naturalHeight] = this._iconBox.get_preferred_size();

        let direction = this.actor.get_direction();

        let yPadding = Math.floor(Math.max(0, allocHeight - naturalHeight) / 2);
        childBox.y1 = yPadding;
        childBox.y2 = childBox.y1 + Math.min(naturalHeight, allocHeight);
        if (direction == St.TextDirection.LTR) {
            childBox.x1 = 0;
            childBox.x2 = childBox.x1 + Math.min(naturalWidth, allocWidth);
        } else {
            childBox.x1 = Math.max(0, allocWidth - naturalWidth);
            childBox.x2 = allocWidth;
        }
        this._iconBox.allocate(childBox, flags);

        let iconWidth = childBox.x2 - childBox.x1;

        [minWidth, minHeight, naturalWidth, naturalHeight] = this._label.actor.get_preferred_size();

        yPadding = Math.floor(Math.max(0, allocHeight - naturalHeight) / 2);
        childBox.y1 = yPadding;
        childBox.y2 = childBox.y1 + Math.min(naturalHeight, allocHeight);

        if (direction == St.TextDirection.LTR) {
            childBox.x1 = Math.floor(iconWidth / 2);
            childBox.x2 = Math.min(childBox.x1 + naturalWidth, allocWidth);
        } else {
            childBox.x2 = allocWidth - Math.floor(iconWidth / 2);
            childBox.x1 = Math.max(0, childBox.x2 - naturalWidth);
        }
        this._label.actor.allocate(childBox, flags);

        if (direction == St.TextDirection.LTR) {
            childBox.x1 = Math.floor(iconWidth / 2) + this._label.actor.width;
            childBox.x2 = childBox.x1 + this._spinner.actor.width;
            childBox.y1 = box.y1;
            childBox.y2 = box.y2 - 1;
            this._spinner.actor.allocate(childBox, flags);
        } else {
            childBox.x1 = -this._spinner.actor.width;
            childBox.x2 = childBox.x1 + this._spinner.actor.width;
            childBox.y1 = box.y1;
            childBox.y2 = box.y2 - 1;
            this._spinner.actor.allocate(childBox, flags);
        }
    },

    /** Callback when the user clicks on the 'Quit [application name]' button
     * in the application menu, {@link #_quitMenu}.
     *
     * This requests a quit of currently-focused application {@link #_targetApp}
     * (note this quits the entire application (i.e. all windows), not just
     * the current window).
     */
    _onQuit: function() {
        if (this._targetApp == null)
            return;
        this._targetApp.request_quit();
    },

    /** Callback on the global {@link Shell.AppSystem}'s 'app-state-changed'
     * signal, fired whenever an application starts up, or is quit.
     *
     * The new state is either STOPPED, STARTING or RUNNING.
     *
     * We update {@link #_startingApps} with the list of apps that are
     * currently starting up, and then call {@link #_sync} to update
     * what the app menu shows (for example it may show the spinner and
     * name of `app` if it has just been started).
     * @param {Shell.AppSystem} appSys - the global/default app system
     * @param {Shell.App} app - the application whose state changed.
     */
    _onAppStateChanged: function(appSys, app) {
        let state = app.state;
        if (state != Shell.AppState.STARTING) {
            this._startingApps = this._startingApps.filter(function(a) {
                return a != app;
            });
        } else if (state == Shell.AppState.STARTING) {
            this._startingApps.push(app);
        }
        // For now just resync on all running state changes; this is mainly to handle
        // cases where the focused window's application changes without the focus
        // changing.  An example case is how we map OpenOffice.org based on the window
        // title which is a dynamic property.
        this._sync();
    },

    /** Key function of the app menu button.
     *
     * This updates the application icon/label shown.
     *
     * * We decide which application's title/icon we will show, being either
     * the currently-focused application or the most recent application that is
     * still in the process of starting ({@link #_startingApps}) and store
     * it in {@link #_targetApp}.
     * * If this is `null` (no focused app, none starting up) we hide the
     * app menu button by fading it out.
     * * If it is not null and the app menu button is hidden, we show it by
     * fading it in.
     * * If the app is not currently starting up, we make sure the spinner
     * animation is not showing ({@link #stopAnimation}).
     * * We update the application icon and title ({@link #_iconBox} and
     * {@link #_label}) to reflect the current application.
     * * We update the "Quit [application name]" item in the popup menu to
     * show the current application's name.
     * * If the target application is still starting up, we show the
     * spinner ({@link #startAnimation}).
     * 
     * Finally, we emit a 'changed' signal.
     * @fires .changed
     */
    _sync: function() {
        let tracker = Shell.WindowTracker.get_default();
        let lastStartedApp = null;
        let workspace = global.screen.get_active_workspace();
        for (let i = 0; i < this._startingApps.length; i++)
            if (this._startingApps[i].is_on_workspace(workspace))
                lastStartedApp = this._startingApps[i];

        let focusedApp = tracker.focus_app;

        if (!focusedApp) {
            // If the app has just lost focus to the panel, pretend
            // nothing happened; otherwise you can't keynav to the
            // app menu.
            if (global.stage_input_mode == Shell.StageInputMode.FOCUSED)
                return;
        }

        let targetApp = focusedApp != null ? focusedApp : lastStartedApp;

        if (targetApp == null) {
            if (!this._targetIsCurrent)
                return;

            this.actor.reactive = false;
            this._targetIsCurrent = false;

            Tweener.removeTweens(this.actor);
            Tweener.addTween(this.actor, { opacity: 0,
                                           time: Overview.ANIMATION_TIME,
                                           transition: 'easeOutQuad' });
            return;
        }

        if (!this._targetIsCurrent) {
            this.actor.reactive = true;
            this._targetIsCurrent = true;

            Tweener.removeTweens(this.actor);
            Tweener.addTween(this.actor, { opacity: 255,
                                           time: Overview.ANIMATION_TIME,
                                           transition: 'easeOutQuad' });
        }

        if (targetApp == this._targetApp) {
            if (targetApp && targetApp.get_state() != Shell.AppState.STARTING)
                this.stopAnimation();
            return;
        }

        this._spinner.actor.hide();
        if (this._iconBox.child != null)
            this._iconBox.child.destroy();
        this._iconBox.hide();
        this._label.setText('');

        this._targetApp = targetApp;
        let icon = targetApp.get_faded_icon(2 * PANEL_ICON_SIZE);

        this._label.setText(targetApp.get_name());
        // TODO - _quit() doesn't really work on apps in state STARTING yet
        this._quitMenu.label.set_text(_("Quit %s").format(targetApp.get_name()));

        this._iconBox.set_child(icon);
        this._iconBox.show();

        if (targetApp.get_state() == Shell.AppState.STARTING)
            this.startAnimation();

        this.emit('changed');
    }
};

Signals.addSignalMethods(AppMenuButton.prototype);
/** Fired whenever the application shown in the app menu button changes.
 * @name changed
 * @event
 * @memberof AppMenuButton
 */

/** creates a new ActivitiesButton
 *
 * We give our top-level actor {@link #actor} style name 'panelActivities'.
 *
 * We add a {@link Shell.GenericContainer} to our top-level actor
 * and connect up its width/height/allocate signals to
 * {@link #_containerGetPreferredWidth}, {@link #_containerGetPreferredHeight}
 * and {@link #_containerAllocate}.
 *
 * In this we place a label with the text "Activities" {@link #_label}, as
 * well as a {@link Layout.HotCorner} to launch the overview, {@link #_hotCorner}.
 *
 * The activities  doesn't have a menu, yet it is a {@link PanelMenu.Button}.
 * This is because everything else in the top bar is a {@link PanelMenu.Button},
 * and it simplifies some things to make this be one too.
 *
 * We just hack it up to not actually have a menu attached to it.
 *
 * To do this, we override {@link #menu}'s open, close and toggle methods
 * to {@link #_onMenuOpenRequest}, {@link #_onMenuCloseRequest} and
 * {@link #_onMenuToggleRequest}. In these we update the menu's 'isOpen'
 * property and emit ['open-state-changed']{@link PopupMenu.PopupMenu.open-state-changed}
 * signals as would be expected of a menu, but we don't actually open
 * or close the menu.
 *
 * @classdesc
 *
 * ![{@link ActivitiesButton}](Panel.ActivitiesButton.png)
 *
 * The Activities button is in the top-left of the top panel.
 *
 * Clicking on it launches the overview, and hovering over it with an
 * item being dragged will also launch the overview (allowing the draggable
 * to be dropped on a window/workspace).
 *
 * It has a hot corner in the top-left corner (top-right for right-to-left
 * text direction) such that hovering the mouse over that will launch
 * the Overview.
 *
 * It doesn't have a menu, yet it is a {@link PanelMenu.Button}.
 * This is because everything else in the top bar is a {@link PanelMenu.Button},
 * and it simplifies some things to make this be one too.
 *
 * We just hack it up to not actually have a menu attached to it.
 * @class
 * @extends PanelMenu.Button
 */
function ActivitiesButton() {
    this._init.apply(this, arguments);
}

ActivitiesButton.prototype = {
    __proto__: PanelMenu.Button.prototype,

    _init: function() {
        PanelMenu.Button.prototype._init.call(this, 0.0);

        let container = new Shell.GenericContainer();
        container.connect('get-preferred-width', Lang.bind(this, this._containerGetPreferredWidth));
        container.connect('get-preferred-height', Lang.bind(this, this._containerGetPreferredHeight));
        container.connect('allocate', Lang.bind(this, this._containerAllocate));
        this.actor.add_actor(container);
        this.actor.name = 'panelActivities';

        /* Translators: If there is no suitable word for "Activities"
           in your language, you can use the word for "Overview". */
        this._label = new St.Label({ text: _("Activities") });
        container.add_actor(this._label);

        this._hotCorner = new Layout.HotCorner();
        container.add_actor(this._hotCorner.actor);

        // Hack up our menu...
        this.menu.open = Lang.bind(this, this._onMenuOpenRequest);
        this.menu.close = Lang.bind(this, this._onMenuCloseRequest);
        this.menu.toggle = Lang.bind(this, this._onMenuToggleRequest);

        this.actor.connect('captured-event', Lang.bind(this, this._onCapturedEvent));
        this.actor.connect_after('button-release-event', Lang.bind(this, this._onButtonRelease));
        this.actor.connect_after('key-release-event', Lang.bind(this, this._onKeyRelease));

        Main.overview.connect('showing', Lang.bind(this, function() {
            this.actor.add_style_pseudo_class('overview');
            this._escapeMenuGrab();
        }));
        Main.overview.connect('hiding', Lang.bind(this, function() {
            this.actor.remove_style_pseudo_class('overview');
            this._escapeMenuGrab();
        }));

        this._xdndTimeOut = 0;
    },

    /** Callback for the 'get-preferred-width' signal of the
     * {@link Shell.GenericContainer} in our top-level actor.
     *
     * We allocate the width asked for by the "Activities" label {@link #_label}.
     */
    _containerGetPreferredWidth: function(actor, forHeight, alloc) {
        [alloc.min_size, alloc.natural_size] = this._label.get_preferred_width(forHeight);
    },

    /** Callback for the 'get-preferred-height' signal of the
     * {@link Shell.GenericContainer} in our top-level actor.
     *
     * We allocate the height asked for by the "Activities" label {@link #_label}.
     */
    _containerGetPreferredHeight: function(actor, forWidth, alloc) {
        [alloc.min_size, alloc.natural_size] = this._label.get_preferred_height(forWidth);
    },

    /** Callback for the 'allocate' signal of the
     * {@link Shell.GenericContainer} in our top-level actor.
     *
     * We allocate the "Activities" label {@link #_label} the entire space
     * available.
     *
     * The hot corner {@link #_hotCorner} is placed at the top-left corner
     * of the primary monitor (top-right for right-to-left text direction).
     */
    _containerAllocate: function(actor, box, flags) {
        this._label.allocate(box, flags);

        // The hot corner needs to be outside any padding/alignment
        // that has been imposed on us
        let primary = Main.layoutManager.primaryMonitor;
        let hotBox = new Clutter.ActorBox();
        let ok, x, y;
        if (actor.get_direction() == St.TextDirection.LTR) {
            [ok, x, y] = actor.transform_stage_point(primary.x, primary.y)
        } else {
            [ok, x, y] = actor.transform_stage_point(primary.x + primary.width, primary.y);
            // hotCorner.actor has northeast gravity, so we don't need
            // to adjust x for its width
        }

        hotBox.x1 = Math.round(x);
        hotBox.x2 = hotBox.x1 + this._hotCorner.actor.width;
        hotBox.y1 = Math.round(y);
        hotBox.y2 = hotBox.y1 + this._hotCorner.actor.height;
        this._hotCorner.actor.allocate(hotBox, flags);
    },

    /** Implements the DND interface allowing the Activities button to be
     * dragged over.
     *
     * Starts a timeout such that if {@link BUTTON_DND_ACTIVATION_TIMEOUT}
     * milliseconds pass and the activities button still has the dragged item
     * being held over it, we will launch the overview.
     *
     * @TODO DND handleDragOver
     * @see #_xdndShowOverview
     */
    handleDragOver: function(source, actor, x, y, time) {
        if (source != Main.xdndHandler)
            return;

        if (this._xdndTimeOut != 0)
            Mainloop.source_remove(this._xdndTimeOut);
        this._xdndTimeOut = Mainloop.timeout_add(BUTTON_DND_ACTIVATION_TIMEOUT,
                                                 Lang.bind(this, this._xdndShowOverview, actor));
    },

    /** Callback when the overview shows or hides - we close our menu if
     * it is open (to avoid screwing up popup menu code since we're
     * hacking around to hide our popup menu) */
    _escapeMenuGrab: function() {
        if (this.menu.isOpen)
            this.menu.close();
    },

    /** Callback when any event is captured to {@link #actor} ('captured-event').
     *
     * If it was a click **and** the activities button is currently sensitive
     * to clicks, we allow the event through to be handled by the activities
     * button.
     *
     * Otherwise, we block the event so that the button-release-event is
     * not passed through.
     * @returns {boolean} true if we want to block the event (we don't want
     * the overview to launch), false otherwise
     * @see Layout.HotCorner#shouldToggleOverviewOnClick
     */
    _onCapturedEvent: function(actor, event) {
        if (event.type() == Clutter.EventType.BUTTON_PRESS) {
            if (!this._hotCorner.shouldToggleOverviewOnClick())
                return true;
        }
        return false;
    },

    /** Callback when {@link #menu}'s 'open' method is called.
     *
     * We update [this.menu.isOpen]{@link PopupMenu.PopupMenu#isOpen} to be
     * `true` and emit ['open-state-changed']{@link PopupMenu.PopupMenu.open-state-changed}
     * from our menu as expected, but do not actually open the menu
     * since the activities button isn't meant to have one. */
    _onMenuOpenRequest: function() {
        this.menu.isOpen = true;
        this.menu.emit('open-state-changed', true);
    },

    /** Callback when {@link #menu}'s 'close' method is called.
     *
     * We update [this.menu.isOpen]{@link PopupMenu.PopupMenu#isOpen} to be
     * `false` and emit ['open-state-changed']{@link PopupMenu.PopupMenu.open-state-changed}
     * from our menu as expected, but do not actually close the menu
     * since the activities button isn't meant to have one. */
    _onMenuCloseRequest: function() {
        this.menu.isOpen = false;
        this.menu.emit('open-state-changed', false);
    },

    /** Callback when {@link #menu}'s 'toggle' method is called.
     *
     * We update [this.menu.isOpen]{@link PopupMenu.PopupMenu#isOpen}
     * and emit ['open-state-changed']{@link PopupMenu.PopupMenu.open-state-changed}
     * from our menu as expected, but do not actually toggle the menu
     * since the activities button isn't meant to have one. */
    _onMenuToggleRequest: function() {
        this.menu.isOpen = !this.menu.isOpen;
        this.menu.emit('open-state-changed', this.menu.isOpen);
    },

    /** Callback when the activities button is clicked. We close our
     * menu and toggle the overview. */
    _onButtonRelease: function() {
        if (this.menu.isOpen) {
            this.menu.close();
            Main.overview.toggle();
        }
    },

    /** Callback when a key release event is received on the activities button.
     *
     * If it is return or space, we close our menu (if it's open) and toggle
     * the overview.
     */
    _onKeyRelease: function(actor, event) {
        let symbol = event.get_key_symbol();
        if (symbol == Clutter.KEY_Return || symbol == Clutter.KEY_space) {
            if (this.menu.isOpen)
                this.menu.close();
            Main.overview.toggle();
        }
    },

    /** Toggles the overview if the user has held a dragged item over it for
     * more than {@link BUTTON_DND_ACTIVATION_TIMEOUT} milliseconds.
     *
     * Shows the overview temporarily and begins an item drag on the dragged
     * item.
     *
     * @see Overview.Overview#showTemporarily
     * @see Overview.Overview#beginItemDrag
     * @see #handleDragOver
     */
    _xdndShowOverview: function(actor) {
        let [x, y, mask] = global.get_pointer();
        let pickedActor = global.stage.get_actor_at_pos(Clutter.PickMode.REACTIVE, x, y);

        if (pickedActor == this.actor) {
            if (!Main.overview.visible && !Main.overview.animationInProgress) {
                Main.overview.showTemporarily();
                Main.overview.beginItemDrag(actor);
            }
        }

        Mainloop.source_remove(this._xdndTimeOut);
        this._xdndTimeOut = 0;
    }
};

/** creates a new PanelCorner
 *
 * ![A {@link PanelCorner} on the left side of {@link Panel#_leftBox}](pics/Panel.PanelCorner.png)
 *
 * The corner will be drawn on the `side` side of `box`.
 *
 * We store `side` and `box` in {@link #_side} and {@link #_box}.
 *
 * Then we create our top-level actor {@link #actor}, a {@link St.DrawingArea}
 * on which we will draw the corner ({@link #_repaint}).
 * @param {St.BoxLayout} box - the box on which to place the corner, LEFT
 * or RIGHT.
 * ({@link Panel#_leftBox}, {@link Panel#_rightBox}).
 * @param {St.Side} side - the side of `box` on which to place the corner.
 * @classdesc
 *
 * ![A {@link PanelCorner} on the left side of {@link Panel#_leftBox}](pics/Panel.PanelCorner.png)
 *
 * This is the corner drawn on either side of the top panel. It isn't actually
 * built into the CSS of the top panel's side (since the rounded corners curve
 * out rather than in), so each corner is *manually* drawn on!
 *
 * Some care is taken to try and draw the corner so its colour matches that
 * of the bit of panel/button directly above.
 * @class
 */
function PanelCorner(panel, side) {
    this._init(panel, side);
}

PanelCorner.prototype = {
    _init: function(box, side) {
        /** the side of the box on which to draw the corner - LEFT or RIGHT
         * @type {St.Side} */
        this._side = side;

        /** the box on which to draw the corner ({@link Panel#_leftBox},
         * {@link Panel#_rightBox}).
         * @type {St.BoxLayout} */
        this._box = box;
        this._box.connect('style-changed', Lang.bind(this, this._boxStyleChanged));

        /** The top-level actor, where we draw our corner.
         * @type {St.DrawingArea}
         * @see #_repaint
         */
        this.actor = new St.DrawingArea({ style_class: 'panel-corner' });
        this.actor.connect('style-changed', Lang.bind(this, this._styleChanged));
        this.actor.connect('repaint', Lang.bind(this, this._repaint));
    },

    /** Finds the right-most button in `container`.
     *
     * This:
     *
     * * finds the right-most child of `container` that is visible;
     * * if it has style class 'panel-menu' or 'panel-button', return it, otherwise:
     * * look through the children of this child for a child with style class
     *   'panel-menu' or 'panel-button'.
     *
     * Used to colour the panel corner to match the button above it.
     *
     * @param {St.Widget} container - container to find the button in.
     * @returns {St.Widget} a descendant of the right-most visible child of
     * `container` such that the descendant has style class 'panel-menu' or
     * 'panel-button', or `null` if not found.
     * @see {@link #_findLeftmostButton}, the same but on the left side.
     */
    _findRightmostButton: function(container) {
        if (!container.get_children)
            return null;

        let children = container.get_children();

        if (!children || children.length == 0)
            return null;

        // Start at the back and work backward
        let index = children.length - 1;
        while (!children[index].visible && index >= 0)
            index--;

        if (index < 0)
            return null;

        if (!(children[index].has_style_class_name('panel-menu')) &&
            !(children[index].has_style_class_name('panel-button')))
            return this._findRightmostButton(children[index]);

        return children[index];
    },

    /** Finds the descendant of the left-most visible button in `container`
     * with style class 'panel-button' or 'panel-menu'.
     *
     * See {@link #_findRightmostButton} for more details (same thing
     * but that is for the right side and this is for the left).
     *
     * @inheritdoc #_findRightmostButton
     */
    _findLeftmostButton: function(container) {
        if (!container.get_children)
            return null;

        let children = container.get_children();

        if (!children || children.length == 0)
            return null;

        // Start at the front and work forward
        let index = 0;
        while (!children[index].visible && index < children.length)
            index++;

        if (index == children.length)
            return null;

        if (!(children[index].has_style_class_name('panel-menu')) &&
            !(children[index].has_style_class_name('panel-button')))
            return this._findLeftmostButton(children[index]);

        return children[index];
    },

    /** Callback when {@link #_box}'s style changes.
     *
     * Updates {@link #_button} with the button directly above the corner
     * and listens to its 'style-changed' signal to ensure that the style
     * classes of the button are also added to this corner. */
    _boxStyleChanged: function() {
        let side = this._side;

        let rtlAwareContainer = this._box instanceof St.BoxLayout;
        if (rtlAwareContainer &&
            this._box.get_direction() == St.TextDirection.RTL) {
            if (this._side == St.Side.LEFT)
                side = St.Side.RIGHT;
            else if (this._side == St.Side.RIGHT)
                side = St.Side.LEFT;
        }

        let button;
        if (side == St.Side.LEFT)
            button = this._findLeftmostButton(this._box);
        else if (side == St.Side.RIGHT)
            button = this._findRightmostButton(this._box);

        if (button) {
            if (this._button && this._buttonStyleChangedSignalId) {
                this._button.disconnect(this._buttonStyleChangedSignalId);
                this._button.style = null;
            }

            /** The PanelMenu.Button directly above the corner (if any),
             * used to colour the corner.
             * @type {?PanelMenu.Button} */
            this._button = button;

            button.connect('destroy', Lang.bind(this,
                function() {
                    if (this._button == button) {
                        this._button = null;
                        this._buttonStyleChangedSignalId = 0;
                    }
                }));

            // Synchronize the locate button's pseudo classes with this corner
            this._buttonStyleChangedSignalId = button.connect('style-changed', Lang.bind(this,
                function(actor) {
                    let pseudoClass = button.get_style_pseudo_class();
                    this.actor.set_style_pseudo_class(pseudoClass);
                }));

            // The corner doesn't support theme transitions, so override
            // the .panel-button default
            button.style = 'transition-duration: 0';
        }
    },

    /** Called whenever {@link #actor} is repainted - draws the corner.
     *
     * We use the `-panel-corner-radius` property of the corner's CSS style
     * draw the corner the appropriate corner radius.
     *
     * Properties
     * '-panel-corner-inner-border-width',
     * '-panel-corner-outer-border-width',
     * '-panel-corner-background-color',
     * '-panel-corner-inner-border-color', and
     * '-panel-corner-outer-border-color'
     * are also used to draw the corner appropriately.
     */
    _repaint: function() {
        let node = this.actor.get_theme_node();

        let cornerRadius = node.get_length("-panel-corner-radius");
        let innerBorderWidth = node.get_length('-panel-corner-inner-border-width');
        let outerBorderWidth = node.get_length('-panel-corner-outer-border-width');

        let backgroundColor = node.get_color('-panel-corner-background-color');
        let innerBorderColor = node.get_color('-panel-corner-inner-border-color');
        let outerBorderColor = node.get_color('-panel-corner-outer-border-color');

        let cr = this.actor.get_context();
        cr.setOperator(Cairo.Operator.SOURCE);

        cr.moveTo(0, 0);
        if (this._side == St.Side.LEFT)
            cr.arc(cornerRadius,
                   innerBorderWidth + cornerRadius,
                   cornerRadius, Math.PI, 3 * Math.PI / 2);
        else
            cr.arc(0,
                   innerBorderWidth + cornerRadius,
                   cornerRadius, 3 * Math.PI / 2, 2 * Math.PI);
        cr.lineTo(cornerRadius, 0);
        cr.closePath();

        let savedPath = cr.copyPath();

        let over = _over(innerBorderColor,
                         _over(outerBorderColor, backgroundColor));
        Clutter.cairo_set_source_color(cr, over);
        cr.fill();

        let xOffsetDirection = this._side == St.Side.LEFT ? -1 : 1;
        let offset = outerBorderWidth;
        over = _over(innerBorderColor, backgroundColor);
        Clutter.cairo_set_source_color(cr, over);

        cr.save();
        cr.translate(xOffsetDirection * offset, - offset);
        cr.appendPath(savedPath);
        cr.fill();
        cr.restore();

        if (this._side == St.Side.LEFT)
            cr.rectangle(cornerRadius - offset, 0, offset, outerBorderWidth);
        else
            cr.rectangle(0, 0, offset, outerBorderWidth);
        cr.fill();

        offset = innerBorderWidth;
        Clutter.cairo_set_source_color(cr, backgroundColor);

        cr.save();
        cr.translate(xOffsetDirection * offset, - offset);
        cr.appendPath(savedPath);
        cr.fill();
        cr.restore();
    },

    /** Callback when the corner's style changes.
     * The '-panel-corner-radius' and '-panel-corner-inner-border-width' properties
     * are used to set the size and anchor point of the corner {@link #actor}.
     */
    _styleChanged: function() {
        let node = this.actor.get_theme_node();

        let cornerRadius = node.get_length("-panel-corner-radius");
        let innerBorderWidth = node.get_length('-panel-corner-inner-border-width');

        this.actor.set_size(cornerRadius, innerBorderWidth + cornerRadius);
        this.actor.set_anchor_point(0, innerBorderWidth);
    }
};


/** creates a new Panel
 *
 * We create the top-level actor {@link #actor}.
 *
 * ![{@link #_leftBox} (red), {@link #_centerBox} (green) and {@link #_rightBox} (blue) in the top {@link Panel}](pics/Panel.boxes.png)
 * 
 * In this we place three "boxes", each a horizontal St.BoxLayout. They are
 * called {@link #_leftBox}, {@link #_centerBox} and {@link #_rightBox}.
 *
 * By default the left box contains the activities button and app menu button,
 * the center box contains the clock/date menu, and the right box contains the
 * user menu and all the status indicators (volume, a11y, ...).
 *
 * We then make two {@link PanelCorner}s, one for the left side of the panel
 * and one for the right ({@link #_leftCorner}, {@link #_rightCorner}).
 *
 * * if the session is a user session (as opposed to GDM), we create an
 * {@link ActivitiesButton} and {@link AppMenuButton}, putting them in the
 * left box.
 * * we create the {@link DateMenu.DateMenuButton} and put it into the
 * center box, showing an events list iff it's a user session.
 * * we create all the status indicators and place them in the
 *
 * Then we store which buttons we'll place into the right box, either
 * {@link GDM_STATUS_AREA_ORDER} or {@link STANDARD_STATUS_AREA_ORDER}
 * depending on the type of session. We don't create them yet though; this
 * is done in {@link #startStatusArea} by {@link Main.start} (since some
 * of the status buttons need various services or gnome-shell objects
 * to have been initialized first).
 *
 * We listen to {@link Main.statusIconDispatcher}'s status-icon-added
 * and status-icon-removed signals, creating or removing status icons if
 * the {@link StatusIconDispatcher.StatusIconDispatcher} determines that these
 * icon should be in the top panel.
 *
 * We also create a {@link PopupMenu.PopupMenuManager} {@link #_menus}
 * to handle the menus for all of the buttons added to the top panel.
 *
 * Finally we add {@link #actor} to {@link Layout.LayoutManager#panelBox}
 * and also add {@link #actor} to the {@link CtrlAltTab.CtrlAltTabManager}.
 * @see StatusIconDispatcher.StatusIconDispatcher.status-icon-added
 * @see #startStatusArea
 * @classdesc
 *
 * The Panel is the bar on top of the screen.
 *
 * ![{@link #_leftBox} (red), {@link #_centerBox} (green) and {@link #_rightBox} (blue) in the top {@link Panel}](pics/Panel.boxes.png)
 *
 * It is split into three parts, the left box, centre box and right box.
 * 
 * By default the left box contains the activities button and app menu button,
 * the center box contains the clock/date menu, and the right box contains the
 * user menu and all the status indicators (volume, a11y, ...).
 *
 * Add buttons to the top-right area of the panel (the "status area") with
 * {@link #addToStatusArea}.
 *
 * @class
 */
function Panel() {
    this._init();
}

Panel.prototype = {
    _init : function() {
        /** Top-level actor for the Panel. Style name 'panel'.
         * @type {Shell.GenericContainer} */
        this.actor = new Shell.GenericContainer({ name: 'panel',
                                                  reactive: true });
        this.actor._delegate = this;

        /** Hash map of items in the status area (top-right of the panel):
         * name of the indicator maps to the button (e.g. key 'a11y', value
         * {@link Accessibility.ATIndicator}).
         *
         * @see STANDARD_STATUS_AREA_SHELL_IMPLEMENTATION
         * @type {Object.<string, PanelMenu.Button>}
         */
        this._statusArea = {};

        Main.overview.connect('shown', Lang.bind(this, function () {
            this.actor.add_style_class_name('in-overview');
        }));
        Main.overview.connect('hiding', Lang.bind(this, function () {
            this.actor.remove_style_class_name('in-overview');
        }));

        /** the popup menu manager - manages the popup menus for all the
         * panel buttons added to the panel (if you add it with
         * {@link #addToStatusArea}).
         * @type {PopupMenu.PopupMenuManager} */
        this._menus = new PopupMenu.PopupMenuManager(this);

        /** The left part of the panel. By default holds the
         * {@link ActivitiesButton} and the {@link AppMenuButton}.
         *
         * Style name 'panelLeft'.
         * @type {St.BoxLayout} */
        this._leftBox = new St.BoxLayout({ name: 'panelLeft' });
        this.actor.add_actor(this._leftBox);
        /** The centre part of the panel. By default holds the
         * [clock/date menu]{@link DateMenu.DateMenuButton}.
         *
         * Style name 'panelCenter'.
         * @type {St.BoxLayout} */
        this._centerBox = new St.BoxLayout({ name: 'panelCenter' });
        this.actor.add_actor(this._centerBox);
        /** The right part of the panel. By default holds all the status
         * buttons ({@link STANDARD_STATUS_AREA_ORDER}), e.g. the
         * user menu, volume indicator, and anything added with
         * {@link #addToStatusArea}.
         *
         * Style name 'panelRight'.
         * @type {St.BoxLayout} */
        this._rightBox = new St.BoxLayout({ name: 'panelRight' });
        this.actor.add_actor(this._rightBox);

        if (this.actor.get_direction() == St.TextDirection.RTL)
            /** The left corner of the top panel.
             * @type {PanelCorner} */
            this._leftCorner = new PanelCorner(this._rightBox, St.Side.LEFT);
        else
            this._leftCorner = new PanelCorner(this._leftBox, St.Side.LEFT);

        this.actor.add_actor(this._leftCorner.actor);

        if (this.actor.get_direction() == St.TextDirection.RTL)
            /** The right corner of the top panel.
             * @type {PanelCorner} */
            this._rightCorner = new PanelCorner(this._leftBox, St.Side.RIGHT);
        else
            this._rightCorner = new PanelCorner(this._rightBox, St.Side.RIGHT);
        this.actor.add_actor(this._rightCorner.actor);

        this.actor.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate', Lang.bind(this, this._allocate));

        /* Button on the left side of the panel. */
        if (global.session_type == Shell.SessionType.USER) {
            this._activitiesButton = new ActivitiesButton();
            this._activities = this._activitiesButton.actor;
            this._leftBox.add(this._activities);

            // The activities button has a pretend menu, so as to integrate
            // more cleanly with the rest of the panel
            this._menus.addMenu(this._activitiesButton.menu);

            this._appMenu = new AppMenuButton();
            this._leftBox.add(this._appMenu.actor);
            this._menus.addMenu(this._appMenu.menu);
        }

        /* center */
        if (global.session_type == Shell.SessionType.USER)
            this._dateMenu = new DateMenu.DateMenuButton({ showEvents: true });
        else
            this._dateMenu = new DateMenu.DateMenuButton({ showEvents: false });
        this._centerBox.add(this._dateMenu.actor, { y_fill: true });
        this._menus.addMenu(this._dateMenu.menu);

        /* right */
        if (global.session_type == Shell.SessionType.GDM) {
            this._status_area_order = GDM_STATUS_AREA_ORDER;
            this._status_area_shell_implementation = GDM_STATUS_AREA_SHELL_IMPLEMENTATION;
        } else {
            this._status_area_order = STANDARD_STATUS_AREA_ORDER;
            this._status_area_shell_implementation = STANDARD_STATUS_AREA_SHELL_IMPLEMENTATION;
        }

        Main.statusIconDispatcher.connect('status-icon-added', Lang.bind(this, this._onTrayIconAdded));
        Main.statusIconDispatcher.connect('status-icon-removed', Lang.bind(this, this._onTrayIconRemoved));

        Main.layoutManager.panelBox.add(this.actor);
        Main.ctrlAltTabManager.addGroup(this.actor, _("Top Bar"), 'start-here',
                                        { sortGroup: CtrlAltTab.SortGroup.TOP });
    },

    /** Callback for {@link #actor}'s 'get-preferred-width' signal.
     *
     * We allocate the width of the primary onitor.
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        alloc.min_size = -1;
        alloc.natural_size = Main.layoutManager.primaryMonitor.width;
    },

    /** Callback for {@link #actor}'s 'get-preferred-height' signal.
     *
     * We don't implement this (i.e. allocate height -1) since the height
     * is forced by the CSS.
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        // We don't need to implement this; it's forced by the CSS
        alloc.min_size = -1;
        alloc.natural_size = -1;
    },

    /** Callback for {@link #actor}'s 'allocate' signal.
     *
     * This positions the left centre and right boxes to the far left, middle
     * and far right of the panel, giving them their preferred widths if
     * possible.
     *
     * If there's not enough width to fit everything, the centre box
     * gets its requested width and the left and right boxes are truncated
     * to fit in the remaining space.
     *
     * The left and right panel corners are also allocated to sit below and
     * either side of the panel.
     */
    _allocate: function(actor, box, flags) {
        let allocWidth = box.x2 - box.x1;
        let allocHeight = box.y2 - box.y1;

        let [leftMinWidth, leftNaturalWidth] = this._leftBox.get_preferred_width(-1);
        let [centerMinWidth, centerNaturalWidth] = this._centerBox.get_preferred_width(-1);
        let [rightMinWidth, rightNaturalWidth] = this._rightBox.get_preferred_width(-1);

        let sideWidth, centerWidth;
        centerWidth = centerNaturalWidth;
        sideWidth = (allocWidth - centerWidth) / 2;

        let childBox = new Clutter.ActorBox();

        childBox.y1 = 0;
        childBox.y2 = allocHeight;
        if (this.actor.get_direction() == St.TextDirection.RTL) {
            childBox.x1 = allocWidth - Math.min(Math.floor(sideWidth),
                                                leftNaturalWidth);
            childBox.x2 = allocWidth;
        } else {
            childBox.x1 = 0;
            childBox.x2 = Math.min(Math.floor(sideWidth),
                                   leftNaturalWidth);
        }
        this._leftBox.allocate(childBox, flags);

        childBox.x1 = Math.ceil(sideWidth);
        childBox.y1 = 0;
        childBox.x2 = childBox.x1 + centerWidth;
        childBox.y2 = allocHeight;
        this._centerBox.allocate(childBox, flags);

        childBox.y1 = 0;
        childBox.y2 = allocHeight;
        if (this.actor.get_direction() == St.TextDirection.RTL) {
            childBox.x1 = 0;
            childBox.x2 = Math.min(Math.floor(sideWidth),
                                   rightNaturalWidth);
        } else {
            childBox.x1 = allocWidth - Math.min(Math.floor(sideWidth),
                                                rightNaturalWidth);
            childBox.x2 = allocWidth;
        }
        this._rightBox.allocate(childBox, flags);

        let [cornerMinWidth, cornerWidth] = this._leftCorner.actor.get_preferred_width(-1);
        let [cornerMinHeight, cornerHeight] = this._leftCorner.actor.get_preferred_width(-1);
        childBox.x1 = 0;
        childBox.x2 = cornerWidth;
        childBox.y1 = allocHeight;
        childBox.y2 = allocHeight + cornerHeight;
        this._leftCorner.actor.allocate(childBox, flags);

        let [cornerMinWidth, cornerWidth] = this._rightCorner.actor.get_preferred_width(-1);
        let [cornerMinHeight, cornerHeight] = this._rightCorner.actor.get_preferred_width(-1);
        childBox.x1 = allocWidth - cornerWidth;
        childBox.x2 = allocWidth;
        childBox.y1 = allocHeight;
        childBox.y2 = allocHeight + cornerHeight;
        this._rightCorner.actor.allocate(childBox, flags);
    },

    /** Creates the status icons for all the status indicators and adds them
     * to {@link #_rightBox} (status area) in the top panel.
     * @see #addToStatusArea */
    startStatusArea: function() {
        for (let i = 0; i < this._status_area_order.length; i++) {
            let role = this._status_area_order[i];
            let constructor = this._status_area_shell_implementation[role];
            if (!constructor) {
                // This icon is not implemented (this is a bug)
                continue;
            }

            let indicator = new constructor();
            this.addToStatusArea(role, indicator, i);
        }
    },

    /** Internal function used by {@link #addToStatusArea}.
     *
     * This is what actually adds the panel button's actor to the
     * {@link #_rightBox} at the specified position.
     * @param {Clutter.Actor} actor - the actor to add
     * @param {number} position - position to insert the actor (0 is on the left).
     */
    _insertStatusItem: function(actor, position) {
        let children = this._rightBox.get_children();
        let i;
        for (i = children.length - 1; i >= 0; i--) {
            let rolePosition = children[i]._rolePosition;
            if (position > rolePosition) {
                this._rightBox.insert_actor(actor, i + 1);
                break;
            }
        }
        if (i == -1) {
            // If we didn't find a position, we must be first
            this._rightBox.insert_actor(actor, 0);
        }
        actor._rolePosition = position;
    },

    /** Adds a button/inidcator to the status area, i.e. the right-most
     * part of the top panel {@link #_rightBox}.
     *
     * The button/indicator to be added should be a {@link PanelMenu.Button}
     * or subclass.
     *
     * Its menu is automatically added to the menu manager {@link #_menus},
     * and it is added to the hash map {@link #_statusArea} with key `role`.
     *
     * We also connect to its 'destroy' signal to remove it from the status area.
     *
     * If there is already an indicator with the same `role` in the status
     * area (as indicated by {@link #_statusArea} we quit with an error.
     * @param {string} role - a string that acts as an ID for the indicator.
     * For example the accessibility button uses 'a11y' (see
     * {@link STANDARD_STATUS_AREA_SHELL_IMPLEMENTATION} for examples). The only
     * use of this is that only one button may be added to the status area for
     * each role.
     * @param {PanelMenu.Button} indicator - the button to add (or a subclass).
     * @param {number} [position=0] - the position in the status area to put
     * the button (0 is left-most). If not provided, it will be added to the
     * left-most side. If -1, it will be added to the right-most side.
     * @see #_insertStatusItem
     */
    addToStatusArea: function(role, indicator, position) {
        if (this._statusArea[role])
            throw new Error('Extension point conflict: there is already a status indicator for role ' + role);

        if (!(indicator instanceof PanelMenu.Button))
            throw new TypeError('Status indicator must be an instance of PanelMenu.Button');

        if (!position)
            position = 0;
        this._insertStatusItem(indicator.actor, position);
        this._menus.addMenu(indicator.menu);

        this._statusArea[role] = indicator;
        let destroyId = indicator.connect('destroy', Lang.bind(this, function(emitter) {
            this._statusArea[role] = null;
            emitter.disconnect(destroyId);
        }));

        return indicator;
    },

    /** Callback for {@link Main.StatusIconDispatcher}'s
     * ['status-icon-added']{@link StatusIconDispatcher.StatusIconDispatcher.status-icon-added}
     * signal.
     *
     * This is emitted for tray icons that the status icon dispatcher determines
     * should be placed into the top panel rather than the bottom message tray
     * (see {@link StatusIconDispatcher.STANDARD_TRAY_ICON_IMPLEMENTATIONS}).
     *
     * If we do *not* already have a gnome-shell indicator for that `role`
     * (for example if `role` is 'a11y' we have the
     * [accessibility indicator]{@link Accessibility.ATIndicator}), we
     * create a {@link PanelMenu.ButtonBox}, put the icon `icon` in its actor,
     * and insert it to the status area ({@link #_insertStatusItem} - no need
     * to use {@link #addToStatusArea because we are inserting a
     * {@link PanelMenu.ButtonBox}, not a {@link PanelMenu.Button} which
     * additionally has a menu).
     *
     * This could be used (for example) to add the legacy/non-gnome-shell Skype 
     * icon to the top panel (the reason it doesn't get added here is that
     * the status icon dispatcher dispatches it to the message tray instead
     * of the top panel).
     * @inheritdoc StatusIconDispatcher.StatusIconDispatcher.status-icon-added
     */
    _onTrayIconAdded: function(o, icon, role) {
        if (this._status_area_shell_implementation[role]) {
            // This icon is legacy, and replaced by a Shell version
            // Hide it
            return;
        }

        icon.height = PANEL_ICON_SIZE;
        let buttonBox = new PanelMenu.ButtonBox();
        let box = buttonBox.actor;
        box.add_actor(icon);

        this._insertStatusItem(box, this._status_area_order.indexOf(role));
    },

    /** Callback for {@link Main.StatusIconDispatcher}'s
     * ['status-icon-removed']{@link StatusIconDispatcher.StatusIconDispatcher.status-icon-removed}
     * signal.
     *
     * Emitted when a tray icon that the status icon dispatcher has determined
     * should be in the top panel (Rather than the bottom message tray) is
     * removed from the tray area.
     *
     * This destroys the {@link PanelMenu.ButtonBox} we made for the `icon`.
     *
     * @inheritdoc StatusIconDispatcher.StatusIconDispatcher.status-icon-removed
     */
    _onTrayIconRemoved: function(o, icon) {
        let box = icon.get_parent();
        if (box && box._delegate instanceof PanelMenu.ButtonBox)
            box.destroy();
    },

};
