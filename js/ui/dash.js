// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file handles the dash (left-hand sidebar in the overview showing the
 * application favourites).
 * 
 * ![the Dash](pics/dash.png)
 */

const Clutter = imports.gi.Clutter;
const Signals = imports.signals;
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const AppDisplay = imports.ui.appDisplay;
const AppFavorites = imports.ui.appFavorites;
const DND = imports.ui.dnd;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Workspace = imports.ui.workspace;

/** The time (in seconds) it takes for an app icon in the dash to fade in
 * when the user adds it to their favourites.
 * @const
 * @default
 * @type {number}
 */
const DASH_ANIMATION_TIME = 0.2;

/** creates a new DashItemContainer
 * @classdesc This is a convenience class defining any item that will appear
 * in the {@link Dash}. It acts like a `St.Bin` but takes the child's
 * scale into account when requesting a size.
 *
 * It is intended to be used with items where you might want to modify the
 * child actor's size often. This class just makes sure that the allocation
 * stays up to date with the child's scale.
 *
 * Use {@link #setChild} to set the child actor and set the
 * {@link #childScale} property to adjust the child actor's
 * size.
 *
 * The UI actor for the DashItemContainer is a `Shell.GenericContainer` with
 * style 'dash-item-container'.
 * @class
 */
function DashItemContainer() {
    this._init();
}

DashItemContainer.prototype = {
    _init: function() {
        /** the UI actor for the DashItemContainer is a
         * `Shell.GenericContainer` with style class 'dash-item-container'.
         * @type {Shell.GenericContainer}
         */
        this.actor = new Shell.GenericContainer({ style_class: 'dash-item-container' });
        this.actor.connect('get-preferred-width',
                           Lang.bind(this, this._getPreferredWidth));
        this.actor.connect('get-preferred-height',
                           Lang.bind(this, this._getPreferredHeight));
        this.actor.connect('allocate',
                           Lang.bind(this, this._allocate));
        this.actor._delegate = this;

        /** The container's child (like a `St.Bin`); do **not** assign to it,
         * but rather use {@link #setChild}.
         * @type {Clutter.Actor}
         */
        this.child = null;
        this._childScale = 1;
        this._childOpacity = 255;
        this.animatingOut = false;
    },

    /** callback for 'allocate' signal of {@link #actor}.
     * It centres the child in the container, allocating it its scaled size.
     */
    _allocate: function(actor, box, flags) {
        if (this.child == null)
            return;

        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;
        let [minChildWidth, minChildHeight, natChildWidth, natChildHeight] =
            this.child.get_preferred_size();
        let [childScaleX, childScaleY] = this.child.get_scale();

        let childWidth = Math.min(natChildWidth * childScaleX, availWidth);
        let childHeight = Math.min(natChildHeight * childScaleY, availHeight);

        let childBox = new Clutter.ActorBox();
        childBox.x1 = (availWidth - childWidth) / 2;
        childBox.y1 = (availHeight - childHeight) / 2;
        childBox.x2 = childBox.x1 + childWidth;
        childBox.y2 = childBox.y1 + childHeight;

        this.child.allocate(childBox, flags);
    },

    /** callback for the 'get-preferred-height' signal of
     * {@link #actor}.
     * Retrieves the child's current scaled height.
     */
    _getPreferredHeight: function(actor, forWidth, alloc) {
        alloc.min_size = 0;
        alloc.natural_size = 0;

        if (this.child == null)
            return;

        let [minHeight, natHeight] = this.child.get_preferred_height(forWidth);
        alloc.min_size += minHeight * this.child.scale_y;
        alloc.natural_size += natHeight * this.child.scale_y;
    },

    /** callback for the 'get-preferred-width' signal of
     * {@link #actor}.
     * Retrieves the child's current scaled width.
     */
    _getPreferredWidth: function(actor, forHeight, alloc) {
        alloc.min_size = 0;
        alloc.natural_size = 0;

        if (this.child == null)
            return;

        let [minWidth, natWidth] = this.child.get_preferred_width(forHeight);
        alloc.min_size = minWidth * this.child.scale_y;
        alloc.natural_size = natWidth * this.child.scale_y;
    },

    /** sets the child actor of the DashItemContainer.
     * @param {Clutter.Actor} actor - the actor to set as child (just has
     * to inherit Clutter.Actor - any St widget will do).
     */
    setChild: function(actor) {
        if (this.child == actor)
            return;

        this.actor.destroy_children();

        this.child = actor;
        this.actor.add_actor(this.child);
    },

    /** Makes the child fade in (opacity) and expand from a dot to its original
     * size over a time {@link DASH_ANIMATION_TIME}.
     *
     * Basically used in the Dash when items are added to it.
     */
    animateIn: function() {
        if (this.child == null)
            return;

        this.childScale = 0;
        this.childOpacity = 0;
        Tweener.addTween(this,
                         { childScale: 1.0,
                           childOpacity: 255,
                           time: DASH_ANIMATION_TIME,
                           transition: 'easeOutQuad'
                         });
    },

    /** Makes the child fade out (opacity) and shrink to a dot over a time
     * {@link DASH_ANIMATION_TIME}, and destroys the actor when finished.
     *
     * Basically used in the Dash when items are removed from it.
     */
    animateOutAndDestroy: function() {
        if (this.child == null) {
            this.actor.destroy();
            return;
        }

        this.animatingOut = true;
        this.childScale = 1.0;
        Tweener.addTween(this,
                         { childScale: 0.0,
                           childOpacity: 0,
                           time: DASH_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: Lang.bind(this, function() {
                               this.actor.destroy();
                           })
                         });
    },

    /** Sets the scale of the child actor. */
    set childScale(scale) {
        this._childScale = scale;

        if (this.child == null)
            return;

        this.child.set_scale_with_gravity(scale, scale,
                                          Clutter.Gravity.CENTER);
        this.actor.queue_relayout();
    },

    /** Gets the scale of the child actor. */
    get childScale() {
        return this._childScale;
    },

    /** Sets the opacity of the child actor */
    set childOpacity(opacity) {
        this._childOpacity = opacity;

        if (this.child == null)
            return;

        this.child.set_opacity(opacity);
        this.actor.queue_redraw();
    },

    /** Gets the opacity of the child actor */
    get childOpacity() {
        return this._childOpacity;
    }
};

/** creates a new RemoveFavoriteIcon
 * @classdesc This is the 'rubbish bin' icon that appears in the dash when the
 * user begins to drag an application favourite. If the user drops the app
 * onto the rubbish bin, the favourite is removed.
 *
 * ![the `RemoveFavoriteIcon` is at the bottom of the dash.](pics/removeFavoriteIcon.png)
 *
 * It extends {@link DashItemContainer}; the child actor is a
 * {@link IconGrid.BaseIcon}. It adds a drag and drop interface (for when
 * the user drags an app over the icon to remove it from their favourites).
 * @class
 * @extends DashItemContainer
 */
function RemoveFavoriteIcon() {
    this._init();
}

RemoveFavoriteIcon.prototype = {
    __proto__: DashItemContainer.prototype,

    _init: function() {
        DashItemContainer.prototype._init.call(this);

        this._iconBin = new St.Bin({ style_class: 'remove-favorite' });
        this._iconActor = null;
        /** the rubbish bin icon (set as the `DashItemContainer`s child). */
        this.icon = new IconGrid.BaseIcon(_("Remove"),
                                           { setSizeManually: true,
                                             showLabel: false,
                                             createIcon: Lang.bind(this, this._createIcon) });
        this._iconBin.set_child(this.icon.actor);
        this._iconBin._delegate = this;

        this.setChild(this._iconBin);
    },

    /** callback to create the rubbish bin icon. It's the stock 'user-trash'
     * icon with style class 'remove-favorite-icon' applied.
     * @param {number} size - the size of the icon to create.
     */
    _createIcon: function(size) {
        this._iconActor = new St.Icon({ icon_name: 'user-trash',
                                        style_class: 'remove-favorite-icon',
                                        icon_size: size });
        return this._iconActor;
    },

    /** adds or removes the hover pseudo style to the icon (i.e.
     *  the style class becomes 'remove-favorite-icon:hover').
     * @param {boolean} hovered - whether the hover style should be added (or
     * removed).
     */
    setHover: function(hovered) {
        this._iconBin.set_hover(hovered);
        if (this._iconActor)
            this._iconActor.set_hover(hovered);
    },

    // Rely on the dragged item being a favorite
    /** callback for drag and drop (see {@link DND}) when an item is
     * dragged over the rubbish bin icon.
     * 
     * This assumes the item being dragged is a app favourite and says that
     * the drop can be accepted.
     * @returns {DND.DragMotionResult} MOVE_DROP.
     */
    handleDragOver: function(source, actor, x, y, time) {
        return DND.DragMotionResult.MOVE_DROP;
    },

    /** callback for drag and drop (see {@link DND}) when an item is
     * actually dropped onto this.
     *
     * If the item being dropped is an {@link AppDisplay.AppWellIcon} (i.e.
     * an app icon representing an app favourite, see
     * {@link AppFavorite}), the app is removed from the favorites.
     *
     * If the item being dropped is a Window clone (one of those window
     * thumbnails in the overview), the app that that
     * window is for is removed from the favorites.
     *
     * @see AppFavorite.AppFavorites#removeFavorite
     */
    acceptDrop: function(source, actor, x, y, time) {
        let app = null;
        if (source instanceof AppDisplay.AppWellIcon) {
            let appSystem = Shell.AppSystem.get_default();
            app = appSystem.lookup_app(source.getId());
        } else if (source.metaWindow) {
            let tracker = Shell.WindowTracker.get_default();
            app = tracker.get_window_app(source.metaWindow);
        }

        let id = app.get_id();

        Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this,
            function () {
                AppFavorites.getAppFavorites().removeFavorite(id);
                return false;
            }));

        return true;
    }
};


/** creates a new DragPlaceholderItem
 * @classdesc This is the white horizontal bar (with dot in the middle) that
 * appears when you attempt to drag an item in the dash and drop it elsewhere
 * on the dash.
 *
 * ![The `DragPlaceholderItem` in the Dash](pics/dragPlaceholderItem.png)
 *
 * It extends {@link DashItemContainer}; its actor/child is essentially a
 * `St.Bin` displaying the 'dash-placeholder.svg' image in
 * `/usr/share/gnome-shell/theme` (this is all defined using style class
 * 'dash-placeholder').
 *
 * Other than that it does not add any of its own functionality beyond its
 * superclass.
 * @extends DashItemContainer
 * @class
 */
function DragPlaceholderItem() {
    this._init();
}

DragPlaceholderItem.prototype = {
    __proto__: DashItemContainer.prototype,

    _init: function() {
        DashItemContainer.prototype._init.call(this);
        this.setChild(new St.Bin({ style_class: 'dash-placeholder' }));
    }
};


/** creates a new Dash
 * @classdesc
 * The Dash is the left-hand sidebar in the overview.
 *
 * It displays icons for the currently-running apps, as well as for all the
 * user's app favourites.
 *
 * It supports a drag and drop interface for when the user reorders their
 * favourites or adds/removes them.
 *
 * Each item in the dash is a subclass of {@link DashItemContainer}.
 *
 * ![the Dash](pics/dash.png)
 *
 * Its actor is a `St.Bin` containing a vertical `St.BoxLayout` to which
 * the items are all added.
 * @class
 */
function Dash() {
    this._init();
}

Dash.prototype = {
    _init : function() {
        this._maxHeight = -1;
        this.iconSize = 64;
        this._shownInitially = false;

        /** 
         * The drag placeholder item (will be added/removed as the user
         * drags things around the dash )
         * @type {Dash.DragPlaceholder}
         */
        this._dragPlaceholder = null;
        this._dragPlaceholderPos = -1;
        this._animatingPlaceholdersCount = 0;
        this._favRemoveTarget = null;

        this._box = new St.BoxLayout({ name: 'dash',
                                       vertical: true,
                                       clip_to_allocation: true });
        this._box._delegate = this;

        this.actor = new St.Bin({ y_align: St.Align.START, child: this._box });
        this.actor.connect('notify::height', Lang.bind(this,
            function() {
                if (this._maxHeight != this.actor.height)
                    this._queueRedisplay();
                this._maxHeight = this.actor.height;
            }));

        this._workId = Main.initializeDeferredWork(this._box, Lang.bind(this, this._redisplay));

        this._tracker = Shell.WindowTracker.get_default();
        this._appSystem = Shell.AppSystem.get_default();

        this._appSystem.connect('installed-changed', Lang.bind(this, this._queueRedisplay));
        AppFavorites.getAppFavorites().connect('changed', Lang.bind(this, this._queueRedisplay));
        this._appSystem.connect('app-state-changed', Lang.bind(this, this._queueRedisplay));

        Main.overview.connect('item-drag-begin',
                              Lang.bind(this, this._onDragBegin));
        Main.overview.connect('item-drag-end',
                              Lang.bind(this, this._onDragEnd));
        Main.overview.connect('item-drag-cancelled',
                              Lang.bind(this, this._onDragCancelled));
        Main.overview.connect('window-drag-begin',
                              Lang.bind(this, this._onDragBegin));
        Main.overview.connect('window-drag-cancelled',
                              Lang.bind(this, this._onDragCancelled));
        Main.overview.connect('window-drag-end',
                              Lang.bind(this, this._onDragEnd));
    },

    /** callback when the overview emits 'item-drag-begin' or
     * 'window-drag-begin' (ie the user starts dragging some item from the
     * overview such as a window thumbnail)
     *
     * This adds a monitor for the drag (see {@link #_onDragMotion} and 
     * {@link DND.addDragMonitor} and {@link DND.DragMonitor}).
     */
    _onDragBegin: function() {
        this._dragCancelled = false;
        this._dragMonitor = {
            dragMotion: Lang.bind(this, this._onDragMotion)
        };
        DND.addDragMonitor(this._dragMonitor);
    },

    /** callback when the user cancels a drag (overview fires the
     * 'item-drag-cancelled' or 'window-drag-cancelled' signals).
     *
     * Calls {@link #_onDragEnd}.
     */
    _onDragCancelled: function() {
        this._dragCancelled = true;
        this._endDrag();
    },

    /** callback when the user finishes a drag (overview fires the
     * 'item-drag-end' or 'window-drag-end' signals).
     *
     * If the drag was not cancelled, this calls {@link #_endDrag}.
     */
    _onDragEnd: function() {
        if (this._dragCancelled)
            return;

        this._endDrag();
    },

    /** Finishes a drag operation. This:
     *
     * - removes the drag placeholder, if any (see
     *   {@link DragPlaceholderItem})
     * - if the user removed a favourite, it fades out the icon for that
     *   favourite
     * - removes the drag monitor added in {@link #_onDragBegin}.
     */
    _endDrag: function() {
        this._clearDragPlaceholder();
        if (this._favRemoveTarget) {
            this._favRemoveTarget.animateOutAndDestroy();
            this._favRemoveTarget.actor.connect('destroy', Lang.bind(this,
                function() {
                    this._favRemoveTarget = null;
                }));
            this._adjustIconSize();
        }
        DND.removeDragMonitor(this._dragMonitor);
    },

    /** Callback when the user is dragging (see {@link DND.addDragMonitor}).
     *
     * If the item being dragged is *not* a {@link AppDisplay.AppWellicon] or
     * a window clone (the window thumbnails in the overview), the drag
     * is continued immediately.
     *
     * Otherwise if the app (corresponding to the icon or to the window being
     * dragged) is an app favourite, the {@link RemoveFavoriteIcon} is shown
     * (so that the user may drop the app onto that if they wish to remove
     * the app from the favourites).
     *
     * If the app/window is currently being held *over* the remove favourite
     * icon, the dash calls {@link RemoveFavoriteIcon#setHover} to let the icon
     * know it should be displaying its 'hover' style.
     *
     * If the app/window is over the remove favourite icon or is *not* being
     * held over the dash, the drag placeholder (if any) is removed.
     *
     * Finally, `DND.DragMotionResult.CONTINUE` is returned.
     *
     * Note that this is different to the code that lets the dash actually
     * handle drops as a drag target (although I don't know how) -- see
     * {@link #handleDragOver} and {@link #acceptDrop}.
     *
     * @param {DND.DragEvent} dragEvent - object describing the current drag
     * position and so on.
     * @returns {DND.DragMotionResult} DND.DragMotionResult.CONTINUE
     */
    _onDragMotion: function(dragEvent) {
        let app = null;
        if (dragEvent.source instanceof AppDisplay.AppWellIcon)
            app = this._appSystem.lookup_app(dragEvent.source.getId());
        else if (dragEvent.source.metaWindow)
            app = this._tracker.get_window_app(dragEvent.source.metaWindow);
        else
            return DND.DragMotionResult.CONTINUE;

        let id = app.get_id();

        let favorites = AppFavorites.getAppFavorites().getFavoriteMap();

        let srcIsFavorite = (id in favorites);

        if (srcIsFavorite &&
            dragEvent.source.actor &&
            this.actor.contains (dragEvent.source.actor) &&
            this._favRemoveTarget == null) {
                this._favRemoveTarget = new RemoveFavoriteIcon();
                this._favRemoveTarget.icon.setIconSize(this.iconSize);
                this._box.add(this._favRemoveTarget.actor);
                this._adjustIconSize();
                this._favRemoveTarget.animateIn();
        }

        let favRemoveHovered = false;
        if (this._favRemoveTarget)
            favRemoveHovered =
                this._favRemoveTarget.actor.contains(dragEvent.targetActor);

        if (!this._box.contains(dragEvent.targetActor) || favRemoveHovered)
            this._clearDragPlaceholder();

        if (this._favRemoveTarget)
            this._favRemoveTarget.setHover(favRemoveHovered);

        return DND.DragMotionResult.CONTINUE;
    },

    /** creates a hash mapping app ID to app.
     * @param {Shell.App[]} apps - an array of Shell.Apps to make a hash for
     * @returns {Object<string, Shell.App>} and object mapping app IDs to
     * their Shell.App.
     */
    _appIdListToHash: function(apps) {
        let ids = {};
        for (let i = 0; i < apps.length; i++)
            ids[apps[i].get_id()] = apps[i];
        return ids;
    },

    /** queues a redisplay of the dash using {@link Main.queueDeferredWork}
     * (this is to avoid using up memory doing the redisplay if the dash
     * isn't visible).
     */
    _queueRedisplay: function () {
        Main.queueDeferredWork(this._workId);
    },

    /** creates a {@link DashItemContainer} for an app in the dash.
     *
     * That is, it creates a {@link DashItemContainer} displaying an
     * {@link AppDisplay.AppWellIcon} to put into the dash.
     *
     * It also specifies that when the user starts dragging the icon,
     * the icon (in the dash, not the one being dragged) should become
     * semi-transparent (see how the top terminal icon in the picture
     * above is semi-transparent because the terminal is being dragged).
     * @param {Shell.App} app - app to create an item for
     * @returns {Dash.DashItemContainer} a DashItemContainer containing
     * an {@link AppDisplay.AppWellicon} for the app (no label).
     */
    _createAppItem: function(app) {
        let display = new AppDisplay.AppWellIcon(app,
                                                 { setSizeManually: true,
                                                   showLabel: false });
        display._draggable.connect('drag-begin',
                                   Lang.bind(this, function() {
                                       display.actor.opacity = 50;
                                   }));
        display._draggable.connect('drag-end',
                                   Lang.bind(this, function() {
                                       display.actor.opacity = 255;
                                   }));
        display.actor.set_tooltip_text(app.get_name());

        let item = new DashItemContainer();
        item.setChild(display.actor);

        display.icon.setIconSize(this.iconSize);

        return item;
    },

    /** Adjusts the icon size of items in the dash.
     * As far as I can tell, this ensures that the size of all items in the
     * dash (except for the drag placeholder or icons that are currently
     * animating out) is the same, and is the largest possible out of
     * a specified set of sizes that will still fit vertically in the dash.
     *
     * The specified set of sizes is (defined locally in the function):
     *
     *     let iconSizes = [ 16, 22, 24, 32, 48, 64 ];
     *
     */
    _adjustIconSize: function() {
        // For the icon size, we only consider children which are "proper"
        // icons (i.e. ignoring drag placeholders) and which are not
        // animating out (which means they will be destroyed at the end of
        // the animation)
        let iconChildren = this._box.get_children().filter(function(actor) {
            return actor._delegate.child &&
                   actor._delegate.child._delegate &&
                   actor._delegate.child._delegate.icon &&
                   !actor._delegate.animatingOut;
        });

        if (iconChildren.length == 0) {
            this._box.add_style_pseudo_class('empty');
            return;
        }

        this._box.remove_style_pseudo_class('empty');

        if (this._maxHeight == -1)
            return;


        let themeNode = this.actor.get_theme_node();
        let maxAllocation = new Clutter.ActorBox({ x1: 0, y1: 0,
                                                   x2: 42 /* whatever */,
                                                   y2: this._maxHeight });
        let maxContent = themeNode.get_content_box(maxAllocation);
        let availHeight = maxContent.y2 - maxContent.y1;
        let spacing = themeNode.get_length('spacing');


        let firstIcon = iconChildren[0]._delegate.child._delegate.icon;

        let minHeight, natHeight;

        // Enforce the current icon size during the size request if
        // the icon is animating
        if (firstIcon._animating) {
            let [currentWidth, currentHeight] = firstIcon.icon.get_size();

            firstIcon.icon.set_size(this.iconSize, this.iconSize);
            [minHeight, natHeight] = iconChildren[0].get_preferred_height(-1);

            firstIcon.icon.set_size(currentWidth, currentHeight);
        } else {
            [minHeight, natHeight] = iconChildren[0].get_preferred_height(-1);
        }


        // Subtract icon padding and box spacing from the available height
        availHeight -= iconChildren.length * (natHeight - this.iconSize) +
                       (iconChildren.length - 1) * spacing;

        let availSize = availHeight / iconChildren.length;

        let iconSizes = [ 16, 22, 24, 32, 48, 64 ];

        let newIconSize = 16;
        for (let i = 0; i < iconSizes.length; i++) {
            if (iconSizes[i] < availSize)
                newIconSize = iconSizes[i];
        }

        if (newIconSize == this.iconSize)
            return;

        let oldIconSize = this.iconSize;
        this.iconSize = newIconSize;
        this.emit('icon-size-changed');

        let scale = oldIconSize / newIconSize;
        for (let i = 0; i < iconChildren.length; i++) {
            let icon = iconChildren[i]._delegate.child._delegate.icon;

            // Set the new size immediately, to keep the icons' sizes
            // in sync with this.iconSize
            icon.setIconSize(this.iconSize);

            // Don't animate the icon size change when the overview
            // is not visible or when initially filling the dash
            if (!Main.overview.visible || !this._shownInitially)
                continue;

            let [targetWidth, targetHeight] = icon.icon.get_size();

            // Scale the icon's texture to the previous size and
            // tween to the new size
            icon.icon.set_size(icon.icon.width * scale,
                               icon.icon.height * scale);

            icon._animating = true;
            Tweener.addTween(icon.icon,
                             { width: targetWidth,
                               height: targetHeight,
                               time: DASH_ANIMATION_TIME,
                               transition: 'easeOutQuad',
                               onComplete: function() {
                                   icon._animating = false;
                               }
                             });
        }
    },

    /** Updates the dash whenever things change. This includes apps closing
     * (remove their icon), apps starting (add their icon), favourites
     * being edited, or items in the dash being moved around.
     */
    _redisplay: function () {
        let favorites = AppFavorites.getAppFavorites().getFavoriteMap();

        let running = this._appSystem.get_running();

        let children = this._box.get_children().filter(function(actor) {
                return actor._delegate.child &&
                       actor._delegate.child._delegate &&
                       actor._delegate.child._delegate.app;
            });
        // Apps currently in the dash
        let oldApps = children.map(function(actor) {
                return actor._delegate.child._delegate.app;
            });
        // Apps supposed to be in the dash
        let newApps = [];

        for (let id in favorites)
            newApps.push(favorites[id]);

        for (let i = 0; i < running.length; i++) {
            let app = running[i];
            if (app.get_id() in favorites)
                continue;
            newApps.push(app);
        }

        // Figure out the actual changes to the list of items; we iterate
        // over both the list of items currently in the dash and the list
        // of items expected there, and collect additions and removals.
        // Moves are both an addition and a removal, where the order of
        // the operations depends on whether we encounter the position
        // where the item has been added first or the one from where it
        // was removed.
        // There is an assumption that only one item is moved at a given
        // time; when moving several items at once, everything will still
        // end up at the right position, but there might be additional
        // additions/removals (e.g. it might remove all the launchers
        // and add them back in the new order even if a smaller set of
        // additions and removals is possible).
        // If above assumptions turns out to be a problem, we might need
        // to use a more sophisticated algorithm, e.g. Longest Common
        // Subsequence as used by diff.
        let addedItems = [];
        let removedActors = [];

        let newIndex = 0;
        let oldIndex = 0;
        while (newIndex < newApps.length || oldIndex < oldApps.length) {
            // No change at oldIndex/newIndex
            if (oldApps[oldIndex] == newApps[newIndex]) {
                oldIndex++;
                newIndex++;
                continue;
            }

            // App removed at oldIndex
            if (oldApps[oldIndex] &&
                newApps.indexOf(oldApps[oldIndex]) == -1) {
                removedActors.push(children[oldIndex]);
                oldIndex++;
                continue;
            }

            // App added at newIndex
            if (newApps[newIndex] &&
                oldApps.indexOf(newApps[newIndex]) == -1) {
                addedItems.push({ app: newApps[newIndex],
                                  item: this._createAppItem(newApps[newIndex]),
                                  pos: newIndex });
                newIndex++;
                continue;
            }

            // App moved
            let insertHere = newApps[newIndex + 1] &&
                             newApps[newIndex + 1] == oldApps[oldIndex];
            let alreadyRemoved = removedActors.reduce(function(result, actor) {
                let removedApp = actor._delegate.child._delegate.app;
                return result || removedApp == newApps[newIndex];
            }, false);

            if (insertHere || alreadyRemoved) {
                let newItem = this._createAppItem(newApps[newIndex]);
                addedItems.push({ app: newApps[newIndex],
                                  item: newItem,
                                  pos: newIndex + removedActors.length });
                newIndex++;
            } else {
                removedActors.push(children[oldIndex]);
                oldIndex++;
            }
        }

        for (let i = 0; i < addedItems.length; i++)
            this._box.insert_actor(addedItems[i].item.actor,
                                   addedItems[i].pos);

        for (let i = 0; i < removedActors.length; i++) {
            let item = removedActors[i]._delegate;

            // Don't animate item removal when the overview is hidden
            if (Main.overview.visible)
                item.animateOutAndDestroy();
            else
                item.actor.destroy();
        }

        this._adjustIconSize();

        // Skip animations on first run when adding the initial set
        // of items, to avoid all items zooming in at once
        if (!this._shownInitially) {
            this._shownInitially = true;
            return;
        }

        // Don't animate item addition when the overview is hidden
        if (!Main.overview.visible)
            return;

        for (let i = 0; i < addedItems.length; i++)
            addedItems[i].item.animateIn();
    },

    /** Removes the drag placeholder from the dash - animates it out. */
    _clearDragPlaceholder: function() {
        if (this._dragPlaceholder) {
            this._dragPlaceholder.animateOutAndDestroy();
            this._dragPlaceholder = null;
            this._dragPlaceholderPos = -1;
        }
    },

    /** Function indicating that the Dash is an item that can be dragged over
     * this function handles the drag over (see {@link DND}).
     *
     * I am UNSURE as to how this overlaps/doesn't with
     * {@link #_onDragMotion}, being a drag monitor callback.
     *
     * If the item being dragged is *neither* an {@link AppDisplay.AppWellicon}
     * or a window clone (e.g. the window thumbnails in the overview), OR
     * the item is one of these but its app is transient, then 
     * `DND.DragMotionResult.NO_DROP` is returned (the user cannot drop
     * whatever they are dragging onto the dash - it won't handle it).
     *
     * Otherwise, the drag placeholder ({@link #_dragPlaceholder}) is
     * created and moved to the appropriate position.
     *
     * If the app is already a favourite and hence is being moved,
     * `DND.DragMotionResult.MOVE_DROP` is returned.
     *
     * Otherwise (the app is being added to the favourites),
     * `DND.DragMotionResult.COPY_DROP` is returned.
     *
     * @returns {DND.DragMotionResult}  `NO_DROP` if the item being dragged
     * doesn't have an associated non-transient app; `MOVE_DROP` if the item
     * is already a favourite and is being moved; `COPY_DROP` if the item is
     * being added to the favourites.
     */
    handleDragOver : function(source, actor, x, y, time) {
        let app = null;
        if (source instanceof AppDisplay.AppWellIcon)
            app = this._appSystem.lookup_app(source.getId());
        else if (source.metaWindow)
            app = this._tracker.get_window_app(source.metaWindow);

        // Don't allow favoriting of transient apps
        if (app == null || app.is_window_backed())
            return DND.DragMotionResult.NO_DROP;

        let favorites = AppFavorites.getAppFavorites().getFavorites();
        let numFavorites = favorites.length;

        let favPos = favorites.indexOf(app);

        let children = this._box.get_children();
        let numChildren = children.length;
        let boxHeight = this._box.height;

        // Keep the placeholder out of the index calculation; assuming that
        // the remove target has the same size as "normal" items, we don't
        // need to do the same adjustment there.
        if (this._dragPlaceholder) {
            boxHeight -= this._dragPlaceholder.actor.height;
            numChildren--;
        }

        let pos = Math.round(y * numChildren / boxHeight);

        if (pos != this._dragPlaceholderPos && pos <= numFavorites) {
            if (this._animatingPlaceholdersCount > 0) {
                let appChildren = children.filter(function(actor) {
                    return actor._delegate &&
                           actor._delegate.child &&
                           actor._delegate.child._delegate &&
                           actor._delegate.child._delegate.app;
                });
                this._dragPlaceholderPos = children.indexOf(appChildren[pos]);
            } else {
                this._dragPlaceholderPos = pos;
            }

            // Don't allow positioning before or after self
            if (favPos != -1 && (pos == favPos || pos == favPos + 1)) {
                if (this._dragPlaceholder) {
                    this._dragPlaceholder.animateOutAndDestroy();
                    this._animatingPlaceholdersCount++;
                    this._dragPlaceholder.actor.connect('destroy',
                        Lang.bind(this, function() {
                            this._animatingPlaceholdersCount--;
                        }));
                }
                this._dragPlaceholder = null;

                return DND.DragMotionResult.CONTINUE;
            }

            // If the placeholder already exists, we just move
            // it, but if we are adding it, expand its size in
            // an animation
            let fadeIn;
            if (this._dragPlaceholder) {
                this._dragPlaceholder.actor.destroy();
                fadeIn = false;
            } else {
                fadeIn = true;
            }

            this._dragPlaceholder = new DragPlaceholderItem();
            this._dragPlaceholder.child.set_width (this.iconSize);
            this._dragPlaceholder.child.set_height (this.iconSize / 2);
            this._box.insert_actor(this._dragPlaceholder.actor,
                                   this._dragPlaceholderPos);
            if (fadeIn)
                this._dragPlaceholder.animateIn();
        }

        let srcIsFavorite = (favPos != -1);

        if (srcIsFavorite)
            return DND.DragMotionResult.MOVE_DROP;

        return DND.DragMotionResult.COPY_DROP;
    },

    /** Draggable target interface - lets the dash accept a drop of another
     * gnome-shell object. Called when the user drops an object on the dash.
     *
     * If the item being dropped has an associated application (ie is an
     * {@link AppDisplay.AppWellicon} or is a window thumbnail) and that
     * application can be favourited (is not transient), then the app is
     * added to the user's favourites (see 
     * {@link AppFavorite.AppFavorites#moveFavoriteToPos} and
     * {@link AppFavorite.AppFavorites#addFavoriteAtPos)).
     *
     * It is added at the position of the drag placeholder.
     *
     * @returns {boolean} false if the item being dragged is not valid to be
     * dropped onto the dash, true otherwise (the drop was handled successfully).
     */
    acceptDrop : function(source, actor, x, y, time) {
        let app = null;
        if (source instanceof AppDisplay.AppWellIcon) {
            app = this._appSystem.lookup_app(source.getId());
        } else if (source.metaWindow) {
            app = this._tracker.get_window_app(source.metaWindow);
        }

        // Don't allow favoriting of transient apps
        if (app == null || app.is_window_backed()) {
            return false;
        }

        let id = app.get_id();

        let favorites = AppFavorites.getAppFavorites().getFavoriteMap();

        let srcIsFavorite = (id in favorites);

        let favPos = 0;
        let children = this._box.get_children();
        for (let i = 0; i < this._dragPlaceholderPos; i++) {
            if (this._dragPlaceholder &&
                children[i] == this._dragPlaceholder.actor)
                continue;

            let childId = children[i]._delegate.child._delegate.app.get_id();
            if (childId == id)
                continue;
            if (childId in favorites)
                favPos++;
        }

        Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this,
            function () {
                let appFavorites = AppFavorites.getAppFavorites();
                if (srcIsFavorite)
                    appFavorites.moveFavoriteToPos(id, favPos);
                else
                    appFavorites.addFavoriteAtPos(id, favPos);
                return false;
            }));

        return true;
    }
};

Signals.addSignalMethods(Dash.prototype);
