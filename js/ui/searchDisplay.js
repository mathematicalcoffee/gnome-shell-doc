// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Classes for *displaying* the results of a search in the overview as opposed
 * to files that do the the searching (which are in {@link namespace:Search}).
 *
 * Overall - A {@link Search.SearchSystem} (containing many 
 * {@link Search.SearchProvider}s)
 * is a *logical* class, handling all the search logic.
 * The {@link SearchResults} (containing many {@link GridSearchResults}, one per
 * provider) are the UI classes, handling the *displaying* of search results.
 *
 * @see namespace:PlaceDisplay
 * @see namespace:AppDisplay
 * @see namespace:DocDisplay
 * @see namespace:ContactDisplay
 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Meta = imports.gi.Meta;
const St = imports.gi.St;

const DND = imports.ui.dnd;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const Overview = imports.ui.overview;
const Search = imports.ui.search;

/** Default row limit for the {@link IconGrid.IconGrid} in the
 * {@link GridSearchResults}.
 * @const
 * @default
 * @type {number} */
const MAX_SEARCH_RESULTS_ROWS = 1;

/** Creates a new SearchResult.
 *
 * The top-level actor is a `St.Button` with style class 'search-result'.
 *
 * If `provider` has implemented
 * {@link Search.SearchProvider#createResultActor}, this is called, stored
 * in {@link #_content}, and displayed in the
 * button.
 *
 * Otherwise, a {@link IconGrid.BaseIcon} for the result
 * (populated from the provided `metaInfo`) is created, stored in
 * {@link #_content}, and nested into the
 * button.
 *
 * The actor {@link #actor} is made draggable, with its drag actor
 * source (i.e. what is considered to be the 'source' actor when the user
 * starts to drag) being either:
 *
 * * `this._content._delegate.getDragActorSource()`, if this exists; or
 * * the `BaseIcon`'s icon, if `provider.createResultActor` was not implemented; or
 * * {@link #_content}, if all else fails.
 *
 * The 'drag-begin', 'drag-cancelled' and 'drag-end' events for the draggable
 * are connected up to {@link Overview.Overview#beginItemDrag},
 * {@link Overview.Overview#cancelledItemDrag}, and
 * {@link Overview.Overview#endItemDrag}.
 *
 * @param {Search.SearchProvider} provider
 * @param {Search.ResultMeta} metaInfo - metadata for this result (needs
 * `id`, `name`, and `createIcon` properties).
 * @param {string[]} terms - Array of search terms, should be used for
 * highlighting
 * @classdesc
 * ![SearchResult with the 'selected' style (left) and the 'hover' style (right)](pics/SearchResultSelectedHover.png)
 * This represents a single result of a search in the Overview. 
 * If the provider that produced this result has implemented
 * {@link Search.SearchProvider#createResultActor}, this is displayed.
 * Otherwise, a `St.Button` containing a {@link IconGrid.BaseIcon} for the result
 * (populated from the provided {@link Search.ResultMeta}) is created.
 *
 * (i.e. most of the icons for the results in the overview are these).
 *
 * This class implements the 
 * @todo how to mark as implementing the drag interface?
 * @implements DND.Draggable
 * @todo I'm sure I had a pic somewhere.
 * @class
 */
function SearchResult(provider, metaInfo, terms) {
    this._init(provider, metaInfo, terms);
}

SearchResult.prototype = {
    _init: function(provider, metaInfo, terms) {
        /** The search provider that produced this result (used to call
         * {@link Search.SearchProvider#createResultActor} to generate the UI
         * part of the search result).
         * {@type Search.SearchProvider} */
        this.provider = provider;
        /** Metadata for this search result.
         * @type {Search.ResultMeta} */
        this.metaInfo = metaInfo;
        /** Top-level actor for the SearchResult. This is a `St.Button` with
         * style class 'search-result'. Inside it will display either
         * [`this.provider.createResultActor(..)`]{@link Search.SearchProvider#createResultActor}
         * (if implemented by the provider), or a {@link IconGrid.BaseIcon}
         * from [`this.metaInfo`]{@link #metaInfo}.
         * @type {St.Button} */
        this.actor = new St.Button({ style_class: 'search-result',
                                     reactive: true,
                                     x_align: St.Align.START,
                                     y_fill: true });
        this.actor._delegate = this;
        /** This is what is returned from {@link #getDragActorSource}.
         * If the provider has implemented `createResultActor` and its delegate
         * has method `getDragActorSource`, this is what `this._dragActorSource`
         * is. Otherwise (a BaseIcon was created for the result), it is the
         * icon.
         *
         * When the user starts dragging, this is what is shown in the original
         * position (where the user started dragging from).
         * @type {?Clutter.Actor} */
        this._dragActorSource = null;

        let content = provider.createResultActor(metaInfo, terms);
        if (content == null) {
            content = new St.Bin({ style_class: 'search-result-content',
                                   reactive: true,
                                   track_hover: true });
            let icon = new IconGrid.BaseIcon(this.metaInfo['name'],
                                             { createIcon: this.metaInfo['createIcon'] });
            content.set_child(icon.actor);
            this._dragActorSource = icon.icon;
            this.actor.label_actor = icon.label;
        } else {
            if (content._delegate && content._delegate.getDragActorSource)
                this._dragActorSource = content._delegate.getDragActorSource();
        }
        /** What gets stored in {@link #actor}. If
         * `this.provider.createResultActor` exists, this will
         * be the result of calling that. Otherwise, it will be a `St.Bin`
         * with style class 'search-result-content' containing a
         * {@link IconGrid.BaseIcon} generated from {@link #metaInfo}.
         * @type {St.Bin|St.Widget}
         */
        this._content = content;
        this.actor.set_child(content);

        this.actor.connect('clicked', Lang.bind(this, this._onResultClicked));

        let draggable = DND.makeDraggable(this.actor);
        draggable.connect('drag-begin',
                          Lang.bind(this, function() {
                              Main.overview.beginItemDrag(this);
                          }));
        draggable.connect('drag-cancelled',
                          Lang.bind(this, function() {
                              Main.overview.cancelledItemDrag(this);
                          }));
        draggable.connect('drag-end',
                          Lang.bind(this, function() {
                              Main.overview.endItemDrag(this);
                          }));
    },

    /** Selects or unselects this item (visually) - adds or removes the
     * pseudo style class 'selected' to {@link #_content}.
     *
     * Note that 'selected' means you navigate to the result with the keyboard,
     * not the same as 'hover' and not the same as 'activate'.
     *
     * ![SearchResult with the 'selected' style (left) and the 'hover' style (right)](pics/SearchResultSelectedHover.png)
     * @param {boolean} selected - whether to select or unselect the item.
     */
    setSelected: function(selected) {
        if (selected)
            this._content.add_style_pseudo_class('selected');
        else
            this._content.remove_style_pseudo_class('selected');
    },

    /** Activates the search result and toggles the overview.
     * This is outsourced to the {@link #provider} to activate this
     * result.
     * @see Search.SearchProvider#activateResult
     */
    activate: function() {
        this.provider.activateResult(this.metaInfo.id);
        Main.overview.toggle();
    },

    /** Callback when the user clicks on the SearchResult. It just activates
     * it.
     * @see #activate
     */
    _onResultClicked: function(actor) {
        this.activate();
    },

    /** Provides the 'getDragActorSource' function for the Draggable interface.
     * Returns either {@link #_dragActorSource}, or if that is null,
     * {@link #_content}.
     * 
     * This is what remains in the original item's position when the user starts
     * dragging.
     * @see DND.Draggable */
    getDragActorSource: function() {
        if (this._dragActorSource)
            return this._dragActorSource;
        // not exactly right, but alignment problems are hard to notice
        return this._content;
    },

    /** Provides the 'getDragActor' function for the Draggable interface.
     *
     * This is {@link #metaInfo}'s `createIcon` member (a function),
     * used to make an icon of size {@link Overview.Overview.dashIconSize}.
     *
     * This is the actor that is being dragged when the user starts a drag
     * (i.e. the actor that appears under the mouse as we are dragging).
     */
    getDragActor: function(stageX, stageY) {
        return this.metaInfo['createIcon'](Main.overview.dashIconSize);
    },

    /** This function is used by {@link Workspace.Workspace} and
     * {@link WorkspaceThumbnail.WorkspaceThumbnail} to work out what to do
     * when the search result is dragged and dropped over one of them (i.e.
     * in the windows tab of the overview or over a workspace thumbnail).
     *
     * If {@link #provider} has a `dragActivateResult` function
     * (what to do if the result is activated as the result of a drag), this
     * is called. Otherwise the {@link Search.SearchProvider#activateResult} is
     * called to activate the result.
     *
     * (At the moment only {@link AppDisplay.AppSearchProvider} has a
     * `dragActivateResult` function; it is used to launch an app on a
     * particular workspace when the app is dragged and dropped on that
     * workspace).
     * @param {Object} params - parameters for the activation
     * @param {number} workspace - the workspace to launch the result on (0-based).
     * -1 for the current active workspace.
     * @param {number} timestamp - the time of the request.
     * @see AppDisplay.AppSearchProvider#dragActivateResult
     */
    shellWorkspaceLaunch: function(params) {
        if (this.provider.dragActivateResult)
            this.provider.dragActivateResult(this.metaInfo.id, params);
        else
            this.provider.activateResult(this.metaInfo.id, params);
    }
};

/** Creates a new GridSearchResults
 * @param {Search.SearchProvider} provider - the search provider for the
 * results.
 * @param {IconGrid.IconGrid} [grid] - the icon grid to show the results in.
 * If not provided, a new one will be made with no column limit and
 * row limit {@link MAX_SEARCH_RESULTS_ROWS} (1).
 * @see Search.SearchProvider
 * @see IconGrid.IconGrid
 *
 * @classdesc
 * A GridSearchResults is a grid of {@link SearchResult}s.
 *
 * It is used by a {@link SearchResults} to render many search results.
 * Most of the `*Display` classes (i.e. showing search results for
 * apps, places, etc) use one of these in their {@link SearchResults}.
 *
 * @class
 * @extends Search.SearchResultDisplay
 */
function GridSearchResults(provider, grid) {
    this._init(provider, grid);
}

GridSearchResults.prototype = {
    __proto__: Search.SearchResultDisplay.prototype,

    _init: function(provider, grid) {
        Search.SearchResultDisplay.prototype._init.call(this, provider);
        /** The IconGrid that will be used to display all the results.
         * If it wasn't provided in the constructor, it will be created for you
         * and will have a row limit of {@link MAX_SEARCH_RESULTS_ROWS} (1) and
         * unlimited column limit.
         * @type {IconGrid.IconGrid} */
        this._grid = grid || new IconGrid.IconGrid({ rowLimit: MAX_SEARCH_RESULTS_ROWS,
                                                     xAlign: St.Align.START });
        /** Top-level actor for the GridSearchResults. It has the grid
         * {@link #_grid} embedded in it.
         * @type {St.Bin} */
        this.actor = new St.Bin({ x_align: St.Align.START });

        this.actor.set_child(this._grid.actor);
        /** Index of the currently-selected item, or -1 if none.
         * @type {number} */
        this.selectionIndex = -1;
        /** The width of the top-level actor. It's cached here so that
         * {@link #_tryAddResults} can work out how many
         * results will fit horizontally in the grid.
         * @type {number} */
        this._width = 0;
        this.actor.connect('notify::width', Lang.bind(this, function() {
            this._width = this.actor.width;
            Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this, function() {
                this._tryAddResults();
            }));
        }));
        /** Array of result (IDs) that are not currently being displayed
         * (because they don't fit on the screen).
         * @type {string[]}
         */
        this._notDisplayedResult = [];
        /** Current search terms.
         * @type {string[]} */
        this._terms = [];
    },

    /** Trys to add results that aren't currently being displayed into the grid,
     * stopping when no more can be fit in.
     *
     * It works out the maximum number of items that will fit in its width
     * (see {@link IconGrid.IconGrid#childrenInRow}) and fills up to that number
     * from {@link #_notDisplayedResult}).
     *
     * It does this by using
     * [`this.provider.getResultMeta`]{@link Search.SearchProvider#getResultMeta}
     * to get the {@link Search.ResultMeta} for a result ID in
     * {@link #_notDisplayedResult}, and uses the resulting
     * metadata to create a {@link SearchResult} for the result.
     *
     * It then adds the the actor to the grid {@link #_grid}.
     *
     * @see Search.SearchProvider#getResultMeta
     * @see SearchResult
     */
    _tryAddResults: function() {
        let canDisplay = this._grid.childrenInRow(this._width) * MAX_SEARCH_RESULTS_ROWS
                         - this._grid.visibleItemsCount();

        for (let i = Math.min(this._notDisplayedResult.length, canDisplay); i > 0; i--) {
            let result = this._notDisplayedResult.shift();
            let meta = this.provider.getResultMeta(result);
            let display = new SearchResult(this.provider, meta, this._terms);
            this._grid.addItem(display.actor);
        }
    },

    /** Wrapper around {@link IconGrid.IconGrid#visibleItemsCount}. Returns
     * the number of search results currently being displayed.
     *
     * Implements the parent function.
     *
     * @returns {number} the number of items in the grid that have their
     * `.visible` property set to TRUE.
     *
     * Note that this would include actors that were in the grid but not visible
     * due to not fitting on the screen but still had their `.visible` property
     * set to true, except that this class is careful not to add a result if it
     * will overflow off the screen.
     * @see IconGrid.IconGrid#visibleItemsCount
     * @override
     */
    getVisibleResultCount: function() {
        return this._grid.visibleItemsCount();
    },

    /** Adds the provided results to the grid, storing those that didn't
     * fit in {@link #_notDisplayedResult}.
     *
     * Implements the parent function.
     *
     * Called by {@link SearchResults#_updateResults} and
     * {@link SearchResults#_updateCurrentResults}.
     * @inheritparams SearchResult
     * @param {string[]} results - an array of result identifier strings to be
     * added to the grid.
     * @override
     */
    renderResults: function(results, terms) {
        // copy the lists
        this._notDisplayedResult = results.slice(0);
        this._terms = terms.slice(0);
        this._tryAddResults();
    },

    /** Clears all the search results from the grid.
     * Overrides the parent function.
     * @override */
    clear: function () {
        this._terms = [];
        this._notDisplayedResult = [];
        this._grid.removeAll();
        this.selectionIndex = -1;
    },

    /** Selects the result at the given index. By 'selects', this is selecting
     * as in navigating over that result with the keyboard as opposed to
     * activating the result.
     *
     * Implements the parent function.
     * @param {number} index - index of the result to select.
     * @see SearchResult#setSelected
     * @override
     */
    selectIndex: function (index) {
        let nVisible = this.getVisibleResultCount();
        if (this.selectionIndex >= 0) {
            let prevActor = this._grid.getItemAtIndex(this.selectionIndex);
            prevActor._delegate.setSelected(false);
        }
        this.selectionIndex = -1;
        if (index >= nVisible)
            return false;
        else if (index < 0)
            return false;
        let targetActor = this._grid.getItemAtIndex(index);
        targetActor._delegate.setSelected(true);
        this.selectionIndex = index;
        return true;
    },

    /** Activates the currently-selected results. Implements the parent function.
     * @see SearchResult#activate
     * @override
     */
    activateSelected: function() {
        if (this.selectionIndex < 0)
            return;
        let targetActor = this._grid.getItemAtIndex(this.selectionIndex);
        targetActor._delegate.activate();
    }
};

/** Creates a new SearchResults.
 *
 * This stores the provided search systems ({@link #_searchSystem},
 * {@link #_openSearchSystem}) and connects to their various
 * signals (['search-updated']{@link Search.SearchSystem.search-updated},
 * ['search-completed']{@link Search.SearchSystem.search-completed}) in order
 * to display the additional results.
 *
 * The top-level actor {@link #actor} is a `St.BoxLayout` with
 * style class 'searchResults' containing a scrollview containing a
 * `St.BoxLayout` with style class 'searchResultsContent'
 * ({@link #_content}).
 *
 * In this inner St.BoxLayout are put a new {@link GridSearchResults} for each
 * {@link Search.SearchProvider} in the search system. If
 * {@link Search.SearchProvider#createResultContainerActor} exists, this
 * is used instead of the GridSearchResults.
 *
 * A button is created for each provider in the {@link Search.OpenSearchSystem}
 * down the bottom of the search results ({@link #_SearchProvidersBox}).
 *
 * A status label is created that displays "Searching..." while we
 * are waiting for results ({@link #_statusText}).
 *
 * @param {Search.SearchSystem} searchSystem - search system we are displaying
 * results for (the system may have many {@link Search.SearchProvider}s).
 * @param {Search.OpenSearchSystem} openSearchSystem - open search system
 * we are displaying results for (the system may have many
 * {@link Search.OpenSearchProvider}s).
 * @classdesc
 * ![SearchDisplay](pics/searchDisplay.png)
 *
 * A SearchDisplay is responsible for displaying results from a
 * {@link Search.SearchSystem} and {@link Search.OpenSearchSystem}.
 *
 * In the picture above, the search display is showing a `GridSearchResults`
 * showing the results from its `SearchSystem`, which itself contains multiple
 * `SearchProvider`s (in the image you see the results for the
 * {@link AppDisplay.AppSearchProvider} and the
 * {@link AppDisplay.SettingsSearchProvider}).
 * 
 * The search box in the overview is *not* part of the SearchResults, but
 * the scrollview and buttons down the bottom (one per {@link Search.OpenSearchProvider}
 * in the {@link Search.OpenSearchSystem}) are part of it.
 *
 * So - a {@link SearchResults} has both a {@link Search.SearchSystem} and
 * {@link Search.OpenSearchSystem}.
 *
 * Each SearchSystem can have many {@link SearchProvider}s providing results
 * for it.
 *
 * The SearchResults is responsible for displaying these results. It creates
 * a new {@link GridSearchResults} for *each* provider to display the results
 * from that provider (or if it is implemented, it uses
 * {@link Search.SearchProvider#createResultContainerActor}).
 * These result containers are all amalgamated together in a single St.BoxLayout
 * to graphically present the results from many different providers.
 *
 * Overall - think of a {@link Search.SearchSystem} (containing many 
 * {@link Search.SearchProvider}s)
 * as being *logical* classes, handling all the search logic.
 * The {@link SearchResults} (containing many {@link GridSearchResults}, one per
 * provider) are the UI classes, handling the *displaying* of search results.
 *
 * @class
 * @todo picture
 */
function SearchResults(searchSystem, openSearchSystem) {
    this._init(searchSystem, openSearchSystem);
}

SearchResults.prototype = {
    _init: function(searchSystem, openSearchSystem) {
        /** Store the the search system we are displaying results for.
         * @type {Search.SearchSystem} */
        this._searchSystem = searchSystem;
        this._searchSystem.connect('search-updated', Lang.bind(this, this._updateCurrentResults));
        this._searchSystem.connect('search-completed', Lang.bind(this, this._updateResults));
        /** Store the the open search system we are creating buttons for.
         * @type {Search.OpenSearchSystem} */
        this._openSearchSystem = openSearchSystem;

        /** Top-level actor for the search results. Will contain the
         * status label, results for each of the search providers, and
         * buttons for each of the open search providers.
         *
         * It is a vertical `St.BoxLayout` with style class 'searchResults'.
         * @type {St.BoxLayout} */
        this.actor = new St.BoxLayout({ name: 'searchResults',
                                        vertical: true });

        /** Contains the results container for each of the search providers
         * in the saerch system. I.e. the elements of this will either be
         * what comes out from {@link Search.SearchProvider#createResultContainerActor}
         * (a {@link Search.SearchResultDisplay})
         * or a {@link GridSearchResults}.
         *
         * It is nested into a `St.ScrollView` before being added to
         * {@link #actor} to enable scrolling.
         *
         * Vertical `St.BoxLayout` with style class 'searchResultsContent'>
         * @type {St.BoxLayout} */
        this._content = new St.BoxLayout({ name: 'searchResultsContent',
                                           vertical: true });

        let scrollView = new St.ScrollView({ x_fill: true,
                                             y_fill: false,
                                             style_class: 'vfade' });
        scrollView.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
        scrollView.add_actor(this._content);

        this.actor.add(scrollView, { x_fill: true,
                                     y_fill: false,
                                     expand: true,
                                     x_align: St.Align.START,
                                     y_align: St.Align.START });
        this.actor.connect('notify::mapped', Lang.bind(this,
            function() {
                if (!this.actor.mapped)
                    return;

                let adjustment = scrollView.vscroll.adjustment;
                let direction = Overview.SwipeScrollDirection.VERTICAL;
                Main.overview.setScrollAdjustment(adjustment, direction);
            }));

        /** A label that displays "Searching..." while we are waiting for
         * search results to come in. Invisible otherwise.
         * @type {St.Label} */
        this._statusText = new St.Label({ style_class: 'search-statustext' });
        this._content.add(this._statusText);
        this._selectedProvider = -1;
        /** Local storage of the providers in {@link #_searchSystem}.
         * @type {Search.SearchProvider[]} */
        this._providers = this._searchSystem.getProviders();
        /** Holds metadata for each SearchProvider in
         * {@link #_searchSystem}.
         *
         * Each element of the array is an object with key 'provider' being
         * the {@link Search.SearchProvider}, 'actor' being the `St.BoxLayout`
         * containing the title and results for the provider, and
         * 'resultDisplay' being the {@link Search.SearchResultDisplay} or
         * {@link GridSearchResults} displaying the search results.
         * @type {[ProviderMeta]{@link ProviderMeta}[]}
         */
        this._providerMeta = [];
        /** Holds the result IDs for the search results for each provider.
         *
         * The key to the object is {@link Search.SearchProvider#title}
         * (like "APPLICATIONS") and the value is an array of result IDs returned
         * by that provider.
         * @type {Object.<string, string[]>} */
        this._providerMetaResults = {};
        for (let i = 0; i < this._providers.length; i++) {
            this.createProviderMeta(this._providers[i]);
            this._providerMetaResults[this.providers[i].title] = [];
        }
        /** This is where a button for each of the open search providers in
         * {@link #_openSearchSystem} is created.
         * A horizontal `St.BoxLayout` with style class 'search-providers-box'
         * that appears at the bottom of the SearchResults.
         * @type {St.BoxLayout} */
        this._searchProvidersBox = new St.BoxLayout({ style_class: 'search-providers-box' });
        this.actor.add(this._searchProvidersBox);

        this._openSearchProviders = [];
        this._openSearchSystem.connect('changed', Lang.bind(this, this._updateOpenSearchProviderButtons));
        this._updateOpenSearchProviderButtons();
    },

    /** Called whenever the providers in {@link #_openSearchSystem}
     * change (i.e. whenever ['changed']{@link Search.OpenSearchSystem.changed}
     * is emitted).
     *
     * This updates the buttons in {@link #_searchProvidersBox},
     * making sure there is one button per open search provider (like
     * Google, Wikipedia, ...).
     *
     * @see #_createOpenSearchProviderButton */
    _updateOpenSearchProviderButtons: function() {
        this._selectedOpenSearchButton = -1;
        for (let i = 0; i < this._openSearchProviders.length; i++)
            this._openSearchProviders[i].actor.destroy();
        this._openSearchProviders = this._openSearchSystem.getProviders();
        for (let i = 0; i < this._openSearchProviders.length; i++)
            this._createOpenSearchProviderButton(this._openSearchProviders[i]);
    },

    /** Updates the state of each of the buttons for the open search providers
     * by adding or removing the 'selected' pseudo style class from the buttons
     * depending on whether they are selected or not.
     *
     * Note that selected is different from hovering; selected is when you
     * navigate to the button with the keyboard and give it focus, while
     * hovering is just hovering over with the mouse. */
    _updateOpenSearchButtonState: function() {
         for (let i = 0; i < this._openSearchProviders.length; i++) {
             if (i == this._selectedOpenSearchButton)
                 this._openSearchProviders[i].actor.add_style_pseudo_class('selected');
             else
                 this._openSearchProviders[i].actor.remove_style_pseudo_class('selected');
         }
    },

    /** Creates a button for a particular open search provider, adding it
     * to {@link #_searchProvidersBox}. Clicking on this
     * button will call {@link Search.OpenSearchSystem#activateResult} on the
     * given provider, typically launching a web browser showing search results
     * for that provider.
     *
     * The button is a `St.Button` with style class 'dash-search-button'.
     *
     * @param {Search.OpenSearchProvider} provider - provider to create a button
     * for.
     */
    _createOpenSearchProviderButton: function(provider) {
        let button = new St.Button({ style_class: 'dash-search-button',
                                     reactive: true,
                                     x_fill: true,
                                     y_align: St.Align.MIDDLE });
        let bin = new St.Bin({ x_fill: false,
                               x_align:St.Align.MIDDLE });
        button.connect('clicked', Lang.bind(this, function() {
            this._openSearchSystem.activateResult(provider.id);
        }));
        let title = new St.Label({ text: provider.name,
                                   style_class: 'dash-search-button-label' });

        button.label_actor = title;
        bin.set_child(title);
        button.set_child(bin);
        provider.actor = button;

        this._searchProvidersBox.add(button);
    },

    /** Creates the UI bits for a particular search provider, adding them into
     * {@link #_content}.
     *
     * This consists of a St.BoxLayout with style class 'search-section'
     * containing:
     *
     * * a St.Label (style class 'search-section-header') displaying
     * the provider's title ({@link Search.SearchProvider#title});
     * * a St.Bin (style class 'search-section-results') containing either
     * [`provider.createResultContainerActor`]{@link Search.SearchProvider#createResultContainerActor}
     * (which should be a {@link Search.SearchResultDisplay})
     * if it was implemented, or a {@link GridSearchResults} for the provider
     * otherwise.
     *
     * It then adds this provider, top-level St.BoxLayout, and the results
     * display (GridSearchResults or createResultContainerActor()) to
     * {@link #_providerMeta}.
     * @param {Search.SearchProvider} provider - the {@link Search.SearchProvider}.
     */
    createProviderMeta: function(provider) {
        let providerBox = new St.BoxLayout({ style_class: 'search-section',
                                             vertical: true });
        let title = new St.Label({ style_class: 'search-section-header',
                                   text: provider.title });
        providerBox.add(title);

        let resultDisplayBin = new St.Bin({ style_class: 'search-section-results',
                                            x_fill: true,
                                            y_fill: true });
        providerBox.add(resultDisplayBin, { expand: true });
        let resultDisplay = provider.createResultContainerActor();
        if (resultDisplay == null) {
            resultDisplay = new GridSearchResults(provider);
        }
        resultDisplayBin.set_child(resultDisplay.actor);

        this._providerMeta.push({ provider: provider,
                                  actor: providerBox,
                                  resultDisplay: resultDisplay });
        this._content.add(providerBox);
    },

    /** Destroys the UI elements for a provider an removes it from
     * {@link #_providerMeta}.
     * @inheritparams #_createProviderMeta
     */
    destroyProviderMeta: function(provider) {
        for (let i=0; i < this._providerMeta.length; i++) {
            let meta = this._providerMeta[i];
            if (meta.provider == provider) {
                meta.actor.destroy();
                this._providerMeta.splice(i, 1);
                break;
            }
        }
    },

    /** Clears *all* result actors from *every* search provider's display.
     * @see GridSearchResults#clear
     * @see Search.SearchResultDisplay#clear */
    _clearDisplay: function() {
        this._selectedProvider = -1;
        this._visibleResultsCount = 0;
        for (let i = 0; i < this._providerMeta.length; i++) {
            let meta = this._providerMeta[i];
            meta.resultDisplay.clear();
            meta.actor.hide();
        }
    },

    /** Clears the result actors for a particular provider.
     * @param {number} index - which provider to clear
     */
    _clearDisplayForProvider: function(index) {
        let meta = this._providerMeta[index];
        meta.resultDisplay.clear();
        meta.actor.hide();
    },

    /** Completely resets the search provider - clears all the result actors,
     * resets the search system.
     * @see Search.SearchSystem#reset */
    reset: function() {
        this._searchSystem.reset();
        this._statusText.hide();
        this._clearDisplay();
        this._selectedOpenSearchButton = -1;
        this._updateOpenSearchButtonState();
    },

    /** Called when you wish to start a new search.
     * This resets the search display and displays the status text
     * "Searching..." ({@link #_statusText}). */
    startingSearch: function() {
        this.reset();
        this._statusText.set_text(_("Searching..."));
        this._statusText.show();
    },

    /** Performs a search.
     * @param {string} searchString - search terms as a single string.
     * @see Search.SearchSystem#updateSearch
     */
    doSearch: function (searchString) {
        this._searchSystem.updateSearch(searchString);
    },

    /** Gets the metadata for a particular provider, from
     * {@link #_providerMeta}.
     * @param {Search.SearchProvider} provider - provider to get the meta for.
     * @returns {ProviderMeta} metadata for that provider, or `undefined`
     * if not found.
     */
    _metaForProvider: function(provider) {
        return this._providerMeta[this._providers.indexOf(provider)];
    },

    /** Callback for {@link #_searchSystem}'s
     * ['search-updated`]{@link Search.SearchSystem.search-updated} signal,
     * called when search results get updated.
     *
     * It calls `renderDisplay` of that provider's result display on the new
     * results.
     * @see GridSearchResults#renderResults
     * @see Search.SearchResultDisplay#renderResults
     * @todo does inheritparams work on an event?
     * @inheritparams Search.SearchSystem.search-updated
     */
    _updateCurrentResults: function(searchSystem, provider, results) {
        let terms = searchSystem.getTerms();
        let meta = this._metaForProvider(provider);
        meta.resultDisplay.clear();
        meta.actor.show();
        meta.resultDisplay.renderResults(results, terms);
        return true;
    },

    /** Callback for {@link #_searchSystem}'s
     * ['search-completed`]{@link Search.SearchSystem.search-completed} signal,
     * called when a search is completed.
     *
     * If no results were found, {@link #_statusText} displays
     * "No matching results.".
     *
     * The search terms are entered into the open search system,
     * and for each provider, the provider is either hidden (if there are no
     * results for that provider), or shown and the results rendered.
     *
     * Finally the first result is selected.
     *
     * @see GridSearchResults#renderResults
     * @see Search.SearchResultDisplay#renderResults
     * @todo does inheritparams work on an event?
     * @inheritparams Search.SearchSystem.search-completed
     */
    _updateResults: function(searchSystem, results) {
        if (results.length == 0) {
            this._statusText.set_text(_("No matching results."));
            this._statusText.show();
        } else {
            this._selectedOpenSearchButton = -1;
            this._updateOpenSearchButtonState();
            this._statusText.hide();
        }

        let terms = searchSystem.getTerms();
        this._openSearchSystem.setSearchTerms(terms);

        // To avoid CSS transitions causing flickering
        // of the selection when the first search result
        // stays the same, we hide the content while
        // filling in the results and setting the initial
        // selection.
        this._content.hide();

        for (let i = 0; i < results.length; i++) {
            let [provider, providerResults] = results[i];
            if (providerResults.length == 0) {
                this._clearDisplayForProvider(i);
            } else {
                this._providerMetaResults[provider.title] = providerResults;
                this._clearDisplayForProvider(i);
                let meta = this._metaForProvider(provider);
                meta.actor.show();
                meta.resultDisplay.renderResults(providerResults, terms);
            }
        }

        if (this._selectedOpenSearchButton == -1)
            this.selectDown(false);

        this._content.show();

        return true;
    },

    /** This is used in the selectUp and selectDown functions to select an
     * appropriate search result *within* a single provider's search result
     * display.
     *
     * It selects the "next" or "previous" item within the results,
     * returning `true` if there was an item to select and `false` otherwise.
     * As a consequence of {@link GridSearchResults#selectIndex} the 'selected'
     * style is also applied to the selected actor.
     *
     * @see #selectUp
     * @see #selectDown
     * @see GridSearchResults#selectIndex
     * @inheritparams SearchResultDisplay#selectIndex
     * @param {GridSearchResults|Search.SearchResultDisplay} resultDisplay - the
     * result display to search within.
     * @param {boolean} up - whether we are searching 'up' (left) or
     * 'down' (right).
     */
    _modifyActorSelection: function(resultDisplay, up) {
        let success;
        let index = resultDisplay.getSelectionIndex();
        if (up && index == -1)
            index = resultDisplay.getVisibleResultCount() - 1;
        else if (up)
            index = index - 1;
        else
            index = index + 1;
        return resultDisplay.selectIndex(index);
    },

    /** Selects the result that is 'up' of the current one.
     * On 3.2, this navigates to the previous result to whatever is currently
     * selected, choosing results within each provider before moving onto the
     * next provider and eventually to the open search buttons.
     * (On 3.4+ it actually navigates in the more intuitive grid-like pattern -
     * pressing 'up' actually selects the result directly above the current int
     * the grid as opposed to the result to the left of the current).
     *
     * It just has to be careful to skip over search providers with no results.
     * @param {boolean} recursing - whether to recurse (what does this mean???)
     */
    selectUp: function(recursing) {
        if (this._selectedOpenSearchButton == -1) {
            for (let i = this._selectedProvider; i >= 0; i--) {
                let meta = this._providerMeta[i];
                if (!meta.actor.visible)
                    continue;
                let success = this._modifyActorSelection(meta.resultDisplay, true);
                if (success) {
                    this._selectedProvider = i;
                    return;
                }
            }
        }

        if (this._selectedOpenSearchButton == -1)
            this._selectedOpenSearchButton = this._openSearchProviders.length;
        this._selectedOpenSearchButton--;
        this._updateOpenSearchButtonState();
        if (this._selectedOpenSearchButton >= 0)
            return;

        if (this._providerMeta.length > 0 && !recursing) {
            this._selectedProvider = this._providerMeta.length - 1;
            this.selectUp(true);
        }
    },

    /** Selects the result that is 'down' of the current one.
     * On 3.2, this navigates to the next result to whatever is currently
     * selected, choosing results within each provider before moving onto the
     * next provider and eventually to the open search buttons.
     * (On 3.4+ it actually navigates in the more intuitive grid-like pattern -
     * pressing 'down' actually selects the result below the current as opposed
     * to the result to the right of the current).
     *
     * It just has to be careful to skip over search providers with no results.
     * @param {boolean} recursing - whether to recurse (what does this mean???)
     */
    selectDown: function(recursing) {
        let current = this._selectedProvider;
        if (this._selectedOpenSearchButton == -1) {
            if (current == -1)
                current = 0;
            for (let i = current; i < this._providerMeta.length; i++) {
                let meta = this._providerMeta[i];
                if (!meta.actor.visible)
                    continue;
                 let success = this._modifyActorSelection(meta.resultDisplay, false);
                 if (success) {
                    this._selectedProvider = i;
                    return;
                 }
            }
        }
        this._selectedOpenSearchButton++;

        if (this._selectedOpenSearchButton < this._openSearchProviders.length) {
            this._updateOpenSearchButtonState();
            return;
        }

        this._selectedOpenSearchButton = -1;
        this._updateOpenSearchButtonState();

        if (this._providerMeta.length > 0 && !recursing) {
            this._selectedProvider = 0;
            this.selectDown(true);
        }
    },

    /** activates the selected (search result or search button).
     * If the selected item is a search result, {@link Search.SearchResultDisplay#activateSelected}
     * ({@link GridSearchResults#activateSelected}) is called.
     *
     * If the selected item is one of the open search provider buttons,
     * {@link Search.OpenSearchSystem#activateResult} is called instead with
     * the selected provider. */
    activateSelected: function() {
        if (this._selectedOpenSearchButton != -1) {
            let provider = this._openSearchProviders[this._selectedOpenSearchButton];
            this._openSearchSystem.activateResult(provider.id);
            Main.overview.hide();
            return;
        }

        let current = this._selectedProvider;
        if (current < 0)
            return;
        let meta = this._providerMeta[current];
        let resultDisplay = meta.resultDisplay;
        resultDisplay.activateSelected();
        Main.overview.hide();
    }
};
/** Describes a {@link Search.SearchProvider} in the {@link SearchResults}.
 * @typedef ProviderMeta
 * @type {Object}
 * @property {Search.SearchProvider} provider - provider the metadata
 * is for.
 * @property {Search.SearchResultDisplay|SearchDisplay.GridSearchResults} resultDisplay -
 * the UI element displaying all the results. Unless
 * [`provider.createResultContainerActor]{@link Search.SearchProvider#createResultContainerActor}
 * is implemented, this will be a {@link GridSearchResults}.
 * @property {St.BoxLayout} actor - the actor containing the section
 * title and search results (`ProviderMeta.resultDisplay`).
 */
