// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * The popup you get upon switching workspaces through the keybindings that
 * shows you which workspace you're switching to.
 *
 * ![WorkspaceSwitcherPopup](pics/workspaceSwitcherPopup.png)
 */

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Main = imports.ui.main;

const Tweener = imports.ui.tweener;

/** @+
 * @const
 * @default
 * @type {number} */
/** time in seconds for the workspace switcher to fade out or in  */
const ANIMATION_TIME = 0.1;
/** time in milliseconds before the workspace switcher fades after being
 * displayed (unless the user does more workspace navigation) */
const DISPLAY_TIMEOUT = 600;

/** Direction to cycle through the workspaces (up) */
const UP = -1;
/** Direction to cycle through the workspaces (down) */
const DOWN = 1;
/** @- */

/** creates a new WorkspaceSwitcherPopup
 *
 * We create the top-level actor {@link #actor}, a St.Group that covers
 * the entire screen (but it's clear), and add it to the screen.
 *
 * Then we create the actual *visual* part of the popup (the grey box),
 * {@link #_container}.
 *
 * Inside this we nest a {@link Shell.GenericContainer}, {@link #_list}, and
 * this is what each workspace box is actually placed in.
 *
 * Finally we call {@link #_redraw} to draw the workspace switcher popup
 * (place all the workspace boxes into the popup) and {@link #_position} to
 * position the popup in the middle of the screen.
 *
 * @classdesc
 * ![WorkspaceSwitcherPopup](pics/workspaceSwitcherPopup.png)
 *
 * This is the popup that appears when you switch workspaces (via
 * e.g. via the keybindings) showing which workspace you've switched to.
 *
 * Its actor (`this.actor`) is a `St.Group` with style
 * 'workspace-switcher-group'. However, think of this is a big transparent
 * window pane that covers your screen that contains the actual
 * workspace switcher popup (the bit that you see) in the centre.
 *
 * The *actual* workspace switcher popup - the visual part - is in
 * `this._container`, a `St.BoxLayout` with style class
 * 'workspace-switcher-container'.
 *
 * Inside `this._container` is a `Shell.GenericContainer` `this._list`; this
 * is what *actually* does all the work laying out the workspace boxes next
 * to each other.
 *
 * To show the popup, use its [`display`]{@link #display}
 * method.
 *
 * @class
 */
function WorkspaceSwitcherPopup() {
    this._init();
}

WorkspaceSwitcherPopup.prototype = {
    _init : function() {
        /** A clear object covering the entire screen in which the visual
         * part of the workspace switcher is centred.
         * Style class 'workspace-switcher-group'.
         * @type {St.Group} */
        this.actor = new St.Group({ reactive: true,
                                         x: 0,
                                         y: 0,
                                         width: global.screen_width,
                                         height: global.screen_height,
                                         style_class: 'workspace-switcher-group' });
        Main.uiGroup.add_actor(this.actor);

        /** The actual visual part of the workspace switcher popup. The
         * style class is 'workspace-switcher-container'.
         * @type {St.BoxLayout}
         */
        this._container = new St.BoxLayout({ style_class: 'workspace-switcher-container' });
        /** This is the container that the boxes representing the workspaces are
         * added to, and the container that controls the layout of those boxes;
         * the style class is 'workspace-switcher'.
         *
         * The spacing between workspace boxes in the switcher is controlled by
         * the 'spacing' CSS property of this element's style class.
         */
        this._list = new Shell.GenericContainer({ style_class: 'workspace-switcher' });
        this._itemSpacing = 0;
        /** the height of each workspace box in the switcher such that they
         * would all fit vertically on the screen; this is the minimum of
         * 100 pixels (see style class 'ws-switcher-box') and the screen height
         * divided by the number of workspaces (taking into account spacing and
         * padding as specified by the various styles).
         *
         * Calculated in {#_getPreferredHeight} and used
         * in {#_getPreferredWidth}.
         * @type {number}
         */
        this._childHeight = 0;
        /** The width of each workspace box in the switcher.
         * It is calculated using `this._childHeight` such that each workspace
         * box would have the same aspect ratio as the screen.
         */
        this._childWidth = 0;
        this._list.connect('style-changed', Lang.bind(this, function() {
                                                        this._itemSpacing = this._list.get_theme_node().get_length('spacing');
                                                     }));

        this._list.connect('get-preferred-width', Lang.bind(this, this._getPreferredWidth));
        this._list.connect('get-preferred-height', Lang.bind(this, this._getPreferredHeight));
        this._list.connect('allocate', Lang.bind(this, this._allocate));
        this._container.add(this._list);

        this.actor.add_actor(this._container);

        this._redraw();

        this._position();

        this.actor.hide();

        this._timeoutId = Mainloop.timeout_add(DISPLAY_TIMEOUT, Lang.bind(this, this._onTimeout));
    },

    /** callback for the 'get-preferred-height' signal of
     * {@link #_list}.
     *
     * This returns 
     *
     *      Math.min(100 pixels * number_of_workspaces + spacing,
     *               availableHeight)
     *
     * where the 100 pixels is the default height of each workspace box
     * (see style class 'ws-switcher-box') and `spacing` incorporates the
     * spacing in between each box, and `availableHeight` is the height
     * available to the popup (the screen height minus any padding/margins
     * required by the style classes).
     *
     * It then stores the final height of each workspace box in
     * [`this._childHeight`]{@link WorkspaceSwitcher#_childHeight} to be
     * used in {@link WorkspaceSwitcher#_getPreferredWidth}.
     *
     * If all the workspace boxes fit in the avilable height with the default
     * height of 100 pixels then `this._childHeight` is just 100 pixels.
     * 
     * However if the boxes had to have a smaller height in order to fit them
     * all vertically in the screen, `this._childHeight` stores that smaller
     * height.
     */
    _getPreferredHeight : function (actor, forWidth, alloc) {
        let children = this._list.get_children();
        let primary = Main.layoutManager.primaryMonitor;

        let availHeight = primary.height;
        availHeight -= Main.panel.actor.height;
        availHeight -= this.actor.get_theme_node().get_vertical_padding();
        availHeight -= this._container.get_theme_node().get_vertical_padding();
        availHeight -= this._list.get_theme_node().get_vertical_padding();

        let height = 0;
        for (let i = 0; i < children.length; i++) {
            let [childMinHeight, childNaturalHeight] = children[i].get_preferred_height(-1);
            let [childMinWidth, childNaturalWidth] = children[i].get_preferred_width(childNaturalHeight);
            height += childNaturalHeight * primary.width / primary.height;
        }

        let spacing = this._itemSpacing * (global.screen.n_workspaces - 1);
        height += spacing;
        height = Math.min(height, availHeight);

        this._childHeight = (height - spacing) / global.screen.n_workspaces;

        alloc.min_size = height;
        alloc.natural_size = height;
    },

    /** callback for the 'get-preferred-width' signal of
     * {@link #_list}. Calculates the width required to
     * display the workspace switcher popup.
     *
     * This is just [`this._childHeight`]{@link WorkspaceSwitcher#_childHeight}
     * multiplied by the aspect ratio of the primary monitor (so that the
     * workspace boxes have the same aspect ratio as the primary monitor).
     */
    _getPreferredWidth : function (actor, forHeight, alloc) {
        let primary = Main.layoutManager.primaryMonitor;
        this._childWidth = Math.round(this._childHeight * primary.width / primary.height);

        alloc.min_size = this._childWidth;
        alloc.natural_size = this._childWidth;
    },

    /** callback for the 'allocate' signal of
     * {@link #_list}. This lays out all the child
     * workspace boxes vertically, spaced by the spacing specified in
     * `this._list`'s style class, and size given by
     * [`this._childWidth`]{@link #_childWidth} and
     * [`this._childHeight`]{@link #_childHeight}.
     */
    _allocate : function (actor, box, flags) {
        let children = this._list.get_children();
        let childBox = new Clutter.ActorBox();

        let y = box.y1;
        let prevChildBoxY2 = box.y1 - this._itemSpacing;
        for (let i = 0; i < children.length; i++) {
            childBox.x1 = box.x1;
            childBox.x2 = box.x1 + this._childWidth;
            childBox.y1 = prevChildBoxY2 + this._itemSpacing;
            childBox.y2 = Math.round(y + this._childHeight);
            y += this._childHeight + this._itemSpacing;
            prevChildBoxY2 = childBox.y2;
            children[i].allocate(childBox, flags);
        }
    },

    /** redraws the workspace switcher popup. This creates a new box for each
     * workspace the user has, and if the user is in the middle of navigating
     * between them it will show an up or down arrow on the workspace the
     * user is currently on indicating the direction the user is navigating.
     *
     * Note that each workspace box is just a `St.Bin` with style class
     * 'ws-switcher-active-up' if the workspace is active and the user is
     * navigating upwards; 'ws-switcher-active-down' if they're navigating
     * downwards, or 'ws-switcher-box' otherwise.
     *
     * @param {int} direction the user is cycling through workspace in.
     * Either {@link UP} or {@link DOWN}.
     * @param {int} activeWorkspaceIndex - the index of the active workspace
     * (will get a different style).
     */
    _redraw : function(direction, activeWorkspaceIndex) {
        this._list.destroy_children();

        for (let i = 0; i < global.screen.n_workspaces; i++) {
            let indicator = null;

           if (i == activeWorkspaceIndex && direction == UP)
               indicator = new St.Bin({ style_class: 'ws-switcher-active-up' });
           else if(i == activeWorkspaceIndex && direction == DOWN)
               indicator = new St.Bin({ style_class: 'ws-switcher-active-down' });
           else
               indicator = new St.Bin({ style_class: 'ws-switcher-box' });

           this._list.add_actor(indicator);

        }
    },

    /** Positions the workspace switcher popup to be in the centre of the
     * primary monitor. */
    _position: function() {
        let primary = Main.layoutManager.primaryMonitor;
        this._container.x = primary.x + Math.floor((primary.width - this._container.width) / 2);
        this._container.y = primary.y + Main.panel.actor.height +
                            Math.floor(((primary.height - Main.panel.actor.height) - this._container.height) / 2);
    },

    /** Fades in the workspace switcher popup over time {@link ANIMATION_TIME}
     * and positions it in the screen
     * ({@link WorkspaceSwitcherPoupup#_position}).
     *
     * The user should use {@link #display}, which calls
     * this.
     */
    _show : function() {
        Tweener.addTween(this._container, { opacity: 255,
                                            time: ANIMATION_TIME,
                                            transition: 'easeOutQuad'
                                           });
        this._position();
        this.actor.show();
    },

    /** Displays the workspace switcher popup on the screen, also making sure
     * that it fades after {@link DISPLAY_TIMEOUT} seconds have elapsed.
     *
     * Note that the popup does not itself cycle through the workspaces;
     * you cycle through the workspaces yourself but display the popup in sync
     * with that.
     * See {@link WindowManager.WindowManager} for an example of this being
     * used.
     *
     * @param {int} direction - the direction the user is cycling through
     * the workspaces, either {@link UP} or {@link DOWN}.
     * @param {index} activeWorkspaceIndex - the index of the active workspace
     * (will be styled differently).
     *
     * @see WindowManager.WindowManager
     */
    display : function(direction, activeWorkspaceIndex) {
        this._redraw(direction, activeWorkspaceIndex);
        if (this._timeoutId != 0)
            Mainloop.source_remove(this._timeoutId);
        this._timeoutId = Mainloop.timeout_add(DISPLAY_TIMEOUT, Lang.bind(this, this._onTimeout));
        this._show();
    },

    /** callback after the popup has been displayed
     * ({@link #display}) to fade it back out.
     */
    _onTimeout : function() {
        Mainloop.source_remove(this._timeoutId);
        this._timeoutId = 0;
        Tweener.addTween(this._container, { opacity: 0.0,
                                            time: ANIMATION_TIME,
                                            transition: 'easeOutQuad',
                                            onComplete: function() { this.actor.hide(); },
                                            onCompleteScope: this
                                           });
    }
};
