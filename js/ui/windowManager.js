// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Extra bells and whistles for the window manager implemented on the JS side
 * (mainly window animations).
 *
 * In particular:
 *
 * * when a window is mapped and it is a transient dialog for another window,
 * that other window is dimmed until the transient dialog is dismissed
 * ({@link WindowDimmer});
 * * when a dialog is mapped, it stretches out vertically from a horizontal line
 * to its full height;
 * * when a window is minimized, it shrinks towards the top-left of the screen
 */

const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const Meta = imports.gi.Meta;
const St = imports.gi.St;
const Shell = imports.gi.Shell;

const AltTab = imports.ui.altTab;
const WorkspaceSwitcherPopup = imports.ui.workspaceSwitcherPopup;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

/** @+
 * @const
 * @default
 * @type {number} */
/** Time (in seconds) to animate windows (e.g. when minimized they shrink). */
const WINDOW_ANIMATION_TIME = 0.25;
/** Time (in seconds) to dim a window. */
const DIM_TIME = 0.500;
/** Time (in seconds) to undim a window */
const UNDIM_TIME = 0.250;
/** @- */

var dimShader = undefined;

/** Retrieves the definition of the dim effect for window dimming (it's stored
 * in an OpenGL Shading Language file).
 *
 * This returns the contents of `global.datadir/shaders/dim-window.glsl`,
 * probably `/usr/share/gnome-shell/shaders/dim-window.glsl`.
 * @returns {string} the contents of the file
 * `/usr/share/gnome-shell/shaders/dim-window`.
 */
function getDimShaderSource() {
    if (!dimShader)
        dimShader = Shell.get_file_contents_utf8_sync(global.datadir + '/shaders/dim-window.glsl');
    return dimShader;
}

/** Calculates the width of the invisible border of a window on its top edge.
 *
 * Meta.WindowActors are slightly larger than their corresponding Meta.Windows
 * in order to make it easier for users to grab the edges (for resizing
 * and so on). The extra padding is invisible.
 * @param {Meta.Window} metaWindow - window
 * @returns {number} the width of the invisible padding on the top edge
 * (pixels).
 */
function getTopInvisibleBorder(metaWindow) {
    let outerRect = metaWindow.get_outer_rect();
    let inputRect = metaWindow.get_input_rect();
    return outerRect.y - inputRect.y;
}

/** creates a new WindowDimmer
 *
 * If Clutter does not support GLSL (OpenGL Shader Language) shaders, we
 * do nothing.
 *
 * If it does, we load the window dimmer shader ({@link getDimShaderSource})
 * into a Clutter.ShadeEffect and store it in {@link #_effect}.
 * @param {Meta.WindowActor} actor - window actor that we wish to apply the
 * dim affect to.
 * @classdesc
 *
 * A WindowDimmer is used to dim a window.
 *
 * It achieves this via a Clutter.ShaderEffect with OpenGL.
 *
 * (Note - why couldn't we use a {@link Lightbox.Lightbox}?)
 *
 * By setting the [dimFraction property]{@link #dimFraction} one changes
 * the amount of dimming (1 is fully dimmed, 0 is undimmed).
 *
 * This is used by the {@link WindowManager} to dim windows that have
 * modal dialogs present (while the dialogs are present).
 *
 * @todo picture
 * @class
 */
function WindowDimmer(actor) {
    this._init(actor);
}

WindowDimmer.prototype = {
    _init: function(actor) {
        if (Clutter.feature_available(Clutter.FeatureFlags.SHADERS_GLSL)) {
            this._effect = new Clutter.ShaderEffect({ shader_type: Clutter.ShaderType.FRAGMENT_SHADER });
            this._effect.set_shader_source(getDimShaderSource());
        } else {
            /** The Clutter.ShaderEffect used to dim the window.
             *
             * It is `null` if Clutter does not support OpenGL shaders
             * @type {?Clutter.ShaderEffect} */
            this._effect = null;
        }

        /** The actor we are dimming.
         * @type {Meta.WindowActor} */
        this.actor = actor;
    },

    /** The dim fraction of the dimmer. 0 is undimmed, 1 is fully dimmed.
     *
     * This adds {@link #_effect} to the window we are dimming, only enabled if
     * the metacity preference to attach modal dialogs to their parent windows
     * is enabled.
     *
     * @param {fraction} dim fraction to set - 0 is undimmed, 1 is dimmed.
     * @todo screenshot dimmed and not dimmed
     */
    set dimFraction(fraction) {
        this._dimFraction = fraction;

        if (this._effect == null)
            return;

        if (!Meta.prefs_get_attach_modal_dialogs()) {
            this._effect.enabled = false;
            return;
        }

        if (fraction > 0.01) {
            Shell.shader_effect_set_double_uniform(this._effect, 'height', this.actor.get_height());
            Shell.shader_effect_set_double_uniform(this._effect, 'fraction', fraction);

            if (!this._effect.actor)
                this.actor.add_effect(this._effect);
        } else {
            if (this._effect.actor)
                this.actor.remove_effect(this._effect);
        }
    },

    get dimFraction() {
        return this._dimFraction;
    },

    _dimFraction: 0.0
};

/** Creates a {@link WindowDimmer} for a Meta.WindowActor `actor`.
 *
 * The newly-created dimmer is returned and also stored in
 * `actor._windowDimmer`.
 * @inheritparams WindowDimmer
 * @returns {WindowManager.WindowDimmer} a {@link WindowDimmer} for this
 * window.
 */
function getWindowDimmer(actor) {
    if (!actor._windowDimmer)
        actor._windowDimmer = new WindowDimmer(actor);

    return actor._windowDimmer;
}

/** creates a new WindowManager
 *
 * We first connect to the window manager signals that we wish to animate
 * (minimize, maximize, map, destroy, unmaximize, switch-workspace).
 *
 * Then we use {@link #setKeybindingHandler} to add hooks onto the keybindings
 * we want to do take over (showing popups and so on): switching workspaces,
 * showing the a11y switcher, and showing the alt tab popup.
 * @classdesc
 *
 * The WindowManager class adds bells and whistles onto the existing
 * global Shell.WindowManager. It mainly handles extra animations for window
 * actions (minimizing, ...) as well as keybindings (for example showing the
 * workspace switcher popup when switching workspaces).
 *
 * Keybindings handled:
 *
 * * switch_to_workspace_{left, right, up, down}: show the
 * [workspace switcher popup]{@link WorkspaceSwitcherPopup.WorkspaceSwitcherPopup}.
 * * switch_{windows, windows_backward, group, group_backward}: show the
 * [alt-tab popup]{@link AltTab.AltTabPopup}.
 * * switch_panels: show the [accessibility switcher]{@link CtrlAltTab.CtrlAltTabManager}.
 *
 * Supports animations for:
 *
 * * switch-workspace: when workspaces are switched (be it by keybinding or
 * elsewise), we animate the contents of this workspace sliding out of
 * the screen and the contents fo the new workspace sliding in.
 * * minimizing windows: shrink them to the top-left corner of the primary
 * monitor (top-right in a right-to-left text system).
 * * maximizing windows: no animation done.
 * * unmaximizing windows: no animation done;
 * * mapping windows: when windows are mapped (unminimized or appear for the
 * first time), we either:
 *  + fade the window in (if it's not a modal dialog);
 *  + animate the window in by starting it as a horizontal line and stretching
 *  it vertically to its full height, if it's a modal dialog. Also, dim the
 *  parent window.
 * * destroying windows: windows that aren't modal dialogs are just destroyed.
 * A modal dialog is animated out in the opposite way it was animated in
 * (shrink vertically to a horizontal line) and its parent window is undimmed
 * if it has no more modal dialogs open.
 * @class
 */
function WindowManager() {
    this._init();
}

WindowManager.prototype = {
    _init : function() {
        /** The global window manager instance.
         * @type {Shell.WM} */
        this._shellwm =  global.window_manager;

        /** Stores the signal IDs we are using to listen to keybindings
         * that we have taken over with {@link #setKeybindingHandler}.
         *
         * Its keys are strings being the keybinding name, and the value
         * is a number being the signal ID from
         * `this._shellwm.connect('keybinding::[keybinding_name]', ...)`.
         *
         * @type {Object.<string, number>}
         * @see #setKeybindingHandler
         */
        // note - although it's initialized as an array, it is an object.
        this._keyBindingHandlers = [];
        /** Array of windows we are currently animating as they minimize.
         * @see #_minimizeWindow
         * @type {Meta.WindowActor[]} */
        this._minimizing = [];
        /** Array of windows we are currently animating as they maximize.
         * @see #_maximizeWindow
         * @type {Meta.WindowActor[]} */
        this._maximizing = [];
        /** Array of windows we are currently animating as they unmaximize.
         * @see #_unmaximizeWindow
         * @type {Meta.WindowActor[]} */
        this._unmaximizing = [];
        /** Array of windows we are currently animating as they are being mapped.
         * @see #_mapWindow
         * @type {Meta.WindowActor[]} */
        this._mapping = [];
        /** Array of windows we are currently animating as they are being destroyed.
         * @see #_destroyWindow
         * @type {Meta.WindowActor[]} */
        this._destroying = [];

        /** Windows that are currently dimmed.
         * @type {Meta.Window[]} */
        this._dimmedWindows = [];

        this._animationBlockCount = 0;

        this._switchData = null;
        this._shellwm.connect('kill-switch-workspace', Lang.bind(this, this._switchWorkspaceDone));
        this._shellwm.connect('kill-window-effects', Lang.bind(this, function (shellwm, actor) {
            this._minimizeWindowDone(shellwm, actor);
            this._maximizeWindowDone(shellwm, actor);
            this._unmaximizeWindowDone(shellwm, actor);
            this._mapWindowDone(shellwm, actor);
            this._destroyWindowDone(shellwm, actor);
        }));

        this._shellwm.connect('switch-workspace', Lang.bind(this, this._switchWorkspace));
        this._shellwm.connect('minimize', Lang.bind(this, this._minimizeWindow));
        this._shellwm.connect('maximize', Lang.bind(this, this._maximizeWindow));
        this._shellwm.connect('unmaximize', Lang.bind(this, this._unmaximizeWindow));
        this._shellwm.connect('map', Lang.bind(this, this._mapWindow));
        this._shellwm.connect('destroy', Lang.bind(this, this._destroyWindow));

        /** This stores the workspace switcher popup (the dialog that pops up
         * when you switch workspaces that shows you which workspace you're
         * going to).
         *
         * It is only created on demand, so may be `null` until the first
         * time it is needed.
         * @type {?WorkspaceSwitcherPopup.WorkspaceSwitcherPopup} */
        this._workspaceSwitcherPopup = null;
        this.setKeybindingHandler('switch_to_workspace_left', Lang.bind(this, this._showWorkspaceSwitcher));
        this.setKeybindingHandler('switch_to_workspace_right', Lang.bind(this, this._showWorkspaceSwitcher));
        this.setKeybindingHandler('switch_to_workspace_up', Lang.bind(this, this._showWorkspaceSwitcher));
        this.setKeybindingHandler('switch_to_workspace_down', Lang.bind(this, this._showWorkspaceSwitcher));
        this.setKeybindingHandler('switch_windows', Lang.bind(this, this._startAppSwitcher));
        this.setKeybindingHandler('switch_group', Lang.bind(this, this._startAppSwitcher));
        this.setKeybindingHandler('switch_windows_backward', Lang.bind(this, this._startAppSwitcher));
        this.setKeybindingHandler('switch_group_backward', Lang.bind(this, this._startAppSwitcher));
        this.setKeybindingHandler('switch_panels', Lang.bind(this, this._startA11ySwitcher));

        Main.overview.connect('showing', Lang.bind(this, function() {
            for (let i = 0; i < this._dimmedWindows.length; i++)
                this._undimWindow(this._dimmedWindows[i], true);
        }));
        Main.overview.connect('hiding', Lang.bind(this, function() {
            for (let i = 0; i < this._dimmedWindows.length; i++)
                this._dimWindow(this._dimmedWindows[i], true);
        }));
    },

    /** Takes over an *existing* (Metacity?) keybinding to add a javascript
     * handler for it. This cannot be used for adding *new* keybindings,
     * only take over existing ones.
     *
     * For example, the WindowManager takes over the 'switch_to_workspace_*'
     * keybindings, diverting them to {@link #_showWorkspaceSwitcher}.
     *
     * Internally, we
     * call `shell_wm_takeover_keybinding` to tell the window manager that we
     * should be notified of this keybinding, and connect to its
     * 'keybinding::keybinding_name' signal with `handler` as the callback.
     *
     * TODO: what's existing? in gsettings? in metacity?
     * @param {string} keybinding - name of the keybinding, e.g.
     * 'switch_to_workspace_down'.
     * @param {function(shellwm, binding, mask, window, backwards)} handler -
     * a javascript function to be called upon the keybinding being pressed.
     * See {@link #_startAppSwitcher}, {@link #_startA11ySwitcher}
     * and {@link #_showWorkspaceSwitcher} which are all examples of this.
     *
     * @see #_startAppSwitcher
     * @see #_startA11ySwitcher
     * @see #_showWorkspaceSwitcher
     */
    setKeybindingHandler: function(keybinding, handler){
        if (this._keyBindingHandlers[keybinding])
            this._shellwm.disconnect(this._keyBindingHandlers[keybinding]);
        else
            this._shellwm.takeover_keybinding(keybinding);

        this._keyBindingHandlers[keybinding] =
            this._shellwm.connect('keybinding::' + keybinding, handler);
    },

    /** Block window animations.
     *
     * Animations are blocked until {@link #unblockAnimations} is called as
     * many times as `blockAnimations` has been called.
     */
    blockAnimations: function() {
        this._animationBlockCount++;
    },

    /** Unblock window animations.
     *
     * Animations are not unblocked until {@link #unblockAnimations} is called as
     * many times as `blockAnimations` has been called.
     */
    unblockAnimations: function() {
        this._animationBlockCount = Math.max(0, this._animationBlockCount - 1);
    },

    /** Whether we should do window animations. If `actor` is supplied,
     * this is whether we should perform window animations on `actor`.
     *
     * If the overview is visible or we are currently blocking animations
     * (see {@link #blockAnimations}), we *shouldn't* animate.
     *
     * If `actor` is provided and the window's type is not NORMAL, we *shouldn't*
     * animate.
     *
     * Otherwise we can.
     * @param {Meta.WindowActor} actor - a window's actor.
     * @returns {boolean} whether we should do any animations, or if `actor`
     * is supplied, whether we should do window animations for `actor`.
     */
    _shouldAnimate : function(actor) {
        if (Main.overview.visible || this._animationsBlocked > 0)
            return false;
        if (actor && (actor.meta_window.get_window_type() != Meta.WindowType.NORMAL))
            return false;
        return true;
    },

    /** Removes `actor` from `list`, returning `true` if it was found in `list`
     * and `false` if it didn't exist in `list`.
     *
     * Its use within this class is to remove a window from the list of
     * windows that are being animated with various effects
     * (e.g. {@link #_minimizing}, {@link #_maximizing}), where we want to
     * know if it was in the list in the first place.
     *
     * @param {Meta.WindowActor[]} list - array to look `actor` up in
     * (for us, one of {@link #_minimizing}, {@link #_maximizing},
     * {@link #_unmaximizing}, {@link #_mapping} or {@link #_destroying}).
     * @inheritparams #_shouldAnimate
     * @returns {boolean} whether `actor` was in `list` to begin with.
     */
    _removeEffect : function(list, actor) {
        let idx = list.indexOf(actor);
        if (idx != -1) {
            list.splice(idx, 1);
            return true;
        }
        return false;
    },

    /** Callback when any window is minimized (Shell.wm's 'minimize' signal).
     *
     * If the window is a NORMAL one (as opposed to modal), we will animate it
     * by shrinking it down to nothing towards the top-left corner of the
     * primary monitor (top-right if text direction is RTL).
     *
     * Upon completion of the animation we call {@link #_minimizeWindowDone},
     * or on tween overwrite (another tween is placed on this window while
     * the first is in progress) we call {@link #_minimizeWindowOverwritten}.
     *
     * The animation occurs over time {@link WINDOW_ANIMATION_TIME}.
     *
     * @param {Shell.WM} shellwm - the global Shell.WindowManager
     * ({@link #_shellwm})
     * @inheritparams #_shouldAnimate
     * @see #_minimizeWindowDone
     * @see #_minimizeWindowOverwritten
     */
    _minimizeWindow : function(shellwm, actor) {
        if (!this._shouldAnimate(actor)) {
            shellwm.completed_minimize(actor);
            return;
        }

        actor.set_scale(1.0, 1.0);
        actor.move_anchor_point_from_gravity(Clutter.Gravity.CENTER);

        /* scale window down to 0x0.
         * maybe TODO: get icon geometry passed through and move the window towards it?
         */
        this._minimizing.push(actor);

        let primary = Main.layoutManager.primaryMonitor;
        let xDest = primary.x;
        if (St.Widget.get_default_direction() == St.TextDirection.RTL)
            xDest += primary.width;

        Tweener.addTween(actor,
                         { scale_x: 0.0,
                           scale_y: 0.0,
                           x: xDest,
                           y: 0,
                           time: WINDOW_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._minimizeWindowDone,
                           onCompleteScope: this,
                           onCompleteParams: [shellwm, actor],
                           onOverwrite: this._minimizeWindowOverwritten,
                           onOverwriteScope: this,
                           onOverwriteParams: [shellwm, actor]
                         });
    },

    /** Callback when a window has finished its minimize animation.
     *
     * All outstanding tweens are removed on the actor, it is removed
     * from {@link #_minimizing} and
     * `shellwm.completed_minimize` is called on it to finalise the window
     * effect.
     *
     * @inheritparams #_minimizeWindow
     * @see #_minimizeWindow
     * @see #_minimizeWindowOverwritten
     */
    _minimizeWindowDone : function(shellwm, actor) {
        if (this._removeEffect(this._minimizing, actor)) {
            Tweener.removeTweens(actor);
            actor.set_scale(1.0, 1.0);
            actor.move_anchor_point_from_gravity(Clutter.Gravity.NORTH_WEST);

            shellwm.completed_minimize(actor);
        }
    },

    /** Callback when a window in the act of minimizing has another tween
     * added to it.
     *
     * We call
     * `shellwm.completed_minimize` is called on it to finalise the window
     * effect and remove the window from {@link #_minimizing}.
     * @inheritparams #_minimizeWindow
     * @see #_minimizeWindow
     * @see #_minimizeWindowDone
     */
    _minimizeWindowOverwritten : function(shellwm, actor) {
        if (this._removeEffect(this._minimizing, actor)) {
            shellwm.completed_minimize(actor);
        }
    },

    /** Callback when any window is maximized (Shell.wm's 'maximize' signal).
     *
     * We just call `shellwm.completed_maximize` (no animation done?).
     *
     * Note that {@link #_maximizeWindowDone} and
     * {@link #_maximizeWindowOverwrite} exist, but don't do anything as we
     * don't animate maximizing a window.
     *
     * They are just there to fit in with the analagous minimize/map/destroy
     * functions (and future proofing).
     *
     * @param {number} targetX - target x coordinate of the window
     * @param {number} targetY - target y coordinate of the window
     * @param {number} targetWidth - target width of the window
     * @param {number} targetHeight - target height of the window
     * @inheritparams #_minimizeWindow
     * @see #_maximizeWindowDone
     * @see #_maximizeWindowOverwrite
     */
    _maximizeWindow : function(shellwm, actor, targetX, targetY, targetWidth, targetHeight) {
        shellwm.completed_maximize(actor);
    },

    /** Does nothing as we don't do maximize window animations.
     *
     * Is intended to be used in a similar way to {@link #_minimizeWindowDone},
     * see {@link #_minimizeWindow}.
     * @see #_maximizeWindow
     * @see #_maximizeWindowOverwrite
     * @inheritparams #_minimizeWindow
     */
    _maximizeWindowDone : function(shellwm, actor) {
    },

    /** Does nothing as we don't do maximize window animations.
     *
     * Is intended to be used in a similar way to {@link #_minimizeWindowOverwritten},
     * see {@link #_minimizeWindow}.
     * @inheritparams #_minimizeWindow
     * @see #_maximizeWindow
     * @see #_maximizeWindowDone
     */
    _maximizeWindowOverwrite : function(shellwm, actor) {
    },

    /** Callback when any window is unmaximized (Shell.wm's 'unmaximize' signal).
     *
     * We just call `shellwm.completed_unmaximize` (no animation done?).
     *
     * Note that {@link #_unmaximizeWindowDone} and
     * {@link #_unmaximizeWindowOverwrite} exist, but don't do anything as we
     * don't animate unmaximizing a window.
     *
     * They are just there to fit in with the analagous minimize/map/destroy
     * functions (and future proofing).
     * @inheritparams #_maximizeWindow
     * @see #_unmaximizeWindowDone
     */
    _unmaximizeWindow : function(shellwm, actor, targetX, targetY, targetWidth, targetHeight) {
        shellwm.completed_unmaximize(actor);
    },

    /** Does nothing as we don't do our own animations for unmaximizing windows.
     *
     * Is intended to be used in a similar way to {@link #_minimizeWindowDone},
     * see {@link #_minimizeWindow}.
     * @see #_unmaximizeWindow
     * @inheritparams #_minimizeWindow
     */
    _unmaximizeWindowDone : function(shellwm, actor) {
    },

    /** Returns whether a window has attached dialogs (besides `ignoreWindow`).
     * @param {Meta.Window} window - window to test
     * @param {Meta.Window} ignoreWindow - if this is a transient of `window`,
     * ignore it.
     * @return {boolean} whether `window` has any attached dialogs that aren't
     * `ignoreWindow`.
     */
    _hasAttachedDialogs: function(window, ignoreWindow) {
        var count = 0;
        window.foreach_transient(function(win) {
            if (win != ignoreWindow && win.is_attached_dialog())
                count++;
            return false;
        });
        return count != 0;
    },

    /** Checks the dimming state of `window`. Recall that a window with
     * an attached dialog should be dimmed (unless the overview is visible):
     *
     * * if `window` has attached dialogs open (other than `ignoreWindow`), it
     * should be dimmed. We dim it if it isn't already (and the overview isn't
     * open).
     * * otherwise if `window` shouldn't be dimmed and it is, we undim it (if
     * the overview is not visible).
     *
     * Either way we keep {@link #_dimmedWindows} in sync with the
     * currently-dimmed windows.
     * @inheritparams #_hasAttachedDialogs
     */
    _checkDimming: function(window, ignoreWindow) {
        let shouldDim = this._hasAttachedDialogs(window, ignoreWindow);

        if (shouldDim && !window._dimmed) {
            window._dimmed = true;
            this._dimmedWindows.push(window);
            if (!Main.overview.visible)
                this._dimWindow(window, true);
        } else if (!shouldDim && window._dimmed) {
            window._dimmed = false;
            this._dimmedWindows = this._dimmedWindows.filter(function(win) {
                                                                 return win != window;
                                                             });
            if (!Main.overview.visible)
                this._undimWindow(window, true);
        }
    },

    /** Dims a window, either over time {@link DIM_TIME} or instantly
     * depending on `animate`. (The animation is done by adjusting
     * {@link WindowDimmer#dimFraction} to 1).
     * @inheritparams #_hasAttachedDialogs
     * @param {boolean} animate - whether to animate the dimming.
     * instantly.
     * @see getWindowDimmer
     */
    _dimWindow: function(window, animate) {
        let actor = window.get_compositor_private();
        if (!actor)
            return;
        if (animate)
            Tweener.addTween(getWindowDimmer(actor),
                             { dimFraction: 1.0,
                               time: DIM_TIME,
                               transition: 'linear'
                             });
        else
            getWindowDimmer(actor).dimFraction = 1.0;
    },

    /** Undims a window, either over time {@link UNDIM_TIME} or instantly
     * depending on `animate`. (The animation is done by adjusting
     * {@link WindowDimmer#dimFraction} back to 0).
     * @inheritparams #_hasAttachedDialogs
     * @param {boolean} animate - whether to animate the undimming or undim it
     * instantly.
     * @see getWindowDimmer
     */
    _undimWindow: function(window, animate) {
        let actor = window.get_compositor_private();
        if (!actor)
            return;
        if (animate)
            Tweener.addTween(getWindowDimmer(actor),
                             { dimFraction: 0.0,
                               time: UNDIM_TIME,
                               transition: 'linear'
                             });
        else
            getWindowDimmer(actor).dimFraction = 0.0;
    },

    /** Callback when any window is mapped (Shell.wm's 'map' signal). Mapping
     * is when the window is unminimized or first opened on the screen.
     *
     * If the window being mapped (`actor.meta_window`) is a modal dialog/transient
     * for another window we check that this parent window is appropriately
     * dimmed ({@link #_checkDimming}).
     *
     * If animations are not blocked
     * ({@link #_shouldAnimate}), we animate it by stretching it vertically
     * from a horizontal line to its full height. This occurs over time
     * {@link WINDOW_ANIMATION_TIME}.
     *
     * Upon completion of the animation we call {@link #_mapWindowDone},
     * or on tween overwrite (another tween is placed on this window while
     * the first is in progress) we call {@link #_mapWindowOverwrite}.
     *
     * After mapping the dialog (whether we animate it or not), we call
     * `shellwm.completed_map` to tell the window manager we're done.
     *
     * If however the meta window is *not* a modal dialog, we instead fade it in
     * from opacity 0 to 255 over time {@link WINDOW_ANIMATION_TIME}.
     *
     * As with the modal dialogs, we call {@link #_mapWindowDone} on completion,
     * and {@link #_mapWindowOverwrite} if another tween overwrites ours.
     *
     * Finally, for all windows, we listen to changes in its window type
     * such that if it changes to or from a modal dialog, the parent window
     * is appropriately dimmed or undimmed.
     *
     * @inheritparams #_minimizeWindow
     * @see #_mapWindowDone
     * @see #_mapWindowOverwrite
     * @see @_checkDimming
     */
    _mapWindow : function(shellwm, actor) {
        actor._windowType = actor.meta_window.get_window_type();
        actor._notifyWindowTypeSignalId = actor.meta_window.connect('notify::window-type', Lang.bind(this, function () {
            let type = actor.meta_window.get_window_type();
            if (type == actor._windowType)
                return;
            if (type == Meta.WindowType.MODAL_DIALOG ||
                actor._windowType == Meta.WindowType.MODAL_DIALOG) {
                let parent = actor.get_meta_window().get_transient_for();
                if (parent)
                    this._checkDimming(parent);
            }

            actor._windowType = type;
        }));
        if (actor.meta_window.is_attached_dialog()) {
            this._checkDimming(actor.get_meta_window().get_transient_for());
            if (this._shouldAnimate()) {
                actor.set_scale(1.0, 0.0);
                actor.show();
                this._mapping.push(actor);

                Tweener.addTween(actor,
                                 { scale_y: 1,
                                   time: WINDOW_ANIMATION_TIME,
                                   transition: "easeOutQuad",
                                   onComplete: this._mapWindowDone,
                                   onCompleteScope: this,
                                   onCompleteParams: [shellwm, actor],
                                   onOverwrite: this._mapWindowOverwrite,
                                   onOverwriteScope: this,
                                   onOverwriteParams: [shellwm, actor]
                                 });
                return;
            }
            shellwm.completed_map(actor);
            return;
        }
        if (!this._shouldAnimate(actor)) {
            shellwm.completed_map(actor);
            return;
        }

        actor.opacity = 0;
        actor.show();

        /* Fade window in */
        this._mapping.push(actor);
        Tweener.addTween(actor,
                         { opacity: 255,
                           time: WINDOW_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._mapWindowDone,
                           onCompleteScope: this,
                           onCompleteParams: [shellwm, actor],
                           onOverwrite: this._mapWindowOverwrite,
                           onOverwriteScope: this,
                           onOverwriteParams: [shellwm, actor]
                         });
    },

    /** Callback when a window has finished its map animation.
     *
     * All outstanding tweens are removed on the actor, it is removed
     * from {@link #_mapping} and
     * `shellwm.completed_map` is called on it to finalise the window
     * effect.
     *
     * @inheritparams #_mapWindow
     * @see #_mapWindow
     * @see #_mapWindowOverwritten
     */
    _mapWindowDone : function(shellwm, actor) {
        if (this._removeEffect(this._mapping, actor)) {
            Tweener.removeTweens(actor);
            actor.opacity = 255;
            shellwm.completed_map(actor);
        }
    },

    /** Callback when a window in the act of mapping has another tween
     * added to it.
     *
     * We call `shellwm.completed_map` is called on it to finalise the window
     * effect and remove the window from {@link #_mapping}.
     * @inheritparams #_mapWindow
     * @see #_mapWindow
     * @see #_mapWindowDone
     */
    _mapWindowOverwrite : function(shellwm, actor) {
        if (this._removeEffect(this._mapping, actor)) {
            shellwm.completed_map(actor);
        }
    },

    /** Callback when any window is destroyd (Shell.wm's 'destroy' signal).
     *
     * We:
     * * disconnect any signals we were listening to (for example the window
     * type in order to do window dimming);
     * * remove dimming of the window/its parent window if relevant;
     * * destroy the window:
     *  + if we shouldn't animate ({@link #_shouldAnimate}), just call
     *    `shell_wm.completed_destroy` to remove the window and return.
     *  + if we are animating ({@link #_shouldAnimate}) and the window is a
     *    modal dialog, the destroy
     *    animation is to contract the window vertically inwards into
     *    a horizontal line before it disappears (opposite as the map animation
     *    for modal dialogs);
     *  + otherwise (we shouldn't animate or it's not a modal dialog), we
     *    just call `shell_wm.completed_destroy` to remove the window and return.
     *
     * The animation if any occurs over time {@link WINDOW_ANIMATION_TIME},
     * and {@link #_destroyWindowDone} is called upon completion or overwrite.
     * @inheritparams #_minimizeWindow
     * @see #_destroyWindowDone
     */
    _destroyWindow : function(shellwm, actor) {
        let window = actor.meta_window;
        if (actor._notifyWindowTypeSignalId) {
            window.disconnect(actor._notifyWindowTypeSignalId);
            actor._notifyWindowTypeSignalId = 0;
        }
        if (window._dimmed) {
            this._dimmedWindows = this._dimmedWindows.filter(function(win) {
                                                                 return win != window;
                                                             });
        }
        if (window.is_attached_dialog()) {
            let parent = window.get_transient_for();
            this._checkDimming(parent, window);
            if (!this._shouldAnimate()) {
                shellwm.completed_destroy(actor);
                return;
            }

            actor.set_scale(1.0, 1.0);
            actor.show();
            this._destroying.push(actor);

            actor._parentDestroyId = parent.connect('unmanaged', Lang.bind(this, function () {
                Tweener.removeTweens(actor);
                this._destroyWindowDone(shellwm, actor);
            }));

            Tweener.addTween(actor,
                             { scale_y: 0,
                               time: WINDOW_ANIMATION_TIME,
                               transition: "easeOutQuad",
                               onComplete: this._destroyWindowDone,
                               onCompleteScope: this,
                               onCompleteParams: [shellwm, actor],
                               onOverwrite: this._destroyWindowDone,
                               onOverwriteScope: this,
                               onOverwriteParams: [shellwm, actor]
                             });
            return;
        }
        shellwm.completed_destroy(actor);
    },

    /** Callback when a window has finished its destroy animation.
     *
     * All outstanding tweens are removed on the actor, it is removed
     * from {@link #_destroying} and
     * `shellwm.completed_destroy` is called on it to finalise the window
     * effect.
     *
     * @inheritparams #_destroyWindow
     * @see #_destroyWindow
     */
    _destroyWindowDone : function(shellwm, actor) {
        if (this._removeEffect(this._destroying, actor)) {
            let parent = actor.get_meta_window().get_transient_for();
            if (parent && actor._parentDestroyId) {
                parent.disconnect(actor._parentDestroyId);
                actor._parentDestroyId = 0;
            }
            shellwm.completed_destroy(actor);
        }
    },

    /** Callback when a workspace is switched (Shell.wm's 'switch-workspace' signal).
     *
     * This performs the *animation* of switching workspaces (where the
     * contents of the current workspace seem to "slide" out of view as you
     * navigate to the next one). I think this is done by creating a Clutter.Group
     * of `from`'s windows and `to`s windows, and we animate sliding `from`'s
     * contents out and `to`'s contents in (according to `direction`). A bit
     * of care is taken to ensure that windows that are marked "on all workspace"
     * are handled appropriately.
     *
     * The animation occurs in {@link WINDOW_ANIMATION_TIME}.
     *
     * Contrast with {@link #_showWorkspaceSwitcher}, which is triggered when
     * the user presses the switch workspace keybinding and tells gnome-shell
     * to do the switching of workspaces. *This* is triggered
     * when workspaces are switched, regardless of the cause.
     *
     * {@link #_switchWorkspaceDone} is called when the animation is complete.
     *
     * @param {Meta.Workspace} from - workspace we just switched from
     * @param {Meta.Workspace} to - workspace we are switching to
     * @param {Meta.MotionDirection} direction - direction we are switching
     * (for example switching to the workspace below has a DOWN direction).
     * @inheritparams #_showSwitchWorkspace
     * @see #_switchWorkspaceDone
     */
    _switchWorkspace : function(shellwm, from, to, direction) {
        if (!this._shouldAnimate()) {
            shellwm.completed_switch_workspace();
            return;
        }

        let windows = global.get_window_actors();

        /* @direction is the direction that the "camera" moves, so the
         * screen contents have to move one screen's worth in the
         * opposite direction.
         */
        let xDest = 0, yDest = 0;

        if (direction == Meta.MotionDirection.UP ||
            direction == Meta.MotionDirection.UP_LEFT ||
            direction == Meta.MotionDirection.UP_RIGHT)
                yDest = global.screen_height;
        else if (direction == Meta.MotionDirection.DOWN ||
            direction == Meta.MotionDirection.DOWN_LEFT ||
            direction == Meta.MotionDirection.DOWN_RIGHT)
                yDest = -global.screen_height;

        if (direction == Meta.MotionDirection.LEFT ||
            direction == Meta.MotionDirection.UP_LEFT ||
            direction == Meta.MotionDirection.DOWN_LEFT)
                xDest = global.screen_width;
        else if (direction == Meta.MotionDirection.RIGHT ||
                 direction == Meta.MotionDirection.UP_RIGHT ||
                 direction == Meta.MotionDirection.DOWN_RIGHT)
                xDest = -global.screen_width;

        let switchData = {};
        this._switchData = switchData;
        switchData.inGroup = new Clutter.Group();
        switchData.outGroup = new Clutter.Group();
        switchData.windows = [];

        let wgroup = global.window_group;
        wgroup.add_actor(switchData.inGroup);
        wgroup.add_actor(switchData.outGroup);

        for (let i = 0; i < windows.length; i++) {
            let window = windows[i];

            if (!window.meta_window.showing_on_its_workspace())
                continue;

            if (window.get_workspace() == from) {
                switchData.windows.push({ window: window,
                                          parent: window.get_parent() });
                window.reparent(switchData.outGroup);
            } else if (window.get_workspace() == to) {
                switchData.windows.push({ window: window,
                                          parent: window.get_parent() });
                window.reparent(switchData.inGroup);
                window.show_all();
            }
        }

        switchData.inGroup.set_position(-xDest, -yDest);
        switchData.inGroup.raise_top();

        Tweener.addTween(switchData.outGroup,
                         { x: xDest,
                           y: yDest,
                           time: WINDOW_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                           onComplete: this._switchWorkspaceDone,
                           onCompleteScope: this,
                           onCompleteParams: [shellwm]
                         });
        Tweener.addTween(switchData.inGroup,
                         { x: 0,
                           y: 0,
                           time: WINDOW_ANIMATION_TIME,
                           transition: 'easeOutQuad'
                         });
    },

    /** Callback when the switching workspaces animation (in
     * {@link #_switchWorkspace}) is complete.
     *
     * We do a bit of resyncing of animated actors (for example if a window
     * actor was animated sliding in or out but the underlying window was
     * closed during the animation, we remove the actor).
     *
     * We call `shellwm.completed_switch_workspace` to tell the window
     * manager that we are finished.
     * @inheritparams #_showSwitchWorkspace
     * @see #_switchWorkspace
     */
    _switchWorkspaceDone : function(shellwm) {
        let switchData = this._switchData;
        if (!switchData)
            return;
        this._switchData = null;

        for (let i = 0; i < switchData.windows.length; i++) {
                let w = switchData.windows[i];
                if (w.window.is_destroyed()) // Window gone
                    continue;
                if (w.window.get_parent() == switchData.outGroup) {
                    w.window.reparent(w.parent);
                    w.window.hide();
                } else
                    w.window.reparent(w.parent);
        }
        Tweener.removeTweens(switchData.inGroup);
        Tweener.removeTweens(switchData.outGroup);
        switchData.inGroup.destroy();
        switchData.outGroup.destroy();

        shellwm.completed_switch_workspace();
    },

    /** Start the alt tab popup in response to the 'switch_windows',
     * 'switch_group', 'switch_windows_backward' and 'switch_group_backward'
     * keybindings.
     *
     * We create a new [alt tab popup]{@link AltTab.AltTabPopup} and show it.
     * It is *not* stored (unlike the workspace switcher popup in
     * {@link #_workspaceSwitcherPopup}), but instead re-created every
     * time it is needed.
     *
     * @see AltTab.AltTabPopup#show
     * @inheritparams #_showSwitchWorkspace
     */
    _startAppSwitcher : function(shellwm, binding, mask, window, backwards) {
        /* prevent a corner case where both popups show up at once */
        if (this._workspaceSwitcherPopup != null)
            this._workspaceSwitcherPopup.actor.hide();

        let tabPopup = new AltTab.AltTabPopup();

        if (!tabPopup.show(backwards, binding, mask))
            tabPopup.destroy();
    },

    /** Start the ctrl alt tab switcher (we feed in parameters `backwards`
     * and `mask`) in response to the 'switch_panels' keybinding.
     * @inheritparams #_showSwitchWorkspace
     * @see CtrlAltTab.CtrlAltTabManager#popup
     */
    _startA11ySwitcher : function(shellwm, binding, mask, window, backwards) {
        Main.ctrlAltTabManager.popup(backwards, mask);
    },

    /** Callback for the 'switch_to_workspace_{up, down, left, right}' keybindings
     * (Ctrl+Alt+[arrow key] for me).
     *
     * If there's only one workspace we do nothing.
     *
     * If {@link #_workspaceSwitcherPopup} has not yet been created, we
     * make a {@link WorkspaceSwitcherPopup.WorkspaceSwitcherPopup} and store
     * it there.
     *
     * If the keybinding was to switch to the workspace up or down, we perform
     * the appropriate action ({@link #actionMoveWorkspaceUp} or
     * {@link #actionMoveWorkspaceDown}).
     *
     * If it was to switch workspaces left and right we don't do anything,
     * because (according to comments) that could be considered confusing.
     * @inheritparams #_minimizeWindow
     * @param {string} binding - the name of the binding (e.g.
     * 'switch_to_workspace_up')
     * @param {int} mask - the (key) modifier mask that were pressed when the
     * keybinding was detected (I think you can compare these against
     * Clutter.ModifierType)
     * @param {?Meta.Window} window - the window that the keybinding is for (if relevant,
     * for example a move-window-to-workspace- binding).
     * @param {boolean} backwards - whether the keybinding was activated in the
     * backwards direction (e.g. Alt+Tab cycles windows forwards;
     * Shift+Alt+Tab cycles them backwards).
     */
    _showWorkspaceSwitcher : function(shellwm, binding, mask, window, backwards) {
        if (global.screen.n_workspaces == 1)
            return;

        if (this._workspaceSwitcherPopup == null)
            this._workspaceSwitcherPopup = new WorkspaceSwitcherPopup.WorkspaceSwitcherPopup();

        if (binding == 'switch_to_workspace_up')
            this.actionMoveWorkspaceUp();
        else if (binding == 'switch_to_workspace_down')
            this.actionMoveWorkspaceDown();
        // left/right would effectively act as synonyms for up/down if we enabled them;
        // but that could be considered confusing.
        // else if (binding == 'switch_to_workspace_left')
        //   this.actionMoveWorkspaceLeft();
        // else if (binding == 'switch_to_workspace_right')
        //   this.actionMoveWorkspaceRight();
    },

    /** Moves to an adjacent workspace (up if left-to-right text direction,
     * down if right-to-left) and displays the workspace switcher popup.
     *
     * That is, in a left to right text system, this is the same as
     * {@link #actionMoveWorkspaceUp} (recall that workspaces are only
     * arranged in one vertical column in gnome-shell).
     * @see WorkspaceSwitcherPopup.WorkspaceSwitcherPopup
     */
    actionMoveWorkspaceLeft: function() {
        let rtl = (St.Widget.get_default_direction() == St.TextDirection.RTL);
        let activeWorkspaceIndex = global.screen.get_active_workspace_index();
        let indexToActivate = activeWorkspaceIndex;
        if (rtl && activeWorkspaceIndex < global.screen.n_workspaces - 1)
            indexToActivate++;
        else if (!rtl && activeWorkspaceIndex > 0)
            indexToActivate--;

        if (indexToActivate != activeWorkspaceIndex)
            global.screen.get_workspace_by_index(indexToActivate).activate(global.get_current_time());

        if (!Main.overview.visible)
            this._workspaceSwitcherPopup.display(WorkspaceSwitcherPopup.UP, indexToActivate);
    },

    /** Moves to an adjacent workspace (down if left-to-right text direction,
     * up if right-to-left) and displays the workspace switcher popup.
     *
     * That is, in a left to right text system, this is the same as
     * {@link #actionMoveWorkspaceDown} (recall that workspaces are only
     * arranged in one vertical column in gnome-shell).
     * @see WorkspaceSwitcherPopup.WorkspaceSwitcherPopup
     */
    actionMoveWorkspaceRight: function() {
        let rtl = (St.Widget.get_default_direction() == St.TextDirection.RTL);
        let activeWorkspaceIndex = global.screen.get_active_workspace_index();
        let indexToActivate = activeWorkspaceIndex;
        if (rtl && activeWorkspaceIndex > 0)
            indexToActivate--;
        else if (!rtl && activeWorkspaceIndex < global.screen.n_workspaces - 1)
            indexToActivate++;

        if (indexToActivate != activeWorkspaceIndex)
            global.screen.get_workspace_by_index(indexToActivate).activate(global.get_current_time());

        if (!Main.overview.visible)
            this._workspaceSwitcherPopup.display(WorkspaceSwitcherPopup.DOWN, indexToActivate);
    },

    /** Moves to the previous workspace (up) and displays the workspace switcher
     * popup.
     * @see WorkspaceSwitcherPopup.WorkspaceSwitcherPopup
     */
    actionMoveWorkspaceUp: function() {
        let activeWorkspaceIndex = global.screen.get_active_workspace_index();
        let indexToActivate = activeWorkspaceIndex;
        if (activeWorkspaceIndex > 0)
            indexToActivate--;

        if (indexToActivate != activeWorkspaceIndex)
            global.screen.get_workspace_by_index(indexToActivate).activate(global.get_current_time());

        if (!Main.overview.visible)
            this._workspaceSwitcherPopup.display(WorkspaceSwitcherPopup.UP, indexToActivate);
    },

    /** Moves to the next workspace (down) and displays the workspace switcher
     * popup.
     * @see WorkspaceSwitcherPopup.WorkspaceSwitcherPopup
     */
    actionMoveWorkspaceDown: function() {
        let activeWorkspaceIndex = global.screen.get_active_workspace_index();
        let indexToActivate = activeWorkspaceIndex;
        if (activeWorkspaceIndex < global.screen.n_workspaces - 1)
            indexToActivate++;

        if (indexToActivate != activeWorkspaceIndex)
            global.screen.get_workspace_by_index(indexToActivate).activate(global.get_current_time());

        if (!Main.overview.visible)
            this._workspaceSwitcherPopup.display(WorkspaceSwitcherPopup.DOWN, indexToActivate);
    }
};
