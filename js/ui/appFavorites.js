// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * This file manages app favourites in the dash (left hand sidebar in the
 * overview).
 */

const Shell = imports.gi.Shell;
const Lang = imports.lang;
const Signals = imports.signals;

const Main = imports.ui.main;

/** creates a new AppFavorites
 * @classdesc This is a class that stores all the user's favourite apps (that
 * get displayed on the dash in the left-hand sidebar of the overview).
 *
 * It doesn't do the displaying of the app icons on the dash (for that see the
 * {@link Dash.Dash} class), but it ensures that the list of app favourites
 * remains in sink with gsettings key 'org.gnome.shell.favorite-apps', provides
 * methods to add/remove from the favourites, and also shows a notification
 * in the overview every time the favourites are updated (see
 * {@link Overview.Overview#setMessage}).
 *
 * There should only be one instance of this; to get the global instance use
 * {@link getAppFavorites}.
 * @class
 */
function AppFavorites() {
    this._init();
}

AppFavorites.prototype = {
    /** the key for the user's favourite apps under gsettings path
     * 'org.gnome.shell'
     * @type {string} */
    FAVORITE_APPS_KEY: 'favorite-apps',

    _init: function() {
        /** the internal list of app favourites (should be kept in sync with
         * the gsettings key). 
         * Object mapping app id to its Shell.App.
         * 
         * Do not access this item directly; instead use
         * {@link #getFavoriteMap}.
         * @type {Object.<string, Shell.App>}
         * */
        this._favorites = {};
        global.settings.connect('changed::' + this.FAVORITE_APPS_KEY, Lang.bind(this, this._onFavsChanged));
        this._reload();
    },

    /** callback when the favourites (from gsettings) change. This ensures that
     * the internal list of favourites ({@link #_favorites}) is kept
     * in sync with the gsettings.
     */
    _onFavsChanged: function() {
        this._reload();
        this.emit('changed');
    },

    /** Repopulates the internal list of favourites
     * ({@link #_favorites}) from the gsettings key. */
    _reload: function() {
        let ids = global.settings.get_strv(this.FAVORITE_APPS_KEY);
        let appSys = Shell.AppSystem.get_default();
        let apps = ids.map(function (id) {
                return appSys.lookup_app(id);
            }).filter(function (app) {
                return app != null;
            });
        this._favorites = {};
        for (let i = 0; i < apps.length; i++) {
            let app = apps[i];
            this._favorites[app.get_id()] = app;
        }
    },

    /** Returns the app IDs of all the favourites
     * @returns {string[]} app IDs of all the favourites */
    _getIds: function() {
        let ret = [];
        for (let id in this._favorites)
            ret.push(id);
        return ret;
    },

    /** Returns the map of app ID to Shell.App. i.e.
     * {@link #_favorites}
     * @returns {Object.<string, Shell.App>} */
    getFavoriteMap: function() {
        return this._favorites;
    },

    /** Returns the Shell.App for all the favourites.
     * @returns {Shell.App[]} all the user's favourites as `Shell.App`s
     */
    getFavorites: function() {
        let ret = [];
        for (let id in this._favorites)
            ret.push(this._favorites[id]);
        return ret;
    },

    /** Returns whether an app (id) is a favourite.
     * @param {string} appId - the ID of the app we are querying
     * @returns {boolean} whether the app is in the user's favourites.
     */
    isFavorite: function(appId) {
        return appId in this._favorites;
    },

    /** internal function to add a favourite at a particular position.
     * Users should use {@link #addFavoriteAtPos} or 
     * {@link #addFavorite} which additionally pops up a
     * notification to the user on success.
     * @param {string} appId - id of app to add to favourites.
     * @param {number} pos - index at which to add the app to the favourites.
     * @returns {boolean} - true if it succeeded, false otherwise (the app
     * is already in the favourites, or the app could not be found).
     */
    _addFavorite: function(appId, pos) {
        if (appId in this._favorites)
            return false;

        let app = Shell.AppSystem.get_default().lookup_app(appId);

        if (!app)
            return false;

        let ids = this._getIds();
        if (pos == -1)
            ids.push(appId);
        else
            ids.splice(pos, 0, appId);
        global.settings.set_strv(this.FAVORITE_APPS_KEY, ids);
        this._favorites[appId] = app;
        return true;
    },

    /** Adds an app to the user's favourites at a particular position (-1 to
     * add to the end).
     *
     * This also uses {@link Overview.Overview#setMessage} to add a popup
     * notification to the message tray notifying the user that the app has been
     * successfully added.
     *
     * Internally calls {@link #_addFavorite}.
     * @param {string} appId - ID of app to add.
     * @param {number} pos - index at which to add the app into the favourites
     * (use -1 to add it to the end).
     */
    addFavoriteAtPos: function(appId, pos) {
        if (!this._addFavorite(appId, pos))
            return;

        let app = Shell.AppSystem.get_default().lookup_app(appId);

        Main.overview.setMessage(_("%s has been added to your favorites.").format(app.get_name()), Lang.bind(this, function () {
            this._removeFavorite(appId);
        }));
    },

    /** Adds an app to the favourites (appends it to the end of the list).
     * 
     * This also uses {@link Overview.Overview#setMessage} to add a popup
     * notification to the message tray notifying the user that the app has been
     * successfully added (with an undo button to remove it).
     *
     * Internally calls {@link #addFavoriteAtPos} with position -1.
     * @param {string} appId - ID of the app to add.
     */
    addFavorite: function(appId) {
        this.addFavoriteAtPos(appId, -1);
    },

    /** Moves an app in the favourites to a particular position within the
     * favourites.
     *
     * This is equivalent to calling:
     *     
     *     this._removeFavorite(appId);
     *     this._addFavorite(appId, pos);
     *
     * @param {string} appId - app ID to move.
     * @param {number} pos - new position to move the app to.
     */
    moveFavoriteToPos: function(appId, pos) {
        this._removeFavorite(appId);
        this._addFavorite(appId, pos);
    },

    /** Internal method to remove an app from the favourites. Users should use
     * {@link #removeFavorite} which additionally notifies the user
     * about the change.
     * @param {string} appId - the app ID to remove.
     * @returns {boolean} true if the app was removed, false if it was not in
     * the favourites to begin with.
     */
    _removeFavorite: function(appId) {
        if (!appId in this._favorites)
            return false;

        let ids = this._getIds().filter(function (id) { return id != appId; });
        global.settings.set_strv(this.FAVORITE_APPS_KEY, ids);
        return true;
    },

    /** Removes an app from the favourites.
     *
     * 
     * This also uses {@link Overview.Overview#setMessage} to add a popup
     * notification to the message tray notifying the user that the app has been
     * successfully removed (with an undo button to re-add it).
     *
     * Internally calls {@link #_removeFavorite}.
     * @param {string} appId - the app ID to remove.
     */
    removeFavorite: function(appId) {
        let ids = this._getIds();
        let pos = ids.indexOf(appId);

        let app = this._favorites[appId];
        if (!this._removeFavorite(appId))
            return;

        Main.overview.setMessage(_("%s has been removed from your favorites.").format(app.get_name()),
                                 Lang.bind(this, function () {
            this._addFavorite(appId, pos);
        }));
    }
};
Signals.addSignalMethods(AppFavorites.prototype);

var appFavoritesInstance = null;
/**
 * Gets a global instance of the app favourites. Use this function to get an
 * instance of {@link AppFavorites} that is shared between all Shell objects.
 *
 * This is used in (for example) {@link Dash.RemoveFavoriteIcon} - when the user
 * drags an app over the rubbish bin in the dash, the app is removed from
 * the favourites.
 * @returns {AppFavorites.AppFavorites}
 */
function getAppFavorites() {
    if (appFavoritesInstance == null)
        appFavoritesInstance = new AppFavorites();
    return appFavoritesInstance;
}
