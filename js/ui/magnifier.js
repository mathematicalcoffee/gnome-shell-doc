// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Shell magnifier (for accessibility): classes that actually implement the
 * magnification: the [Magnifier]{@link Magnifier}, [Crosshairs]{@link Crosshairs},
 * and [ZoomRegion]{@link ZoomRegion}.
 * See [Magnifier specification](https://live.gnome.org/GnomeShell/Magnification)).
 *
 * ![Crosshairs and the gnome-shell magnifier](pics/magnifier.png)
 *
 * The DBus wrappers that essentially export these objects are in
 * {@link namespace:MagnifierDBus}.
 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;

const Main = imports.ui.main;
const MagnifierDBus = imports.ui.magnifierDBus;
const Params = imports.misc.params;

/** Enum for mouse tracking mode (org.gnome.desktop.a11y.magnifier.mouse-tracking).
 * Determines the position of the magnified mouse image within the magnified
 * view and how it reacts to system mouse movement. 
 * @enum
 * @const */
const MouseTrackingMode = {
    /** No mouse tracking */
    NONE: 0,
    /** The mouse image is displayed at the center of the zoom region
     * (which also represents the point under the system mouse) and the
     * magnified contents are scrolled as the system mouse moves */
    CENTERED: 1,
    /* The position of the magnified mouse in the zoom region is proportionally
     * the same as the position of the system mouse on screen */
    PROPORTIONAL: 2,
    /** When the magnified mouse intersects a boundary of the zoom region,
     * the contents are scrolled into view. */
    PUSH: 3
};

/** Enum for screen position (org.gnome.desktop.a11y.magnifier.screen-position).
 * Where the magnified view is.
 * @enum
 * @const */
const ScreenPosition = {
    /** No magnified view */
    NONE: 0,
    /** Magnified view fills the entire screen */
    FULL_SCREEN: 1,
    /** Magnified view occupies the top half of the screen */
    TOP_HALF: 2,
    /** Magnified view occupies the bottom half of the screen */
    BOTTOM_HALF: 3,
    /** Magnified view occupies the left half of the screen */
    LEFT_HALF: 4,
    /** Magnified view occupies the right half of the screen */
    RIGHT_HALF: 5
};

/** @+
 * @const
 * @default */
/** How frequently to poll mouse position (ms) */
const MOUSE_POLL_FREQUENCY = 50;
/** Size of the box around the cursor that we clip the crosshairs at */
const CROSSHAIRS_CLIP_SIZE = [100, 100];

/** @+
 * @type {string} */
// Settings
/** Gsettings a11y schema */
const APPLICATIONS_SCHEMA       = 'org.gnome.desktop.a11y.applications';
/** GSettings key - whether to show the screen magnifier */
const SHOW_KEY                  = 'screen-magnifier-enabled';

/** Gsettings magnifier schema */
const MAGNIFIER_SCHEMA          = 'org.gnome.desktop.a11y.magnifier';
/** Gsettings key - whether the magnified view fills the entire screen,
 * top half, bottom half, left half, or right half of the screen.
 * @see ScreenPosition */
const SCREEN_POSITION_KEY       = 'screen-position';
/** Gsettings key - magnification factor. */
const MAG_FACTOR_KEY            = 'mag-factor';
/** Gsettings key - whether magnified view is centered over the mouse and moves with it */
const LENS_MODE_KEY             = 'lens-mode';
/** Gsettings key - when the pointer's at the edge of the screen and we have
 * centred mouse tracking, whether the magnified contents continue to scroll such that
 * the screen edge moves into the magnified view. */
const CLAMP_MODE_KEY            = 'scroll-at-edges';
/** Gsettings key - How the zoom region follows the mouse (or if it does)
 * @see MouseTrackingMode */
const MOUSE_TRACKING_KEY        = 'mouse-tracking';
/** gsettings key - show the crosshairs? */
const SHOW_CROSS_HAIRS_KEY      = 'show-cross-hairs';
/** gsettings key - crosshair thickness */
const CROSS_HAIRS_THICKNESS_KEY = 'cross-hairs-thickness';
/** gsettings key - crosshair colour */
const CROSS_HAIRS_COLOR_KEY     = 'cross-hairs-color';
/** gsettings key - crosshair opacity */
const CROSS_HAIRS_OPACITY_KEY   = 'cross-hairs-opacity';
/** gsettings key - crosshair length */
const CROSS_HAIRS_LENGTH_KEY    = 'cross-hairs-length';
/** gsettings key - clip the crosshairs around the pointer? */
const CROSS_HAIRS_CLIP_KEY      = 'cross-hairs-clip';
/** @- */
/** @- */

/** Global instance of the Magnifier DBus service.
 * @type {MagnifierDBus.ShellMagnifier} */
let magDBusService = null;

/** creates a new Magnifier.
 *
 * The constructor creates a {@link MagnifierDBus.ShellMagnifier} and stores
 * it in {@link magDBusService}.
 * @classdesc
 * A Magnifier is a manager of {@link ZoomRegion}s.
 *
 * This implements an accessibility Magnifier - the ability to magnify
 * bits of the screen. It makes sure the gsettings keys defined in this
 * module (e.g. {@link CROSS_HAIRS_CLIP_KEY}) are all obeyed when the magnifier
 * is turned on.
 *
 * Only make one of these. Stored as {@link Main.magnifier}.
 *
 * It also has a DBus wrapper {@link MagnifierDBus.ShellMagnifier}.
 * @todo screenshots with various settings (clip, width, length, colour, ...)
 * @class
 */
function Magnifier() {
    this._init();
}

Magnifier.prototype = {
    _init: function() {
        // Magnifier is a manager of ZoomRegions.
        this._zoomRegions = [];

        // Create small clutter tree for the magnified mouse.
        let xfixesCursor = Shell.XFixesCursor.get_default();
        this._mouseSprite = new Clutter.Texture();
        xfixesCursor.update_texture_image(this._mouseSprite);
        this._cursorRoot = new Clutter.Group();
        this._cursorRoot.add_actor(this._mouseSprite);

        // Create the first ZoomRegion and initialize it according to the
        // magnification settings.

        let mask;
        [this.xMouse, this.yMouse, mask] = global.get_pointer();

        let aZoomRegion = new ZoomRegion(this, this._cursorRoot);
        this._zoomRegions.push(aZoomRegion);
        let showAtLaunch = this._settingsInit(aZoomRegion);
        aZoomRegion.scrollContentsTo(this.xMouse, this.yMouse);

        xfixesCursor.connect('cursor-change', Lang.bind(this, this._updateMouseSprite));
        this._xfixesCursor = xfixesCursor;

        // Export to dbus.
        magDBusService = new MagnifierDBus.ShellMagnifier();
        this.setActive(showAtLaunch);
    },

    /**
     * Show the system mouse pointer.
     */
    showSystemCursor: function() {
        this._xfixesCursor.show();
    },

    /**
     * Hide the system mouse pointer.
     */
    hideSystemCursor: function() {
        this._xfixesCursor.hide();
    },

    /** Activate or deactivate the magnifier and emits a signal.
     *  @inheritparams MagnifierDBus.ShellMagnifier#setActive
     *  @fires .active-changed
     */
    setActive: function(activate) {
        this._zoomRegions.forEach (function(zoomRegion, index, array) {
            zoomRegion.setActive(activate);
        });

        if (activate)
            this.startTrackingMouse();
        else
            this.stopTrackingMouse();

        // Make sure system mouse pointer is shown when all zoom regions are
        // invisible.
        if (!activate)
            this._xfixesCursor.show();

        // Notify interested parties of this change
        this.emit('active-changed', activate);
    },

    // Whether the magnifier is active (boolean).
    /** @inheritdoc MagnifierDBus.ShellMagnifier#isActive */
    isActive: function() {
        // Sufficient to check one ZoomRegion since Magnifier's active
        // state applies to all of them.
        if (this._zoomRegions.length == 0)
            return false;
        else
            return this._zoomRegions[0].isActive();
    },

    /**
     * Turn on mouse tracking, if not already doing so.
     */
    startTrackingMouse: function() {
        if (!this._mouseTrackingId)
            this._mouseTrackingId = Mainloop.timeout_add(
                MOUSE_POLL_FREQUENCY,
                Lang.bind(this, this.scrollToMousePos)
            );
    },

    /**
     * Turn off mouse tracking, if already doing so.
     */
    stopTrackingMouse: function() {
        if (this._mouseTrackingId)
            Mainloop.source_remove(this._mouseTrackingId);

        this._mouseTrackingId = null;
    },

    /**
     * Is the magnifier tracking the mouse currently?
     * @returns {boolean} whether the magnifier is currently tracking the mouse.
     */
    isTrackingMouse: function() {
        return !!this._mouseTrackingId;
    },

    /**
     * Position all zoom regions' ROI relative to the current location of the
     * system pointer.
     * @returns {boolean} true
     */
    scrollToMousePos: function() {
        let [xMouse, yMouse, mask] = global.get_pointer();

        if (xMouse != this.xMouse || yMouse != this.yMouse) {
            this.xMouse = xMouse;
            this.yMouse = yMouse;

            let sysMouseOverAny = false;
            this._zoomRegions.forEach(function(zoomRegion, index, array) {
                if (zoomRegion.scrollToMousePos())
                    sysMouseOverAny = true;
            });
            if (sysMouseOverAny)
                this.hideSystemCursor();
            else
                this.showSystemCursor();
        }
        return true;
    },

    /**
     * Create a ZoomRegion instance with the given properties.
     * @inheritparams ZoomRegion#getMagFactor
     * @inheritparams ZoomRegion#setViewPort
     * @inheritparams ZoomRegion#setROI
     *
     * @returns {Magnifier.ZoomRegion} The newly created ZoomRegion.
     */
    createZoomRegion: function(xMagFactor, yMagFactor, roi, viewPort) {
        let zoomRegion = new ZoomRegion(this, this._cursorRoot);
        zoomRegion.setViewPort(viewPort);

        // We ignore the redundant width/height on the ROI
        let fixedROI = new Object(roi);
        fixedROI.width = viewPort.width / xMagFactor;
        fixedROI.height = viewPort.height / yMagFactor;
        zoomRegion.setROI(fixedROI);

        zoomRegion.addCrosshairs(this._crossHairs);
        return zoomRegion;
    },

    /**
     * Append the given ZoomRegion to the list of currently defined ZoomRegions
     * for this Magnifier instance.
     * @param {Magnifier.ZoomRegion} zoomRegion - The zoomRegion to add.
     */
    addZoomRegion: function(zoomRegion) {
        if(zoomRegion) {
            this._zoomRegions.push(zoomRegion);
            if (!this.isTrackingMouse())
                this.startTrackingMouse();
        }
    },

    /**
     * Return a list of ZoomRegion's for this Magnifier.
     * @returns {Magnifier.ZoomRegion[]} The Magnifier's zoom region list (array).
     */
    getZoomRegions: function() {
        return this._zoomRegions;
    },

    /**
     * Remove all the zoom regions from this Magnfier's ZoomRegion list.
     */
    clearAllZoomRegions: function() {
        for (let i = 0; i < this._zoomRegions.length; i++)
            this._zoomRegions[i].setActive(false);

        this._zoomRegions.length = 0;
        this.stopTrackingMouse();
        this.showSystemCursor();
    },

    /**
     * Add and show a cross hair centered on the magnified mouse, respecting
     * the gsettings properties {@link CROSS_HAIRS_THICKNESS_KEY},
     * {@link CROSS_HAIRS_COLOR_KEY}, {@link CROSS_HAIRS_OPACITY_KEY},
     * {@link CROSS_HAIRS_CLIP_KEY}, and {@link CROSS_HAIRS_LENGTH_KEY}.
     * @see Crosshairs
     */
    addCrosshairs: function() {
        if (!this._crossHairs)
            this._crossHairs = new Crosshairs();

        let thickness = this._settings.get_int(CROSS_HAIRS_THICKNESS_KEY);
        let color = this._settings.get_string(CROSS_HAIRS_COLOR_KEY);
        let opacity = this._settings.get_double(CROSS_HAIRS_OPACITY_KEY);
        let length = this._settings.get_int(CROSS_HAIRS_LENGTH_KEY);
        let clip = this._settings.get_boolean(CROSS_HAIRS_CLIP_KEY);

        this.setCrosshairsThickness(thickness);
        this.setCrosshairsColor(color);
        this.setCrosshairsOpacity(opacity);
        this.setCrosshairsLength(length);
        this.setCrosshairsClip(clip);

        let theCrossHairs = this._crossHairs;
        this._zoomRegions.forEach (function(zoomRegion, index, array) {
            zoomRegion.addCrosshairs(theCrossHairs);
        });
    },

    /**
     * Show or hide the cross hair.
     * @param {boolean} visible - Flag that indicates show (true) or hide (false).
     * @see #addCrosshairs
     */
    setCrosshairsVisible: function(visible) {
        if (visible) {
            if (!this._crossHairs)
                this.addCrosshairs();
            this._crossHairs.show();
        }
        else {
            if (this._crossHairs)
                this._crossHairs.hide();
        }
    },

    /**
     * Set the color of the crosshairs for all ZoomRegions.
     * @param {string} color - The color as a string, e.g. '#ff0000ff' or 'red'.
     * @see Crosshairs#setColor
     */
    setCrosshairsColor: function(color) {
        if (this._crossHairs) {
            let clutterColor = new Clutter.Color();
            clutterColor.from_string(color);
            this._crossHairs.setColor(clutterColor);
        }
    },

    /**
     * Get the color of the crosshairs.
     * @returns {string} The color as a string, e.g. '#0000ffff' or 'blue'.
     * @see Crosshairs#getColor
     */
    getCrosshairsColor: function() {
        if (this._crossHairs) {
            let clutterColor = this._crossHairs.getColor();
            return clutterColor.to_string();
        }
        else
            return '#00000000';
    },

    /**
     * Set the crosshairs thickness for all ZoomRegions.
     * @param {number} thickness - The width of the vertical and horizontal lines of the
     *              crosshairs.
     * @see Crosshairs#setThickness
     */
    setCrosshairsThickness: function(thickness) {
        if (this._crossHairs)
            this._crossHairs.setThickness(thickness);
    },

    /**
     * Get the crosshairs thickness.
     * @returns {number} The width of the vertical and horizontal lines of the
     *          crosshairs.
     * @see Crosshairs#getThickness
     */
    getCrosshairsThickness: function() {
        if (this._crossHairs)
            return this._crossHairs.getThickness();
        else
            return 0;
    },

    /**
     * Set the crosshair opacity for all zoom regions.
     * @param {number} opacity - Value between 0.0 (transparent) and 1.0 (fully opaque).
     * @see Crosshairs#setOpacity
     */
    setCrosshairsOpacity: function(opacity) {
        if (this._crossHairs)
            this._crossHairs.setOpacity(opacity * 255);
    },

    /**
     * Get the crosshair opacity.
     * @returns {number} Value between 0.0 (transparent) and 1.0 (fully opaque).
     * @see Crosshairs#getOpacity
     */
    getCrosshairsOpacity: function() {
        if (this._crossHairs)
            return this._crossHairs.getOpacity() / 255.0;
        else
            return 0.0;
    },

    /**
     * Set the crosshairs length for all ZoomRegions.
     * @param {number} length - The length of the vertical and horizontal lines making up the
     *          crosshairs.
     * @see Crosshairs#setLength
     */
    setCrosshairsLength: function(length) {
        if (this._crossHairs)
            this._crossHairs.setLength(length);
    },

    /**
     * Get the crosshairs length.
     * @returns {number} The length of the vertical and horizontal lines making up the
     *          crosshairs.
     * @see Crosshairs#getLength
     */
    getCrosshairsLength: function() {
        if (this._crossHairs)
            return this._crossHairs.getLength();
        else
            return 0;
    },

    /**
     * Set whether the crosshairs are clipped at their intersection.
     * Uses clip size {@link CROSSHAIRS_CLIP_SIZE}.
     * @param {boolean} clip - whether to clip the crosshairs.
     * @see Crosshairs#setClip
     */
    setCrosshairsClip: function(clip) {
        if (clip) {
            if (this._crossHairs)
                this._crossHairs.setClip(CROSSHAIRS_CLIP_SIZE);
        }
        else {
            // Setting no clipping on crosshairs means a zero sized clip
            // rectangle.
            if (this._crossHairs)
                this._crossHairs.setClip([0, 0]);
        }
    },

    /**
     * Get whether the crosshairs are clipped by the mouse image.
     * @returns {boolean} Whether the crosshairs are clipped.
     * @see Crosshairs#getClip
     */
     getCrosshairsClip: function() {
        if (this._crossHairs) {
            let [clipWidth, clipHeight] = this._crossHairs.getClip();
            return (clipWidth > 0 && clipHeight > 0);
        }
        else
            return false;
     },

    //// Private methods ////

    /** Updates the internal cursor picture to reflect the actual cursor.
     * (This class stores a local version of the picture of the mouse to
     * draw on the zoom region(s)). */
    _updateMouseSprite: function() {
        this._xfixesCursor.update_texture_image(this._mouseSprite);
        let xHot = this._xfixesCursor.get_hot_x();
        let yHot = this._xfixesCursor.get_hot_y();
        this._mouseSprite.set_anchor_point(xHot, yHot);
    },

    /** Initialises local gsettings storage ({@link APPLICATIONS_SCHEMA},
     * {@link MAGNIFIER_SCHEMA}) and makes sure our settings are in sync
     * with these. Also monitors all the settings for changes in order to
     * update ourselves according to them.
     *
     * Called from the constructor to set things up.
     * @param {?Magnifier.ZoomRegion} zoomRegion - zoom region to apply settings
     * to (in particular {@link MAG_FACTOR_KEY}, {@link SCREEN_POSITION_KEY},
     * {@link LENS_MODE_KEY}, {@link CLAMP_MODE_KEY}, {@link CLAMP_MODE_KEY},
     * {@link MOUSE_TRACKING_KEY}. If not supplied just the non-zoom-region-specific
     * settings will be initialised (crosshair colour, thickness, ...)
     * @returns {boolean} whether the magnifier should be visible.
     */
    _settingsInit: function(zoomRegion) {
        this._appSettings = new Gio.Settings({ schema: APPLICATIONS_SCHEMA });
        this._settings = new Gio.Settings({ schema: MAGNIFIER_SCHEMA });

        if (zoomRegion) {
            // Mag factor is accurate to two decimal places.
            let aPref = parseFloat(this._settings.get_double(MAG_FACTOR_KEY).toFixed(2));
            if (aPref != 0.0)
                zoomRegion.setMagFactor(aPref, aPref);

            aPref = this._settings.get_enum(SCREEN_POSITION_KEY);
            if (aPref)
                zoomRegion.setScreenPosition(aPref);

            zoomRegion.setLensMode(this._settings.get_boolean(LENS_MODE_KEY));
            zoomRegion.setClampScrollingAtEdges(!this._settings.get_boolean(CLAMP_MODE_KEY));

            aPref = this._settings.get_enum(MOUSE_TRACKING_KEY);
            if (aPref)
                zoomRegion.setMouseTrackingMode(aPref);
        }

        let showCrosshairs = this._settings.get_boolean(SHOW_CROSS_HAIRS_KEY);
        this.addCrosshairs();
        this.setCrosshairsVisible(showCrosshairs);

        this._appSettings.connect('changed::' + SHOW_KEY,
                                  Lang.bind(this, function() {
            this.setActive(this._appSettings.get_boolean(SHOW_KEY));
        }));

        this._settings.connect('changed::' + SCREEN_POSITION_KEY,
                               Lang.bind(this, this._updateScreenPosition));
        this._settings.connect('changed::' + MAG_FACTOR_KEY,
                               Lang.bind(this, this._updateMagFactor));
        this._settings.connect('changed::' + LENS_MODE_KEY,
                               Lang.bind(this, this._updateLensMode));
        this._settings.connect('changed::' + CLAMP_MODE_KEY,
                               Lang.bind(this, this._updateClampMode));
        this._settings.connect('changed::' + MOUSE_TRACKING_KEY,
                               Lang.bind(this, this._updateMouseTrackingMode));

        this._settings.connect('changed::' + SHOW_CROSS_HAIRS_KEY,
                               Lang.bind(this, function() {
            this.setCrosshairsVisible(this._settings.get_boolean(SHOW_CROSS_HAIRS_KEY));
        }));

        this._settings.connect('changed::' + CROSS_HAIRS_THICKNESS_KEY,
                               Lang.bind(this, function() {
            this.setCrosshairsThickness(this._settings.get_int(CROSS_HAIRS_THICKNESS_KEY));
        }));

        this._settings.connect('changed::' + CROSS_HAIRS_COLOR_KEY,
                               Lang.bind(this, function() {
            this.setCrosshairsColor(this._settings.get_string(CROSS_HAIRS_COLOR_KEY));
        }));

        this._settings.connect('changed::' + CROSS_HAIRS_OPACITY_KEY,
                               Lang.bind(this, function() {
            this.setCrosshairsOpacity(this._settings.get_double(CROSS_HAIRS_OPACITY_KEY));
        }));

        this._settings.connect('changed::' + CROSS_HAIRS_LENGTH_KEY,
                               Lang.bind(this, function() {
            this.setCrosshairsLength(this._settings.get_int(CROSS_HAIRS_LENGTH_KEY));
        }));

        this._settings.connect('changed::' + CROSS_HAIRS_CLIP_KEY,
                               Lang.bind(this, function() {
            this.setCrosshairsClip(this._settings.get_boolean(CROSS_HAIRS_CLIP_KEY));
        }));

        return this._appSettings.get_boolean(SHOW_KEY);
   },

    /** callback when {@link SCREEN_POSITION_KEY} setting changes - updates
     * the screen position of the first zoom region.
     * @see ScreenPosition
     * @see ZoomRegion#setScreenPosition */
    _updateScreenPosition: function() {
        // Applies only to the first zoom region.
        if (this._zoomRegions.length) {
            let position = this._settings.get_enum(SCREEN_POSITION_KEY);
            this._zoomRegions[0].setScreenPosition(position);
            if (position != ScreenPosition.FULL_SCREEN)
                this._updateLensMode();
        }
    },

    /** callback when {@link MAG_FACTOR_KEY} setting changes - updates the
     * magnification factor of the first zoom region.
     * @see ZoomRegion#setMagFactor */
    _updateMagFactor: function() {
        // Applies only to the first zoom region.
        if (this._zoomRegions.length) {
            // Mag factor is accurate to two decimal places.
            let magFactor = parseFloat(this._settings.get_double(MAG_FACTOR_KEY).toFixed(2));
            this._zoomRegions[0].setMagFactor(magFactor, magFactor);
        }
    },

    /** callback when {@link LENS_MODE_KEY} setting changes - updates the
     * lens mode (i.e. whether the magnified view follows the cursor) for the
     * first zoom region.
     * @see ZoomRegion#setLensMode */
    _updateLensMode: function() {
        // Applies only to the first zoom region.
        if (this._zoomRegions.length) {
            this._zoomRegions[0].setLensMode(this._settings.get_boolean(LENS_MODE_KEY));
        }
    },

    /** callback when {@link CLAMP_MODE_KEY} setting changes - updates the
     * clamp mode (i.e. what happens at the edges of the screen) for the
     * first zoom region.
     * @see ZoomRegion#setClampScrollingAtEdges */
    _updateClampMode: function() {
        // Applies only to the first zoom region.
        if (this._zoomRegions.length) {
            this._zoomRegions[0].setClampScrollingAtEdges(
                !this._settings.get_boolean(CLAMP_MODE_KEY)
            );
        }
    },

    /** callback when {@link MOUSE_TRACKING_KEY} setting changes - updates the
     * mouse tracking mode (i.e. how the zoom region follows the mouse) for the
     * first zoom region.
     * @see MouseTrackingMode
     * @see ZoomRegion#setMouseTrackingMode */
    _updateMouseTrackingMode: function() {
        // Applies only to the first zoom region.
        if (this._zoomRegions.length) {
            this._zoomRegions[0].setMouseTrackingMode(
                this._settings.get_enum(MOUSE_TRACKING_KEY)
            );
        }
    }
};
Signals.addSignalMethods(Magnifier.prototype);
/**  Emitted when the magnifier is activated or deactivated.
 * @name active-changed
 * @memberof Magnifier
 * @parameter {Magnifier.Magnifier} o - instance emitting the signal.
 * @parameter {boolean} active - whether the magnifier is active or not.
 * @event
 */

/** creates a new ZoomRegion
 * @param {Magnifier.Magnifier} magnifier - magnifier the zoom region is for
 * @param {Clutter.Texture} mouseSourceActor - actor with the mouse picture.
 * @classdesc
 * A zoom region represents the magnified view of part of the desktop.
 *
 * This object is used by the {@link Magnifier}, and {@link MagnifierDBus.ZoomRegion}
 * wraps it to export it over DBus.
 * @class
 */
function ZoomRegion(magnifier, mouseSourceActor) {
    this._init(magnifier, mouseSourceActor);
}

ZoomRegion.prototype = {
    _init: function(magnifier, mouseSourceActor) {
        this._magnifier = magnifier;

        this._mouseTrackingMode = MouseTrackingMode.NONE;
        this._clampScrollingAtEdges = false;
        this._lensMode = false;
        this._screenPosition = ScreenPosition.FULL_SCREEN;

        this._magView = null;
        this._uiGroupClone = null;
        this._mouseSourceActor = mouseSourceActor;
        this._mouseActor  = null;
        this._crossHairs = null;
        this._crossHairsActor = null;

        this._viewPortX = 0;
        this._viewPortY = 0;
        this._viewPortWidth = global.screen_width;
        this._viewPortWidth = global.screen_height;
        this._xCenter = this._viewPortWidth / 2;
        this._yCenter = this._viewPortHeight / 2;
        this._xMagFactor = 1;
        this._yMagFactor = 1;
        this._followingCursor = false;
    },

    /**
     * Show/hide this zoom region.
     * @param {boolean} activate - whether to show/hide the ZoomRegion.
     */
    setActive: function(activate) {
        if (activate && !this.isActive()) {
            this._createActors();
            if (this._isMouseOverRegion())
                this._magnifier.hideSystemCursor();
            this._updateMagViewGeometry();
            this._updateCloneGeometry();
            this._updateMousePosition();
        } else if (!activate && this.isActive()) {
            this._destroyActors();
        }
    },

    /**
     * isActive:
     * Whether this ZoomRegion is active.
     * @returns {boolean} whether this zoom region is active.
     */
    isActive: function() {
        return this._magView != null;
    },

    /**
     * Sets the magnification factor of the zoom region, updating its ROI.
     * @param {number} xMagFactor - The power to set the horizontal magnification factor to
     *                  of the magnified view.  A value of 1.0 means no
     *                  magnification.  A value of 2.0 doubles the size.
     * @param {number} yMagFactor - The power to set the vertical magnification factor to
     *                  of the magnified view.
     */
    setMagFactor: function(xMagFactor, yMagFactor) {
        this._changeROI({ xMagFactor: xMagFactor,
                          yMagFactor: yMagFactor,
                          redoCursorTracking: this._followingCursor });
    },

    /**
     * Gets the magnification factor of the zoom region.
     * @returns {number[]} an array, `[xMagFactor, yMagFactor]`,
     *          containing the horizontal
     *          and vertical magnification powers.  A value of 1.0 means no
     *          magnification.  A value of 2.0 means the contents are doubled
     *          in size, and so on.
     */
    getMagFactor: function() {
        return [this._xMagFactor, this._yMagFactor];
    },

    /**
     * Sets the mouse tracking mode.
     * @param {Magnifier.MouseTrackingMode} mode - the input tracking mode.
     */
    setMouseTrackingMode: function(mode) {
        if (mode >= MouseTrackingMode.NONE && mode <= MouseTrackingMode.PUSH)
            this._mouseTrackingMode = mode;
    },

    /**
     * Gets the mouse tracking mode.
     * @returns {Magnifier.MouseTrackingMode} The current mouse tracking mode.
     */
    getMouseTrackingMode: function() {
        return this._mouseTrackingMode;
    },

    /**
     * Sets the position and size of the ZoomRegion on screen.
     * @param {Object} viewPort - Object in the form { x, y, width, height } that defines
     *                  the position of the ZoomRegion on screen. The values are
     *                  in stage coordinate space.
     * @param {number} viewPort.x - x coordinate of ZoomRegion on the screen.
     * @param {number} viewPort.y - y coordinate of ZoomRegion on the screen.
     * @param {number} viewPort.width - width of ZoomRegion on the screen.
     * @param {number} viewPort.height - height of ZoomRegion on the screen.
     */
    setViewPort: function(viewPort) {
        this._setViewPort(viewPort);
        this._screenPosition = ScreenPosition.NONE;
    },

    /**
     * Sets the "region of interest" that the ZoomRegion is magnifying.
     * @param {Object} roi - Object that defines the region of the screen to magnify.  It
     *          has members x, y, width, height.  The values are in
     *          screen (unmagnified) coordinate space.
     * @param {number} roi.x - x coordinate of region to magnify
     * @param {number} roi.y - y coordinate of region to magnify
     * @param {number} roi.width - width of region to magnify
     * @param {number} roi.height - height of region to magnify
     */
    setROI: function(roi) {
        if (roi.width <= 0 || roi.height <= 0)
            return;

        this._followingCursor = false;
        this._changeROI({ xMagFactor: this._viewPortWidth / roi.width,
                          yMagFactor: this._viewPortHeight / roi.height,
                          xCenter: roi.x + roi.width  / 2,
                          yCenter: roi.y + roi.height / 2 });
    },

    /**
     * Retrieves the "region of interest" -- the rectangular bounds of that part
     * of the desktop that the magnified view is showing `(x, y, width, height)`.
     * The bounds are given in non-magnified coordinates.
     * @returns {number[]}  an array, `[x, y, width, height]`, representing the bounding
     *          rectangle of what is shown in the magnified view.
     */
    getROI: function() {
        let roiWidth = this._viewPortWidth / this._xMagFactor;
        let roiHeight = this._viewPortHeight / this._yMagFactor;

        return [this._xCenter - roiWidth / 2,
                this._yCenter - roiHeight / 2,
                roiWidth, roiHeight];
    },

    /**
     * Turn lens mode on/off.  In full screen mode, lens mode does nothing since
     * a lens the size of the screen is pointless.
     * @param {boolean} lensMode - whether lens mode is on or off.
     */
    setLensMode: function(lensMode) {
        this._lensMode = lensMode;
        if (!this._lensMode)
            this.setScreenPosition (this._screenPosition);
    },

    /**
     * Is lens mode on or off?
     * @returns {boolean} The lens mode state as a boolean.
     */
    isLensMode: function() {
        return this._lensMode;
    },

    /**
     * Stop vs. allow scrolling of the magnified contents when it scroll beyond
     * the edges of the screen.
     * @param {boolean} clamp - Boolean to turn on/off clamping.
     */
    setClampScrollingAtEdges: function(clamp) {
        this._clampScrollingAtEdges = clamp;
        if (clamp)
            this._changeROI();
    },

    /** Make the magnifier view occupy the top half of the screen
     * ({@link ScreenPosition.TOP_HALF}). */
    setTopHalf: function() {
        let viewPort = {};
        viewPort.x = 0;
        viewPort.y = 0;
        viewPort.width = global.screen_width;
        viewPort.height = global.screen_height/2;
        this._setViewPort(viewPort);
        this._screenPosition = ScreenPosition.TOP_HALF;
    },

    /** Make the magnifier view occupy the bottom half of the screen
     * ({@link ScreenPosition.BOTTOM_HALF}). */
    setBottomHalf: function() {
        let viewPort = {};
        viewPort.x = 0;
        viewPort.y = global.screen_height/2;
        viewPort.width = global.screen_width;
        viewPort.height = global.screen_height/2;
        this._setViewPort(viewPort);
        this._screenPosition = ScreenPosition.BOTTOM_HALF;
    },

    /** Make the magnifier view occupy the left half of the screen
     * ({@link ScreenPosition.LEFT_HALF}). */
    setLeftHalf: function() {
        let viewPort = {};
        viewPort.x = 0;
        viewPort.y = 0;
        viewPort.width = global.screen_width/2;
        viewPort.height = global.screen_height;
        this._setViewPort(viewPort);
        this._screenPosition = ScreenPosition.LEFT_HALF;
    },

    /** Make the magnifier view occupy the right half of the screen
     * ({@link ScreenPosition.RIGHT_HALF}). */
    setRightHalf: function() {
        let viewPort = {};
        viewPort.x = global.screen_width/2;
        viewPort.y = 0;
        viewPort.width = global.screen_width/2;
        viewPort.height = global.screen_height;
        this._setViewPort(viewPort);
        this._screenPosition = ScreenPosition.RIGHT_HALF;
    },

    /** Make the magnifier view occupy the entire screen.
     * ({@link ScreenPosition.FULL_SCREEN}).
     * Note: this disallows lens mode. */
    setFullScreenMode: function() {
        let viewPort = {};
        viewPort.x = 0;
        viewPort.y = 0;
        viewPort.width = global.screen_width;
        viewPort.height = global.screen_height;
        this.setViewPort(viewPort);

        this._screenPosition = ScreenPosition.FULL_SCREEN;
    },

    /**
     * Positions the magnified view to one of the enumerated positions on the
     * screen.
     * @param {Magnifier.ScreenPosition} position - the position to display
     * the magnified view - one of `FULL_SCREEN`, `TOP_HALF`, `BOTTOM_HALF`,
     * `LEFT_HALF`, or `RIGHT_HALF`.
     */
    setScreenPosition: function(inPosition) {
        switch (inPosition) {
            case ScreenPosition.FULL_SCREEN:
                this.setFullScreenMode();
                break;
            case ScreenPosition.TOP_HALF:
                this.setTopHalf();
                break;
            case ScreenPosition.BOTTOM_HALF:
                this.setBottomHalf();
                break;
            case ScreenPosition.LEFT_HALF:
                this.setLeftHalf();
                break;
            case ScreenPosition.RIGHT_HALF:
                this.setRightHalf();
                break;
        }
    },

    /**
     * Tell the outside world what the current mode is -- magnifying the
     * top half, bottom half, etc.
     * @returns {Magnifier.ScreenPosition} the current mode.
     */
    getScreenPosition: function() {
        return this._screenPosition;
    },

    /**
     * Set the region of interest based on the position of the system pointer.
     * @returns {boolean} Whether the system mouse pointer is over the magnified view.
     */
    scrollToMousePos: function() {
        this._followingCursor = true;
        if (this._mouseTrackingMode != MouseTrackingMode.NONE)
            this._changeROI({ redoCursorTracking: true });
        else
            this._updateMousePosition();

        // Determine whether the system mouse pointer is over this zoom region.
        return this._isMouseOverRegion();
    },

    /**
     * Shift the contents of the magnified view such it is centered on the given
     * coordinate.
     * @param {number} x - The x-coord of the point to center on (stage coordinates).
     * @param {number} y - The y-coord of the point to center on (stage coordinates).
     */
    scrollContentsTo: function(x, y) {
        this._followingCursor = false;
        this._changeROI({ xCenter: x,
                          yCenter: y });
    },

    /**
     * Add crosshairs centered on the magnified mouse.
     * @param {Magnifier.Crosshairs} crossHairs - Crosshairs instance
     * @see Crosshairs#addToZoomRegion
     */
    addCrosshairs: function(crossHairs) {
        this._crossHairs = crossHairs;

        // If the crossHairs is not already within a larger container, add it
        // to this zoom region.  Otherwise, add a clone.
        if (crossHairs && this.isActive()) {
            this._crossHairsActor = crossHairs.addToZoomRegion(this, this._mouseActor);
        }
    },

    //// Private methods ////

    /** Creates the actors for this zoom region - the crosshairs, the visual
     * zoom region itself (i.e. magnified view) and adds everything to the stage.
     */
    _createActors: function() {
        // The root actor for the zoom region
        this._magView = new St.Bin({ style_class: 'magnifier-zoom-region', x_fill: true, y_fill: true });
        global.stage.add_actor(this._magView);

        // hide the magnified region from CLUTTER_PICK_ALL
        Shell.util_set_hidden_from_pick (this._magView, true);

        // Append a Clutter.Group to clip the contents of the magnified view.
        let mainGroup = new Clutter.Group({ clip_to_allocation: true });
        this._magView.set_child(mainGroup);

        // Add a background for when the magnified uiGroup is scrolled
        // out of view (don't want to see desktop showing through).
        let background = new Clutter.Rectangle({ color: Main.DEFAULT_BACKGROUND_COLOR });
        mainGroup.add_actor(background);

        // Clone the group that contains all of UI on the screen.  This is the
        // chrome, the windows, etc.
        this._uiGroupClone = new Clutter.Clone({ source: Main.uiGroup });
        mainGroup.add_actor(this._uiGroupClone);
        Main.uiGroup.set_size(global.screen_width, global.screen_height);
        background.set_size(global.screen_width, global.screen_height);

        // Add either the given mouseSourceActor to the ZoomRegion, or a clone of
        // it.
        if (this._mouseSourceActor.get_parent() != null)
            this._mouseActor = new Clutter.Clone({ source: this._mouseSourceActor });
        else
            this._mouseActor = this._mouseSourceActor;
        mainGroup.add_actor(this._mouseActor);

        if (this._crossHairs)
            this._crossHairsActor = this._crossHairs.addToZoomRegion(this, this._mouseActor);
        else
            this._crossHairsActor = null;
    },

    /** Destroys all the actors for this zoom region - the magnified view,
     * crosshairs, etc.*/
    _destroyActors: function() {
        if (this._mouseActor == this._mouseSourceActor)
            this._mouseActor.get_parent().remove_actor (this._mouseActor);
        if (this._crossHairs)
            this._crossHairs.removeFromParent(this._crossHairsActor);

        this._magView.destroy();
        this._magView = null;
        this._uiGroupClone = null;
        this._mouseActor = null;
        this._crossHairsActor = null;
    },

    /** Sets the position of the zoom region on the screen.
     * @inheritparams #setViewPort
     * @param {boolean} [fromROIUpdate=false] - whether this change is triggered
     * by a ROI update or not. If not, the ROI needs to be updated for the
     * mouse position (if `fromROIUpdate=true`, the ROI has already been updated).
     */
    _setViewPort: function(viewPort, fromROIUpdate) {
        // Sets the position of the zoom region on the screen

        let width = Math.round(Math.min(viewPort.width, global.screen_width));
        let height = Math.round(Math.min(viewPort.height, global.screen_height));
        let x = Math.max(viewPort.x, 0);
        let y = Math.max(viewPort.y, 0);

        x = Math.round(Math.min(x, global.screen_width - width));
        y = Math.round(Math.min(y, global.screen_height - height));

        this._viewPortX = x;
        this._viewPortY = y;
        this._viewPortWidth = width;
        this._viewPortHeight = height;

        this._updateMagViewGeometry();

        if (!fromROIUpdate)
            this._changeROI({ redoCursorTracking: this._followingCursor }); // will update mouse

        if (this.isActive() && this._isMouseOverRegion())
            this._magnifier.hideSystemCursor();
    },

    /**
     * Updates the area we are viewing; the magnification factors
     * and center can be set explicitly, or we can recompute
     * the position based on the mouse cursor position
     * @param {Object} [params] - parameters to calculate the new ROI
     * @param {number} [xMagFactor=this._xMagFactor] - x magnification factor
     * (default use the current factor).
     * @param {number} [yMagFactor=this._xMagFactor] - y magnification factor
     * (default use the current factor).
     * @param {number} [xCenter=this._xCenter] - x centre of the region to view
     * (default unchanged from current)
     * @param {number} [yCenter=this._yCenter] - y centre of the region to view
     * (default unchanged from current)
     * @param {boolean} [redoCursorTracking=false] - whether to update the 
     * region being magnified by re-polling the mouse position.
     */
    _changeROI: function(params) {
        // Updates the area we are viewing; the magnification factors
        // and center can be set explicitly, or we can recompute
        // the position based on the mouse cursor position

        params = Params.parse(params, { xMagFactor: this._xMagFactor,
                                        yMagFactor: this._yMagFactor,
                                        xCenter: this._xCenter,
                                        yCenter: this._yCenter,
                                        redoCursorTracking: false });

        if (params.xMagFactor <= 0)
            params.xMagFactor = this._xMagFactor;
        if (params.yMagFactor <= 0)
            params.yMagFactor = this._yMagFactor;

        this._xMagFactor = params.xMagFactor;
        this._yMagFactor = params.yMagFactor;

        if (params.redoCursorTracking &&
            this._mouseTrackingMode != MouseTrackingMode.NONE) {
            // This depends on this.xMagFactor/yMagFactor already being updated
            [params.xCenter, params.yCenter] = this._centerFromMousePosition();
        }

        if (this._clampScrollingAtEdges) {
            let roiWidth = this._viewPortWidth / this._xMagFactor;
            let roiHeight = this._viewPortHeight / this._yMagFactor;

            params.xCenter = Math.min(params.xCenter, global.screen_width - roiWidth / 2);
            params.xCenter = Math.max(params.xCenter, roiWidth / 2);
            params.yCenter = Math.min(params.yCenter, global.screen_height - roiHeight / 2);
            params.yCenter = Math.max(params.yCenter, roiHeight / 2);
        }

        this._xCenter = params.xCenter;
        this._yCenter = params.yCenter;

        // If in lens mode, move the magnified view such that it is centered
        // over the actual mouse. However, in full screen mode, the "lens" is
        // the size of the screen -- pointless to move such a large lens around.
        if (this._lensMode && !this._isFullScreen())
            this._setViewPort({ x: this._xCenter - this._viewPortWidth / 2,
                                y: this._yCenter - this._viewPortHeight / 2,
                                width: this._viewPortWidth,
                                height: this._viewPortHeight }, true);

        this._updateCloneGeometry();
        this._updateMousePosition();
    },

    /** Return whether the system mouse sprite is over this ZoomRegion.
     * @returns {boolean} whether the system mouse sprite is over this ZoomRegion.
     */
    _isMouseOverRegion: function() {
        // Return whether the system mouse sprite is over this ZoomRegion.  If the
        // mouse's position is not given, then it is fetched.
        let mouseIsOver = false;
        if (this.isActive()) {
            let xMouse = this._magnifier.xMouse;
            let yMouse = this._magnifier.yMouse;

            mouseIsOver = (
                xMouse >= this._viewPortX && xMouse < (this._viewPortX + this._viewPortWidth) &&
                yMouse >= this._viewPortY && yMouse < (this._viewPortY + this._viewPortHeight)
            );
        }
        return mouseIsOver;
    },

    /** Does the magnified view occupy the whole screen? Note that this
     * doesn't necessarily imply
     * `this._screenPosition == ScreenPosition.FULL_SCREEN`.
     * @returns {boolean} whether the viewport showing the magnified view
     * happens to cover the whole screen.
     */
    _isFullScreen: function() {
        // Does the magnified view occupy the whole screen? Note that this
        // doesn't necessarily imply
        // this._screenPosition = ScreenPosition.FULL_SCREEN;

        if (this._viewPortX != 0 || this._viewPortY != 0)
            return false;
        if (this._viewPortWidth != global.screen_width ||
            this._viewPortHeight != global.screen_height)
            return false;
        return true;
    },

    /** Determines where the center should be given the current cursor
     * position and mouse tracking mode
     * @returns {number[]} `[x, y]` position of the centre of the region
     * that should be magnified (stage coordinates).
     * @see #_centerFromMouseCentered
     * @see #_centerFromMousePush
     * @see #_centerFromMouseProportional
     */
    _centerFromMousePosition: function() {
        // Determines where the center should be given the current cursor
        // position and mouse tracking mode

        let xMouse = this._magnifier.xMouse;
        let yMouse = this._magnifier.yMouse;

        if (this._mouseTrackingMode == MouseTrackingMode.PROPORTIONAL) {
            return this._centerFromMouseProportional(xMouse, yMouse);
        }
        else if (this._mouseTrackingMode == MouseTrackingMode.PUSH) {
            return this._centerFromMousePush(xMouse, yMouse);
        }
        else if (this._mouseTrackingMode == MouseTrackingMode.CENTERED) {
            return this._centerFromMouseCentered(xMouse, yMouse);
        }

        return null; // Should never be hit
    },

    /** Calculates the
     * stage coordinates of the centre of the region that should be magnified
     * based on tracking mode {@link MouseTrackingMode.PUSH}.
     * @param {number} xMouse - x position of the mouse
     * @param {number} yMouse - y position of the mouse
     * @returns {number[]} `[x, y]` position of the centre of the region
     * that should be magnified (stage coordinates).
     */
    _centerFromMousePush: function(xMouse, yMouse) {
        let [xRoi, yRoi, widthRoi, heightRoi] = this.getROI();
        let [cursorWidth, cursorHeight] = this._mouseSourceActor.get_size();
        let xPos = xRoi + widthRoi / 2;
        let yPos = yRoi + heightRoi / 2;
        let xRoiRight = xRoi + widthRoi - cursorWidth;
        let yRoiBottom = yRoi + heightRoi - cursorHeight;

        if (xMouse < xRoi)
            xPos -= (xRoi - xMouse);
        else if (xMouse > xRoiRight)
            xPos += (xMouse - xRoiRight);

        if (yMouse < yRoi)
            yPos -= (yRoi - yMouse);
        else if (yMouse > yRoiBottom)
            yPos += (yMouse - yRoiBottom);

        return [xPos, yPos];
    },

    /** Calculates the
     * stage coordinates of the centre of the region that should be magnified
     * based on tracking mode {@link MouseTrackingMode.PROPORTIONAL}.
     * @inheritparams #_centerFromMousePush
     */
    _centerFromMouseProportional: function(xMouse, yMouse) {
        let [xRoi, yRoi, widthRoi, heightRoi] = this.getROI();
        let halfScreenWidth = global.screen_width / 2;
        let halfScreenHeight = global.screen_height / 2;
        // We want to pad with a constant distance after zooming, so divide
        // by the magnification factor.
        let unscaledPadding = Math.min(this._viewPortWidth, this._viewPortHeight) / 5;
        let xPadding = unscaledPadding / this._xMagFactor;
        let yPadding = unscaledPadding / this._yMagFactor;
        let xProportion = (xMouse - halfScreenWidth) / halfScreenWidth;   // -1 ... 1
        let yProportion = (yMouse - halfScreenHeight) / halfScreenHeight; // -1 ... 1
        let xPos = xMouse - xProportion * (widthRoi / 2 - xPadding);
        let yPos = yMouse - yProportion * (heightRoi /2 - yPadding);

        return [xPos, yPos];
    },

    /** Calculates the
     * stage coordinates of the centre of the region that should be magnified
     * based on tracking mode {@link MouseTrackingMode.CENTERED}.
     * @inheritparams #_centerFromMousePush
     */
    _centerFromMouseCentered: function(xMouse, yMouse) {
        return [xMouse, yMouse];
    },

    /** Converts coordinates relative to the unmagnified screen to coordinates
     * relative to the origin of `this._magView`.
     * @param {number} screenX - X coordinate in unmagnified screen coordinates.
     * @param {number} screenY - Y coordinate in unmagnified screen coordinates.
     * @returns {number[]} x, y coordinates in the magnified view coordinate
     * system (?) (`ViewPortCenter + (screenX - screenCentre) * xMagFactor`).
     */
    _screenToViewPort: function(screenX, screenY) {
        // Converts coordinates relative to the (unmagnified) screen to coordinates
        // relative to the origin of this._magView
        return [this._viewPortWidth / 2 + (screenX - this._xCenter) * this._xMagFactor,
                this._viewPortHeight / 2 + (screenY - this._yCenter) * this._yMagFactor];
    },

    /** Updates the magnified view to reflected a change on dimensions or
     * position. If the view happens to occupy the entire screen, style class
     * name 'full-screen' is added. */
    _updateMagViewGeometry: function() {
        if (!this.isActive())
            return;

        if (this._isFullScreen())
            this._magView.add_style_class_name('full-screen');
        else
            this._magView.remove_style_class_name('full-screen');

        this._magView.set_size(this._viewPortWidth, this._viewPortHeight);
        this._magView.set_position(this._viewPortX, this._viewPortY);
    },

    /** Updates the scale/position of the mouse picture (in the magnified region)
     * and contents of the viewport (?) */
    _updateCloneGeometry: function() {
        if (!this.isActive())
            return;

        this._uiGroupClone.set_scale(this._xMagFactor, this._yMagFactor);
        this._mouseActor.set_scale(this._xMagFactor, this._yMagFactor);

        let [x, y] = this._screenToViewPort(0, 0);
        this._uiGroupClone.set_position(x, y);

        this._updateMousePosition();
    },

    /** Updates the position of the crosshairs and picture of the mouse in
     * the magnified region. */
    _updateMousePosition: function() {
        if (!this.isActive())
            return;

        let [xMagMouse, yMagMouse] = this._screenToViewPort(this._magnifier.xMouse,
                                                            this._magnifier.yMouse);

        xMagMouse = Math.round(xMagMouse);
        yMagMouse = Math.round(yMagMouse);

        this._mouseActor.set_position(xMagMouse, yMagMouse);

        if (this._crossHairsActor) {
            let [groupWidth, groupHeight] = this._crossHairsActor.get_size();
            this._crossHairsActor.set_position(xMagMouse - groupWidth / 2,
                                               yMagMouse - groupHeight / 2);
        }
    }
};

/** creates a new Crosshairs
 * @classdesc
 * ![Crosshairs and the gnome-shell magnifier](pics/magnifier.png)
 *
 * This is the Crosshairs that appears for the magnifier.
 * This class manages things like the crosshair colour, width, etc.
 *
 * Actor-wise, the Crosshairs actor is a `Clutter.Group` containing
 * four `Clutter.Rectangle`s, one for each of the top, bottom, left and right hairs.
 * @class
 */
function Crosshairs() {
    this._init();
}

Crosshairs.prototype = {
    _init: function() {

        // Set the group containing the crosshairs to three times the desktop
        // size in case the crosshairs need to appear to be infinite in
        // length (i.e., extend beyond the edges of the view they appear in).
        let groupWidth = global.screen_width * 3;
        let groupHeight = global.screen_height * 3;

        this._actor = new Clutter.Group({
            clip_to_allocation: false,
            width: groupWidth,
            height: groupHeight
        });
        this._horizLeftHair = new Clutter.Rectangle();
        this._horizRightHair = new Clutter.Rectangle();
        this._vertTopHair = new Clutter.Rectangle();
        this._vertBottomHair = new Clutter.Rectangle();
        this._actor.add_actor(this._horizLeftHair);
        this._actor.add_actor(this._horizRightHair);
        this._actor.add_actor(this._vertTopHair);
        this._actor.add_actor(this._vertBottomHair);
        this._clipSize = [0, 0];
        this._clones = [];
        this.reCenter();
    },

   /**
    * Either add the crosshairs actor to the given ZoomRegion, or, if it is
    * already part of some other ZoomRegion, create a clone of the crosshairs
    * actor, and add the clone instead.  Returns either the original or the
    * clone.
    * @param {Magnifier.ZoomRegion} zoomRegion - The container to add the crosshairs group to.
    * @param {Clutter.Texture} magnifiedMouse - The mouse actor for the zoom region -- used to
    *                   position the crosshairs and properly layer them below
    *                   the mouse.
    * @returns {Clutter.Group} The crosshairs actor, or its clone.
    */
    addToZoomRegion: function(zoomRegion, magnifiedMouse) {
        let crosshairsActor = null;
        if (zoomRegion && magnifiedMouse) {
            let container = magnifiedMouse.get_parent();
            if (container) {
                crosshairsActor = this._actor;
                if (this._actor.get_parent() != null) {
                    crosshairsActor = new Clutter.Clone({ source: this._actor });
                    this._clones.push(crosshairsActor);
                }
                if (this._actor.visible)
                    crosshairsActor.show();
                else
                    crosshairsActor.hide();

                container.add_actor(crosshairsActor);
                container.raise_child(magnifiedMouse, crosshairsActor);
                let [xMouse, yMouse] = magnifiedMouse.get_position();
                let [crosshairsWidth, crosshairsHeight] = crosshairsActor.get_size();
                crosshairsActor.set_position(xMouse - crosshairsWidth / 2 , yMouse - crosshairsHeight / 2);
            }
        }
        return crosshairsActor;
    },

    /**
     * Remove the crosshairs actor from its parent container, or destroy the
     * child actor if it was just a clone of the crosshairs actor.
     * @param {Clutter.Group} childActor - the actor returned from
     * {@link #addToZoomRegion}.
     */
    removeFromParent: function(childActor) {
        if (childActor == this._actor)
            childActor.get_parent().remove_actor(childActor);
        else
            childActor.destroy();
    },

    /**
     * Set the color of the crosshairs.
     * @param {Clutter.Color} clutterColor - The color as a Clutter.Color.
     */
    setColor: function(clutterColor) {
        this._horizLeftHair.set_color(clutterColor);
        this._horizRightHair.set_color(clutterColor);
        this._vertTopHair.set_color(clutterColor);
        this._vertBottomHair.set_color(clutterColor);
    },

    /**
     * Get the color of the crosshairs.
     * @returns {Clutter.Color} The color as a Clutter.Color.
     */
    getColor: function() {
        let clutterColor = new Clutter.Color();
        this._horizLeftHair.get_color(clutterColor);
        return clutterColor;
    },

    /**
     * Set the width of the vertical and horizontal lines of the crosshairs.
     * @inheritparams Magnifier.setCrosshairsThickness
     */
    setThickness: function(thickness) {
        this._horizLeftHair.set_height(thickness);
        this._horizRightHair.set_height(thickness);
        this._vertTopHair.set_width(thickness);
        this._vertBottomHair.set_width(thickness);
        this.reCenter();
    },

    /**
     * Get the width of the vertical and horizontal lines of the crosshairs.
     * @inheritparams Magnifier.getCrosshairsThickness
     */
    getThickness: function() {
        return this._horizLeftHair.get_height();
    },

    /**
     * Set how opaque the crosshairs are.
     * @inheritparams Magnifier.setCrosshairsOpacity
     */
    setOpacity: function(opacity) {
        // set_opacity() throws an exception for values outside the range
        // [0, 255].
        if (opacity < 0)
            opacity = 0;
        else if (opacity > 255)
            opacity = 255;

        this._horizLeftHair.set_opacity(opacity);
        this._horizRightHair.set_opacity(opacity);
        this._vertTopHair.set_opacity(opacity);
        this._vertBottomHair.set_opacity(opacity);
    },

    /**
     * Retrieve how opaque the crosshairs are.
     * @inheritparams Magnifier.getCrosshairsOpacity
     */
    getOpacity: function() {
        return this._horizLeftHair.get_opacity();
    },

    /**
     * Set the length of the vertical and horizontal lines in the crosshairs.
     * @inheritparams Magnifier.setCrosshairsLength
     */
    setLength: function(length) {
        this._horizLeftHair.set_width(length);
        this._horizRightHair.set_width(length);
        this._vertTopHair.set_height(length);
        this._vertBottomHair.set_height(length);
        this.reCenter();
    },

    /**
     * Get the length of the vertical and horizontal lines in the crosshairs.
     * @inheritparams Magnifier.getCrosshairsLength
     */
    getLength: function() {
        return this._horizLeftHair.get_width();
    },

    /**
     * Set the width and height of the rectangle that clips the crosshairs at
     * their intersection.
     * @param {number[]} size - Array of `[width, height]` defining the size of
     *                          the clip rectangle.
     */
    setClip: function(size) {
        if (size) {
            // Take a chunk out of the crosshairs where it intersects the
            // mouse.
            this._clipSize = size;
            this.reCenter();
        }
        else {
            // Restore the missing chunk.
            this._clipSize = [0, 0];
            this.reCenter();
        }
     },

    /**
     * Get the dimensions of the clip rectangle.
     * @param {number[]} return - An array of the form `[width, height]`.
     */
    getClip: function() {
        return this._clipSize;
    },

    /**
     * Show the crosshairs.
     */
    show: function() {
        this._actor.show();
        // Clones don't share visibility.
        for (let i = 0; i < this._clones.length; i++)
            this._clones[i].show();
    },

    /**
     * Hide the crosshairs.
     */
    hide: function() {
        this._actor.hide();
        // Clones don't share visibility.
        for (let i = 0; i < this._clones.length; i++)
            this._clones[i].hide();
    },

    /**
     * Reposition the horizontal and vertical hairs such that they cross at
     * the center of crosshairs group.  If called with the dimensions of
     * the clip rectangle, these are used to update the size of the clip.
     * @param {number[]} [clipSize] - Dimensions of the clip rectangle (optional).
     * If present, an array of the form `[width, height]`.
     */
    reCenter: function(clipSize) {
        let [groupWidth, groupHeight] = this._actor.get_size();
        let leftLength = this._horizLeftHair.get_width();
        let rightLength = this._horizRightHair.get_width();
        let topLength = this._vertTopHair.get_height();
        let bottomLength = this._vertBottomHair.get_height();
        let thickness = this._horizLeftHair.get_height();

        // Deal with clip rectangle.
        if (clipSize)
            this._clipSize = clipSize;
        let clipWidth = this._clipSize[0];
        let clipHeight = this._clipSize[1];

        // Note that clip, if present, is not centred on the cross hair
        // intersection, but biased towards the top left.
        let left = groupWidth / 2 - clipWidth * 0.25 - leftLength;
        let right = groupWidth / 2 + clipWidth * 0.75;
        let top = groupHeight / 2 - clipHeight * 0.25 - topLength - thickness / 2;
        let bottom = groupHeight / 2 + clipHeight * 0.75 + thickness / 2;
        this._horizLeftHair.set_position(left, (groupHeight - thickness) / 2);
        this._horizRightHair.set_position(right, (groupHeight - thickness) / 2);
        this._vertTopHair.set_position((groupWidth - thickness) / 2, top);
        this._vertBottomHair.set_position((groupWidth - thickness) / 2, bottom);
    }
};
