// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * A class that saves history to a gsettings key (like looking glass command
 * history).
 *
 * Used by the {@link RunDialog.RunDialog},
 * {@link LookingGlass.LookingGlass#_entry},
 * {@link TelepathyClient.ChatNotification}.
 */

const Lang = imports.lang;
const Signals = imports.signals;
const Clutter = imports.gi.Clutter;
const Params = imports.misc.params;

/** the default number of commands to store in history.
 * @const
 * @type {number}
 * @default */
const DEFAULT_LIMIT = 512;

/** creates a new HistoryManager.
 *
 * This adds a keypress event handler to `params.entry` (if not `null`) such
 * that the up and down keys navigate through the history.
 *
 * Also if `params.gsettingsKey` is not null, the constructor monitors the
 * key (under 'org.gnome.shell') to make sure it remains in sync with the history.
 *
 * @classdesc
 * The HistoryManager remembers the last {@link DEFAULT_LIMIT} things typed
 * into a specified `Clutter.Text`, and connects the up and down key press
 * events to navigate through the history.
 *
 * It also stores the history in a gsettings key.
 *
 * The gsettings key and entry are both optional, so the `HistoryManager` can
 * be used to just store history to a gsettings key without the entry, or to
 * be connected to an entry without a gsettings key, or simply to store history
 * (not with an entry or a gsettings key).
 * 
 * @param {Object} [params] - input parameters.
 * @param {?string} [params.gsettingsKey] - Gsettings key (under 'org.gnome.shell')
 * to store the history in (if provided).
 * @param {number} [params.limit=DEFAULT_LIMIT] - Limit of commands to store
 * in the history.
 * @param {Clutter.Text} [params.entry] - if provided, up and down key presses
 * on the entry will navigate through the history.
 * @class
 */
function HistoryManager(params) {
    this._init(params);
}

HistoryManager.prototype = {
    _init: function(params) {
        params = Params.parse(params, { gsettingsKey: null,
                                        limit: DEFAULT_LIMIT,
                                        entry: null });

        /** The GSettings key that will hold the history. Can be null if this
         * history is not to be stored in gsettings.
         * @type {?string} */
        this._key = params.gsettingsKey;
        /** The limit on number of items stored in the history.
         * Default {@link DEFAULT_LIMIT} (can be modified upon construction).
         * @type {number} */
        this._limit = params.limit;

        this._historyIndex = 0;
        if (this._key) {
            this._history = global.settings.get_strv(this._key);
            global.settings.connect('changed::' + this._key,
                                    Lang.bind(this, this._historyChanged));

        } else {
            this._history = [];
        }

        /** The Clutter.Text the history is tied to. Can be null. If supplied,
         * pressing the up and down buttons while in the entry will navigate
         * through the history.
         * @type {Clutter.Text} */
        this._entry = params.entry;

        if (this._entry) {
            this._entry.connect('key-press-event', 
                                Lang.bind(this, this._onEntryKeyPress));
        }
    },

    /** callback when the gsettings key that is tied to this history changes.
     * It updates the local list of commands to be the same as what is in
     * the gsettings key.
     */
    _historyChanged: function() {
        this._history = global.settings.get_strv(this._key);
        this._historyIndex = this._history.length;
    },

    /** Returns the previous item in the history, storing the current item
     * (if supplied).
     * @param {string} text - the text to store in the history before going
     * to the previous item.
     * @returns {string} the previous item in the history, or the input `text`
     * if we are at the start.
     */
    prevItem: function(text) {
        if (this._historyIndex <= 0)
            return text;

        if (text)
            this._history[this._historyIndex] = text;
        this._historyIndex--;
        return this._indexChanged();
    },

    /** Returns the next item in the history, saving the input text into the
     * history first (if provided).
     * @param {string} text - the text to store in the history before going
     * to the next item.
     * @returns {string} the next item in the history, or the input `text`
     * if we are already at the end.
     */
    nextItem: function(text) {
        if (this._historyIndex >= this._history.length)
            return text;

        if (text)
            this._history[this._historyIndex] = text;
        this._historyIndex++;
        return this._indexChanged();
    },

    /** Resets the history to the last item and clears the entry (if any).
     * This is so that subsequent calls to {@link #prevItem} and
     * {@link #nextItem} will go from the most recently-entered
     * command.
     *
     * Used in {@link RunDialog.RunDialog#open} and
     * {@link LookingGlass.LookingGlass#open}. (returns `undefined`, but you
     * don't use this function to retrieve history, only to set it back to the
     * most recent entry).
     */
    lastItem: function() {
        if (this._historyIndex != this._history.length) {
            this._historyIndex = this._history.length;
            this._indexChanged();
        }

        return this._historyIndex[this._history.length];
    },

    /** Adds an item to the history and saves it, truncating to
     * {@link #_limit} if necessary.
     * @param {string} input - text to add to the history.
     */
    addItem: function(input) {
        if (this._history.length == 0 ||
            this._history[this._history.length - 1] != input) {

            this._history.push(input);
            this._save();
        }
        this._historyIndex = this._history.length;
    },

    /** callback when the user presses a key while focus is on
     * {@link #_entry}. If the user pressed 'up', the history
     * manager navigates to the previous item, showing it in the entry (and
     * storing any text already in the entry into the history so that it
     * may be retrieved later). If the user pressed 'down', the same thing
     * happens except for the next item.
     * @param {Clutter.Text} entry - entry that fired the 'key-press-event'
     * signal ({@link #_entry}).
     * @param {Clutter.ButtonEvent} event - event fired.
     */
    _onEntryKeyPress: function(entry, event) {
        let symbol = event.get_key_symbol();
        if (symbol == Clutter.KEY_Up) {
            this.prevItem(entry.get_text());
            return true;
        } else if (symbol == Clutter.KEY_Down) {
            this.nextItem(entry.get_text());
            return true;
        }
        return false;
    },

    /** Called by almost all the public functions
     * ({@link #nextItem} etc) - grabs the item from the
     * history according to our current position within it and sets
     * {@link #_entry}'s text to it (if we have an entry).
     *
     * Also emits the 'changed' signal with the current history value.
     * @returns {string} the item in the history at the current location.
     * @fires .changed
     */
    _indexChanged: function() {
        let current = this._history[this._historyIndex] || '';
        this.emit('changed', current);

        if (this._entry)
            this._entry.set_text(current);

        return current;
    },

    /** Internal function that saves the history to the gsettings key 
     * ({@link #_key}, if any),
     * and also truncates the history to the limit specified
     * ({@link #_limit}).
     */
    _save: function() {
        if (this._history.length > this._limit)
            this._history.splice(0, this._history.length - this._limit);

        if (this._key)
            global.settings.set_strv(this._key, this._history);
    }
};
Signals.addSignalMethods(HistoryManager.prototype);

// event documentation.

/** Fired whenever the current location in the history changed.
 * @memberof HistoryManager
 * @name changed
 * @event
 * @param {History.HistoryManager} o - the history manager that fired the
 * signal.
 * @param {string} current - the current item of the history.
 */
