// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * a set of useful functions, mainly to do with spawning external processes in
 * the background.
 */

const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Shell = imports.gi.Shell;

const Main = imports.ui.main;

// http://daringfireball.net/2010/07/improved_regex_for_matching_urls
const _balancedParens = '\\((?:[^\\s()<>]+|(?:\\(?:[^\\s()<>]+\\)))*\\)';
const _leadingJunk = '[\\s`(\\[{\'\\"<\u00AB\u201C\u2018]';
const _notTrailingJunk = '[^\\s`!()\\[\\]{};:\'\\".,<>?\u00AB\u00BB\u201C\u201D\u2018\u2019]';

const _urlRegexp = new RegExp(
    '(^|' + _leadingJunk + ')' +
    '(' +
        '(?:' +
            '[a-z][\\w-]+://' +                   // scheme://
            '|' +
            'www\\d{0,3}[.]' +                    // www.
            '|' +
            '[a-z0-9.\\-]+[.][a-z]{2,4}/' +       // foo.xx/
        ')' +
        '(?:' +                                   // one or more:
            '[^\\s()<>]+' +                       // run of non-space non-()
            '|' +                                 // or
            _balancedParens +                     // balanced parens
        ')+' +
        '(?:' +                                   // end with:
            _balancedParens +                     // balanced parens
            '|' +                                 // or
            _notTrailingJunk +                    // last non-junk char
        ')' +
    ')', 'gi');

/**
 * Searches `str` for URLs and returns an array containing their information.
 * @param {string} str - string to find URLs in
 * @returns {Array.<{url: string, pos: number}>} array of objects, each with
 * members `url` showing the matched URL, and `pos` with the position within
 * `str` the URL was found.
 */
function findUrls(str) {
    let res = [], match;
    while ((match = _urlRegexp.exec(str)))
        res.push({ url: match[2], pos: match.index + match[1].length });
    return res;
}

/**
 * Runs the command `argv` in the background, handling any errors that occur
 * when trying to start the program.
 *
 * @param {string[]} argv - an argv array
 *
 * @example
 *     spawn(['chromium-browser', 'https://extensions.gnome.org']);
 */
function spawn(argv) {
    try {
        trySpawn(argv);
    } catch (err) {
        _handleSpawnError(argv[0], err);
    }
}

/**
 * Runs `command_line` in the background, handling any errors that
 * occur when trying to parse or start the program.
 *
 * @param {string} command_line - a command to run.
 * @example
 *     spawn('chromium-browser https://extensions.gnome.org')
 */
function spawnCommandLine(command_line) {
    try {
        let [success, argv] = GLib.shell_parse_argv(command_line);
        trySpawn(argv);
    } catch (err) {
        _handleSpawnError(command_line, err);
    }
}

/**
 * Runs `argv` in the background. If launching `argv` fails,
 * this will throw an error.
 *
 * This function is used internally by {@link spawn} and {@link spawnCommandLine}.
 * @inheritparams spawn
 * @throws {GLib.SpawnError} any error thrown by the spawn attempt (for example
 * command not found, or 'failed to execute child process').
 */
function trySpawn(argv)
{
    try {
        GLib.spawn_async(null, argv, null,
                         GLib.SpawnFlags.SEARCH_PATH,
                         null, null);
    } catch (err) {
        if (err.code == GLib.SpawnError.G_SPAWN_ERROR_NOENT) {
            err.message = _("Command not found");
        } else {
            // The exception from gjs contains an error string like:
            //   Error invoking GLib.spawn_command_line_async: Failed to
            //   execute child process "foo" (No such file or directory)
            // We are only interested in the part in the parentheses. (And
            // we can't pattern match the text, since it gets localized.)
            err.message = err.message.replace(/.*\((.+)\)/, '$1');
        }

        throw err;
    }
}

/**
 * Runs `command_line` in the background. If launching `command_line`
 * fails, this will throw an error.
 * @inheritparams spawnCommandLine
 * @throws {GLib.SpawnError} any error thrown by the spawn attempt (for example
 * command not found, or 'failed to execute child process').
 */
function trySpawnCommandLine(command_line) {
    let success, argv;

    try {
        [success, argv] = GLib.shell_parse_argv(command_line);
    } catch (err) {
        // Replace "Error invoking GLib.shell_parse_argv: " with
        // something nicer
        err.message = err.message.replace(/[^:]*: /, _("Could not parse command:") + "\n");
        throw err;
    }

    trySpawn(argv);
}

/** Handler for any errors thrown by {@link spawn}, {@link spawnCommandLine},
 * {@link trySpawn}, and {@link trySpawnCommandLine}. Calls
 * {@link Main.notifyError} with the error message.
 * @param {string} command - command entered that caused the error.
 * @param {Error} err - associated error.
 */
function _handleSpawnError(command, err) {
    let title = _("Execution of '%s' failed:").format(command);
    Main.notifyError(title, err.message);
}

/**
 * Kills `processName`. If no process with the given name is found,
 * this will fail silently.
 * @param {string} processName - a process name
 */
function killall(processName) {
    try {
        // pkill is more portable than killall, but on Linux at least
        // it won't match if you pass more than 15 characters of the
        // process name... However, if you use the '-f' flag to match
        // the entire command line, it will work, but we have to be
        // careful in that case that we can match
        // '/usr/bin/processName' but not 'gedit processName.c' or
        // whatever...

        let argv = ['pkill', '-f', '^([^ ]*/)?' + processName + '($| )'];
        GLib.spawn_sync(null, argv, null, GLib.SpawnFlags.SEARCH_PATH, null, null);
        // It might be useful to return success/failure, but we'd need
        // a wrapper around WIFEXITED and WEXITSTATUS. Since none of
        // the current callers care, we don't bother.
    } catch (e) {
        logError(e, 'Failed to kill ' + processName);
    }
}

// This was ported from network-manager-applet
// Copyright 2007 - 2011 Red Hat, Inc.
// Author: Dan Williams <dcbw@redhat.com>

/** List of words to ignore in PCI descriptions. Used by
 * {@link fixupPCIDescription}.
 * @const
 * @type {string[]} */
const _IGNORED_WORDS = [
        'Semiconductor',
        'Components',
        'Corporation',
        'Communications',
        'Company',
        'Corp.',
        'Corp',
        'Co.',
        'Inc.',
        'Inc',
        'Incorporated',
        'Ltd.',
        'Limited.',
        'Intel',
        'chipset',
        'adapter',
        '[hex]',
        'NDIS',
        'Module'
];

/** List of phrases to ignore in PCI descriptions. Used by
 * {@link fixupPCIDescription}.
 * @const
 * @type {string[]} */
const _IGNORED_PHRASES = [
        'Multiprotocol MAC/baseband processor',
        'Wireless LAN Controller',
        'Wireless LAN Adapter',
        'Wireless Adapter',
        'Network Connection',
        'Wireless Cardbus Adapter',
        'Wireless CardBus Adapter',
        '54 Mbps Wireless PC Card',
        'Wireless PC Card',
        'Wireless PC',
        'PC Card with XJACK(r) Antenna',
        'Wireless cardbus',
        'Wireless LAN PC Card',
        'Technology Group Ltd.',
        'Communication S.p.A.',
        'Business Mobile Networks BV',
        'Mobile Broadband Minicard Composite Device',
        'Mobile Communications AB',
        '(PC-Suite Mode)'
];

/** Attempts to shorten a PCI description to something a little more readable.
 *
 * This works by removing phrases and words in a list of ignored words
 * and phrases ({@link _IGNORED_WORDS}, {@link _IGNORED_PHRASES}).
 * @param {string} desc - PCI description
 * @returns {string} shortened PCI description.
 * @todo example
 */
function fixupPCIDescription(desc) {
    desc = desc.replace(/[_,]/, ' ');

    /* Attempt to shorten ID by ignoring certain phrases */
    for (let i = 0; i < _IGNORED_PHRASES.length; i++) {
        let item = _IGNORED_PHRASES[i];
        let pos = desc.indexOf(item);
        if (pos != -1) {
            let before = desc.substring(0, pos);
            let after = desc.substring(pos + item.length, desc.length);
            desc = before + after;
        }
    }

    /* Attmept to shorten ID by ignoring certain individual words */
    let words = desc.split(' ');
    let out = [ ];
    for (let i = 0; i < words.length; i++) {
        let item = words[i];

        // skip empty items (that come out from consecutive spaces)
        if (item.length == 0)
            continue;

        if (_IGNORED_WORDS.indexOf(item) == -1) {
            out.push(item);
        }
    }

    return out.join(' ');
}
