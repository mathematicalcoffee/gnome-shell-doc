// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * wrappers around `Shell.DocSystem` that are used in `docDisplay.js` to allow
 * documents to be searched for in the Overview.
 */
const St = imports.gi.St;
const Shell = imports.gi.Shell;
const Lang = imports.lang;
const Signals = imports.signals;
const Search = imports.ui.search;

const THUMBNAIL_ICON_MARGIN = 2;

/**
 * creates a new DocInfo.
 *
 * This wraps around the input `Gtk.RecentInfo` describing a particular
 * recently-used file.
 *
 * The file's name, URI, mime type and timestamp are stored.
 *
 * @param {Gtk.RecentInfo} recentInfo - file about the document.
 * @classdesc A DocInfo is a wrapper around a Gtk.RecentInfo that stores
 * information about a document (modified time, name, URI), plus some
 * convenience methods for opening the document/creating an icon for it.
 *
 * Used by {@link DocManager}.
 * @class
 */
function DocInfo(recentInfo) {
    this._init(recentInfo);
}

DocInfo.prototype = {
    _init : function(recentInfo) {
        /** @type Gtk.RecentInfo the Gtk.RecentInfo for the instance */
        this.recentInfo = recentInfo;
        // We actually used get_modified() instead of get_visited()
        // here, as GtkRecentInfo doesn't updated get_visited()
        // correctly. See http://bugzilla.gnome.org/show_bug.cgi?id=567094
        /** the timestamp of when the file was last modified
         * @type {number} */
        this.timestamp = recentInfo.get_modified();
        /** @type string the display name of the file */
        this.name = recentInfo.get_display_name();
        this._lowerName = this.name.toLowerCase();
        /** @type string the URI to the file */
        this.uri = recentInfo.get_uri();
        this.mimeType = recentInfo.get_mime_type();
    },

    /** creates the icon for the doc info.
     * @param {number} size - size of the icon.
     */
    createIcon : function(size) {
        return St.TextureCache.get_default().load_recent_thumbnail(size, this.recentInfo);
    },

    /** launches the doc
     * @param {number} workspaceIndex - the index on which to open the doc.
     */
    launch : function(workspaceIndex) {
        Shell.DocSystem.get_default().open(this.recentInfo, workspaceIndex);
    },

    /** matches this document against search terms
     * @param {string[]} terms - array of terms to match the document against
     *  (lower-case).
     * @returns {Search.MatchType} - the match type. Either `NONE` (no match
     * against the document's name), `PREFIX` (matches the start of the
     * document's name), or `SUBSTRING` (matches some substring of the
     * document's name).
     */
    matchTerms: function(terms) {
        let mtype = Search.MatchType.NONE;
        for (let i = 0; i < terms.length; i++) {
            let term = terms[i];
            let idx = this._lowerName.indexOf(term);
            if (idx == 0) {
                mtype = Search.MatchType.PREFIX;
            } else if (idx > 0) {
                if (mtype == Search.MatchType.NONE)
                    mtype = Search.MatchType.SUBSTRING;
            } else {
                return Search.MatchType.NONE;
            }
        }
        return mtype;
    }
};


var docManagerInstance = null;

/**
 * Gets a global instance of the doc manager. Use this function to get an
 * instance of {@link DocManager} that is shared between all gnome-shell
 * objects.
 *
 * This is used in (for example) {@link DocDisplay.DocSearchProvider} to
 * provide the search capability to the provider without needlessly creating
 * many instances.
 * @returns {DocInfo.DocManager}
 */
function getDocManager() {
    if (docManagerInstance == null)
        docManagerInstance = new DocManager();
    return docManagerInstance;
}

/** creates a new DocManager
 *
 * This creates an instance of `Shell.DocSystem` and stores it so that the
 * class may wrap around it.
 *
 * It also connects to the 'changed' signal of the `DocSystem` to reload all
 * the recent docs (creating {@link DocInfo}s for them all).
 *
 * Finally it creats a number of hashes between a document's timestamp
 * and its {@link DocInfo}, and between a document's URI and {@link DocInfo}.
 * @classdesc
 * DocManager wraps the `Shell.DocSystem`, primarily to expose 
 * {@link DocInfo.DocInfo} objects.
 * @class
 */
function DocManager() {
    this._init();
}

DocManager.prototype = {
    _init: function() {
        /** The DocManager wraps around a `Shell.DocSystem` instance stored
         * here.
         * @type {Shell.DocSystem} */
        this._docSystem = Shell.DocSystem.get_default();
        /** Array of {@link DocInfo}s ordered by timestamp, most recent first.
         * @type {DocInfo.DocInfo[]} */
        this._infosByTimestamp = [];
        /** Hash of document URIs to their relevant {@link DocInfo}.
         * @type {Object.<string, DocInfo.DocInfo>} */
        this._infosByUri = {};
        this._docSystem.connect('changed', Lang.bind(this, this._reload));
        this._reload();
    },

    /** callback when the `Shell.DocSystem` reports a change to the recent
     * documents (emits the 'changed' signal). This updates the internal list
     * of recent documents ({@link #_infosByTimestamp},
     * {@link #_infosByUri}) and emits a `changed` signal.
     * @fires .changed
     */
    _reload: function() {
        let docs = this._docSystem.get_all();
        this._infosByTimestamp = [];
        this._infosByUri = {};
        for (let i = 0; i < docs.length; i++) {
            let recentInfo = docs[i];

            let docInfo = new DocInfo(recentInfo);
            this._infosByTimestamp.push(docInfo);
            this._infosByUri[docInfo.uri] = docInfo;
        }
        this.emit('changed');
    },

    /** Gets the current list of recent documents ordered most recent to
     * least recent ({@link #_infosByTimestamp}).
     * @returns {DocInfo.DocInfo[]} */
    getTimestampOrderedInfos: function() {
        return this._infosByTimestamp;
    },

    /** Gets the hash map of URI to {@link DocInfo}
     * ({@link #_infosByUri}).
     * @returns {Object.<string, DocInfo.DocInfo} */
    getInfosByUri: function() {
        return this._infosByUri;
    },

    /** Retrieves the {@link DocInfo} for a particular URI.
     * @param {string} uri - uri to look up.
     * @returns {DocInfo.DocInfo} doc info corresponding to that URI or
     * `undefined` if not found. */
    lookupByUri: function(uri) {
        return this._infosByUri[uri];
    },

    /** Asynchronously start a check of a number of recent files for existence.
     * @param {number} count - number of items to check for existence, starting
     * from most recent.
     */
    queueExistenceCheck: function(count) {
        return this._docSystem.queue_existence_check(count);
    },

    /** Internal function used to search documents matchin terms.
     * @param {DocInfo.DocInfo[]} items - array of {@link DocInfo}s to search
     * through.
     * @param {string[]} terms - terms to match (the URI) against.
     * @returns {DocInfo.DocInfo[]} doc infos that match the URIs in various ways:
     * first those that match with the start of multiple of the terms, then
     * those that match with the start of a single term, then those that match
     * with multiple substrings (of what?? term against URI?? URI against term?),
     * and finally those that just match aginst a single substring.
     * @see Search.MatchType
     */
    _searchDocs: function(items, terms) {
        let multiplePrefixMatches = [];
        let prefixMatches = [];
        let multipleSubtringMatches = [];
        let substringMatches = [];
        for (let i = 0; i < items.length; i++) {
            let item = items[i];
            let mtype = item.matchTerms(terms);
            if (mtype == Search.MatchType.MULTIPLE_PREFIX)
                multiplePrefixMatches.push(item.uri);
            else if (mtype == Search.MatchType.PREFIX)
                prefixMatches.push(item.uri);
            else if (mtype == Search.MatchType.MULTIPLE_SUBSTRING)
                multipleSubtringMatches.push(item.uri);
            else if (mtype == Search.MatchType.SUBSTRING)
                substringMatches.push(item.uri);
         }
        return multiplePrefixMatches.concat(prefixMatches.concat(multipleSubtringMatches.concat(substringMatches)));
    },

    /** Performs an intial search (i.e. through all of the recent documents)
     * against the terms.
     * @param {string[]} terms - terms to search against.
     * @returns {DocInfo.DocInfo[]} any matching results.
     */
    initialSearch: function(terms) {
        return this._searchDocs(this._infosByTimestamp, terms);
    },

    /** Performs a subsearch of results against further terms.
     * @param {DocInfo.DocInfo[]} previousResults - results to subsearch in
     * @param {string[]} terms - search terms
     * @returns {DocInfo.DocInfo[]} any matching results.
     */
    subsearch: function(previousResults, terms) {
        return this._searchDocs(previousResults.map(Lang.bind(this,
            function(url) {
                return this._infosByUri[url];
            })), terms);
    }
};

Signals.addSignalMethods(DocManager.prototype);

/** Event fired when the {@link DocManager}'s list of recent documents
 * changes.
 *
 * Note used anywhere so far as I can tell.
 * @example
 *     const docManager = imports.misc.docInfo.getDocManager();
 *     docManger.connect('changed', callback)
 *
 * @event
 * @name DocManager#changed
 * @param {Object} o - {@link DocManager} instance that fired the signal.
 */
