// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * A set of helpful functions for Gio files (wrapping around various
 * [Gio](http://developer.gnome.org/gio/stable/) functions).
 */

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;

/** Applies a function to each child of a directory.
 *
 * This wraps around
 * [`Gio.File.enumerate_children_async`](http://developer.gnome.org/gio/stable/GFile.html#g-file-enumerate-children-async)
 * and applies `callback` to each child once all the files have been gathered.
 *
 * Note that this does not recurse into subdirectories and also performs
 * the enumeration of children asynchronously.
 * @param {Gio.File} file - the directory to recurse the children of
 * (for example `Gio.file_new_for_path('/path/to/directory')`).
 * @param {function(Gio.GFileInfo[]} callback - function to apply to the
 * children of the directory `file`.
 *
 * The callback takes in an array of {@link Gio.FileInfo},
 * one for each child of `file`, and does something with them.
 * @todo example
 */
function listDirAsync(file, callback) {
    let allFiles = [];
    file.enumerate_children_async(Gio.FILE_ATTRIBUTE_STANDARD_NAME,
                                  Gio.FileQueryInfoFlags.NONE,
                                  GLib.PRIORITY_LOW, null, function (obj, res) {
        let enumerator = obj.enumerate_children_finish(res);
        function onNextFileComplete(obj, res) {
            let files = obj.next_files_finish(res);
            if (files.length) {
                allFiles = allFiles.concat(files);
                enumerator.next_files_async(100, GLib.PRIORITY_LOW, null, onNextFileComplete);
            } else {
                enumerator.close(null);
                callback(allFiles);
            }
        }
        enumerator.next_files_async(100, GLib.PRIORITY_LOW, null, onNextFileComplete);
    });
}

/** Deletes a Gio.File.
 *
 * This just calls `file.delete(null)` where `file` is a {@link Gio.File} (for
 * example created with `Gio.file_new_for_path(...)`).
 *
 * According to the function it's just a workaround for 'delete' being a keyword
 * in JS (although in GNOME 3.2+ anyway you can just call `file.dete(null)`
 * directly).
 * @param {Gio.File} file - file to delete.
 * @returns {boolean} `true` if the file was deleted, `false` otherwise.
 */
function deleteGFile(file) {
    // Work around 'delete' being a keyword in JS.
    return file['delete'](null);
}

/** Deletes a directory including all of its subdirectories and subfiles.
 *
 * This enumerates through all children of a directory and either deletes them
 * if they are a file, or recurses into them to delete their children if they
 * are a directory, finally deleting the directory itself.
 *
 * @param {Gio.File} dir - directory to delete.
 */
function recursivelyDeleteDir(dir) {
    let children = dir.enumerate_children('standard::name,standard::type',
                                          Gio.FileQueryInfoFlags.NONE, null);

    let info, child;
    while ((info = children.next_file(null)) != null) {
        let type = info.get_file_type();
        let child = dir.get_child(info.get_name());
        if (type == Gio.FileType.REGULAR)
            deleteGFile(child);
        else if (type == Gio.FileType.DIRECTORY)
            recursivelyDeleteDir(child);
    }

    deleteGFile(dir);
}
