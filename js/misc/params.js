// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * a helpful function for parsing input parameters against default parameters.
 */

/**
 * Helpful function that fills in a set of parameters with default values from
 * user-provided input.
 *
 * Examines `params` and fills in default values from `defaults` for
 * any properties in `defaults` that don't appear in `params`. If
 * `allowExtras` is not `true`, it will throw an error if `params`
 * contains any properties that aren't in `defaults`.
 *
 * If `params` is `null`, this returns the values from `defaults`.
 *
 * @param {?Object<string, any>} params - the parameters that the user provided,
 * as a hash (parameter name =&gt; value).
 * @param {Object<string, any>} defaults - the parameters that the function
 * expects with their defaults, as a hash (parameter name =&gt; value)
 * @param {boolean} allowExtras - whether or not to allow properties not in
 * `default`
 * @returns {Object} a new object with the merged parameters from `params`
 * and `defaults`.
 */
function parse(params, defaults, allowExtras) {
    let ret = {}, prop;

    if (!params)
        params = {};

    for (prop in params) {
        if (!(prop in defaults) && !allowExtras)
            throw new Error('Unrecognized parameter "' + prop + '"');
        ret[prop] = params[prop];
    }

    for (prop in defaults) {
        if (!(prop in params))
            ret[prop] = defaults[prop];
    }

    return ret;
}
