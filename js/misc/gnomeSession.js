// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Defines a set of classes that wrap around various DBus objects/interfaces.
 *
 * In particular, on bus 'org.gnome.SessionManager' with objects
 * '/org/gnome/SessionManager' and '/org/gnome/SessionManager/Presence'.
 *
 * These expose methods to shut down/log out of the current session, and also
 * to set/get the user's chat presence (busy, ...), and to retrieve inhibitors
 * to logging out (interface org.gnome.SessionManger.Inhibitor).
 *
 * See [Gnome Session documentation](http://people.gnome.org/~mccann/gnome-session/docs/gnome-session.html)
 *
 * @see MessageTray.MessageTray
 * @see EndSessionDialog.EndSessionDialog
 * @see UserMenu.UserMenuButton
 */

const DBus = imports.dbus;
const Lang = imports.lang;
const Signals = imports.signals;

/**
 * Interface definition for 'org.gnome.SessionManager.Presence'.
 *
 * Use {@link Presence} which uses this interface with
 * '/org/gnome/SessionManager/Presence'.
 *
 * See [Presence interface documentation](http://people.gnome.org/~mccann/gnome-session/docs/gnome-session.html#org.gnome.SessionManager.Presence)
 * @const
 * @fires PresenceIface.StatusChanged
 * @type {Object}
 */
const PresenceIface = {
    name: 'org.gnome.SessionManager.Presence',
    /** Changes the user's status (available, invisible, busy, idle, ...).
     * @param {GnomeSession.PresenceStatus} status - value to change the user's
     * status to.
     * @function
     * @memberof PresenceIface
     * @name SetStatus */
    methods: [{ name: 'SetStatus',
                inSignature: 'u',
                outSignature: '' }],
    /** The user's current status (this is read/write).
     * @name status
     * @memberof PresenceIface
     * @type {GnomeSession.PresenceStatus} */
    properties: [{ name: 'status',
                   signature: 'u',
                   access: 'readwrite' }],
    /** Signal fired whenever the user's status changes.
     * @name StatusChanged
     * @memberof PresenceIface
     * @event
     * @param {GnomeSession.PresenceStatus} status - the new status. */
    signals: [{ name: 'StatusChanged',
                inSignature: 'u' }]
};

/** Enum representing the user's presence status (available, invisible ...).
 * @type {number}
 * @enum
 * @const */
const PresenceStatus = {
    /** User is available */
    AVAILABLE: 0,
    /** User is invisible */
    INVISIBLE: 1,
    /** User is busy */
    BUSY: 2,
    /** User is idle */
    IDLE: 3
};

/** creates a new Presence.
 *
 * This exports the instance as a proxy for dbus object '/org/gnome/SessionManager/Presence'
 * on bus 'org.gnome.SessionManager' implementing interface
 * {@link PresenceIface}.
 * @classdesc This is a proxy class for dbus object '/org/gnome/SessionManager/Presence'
 * on bus 'org.gnome.SessionManager' implementing interface
 * {@link PresenceIface}.
 *
 * It allows gnome-shell objects to query/set the user's status.
 *
 * Used in {@link UserMenu.IMStatusChooserItem} (the combo box letting a user
 * set their presence) and {@link MessageTray.MessageTray}.
 * @class
 * @see PresenceIface
 * @todo autogenerate the DBus methods from {@link PresenceIface} (add `Remote`
 * on the end), plus the methods `GetRemote` and `SetRemote` (for properties)?
 */
function Presence() {
    this._init();
}

Presence.prototype = {
    _init: function() {
        DBus.session.proxifyObject(this, 'org.gnome.SessionManager', '/org/gnome/SessionManager/Presence', this);
    },

    /** Gets the user's status, applying a callback to it.
     * Wraps around {@link Presence#GetRemote} to retrieve
     * {@link PresenceIface#status}.
     * @param {function(object, GnomeSession.PresenceStatus)} callback - a
     * function taking in an object (the instance of {@link Presence}) and a
     * {@link GnomeSession.PresenceStatus} being the user's status.
     */
    getStatus: function(callback) {
        this.GetRemote('status', Lang.bind(this,
            function(status, ex) {
                if (!ex)
                    callback(this, status);
            }));
    },

    /** Sets the user's status. Wraps around {@link PresenceIface#SetStatus}.
     * Equivalent to calling {@link Presence#SetStatusRemote}.
     * @inheritparams PresenceIface#SetStatus
     */
    setStatus: function(status) {
        this.SetStatusRemote(status);
    }
};
DBus.proxifyPrototype(Presence.prototype, PresenceIface);

/**
 * Interface definition for 'org.gnome.SessionManager.Inhibitor'.
 * Inhibitors are things that inhibit shutting down the computer or logging out.
 *
 * See the [Inhibitor interface documentation](http://people.gnome.org/~mccann/gnome-session/docs/gnome-session.html#org.gnome.SessionManager.Inhibitor).
 *
 * Use {@link Inhibitor} which uses this interface.
 * @const
 * @todo how to properly document? as a class so that functions show up nicely?
 * @type {Object}
 *
 * @property {string} app_id -
 * ID of the app that is associated with this inhibitor
 * (usually the name of the app's `.desktop` file).
 *
 * @property {string} client_id -
 * Gets the client object path associated with this inhibit.
 *
 * @property {string} reason - Gets the reason for the inhibit.
 *
 * @property {number} flag -
 * Gets the flags that determine the scope of the inhibit.
 *
 * @property {number} toplevel_xid -
 * Gets the X11 toplevel window identifier associated with this inhibit,
 * if any. Zero if not set.
 *
 * @property {number} cookie -
 * Gets the cookie used to uniquely identify this inhibit.
 */
const InhibitorIface = {
    name: 'org.gnome.SessionManager.Inhibitor',
    properties: [
                 { name: 'app_id',
                   signature: 's',
                   access: 'readonly' },
                 { name: 'client_id',
                   signature: 's',
                   access: 'readonly' },
                 { name: 'reason',
                   signature: 's',
                   access: 'readonly' },
                 { name: 'flags',
                   signature: 'u',
                   access: 'readonly' },
                 { name: 'toplevel_xid',
                   signature: 'u',
                   access: 'readonly' },
                 { name: 'cookie',
                   signature: 'u',
                   access: 'readonly' }],
};

/**
 * Creates a new Inhibitor.
 *
 * This sets up the instance as a proxy for the object specified by
 * `objectPath` on bus 'org.gnome.SessionManager'.
 *
 * The constructor also saves each of the properties in {@link InhibitorIface}
 * locally and emits the 'is-loaded' signal when it is done.
 *
 * Note inhibitors are immutable objects, so they don't change at runtime
 * (changes always come in the form of new inhibitors). Hence there's no need
 * to monitor for changes in any of the properties.
 * @param {string} objectPath - the DBus object path to use this instance as a
 * proxy for.
 * @classdesc This allows gnome-shell classes to use interface
 * {@link InhibitorIface} on bus 'org.gnome.SessionManager' with the object
 * on the path specified.
 *
 * Used by the {@link EndSessionDialog.EndSessionDialog} to determine if there
 * are applications inhibiting a logout, for example.
 * @class
 * @see InhibitorIface
 * @fires Inhibitor.is-loaded
 * @borrows InhibitorIface#app_id as app_id
 * @borrows InhibitorIface#client_id as client_id
 * @borrows InhibitorIface#reason as reason
 * @borrows InhibitorIface#flag as flag
 * @borrows InhibitorIface#toplevel_xid as toplevel_xid
 * @borrows InhibitorIface#cookie as cookie
 */
function Inhibitor(objectPath) {
    this._init(objectPath);
}

Inhibitor.prototype = {
    _init: function(objectPath) {
        DBus.session.proxifyObject(this,
                                   'org.gnome.SessionManager',
                                   objectPath);
        this.isLoaded = false;
        this._loadingPropertiesCount = InhibitorIface.properties.length;
        for (let i = 0; i < InhibitorIface.properties.length; i++) {
            let propertyName = InhibitorIface.properties[i].name;
            this.GetRemote(propertyName, Lang.bind(this,
                function(value, exception) {
                    if (exception)
                        return;

                    this[propertyName] = value;
                    this._loadingPropertiesCount--;

                    if (this._loadingPropertiesCount == 0) {
                        this.isLoaded = true;
                        this.emit('is-loaded');
                    }
                }));
        }
    },
};
DBus.proxifyPrototype(Inhibitor.prototype, InhibitorIface);
Signals.addSignalMethods(Inhibitor.prototype);
/** Signals that the Inhibitor has finished loading all of its properties from
 * its DBus object.
 * @name is-loaded
 * @memberof Inhibitor
 * @event */


/** Part of the 'org.gnome.SessionManager' interface specification (just methods
 * that are used within gnome-shell).
 *
 * For the full specification see [SessionManager interface specification](http://people.gnome.org/~mccann/gnome-session/docs/gnome-session.html#org.gnome.SessionManager).
 *
 * Use the {@link SessionManager} to interact with it.
 * @see SessionManager
 */
const SessionManagerIface = {
    name: 'org.gnome.SessionManager',
    methods: [
        /** Requests a logout dialog.
         * @param {number} mode - the type of logout being requested:
         *
         * 0. normal
         * 1. No confirmation interface should be shown
         * 2. Forcefully logout, ignoring all inhibitors and not showing a
         * confirmation interface.
         * @function
         * @name SessionManagerIface#Logout */
        { name: 'Logout', inSignature: 'u', outSignature: '' },
        /** Requests a shutdown dialog.
         * @function
         * @name SessionManagerIface#Shutdown */
        { name: 'Shutdown', inSignature: '', outSignature: '' },
        /** Asks whether or nto it's OK to show a shutdown option in the UI.
         * @returns {boolean} true if shutdown is available to the user, false
         * otherwise.
         * @function
         * @name SessionManagerIface#CanShutdown */
        { name: 'CanShutdown', inSignature: '', outSignature: 'b' }
    ]
};

/** creates a new SessionManager.
 *
 * This exports the instance as a proxy for object '/org/gnome/SessionManager'
 * on bus 'org.gnome.SessionManager' with interface 'org.gnome.SessionManager'
 * ({@link SessionManagerIface}).
 * @classdesc
 * A proxy for object '/org/gnome/SessionManager'
 * on bus 'org.gnome.SessionManager' with interface 'org.gnome.SessionManager'
 * ({@link SessionManagerIface}).
 * @class
 * @see SessionManagerIface
 * @todo document methods inherited from {@link SessionManagerIface}?
 */
function SessionManager() {
    this._init();
}

SessionManager.prototype = {
    _init: function() {
        DBus.session.proxifyObject(this, 'org.gnome.SessionManager', '/org/gnome/SessionManager');
    }
};
DBus.proxifyPrototype(SessionManager.prototype, SessionManagerIface);
