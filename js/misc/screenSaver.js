// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Lets gnome-shell objects interact with '/org/gnome/Screensaver' on bus
 * 'org.gnome.ScreenSaver' to see whether the screensaver is active and
 * turn it on or off.
 */

const DBus = imports.dbus;
const Lang = imports.lang;

/** Definition for DBus interface 'org.gnome.ScreenSaver' (lets users
 * turn on/off the screensaver, detect when it turns on/off, lock the screen).
 *
 * See [the DBus method documentation for org.gnome.Screensaver](http://people.gnome.org/~mccann/gnome-screensaver/docs/gnome-screensaver.html#dbus-interface)
 *
 * Methods are documented in the ???? class (where??).
 *
 * The interface is used by the {@link ScreenSaverProxy}.
 * @todo document methods here and in ScreenSaverProxy??
 * @const
 * @type {Object}
 * @see ScreenSaverProxy
 * @fires ScreenSaverIface.ActiveChanged
 */
const ScreenSaverIface = {
    name: 'org.gnome.ScreenSaver',
    methods: [
        /** Gets whether the screensaver is currently active.
         * @returns {boolean} whether the screensaver is active.
         * @name GetActive
         * @memberof ScreenSaverIface
         * @function
         */
              { name: 'GetActive',
                inSignature: '',
                outSignature: 'b' },
        /** Locks or unlocks the screen, bypassing all inhibit requests.
         * @name Lock
         * @memberof ScreenSaverIface
         * @function
         */
              { name: 'Lock',
                inSignature: '' },
        /** Requests a change in state of the screensaver (i.e. activates
         * or deactivates it).
         * @name SetActive
         * @parameter {boolean} active - whether to activate or deactivate the
         * screensaver.
         * @memberof ScreenSaverIface
         * @function
         */
              { name: 'SetActive',
                inSignature: 'b' }],
        /** Signal emitted whenever the screensaver is activated or
         * deactivated.
         * @event
         * @memberof ScreenSaverIface
         * @name ActiveChanged
         * @param {?} object - unsure (whatever emitted the signal?)
         * @param {boolean} active - whether the screensaver is active or not.
         */
    signals: [{ name: 'ActiveChanged',
                inSignature: 'b' }]
};

/** creates a new ScreenSaverProxy.
 *
 * This sets up the instance as a proxy for DBus object '/org/gnome/ScreenSaver'
 * using {@link ScreenSaverIface} and exposes the {@link ScreenSaverIface}
 * methods as `<methodname>Remote`.
 *
 * It also watches the bus name 'org.gnome.ScreenSaver' and updates whether
 * the screensaver is active ({@link ScreenSaverProxy#screenSaverActive}) when
 * this changes ({@link ScreenSaverProxy#_onSSAppeared},
 * {@link ScreenSaverProxy#_onSSVanished} ).
 *
 * @classdesc
 * This makes a proxy class for the DBus interface org.gnome.ScreenSaver
 * ({@link ScreenSaverIface}) on object '/org/gnome/ScreenSaver' on bus
 * 'org.gnome.ScreenSaver'.
 *
 * It allows gnome-shell classes to query the state of the screensaver and
 * activate or deactivate it (the classes in the 'See' section all make use
 * of this class).
 *
 * @see UserMenu.UserMenuButton
 * @see AutomountManager.AutomountManger
 * @see Layout.Chrome
 * @class
 */
function ScreenSaverProxy() {
    this._init();
}

ScreenSaverProxy.prototype = {
    _init: function() {
        DBus.session.proxifyObject(this,
                                   'org.gnome.ScreenSaver',
                                   '/org/gnome/ScreenSaver');

        DBus.session.watch_name('org.gnome.ScreenSaver',
                                false, // do not launch a name-owner if none exists
                                Lang.bind(this, this._onSSAppeared),
                                Lang.bind(this, this._onSSVanished));

        /** Whether the screensaver is currently active.
         * Remains in sync with the actual value by monitoring
         * {@link ScreenSaverIface.ActiveChanged}.
         * @type {boolean} */
        this.screenSaverActive = false;
        this.connect('ActiveChanged',
                     Lang.bind(this, this._onActiveChanged));
    },

    /** callback when bus name 'org.gnome.ScreenSaver' gets an owner. Updates
     * {@link ScreenSaverProxy#screenSaverActive}. */
    _onSSAppeared: function(owner) {
        this.GetActiveRemote(Lang.bind(this, function(isActive) {
            this.screenSaverActive = isActive;
        }))
    },

    /** callback when the owner of name 'org.gnome.ScreenSaver' disappears -
     * this means the screensaver is no longer active so it adjusts
     * {@link ScreenSaverProxy#screenSaverActive}. */
    _onSSVanished: function(oldOwner) {
        this.screenSaverActive = false;
    },

    /** callback for the {@link ScreenSaverIface.ActiveChanged} signal -
     * just makes sure {@link ScreenSaverProxy#screenSaverActive} is kept in
     * sync.
     * @inheritparams ScreenSaverIface.ActiveChanged */
    _onActiveChanged: function(object, isActive) {
        this.screenSaverActive = isActive;
    }
};
DBus.proxifyPrototype(ScreenSaverProxy.prototype, ScreenSaverIface);
