// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
/**
 * @fileOverview
 * Various DBus proxy classes allowing us to communicate with objects
 * on bus 'org.freedesktop.ModemManager' implementing interface
 * 'org.freedesktop.ModemManager.Gsm.Network' and
 * 'org.freedesktop.ModemManager.Cdma'.
 *
 */

const DBus = imports.dbus;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Signals = imports.signals;

// The following are not the complete interfaces, just the methods we need
// (or may need in the future)
/** A partial definition of DBus interface
 * 'org.freedesktop.ModemManager.Modem.Gsm.Network' (just the methods that
 * are currently used/might be used).
 *
 * Methods are documented in {@link ModemGsmNetworkProxy}.
 *
 * See also [the DBus interface definition](http://projects.gnome.org/NetworkManager/developers/mm-spec-04.html#org.freedesktop.ModemManager.Modem.Gsm.Network).
 * @const
 * @fires ModemCdmaProxy.SignalQuality
 * @fires ModemCdmaProxy.RegistrationInfo
 * @type {Object} */
const ModemGsmNetworkInterface = {
    name: 'org.freedesktop.ModemManager.Modem.Gsm.Network',
    methods: [
        /** Get the registration status and the current operator (if registered).
         * @name GetRegistrationInfo
         * @returns {Array.<int, string, string>}
         * the mobile registration status, current operator code of the operator
         * to which the mobile is currently registered, and the current operator
         * name of the operator to which the mobile is currently registered.
         * @see http://projects.gnome.org/NetworkManager/developers/mm-spec-04.html#org.freedesktop.ModemManager.Modem.Gsm.Network
         * @function
         * @memberof ModemGsmNetworkProxy */
        { name: 'GetRegistrationInfo', inSignature: '', outSignature: 'uss' },
        /** Gets the current signal quality (percent).
         * @name GetSignalQuality
         * @returns {int} the signal quality (percent).
         * @function
         * @memberof ModemGsmNetworkProxy */
        { name: 'GetSignalQuality', inSignature: '', outSignature: 'u' }
    ],
    properties: [
        /** The current network access technology used by the device to
         * communicate with the base station.
         * @type {[int (enum)](http://projects.gnome.org/NetworkManager/developers/mm-spec-04.html#type-MM_MODEM_GSM_ACCESS_TECH)}
         * @member
         * @memberof ModemGsmNetworkProxy
         */
        { name: 'AccessTechnology', signature: 'u', access: 'read' }
    ],
    signals: [
        /** signal fired when the signal quality changes.
         * @name SignalQuality
         * @event
         * @memberof ModemGsmNetworkProxy
         * @parameter {number} quality - the new quality (percent)
         */
        { name: 'SignalQuality', inSignature: 'u' },
        /** signal fired when the registration status of the mobile changes.
         * @name RegistrationInfo
         * @event
         * @memberof ModemGsmNetworkProxy
         * @parameter {number} status - Mobile registration status
         * (see http://projects.gnome.org/NetworkManager/developers/mm-spec-04.html#type-MM_MODEM_GSM_NETWORK_REG_STATUS)
         * @parameter {string} operator_code - operator code of the current
         * operator for the mobile device.
         * Returned in the format "MCCMNC", where MCC is the three-digit ITU
         * E.212 Mobile Country Code and MNC is the two- or three-digit GSM
         * Mobile Network Code.
         * @parameter {string} operator_name - Current operator name of the
         * operator to which the mobile is currently registered.
         */
        { name: 'RegistrationInfo', inSignature: 'uss' }
    ]
};
/** Creates a new ModemGsmNetworkProxy
 * @classdesc
 * A proxy class for DBus interface 'org.freedesktop.ModemManager.Modem.Gsm.Network'.
 * @todo document methods for this class as [method]Remote, and in the Iface
 * as [method].
 * @class
 */
const ModemGsmNetworkProxy = DBus.makeProxyClass(ModemGsmNetworkInterface);

/** A partial definition of DBus interface
 * 'org.freedesktop.ModemManager.Modem.Cdma' (just the methods that
 * are currently used/might be used).
 *
 * Methods are documented in {@link ModemCdmaProxy}.
 *
 * See also [the DBus interface definition](http://projects.gnome.org/NetworkManager/developers/mm-spec-04.html#org.freedesktop.ModemManager.Modem.Cdma).
 * @const
 * @fires ModemCdmaProxy.SignalQuality
 * @type {Object} */
const ModemCdmaInterface = {
    name: 'org.freedesktop.ModemManager.Modem.Cdma',
    methods: [
        /**
         * @name GetSignalQuality
         * @inheritdoc ModemGsmNetworkProxy#GetSignalQuality
         * @function
         * @memberof ModemCdmaProxy */
        { name: 'GetSignalQuality', inSignature: '', outSignature: 'u' },
        /** Get the Service System details of the current network, if registered.
         * @name GetServingSystem
         * @function
         * @returns {Array.<number, string, number} An array containing the Band
         * Class (0 = unknown, 1 = 800 MHz, 2 = 1900 MHz), the Band ("A" - "F"
         * as defined by IS707-A), and the System ID of the serving network.
         * @memberof ModemCdmaProxy */
        { name: 'GetServingSystem', inSignature: '', outSignature: 'usu' }
    ],
    signals: [
        /** signal fired when the signal quality changes.
         * @name SignalQuality
         * @event
         * @memberof ModemCdmaProxy
         * @parameter {number} quality - the new quality (percent)
         */
        { name: 'SignalQuality', inSignature: 'u' }
    ]
};
const ModemCdmaProxy = DBus.makeProxyClass(ModemCdmaInterface);

/** Gets a hash table of network providers.
 *
 * Wraps around `Shell.mobile_providers_parse`.
 * @returns {Object.<string, Shell.MobileProvider[]} object that is a hash
 * mapping the mobile country code (string) to an array of mobile providers for
 * that country.
 *
 * As far as I can tell, a `Shell.MobileProvider` has methods `get_cdma_sid`
 * (used by {@link ModemCdma} and `get_gsm_mcc_mnc` (used by {@link ModemGsm}).
 */
let _providersTable;
function _getProvidersTable() {
    if (_providersTable)
        return _providersTable;
    let [providers, countryCodes] = Shell.mobile_providers_parse();
    return _providersTable = providers;
}

/** creates a new ModemGsm
 *
 * This constructs an internal {@link ModemGsmNetworkProxy} instance, and
 * connects to {@link ModemGsmNetworkProxy.SignalChanged} in order to emit
 * {@link .notify::signal-quality} and keep {@link #signal_quality}
 * in sync with the current signal quality.
 *
 * It also connects to {@link ModemGsmNetworkProxy.RegistrationInfo} in order to
 * emit {@link .notify::operator-name} and keep
 * {@link #operator_name} in sync with it.
 * @param {string} path - the DBus path of the object to apply the interface
 * to (on bus org.freedesktop.ModemManager).
 * @classdesc
 * Wraps around an internal {@link ModemGsmNetworkProxy} on the input path on
 * bus 'org.freedesktop.ModemManager'.
 *
 * It has two properties of interest {@link #signal_quality} and
 * {@link #operator_name}, which it populates with the signal quality
 * and operator name of the specified mobile device.
 *
 * @see ModemGsmNetworkProxy
 * @class
 */
function ModemGsm() {
    this._init.apply(this, arguments);
}

ModemGsm.prototype = {
    /**
     * @param {?} path
     */
    _init: function(path) {
        this._proxy = new ModemGsmNetworkProxy(DBus.system, 'org.freedesktop.ModemManager', path);

        /** the current signal quality (percent, 0 to 100).
         * @type {number} */
        this.signal_quality = 0;
        /** the current operator name.
         * @type {?string} */
        this.operator_name = null;

        // Code is duplicated because the function have different signatures
        this._proxy.connect('SignalQuality', Lang.bind(this, function(proxy, quality) {
            this.signal_quality = quality;
            this.emit('notify::signal-quality');
        }));
        this._proxy.connect('RegistrationInfo', Lang.bind(this, function(proxy, status, code, name) {
            this.operator_name = this._findOperatorName(name, code);
            this.emit('notify::operator-name');
        }));
        this._proxy.GetRegistrationInfoRemote(Lang.bind(this, function(result, err) {
            if (err) {
                log(err);
                return;
            }

            let [status, code, name] = result;
            this.operator_name = this._findOperatorName(name, code);
            this.emit('notify::operator-name');
        }));
        this._proxy.GetSignalQualityRemote(Lang.bind(this, function(result, err) {
            if (err) {
                // it will return an error if the device is not connected
                this.signal_quality = 0;
            } else {
                let [quality] = result;
                this.signal_quality = quality;
            }
            this.emit('notify::signal-quality');
        }));
    },

    /** Gets the operator description/name from their name and code.
     * If the input `name` is text rather than a numberic code then it is
     * returned as the operator name. Otherwise it's a 5 or 6-digit code
     * where the first 3 digits are a country code (MCC) and the next 2 or 3 are
     * the GSM Mobile network code (MNC); {@link #_findProviderForMCCMNC}
     * is used to look up the operator name.
     * @param {string} name - operator name
     * @param {string} opCode - operator code
     * @returns {string} the operator name.
     */
    _findOperatorName: function(name, opCode) {
        if (name.length != 0 && (name.length > 6 || name.length < 5)) {
            // this looks like a valid name, i.e. not an MCCMNC (that some
            // devices return when not yet connected
            return name;
        }
        if (isNaN(parseInt(name))) {
            // name is definitely not a MCCMNC, so it may be a name
            // after all; return that
            return name;
        }

        let needle;
        if (name.length == 0 && opCode)
            needle = opCode;
        else if (name.length == 6 || name.length == 5)
            needle = name;
        else // nothing to search
            return null;

        return this._findProviderForMCCMNC(needle);
    },

    /** Finds the provider name given the operator code which is in the format
     * "MCCMNC" where the MCC is the three-digit mobile country code and the MNC
     * is the two- or three-digit GSM Mobile network code.
     *
     * This function searches through {@link _getProvidersTable} to look up
     * the code.
     * @param {string} needle - the code to look up (MCC followed by MNC).
     * @returns {string} the operator name corresponding to `needle`.
     */
    _findProviderForMCCMNC: function(needle) {
        let table = _getProvidersTable();
        let needlemcc = needle.substring(0, 3);
        let needlemnc = needle.substring(3, needle.length);

        let name2, name3;
        for (let iter in table) {
            let providers = table[iter];

            // Search through each country's providers
            for (let i = 0; i < providers.length; i++) {
                let provider = providers[i];

                // Search through MCC/MNC list
                let list = provider.get_gsm_mcc_mnc();
                for (let j = 0; j < list.length; j++) {
                    let mccmnc = list[j];

                    // Match both 2-digit and 3-digit MNC; prefer a
                    // 3-digit match if found, otherwise a 2-digit one.
                    if (mccmnc.mcc != needlemcc)
                        continue;  // MCC was wrong

                    if (!name3 && needle.length == 6 && needlemnc == mccmnc.mnc)
                        name3 = provider.name;

                    if (!name2 && needlemnc.substring(0, 2) == mccmnc.mnc.substring(0, 2))
                        name2 = provider.name;

                    if (name2 && name3)
                        break;
                }
            }
        }

        return name3 || name2 || null;
    }
}
Signals.addSignalMethods(ModemGsm.prototype);
// events for ModemGsm
/** Fired when the signal quality changes.
 * @name notify::signal-quality
 * @param {ModemManager.ModemGsm} obj - the instance of the `ModemGsm` that
 * fired the signal. Query its `signal_quality` property to get the new
 * signal quality.
 * @memberof ModemGsm
 * @event
 */
/** Fired when the operator name changes.
 * @name notify::operator-name
 * @param {ModemManager.ModemGsm} obj - the instance of the `ModemGsm` that
 * fired the signal. Query its `operator_name` property to get the new
 * operator name.
 * @memberof ModemGsm
 * @event
 */

/** creates a new ModemCdma.
 *
 * This constructs an internal {@link ModemCdmaProxy} instance, and connects
 * to {@link ModemCdmaProxy.SignalChanged} in order to emit
 * {@link .notify::signal-quality} and keep {@link #signal_quality}
 * in sync with the current signal quality.
 *
 * It also populates {@link #operator_name}.
 * @param {string} path - the DBus path of the object to apply the interface
 * to (on bus org.freedesktop.ModemManager).
 * @classdesc
 * Wraps around an internal {@link ModemCdmaProxy} on the input path on bus
 * 'org.freedesktop.ModemManager'.
 *
 * It has two properties of interest {@link #signal_quality} and
 * {@link #operator_name}, which it populates with the signal quality
 * and operator name of the specified mobile device.
 *
 * @see ModemCdmaProxy
 * @class
 */
function ModemCdma() {
    this._init.apply(this, arguments);
}

ModemCdma.prototype = {
    _init: function(path) {
        this._proxy = new ModemCdmaProxy(DBus.system, 'org.freedesktop.ModemManager', path);

        /** the current signal quality (percent, 0 to 100).
         * @type {number} */
        this.signal_quality = 0;
        /** the current operator name.
         * @type {string} */
        this.operator_name = null;
        this._proxy.connect('SignalQuality', Lang.bind(this, function(proxy, quality) {
            this.signal_quality = quality;
            this.emit('notify::signal-quality');

            // receiving this signal means the device got activated
            // and we can finally call GetServingSystem
            if (this.operator_name == null)
                this._refreshServingSystem();
        }));
        this._proxy.GetSignalQualityRemote(Lang.bind(this, function(result, err) {
            if (err) {
                // it will return an error if the device is not connected
                this.signal_quality = 0;
            } else {
                let [quality] = result;
                this.signal_quality = quality;
            }
            this.emit('notify::signal-quality');
        }));
    },

    /** Refreshes the service system details of the current network, updating
     * {@link #operator_name}, firing the 'notify::operator-name' signal
     * when complete.
     * @fires .notify::operator-name
     * @see #GetServingSystem
     */
    _refreshServingSystem: function() {
        this._proxy.GetServingSystemRemote(Lang.bind(this, function(result, err) {
            if (err) {
                // it will return an error if the device is not connected
                this.operator_name = null;
            } else {
                let [bandClass, band, id] = result;
                if (name.length > 0)
                    this.operator_name = this._findProviderForSid(id);
                else
                    this.operator_name = null;
            }
            this.emit('notify::operator-name');
        }));
    },

    /** Looks up a provider name against the system ID of the network.
     * @param {string} sid - System ID of the network to find the provider
     * name for.
     * @returns {?string} provider name for that SID (searches by country, then
     * provider, then SID), or `null` if not found.
     * @see _getProvidersTable
     */
    _findProviderForSid: function(sid) {
        if (sid == 0)
            return null;

        let table = _getProvidersTable();

        // Search through each country
        for (let iter in table) {
            let providers = table[iter];

            // Search through each country's providers
            for (let i = 0; i < providers.length; i++) {
                let provider = providers[i];
                let cdma_sid = provider.get_cdma_sid();

                // Search through CDMA SID list
                for (let j = 0; j < cdma_sid.length; j++) {
                    if (cdma_sid[j] == sid)
                        return provider.name;
                }
            }
        }

        return null;
    }
};
Signals.addSignalMethods(ModemCdma.prototype);
/** Fired when the signal quality changes.
 * @name notify::signal-quality
 * @param {ModemManager.ModemCdma} obj - the instance of the `ModemCdma` that
 * fired the signal. Query its `signal_quality` property to get the new
 * signal quality.
 * @memberof ModemCdma
 * @event
 */
/** Fired when the operator name changes.
 * @name notify::operator-name
 * @param {ModemManager.ModemCdma} obj - the instance of the `ModemCdma` that
 * fired the signal. Query its `operator_name` property to get the new
 * operator name.
 * @memberof ModemCdma
 * @event
 */
