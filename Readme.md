This repository is basically a mirror of [GNOME's official gnome-shell git repository](http://git.gnome.org/browse/gnome-shell/refs/heads), with the following addition: the JS files (in `js/`) are commented with [JSDoc3](https://github.com/jsdoc3/jsdoc) markup.

Documentation has been added as follows:

* GNOME 3.2: to branch `gnome-3-2`, which pulls from `upstream/gnome-3-2`.
* GNOME 3.4: to branch `gnome-3-4`, which pulls from `upstream/gnome-3-4`.
* GNOME 3.6: to branch `master`, which pulls from `upstream/master`.

Feel free to contribute!

# Instructions for contributors.
Feel free to add to the documentation.

My basic idea is to start on the `gnome-3-2` branch and then merge all documentation up to the `gnome-3-4` and then the `master` branch, tweaking it each time to keep up with changes (and adding in new documentation) - mainly to avoid re-documenting the same thing over and over.

**I'm currently (5/Nov/2012) still working on the `gnome-3-2` branch. Other branches are empty.**

# Build the documentation.
NOTE - I've made a number of plugins for JSDoc 3 to assist in the documentation,
effort, and because of this I've decided to include JSDoc 3 as a submodule in
his master repository. You should use this version of JSDoc to build the documentation
since it has all the plugins in it.

To set up the repository:

    # 1. checkout the repo
    git clone git@bitbucket.org:mathematicalcoffee/gnome-shell-doc.git
    cd gnome-shell-doc
    # 2. JSDoc3 is provided as a submodule so you have to check it out
    git submodule init
    git submodule update

To generate documentation (may take a while):

    ./jsdoc/jsdoc js Readme.md -c conf.json

(Note: this is equivalent to

    ./jsdoc/jsdoc -r js Readme.md -c conf.json -d docs/reference/js -u docs/reference/js/tutorials

I just encoded some of the options in the the `conf.json` file.)


Now view the documentation by looking at `docs/reference/js/index.html`.

# Did you know?

* With the looking glass picker (the red border thing) you can scroll up and down to select the current actor's parent or child;
* You can start the gnome-shell screen recorder with Ctrl+Alt+Shift+R
* In the overview, you can navigate between tabs with Ctrl+Page(Up|Down) (up to GNOME 3.4)

# JSDoc3 recipes/Documentation notes/Cookbook

The documentation is [Markdown](http://daringfireball.net/projects/markdown/)-enabled, in particular [Git flavoured markdown](http://github.github.com/github-flavored-markdown/).

# Customisations to JSDoc 3
I have added a number of plugins to JSDoc 3 to aid in documenting the code, and this modifies how stuff works.

Firstly, each file automatically gets its own namespace, being the name of the file with the first letter capitalised. There are a couple of exceptions here - the objects in `dnd.js` are in namespace `DND` and the objects in `perf/core.js` are in namespace `Perf`.

To link to objects within a file, you can just do `{@link Class#function}`. To link to objects in *another* file, you use the namespace of that file, for example `{@link UserMenu.Class#function}` for a the function `Class.function` in `userMenu.js`.

Secondly, I've added a plugin that provides relative links within classes. For example, if you are documenting the class `MyClass` and you want to do `{@link MyClass#foo}`, you can instead do `{@link #foo}` and it will be resolved to `MyClass`. This only works within doclets that belong to a class, or the doclet for a class, and you can use it to make links to things in the current class.

The other plugins added provide non-standard JSDoc 3 tags:

* pragma tags `@+` and `@-`: these can be used to apply all tags that appear in the `@+` to all JSDoc comments (that have corresponding code) until the `@-`. Not fully tested yet, mainly useful for declaring lots of constants:

        // example: use of pragma tags for lots of constants
        // Let's document the following code:
        const MY_CONSTANT = 1;
        const MY_CONSTANT2 = 2;
        const MY_CONSTANT3 = 3;
        const MY_CONSTANT4 = 4;

        // ------ without pragma tags ------ // ------ with pragma tags ------ //
        /** Some documentation               // /** @+
         * @const                            //  * @const
         * @default                          //  * @default
         * @type {number} */                 //  * @type {number} */
        const MY_CONSTANT = 1;               //
                                             // /** Some documentation */
        /** Some documentation               // const MY_CONSTANT = 1;
         * @const                            //
         * @default                          // /** Some documentation */
         * @type {number} */                 // const MY_CONSTANT2 = 2;
        const MY_CONSTANT2 = 2;              //
                                             // /** Some documentation */
        /** Some documentation               // const MY_CONSTANT3 = 3;
         * @const                            //
         * @default                          // /** Some documentation */
         * @type {number} */                 // const MY_CONSTANT4 = 4;
        const MY_CONSTANT3 = 3;              //
                                             // /** @- */
        /** Some documentation               //
         * @const                            //
         * @default                          //
         * @type {number} */                 //
        const MY_CONSTANT4 = 4;              //

* `@override` : add to a function in a class that overrides the same function in the superclass (this means the class has to have an `@extends [Superclass]` in it). This adds the superclass function to the `@see` documentation for that function, and also duplicates all the documentation of the superclass method to the overridden one. If you additionally document parameters etc in the subclass method they will override that of the superclass method.

        /** A test class
         * @extends Superclass
         * @class
         */
        function MyClass() {
            /** This description is for `myFunction` which overrides {@link Superclass#myFunction}.
              * Adding the '@override' tag means that documentation for any of the parameters below
              * that I do not document myself below will be used, and it will also add a
              * `@see Superclass#myFunction` bit to this doclet.
              * @param {string} a - this will appear instead of the documentation for `a` in
              *                     Superclass#myFunction. `b` and `c` will take their
              *                     documentation from Superclass#myFunction since I haven't
              *                     documented them explicitly.
              */
            myFunction: function (a, b, c) {
            }
        }

* `@inheritparams otherFunction`: causes this function to inherit parameter and return type documentation from `otherFunction`.
If you explicitly document some of the parameters yourself that are also documented in `otherFunction`, your documentation
will appear. In the example below, `doSomethingExtra` will take the `x` documentation from `doSomething`, and the `y` and `z` documentation as defined i nthe `doSomethingExtra` doclet.

        /** a function that does something
         * @param {number} x - documentation for x
         * @param {number} y - documentation for y
         * @param {number} z - documentation for z
         */
        function doSomething(x, y, z) {
        }
        /** A function that does something extra. I'm going to steal documentation
         * for the 'x' and 'y' parameters from `doSomething`. 
         * @param {number} a - I have to document this myself since it doesn't appear in
         *                     doSomething.
         * @param {number} y - this documentation will override the documentation for
         *                     parameter `y` in doSomething's documentation.
         * @inheritparams doSomething
         */
        function doSomethingExtra(x, y, a) {
        }

* `@inheritdoc otherFunction`: the same as `@inheritparams` but will also inherit `otherFunction`s summary, description,
   class description and classdesc (if not defined in the doclet using the tag).

* `@registerlink symbol URL`: this means that all places where you'd expect a link to `symbol`, it will properly link to URL. In the example below, this means that whenever I do `{@link Clutter.Actor}` or even `@param {Clutter.Actor}`, the 'Clutter.Actor' will link to the URL provided. A list of the currently-registered links is in `js/ui/.registeredLinks`, feel free to add to it.

        @registerlink Clutter.Actor http://developer.gnome.org/clutter/stable/ClutterActor.html

# Cookbook:

* **[TODO: check]** less than and greater than signs in the comments should be written as `&lt;` and `&gt;` or else they get lost.
* link to an object 'Xyz' in another file 'asdf.js':

        {@link Asdf.Xyz}

* label on a link to a symbol (for a link to a URL just use the usual Markdown syntax):

        [link label]{@link Asdf.Xyz} 

* Add pictures by putting your picture into the `docs/reference/js/pics` folder, and link them from documentation like shown below. Note - I'm trying to make it so that the image alt text *also* gets added as the image caption, so keep that in mind when captioning things.

        ![image alt text](pics/picture_name)

* Documenting a class (GNOME 3.2):

        /**
         * creates a new ClassName.
         * (document the *constructor* function, this._init, here. Along with
         * params for the constructor).
         *
         * Note that within this doclet and any documentation for methods of
         * this class, you can use {@link #x} to refer to {@link ClassName#x}.
         *
         * @classdesc describe the *class itself* here.
         * @extends SuperClass
         * @class
         */
        function ClassName(params) {
            this._init......;
        }
        
        Classname.prototype = {
              // do *not* document _init; it is documented above.
              ...
        };

* Documenting an object supposed to be used as an enum:

        /**
         * Enum for asdf.
         * @enum {number}
         * @const
         */
        const ASDF = {
            VAL1: 0,
            VAL2: 1
        };

* Documenting a constant:

        /** constant that does such-and-such
         * @type {string}
         * @const
         * @default
         */
        const FDSA = 'alkjaskldjfa';

* function: parameters/return value, with a string input, object input
  (with required property `name`) array of numbers and two optional booleans,
  one with a default value.

        /** A function that does something.
         * @param {string} str - input string
         * @param {Object} obj - input object. must have properties:
         * @param {string} obj.name - name of ...
         * @param {number[]} arr - array of numbers
         * @param {boolean} [bool] - optional boolean
         * @param {boolean} [bool2=true] - optional boolean with default value of true.
         * @returns {number} some return value with explanation.
         */
        function doStuff(str, obj, arr, bool, bool2) {
          // ...
        }

* Documenting a function's parameter that is an object that is meant to have particular properties:

        /** A function that does something.
         * @param {Object} params - parameters
         * @param {boolean} params.x_fill - whether to fill in the x direction
         * @param {boolean} params.y_fill - whether to fill in the x direction
         */
        function myFunction(params) {
        }

* Events fired by a class (convention on where to document them - bottom of class (??)

        /** event description.
         * @name PopupMenu#event:open-state-changed
         * @param {PopupMenu.PopupMenu} menu - the menu that fired the signal
         * @param {boolean} state - whether the menu is open or closed.
         */
        // in PopupMenu#open documentation:
        /** Opens a popup menu.
         * @param {boolean} animate - (documentation)
         * @fires PopupMenu#event:open-state-changed
         */                                                                         
        open: function(animate) {             
            // ...
            this.emit('open-state-changed', true);
        }

* Documenting a class method that also exists in the superclass - this will automatically inherit the function parameter and @returns documentation from the superclass:

        /** function description (e.g. "extends the parent method to ....")
         * @override */
        activate: function () {
        }

# TODO
## Urgent
* events documented with @memberof are static members of the parent class, should they be??/update links.
* newlines in my documentation appear to be *preserved* in the output?!
* markdown links in @param and @type appear to not be done

## Documentation todo
* add @virtual
* tutorial for DND
* more example code
* prettyprint markdown code snippets or provide a @code tag for them (that won't be added to the Examples section)
* provide Screenshots of examples (especially in PopupMenu).
* check that various @inheritparams of events worked
* getters/setters are documented as functions or properties?
* go back and fill out the constructor documentation for a number of classes that are missing them (dash.js, ...)
* have to work out how to do DBus stuff --> automagically document *Remote methods?
* check that "parent method" links are not using relativeLinks, i.e. [parent method]{@link #method} is WRONG
* '@summary' tag -- fill out
* add usage example to top of each fileOverview: `const DND = imports.ui.dnd;`
* document the CSS styles each (UI) class has, with a link to that bit of the CSS file?
* **Is there some way to have the generated documentation upload to the project's wiki automatically?**
* I don't know much about interfaces and such, but I think somehow '_Draggable' should be an @interface and things like Dash should @implement it (acceptDrop, handleDragOver).
* sizes of the pictures.
* screenshots should be updated on each branch (e.g. looking glass screenshots for 3.2 branch should be green since they're taken in 3.2;
looking glass screenshots for the 3.4 branch should have white text).
* makefile for docs with make/make clean?

## JSDoc todo
* markdown code is prettified (need `<pre class="prettyPrint">` instead of `<pre>`)
* check for broken links and make an index
* @listens tag What about @handles? are these different?
* '@family ...' to group functions in the same namespace.
* convert types that should be links but are arrays to types..(e.g. @param {Classname} would link to the class;
  @param {Classname[]} should too).
* make up a tag '@connectsto <event>' to give a list from the event of examples of use?
* list "public" methods before "private" ones (with underscores).
* something that adds the alt text of an image as a caption below the image: figure tags? (separate to inline pictures)
* add @default to class properties so that defaults show??

## Template todo
* generate class heirarchy
* each class should have a list of subclasses!
* indices: images, todo, events, undocumented, examples, ...
* there should be an image index where the user can look at all the images on the site and click on it to go to the
class for that image (just so if you don't know what a thing is called but you can see a picture of it, you can
still get to the page).
* make a template that looks like the GTK one.(this one is nice: http://documentation.vizrt.com/ece-cs-jsdoc/5.4/symbols/content-studio.html)
* search bar/some decent navigation (by file, by module, by class, huge index, ..)

## Tutorials
* the popup slider menu item subclass with label and dynamic range.
* summary of popup menu items and the relevant signals
* sending a notification.
* subclassing searchprovider.


# Changes made to JS files
This summarizes changes made to the code in the JS files in order to get the documentation to work.
(I'll probably put this file in each branch and fill out the changes in the file on that branch and hopefully remember to merge them every now and then???)

### GNOME 3.6 (`master`)
### GNOME 3.4 (`gnome-3-4`)
### GNOME 3.2 (`gnome-3-2`)
(line numbers are prior to documentation being added....)

* `ui/boxpointer.js` line 354: removed redeclaration of variable `themeNode`.
* `ui/userMenu.js` line 383: removed redeclaration of variable `newPresence`.
* `misc/config.js.in`: modified `HAVE_BLUETOOTH = @HAVE_BLUETOOTH@` to `HAVE_BLUETOOTH = parseInt('@HAVE_BLUETOOTH@', 10)` in order to have a config file with valid JS syntax (to include it in the documentation).
