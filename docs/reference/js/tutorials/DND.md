# Drag and Drop in GNOME-Shell.

#### Table of Contents
UPTO
* [Making an actor draggable](#draggable)
 + [Customising the draggable](#draggable-customise)
* [Letting an actor accept drops](#drop-target)

This covers giving gnome-shell classes the ability to:

* be dragged;
* accept a drop of another gnome-shell class OR Xdnd object.

First, let's establish some terminology.

![Example of a draggable](pics/dragEvent.png)

The above picture shows the Thunderbird {@link AppDisplay.AppWellIcon} being
dragged from its place in the dash.

The original {@link AppDisplay.AppWellIcon}'s actor (its icon) is called the *source actor*.

The copy of the icon that is being dragged by the mouse is called the *drag actor*.

In this instance, the {@link AppDisplay.AppWellIcon} has been *made draggable* (we will explain how to specify that a class and its actor are draggable later).

The actor that I am *holding the icon over* (in this case, the dash), is a *potential drop target*. GNOME-shell recognises an actor as a potential drop target if its class implements the {@link DND.DropTarget} API, which we will explain later. The drop target is also called the *target actor*.

Note that a class that is draggable does not necessarily have to itself accept drops, and vice versa.

## <a id="draggable">Making an actor draggable</a>
Suppose you have a class `MyClass` and it has an actor `this.actor` that you wish to be draggable (for example the {@link AppWellDisplay.AppWellIcon} makes its icon draggable).

Use {@link DND.makeDraggable} on the actor, and **additionally provide a link from the actor back to its class**:

    // make this.actor draggable
    this._draggable = DND.makeDraggable(this.actor);
    // ensure that given the actor we can always find its source class.
    this.actor._delegate = this;

{@link DND.makeDraggable} returns a class {@link DND._Draggable} with various functions and signals connected up to make the dragging magic happen.

### <a id="draggable-params">Customising drag behaviour</a>
One can provide a second optional parameter that affects the behaviour of the drag.
This second parameter is an object containing any of the following options:

 * `manualMode` (default `false`) - if `true`, then drag/drop is *not* automatically started upon the user clicking the source actor and trying to drag. Instead, one must call {@link _Draggable#startDrag} to initiate the drag manually.
 * `restoreOnSuccess` (default `false`) - upon a successful drop, should we fade the drag actor back in in its original position?
 * `dragActorMaxSize` - the maximum size to use for the drag actor (if it is smaller than this it will be left as-is; if it is larger, it will be scaled to this size).
                        If not provided, the drag actor will be used without resizing.
 * `dragActorOpacity` - the opacity to use for the drag actor (otherwise it's opaque).

### <a id="draggable-customise">Customise the drag actor (Draggable API)</a>
There are various ways to customise what the drag actor itself looks like.

![Example of a draggable](pics/dragEvent.png)

By default the drag actor (that appears under the cursor during the drag) is the same actor that was made draggable (i.e. the actor given to {@link DND.makeDraggable}).

This can be changed by implementing the [Draggable API]{@link DND.Draggable}.

This is comprised of the functions `getDragActor` and `getDragActorSource`. At least `getDragActor` must be implemented.

For this to work, the actor that is made draggable must have a `_delegate` property pointing to the class with the `getDragActor` function:

     _init: function () {
         // make the draggable.
         this._draggable = DND.makeDraggable(this.actor);
         // set this.actor._delegate to point back to this to find the
         // getDragActor function.
         this.actor._delegate = this;
     },

#### <a id="draggable-getDragActor">`getDragActor`</a>

A class that wishes to modify the drag actor should implement the [getDragActor]{@link DND.Draggable#getDragActor} function of the Draggable API.
This should return a new actor that will be the draggable.

     // implement getDragActor
     getDragActor: function () {
         // return a Clutter.Actor. Should be independent to any of my actors
         // as its size/opacity/position will be modified and it will be destroyed
         // upon completion of the drag.
     },

For example, you might have made a button containing an icon and text, and made that button draggable by passing it in to {@link DND.makeDraggable}.
However, when the item is dragged you might wish just the icon to be the drag actor, so you could create an instance of the icon and return it in `getDragActor`.

#### <a id="draggable-getDragActorSource">`getDragActorSource`</a>

If a class implements `getDragActor`, it can also (optionally) implement [getDragActorSource`]{@link DND.Draggable#getDragActorSource} in the Draggable API.

This should provide an actor (that the class owns) to which the drag actor will "snap back" to upon completion of the drag.
This is optional (if not implemented, the actor passed in to `makeDraggable` will be used).

For example, you might have made a button containing an icon and text, and made that button draggable by passing it in to {@link DND.makeDraggable}.
Then you implemented `getDragActor` to make the draggable be just the icon.

When the drag completes, the icon will by default "snap back" to the centre of the button (being icon + text). 
If you implement `getDragActorSource` to return the icon component of the button, the draggable (icon) will "snap back" to the icon of the button, so the two are properly aligned.

### <a id="draggable-example">Example: {@link AppDisplay.AppWellIcon}</a>
The following is an icon used for applications in the Dash and Overview, showing an application's icon and its title underneath. The icon and title together are placed into a {@link St.Button}, being {@link AppDisplay.AppWellIcon#actor}.

![Example: A {@link AppDisplay.AppWellIcon}](pics/AppWellIcon.png)

We wish the AppWellIcon to be draggable, so we use {@link DND.makeDraggable} in the constructor on {@link AppDisplay.AppWellIcon#actor} (our St.Button), and ensure that `this.actor._delegate` points back to the AppWellIcon class (the following from `appDisplay.js`).

    AppWellIcon.prototype = {
        _init : function(app, iconParams, onActivateOverride) {
            // ...
            this.actor = new St.Button({ style_class: 'app-well-app',
                                         reactive: true,
                                         button_mask: St.ButtonMask.ONE | St.ButtonMask.TWO,
                                         can_focus: true,
                                         x_fill: true,
                                         y_fill: true });
            this.actor._delegate = this;
            // ...
        },
        // ...
    };


Now, when the AppWellIcon is dragged we wish *just the application icon* to be the drag actor (follow the mouse), rather than the entire St.Button (icon and text) being dragged.
Hence we implement {@link AppWellIcon#getDragActor} from the [Draggable API]{@link DND.Draggable}:

    getDragActor: function() {
        return this.app.create_icon_texture(Main.overview.dashIconSize);
    },

This returns an instance of the application's icon. 
Note that it is **not** the same icon instance as in our St.Button (`this.icon.icon`), but a *separate instance*. 
This is necessary.

If we drop the draggable (icon) somewhere invalid, it "snaps back" to the source actor's original position.
Since the source actor for the draggable is the entire St.Button (icon plus text), the draggable (icon) will snap back to the centre St.Button, and hence will be out of alignment with the icon component of the St.Button.
We wish to make it snap back to just the *icon* part of the St.Button so that it appears to snap back to exactly where it started.
Hence we implement {@link AppWellIcon#getDragActorSource} from the [Draggable API]{@link DND.Draggable}:

    getDragActorSource: function() {
        // this.icon.icon is the icon actor for the AppWellIcon
        return this.icon.icon;
    }

Note that we return the same icon instance that is used in our St.Button. This is because its position will be queried to make sure the draggable ends up aligning with this.

## <a id="drop-target">Accepting Drops</a>
Suppose you have a class `MyClass` and it has an actor `this.actor`, and if someone is holding a draggable over that actor you want the class to respond to it to be able to accept or handle the drag/drop.

To set up `MyClass` to accept drops (on behalf of an actor), you must set the actor to point back to the class:

    // suppose MyClass should be responsible for accepting drops on behalf of MyClass.actor.
    // then we must ensure MyClass.actor._delegate points back to MyClass:
    this.actor._delegate = this;

### <a id="drop-target-behaviour">The DropTarget API</a>
Then, `MyClass` must implement the [DropTarget API]{@link DND.DropTarget}, being functions [handleDragOver]{@link DND.DropTarget#handleDragOver} and [acceptDrop]{@link DND.DropTarget#acceptDrop}.

These are consulted when a draggable is held over (or dropped onto) an actor to determine whether that actor knows how to handle the drag or drop.

#### <a id="drop-target-handleDragOver">`handleDragOver`</a>
If a draggable is being held over `target` and `target._delegate.handleDragOver` exists, it is called.
It is given access to the drag actor, the drag actor and its class, and the actor it is requesting to drop on (`target`).

The `handleDragOver` function gives `target` (our potential drop target) the chance to indicate whether it is a potential drop target for this draggable or not.
The mouse cursor is updated to reflect this (to provide a hint to the user as to what would happen if they dropped the draggable here).

#### <a id="drop-target-acceptDrop">`acceptDrop`</a>
If the user *drops* an actor over a target actor `target`, and `target._delegate.acceptDrop` exists, it is called to handle the drop.

If it returns `true`, the drop is regarded as handled and we do not let any ancestors of `target` try to handle it. 
Otherwise, we continue walking up `target`'s ancestors giving them each a chanc to handle the drop.

This function should do whatever is expected to be done when the draggable is dropped there (for example, when an app icon is dropped onto the dash, its app should be added to the user's favourites).

It is not necessary for `acceptDrop` to do anything with the drag actor (though it can if it wants, for example you might reparent it to accept it into the target actor).
If `acceptDrop` doesn't reparent the drag actor, the cleanup of the drag actor is handled automatically by the draggable - be it restoring it at its original position, or destroying it.

### <a id="drop-target-example">Example: {@link Workspace.Workspace}</a>
A {@link Workspace.Workspace} is the space that shows all the window previews in the Overview (not the right-hand sidebar workspace previews).

It handles drag-and-drop: if the user drops a search result (eg an app icon from the Apps tab) or any of the window thumbnails (either from the right-hand sidebar or from the {@link Workspace.Workspace}) onto the Workspace (empty space on the main tab of the Overview), the search result or window will be activated.

Recall that to specify that an actor is a potential drop target, we need to make sure its `_delegate` property points back to the class {@link Workspace.Workspace}.

In this case, {@link Workspace.Workspace#_dropRect} is the actor of {@link Workspace.Workspace} that covers the "potential drop region". 
That is, if a draggable is held over {@link Workspace.Workspace#_dropRect}, we wish to handle the drop.

First we set the actor to point back to the class (the following is from `workspace.js`):

    Workspace.prototype = {
        _init : function(metaWorkspace, monitorIndex) {
            // ...

            // set up this._dropRect as the drop target for this class:
            this._dropRect._delegate = this;
            // ...
        },
        // ...
    };

Now we implement {@link Workspace.Workspace#handleDragOver} to provide the user with hints as to how a drop would be handled.

If the draggable is a {@link Workspace.WindowClone} or {@link WorkspaceThumbnail.WindowClone} (as tested by having a `realWindow` property), we return {@link DND.DragMotionResult.MOVE_DROP}.
This changes the mouse cursor to indicate that the window would be **moved** to this workspace if dropped.

If the draggable is a {@link SearchDisplay.SearchResult} (as tested by having a `shellWorkspaceLaunch` property), e.g. an app icon in the apps tab, we return {@link DND.DragMotionResult.COPY_DROP}.
This changes the mouse cursor to indicate that the search result (or application) would be **copied** to this workspace is dropped (i.e. create an instance of the result on this workspace).

Otherwise if the draggable is any other class, we wouldn't accept if if it was dropped, so we return {@link DND.DragMotionResult.CONTINUE} to indicate so.

    handleDragOver : function(source, actor, x, y, time) {
        if (source.realWindow && !this._isMyWindow(source.realWindow))
            // if `source` is a window thumbnail (Workspace.WindowClone or WorkspaceThumbnail.WindowClone),
            //  indicate that the window will be moved to this workspace if dropped.
            return DND.DragMotionResult.MOVE_DROP;
        if (source.shellWorkspaceLaunch)
            // if `source` is a search result (e.g. an AppDisplay.AppWellIcon),
            //  indicate that the result will be copied to this workspace if dropped
            //  (i.e. an instance will be opened).
            return DND.DragMotionResult.COPY_DROP;

        // otherwise we won't handle the drop, so return DND.DragMotionResult.CONTINUE to
        // let other drop targets be attempted.
        return DND.DragMotionResult.CONTINUE;
    },


Then we implement {@link Workspace.Workspace#acceptDrop} to determine what happens if the user actually *drops* a draggable over us.
If they dropped a window thumbnail on us we ensure that that window is moved to our workspace (returning `true` to handle the drop).
If they dropped a search result on us (e.g. an app icon from the apps tab) then we launch that search result on this workspace (see {@link SearchDisplay.SearchResult#shellWorkspacelaunch}), returning `true` to handle the drop.
Otherwise we refuse to accept the drop so we return `false`.

    acceptDrop : function(source, actor, x, y, time) {
        if (source.realWindow) {
            // If it's a window thumbnail (Workspace.WindowClone or WorkspaceThumbnail.WindowClone)
            //  and the thumbnail is *not* already on this workspace, we move it there and return `true` (handled the drop).
            // If the window is already on this workspace we just return false (drop not handled) and do nothing.
            let win = source.realWindow;
            if (this._isMyWindow(win))
                return false;

            // ... [code here to set up moving the window to this workspace]
            metaWindow.change_workspace_by_index(index,
                                                 false, // don't create workspace
                                                 time);
            return true;
        } else if (source.shellWorkspaceLaunch) {
            // If it's a SearchDisplay.SearchResult, we activate the result, passing in the workspace
            // in case it wishes to use it (e.g. apps will launch on this workspace).
            source.shellWorkspaceLaunch({ workspace: this.metaWorkspace ? this.metaWorkspace.index() : -1,
                                          timestamp: time });
            return true;
        }

        // otherwise we don't accept the drop, i.e. return false.
        return false;
    }

## <a id="drag-monitor">Drag Monitors</a>
Sometimes you want to know about/handle drags and drops where the target actor is irrelevant.

**Drag monitors** provide equivalents to the [DropTarget API]{@link DND.DropTarget} (`handleDragOver` and `acceptDrop`) when one wishes to monitor drags and drops regardless of what actor the draggable is being held over.
It may be of interest to know that drag monitors get to handle dragovers/accept drops *before* any class implementing the [DropTarget API]{@link DND.DropTarget}.

### <a id="drag-monitor-api">Drag Monitor API</a>
A drag monitor is simply an object that has at least one of `dragMotion` or `dragDrop` as a property. (It is documented as a typedef for now, {@link DND.DragMonitor}).

     let dragMonitor = {
         dragDrop: function(DropEvent) {
             // should return a DragDropResult
         },
         dragMotion: function(DropEvent) {
             // should return a DragMotionResult
         }
     };

It is not necessary that both `dragDrop` and ` dragMotion` be provided in the drag monitor - modify according to what you want to do.

To add a drag monitor, you initialise it as above and then use {@link DND.addDragMonitor}:

    const DND = imports.ui.dnd;
    DND.addDragMonitor(dragMonitor);

One can also use {@link DND.removeDragMonitor} to remove a drag monitor.

UPTO
#### <a id="drag-monitor-dragMotion>`dragMotion`</a>
#### <a id="drag-monitor-dragDrop>`dragDrop`</a>
### <a id="drag-monitor-example">Example: TODO</a>
Use WorkspacesView.WorkspacesDisplay
